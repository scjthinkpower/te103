/******************** (C) COPYRIGHT 2009 STMicroelectronics ********************
* File Name          : spi_flash.h
* Author             : MCD Application Team
* Version            : V3.1.0
* Date               : 10/30/2009
* Description        : Header for spi_flash.c file.
********************************************************************************
* THE PRESENT FIRMWARE WHICH IS FOR GUIDANCE ONLY AIMS AT PROVIDING CUSTOMERS
* WITH CODING INFORMATION REGARDING THEIR PRODUCTS IN ORDER FOR THEM TO SAVE TIME.
* AS A RESULT, STMICROELECTRONICS SHALL NOT BE HELD LIABLE FOR ANY DIRECT,
* INDIRECT OR CONSEQUENTIAL DAMAGES WITH RESPECT TO ANY CLAIMS ARISING FROM THE
* CONTENT OF SUCH FIRMWARE AND/OR THE USE MADE BY CUSTOMERS OF THE CODING
* INFORMATION CONTAINED HEREIN IN CONNECTION WITH THEIR PRODUCTS.
*******************************************************************************/

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __SPI_FLASH_H
#define __SPI_FLASH_H

/* Includes ------------------------------------------------------------------*/
#include "integer.h"
#include "stm32l0xx_hal.h"


/* Exported types ------------------------------------------------------------*/
/* Exported constants --------------------------------------------------------*/


/* Exported macro ------------------------------------------------------------*/
/* Select SPI FLASH: Chip Select pin low  */
#define SPI_FLASH_CS_LOW()       HAL_GPIO_WritePin(GPIOB, GPIO_PIN_12, GPIO_PIN_RESET)
/* Deselect SPI FLASH: Chip Select pin high */
#define SPI_FLASH_CS_HIGH()      HAL_GPIO_WritePin(GPIOB, GPIO_PIN_12, GPIO_PIN_SET)


// add by ww 20160211 leave the first numbers of sectors to be the configuration, must be the times of block .
// we left the first 64KB as the config file area .
#define SPI_FLASH_OFFSET_SECTORS          0 //8*16  // 8 sectors = 1 block ,         

// sector size of the media into the WORD variable pointed, valid sector size are 512, 1024, 2048, 4096
#define FLASH_SECTOR_SIZE  512   // one sector to the physical 

// erase block size in bytes
#define FLASH_BLOCK_SIZE    (4096 )

// erase block size of the flash memory in unit of sector

#define FLASH_BLOCK_COUNT_IN_SECTOR_UNIT  (FLASH_BLOCK_SIZE/FLASH_SECTOR_SIZE) // 8  //  1 block equal 4096 bytes , 1 sector equal 256 , so the block size actually block count

//number of available sectors on the drive
#define FLASH_SECTOR_COUNT  (((4096 * 1024)  / FLASH_SECTOR_SIZE) - SPI_FLASH_OFFSET_SECTORS)// total sector counts of this flash 

#define FLASH_PAGE_SIZE_NEW 256

#define FLASH_CMD_LENGTH 4

/* Exported functions ------------------------------------------------------- */
/*----- High layer function -----*/
void SPI_FLASH_Init(void);
void SPI_FLASH_SectorErase(uint32_t SectorAddr);
void SPI_FLASH_BulkErase(void);
void SPI_FLASH_PageWrite(uint8_t* pBuffer, uint32_t WriteAddr, uint16_t NumByteToWrite);
void SPI_FLASH_BufferWrite(uint8_t* pBuffer, uint32_t WriteAddr, uint16_t NumByteToWrite);
void SPI_FLASH_BufferRead(uint8_t* pBuffer, uint32_t ReadAddr, uint16_t NumByteToRead);
uint32_t SPI_FLASH_ReadID(void);
void SPI_FLASH_StartReadSequence(uint32_t ReadAddr);

/*----- Low layer function -----*/
uint8_t SPI_FLASH_ReadByte(void);
uint8_t SPI_FLASH_SendByte(uint8_t byte);
uint16_t SPI_FLASH_SendHalfWord(uint16_t HalfWord);
void SPI_FLASH_WriteEnable(void);
void SPI_FLASH_WaitForWriteEnd(void);
void SPI_FLASH_SendBuff(uint8_t* cmd,uint8_t* cmdResult,uint16_t length);
void SPI_FLASH_SendCMD(uint8_t* cmd,uint16_t length);


extern uint32_t spi_flash_disk_write(const BYTE *buff, uint32_t sector, uint32_t count) ;
extern uint32_t spi_flash_disk_read(uint8_t *buff, uint32_t sector, uint32_t count) ;

#endif /* __SPI_FLASH_H */

/******************* (C) COPYRIGHT 2009 STMicroelectronics *****END OF FILE****/

