/******************** (C) COPYRIGHT 2009 STMicroelectronics ********************
* File Name          : spi_flash.c
* Author             : MCD Application Team
* Version            : V3.1.0
* Date               : 10/30/2009
* Description        : This file provides a set of functions needed to manage the
*                      communication between SPI peripheral and SPI M25P64 FLASH.
********************************************************************************
* THE PRESENT FIRMWARE WHICH IS FOR GUIDANCE ONLY AIMS AT PROVIDING CUSTOMERS
* WITH CODING INFORMATION REGARDING THEIR PRODUCTS IN ORDER FOR THEM TO SAVE TIME.
* AS A RESULT, STMICROELECTRONICS SHALL NOT BE HELD LIABLE FOR ANY DIRECT,
* INDIRECT OR CONSEQUENTIAL DAMAGES WITH RESPECT TO ANY CLAIMS ARISING FROM THE
* CONTENT OF SUCH FIRMWARE AND/OR THE USE MADE BY CUSTOMERS OF THE CODING
* INFORMATION CONTAINED HEREIN IN CONNECTION WITH THEIR PRODUCTS.
*******************************************************************************/

/* Includes ------------------------------------------------------------------*/
#include "spi_flash.h"
#include "stm32l0xx_nucleo.h"
#include "hardware.h"
#include "main.h"
#include "debug.h"

/* Private typedef -----------------------------------------------------------*/
#define SPI_FLASH_PageSize    0x100

/* Private define ------------------------------------------------------------*/
#define WRITE      0x02  /* Write to Memory instruction */
#define WRSR       0x01  /* Write Status Register instruction */
#define WREN       0x06  /* Write enable instruction */

#define READ       0x03  /* Read from Memory instruction */
#define RDSR       0x05  /* Read Status Register instruction  */
#define RDID       0x9F  /* Read identification */
#define SE         0x20 // 0xD8  /* Sector Erase instruction */  // page 20 of w25q32 
#define BE         0xC7  /* Bulk Erase instruction */

#define WIP_Flag   0x01  /* Write In Progress (WIP) flag */

#define Dummy_Byte 0xA5

/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
extern SPI_HandleTypeDef SpiHandle;
extern uint8_t BackUpData[8 * 512];		//BackUpData[4][512];
extern __IO uint32_t wTransferState;
/* Private function prototypes -----------------------------------------------*/
/* Private functions ---------------------------------------------------------*/

/*******************************************************************************
* Function Name  : SPI_FLASH_Init
* Description    : Initializes the peripherals used by the SPI FLASH driver.
* Input          : None
* Output         : None
* Return         : None
*******************************************************************************/
void SPI_FLASH_Init(void)
{
   /* Set the SPI parameters */
  SpiHandle.Instance               = SPIx;
  SpiHandle.Init.BaudRatePrescaler = SPI_BAUDRATEPRESCALER_256;
  SpiHandle.Init.Direction         = SPI_DIRECTION_2LINES;
  SpiHandle.Init.CLKPhase          = SPI_PHASE_2EDGE;
  SpiHandle.Init.CLKPolarity       = SPI_POLARITY_HIGH;
  SpiHandle.Init.DataSize          = SPI_DATASIZE_8BIT;
  SpiHandle.Init.FirstBit          = SPI_FIRSTBIT_MSB;
  SpiHandle.Init.TIMode            = SPI_TIMODE_DISABLE;
  SpiHandle.Init.CRCCalculation    = SPI_CRCCALCULATION_DISABLE;
  SpiHandle.Init.CRCPolynomial     = 7;
  SpiHandle.Init.NSS               = SPI_NSS_SOFT;
	SpiHandle.Init.Mode = SPI_MODE_MASTER;
	
	if(HAL_SPI_DeInit(&SpiHandle) != HAL_OK)
  {
    /* Initialization Error */
		DEBUG("SPI_DeInit fail");
		return;
  }
	
	 if(HAL_SPI_Init(&SpiHandle) != HAL_OK)
  {
    /* Initialization Error */
		DEBUG("SPI_Init fail");
		return;
  }
	//spi_flash_disk_read(BackUpData,0,1);
}

/*******************************************************************************
* Function Name  : SPI_FLASH_SectorErase
* Description    : Erases the specified FLASH sector.
* Input          : SectorAddr: address of the sector to erase.
* Output         : None
* Return         : None
*******************************************************************************/
void SPI_FLASH_SectorErase(uint32_t SectorAddr)
{
	uint8_t cmd[FLASH_CMD_LENGTH], cmdResult[FLASH_CMD_LENGTH];
  /* Send write enable instruction */
  SPI_FLASH_WriteEnable();

  /* Sector Erase */
  /* Select the FLASH: Chip Select low */
  SPI_FLASH_CS_LOW();
	#if 0
  /* Send Sector Erase instruction */
  SPI_FLASH_SendByte(SE);
  /* Send SectorAddr high nibble address byte */
  SPI_FLASH_SendByte((SectorAddr & 0xFF0000) >> 16);
  /* Send SectorAddr medium nibble address byte */
  SPI_FLASH_SendByte((SectorAddr & 0xFF00) >> 8);
  /* Send SectorAddr low nibble address byte */
  SPI_FLASH_SendByte(SectorAddr & 0xFF);
	#endif
	
	cmd[0] = SE;
	cmd[1] = (SectorAddr & 0xFF0000) >> 16;
	cmd[2] = (SectorAddr & 0xFF00) >> 8;
	cmd[3] = (SectorAddr & 0xFF);

	SPI_FLASH_SendBuff(cmd,cmdResult,FLASH_CMD_LENGTH);
	
  /* Deselect the FLASH: Chip Select high */
  SPI_FLASH_CS_HIGH();

  /* Wait the end of Flash writing */
  SPI_FLASH_WaitForWriteEnd();
}

/*******************************************************************************
* Function Name  : SPI_FLASH_BulkErase
* Description    : Erases the entire FLASH.
* Input          : None
* Output         : None
* Return         : None
*******************************************************************************/
void SPI_FLASH_BulkErase(void)
{
  /* Send write enable instruction */
  SPI_FLASH_WriteEnable();

  /* Bulk Erase */
  /* Select the FLASH: Chip Select low */
  SPI_FLASH_CS_LOW();
  /* Send Bulk Erase instruction  */
  SPI_FLASH_SendByte(BE);
  /* Deselect the FLASH: Chip Select high */
  SPI_FLASH_CS_HIGH();

  /* Wait the end of Flash writing */
  SPI_FLASH_WaitForWriteEnd();
}

/*******************************************************************************
* Function Name  : SPI_FLASH_PageWrite
* Description    : Writes more than one byte to the FLASH with a single WRITE
*                  cycle(Page WRITE sequence). The number of byte can't exceed
*                  the FLASH page size.
* Input          : - pBuffer : pointer to the buffer  containing the data to be
*                    written to the FLASH.
*                  - WriteAddr : FLASH's internal address to write to.
*                  - NumByteToWrite : number of bytes to write to the FLASH,
*                    must be equal or less than "SPI_FLASH_PageSize" value.
* Output         : None
* Return         : None
*******************************************************************************/
void SPI_FLASH_PageWrite(uint8_t* pBuffer, uint32_t WriteAddr, uint16_t NumByteToWrite)
{
	uint8_t cmd[FLASH_CMD_LENGTH], cmdResult[FLASH_CMD_LENGTH];
  /* Enable the write access to the FLASH */
  SPI_FLASH_WriteEnable();

  /* Select the FLASH: Chip Select low */
  SPI_FLASH_CS_LOW();
	#if 0
  /* Send "Write to Memory " instruction */
  SPI_FLASH_SendByte(WRITE);
  /* Send WriteAddr high nibble address byte to write to */
  SPI_FLASH_SendByte((WriteAddr & 0xFF0000) >> 16);
  /* Send WriteAddr medium nibble address byte to write to */
  SPI_FLASH_SendByte((WriteAddr & 0xFF00) >> 8);
  /* Send WriteAddr low nibble address byte to write to */
  SPI_FLASH_SendByte(WriteAddr & 0xFF);
	#endif
	
	cmd[0] = WRITE;
	cmd[1] = (WriteAddr & 0xFF0000) >> 16;
	cmd[2] = (WriteAddr & 0xFF00) >> 8;
	cmd[3] = (WriteAddr & 0xFF);

	SPI_FLASH_SendBuff(cmd,cmdResult,FLASH_CMD_LENGTH);

  /* while there is data to be written on the FLASH */
  while (NumByteToWrite--)
  {
		//if(*pBuffer!=0 && *pBuffer != 0xff)
		//if(DebugEnable) DEBUG("%x",*pBuffer)
    /* Send the current byte */
    SPI_FLASH_SendByte(*pBuffer);
    /* Point on the next byte to be written */
    pBuffer++;
  }
 
  /* Deselect the FLASH: Chip Select high */
  SPI_FLASH_CS_HIGH();

  /* Wait the end of Flash writing */
  SPI_FLASH_WaitForWriteEnd();
}

/*******************************************************************************
* Function Name  : SPI_FLASH_BufferWrite
* Description    : Writes block of data to the FLASH. In this function, the
*                  number of WRITE cycles are reduced, using Page WRITE sequence.
* Input          : - pBuffer : pointer to the buffer  containing the data to be
*                    written to the FLASH.
*                  - WriteAddr : FLASH's internal address to write to.
*                  - NumByteToWrite : number of bytes to write to the FLASH.
* Output         : None
* Return         : None
*******************************************************************************/
void SPI_FLASH_BufferWrite(uint8_t* pBuffer, uint32_t WriteAddr, uint16_t NumByteToWrite)
{
  uint8_t NumOfPage = 0, NumOfSingle = 0, Addr = 0, count = 0, temp = 0;

  Addr = WriteAddr % SPI_FLASH_PageSize;
  count = SPI_FLASH_PageSize - Addr;
  NumOfPage =  NumByteToWrite / SPI_FLASH_PageSize;
  NumOfSingle = NumByteToWrite % SPI_FLASH_PageSize;

  if (Addr == 0) /* WriteAddr is SPI_FLASH_PageSize aligned  */
  {
    if (NumOfPage == 0) /* NumByteToWrite < SPI_FLASH_PageSize */
    {
      SPI_FLASH_PageWrite(pBuffer, WriteAddr, NumByteToWrite);
    }
    else /* NumByteToWrite > SPI_FLASH_PageSize */
    {
      while (NumOfPage--)
      {
        SPI_FLASH_PageWrite(pBuffer, WriteAddr, SPI_FLASH_PageSize);
        WriteAddr +=  SPI_FLASH_PageSize;
        pBuffer += SPI_FLASH_PageSize;
      }

      SPI_FLASH_PageWrite(pBuffer, WriteAddr, NumOfSingle);
    }
  }
  else /* WriteAddr is not SPI_FLASH_PageSize aligned  */
  {
    if (NumOfPage == 0) /* NumByteToWrite < SPI_FLASH_PageSize */
    {
      if (NumOfSingle > count) /* (NumByteToWrite + WriteAddr) > SPI_FLASH_PageSize */
      {
        temp = NumOfSingle - count;

        SPI_FLASH_PageWrite(pBuffer, WriteAddr, count);
        WriteAddr +=  count;
        pBuffer += count;

        SPI_FLASH_PageWrite(pBuffer, WriteAddr, temp);
      }
      else
      {
        SPI_FLASH_PageWrite(pBuffer, WriteAddr, NumByteToWrite);
      }
    }
    else /* NumByteToWrite > SPI_FLASH_PageSize */
    {
      NumByteToWrite -= count;
      NumOfPage =  NumByteToWrite / SPI_FLASH_PageSize;
      NumOfSingle = NumByteToWrite % SPI_FLASH_PageSize;

      SPI_FLASH_PageWrite(pBuffer, WriteAddr, count);
      WriteAddr +=  count;
      pBuffer += count;

      while (NumOfPage--)
      {
        SPI_FLASH_PageWrite(pBuffer, WriteAddr, SPI_FLASH_PageSize);
        WriteAddr +=  SPI_FLASH_PageSize;
        pBuffer += SPI_FLASH_PageSize;
      }

      if (NumOfSingle != 0)
      {
        SPI_FLASH_PageWrite(pBuffer, WriteAddr, NumOfSingle);
      }
    }
  }
}

/*******************************************************************************
* Function Name  : SPI_FLASH_BufferRead
* Description    : Reads a block of data from the FLASH.
* Input          : - pBuffer : pointer to the buffer that receives the data read
*                    from the FLASH.
*                  - ReadAddr : FLASH's internal address to read from.
*                  - NumByteToRead : number of bytes to read from the FLASH.
* Output         : None
* Return         : None
*******************************************************************************/
void SPI_FLASH_BufferRead(uint8_t* pBuffer, uint32_t ReadAddr, uint16_t NumByteToRead)
{
	uint8_t cmd[FLASH_CMD_LENGTH], cmdResult[FLASH_CMD_LENGTH];
	//if(DebugEnable) DEBUG("SPI_FLASH_BufferRead **************************%d",ReadAddr);
  /* Select the FLASH: Chip Select low */
  SPI_FLASH_CS_LOW();

	#if 0
  /* Send "Read from Memory " instruction */
  SPI_FLASH_SendByte(READ);

  /* Send ReadAddr high nibble address byte to read from */
  SPI_FLASH_SendByte((ReadAddr & 0xFF0000) >> 16);
  /* Send ReadAddr medium nibble address byte to read from */
  SPI_FLASH_SendByte((ReadAddr& 0xFF00) >> 8);
  /* Send ReadAddr low nibble address byte to read from */
  SPI_FLASH_SendByte(ReadAddr & 0xFF);
	#endif
	
	cmd[0] = READ;
	cmd[1] = (ReadAddr & 0xFF0000) >> 16;
	cmd[2] = (ReadAddr & 0xFF00) >> 8;
	cmd[3] = (ReadAddr & 0xFF);
	
	SPI_FLASH_SendBuff(cmd,cmdResult,FLASH_CMD_LENGTH);

  while (NumByteToRead--) /* while there is data to be read */
  {
    /* Read a byte from the FLASH */
    *pBuffer = SPI_FLASH_SendByte(Dummy_Byte);
    /* Point to the next location where the byte read will be saved */
    pBuffer++;
  }

  /* Deselect the FLASH: Chip Select high */
  SPI_FLASH_CS_HIGH();
	//if(DebugEnable) DEBUG("SPI_FLASH_BufferRead **************************end");
}

/*******************************************************************************
* Function Name  : SPI_FLASH_ReadID
* Description    : Reads FLASH identification.
* Input          : None
* Output         : None
* Return         : FLASH identification
*******************************************************************************/
uint32_t SPI_FLASH_ReadID(void)
{
  uint32_t Temp = 0, Temp0 = 0, Temp1 = 0, Temp2 = 0;

  /* Select the FLASH: Chip Select low */
  SPI_FLASH_CS_LOW();

  /* Send "RDID " instruction */
  SPI_FLASH_SendByte(0x9F);

  /* Read a byte from the FLASH */
  Temp0 = SPI_FLASH_SendByte(Dummy_Byte);

  /* Read a byte from the FLASH */
  Temp1 = SPI_FLASH_SendByte(Dummy_Byte);

  /* Read a byte from the FLASH */
  Temp2 = SPI_FLASH_SendByte(Dummy_Byte);

  /* Deselect the FLASH: Chip Select high */
  SPI_FLASH_CS_HIGH();

  Temp = (Temp0 << 16) | (Temp1 << 8) | Temp2;

  return Temp;
}

/*******************************************************************************
* Function Name  : SPI_FLASH_StartReadSequence
* Description    : Initiates a read data byte (READ) sequence from the Flash.
*                  This is done by driving the /CS line low to select the device,
*                  then the READ instruction is transmitted followed by 3 bytes
*                  address. This function exit and keep the /CS line low, so the
*                  Flash still being selected. With this technique the whole
*                  content of the Flash is read with a single READ instruction.
* Input          : - ReadAddr : FLASH's internal address to read from.
* Output         : None
* Return         : None
*******************************************************************************/
void SPI_FLASH_StartReadSequence(uint32_t ReadAddr)
{
  /* Select the FLASH: Chip Select low */
  SPI_FLASH_CS_LOW();

  /* Send "Read from Memory " instruction */
  SPI_FLASH_SendByte(READ);

  /* Send the 24-bit address of the address to read from -----------------------*/
  /* Send ReadAddr high nibble address byte */
  SPI_FLASH_SendByte((ReadAddr & 0xFF0000) >> 16);
  /* Send ReadAddr medium nibble address byte */
  SPI_FLASH_SendByte((ReadAddr& 0xFF00) >> 8);
  /* Send ReadAddr low nibble address byte */
  SPI_FLASH_SendByte(ReadAddr & 0xFF);
}

/*******************************************************************************
* Function Name  : SPI_FLASH_ReadByte
* Description    : Reads a byte from the SPI Flash.
*                  This function must be used only if the Start_Read_Sequence
*                  function has been previously called.
* Input          : None
* Output         : None
* Return         : Byte Read from the SPI Flash.
*******************************************************************************/
uint8_t SPI_FLASH_ReadByte(void)
{
  return (SPI_FLASH_SendByte(Dummy_Byte));
}

/*******************************************************************************
* Function Name  : SPI_FLASH_SendByte
* Description    : Sends a byte through the SPI interface and return the byte
*                  received from the SPI bus.
* Input          : byte : byte to send.
* Output         : None
* Return         : The value of the received byte.
*******************************************************************************/
uint8_t SPI_FLASH_SendByte(uint8_t byte)
{
	uint8_t read = 0;
	wTransferState = TRANSFER_WAIT;
	switch(HAL_SPI_TransmitReceive_IT(&SpiHandle,(uint8_t*) &byte, (uint8_t*)&read,1))
	{
	case HAL_OK:
		//if(DebugEnable) DEBUG("Byte HAL_OK");
		break;
	case HAL_TIMEOUT:
		//if(DebugEnable) DEBUG("Byte HAL_TIMEOUT:%d",byte);
		break;
	case HAL_ERROR:
		//if(DebugEnable) DEBUG("Byte HAL_ERROR");
		break;
	case HAL_BUSY:
		//if(DebugEnable) DEBUG("Byte HAL_BUSY");
		break;
	default:
		break;
	}
	
	while (wTransferState == TRANSFER_WAIT)
  {
  }
	
		return read;
}



/*******************************************************************************
* Function Name  : SPI_FLASH_SendBuff
* Description    : Sends a byte through the SPI interface and return the byte
*                  received from the SPI bus.
* Input          : cmd : buff to send.
								 : cmdResult : buff to read.
								 : length: buff length
* Output         : None
* Return         : The value of the received byte.
*******************************************************************************/
void SPI_FLASH_SendBuff(uint8_t* cmd,uint8_t* cmdResult,uint16_t length)
{
	wTransferState = TRANSFER_WAIT;
	
	switch(HAL_SPI_TransmitReceive_IT(&SpiHandle,(uint8_t*)cmd,cmdResult, length))
	{
	case HAL_OK:
		//if(DebugEnable) DEBUG("Buff HAL_OK");
		break;
	case HAL_TIMEOUT:
		//if(DebugEnable) DEBUG("Buff HAL_TIMEOUT");
		break;
	case HAL_ERROR:
		//if(DebugEnable) DEBUG("Buff HAL_ERROR");
		break;
	case HAL_BUSY:
		//if(DebugEnable) DEBUG("Buff HAL_BUSY");
		break;
	default:
		break;
	}
	while (wTransferState == TRANSFER_WAIT)
  {
  }
}

/*******************************************************************************
* Function Name  : SPI_FLASH_SendHalfWord
* Description    : Sends a Half Word through the SPI interface and return the
*                  Half Word received from the SPI bus.
* Input          : Half Word : Half Word to send.
* Output         : None
* Return         : The value of the received Half Word.
*******************************************************************************/
uint16_t SPI_FLASH_SendHalfWord(uint16_t HalfWord)
{
 return 1;
}

/*******************************************************************************
* Function Name  : SPI_FLASH_WriteEnable
* Description    : Enables the write access to the FLASH.
* Input          : None
* Output         : None
* Return         : None
*******************************************************************************/
void SPI_FLASH_WriteEnable(void)
{
  /* Select the FLASH: Chip Select low */
  SPI_FLASH_CS_LOW();

  /* Send "Write Enable" instruction */
  SPI_FLASH_SendByte(WREN);

  /* Deselect the FLASH: Chip Select high */
  SPI_FLASH_CS_HIGH();
}

/*******************************************************************************
* Function Name  : SPI_FLASH_WaitForWriteEnd
* Description    : Polls the status of the Write In Progress (WIP) flag in the
*                  FLASH's status  register  and  loop  until write  opertaion
*                  has completed.
* Input          : None
* Output         : None
* Return         : None
*******************************************************************************/
void SPI_FLASH_WaitForWriteEnd(void)
{
  uint8_t FLASH_Status = 0;

  /* Select the FLASH: Chip Select low */
  SPI_FLASH_CS_LOW();

  /* Send "Read Status Register" instruction */
  SPI_FLASH_SendByte(RDSR);

  /* Loop as long as the memory is busy with a write cycle */
  do
  {
    /* Send a dummy byte to generate the clock needed by the FLASH
    and put the value of the status register in FLASH_Status variable */
    FLASH_Status = SPI_FLASH_SendByte(Dummy_Byte);

  }
  while ((FLASH_Status & WIP_Flag) == SET); /* Write in progress */

  /* Deselect the FLASH: Chip Select high */
  SPI_FLASH_CS_HIGH();
}

uint32_t spi_flash_disk_write(const BYTE *buff, uint32_t sector, uint32_t count) {
	uint32_t WriteBlockNum;
	uint16_t IndexTmp, OffsetPage;
	uint32_t WriteBlockAddr ; 

	
	//块码: 写数据所在块号码
	WriteBlockNum = sector   / FLASH_BLOCK_COUNT_IN_SECTOR_UNIT;

	//页码: 写数据在块中(偏移)页码
	OffsetPage = sector  % FLASH_BLOCK_COUNT_IN_SECTOR_UNIT;
	
	//将块数据拷贝到数组 BackUpData 4096中:

	WriteBlockAddr = WriteBlockNum * FLASH_BLOCK_SIZE ; 
	
	SPI_FLASH_BufferRead((uint8_t*)(&BackUpData[0]), WriteBlockAddr , FLASH_BLOCK_SIZE);
	
	//擦除写页所在块:
	SPI_FLASH_SectorErase(WriteBlockAddr);
	
	// return write the block  with modify //
	for (IndexTmp = 0; IndexTmp < FLASH_BLOCK_COUNT_IN_SECTOR_UNIT; IndexTmp++ )
	{
		if ((IndexTmp>=OffsetPage)&&(IndexTmp < (OffsetPage+count)))
		{
		  SPI_FLASH_PageWrite((uint8_t *)buff, WriteBlockAddr + IndexTmp * FLASH_SECTOR_SIZE, 1 * FLASH_PAGE_SIZE_NEW );
		  buff = (uint8_t *)buff + FLASH_PAGE_SIZE_NEW;
		  SPI_FLASH_PageWrite((uint8_t*)buff, WriteBlockAddr  + IndexTmp * FLASH_SECTOR_SIZE + FLASH_PAGE_SIZE_NEW,
		  	                                1 * FLASH_PAGE_SIZE_NEW) ; 
		  buff = (uint8_t *)buff + FLASH_PAGE_SIZE_NEW ; 
		}
		else
		{
		  //将数组 BackUpData[4][512] 中块数据回写到STM Flash中:
		  SPI_FLASH_PageWrite((uint8_t *)(&BackUpData[IndexTmp * FLASH_SECTOR_SIZE]), WriteBlockAddr + IndexTmp * FLASH_SECTOR_SIZE, 
		                                       1 * FLASH_PAGE_SIZE_NEW);
		   SPI_FLASH_PageWrite((uint8_t *)(&BackUpData[IndexTmp * FLASH_SECTOR_SIZE + FLASH_PAGE_SIZE_NEW]), 
		   	                                WriteBlockAddr +IndexTmp * FLASH_SECTOR_SIZE + FLASH_PAGE_SIZE_NEW ,
		                                        1 * FLASH_PAGE_SIZE_NEW);
		}
	}
	return 	0;
}

uint32_t spi_flash_disk_read(uint8_t *buff, uint32_t sector, uint32_t count) {
    SPI_FLASH_BufferRead(buff, sector * FLASH_SECTOR_SIZE,  count * FLASH_SECTOR_SIZE) ;
    return 0 ;
}





