#include <string.h>
#include <stdint.h>

#include "main.h"
#include "hardware.h"
#include "debug.h"
#include "utility.h"
#include "eeprom.h"
#include "config_file.h"
  
#include "FreeRTOS.h"
#include "task.h"
#include "semphr.h"
#include "gsm.h"
#include "common.h"
#include "menu.h"
#include "gps.h"


/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
#define RTC_ASYNCH_PREDIV    0x7F
	//#define RTC_SYNCH_PREDIV     0x0130
#define RTC_SYNCH_PREDIV   0x00FF

/* Private variables ---------------------------------------------------------*/
extern pFunction JumpToApplication;
extern uint32_t JumpAddress;
extern RTC_HandleTypeDef RTCHandle;

/* Private function prototypes -----------------------------------------------*/
uint8_t HardwareInit(void);
void SystemClock_Config(void);
int GetKey_Unblocked(void);

/* CRC handler declaration */
volatile uint16_t GlobalStatus;

int jump = false ;	 // jump = true , indicate directly run the application , false indicate do bootloader things
int wait_seconds = 0 ;
uint8_t downloadfilename[10]={0};
WWDG_HandleTypeDef WwdgHandle;
bool isRs232 = false;

/**
  * @brief  System Clock Configuration
  *         The system Clock is configured as follow : 
  *            System Clock source            = MSI
  *            SYSCLK(Hz)                     = 2000000
  *            HCLK(Hz)                       = 2000000
  *            AHB Prescaler                  = 1
  *            APB1 Prescaler                 = 1
  *            APB2 Prescaler                 = 1
  *            Flash Latency(WS)              = 0
  *            Main regulator output voltage  = Scale3 mode
  * @retval None
  */
void bakSystemClock_Config(void)
{
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  
  /* Enable MSI Oscillator */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_MSI;
  RCC_OscInitStruct.MSIState = RCC_MSI_ON;
  RCC_OscInitStruct.MSIClockRange = RCC_MSIRANGE_5;
  RCC_OscInitStruct.MSICalibrationValue=0x00;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_NONE;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct)!= HAL_OK)
  {
    /* Initialization Error */
    while(1); 
  }
  
  /* Select MSI as system clock source and configure the HCLK, PCLK1 and PCLK2 
     clocks dividers */
  RCC_ClkInitStruct.ClockType = (RCC_CLOCKTYPE_SYSCLK | RCC_CLOCKTYPE_HCLK | RCC_CLOCKTYPE_PCLK1 | RCC_CLOCKTYPE_PCLK2);
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_MSI;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV1;  
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;  
  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_0)!= HAL_OK)
  {
    /* Initialization Error */
    while(1); 
  }
  /* Enable Power Control clock */
  __HAL_RCC_PWR_CLK_ENABLE();
  
  /* The voltage scaling allows optimizing the power consumption when the device is 
     clocked below the maximum system frequency, to update the voltage scaling value 
     regarding system frequency refer to product datasheet.  */
  __HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE3);
  
  /* Disable Power Control clock */
  __HAL_RCC_PWR_CLK_DISABLE();
}


/**
  * @brief  System Clock Configuration
  *         The system Clock is configured as follow : 
  *            System Clock source            = PLL (HSI)
  *            SYSCLK(Hz)                     = 32000000
  *            HCLK(Hz)                       = 32000000
  *            AHB Prescaler                  = 1
  *            APB1 Prescaler                 = 1
  *            APB2 Prescaler                 = 1
  *            HSI Frequency(Hz)              = 16000000
  *            PLL_MUL                        = 4
  *            PLL_DIV                        = 2
  *            Flash Latency(WS)              = 1
  *            Main regulator output voltage  = Scale1 mode
  * @param  None
  * @retval None
  */
static void HSISystemClock_Config(void)
{
  RCC_ClkInitTypeDef RCC_ClkInitStruct;
  RCC_OscInitTypeDef RCC_OscInitStruct;
  
  /* Enable Power Control clock */
  __HAL_RCC_PWR_CLK_ENABLE();
  
  /* The voltage scaling allows optimizing the power consumption when the device is 
     clocked below the maximum system frequency, to update the voltage scaling value 
     regarding system frequency refer to product datasheet.  */
  __HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE1);

  /* Enable HSI Oscillator and activate PLL with HSI as source */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI;
  RCC_OscInitStruct.HSEState = RCC_HSE_OFF;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSI;
  RCC_OscInitStruct.PLL.PLLMUL = RCC_PLLMUL_4;
  RCC_OscInitStruct.PLL.PLLDIV = RCC_PLLDIV_2;
  RCC_OscInitStruct.HSICalibrationValue = 0x10;
  if(HAL_RCC_OscConfig(&RCC_OscInitStruct)!= HAL_OK) 
  {
    /* Initialization Error */
    Error_Handler();
  }

  /* Select PLL as system clock source and configure the HCLK, PCLK1 and PCLK2 
     clocks dividers */
  RCC_ClkInitStruct.ClockType = (RCC_CLOCKTYPE_SYSCLK | RCC_CLOCKTYPE_HCLK | RCC_CLOCKTYPE_PCLK1 | RCC_CLOCKTYPE_PCLK2);
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV1;  
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;  
  if(HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_1) != HAL_OK)
  {
    /* Initialization Error */
    Error_Handler();
  }
}


/**
  * @brief  System Clock Configuration
  *         The system Clock is configured as follow : 
  *            System Clock source            = PLL (HSE)
  *            SYSCLK(Hz)                     = 32000000
  *            HCLK(Hz)                       = 32000000
  *            AHB Prescaler                  = 1
  *            APB1 Prescaler                 = 1
  *            APB2 Prescaler                 = 1
  *            Flash Latency(WS)              = 1
  *            Main regulator output voltage  = Scale1 mode
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_ClkInitTypeDef RCC_ClkInitStruct ={0};
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  
  /* Enable Power Control clock */
  __HAL_RCC_PWR_CLK_ENABLE();
  
  /* The voltage scaling allows optimizing the power consumption when the device is 
     clocked below the maximum system frequency, to update the voltage scaling value 
     regarding system frequency refer to product datasheet.  */
  __HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE1);
  
  /* Disable Power Control clock */
  __HAL_RCC_PWR_CLK_DISABLE();
  
  /* Enable HSE Oscillator */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSE;
  RCC_OscInitStruct.HSEState = RCC_HSE_ON;
  RCC_OscInitStruct.PLL.PLLSource   = RCC_PLLSOURCE_HSE;
  RCC_OscInitStruct.PLL.PLLState    = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLMUL      = RCC_PLLMUL_4;
  RCC_OscInitStruct.PLL.PLLDIV      = RCC_PLLDIV_2;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct)!= HAL_OK)
  {
    /* Initialization Error */
    while(1)
    {}
  }
  
  /* Select PLL as system clock source and configure the HCLK, PCLK1 and PCLK2 
     clocks dividers */
  RCC_ClkInitStruct.ClockType = (RCC_CLOCKTYPE_SYSCLK | RCC_CLOCKTYPE_HCLK | RCC_CLOCKTYPE_PCLK1 | RCC_CLOCKTYPE_PCLK2);
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV1;  
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;  
  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_1)!= HAL_OK)
  {
    /* Initialization Error */
    while(1)
    {}
  }
}

void RTC_Config(void)
{
 /* Configure RTC */
  RTCHandle.Instance = RTC;
  /* Set the RTC time base to 1s */
  /* Configure RTC prescaler and RTC data registers as follow:
    - Hour Format = Format 24
    - Asynch Prediv = Value according to source clock
    - Synch Prediv = Value according to source clock
    - OutPut = Output Disable
    - OutPutPolarity = High Polarity
    - OutPutType = Open Drain */
  RTCHandle.Init.HourFormat = RTC_HOURFORMAT_24;
  RTCHandle.Init.AsynchPrediv = RTC_ASYNCH_PREDIV;
  RTCHandle.Init.SynchPrediv = RTC_SYNCH_PREDIV;
  RTCHandle.Init.OutPut = RTC_OUTPUT_DISABLE;
  RTCHandle.Init.OutPutPolarity = RTC_OUTPUT_POLARITY_HIGH;
  RTCHandle.Init.OutPutType = RTC_OUTPUT_TYPE_OPENDRAIN;
  if(HAL_RTC_Init(&RTCHandle) != HAL_OK)
  {
   /* Initialization Error */
   DEBUG("RTC_Init error");
  }
}


/*******************************************************************************
 * Function Name	:	main
 * Description	:	Main program
 *******************************************************************************/
int main(void)
{
	GlobalStatus	= 0;
	wait_seconds = BOOT_WAIT_TOTAL_SECONDS ;  
  bootContext.uoption = UPDATE_NONE;
	uint8_t* name;
	
	if(HardwareInit()== ERROR)
		return ERROR;

#if 0
	gJumpToApplication();
  while (1)
  {
	}

#endif	
	
	DebugInit();

	DEBUG("boot version v1.6");
	
	RTC_Config();
	
#if 0	
	if(!init_sw_conf_table()){
		DEBUG("create conf table");
		if(!create_sw_conf_table()){
		DEBUG("create conf table fail");
		}
	}else{
		//show_sw_conf();
	}
#endif	
	
	name = readConfigFile();
	
	DEBUG("name %s %d",name,HAL_RTCEx_BKUPRead(&RTCHandle, RTC_BKP_DR3));
	
	//if(strncmp((char *)get_conf_elem(IAP_CONF),"0",1)){
	if(HAL_RTCEx_BKUPRead(&RTCHandle, RTC_BKP_DR3) == 1){
		HAL_RTCEx_BKUPWrite(&RTCHandle,RTC_BKP_DR3,0);
	if(name != NULL && strncmp(name,"0",1)){
		memset(downloadfilename,0,10);
		//memcpy(downloadfilename,get_conf_elem(IAP_CONF),strlen(get_conf_elem(IAP_CONF)));
		memcpy(downloadfilename,name,strlen(name));
		DEBUG("downloadfilename %s",downloadfilename);
		bootContext.uoption = UPDATE_FTP ; 
	}
	}
	else{
	while(1) {
	   if (GetKey_Unblocked() < 0 ) {
              HAL_Delay(200); 
              wait_seconds -= 100 ; 
              if (wait_seconds < 0) {
                  jump = true ; 
	           break ; 
				}

	   } else {
	       bootContext.uoption = UPDATE_SERIAL ; 
				 break ;
     }	 
}
	}
//bootContext.uoption = UPDATE_FTP;
 
 if (bootContext.uoption == UPDATE_NONE) {
      // workaround for test purpose , modified by ww 20141107  
      jump = true ; 
  } else if (bootContext.uoption == UPDATE_SERIAL) {
      jump = false ; 
  } else {
      jump = false ; 
  }
	
	if(!jump){
	
	/* Initialise Flash */
  //FLASH_If_Init();
  if (bootContext.uoption == UPDATE_SERIAL) {
  /* Display main menu */
  Main_Menu ();
	}else if(bootContext.uoption == UPDATE_FTP){
	DEBUG("init_modem");
	init_modem(); 
	//SerialDownload();
	}
	}
  /* Keep the user application running */
  else
  {
    /* Test if user code is programmed starting from address "APPLICATION_ADDRESS" */
    if (((*(__IO uint32_t*)APPLICATION_ADDRESS) & 0x2FFE0000 ) == 0x20000000)
    {
      /* Jump to user application */
      JumpAddress = *(__IO uint32_t*) (APPLICATION_ADDRESS + 4);
      JumpToApplication = (pFunction) JumpAddress;
      /* Initialize user application's Stack Pointer */
      __set_MSP(*(__IO uint32_t*) APPLICATION_ADDRESS);
      JumpToApplication();
    }
  }
  while (1)
  {
	}
}
void init_modem(){
	
	initWWDG();

  /*  GSM_THREAD  definition */
  osThreadDef(GSM_THREAD, vGsmTask, osPriorityNormal, 0, configMINIMAL_STACK_SIZE);
	
	/* Start thread GSM */
  osThreadCreate(osThread(GSM_THREAD), NULL); 
	
	/* GPS_THREAD definition */
  osThreadDef(GPS_THREAD, vGpsTask, osPriorityNormal, 0, configMINIMAL_STACK_SIZE);
	
	/* Start thread GPS */
  osThreadCreate(osThread(GPS_THREAD), NULL);
	
	startWWDG() ;
	
  /* Start scheduler */
  osKernelStart();

}

void gJumpToApplication(void)
{
      /* Jump to user application */
      JumpAddress = *(__IO uint32_t*) (APPLICATION_ADDRESS + 4);
      JumpToApplication = (pFunction) JumpAddress;
      /* Initialize user application's Stack Pointer */
      __set_MSP(*(__IO uint32_t*) APPLICATION_ADDRESS);
      JumpToApplication();	
}

/**
* brief : poll key unblocked 
* param :
* return :
*     -1 indicate no key entered
**/
int GetKey_Unblocked(void) {

    uint8_t key = 0 ; 
    __HAL_UART_FLUSH_DRREGISTER(&UartHandle);
    __HAL_UART_CLEAR_IT(&UartHandle, UART_CLEAR_OREF);
	
		__HAL_UART_FLUSH_DRREGISTER(&lpUartHandle);
    __HAL_UART_CLEAR_IT(&lpUartHandle, UART_CLEAR_OREF);
	
    /* Receive key */
    if(HAL_OK == HAL_UART_Receive(&UartHandle, &key, 1, 50))
		return 1;
		else if(HAL_OK == HAL_UART_Receive(&lpUartHandle, &key, 1, 50))
		{
			if(key == 'u'){
			isRs232 = true;
			return 1;
			}else
			return -1;
		}
		else 
		return -1;
}

/*******************************************************************************
* Function Name	:	HardwareInit
* Description	:	Initialize hardware
*******************************************************************************/
uint8_t HardwareInit(void)
{

	HAL_Init();

	SystemClock_Config();
	
	return 1;
}


uint8_t * getDownLoadFileName(void)
{
	return downloadfilename;
}


void initWWDG()
{
	
	if (__HAL_RCC_GET_FLAG(RCC_FLAG_WWDGRST) != RESET)
  {
    /* Insert 4s delay */
    HAL_Delay(4000);

    /* Clear reset flags */
    __HAL_RCC_CLEAR_RESET_FLAGS();
  }
	
	WwdgHandle.Instance = WWDG;

  WwdgHandle.Init.Prescaler = WWDG_PRESCALER_8;
  WwdgHandle.Init.Window    = 127;
  WwdgHandle.Init.Counter   = 127;

  if (HAL_WWDG_Init(&WwdgHandle) != HAL_OK)
  {
    /* Initialization Error */
    DEBUG(" HAL_WWDG_Init error");
  }

}

void startWWDG()
{
	#if 0
	if (HAL_WWDG_Start(&WwdgHandle) != HAL_OK)
  {
    DEBUG(" HAL_WWDG_Start error");
  }
	#endif
}


void refreshWWDG()
{
	if (HAL_WWDG_Refresh(&WwdgHandle) != HAL_OK)
  {
      DEBUG(" HAL_WWDG_Refresh error");
  }
}


void deInitWWDG()
{
	#if 0
	if (HAL_WWDG_DeInit(&WwdgHandle) != HAL_OK)
  {
      DEBUG(" HAL_WWDG_DeInit error");
  }
	#endif
	/* Enable WWDG reset state */
  __HAL_RCC_WWDG_FORCE_RESET();

  /* Release WWDG from reset state */
  __HAL_RCC_WWDG_RELEASE_RESET();
}


/**
  * @brief  Application Idle Hook
  * @param  None 
  * @retval None
  */
void vApplicationTickHook (void)
{
  refreshWWDG() ;
}



/**
  * @brief  Pre Sleep Processing
  * @param  ulExpectedIdleTime: Expected time in idle state
  * @retval None
  */
void PreSleepProcessing(uint32_t * ulExpectedIdleTime)
{
  /* Called by the kernel before it places the MCU into a sleep mode because
  configPRE_SLEEP_PROCESSING() is #defined to PreSleepProcessing().

  NOTE:  Additional actions can be taken here to get the power consumption
  even lower.  For example, peripherals can be turned off here, and then back
  on again in the post sleep processing function.  For maximum power saving
  ensure all unused pins are in their lowest power state. */

  /* 
    (*ulExpectedIdleTime) is set to 0 to indicate that PreSleepProcessing contains
    its own wait for interrupt or wait for event instruction and so the kernel vPortSuppressTicksAndSleep 
    function does not need to execute the wfi instruction  
  */
  *ulExpectedIdleTime = 0;
  
  /*Enter to sleep Mode using the HAL function HAL_PWR_EnterSLEEPMode with WFI instruction*/
  HAL_PWR_EnterSLEEPMode(PWR_MAINREGULATOR_ON, PWR_SLEEPENTRY_WFI);  
}

/**
  * @brief  Post Sleep Processing
  * @param  ulExpectedIdleTime: Not used
  * @retval None
  */
void PostSleepProcessing(uint32_t * ulExpectedIdleTime)
{
  /* Called by the kernel when the MCU exits a sleep mode because
  configPOST_SLEEP_PROCESSING is #defined to PostSleepProcessing(). */

  /* Avoid compiler warnings about the unused parameter. */
  (void) ulExpectedIdleTime;
}

