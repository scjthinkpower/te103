#include <stdio.h>
#include <stdarg.h>
#include <stdbool.h>
#include <string.h>
#include <math.h>

#include "hardware.h"
#include "FreeRTOS.h"
#include "queue.h"
#include "semphr.h"
#include "task.h"

#include "main.h"
#include "eeprom.h"
#include "gsm.h"
#include "debug.h"
#include "utility.h"
#include "ring_buffer.h"
#include "flash_if.h"
#include "common.h"
#include "config_file.h"

#define GSM_MODE_IGNORE			0
#define GSM_MODE_AT				1
#define GSM_MODE_STREAM			2
#define GSM_MODE_URC				3
#define GSM_MODE_UPDATE				4


#define GSM_GPRS_TIMEOUT		30000
#define GSM_TCP_TIMEOUT			30000
#define GSM_COMMAND_TIMEOUT	60000
#define GSM_INITIAL_WAIT		2000


#define GSM_STATUS_TCP_ON		0x01
#define GSM_STATUS_AUTH_ERR		0x02
#define GSM_STATUS_TCP_DISABLE	0x04
#define GSM_STATUS_REGISTER		0x08

#define FTP_CFG_CM 0
#define FTP_OPEN_CM 1
#define FTP_GET_CM 2
#define FTP_FILEOPEN_CM 3
#define FTP_FILESEEK_CM 4
#define FTP_FILEREAD_CM 5
#define FTP_FILECLOSE_CM 6

#define BUFFER_LENGTH		1024

typedef struct {
	uint8_t	(*GprsStop)(void);
	uint8_t	(*TcpClose)(void);
	uint8_t	(*GprsSet)(void);
	uint8_t	(*GprsStart)(void);
	uint8_t	(*TcpOpen)(void);	// 4
	const int8_t	* GetIMEI;
	void	(*TcpSend)(void);
	uint8_t	(*GetFTPFile)(void);
} GsmCommands_t;


typedef struct GsmContext_s
{
	const int8_t *	RxWaitFor;
	const int8_t *	WaitAlive;

	int					WaitAliveLen;
	uint16_t		HostIndex;
	uint8_t			SkipHostLine;
	uint8_t			RxMode;
	int8_t				Data;
	uint8_t			Status;
	uint8_t			Error;

	volatile portTickType 	PingTick;

	RingBuffer_t	RB_Rx;
	const GsmCommands_t * Commands;
	xSemaphoreHandle	ReadySemaphore;

} GsmContext_t;


/*	Private functions */
void	gsmSend		(const int8_t *text);
void	gsmSends	(const int8_t * cmd, ...);
uint8_t	gsmCmd0		(const int8_t* text);
uint8_t	gsmCmd1		(const int8_t* text, u16 timeout);
uint8_t	gsmCmd2		(const int8_t* text, u16 timeout, const int8_t* waitFor, const int8_t* waitAlive);
uint8_t	gsmCmdX0	(const int8_t* str1, int8_t* str2, const int8_t* str3);
uint8_t	gsmCmdX2	(const int8_t* str1, int8_t* str2, const int8_t* str3, int8_t* str4, const int8_t* str5, u16 timeout, const int8_t* wait);

uint8_t	gsmCheckPIN		(GsmContext_t *);
uint8_t	gsmIsRegister	(GsmContext_t *);
uint8_t	gsmWaitRegister	(GsmContext_t *);
void	gsmWaitNetwork	(GsmContext_t * gc);
int8_t *	gsmToken		(int8_t *);
void	gsmRxFlush		(GsmContext_t *);


uint8_t doSendCommand(uint8_t cmd,GsmContext_t *gc,uint32_t timeout,uint32_t timestep);

GsmContext_t GsmContext;

int8_t gsmRxBuffer[BUFFER_LENGTH+128];	// Buffer for data from GSM


const int8_t GSM_AT[]				= "AT\r\n";
const int8_t GSM_E0[]				= "ATE0\r\n";
const int8_t GSM_CFUN1[]			= "AT+CFUN=1\r\n";
const int8_t GSM_CMEE[]			= "AT+CMEE=1\r\n";
const int8_t GSM_GET_CPIN[]		= "AT+CPIN?\r\n";
const int8_t GSM_CPIN_RDY[]		= "+CPIN: READY";
const int8_t GSM_NO_SIM[]			= "+CME ERROR: 10";

const int8_t GSM_CREG[]			= "AT+CREG?\r\n";
const int8_t GSM_CREG0[]			= "AT+CREG=0\r\n";
const int8_t GSM_COPS0[]			= "AT+COPS?\r\n";
const int8_t GSM_CREG01[]			= "+CREG: 0,1";
const int8_t GSM_CREG03[]			= "+CREG: 0,3";
const int8_t GSM_CREG05[]			= "+CREG: 0,5";

const int8_t GSM_CGATT[]			= "AT+CGATT?\r\n";
const int8_t GSM_CGATT1[]			= "+CGATT: 1";
const int8_t GSM_LINE_END[]		= "\"\r\n";
const int8_t GSM_CRLF[]			= "\r\n";

const int8_t GSM_OK[]				= "OK";
const int8_t GSM_ERROR[]			= "ERROR";
const int8_t GSM_CME_ERROR[]		= "+CME ERROR:";
const int8_t GSM_CMS_ERROR[]		= "+CMS ERROR:";
const int8_t GSM_SHUTDOWN[]		= "SHUTDOWN";
const int8_t GSM_CONNECT[]		= "CONNECT";
const int8_t GSM_NO_CARRIER[]		= "NO CARRIER";

const int8_t GSM_K0[]				= "AT+IFC=0,0\r\n";
const int8_t GSM_ATI[]			= "ATI\r\n";

uint8_t GSM_FTP_OPEN[]		= "+QFTPOPEN: 0,0";
uint8_t GSM_FTP_GET[]    = "+QFTPGET: 0";
uint8_t GSM_FILE_OPEN[]  = "+QFOPEN:";
uint8_t gsm_aRxData;
uint32_t gAddress = APPLICATION_ADDRESS;
uint8_t file_handle[10];
uint32_t receiveData;


/*******************************************************************************
* Function Name	:	initGsmUart
* Description	:	initGsmUart GSM
*******************************************************************************/
void initGsmUart(void)
{
	if(HAL_UART_DeInit(&gsmUartHandle) != HAL_OK)
  {
     DEBUG(" gsm HAL_UART_DeInit error");
  } 
	
  if(HAL_UART_Init(&gsmUartHandle) != HAL_OK)
  {
    DEBUG(" gsm HAL_UART_Init error");
  }  
	
	if(HAL_UART_Receive_IT(&gsmUartHandle, (uint8_t *)&gsm_aRxData, RXBUFFERSIZE) != HAL_OK)
  {
    DEBUG(" gsm HAL_UART_Receive_IT error");
  }
}


/*******************************************************************************
* Function Name	:	GsmDeInit
* Description	:	Deinitialize GSM
*******************************************************************************/
void GsmDeInit(void)
{
	//HAL_UART_DeInit(&gsmUartHandle);
	//GSM_USART_CLK(DISABLE);
	//GSM_PIN_DEINIT();
	//GSM_PWR_OFF();
	
}
/*******************************************************************************
* Function Name :	GsmInit
* Description   :	Initialize GSM hardware,
*					check communication,
*					read IMEI code
*******************************************************************************/
uint8_t GsmInit(void)
{
	uint8_t result = E_GSM_NOT_READY;
	uint8_t retry;
	int8_t *item;
	
	
	__HAL_RCC_GPIOC_CLK_ENABLE();
	GPIO_InitStruct.Pin       = GPIO_PIN_9;
  GPIO_InitStruct.Mode      = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull      = GPIO_NOPULL;
  GPIO_InitStruct.Speed     = GPIO_SPEED_FREQ_VERY_HIGH;
	
	HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);
	DEBUG("*************************************** %d",HAL_GPIO_ReadPin(GPIOC, GPIO_PIN_9));
	
	if(HAL_GPIO_ReadPin(GPIOC, GPIO_PIN_9) == 0)
	{
	__disable_irq();
	HAL_NVIC_SystemReset();
	__enable_irq() ;
	}
		
	//if(strcmp((char *)get_conf_elem(IAP_CONF),"1")){
		//GSM_PWR_ON();
	//}
	
	GPS_PWR_ON();
	
	retry = 0;
	while (retry-- != 0)
	{
		osDelay(1000);
	}

	gsmUartHandle.Instance        = GSM_USART; 
  gsmUartHandle.Init.BaudRate   = 115200; 
  gsmUartHandle.Init.WordLength = UART_WORDLENGTH_8B; 
  gsmUartHandle.Init.StopBits   = UART_STOPBITS_1; 
  gsmUartHandle.Init.Parity     = UART_PARITY_NONE; 
  gsmUartHandle.Init.HwFlowCtl  = UART_HWCONTROL_NONE; 
  gsmUartHandle.Init.Mode       = UART_MODE_TX_RX;


	initGsmUart();
	
	GsmContext.RxMode = GSM_MODE_IGNORE;
	
	//startUpURC();
	
	gsmDTRLow();
	gsmRtsLow();
	
	retry = 8;
	while(retry-- != 0)
	{
		osDelay(500);
		if (E_OK == gsmCmd1(GSM_AT, 2000) &&
			E_OK == gsmCmd0(GSM_E0) &&
			E_OK == gsmCmd0(GSM_K0)
			)
		{
			gsmCmd0("AT+IPR=115200\r\n");
			
			if (E_OK == gsmCmd0("AT+QFLST=\"RAM:*\"\r\n"))
			{
				item = TokenBefore(gsmToken(NULL), "OK");
				DEBUG("item:%s",item);
				if(item == NULL || strncmp(item,"+QFLST:",7))
				{
					__disable_irq();
					HAL_NVIC_SystemReset();
					__enable_irq() ;
				
				}
				item = (int8_t*)strchr((char *)item,'"');
				item +=1;
				item = (int8_t*)strchr((char *)item,'"');
				item +=2;
				DEBUG("filesize:%s %d",item,atoi(item));
				if(!strncmp(item,"0",1)|| atoi(item) < 90000){
					__disable_irq();
					HAL_NVIC_SystemReset();
					__enable_irq() ;	
				}
			}
			
			if (E_OK == gsmCmd0(GSM_ATI))
			{
				item = TokenBefore(gsmToken(NULL), "OK");
				DEBUG("item:%s",item);
				#if 1
				if (item != NULL && strncmp((char*)item, "Revision: M35FAR01A08",13) == 0){
					if (E_OK == gsmCmd1("AT+CREG=0\r\n",60000))
					{
						return E_OK;
					}else 
					{
						continue;
					}
				}
				else{
					if (E_OK == gsmCmd1("AT+CREG=0\r\n",60000))
					{
					GSM_PWR_ON();
					return E_OK;
					}else 
					{			
						continue;
					}
				}
				#endif
			}
			//gsmCmd0("AT&F\r\n");
			if (E_OK == gsmCmd0(GSM_CMEE)
			&&	E_OK == gsmCmd0(GSM_CREG0)
			&&	E_OK == gsmCmd0(GSM_CFUN1)
				)
			{
				gsmCmd0(GSM_COPS0);
			
				result = E_OK;
				break;
		
			}
		}
	}
	
	return result;
}

/*******************************************************************************
* Function Name	:	gsmCmdX0
* Description	:
*******************************************************************************/
uint8_t gsmCmdX0(const int8_t* str1, int8_t* str2, const int8_t* str3)
{
	gsmSends(str1, str2, NULL);
	return gsmCmd0(str3);
}

/*******************************************************************************
* Function Name	:	gsmCmdX2
* Description	:
*******************************************************************************/
uint8_t gsmCmdX2(const int8_t* str1,
			int8_t* str2,
			const int8_t* str3,
			int8_t* str4,
			const int8_t* str5,
			u16 timeout, const int8_t* wait)
{
	gsmSends(str1, str2, str3, str4, NULL);
	return gsmCmd2(str5, timeout, wait, NULL);
}

/*******************************************************************************
* Function Name	:	gsmWaitNetwork
* Description	:	Wait Network Registration
*******************************************************************************/
void gsmWaitNetwork(GsmContext_t * gc)
{
	int retry;
	int restart = 0;
	GsmDeInit();
	while(1)
	{
		retry = 10;
		if(restart > 5 )
		{
				__disable_irq();
				HAL_NVIC_SystemReset();
				__enable_irq() ;
		}
		while (--retry != 0)
		{
			if (GsmInit() != E_OK)
			{
				GsmDeInit();
				osDelay(5000);
				continue;
			}

			//if (gsmWaitRegister(gc) == E_OK)
			{
				return;
			}
		}

		GsmDeInit();
		restart++;
		osDelay(5000);
	}
}

/*******************************************************************************
* Function Name	:	gsmWaitRegister
* Description	:	Wait GSM registration
*******************************************************************************/
uint8_t gsmWaitRegister(GsmContext_t * gc)
{
	uint8_t retry = 10;
	
	// Wait for SIM ready
	while (retry-- != 0)
	{
		gsmCheckPIN(gc);
		if (gc->Error == E_OK || gc->Error == E_TIMEOUT)
			break;
		if (gc->Error == E_NO_SIM)
			osDelay(1000);
		else
			osDelay(2000);
	}

	if (gc->Error == E_OK)
	{	// Wait for registration
		retry = 12;
		while (retry-- != 0 && gsmIsRegister(gc) != E_OK)
			osDelay(8000);
	}

	return gc->Error;
}

/*******************************************************************************
* Function Name	:	gsmCheckPIN
* Description	:	Check SIM ready
*******************************************************************************/
uint8_t gsmCheckPIN(GsmContext_t * gc)
{
	
	if (gsmCmd0(GSM_GET_CPIN) == E_OK)
	{
		if (FindToken(gsmToken(NULL), GSM_CPIN_RDY) == NULL)
			gc->Error = E_SIM_NOT_READY;
		else if (FindToken(gsmToken(NULL), GSM_NO_SIM) != NULL)
			gc->Error = E_NO_SIM;
	}
	
	return gc->Error;
}

/*******************************************************************************
* Function Name	:	gsmIsRegister
* Description	:	Check registration (CREG and CGATT)
*******************************************************************************/
uint8_t gsmIsRegister(GsmContext_t * gc)
{	
	gc->Status &= ~GSM_STATUS_REGISTER;
	if (gsmCmd0(GSM_CREG) == E_OK)
	{
		
		if (FindToken(gsmToken(NULL), GSM_CREG01) != NULL
		||	FindToken(gsmToken(NULL), GSM_CREG05) != NULL
			)
		{
		
			if (gsmCmd0(GSM_CGATT) == E_OK
			&&	FindToken(gsmToken(NULL), GSM_CGATT1) == NULL
				)
				gc->Error = E_NOT_REGISTER;
			
			else
				gc->Status |= GSM_STATUS_REGISTER;
			
		}
		
		else if (FindToken(gsmToken(NULL), GSM_CREG03) != NULL)
		{
			gc->Error = E_REG_DENIED;
		}
		else
			gc->Error = E_NOT_REGISTER;
		
	}
	return gc->Error;
}

/*******************************************************************************
* Function Name	:	gsmSend
* Description	:	Put string to GSM TX queue
* Input			:	string
* Return		:	result code
*******************************************************************************/
void gsmSend(const int8_t *text)
{
	DEBUG("%s",text);
	if(HAL_UART_Transmit_IT(&gsmUartHandle, (uint8_t*)text, strlen((char *)text)) != HAL_OK)
	{
		Error_Handler();
	}
}

/*******************************************************************************
* Function Name	:	gsmSends
* Description	:
*******************************************************************************/
void gsmSends(const int8_t * cmd, ...)
{
	va_list arg_ptr;
	const int8_t *p = cmd;

	va_start(arg_ptr, cmd);
	while (p != NULL)
	{
		gsmSend(p);
		p = (const int8_t *)va_arg(arg_ptr, const int8_t *);
	}
	va_end(arg_ptr);
}

/*******************************************************************************
* Function Name	:	gsmToken
* Description	:	Get first/next GSM token
* Input			:	NULL for first, previous token for next
* Return		:	next token or NULL
*******************************************************************************/
int8_t * gsmToken(int8_t * src)
{
	if (src == NULL)
		src = gsmRxBuffer;
	else
		while(*src++ != 0);
	return (*src == 0 ? NULL : src);
}

/*******************************************************************************
* Function Name	:	gsmShowResponse
* Description   :	Display response from GSM
*******************************************************************************/
void gsmShowResponse(void)
{
	int8_t * token = gsmToken(NULL);
	if (token != NULL)
	{
		DEBUG("%s",token);
	}
	/*
	while(token != NULL)
	{
		DEBUG(token);
		token = gsmToken(token);
	}
	*/
}

/*******************************************************************************
* Function Name	:	gsmShowError
* Description   :	Display last GSM Error
*******************************************************************************/
void gsmShowError(void)
{
	if (GsmContext.Error == E_OK){
		DEBUG("E_OK");
	}
	else if (GsmContext.Error == E_ERROR){
		DEBUG("E_ERROR");
	}
	else if (GsmContext.Error == E_TIMEOUT){
		DEBUG("E_TIMEOUT");
	}
	else if (GsmContext.Error == E_SEND_ERROR){
		DEBUG("E_SEND_ERROR");
	}
	else{
		DEBUG("Unknown E_?");
	}
}

/*******************************************************************************
* Function Name	:	gsmRxFlush
* Description   :
*******************************************************************************/
void gsmRxFlush(GsmContext_t * gc)
{
	gc->RxMode	= GSM_MODE_IGNORE;
	rb_Init(&gc->RB_Rx, gsmRxBuffer, sizeof(gsmRxBuffer)-4);
	gc->Error	= E_TIMEOUT;
	
	// Clear semaphore
	while(xSemaphoreTake(gc->ReadySemaphore, 1) == pdTRUE)
	{ }
	gc->RxMode	= GSM_MODE_AT;
}

/*******************************************************************************
* Function Name	:	gsmCmd0
* Description   :	Send string to GSM and wait response within 500ms
* Input         :	string to send
* Return		:	result code
*******************************************************************************/
uint8_t gsmCmd0(const int8_t* cmd)
{
	return gsmCmd2(cmd, 500, NULL, NULL);
}

/*******************************************************************************
* Function Name	:	gsmCmd1
* Description   :	Send string to GSM and wait response with timeout
* Input         :	string to send
*				:	timeout
* Return		:	result code
*******************************************************************************/
uint8_t gsmCmd1(const int8_t* text, uint16_t timeout)
{
	return gsmCmd2(text, timeout, NULL, NULL);
}
uint8_t gsmCmd1E(const int8_t* text, uint16_t timeout)
{
	return gsmCmd2(text, timeout, NULL, NULL);
}

/*******************************************************************************
* Function Name	:	gsmCmd2
* Description   :	Send string to GSM and wait response with timeout or alive,
*				:	always check ERROR, +CME ERROR: and +CMS ERROR:
* Input         :	string to send
*				:	timeout
*				:	wait for token for complete
*				:	wait for alive token for complete
* Return		:	result code
*******************************************************************************/
uint8_t gsmCmd2(const int8_t* text, uint16_t timeout, const int8_t* waitFor, const int8_t* waitAlive)
{
	GsmContext_t * gc = &GsmContext;

	if (text == NULL)
		return (gc->Error = E_OK);

	gc->RxMode	= GSM_MODE_IGNORE ;
	gc->WaitAlive = (int8_t *)waitAlive ;
	gc->WaitAliveLen = (gc->WaitAlive == NULL) ? 0 : strlen((char *)gc->WaitAlive);
	gc->RxWaitFor = (waitFor == NULL) ? GSM_OK : waitFor;
	gsmRxFlush(gc);
	
	gsmSend(text);

	// Wait for response and set ignore on timeout
	if (xSemaphoreTake(gc->ReadySemaphore, timeout) != pdTRUE)
		gc->RxMode = GSM_MODE_IGNORE;
	else
		gsmShowResponse();
		gsmShowError();

	return (gc->Error);
}

void gsmData(uint8_t data)
{
	register GsmContext_t *gc = &GsmContext;
		
		gc->Data = data;
	
	  receiveData++;
	
		if(gc->RxMode == GSM_MODE_UPDATE){
			rb_Put(&gc->RB_Rx, gc->Data);
		}else{

		if (gc->Data != '\r')
		{
			if (gc->Data == '\n') // Receive End Of Line
			gc->Data = 0;
				
			if (gc->RxMode == GSM_MODE_AT)
			{
				// Save data to buffer (use as linear)
				if (gc->Data == 0)
				{
					if (gc->RB_Rx.Start != gc->RB_Rx.Tail)// Check for empty token and ignore it
					{
						*gc->RB_Rx.Tail++ = 0;
						if (strcmp((char *)gc->RB_Rx.Start, (char *)gc->RxWaitFor) == 0)
						{
							*gc->RB_Rx.Tail = 0;		// Write Double 0 as End of tokens
							gc->Error = E_OK;
						}
						else if (strcmp((char *)gc->RB_Rx.Start, (char *)GSM_ERROR) == 0
							||	strncmp((char *)gc->RB_Rx.Start, (char *)GSM_CME_ERROR, sizeof(GSM_CME_ERROR) - 1) == 0
							||	strncmp((char *)gc->RB_Rx.Start, (char *)GSM_CMS_ERROR, sizeof(GSM_CMS_ERROR) - 1) == 0
								)
						{
							*gc->RB_Rx.Tail = 0;		// Write Double 0 as End of tokens
							gc->Error = E_ERROR;
						}
						else
							gc->RB_Rx.Start = gc->RB_Rx.Tail;
					}
				}
				else if (gc->RB_Rx.Tail >= gc->RB_Rx.End)
				{
					gc->Error = E_BAD_RESPONSE;
				}
				else
				{
					*gc->RB_Rx.Tail++ = gc->Data;
					if (gc->WaitAlive != NULL 
					&&	(gc->RB_Rx.Tail - gc->RB_Rx.Start) == gc->WaitAliveLen
					   )
					{
						// Check for non-CRLF tokens (such as CONNECT)
						if (strncmp((char *)gc->RB_Rx.Start, (char *)gc->WaitAlive, gc->WaitAliveLen) == 0)
						{
							*gc->RB_Rx.Tail++ = 0;		// Write Double 0 as End of tokens
							*gc->RB_Rx.Tail   = 0;
							gc->Error = E_OK;
						}
					}
				}

				if (gc->Error != E_TIMEOUT )
				{
					portBASE_TYPE RxNeedTaskSwitch;
					gc->RxMode = GSM_MODE_IGNORE;
					RxNeedTaskSwitch = pdFALSE;
					xSemaphoreGiveFromISR( gc->ReadySemaphore, &RxNeedTaskSwitch );
					portEND_SWITCHING_ISR( RxNeedTaskSwitch );
				}
			}
			else if (gc->RxMode == GSM_MODE_STREAM)
			{
				rb_Put(&gc->RB_Rx, gc->Data);
			}else if(gc->RxMode == GSM_MODE_URC){
				if(gc->Data != 0)
				rb_Put(&gc->RB_Rx, gc->Data);
			}
			
		}
	}
}

uint8_t handleReadData(GsmContext_t *gc)
{
	int8_t * token, *dst;
	uint32_t nlen;
	uint8_t result = E_ERROR;
	uint32_t wait = 4000;
	uint32_t ramsource;
	int8_t size = 0;

	while(wait != 0){
		if ( (token = FindTokenStartWith(gsmRxBuffer+2, "CONNECT ")) == NULL)
		{
			DEBUG("token is null");
			osDelay(100);
			wait -= 100;
			continue;
		}else
			break;
		}
	  if(token == NULL)
			 return result;
		//DEBUG("token is %s",token);
		dst = token = token + sizeof("CONNECT ")-1;
		while(*dst!='\r' &&  *(dst+1) != '\n'){
		DEBUG("dst:%x",*dst);
		dst++;
		size++;
		}
		*dst = 0;
		DEBUG("size:%d",size);
		if(size > 5)
			return result;
	  DEBUG("token:%s",token);
		nlen = atoi((char *)token);
		
    if(nlen > 1040)		
			return result;
		
		DEBUG("nlen:%d",nlen);
		DEBUG("len:%d",strlen((char *)token));
		dst = token = token+ strlen((char *)token)+2;
							
		DEBUG("start:%d",token-gsmRxBuffer);
		DEBUG("token:%x",*token);
		DEBUG("fw_address:%x",gAddress);
		
		wait = 4000;
		while(wait != 0){
		DEBUG("receiveData:%d",receiveData);
		if (receiveData >= nlen){
			 //osDelay(300);
		   break;
		}
			osDelay(100);
			wait -= 100;
		}
		wait = 4000;
		
		HAL_UART_DeInit(&gsmUartHandle);
		__disable_irq();
		
		if(token-gsmRxBuffer != 16){
			for(int i = 0;i< nlen;i++){
					gsmRxBuffer[i+8] = gsmRxBuffer[dst-gsmRxBuffer];
					dst++;
			}
		ramsource = (uint32_t) & gsmRxBuffer[8];
		}
		else
		ramsource = (uint32_t) & gsmRxBuffer[token-gsmRxBuffer];
		DEBUG("flash start");
		result = FLASH_If_Write(gAddress, (uint32_t*)ramsource, nlen/4);

		if (result == FLASHIF_OK)                   
		{
		gAddress += nlen;
		DEBUG("flash success");
		}
		DEBUG("result %d",result);
							
		if(nlen > 0 && nlen < 1024 && wait !=0){
			gc->RxMode = GSM_MODE_IGNORE;
			gAddress = APPLICATION_ADDRESS;
			return E_FW_OK;
		}
		return result;
}

/*******************************************************************************
* Function Name	:	gsmGetFileFTP
* Description	:
*******************************************************************************/
uint8_t gsmGetFileFTP(GsmContext_t *gc, char * filename)
{
	uint8_t result = E_OK;
	uint8_t retry = 5;
#if 0
	result = doSendCommand(FTP_CFG_CM,gc,2000,400);
		if(result!=E_OK)
			return result;
	result = doSendCommand(FTP_OPEN_CM,gc,2000,400);
		if(result!=E_OK)
			return result;
	result = doSendCommand(FTP_GET_CM,gc,2000,400);
		if(result!=E_OK)
			return result;
#endif
#if 1
open_file:
	while (retry-- != 0 && (result = doSendCommand(FTP_FILEOPEN_CM,gc,2000,400)) != E_OK)
			osDelay(2000);
		if(result!=E_OK)
			return result;
		retry = 5;
	while (retry-- != 0 && (result = doSendCommand(FTP_FILESEEK_CM,gc,2000,400)) != E_OK)
		osDelay(2000);
		if(result!=E_OK)
			return result;
	HAL_NVIC_DisableIRQ(GSM_USART_IRQn);
	FLASH_If_Erase(APPLICATION_ADDRESS);
	DEBUG("FLASH_If_Erase");
	gAddress = APPLICATION_ADDRESS;
	for(;;)
	{
	gsmRxFlush(gc);
	gc->RxMode = GSM_MODE_UPDATE;
	result = doSendCommand(FTP_FILEREAD_CM,gc,2000,400);
		if(result == E_FW_OK)
			break;
		if(result == E_ERROR)
		{
			initGsmUart();
			doSendCommand(FTP_FILECLOSE_CM,gc,2000,400);
			goto open_file;
		}

	}
	
	__disable_irq();
	HAL_NVIC_SystemReset();
	__enable_irq() ;
	#endif
	return result;
}

uint8_t doSendCommand(uint8_t cmd,GsmContext_t *gc,uint32_t timeout,uint32_t timestep)
{
  uint32_t wait = timeout;
	uint8_t result = E_OK;
	char sendMessage[50];
	uint8_t * Token;
	uint8_t filename[20]={0};
	
	switch(cmd)
	{
		case FTP_CFG_CM:
			sprintf(sendMessage, "AT+QFTPCFG=\"account\",\"%s\",\"%s\"\r\n", "firmware","123456");
		  Token = NULL;
			break;
		case FTP_OPEN_CM:
			sprintf(sendMessage, "AT+QFTPOPEN=\"%s\",%s\r\n", (char *)get_conf_elem(MAINSERVERIPADDR_CONF),"21");
			Token = GSM_FTP_OPEN;
			break;
		case FTP_GET_CM:
			//sprintf(sendMessage, "AT+QFTPGET=\"%s\",\"%s\"\r\n", "te100.bin","RAM:te100.bin");
			sprintf(sendMessage, "AT+QFTPGET=\"%s\",\"%s\"\r\n", (char *)getDownLoadFileName(),(char *)strcat(strcat((char *)filename,"RAM:"),(char *)getDownLoadFileName()));
			Token = GSM_FTP_GET;
			break;
		case FTP_FILEOPEN_CM:
			//sprintf(sendMessage, "AT+QFOPEN=\"%s\",%s\r\n", "RAM:te100.bin","0");
			sprintf(sendMessage, "AT+QFOPEN=\"%s\",%s\r\n", (char *)strcat(strcat((char *)filename,"RAM:"),(char *)getDownLoadFileName()),"0");
			Token = GSM_FILE_OPEN;
			break;
		case FTP_FILESEEK_CM:
			sprintf(sendMessage, "AT+QFSEEK=%s,%s,%s\r\n", file_handle,"0","0");
			Token = NULL;
			break;
		case FTP_FILEREAD_CM:
			sprintf(sendMessage, "AT+QFREAD=%s,%s\r\n", file_handle,"1024");
			Token = NULL;
			break;
		case FTP_FILECLOSE_CM:
			sprintf(sendMessage, "AT+QFCLOSE=%s\r\n", file_handle);
			Token = NULL;
			break;
	}
	
	while(wait != 0){
		if(cmd != FTP_FILEREAD_CM){
			if (E_OK != gsmCmd1((int8_t*)sendMessage, GSM_COMMAND_TIMEOUT))
			{
			osDelay(5000);
			wait -= timestep;
			continue;
		}else{
			gsmRxFlush(gc);
			gc->RxMode = GSM_MODE_URC;
			break;
	}
   }else{
		initGsmUart();
		gsmRxFlush(gc);
		gc->RxMode = GSM_MODE_UPDATE;
		memset(gsmRxBuffer,0,BUFFER_LENGTH+128);
		sprintf(sendMessage, "AT+QFREAD=%s,%s\r\n", file_handle,"1024");
		gsmSend((int8_t*)sendMessage);
		receiveData = 0;
		result = handleReadData(gc);
				return result;
	 }
	}
	
	
	if(Token != NULL){
		wait = timeout;
		while(strstr((char *)gsmToken(NULL), (char *)Token) == NULL){
		osDelay(10000);
		wait -= timestep;
		if(wait == 0)
			break;
	}
		strcpy((char *)file_handle,(char *)gsmToken(NULL)+9);
	}
  if(wait == 0)
		return E_TIMEOUT;
  return E_OK;
}

/*******************************************************************************
* Function Name	:	vGsmTask
* Description	:	GSM Task
*******************************************************************************/
void vGsmTask(void const  *pvArg)
{
	register GsmContext_t *gc = &GsmContext;
	
	//update_conf_elem(IAP_CONF, "0");
	deInitWWDG();
	
	vSemaphoreCreateBinary( gc->ReadySemaphore );
	xSemaphoreTake( gc->ReadySemaphore, 1 );
	
	gc->PingTick = 60;
	gc->Status = 0;
	
	GlobalStatus |= GSM_COMPLETE;
	
	gsmWaitNetwork(gc);
	if(gsmGetFileFTP(gc, "te101.bin") != E_OK)
	{
	__disable_irq();
	HAL_NVIC_SystemReset();
	__enable_irq() ;
	}
	while(1);
}

void startUpURC(void)
{
	register GsmContext_t *gc = &GsmContext;
	rb_Init(&gc->RB_Rx, gsmRxBuffer, sizeof(gsmRxBuffer)-4);
	gc->RxMode = GSM_MODE_URC;
	while(strstr((char *)gsmToken(NULL), (char *)GSM_CPIN_RDY)==NULL);
}

void gsmDTRLow(void)
{
	GPIO_InitTypeDef  GPIO_InitStruct;
	
	__HAL_RCC_GPIOB_CLK_ENABLE();
	
	GPIO_InitStruct.Pin       = GPIO_PIN_6;
  GPIO_InitStruct.Mode      = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull      = GPIO_NOPULL;
  GPIO_InitStruct.Speed     = GPIO_SPEED_FREQ_VERY_HIGH;
	
	HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);
	HAL_GPIO_WritePin(GPIOB, GPIO_PIN_6, GPIO_PIN_RESET);

}

void gsmRtsLow(void)
{
	GPIO_InitTypeDef  GPIO_InitStruct;
	
	__HAL_RCC_GPIOA_CLK_ENABLE();
	
	GPIO_InitStruct.Pin       = GPIO_PIN_15;
  GPIO_InitStruct.Mode      = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull      = GPIO_NOPULL;
  GPIO_InitStruct.Speed     = GPIO_SPEED_FREQ_VERY_HIGH;
	
	HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);
	HAL_GPIO_WritePin(GPIOA, GPIO_PIN_15, GPIO_PIN_RESET);
}



void gsmPowerOff(void)
{
  DEBUG("gsmPowerOff");
	GPIO_InitTypeDef  GPIO_InitStruct;
	
	__HAL_RCC_GPIOA_CLK_ENABLE();
	
	GPIO_InitStruct.Pin       = GPIO_PIN_12;
  GPIO_InitStruct.Mode      = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull      = GPIO_NOPULL;
  GPIO_InitStruct.Speed     = GPIO_SPEED_FREQ_VERY_HIGH;
	
	HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);
	HAL_GPIO_WritePin(GPIOA, GPIO_PIN_12, GPIO_PIN_RESET);
	#if 0
	__HAL_RCC_GPIOC_CLK_ENABLE();
	GPIO_InitStruct.Pin       = GPIO_PIN_9;
  GPIO_InitStruct.Mode      = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull      = GPIO_NOPULL;
  GPIO_InitStruct.Speed     = GPIO_SPEED_FREQ_VERY_HIGH;
	
	HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);
	HAL_GPIO_WritePin(GPIOC, GPIO_PIN_9, GPIO_PIN_RESET);
	#endif
	osDelay(500);

}

void gsmPowerOn(void)
{
	GPIO_InitTypeDef  GPIO_InitStruct;
	
	
	__HAL_RCC_GPIOC_CLK_ENABLE();
	GPIO_InitStruct.Pin       = GPIO_PIN_9;
  GPIO_InitStruct.Mode      = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull      = GPIO_NOPULL;
  GPIO_InitStruct.Speed     = GPIO_SPEED_FREQ_VERY_HIGH;
	
	HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);
	HAL_GPIO_WritePin(GPIOC, GPIO_PIN_9, GPIO_PIN_SET);
	
	__HAL_RCC_GPIOB_CLK_ENABLE();
	
	//PB2
	GPIO_InitStruct.Pin      	= GPIO_PIN_2;
  GPIO_InitStruct.Mode      = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull      = GPIO_NOPULL;
  GPIO_InitStruct.Speed     = GPIO_SPEED_FREQ_VERY_HIGH;
	
	HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);
	HAL_GPIO_WritePin(GPIOB, GPIO_PIN_2, GPIO_PIN_SET);
	
	//PA12
	__HAL_RCC_GPIOA_CLK_ENABLE();
	
	GPIO_InitStruct.Pin       = GPIO_PIN_12;
  GPIO_InitStruct.Mode      = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull      = GPIO_NOPULL;
  GPIO_InitStruct.Speed     = GPIO_SPEED_FREQ_VERY_HIGH;
	
	HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);
	HAL_GPIO_WritePin(GPIOA, GPIO_PIN_12, GPIO_PIN_SET);
	
	//PA11
	GPIO_InitStruct.Pin       = GPIO_PIN_11;
  GPIO_InitStruct.Mode      = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull      = GPIO_NOPULL;
  GPIO_InitStruct.Speed     = GPIO_SPEED_FREQ_VERY_HIGH;

  HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

	HAL_GPIO_WritePin(GPIOA, GPIO_PIN_11, GPIO_PIN_RESET);
	osDelay(1000);
	HAL_GPIO_WritePin(GPIOA, GPIO_PIN_11, GPIO_PIN_SET);
	
}
