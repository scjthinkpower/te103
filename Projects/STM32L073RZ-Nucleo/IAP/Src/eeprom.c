#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <math.h>
#include "eeprom.h"
#include "debug.h"

#define FLASH_USER_START_ADDR   DATA_EEPROM_BANK2_BASE   /* Start @ of user Flash area */
#define FLASH_USER_END_ADDR     DATA_EEPROM_BANK2_END   /* End @ of user Flash area */

uint32_t Address = 0;
__IO uint32_t data32 = 0 , MemoryProgramStatus = 0;

bool flash_eeprom(uint8_t * buff,uint32_t length)
{
	
	uint32_t index = 0;
	
	HAL_FLASHEx_DATAEEPROM_Unlock();

	/* Erase the user Flash area
    (area defined by FLASH_USER_START_ADDR and FLASH_USER_END_ADDR) ***********/

	 if (HAL_FLASHEx_DATAEEPROM_Erase(FLASH_USER_START_ADDR) != HAL_OK)
  {
    /*
      Error occurred while page erase.
      User can add here some code to deal with this error.
      PAGEError will contain the faulty page and then to know the code error on this page,
      user can call function 'HAL_FLASH_GetError()'
    */
	 index = HAL_FLASH_GetError();
	 DEBUG("DATAEEPROM ERASE FAIL %x",index);
   return false;
  }
	
  /* Program the user Flash area word by word
    (area defined by FLASH_USER_START_ADDR and FLASH_USER_END_ADDR) ***********/

  Address = FLASH_USER_START_ADDR;

  while (Address < FLASH_USER_END_ADDR && index <= length)
  {
		data32 =  buff[index];
		
		index++;
		//DEBUG("write eeprom data32:%c",data32);
		
    if (HAL_FLASHEx_DATAEEPROM_Program(FLASH_TYPEPROGRAMDATA_WORD, Address, data32) == HAL_OK)
    {
      Address = Address + 4;
    }
   else
    {
      /* Error occurred while writing data in Flash memory.
         User can add here some code to deal with this error */
		 index = HAL_FLASH_GetError();
		 DEBUG("DATAEEPROM FLASH FAIL %x",index);
     return false;
    }
  }
	HAL_FLASHEx_DATAEEPROM_Lock();
	
	/* Check if the programmed data is OK 
      MemoryProgramStatus = 0: data programmed correctly
      MemoryProgramStatus != 0: number of words not programmed correctly ******/
  Address = FLASH_USER_START_ADDR;
  MemoryProgramStatus = 0x0;
	index = 0;
  
  while (Address < FLASH_USER_END_ADDR && index <= length)
  {
    data32 = *(__IO uint32_t*)Address;
	
    if (data32 != buff[index])
    {
      MemoryProgramStatus++;  
    }
		index++;
    Address = Address + 4;
  }  

  /*Check if there is an issue to program data*/
  if (MemoryProgramStatus == 0)
  {
    return true;
  }
  else
  {
		DEBUG("MemoryProgramStatus %d",MemoryProgramStatus);
		return false;
  }
 
}

void read_eeprom(uint8_t * buff,uint32_t length)
{
	uint32_t index = 0;
	
	Address = FLASH_USER_START_ADDR;

  while (Address < FLASH_USER_END_ADDR && index <=length)
  {
    data32= *(__IO uint32_t*)Address;
		*buff++  = data32;
    Address = Address + 4;
		index++;	
  }
	DEBUG("buff index:%d",index);
}
