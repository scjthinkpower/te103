#include <stdio.h>
#include <string.h>
#include <math.h>

#include "hardware.h"
#include "FreeRTOS.h"
#include "queue.h"
#include "semphr.h"
#include "task.h"
#include "main.h"
#include "gps.h"
#include "gsm.h"
#include "debug.h"
#include "ring_buffer.h"
#include "debug.h"

char gpsRxBuffer[128];
GpsContext_t GpsContext;
uint8_t gps_aRxData;

void InitLed()
{
	GPIO_InitTypeDef  GPIO_InitStruct;
	//pwr led
	__HAL_RCC_GPIOB_CLK_ENABLE();
	GPIO_InitStruct.Pin       = GPIO_PIN_8;
  GPIO_InitStruct.Mode      = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull      = GPIO_NOPULL;
  GPIO_InitStruct.Speed     = GPIO_SPEED_FREQ_VERY_HIGH;
	
	HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);
	HAL_GPIO_WritePin(GPIOB, GPIO_PIN_8, GPIO_PIN_RESET);
	
	//gsm led
	GPIO_InitStruct.Pin       = GPIO_PIN_9;
  GPIO_InitStruct.Mode      = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull      = GPIO_NOPULL;
  GPIO_InitStruct.Speed     = GPIO_SPEED_FREQ_VERY_HIGH;
	
	HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);
	HAL_GPIO_WritePin(GPIOB, GPIO_PIN_9, GPIO_PIN_RESET);
	//gps led
	__HAL_RCC_GPIOA_CLK_ENABLE();
	GPIO_InitStruct.Pin       = GPIO_PIN_8;
  GPIO_InitStruct.Mode      = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull      = GPIO_NOPULL;
  GPIO_InitStruct.Speed     = GPIO_SPEED_FREQ_VERY_HIGH;
	
	HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);
	HAL_GPIO_WritePin(GPIOA, GPIO_PIN_8, GPIO_PIN_RESET);

}
void ledoff()
{
	GPIO_InitTypeDef  GPIO_InitStruct;
	//pwr led
	__HAL_RCC_GPIOB_CLK_ENABLE();
	GPIO_InitStruct.Pin       = GPIO_PIN_8;
  GPIO_InitStruct.Mode      = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull      = GPIO_NOPULL;
  GPIO_InitStruct.Speed     = GPIO_SPEED_FREQ_VERY_HIGH;
	
	HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);
	HAL_GPIO_WritePin(GPIOB, GPIO_PIN_8, GPIO_PIN_SET);
	
	//gsm led
	GPIO_InitStruct.Pin       = GPIO_PIN_9;
  GPIO_InitStruct.Mode      = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull      = GPIO_NOPULL;
  GPIO_InitStruct.Speed     = GPIO_SPEED_FREQ_VERY_HIGH;
	
	HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);
	HAL_GPIO_WritePin(GPIOB, GPIO_PIN_9, GPIO_PIN_SET);
	
	//gps led
	__HAL_RCC_GPIOA_CLK_ENABLE();
	GPIO_InitStruct.Pin       = GPIO_PIN_8;
  GPIO_InitStruct.Mode      = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull      = GPIO_NOPULL;
  GPIO_InitStruct.Speed     = GPIO_SPEED_FREQ_VERY_HIGH;
	
	HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);
	HAL_GPIO_WritePin(GPIOA, GPIO_PIN_8, GPIO_PIN_SET);

}


void gpsLedToggle()
{
	GPIO_InitTypeDef  GPIO_InitStruct;
	__HAL_RCC_GPIOA_CLK_ENABLE();
	GPIO_InitStruct.Pin       = GPIO_PIN_8;
  GPIO_InitStruct.Mode      = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull      = GPIO_NOPULL;
  GPIO_InitStruct.Speed     = GPIO_SPEED_FREQ_VERY_HIGH;
	
	HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);
	if(HAL_GPIO_ReadPin(GPIOA,GPIO_PIN_8) == GPIO_PIN_RESET)
	HAL_GPIO_WritePin(GPIOA, GPIO_PIN_8, GPIO_PIN_SET);
	else
	HAL_GPIO_WritePin(GPIOA, GPIO_PIN_8, GPIO_PIN_RESET);		

}

void gpsLedOn()
{
	GPIO_InitTypeDef  GPIO_InitStruct;
	__HAL_RCC_GPIOA_CLK_ENABLE();
	GPIO_InitStruct.Pin       = GPIO_PIN_8;
  GPIO_InitStruct.Mode      = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull      = GPIO_NOPULL;
  GPIO_InitStruct.Speed     = GPIO_SPEED_FREQ_VERY_HIGH;
	
	HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);
	HAL_GPIO_WritePin(GPIOA, GPIO_PIN_8, GPIO_PIN_RESET);
}

void gsmLedToggle()
{
	GPIO_InitTypeDef  GPIO_InitStruct;
	__HAL_RCC_GPIOB_CLK_ENABLE();
	GPIO_InitStruct.Pin       = GPIO_PIN_9;
  GPIO_InitStruct.Mode      = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull      = GPIO_NOPULL;
  GPIO_InitStruct.Speed     = GPIO_SPEED_FREQ_VERY_HIGH;
	
	HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);
	if(HAL_GPIO_ReadPin(GPIOB,GPIO_PIN_9) == GPIO_PIN_RESET)
	HAL_GPIO_WritePin(GPIOB, GPIO_PIN_9, GPIO_PIN_SET);
	else
	HAL_GPIO_WritePin(GPIOB, GPIO_PIN_9, GPIO_PIN_RESET);	
}

void gsmLedOn()
{
	GPIO_InitTypeDef  GPIO_InitStruct;
	__HAL_RCC_GPIOB_CLK_ENABLE();
	GPIO_InitStruct.Pin       = GPIO_PIN_9;
  GPIO_InitStruct.Mode      = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull      = GPIO_NOPULL;
  GPIO_InitStruct.Speed     = GPIO_SPEED_FREQ_VERY_HIGH;
	
	HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);
	HAL_GPIO_WritePin(GPIOB, GPIO_PIN_9, GPIO_PIN_RESET);
}



/*******************************************************************************
* Function Name	:	GpsInit
* Description	:	Initialize GPS
*******************************************************************************/
void GpsInit(GpsContext_t * gc, uint32_t speed)
{
	
	gc->WaitAliveSize = 0;
	gc->RxMode = GPS_MODE_START;

	GPS_PWR_ON();

	gpsUartHandle.Instance        = GPS_USART; 
  gpsUartHandle.Init.BaudRate   = speed; 
  gpsUartHandle.Init.WordLength = UART_WORDLENGTH_8B; 
  gpsUartHandle.Init.StopBits   = UART_STOPBITS_1; 
  gpsUartHandle.Init.Parity     = UART_PARITY_NONE; 
  gpsUartHandle.Init.HwFlowCtl  = UART_HWCONTROL_NONE; 
  gpsUartHandle.Init.Mode       = UART_MODE_TX_RX; 
	
	if(HAL_UART_DeInit(&gpsUartHandle) != HAL_OK)
  {
    Error_Handler();
  }  
  if(HAL_UART_Init(&gpsUartHandle) != HAL_OK)
  {
		Error_Handler();
  } 
	
	//__HAL_UART_ENABLE_IT(&gpsUartHandle, UART_IT_RXNE);
	
	//osThreadDef(GPS_RECEIVE_THREAD, vGpsReceive, osPriorityNormal, 0, configMINIMAL_STACK_SIZE);
	
	//osThreadCreate(osThread(GPS_RECEIVE_THREAD), NULL);
	
	//osThreadDef(GPS_HANDLE_THREAD, vGpsHandle, osPriorityNormal, 0, configMINIMAL_STACK_SIZE);
	
	//osThreadCreate(osThread(GPS_HANDLE_THREAD), NULL);

	//if(HAL_UART_Receive_IT(&gpsUartHandle, (uint8_t *)&gps_aRxData, RXBUFFERSIZE) != HAL_OK)
  //{
    //Error_Handler();
  //}
}

/*******************************************************************************
* Function Name	:	GpsDeInit
* Description	:	DeInitialize GPS
*******************************************************************************/
void GpsDeInit(GpsContext_t * gc)
{
	gc->RxMode = GPS_MODE_IGNORE;
	GPS_PWR_OFF();
	HAL_Delay(1000);
}

/*******************************************************************************
* Function Name	:	vGpsTask
* Description	:	GPS Task
*******************************************************************************/
void vGpsTask(void const *pvArg)
{
#if 0
	register GpsContext_t * gc = &GpsContext;
	DEBUG("restart_gps");
	GpsDeInit(gc);
	
	while (GlobalStatus & GPS_STOP)
		HAL_Delay(5000);
	
	gc->RxMode = GPS_MODE_IGNORE;

	GpsInit(gc, 115200);
		
	GlobalStatus |= GPS_COMPLETE;
#endif
	InitLed();
	while(1)
	{
	gsmLedToggle();
	osDelay(200);
	}
	
	
}

/*******************************************************************************
* Function Name	:	GPS_USART_IRQHandler
* Description	:	GPS USART interrupt handler
*******************************************************************************/
void gpsData(uint8_t data)
{	
	register GpsContext_t *gc = &GpsContext;
		
		gc->Data = data;
		
		if (gc->RxMode == GPS_MODE_IGNORE)
		{
			return;
		}
		else if (gc->RxMode == GPS_MODE_END
			||	gc->RxMode == GPS_MODE_END2)
		{
			if (gc->RxIndex > (sizeof(gpsRxBuffer) - 2))
			{	// String too long, ignore it and set wait a $
				gc->RxMode = (gc->RxMode == GPS_MODE_END2) ? GPS_MODE_CHECK : GPS_MODE_START;
			}
			else
			{
				if (gc->Data == '$')
					gc->RxIndex = 0;
					
				gpsRxBuffer[gc->RxIndex++] = gc->Data;
				if (gc->WaitAliveSize != 0)
				{
					if (gc->WaitAliveSize == gc->RxIndex)
					{
						if (strncmp(gpsRxBuffer, gc->WaitAliveText, gc->WaitAliveSize) == 0)
							gc->WaitAliveSize = 0;
						else
							gc->RxMode = (gc->RxMode == GPS_MODE_END2) ? GPS_MODE_CHECK : GPS_MODE_START;
					}
					else if (gc->Data == '\n')
					{
						gc->RxMode = (gc->RxMode == GPS_MODE_END2) ? GPS_MODE_CHECK : GPS_MODE_START;
					}
				}
				else if (gc->Data == '\n')
				{
					if (GPS_MODE_END2 || (gc->RxIndex > 6 && strncmp(gpsRxBuffer, "$GPRMC", 6) == 0))
					{
						gpsRxBuffer[gc->RxIndex] = 0;
						gc->RxMode = GPS_MODE_READY;
					}
					else
					{
						gc->RxMode = (gc->RxMode == GPS_MODE_END2) ? GPS_MODE_CHECK : GPS_MODE_START;
					}
				}
			}
		}
		else if (gc->RxMode == GPS_MODE_START || gc->RxMode == GPS_MODE_CHECK)
		{
			if (gc->Data == '$')
			{
				gc->RxIndex = 0;
				gpsRxBuffer[gc->RxIndex++] = gc->Data;
				gc->RxMode = (gc->RxMode == GPS_MODE_CHECK) ? GPS_MODE_CHECK2 : GPS_MODE_END;
			}
		}
		else if (gc->RxMode == GPS_MODE_CHECK2)
		{
			gpsRxBuffer[gc->RxIndex++] = gc->Data;
			if (gc->RxIndex == 3)
			{
				if (gpsRxBuffer[0] == '$'
				&&	gpsRxBuffer[1] == 'G'
				&&	gpsRxBuffer[2] == 'P'
					)
					gc->RxMode = GPS_MODE_END2;
				else
					gc->RxMode = GPS_MODE_CHECK;
			}
		}
}

void gpsPowerOff(void)
{
	GPIO_InitTypeDef  GPIO_InitStruct;
	
	__HAL_RCC_GPIOC_CLK_ENABLE();
	GPIO_InitStruct.Pin       = GPIO_PIN_10;
  GPIO_InitStruct.Mode      = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull      = GPIO_NOPULL;
  GPIO_InitStruct.Speed     = GPIO_SPEED_FREQ_VERY_HIGH;
	
	HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);
	HAL_GPIO_WritePin(GPIOC, GPIO_PIN_10, GPIO_PIN_RESET);
}


void gpsPowerOn(void)
{
	GPIO_InitTypeDef  GPIO_InitStruct;
	
	__HAL_RCC_GPIOC_CLK_ENABLE();
	GPIO_InitStruct.Pin       = GPIO_PIN_10;
  GPIO_InitStruct.Mode      = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull      = GPIO_NOPULL;
  GPIO_InitStruct.Speed     = GPIO_SPEED_FREQ_VERY_HIGH;
	
	HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);
	HAL_GPIO_WritePin(GPIOC, GPIO_PIN_10, GPIO_PIN_SET);
	
}

