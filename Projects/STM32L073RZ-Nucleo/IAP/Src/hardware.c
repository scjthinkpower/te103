#include "hardware.h"
#include "debug.h"
#include "gps.h"
#include "gsm.h"

uint8_t aRxGsmData = 0;
uint8_t aRxGpsData = 0;
uint8_t aRxShellData = 0;
UART_HandleTypeDef gsmUartHandle;
UART_HandleTypeDef gpsUartHandle;
UART_HandleTypeDef UartHandle;
UART_HandleTypeDef lpUartHandle;
SPI_HandleTypeDef SpiHandle;
RTC_HandleTypeDef RTCHandle;

__IO uint32_t wTransferState = TRANSFER_WAIT;

void Error_Handler(void)
{
   DEBUG("send Error_Handler");
}


/*******************************************************************************
* Function Name	:	GSM_USART_IRQHandler
* Description   :	GSM iterrupt handler
*******************************************************************************/
void GPS_GSM_USART_IRQHandler(void)
{	
	HAL_UART_IRQHandler(&gsmUartHandle);
	HAL_UART_IRQHandler(&gpsUartHandle);
}

/**
  * @brief  Tx Transfer completed callback
  * @param  UartHandle: UART handle. 
  * @note   This example shows a simple way to report end of IT Tx transfer, and 
  *         you can add your own implementation. 
  * @retval None
  */
void HAL_UART_TxCpltCallback(UART_HandleTypeDef *UartHandle)
{

}

/**
  * @brief  UART error callbacks
  * @param  UartHandle: UART handle
  * @note   This example shows a simple way to report transfer error, and you can
  *         add your own implementation.
  * @retval None
*/
void HAL_UART_ErrorCallback(UART_HandleTypeDef *UartHandle)
{
}

/**
  * @brief  Rx Transfer completed callback
  * @param  UartHandle: UART handle
  * @note   This example shows a simple way to report end of DMA Rx transfer, and 
  *         you can add your own implementation.
  * @retval None
  */
void HAL_UART_RxCpltCallback(UART_HandleTypeDef *UartHandle)
{
	 uint8_t ret = HAL_OK ; 
   if(UartHandle->Instance == GSM_USART){
	 do {
        ret = HAL_UART_Receive_IT(UartHandle, (uint8_t*)&aRxGsmData, RXBUFFERSIZE) ; 
    } while (ret != HAL_OK) ;
	 gsmData(aRxGsmData);
	 }
	 else if(UartHandle->Instance == GPS_USART){
	 do {
        ret = HAL_UART_Receive_IT(UartHandle, (uint8_t*)&aRxGpsData, RXBUFFERSIZE) ; 
    } while (ret != HAL_OK) ;
	 gpsData(aRxGpsData);
	 }
}


