#include <string.h>

#include "hardware.h"
#include "utility.h"
#include <nmea/tok.h>

int8_t* NextToken(int8_t *src)
{
	if (src != NULL)
	{
		while (*src++ != 0);
		if (*src != 0)
			return src;
	}
	return NULL;
}

int TokenSizeQuote(int8_t* src)
{
	int8_t* pos = src;
	if (src != NULL && *src != 0)
	{
		while(*src != 0 && *src != '"')
			src++;
		return (src - pos);
	}
	return 0;
}

int TokenSizeComma(int8_t* src)
{
	int8_t* pos = src;
	if (src != NULL && *src != 0)
	{
		while (*src != 0 && *src != ',')
			src++;
		return (src - pos);
	}
	return 0;
}

int8_t* TokenNextComma(int8_t *src)
{
	if (src != NULL)
	{
		while (*src != 0)
			if (*src++ == ',')
				break;
		if (*src != 0)
			return src;
	}
	return NULL;
}

int8_t* FindTokenStartWith(int8_t *src, const int8_t *pattern)
{
	int patt_len;
	patt_len = strlen((char *)pattern);
	while (src != NULL && *src != 0)
	{
		if (strncmp((char *)src,(char *) pattern, patt_len) == 0)
			return src;
		src = NextToken(src);
	}
	return NULL;
}

int8_t* FindToken(int8_t *src, const int8_t *pattern)
{
	while (src != NULL && *src != 0)
	{
		if (strcmp((char *)src, (char *)pattern) == 0)
			return src;
		src = NextToken(src);
	}
	return NULL;
}

int8_t* TokenBefore(int8_t *src, const int8_t *pattern)
{
	int8_t* before = NULL;
	while (src != NULL && *src != 0)
	{
		if (strcmp((char *)src, (char *)pattern) == 0)
			break;
		before = src;
		if ((src = NextToken(src)) == NULL)
			before = NULL;
	}
	return before;
}

int8_t* CharReplace(int8_t* src, int8_t from, int8_t to)
{
	int8_t* dst = src;
	if (src == NULL || *src == 0)
		return NULL;

	while(*src != 0)
		if (*src == from)
			*src++ = to;
		else
			src++;
	return dst;
}
