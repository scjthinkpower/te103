#ifndef __DEBUG_H__
#define __DEBUG_H__

#include "hardware.h"

#if ( USE_SERIAL_DEBUG_ENABLE )

#define DEBUG(fmt, arg...) \
    do {    \
        printf("DEBUG I: " fmt "\r\n" , ## arg) ; \
	} while (0) ;  
    
#else
#define DEBUG(fmt, arg...) \
    do {    \
         \
	} while (0) ;
	
#endif

void DebugInit(void);
#endif	// __DEBUG_H__
