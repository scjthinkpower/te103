#ifndef __EEPROM_H__
#define __EEPROM_H__

#include <stdbool.h>
#include "hardware.h"

#define DATAEEPROM_ERASE_ERROR -1
#define DATAEEPROM_PROGRAM_ERROR -2
#define DATAEEPROM_PROGRAM_VERIFICATION_ERROR -3

bool flash_eeprom(uint8_t *buff,uint32_t length);
void read_eeprom(uint8_t * buff,uint32_t length);
#endif
