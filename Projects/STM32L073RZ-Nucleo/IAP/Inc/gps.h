#ifndef __GPS_H__
#define __GPS_H__

#include <stdint.h>
#include "hardware.h"
#include "gsm.h"
#include "ring_buffer.h"
#include "FreeRTOS.h"
#include "semphr.h"

#define GPS_MODE_IGNORE 0
#define GPS_MODE_START	1
#define GPS_MODE_END	2
#define GPS_MODE_READY	3
#define GPS_MODE_CHECK	4
#define GPS_MODE_CHECK2	5
#define GPS_MODE_END2	6

struct GpsContext_s
{
			const char *WaitAliveText;
			uint16_t	RxIndex;
			uint16_t	WaitAliveSize;

			volatile uint8_t	RxMode;
			char		Data;
			RingBuffer_t	RB_Tx;
};

typedef struct GpsContext_s GpsContext_t;

extern GpsContext_t GpsContext;

void vGpsTask(void const *pvArg);
void gpsData(uint8_t data);
void gpsPowerOff(void);
void gpsPowerOn(void);
#endif
