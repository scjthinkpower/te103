#ifndef __GSM_H__
#define __GSM_H__

#include <stdint.h>

#define GSM_TASK_QUEUE_SIZE	8

#define GSM_TASK_CMD_NOP	0
#define GSM_TASK_CMD_GPS	1
#define GSM_TASK_CMD_PING	2
#define GSM_TASK_CMD_RET	3
#define GSM_TASK_CMD_ERR	4
#define GSM_TASK_CMD_SMS	5
#define GSM_TASK_CMD_LOGIN	6
#define GSM_TASK_CMD_UPDATE	7

char * GsmIMEI(void);
void vGsmTask(void const *pvArg);
void startUpURC(void);
void gsmRtsLow(void);
void gsmDTRLow(void);
void gsmPowerOn(void);
void gsmPowerOff(void);
void gpsPowerOn(void);
void gsmData(uint8_t data);
void init_modem(void);

#endif
