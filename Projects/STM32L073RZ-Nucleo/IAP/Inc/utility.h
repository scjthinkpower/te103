#ifndef __UTILITY_H__
#define __UTILITY_H__

int8_t* NextToken(int8_t *src);
int8_t* FindTokenStartWith(int8_t *src, const int8_t *pattern);
int8_t* FindToken(int8_t *src, const int8_t *pattern);
int8_t* TokenBefore(int8_t *src, const int8_t *pattern);

int32_t TokenSizeComma(int8_t* src);
int32_t TokenSizeQuote(int8_t* src);
int8_t* TokenNextComma(int8_t *src);

#endif
