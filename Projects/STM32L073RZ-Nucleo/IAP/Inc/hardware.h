//*** <<< Use Configuration Wizard in Context Menu >>> ***
#ifndef __HARDWARE_H__
#define __HARDWARE_H__

#include "main.h"
#include "stm32xxx.h"
#include "ff_gen_drv.h"
#include "flash_diskio.h"


#define USE_SERIAL_DEBUG	1

#define USE_SERIAL_DEBUG_ENABLE	1

typedef enum { COM1 = 0}COM_TypeDef;

/* Definition for COM port1, connected to USART1 */ 
#define COMn                            1

#define BSP_COM1                       USART2
#define BSP_COM1_CLK_ENABLE()          __HAL_RCC_USART2_CLK_ENABLE()
#define BSP_COM1_CLK_DISABLE()         __HAL_RCC_USART2_CLK_DISABLE()

#define BSP_COM1_FORCE_RESET()         __HAL_RCC_USART2_FORCE_RESET()
#define BSP_COM1_RELEASE_RESET()       __HAL_RCC_USART2_RELEASE_RESET()
  
#define BSP_COM1_TX_PIN                GPIO_PIN_2
#define BSP_COM1_TX_GPIO_PORT          GPIOA
#define BSP_COM1_TX_GPIO_CLK_ENABLE()  __HAL_RCC_GPIOA_CLK_ENABLE()
#define BSP_COM1_TX_GPIO_CLK_DISABLE() __HAL_RCC_GPIOA_CLK_DISABLE()
#define BSP_COM1_TX_AF                 GPIO_AF4_USART2

#define BSP_COM1_RX_PIN                GPIO_PIN_3
#define BSP_COM1_RX_GPIO_PORT          GPIOA
#define BSP_COM1_RX_GPIO_CLK_ENABLE()  __HAL_RCC_GPIOA_CLK_ENABLE()
#define BSP_COM1_RX_GPIO_CLK_DISABLE() __HAL_RCC_GPIOA_CLK_DISABLE()
#define BSP_COM1_RX_AF                 GPIO_AF4_USART2

#define BSP_COM1_IRQn                  USART2_IRQn


#define COMx_CLK_ENABLE(__COM__)        (((__COM__) == COM1) ? BSP_COM1_CLK_ENABLE() : 0)
#define COMx_CLK_DISABLE(__COM__)       (((__COM__) == COM1) ? BSP_COM1_CLK_DISABLE() : 0)

#define COMx_TX_GPIO_CLK_ENABLE(__COM__) do { if(__COM__ == COM1) BSP_COM1_TX_GPIO_CLK_ENABLE(); } while (0)
#define COMx_TX_GPIO_CLK_DISABLE(__COM__) (((__COM__) == COM1) ? BSP_COM1_TX_GPIO_CLK_DISABLE() : 0)

#define COMx_RX_GPIO_CLK_ENABLE(__COM__) do { if(__COM__ == COM1) BSP_COM1_RX_GPIO_CLK_ENABLE(); } while (0)
#define COMx_RX_GPIO_CLK_DISABLE(__COM__) (((__COM__) == COM1) ? BSP_COM1_RX_GPIO_CLK_DISABLE() : 0)

/* Exported types ------------------------------------------------------------*/
/* Exported constants --------------------------------------------------------*/
/* Exported macro ------------------------------------------------------------*/
/* Exported functions ------------------------------------------------------- */
void              BSP_COM_Init(COM_TypeDef COM, UART_HandleTypeDef* huart);


	/* Exported types ------------------------------------------------------------*/
	/* Exported constants --------------------------------------------------------*/
	/* User can use this section to tailor USARTx/UARTx instance used and associated 
   resources */
	/* Definition for USARTx clock resources */
	#define DEBUG_USART                      USART2
	#define DEBUG_USART_CLK_ENABLE()         __HAL_RCC_USART2_CLK_ENABLE()
	#define DEBUG_RX_GPIO_CLK_ENABLE()      __HAL_RCC_GPIOA_CLK_ENABLE()
	#define DEBUG_TX_GPIO_CLK_ENABLE()      __HAL_RCC_GPIOA_CLK_ENABLE()

	#define DEBUG_USART_FORCE_RESET()         __HAL_RCC_USART2_FORCE_RESET()
	#define DEBUG_USART_RELEASE_RESET()       __HAL_RCC_USART2_RELEASE_RESET()

	/* Definition for USARTx Pins */
	#define DEBUG_USART_TX_PIN                    GPIO_PIN_2
	#define DEBUG_USART_TX_GPIO_PORT              GPIOA
	#define DEBUG_USART_TX_AF                     GPIO_AF4_USART2
	#define DEBUG_USART_RX_PIN                    GPIO_PIN_3
	#define DEBUG_USART_RX_GPIO_PORT              GPIOA
	#define DEBUG_USART_RX_AF                     GPIO_AF4_USART2

	/* Definition for USARTx's NVIC */
	#define DEBUG_USART_IRQn                 USART2_IRQn
	#define DEBUG_USART_IRQHandler           USART2_IRQHandler

/* Size of Trasmission buffer */
#define TXBUFFERSIZE                      (COUNTOF(aTxBuffer) - 1)
/* Size of Reception buffer */
#define RXBUFFERSIZE                      1//TXBUFFERSIZE
  
/* Exported macro ------------------------------------------------------------*/
#define COUNTOF(__BUFFER__)   (sizeof(__BUFFER__) / sizeof(*(__BUFFER__)))
/* Exported functions ------------------------------------------------------- */

/*
 *	GPS
 */
	#define GPS_PWR_ON()      gpsPowerOn() 
	#define GPS_PWR_OFF()			gpsPowerOff()
		
	#define GPS_USART                      USART5
	#define GPS_USART_CLK_ENABLE()         __HAL_RCC_USART5_CLK_ENABLE()
	#define GPS_RX_GPIO_CLK_ENABLE()       __HAL_RCC_GPIOB_CLK_ENABLE()
	#define GPS_TX_GPIO_CLK_ENABLE()       __HAL_RCC_GPIOB_CLK_ENABLE()

	#define GPS_USART_FORCE_RESET()         __HAL_RCC_USART5_FORCE_RESET()
	#define GPS_USART_RELEASE_RESET()       __HAL_RCC_USART5_RELEASE_RESET()

	/* Definition for USARTx Pins */
	#define GPS_USART_TX_PIN                    GPIO_PIN_3
	#define GPS_USART_TX_GPIO_PORT              GPIOB
	#define GPS_USART_TX_AF                     GPIO_AF6_USART5
	#define GPS_USART_RX_PIN                    GPIO_PIN_4
	#define GPS_USART_RX_GPIO_PORT              GPIOB
	#define GPS_USART_RX_AF                     GPIO_AF6_USART5

	/* Definition for USARTx's NVIC */
	#define GPS_USART_IRQn                 USART4_5_IRQn
		
/*
 *	GSM
 */
	#define GSM_PWR_ON()			gsmPowerOn()           
	#define GSM_PWR_OFF()			gsmPowerOff()
	
	#define GSM_USART                      USART4
	#define GSM_USART_CLK_ENABLE()         __HAL_RCC_USART4_CLK_ENABLE()
	#define GSM_RX_GPIO_CLK_ENABLE()       __HAL_RCC_GPIOA_CLK_ENABLE()
	#define GSM_TX_GPIO_CLK_ENABLE()       __HAL_RCC_GPIOA_CLK_ENABLE()

	#define GSM_USART_FORCE_RESET()         __HAL_RCC_USART4_FORCE_RESET()
	#define GSM_USART_RELEASE_RESET()       __HAL_RCC_USART4_RELEASE_RESET()

	/* Definition for USARTx Pins */
	#define GSM_USART_TX_PIN                    GPIO_PIN_0
	#define GSM_USART_TX_GPIO_PORT              GPIOA
	#define GSM_USART_TX_AF                     GPIO_AF6_USART4
	#define GSM_USART_RX_PIN                    GPIO_PIN_1
	#define GSM_USART_RX_GPIO_PORT              GPIOA
	#define GSM_USART_RX_AF                     GPIO_AF6_USART4

	/* Definition for USARTx's NVIC */
	#define GSM_USART_IRQn                 USART4_5_IRQn
	#define GPS_GSM_USART_IRQHandler           USART4_5_IRQHandler
	
	//LPUART_USART
	#define LPUART_USART                      LPUART1
	#define LP_USART_CLK_ENABLE()         		__HAL_RCC_LPUART1_CLK_ENABLE()
	#define LP_RX_GPIO_CLK_ENABLE()       		__HAL_RCC_GPIOB_CLK_ENABLE()
	#define LP_TX_GPIO_CLK_ENABLE()       		__HAL_RCC_GPIOB_CLK_ENABLE()

	#define LP_USART_FORCE_RESET()         		__HAL_RCC_LPUART1_FORCE_RESET()
	#define LP_USART_RELEASE_RESET()       		__HAL_RCC_LPUART1_RELEASE_RESET()

	/* Definition for USARTx Pins */
	#define LP_USART_TX_PIN                    GPIO_PIN_10
	#define LP_USART_TX_GPIO_PORT              GPIOB
	#define LP_USART_TX_AF                     GPIO_AF4_LPUART1
	#define LP_USART_RX_PIN                    GPIO_PIN_11
	#define LP_USART_RX_GPIO_PORT              GPIOB
	#define LP_USART_RX_AF                     GPIO_AF4_LPUART1
	
	
	/* Definition for SPIx clock resources */
	#define SPIx                             SPI2
	#define SPIx_CLK_ENABLE()                __HAL_RCC_SPI2_CLK_ENABLE()
	#define SPIx_SCK_GPIO_CLK_ENABLE()       __HAL_RCC_GPIOB_CLK_ENABLE()
	#define SPIx_MISO_GPIO_CLK_ENABLE()      __HAL_RCC_GPIOB_CLK_ENABLE()
	#define SPIx_MOSI_GPIO_CLK_ENABLE()      __HAL_RCC_GPIOB_CLK_ENABLE()

	#define SPIx_FORCE_RESET()               __HAL_RCC_SPI2_FORCE_RESET()
	#define SPIx_RELEASE_RESET()             __HAL_RCC_SPI2_RELEASE_RESET()

	/* Definition for SPIx Pins */
	#define SPIx_SCK_PIN                     GPIO_PIN_13
	#define SPIx_SCK_GPIO_PORT               GPIOB
	#define SPIx_SCK_AF                      GPIO_AF0_SPI2
	#define SPIx_MISO_PIN                    GPIO_PIN_14
	#define SPIx_MISO_GPIO_PORT              GPIOB
	#define SPIx_MISO_AF                     GPIO_AF0_SPI2
	#define SPIx_MOSI_PIN                    GPIO_PIN_15
	#define SPIx_MOSI_GPIO_PORT              GPIOB
	#define SPIx_MOSI_AF                     GPIO_AF0_SPI2
	
	/* Definition for SPIx's NVIC */
	#define SPIx_IRQn                        SPI2_IRQn
	#define SPIx_IRQHandler                  SPI2_IRQHandler

enum {
	E_OK				= 0,
	E_ERROR				= 1,
	E_TIMEOUT			= 2,
	E_SEND_ERROR		= 3,
	E_BAD_RESPONSE		= 9,
	E_GSM_NOT_READY		= 10,
	E_SIM_NOT_READY		= 11,
	E_NO_SIM			= 12,
	E_REG_DENIED		= 13,
	E_NOT_REGISTER		= 14,
	E_GSM_TCP_DISABLE	= 15,
	E_GSM_TCP_NOT_OPEN	= 16,
	E_BAD_COMMAND		= 17,
	E_NO_RESPONSE		= 18,
	E_FW_ERROR			= 20,
	
	E_FW_OK			= 21,

	E_TASK_DONE			= 252,
	E_TASK_ERROR		= 253,
	E_TASK_QUEUED		= 254,
	E_TASK_SENDED	 	= 255
};

enum {
	TRANSFER_WAIT,
	TRANSFER_COMPLETE,
	TRANSFER_ERROR
};

extern UART_HandleTypeDef gsmUartHandle;
extern UART_HandleTypeDef gpsUartHandle;
extern UART_HandleTypeDef UartHandle;
extern UART_HandleTypeDef lpUartHandle;
extern CRC_HandleTypeDef CrcHandle;
static GPIO_InitTypeDef  GPIO_InitStruct;

void Error_Handler(void);
#endif
