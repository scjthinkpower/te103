#include <string.h>
#include <stdint.h>
#include "gsensor.h"

#include "main.h"
#include "hardware.h"
#include "debug.h"
#include "utility.h"
#include "eeprom.h"
#include "config_file.h"
  
#include "FreeRTOS.h"
#include "task.h"
#include "semphr.h"
#include "gps.h"
#include "gsm.h"
#include "work.h"
#include "acc.h"
#include "can.h"
#include "gpsOne.h"
#include "nmea/tok.h"
#include "spi_flash.h"
#include "task.h"


#define BIT(x) ( 1<<(x) )

#define CTRL_REG1             0x20
#define CTRL_REG2             0x21
#define CTRL_REG3							0x22
#define CTRL_REG4							0x23
#define CTRL_REG5							0x24
#define CTRL_REG6							0x25
#define REFERENCE							0x26
#define INT1_CFG							0x30
#define INT1_SRC							0x31
#define INT1_THS              0x32
#define INT1_DURATION         0x33

#define HIGH_RESOLUTION 1
#define I2C_ERROR 101
#define I2C_OK 0

#define MOVE    0.05
#define STATIC  0.01

typedef enum
{
  X = 0,
  Y,
	Z,
} Gsensor_ValueDef;

/* I2C handler declaration */
I2C_HandleTypeDef I2cHandle;

float g_target = 0.4;
float gx_cur = 0;
float gy_cur = 0;
float gz_cur = 0;

gsensor_detect_t G_gsensor_detect;
g_sensor_info_t g_sensor_info_main;
u16 level_shift = 0;
u16 mark_bit = 0;

bool gsensorok = false;
bool move = false;


bool notifyShock(void)
{
  //shock = true;
	return true;
}

void setMoveState(bool isMove)
{
		move = isMove;
}

bool getMoveState(void)
{
		return move;
}

void Max3(float *dtmax, float a, float b, float c)
{
	float temp = 0.0;
	if (temp<a)
		temp = a;
	if (temp<b)
		temp = b;
	if(temp<c)
		temp = c;
	*dtmax =  temp;
}


void fabs_g(float *a, float b)
{
	*a = (((b) < 0) ? -(b) : (b));
}


void g_sensor_get_info(g_sensor_info_t *g_sensorinfo)
{
  //G_g_sensor.g_sensor_get_info(g_sensorinfo);

	static u8 flg_once = 0;

	if (!flg_once) {
	 	flg_once = 1;
	  memset(&g_sensor_info_main, 0, sizeof(g_sensor_info_t));

#if 1//(CONFIG_IS_ON)
		//g_sensor_info_main.g_sensor_id = G_ST_LIS3DE_ID;
		g_sensor_info_main.g_sensor_id = 1;

#else
		g_sensor_info_main.g_sensor_id = NULL_IMAGE_STABILIZER;
#endif
    //g_sensor_info_main.sensor_axis		= G_SENSOR_3_AXIS;
		g_sensor_info_main.sensor_axis		= 1;

    #ifdef HIGH_RESOLUTION
		g_sensor_info_main.max_sense 		= 16383;			// LSB/g
    g_sensor_info_main.max_bias 		= 65535;//+40;		// LSB		
    g_sensor_info_main.adc_resolution           = 16;			// bits
    #else
		g_sensor_info_main.max_sense 		= 63;			// LSB/g
    g_sensor_info_main.max_bias 		= 255;//+40;		// LSB			
    g_sensor_info_main.adc_resolution           = 8;			// bits
		#endif
		g_sensor_info_main.sampling_cycle      = 100;
        	
	  level_shift = 1 << (g_sensor_info_main.adc_resolution - 1);
		mark_bit = (1 << (g_sensor_info_main.adc_resolution))-1;
	}
	memcpy(g_sensorinfo, &g_sensor_info_main, sizeof(g_sensor_info_t));
} /* void get_G_SENSOR_info(g_sensor_info_t *g_sensorinfo) */
  



u16 g_sensor_read(uint16_t xyz){
	uint16_t data;
	data = readGsenserXYZ(xyz);
	return data;
}


void Check_even(float x, float y, float z)
{
	static float g_data[5][3] = {0.0};
	static int gc =0;
	int i = 0, j = 0;
	static float avg[3] = {0.0};
	static float dtmax, dtx, dty, dtz;

	if (gc<5){
		g_data[gc][0] = x;
		g_data[gc][1] = y;
		g_data[gc][2] = z;
		gc++;
		avg[0] = (g_data[0][0] + g_data[1][0] + g_data[2][0] + g_data[3][0] + g_data[4][0])/gc;
		avg[1] = (g_data[0][1] + g_data[1][1] + g_data[2][1] + g_data[3][1] + g_data[4][1])/gc;
		avg[2] = (g_data[0][2] + g_data[1][2] + g_data[2][2] + g_data[3][2] + g_data[4][2])/gc;
		return;
	}
	else{
		
		fabs_g(&dtx, (x-avg[0]));
		fabs_g(&dty, (y-avg[1]));
		fabs_g(&dtz, (z-avg[2]));
		Max3(&dtmax, dtx, dty, dtz);
		
		if(dtmax >= MOVE)
			setMoveState(true);
		else if(dtmax <= STATIC)
			setMoveState(false);
		
		//if(DebugEnable) DEBUG("dtmax %.2f",dtmax);
		if (dtmax > g_target){
			//if(DebugEnable) DEBUG("#GSENSOR_ACTIVITY_EVENT#");
			setEvent(GSENSOR_ACTIVITY_EVENT);
			gc = 0;
			return;
		}

		for(i=0; i<4; i++){
			for(j=0; j<3; j++){
			g_data[i][j] = g_data[i+1][j];
			}
		}

		g_data[4][0] = x;
		g_data[4][1] = y;
		g_data[4][2] = z;

		avg[0] = (g_data[0][0] + g_data[1][0] + g_data[2][0] + g_data[3][0] + g_data[4][0])/5.0;
		avg[1] = (g_data[0][1] + g_data[1][1] + g_data[2][1] + g_data[3][1] + g_data[4][1])/5.0;
		avg[2] = (g_data[0][2] + g_data[1][2] + g_data[2][2] + g_data[3][2] + g_data[4][2])/5.0;
	}

}

void calc_gvalue(u16 g, float *fg)
{
	float temp = 0.00;
	float temp_g = (float) (g);
	static g_sensor_info_t g_sensorinfo = {0};
	
	g_sensor_get_info(&g_sensorinfo); //Get G_Sensor information
	
	if(g<(1 << (g_sensorinfo.adc_resolution - 1)))
	{
		temp_g=(float)(1 << (g_sensorinfo.adc_resolution - 1))-temp_g;
		temp = (float)(temp_g/(1 << (g_sensorinfo.adc_resolution - 1)));

	}else{
		temp_g=temp_g-(float)(1 << (g_sensorinfo.adc_resolution - 1));
		//temp_g=g_sensorinfo.max_bias-temp_g;
		temp = (float)(temp_g/(1 << (g_sensorinfo.adc_resolution - 1)));

	}
	*fg = temp;
}



uint8_t gsensor_detect(void)
{
  u16 xVal, yVal, zVal;
	float gx, gy, gz;
	//SYSTIM tm1, tm2;
	g_sensor_info_t g_sensorinfo = {0};
	g_sensor_get_info(&g_sensorinfo); //Get G_Sensor information

  xVal = yVal = zVal = 0;

	G_gsensor_detect.gsensor_enable = true;
	
	if(G_gsensor_detect.gsensor_enable) {

		xVal = g_sensor_read(X);
		if(xVal == I2C_ERROR)
			return I2C_ERROR;
		yVal = g_sensor_read(Y);
		if(yVal == I2C_ERROR)
			return I2C_ERROR;
		zVal = g_sensor_read(Z);
		if(zVal == I2C_ERROR)
			return I2C_ERROR;
		
		calc_gvalue(xVal, &gx);
		calc_gvalue(yVal, &gy);
		calc_gvalue(zVal, &gz);
		
		gx_cur = gx;
		gy_cur = gy;
		gz_cur = gz;
		
		Check_even(gx, gy, gz);
	
    } 
	return 0;
}




void gsensor_detect_start(void)
{
    G_gsensor_detect.gsensor_enable = 1;
}

void gsensor_detect_stop(void)
{
    G_gsensor_detect.gsensor_enable = 0;
}

void gsensor_set_event_sensitivity(u8 sens)
{
	if(sens == 1)
		g_target = 0.3;//0.6g
	else if(sens == 2)
		g_target = 0.4;//0.8g
	else if(sens == 3)
		g_target = 0.5;//1.0g
	else if(sens == 4)
		g_target = 0.6;//1.2g
	else
		g_target = 0.4;

}

void sensitivity(void)
{
	if(!strcmp(get_conf_elem(GSENSOR_SENSITIVITY_CONF),"2"))
		gsensor_set_event_sensitivity(1);
	else if(!strcmp(get_conf_elem(GSENSOR_SENSITIVITY_CONF),"1"))
		gsensor_set_event_sensitivity(2);
	else if(!strcmp(get_conf_elem(GSENSOR_SENSITIVITY_CONF),"0"))
		gsensor_set_event_sensitivity(3);
	else if(!strcmp(get_conf_elem(GSENSOR_SENSITIVITY_CONF),"3"))
		gsensor_set_event_sensitivity(4);
	else
		gsensor_set_event_sensitivity(1);

}

void gsensor_cur_gvalue(float *gx,float *gy,float *gz)
{
	*gx = gx_cur;
	*gy = gy_cur;
	*gz = gz_cur;

}


uint8_t configReg(){
	
	if(writeReg(CTRL_REG1,0x3f)==I2C_ERROR)
			return I2C_ERROR;
	
	if(writeReg(CTRL_REG2,0x09)==I2C_ERROR)
		return I2C_ERROR;

	if(writeReg(CTRL_REG3,0x40)== I2C_ERROR)
		return I2C_ERROR;
	
	if(writeReg(CTRL_REG4,0x00)==I2C_ERROR)
		return I2C_ERROR;
	
	if(writeReg(CTRL_REG5,0x00)==I2C_ERROR)
		return I2C_ERROR;
	
	if(writeReg(CTRL_REG6,0x02)==I2C_ERROR)
		return I2C_ERROR;
	
	if(writeReg(INT1_THS,0x10)==I2C_ERROR)
		return I2C_ERROR;

	if(writeReg(INT1_DURATION,0x00)==I2C_ERROR)
		return I2C_ERROR;
	
  //if(readReg(REFERENCE)==I2C_ERROR)
		//return I2C_ERROR;
	
	if(writeReg(INT1_CFG,0x2A)==I2C_ERROR)
		return I2C_ERROR;

	return I2C_OK;
}



uint8_t initGsensor(void){
		
	gsensor_INT();

 /*##-1- Configure the I2C peripheral ######################################*/
  I2cHandle.Instance             = I2Cx;
  I2cHandle.Init.Timing          = I2C_TIMING;
  I2cHandle.Init.OwnAddress1     = 0x3E;
	//I2cHandle.Init.OwnAddress1     = 0x19;
  I2cHandle.Init.AddressingMode  = I2C_ADDRESSINGMODE_7BIT;
  I2cHandle.Init.DualAddressMode = I2C_DUALADDRESS_DISABLE;
  I2cHandle.Init.OwnAddress2Masks = I2C_OA2_NOMASK;
  I2cHandle.Init.OwnAddress2     = 0xFF;
  I2cHandle.Init.GeneralCallMode = I2C_GENERALCALL_DISABLE;
  I2cHandle.Init.NoStretchMode   = I2C_NOSTRETCH_DISABLE;  
  
	
	if(HAL_I2C_DeInit(&I2cHandle) != HAL_OK)
  {
    /* Initialization Error */
    if(DebugEnable) DEBUG("I2C_DeInit error");
		return I2C_ERROR;
  }
	
  if(HAL_I2C_Init(&I2cHandle) != HAL_OK)
  {
    /* Initialization Error */
    if(DebugEnable) DEBUG("I2C_Init error");
		return I2C_ERROR;
  }
  
  /* Enable the Analog I2C Filter */
  HAL_I2CEx_ConfigAnalogFilter(&I2cHandle,I2C_ANALOGFILTER_ENABLE);
#if 0
	if(DebugEnable) DEBUG("config gsensor");
	writeReg(0x20,0x07);
	if(DebugEnable) DEBUG("config 20");
	writeReg(0x24,0x00);
	if(DebugEnable) DEBUG("config 24");
	writeReg(0x2E,0x00);
	if(DebugEnable) DEBUG("config 2e");
	writeReg(0x22,0x00);
	if(DebugEnable) DEBUG("config 22");
	//writeReg(0x30,0x00);
	if(DebugEnable) DEBUG("config gsensor success");
#else
	
	if (configReg() != I2C_ERROR) {
	    if (DebugEnable) DEBUG("config gsensor success") ;
	} else {
	    if (DebugEnable) DEBUG("config gsensor failure") ;
	}

	return 1;

#endif
}

uint8_t writeReg(u8 reg,u8 value)
{
		uint8_t config[2];
		config[0] = reg;
		config[1] = value;
	  
    while(HAL_I2C_Master_Transmit_IT(&I2cHandle, (uint16_t)I2C_ADDRESS_WRITE, config, 2)!= HAL_OK)
    {
      if (HAL_I2C_GetError(&I2cHandle) != HAL_I2C_ERROR_AF)
      {
        	if(DebugEnable) DEBUG("writeReg error");
					return I2C_ERROR;
      }
    }

    while (HAL_I2C_GetState(&I2cHandle) != HAL_I2C_STATE_READY)
    {
    }
		
   return 0;
}


uint8_t readReg(uint8_t address){
	
		uint8_t data;
		while(HAL_I2C_Master_Transmit_IT(&I2cHandle, (uint16_t)I2C_ADDRESS_WRITE, (uint8_t*)&address, 1)!= HAL_OK)
    {
      if (HAL_I2C_GetError(&I2cHandle) != HAL_I2C_ERROR_AF)
      {
        	if(DebugEnable) DEBUG("readReg write error");
					return I2C_ERROR;
      }
    }
		
		 while(HAL_I2C_Master_Receive_IT(&I2cHandle, (uint16_t)I2C_ADDRESS_READ, (uint8_t*)&data, 1)!= HAL_OK)
    {
      if (HAL_I2C_GetError(&I2cHandle) != HAL_I2C_ERROR_AF)
      {
        if(DebugEnable) DEBUG("readReg read error");
				return I2C_ERROR;
      }
    }
		//if(DebugEnable) DEBUG("%x",data);
		return data;

}


uint16_t readGsenserXYZ(uint16_t xyz){
	
	uint16_t value,lRx=0,hRx=0;
	
	uint8_t lRequest,hRequest;
	
	  if(xyz == X){
			lRequest = REQ_X_L;
			hRequest = REQ_X_H;
		}else if(xyz == Y){
			lRequest = REQ_Y_L;
			hRequest = REQ_Y_H;
		}else{
			lRequest = REQ_Z_L;
			hRequest = REQ_Z_H;
		}
		
		while(HAL_I2C_Master_Transmit_IT(&I2cHandle, (uint16_t)I2C_ADDRESS_WRITE, (uint8_t*)&hRequest, 1)!= HAL_OK)
    {
      if (HAL_I2C_GetError(&I2cHandle) != HAL_I2C_ERROR_AF)
      {
        if(DebugEnable) DEBUG("write h error");
				return I2C_ERROR;
      }
    }
    while (HAL_I2C_GetState(&I2cHandle) != HAL_I2C_STATE_READY)
    {
    }
		
		 while(HAL_I2C_Master_Receive_IT(&I2cHandle, (uint16_t)I2C_ADDRESS_READ, (uint8_t*)&hRx, 1)!= HAL_OK)
    {
			if(DebugEnable) DEBUG("read h error %d",HAL_I2C_GetError(&I2cHandle));
			
      if (HAL_I2C_GetError(&I2cHandle) != HAL_I2C_ERROR_AF)
      {
        if(DebugEnable) DEBUG("read h error");
				return I2C_ERROR;
      }
    }
    while (HAL_I2C_GetState(&I2cHandle) != HAL_I2C_STATE_READY)
    {
    }
		
    while(HAL_I2C_Master_Transmit_IT(&I2cHandle, (uint16_t)I2C_ADDRESS_WRITE, (uint8_t*)&lRequest, 1)!= HAL_OK)
    {
      if (HAL_I2C_GetError(&I2cHandle) != HAL_I2C_ERROR_AF)
      {
        if(DebugEnable) DEBUG("write l error");
				return I2C_ERROR;
      }
    }
    while (HAL_I2C_GetState(&I2cHandle) != HAL_I2C_STATE_READY)
    {
    }
		
		 while(HAL_I2C_Master_Receive_IT(&I2cHandle, (uint16_t)I2C_ADDRESS_READ, (uint8_t*)&lRx, 1)!= HAL_OK)
    {
			if(DebugEnable) DEBUG("read l error %d",HAL_I2C_GetError(&I2cHandle));
			
      if (HAL_I2C_GetError(&I2cHandle) != HAL_I2C_ERROR_AF)
      {
         if(DebugEnable) DEBUG("read l error");
					return I2C_ERROR;
      }
    }
		
    while (HAL_I2C_GetState(&I2cHandle) != HAL_I2C_STATE_READY)
    {
    }
		
		//if(DebugEnable) DEBUG("lRx:%x",lRx);
		//if(DebugEnable) DEBUG("hRx:%x",hRx);
		value= (uint16_t) ((uint16_t)(hRx << 8 ))|((uint16_t)lRx);
		//if(DebugEnable) DEBUG("value:%x",value);
		return value;
		
}


void vGsensor(void const *pvArg)
{
	//initGsensor();
	
	int8_t timeout = 0;
	
	for(;;)
	{
		//if(shock){
		//shock = false;
		//if(DebugEnable) DEBUG("%.2f",dtmax);
		sensitivity();
		if(gsensor_detect() == 0)
				gsensorok = true;
		//}
		timeout++;
		if(timeout == 60)
		{
		timeout = 0;
		testGpsRun(false);
		}else if(timeout == 30)
		{
		if(!getGpsRun())
			SystemRestart();
		}
		osDelay(500);
	}
	
}
	
void gsenssorDebugEnable(bool enable)
{
	DebugEnable = enable;
}




