#include "common.h"


/**
* brief : init the timer in message timeout task 
* params :
*     t : the timer structure pointer
*     units : the units of timer tick 
*     cb : timer callback function
* return :
*     none
**/
void init_timer(common_timer_t *t, int units, timer_callback cb) {
    t->is_running = false ; 
    t->elapse = 0 ; 
    t->unit = units ; 
    t->value = 0 ; 
    t->cb = cb  ; 
}

/**
* brief : start the timer counter 
* params :
*      t : the timer structure pointer
*     counter : the counts of the timer
* return :
*      none
**/
void start_timer(common_timer_t *t, int counter) {
    t->value = counter ; 
    t->elapse = 0 ;
    t->is_running = true ; 
}


/**
* brief : stop the timer 
* params :
*       t : the timer structure pointer
* return :
*      none
**/
void stop_timer(common_timer_t *t) {
    t->is_running = false  ; 
    t->elapse = 0 ; 
    t->value = 0 ; 
}



/**
* brief : handle the timer function , this should be put into the timeout partment
*           of the message+wait task 
* params :
*       t : the timer structure pointer
*       type : 1 indicate a continuously timer , 0 indicate a one shot timer
* return :
*       none
**/
void handle_timer(common_timer_t *t, common_timer_type_e type) {
    if (t->is_running) {
        t->elapse += t->unit ; 
        if (t->elapse > t->value) {
            //t->cb();      
            if (type == ONE_SHORT_TIMER) {
                t->is_running = false ; 
            } else {
                t->elapse = 0 ; 
            }    
	     t->cb(); 
        }
    }
  

}

