
#include "geofence.h"
#include "config_file.h"
#include "work.h"
#include "debug.h"
#include <math.h>
#include "bitmap_flash.h"

#define	SETTINGS_GEOFENCE_CNT	15	//need flash: 40*SETTINGS_GEOFENCE_CNT (byte)

unsigned int GeofenceCnt = 0;
geoStatus_t geoStatus[SETTINGS_GEOFENCE_CNT];	// 3*SETTINGS_GEOFENCE_CNT (byte)

uint8_t geofence_alarm[10];

/*从flash中读取一个geofence数据*/
static geofence_t getGeofenceData(unsigned int index)
{
	geofence_t geo;

	//MCUFlash_ReadGeo(index);
	//if(readGeofencedata(&geo,index)==-1)
	bit_map_read_data_index((char *)&geo,sizeof(geo),index);
	//geo.Name[0]=0;
	return geo;
}

/*保存一个geofence结构到flash*/
int addGeofenceData(geofence_t* geo_data)
{
	unsigned int geo_index = 0;
	geofence_t geo;

	if((geo_data->Name[0] == 0)){
		return -1;
	}
	
	if((geo_data->Type == '0') && (geo_data->Radius <= 0.02))
		return -1;

	for(geo_index = 0; geo_index < SETTINGS_GEOFENCE_CNT; geo_index++)
	{
		geo = getGeofenceData(geo_index);
		if((geo.Name[0] <= 0) || (geo.Name[0] > 127)){
			continue;
		}else{
			if(strcmp(geo.Name, geo_data->Name) == 0){
			strcpy(geo.Name, geo_data->Name);
			geo.Point.lat = geo_data->Point.lat;
			geo.Point.lng = geo_data->Point.lng;
			geo.Point.lat1 = geo_data->Point.lat1;
			geo.Point.lng1 = geo_data->Point.lng1;
			geo.Radius = geo_data->Radius;
			geo.AlarmType = geo_data->AlarmType;
			geo.Type = geo_data->Type;
			geo.Zone = geo_data->Zone;
			// MCUFlash_WriteGeo(&geo, geo_index);
			//writegeofendata(&geo,geo_index);
			bit_map_write_data_index((char *)&geo,sizeof(geo),geo_index);
			return 0;
			}
	}
	}
	
	for(geo_index = 0; geo_index < SETTINGS_GEOFENCE_CNT; geo_index++)
	{
		geo = getGeofenceData(geo_index);
		//if((geo.Name[0]==0x00) || (geo.Name[0]==0xff)){
		if((geo.Name[0] <= 0) || (geo.Name[0] > 127)){
			strcpy(geo.Name, geo_data->Name);
			geo.Point.lat = geo_data->Point.lat;
			geo.Point.lng = geo_data->Point.lng;
			geo.Point.lat1 = geo_data->Point.lat1;
			geo.Point.lng1 = geo_data->Point.lng1;
			geo.Radius = geo_data->Radius;
			geo.AlarmType = geo_data->AlarmType;
			geo.Type = geo_data->Type;
			geo.Zone = geo_data->Zone;
			// MCUFlash_WriteGeo(&geo, geo_index);
			//writegeofendata(&geo,geo_index);
			bit_map_write_data_index((char *)&geo,sizeof(geo),geo_index);
			return 0;
		}else{
			continue;
		}
	}
	return -1;
}

/*从flash中删除一个geofence结构*/
int deleteGeofenceData(geofence_t* geo_data)
{
	unsigned int geo_index = 0;
	geofence_t geo;
	
	if((geo_data->Name[0] == 0)){
		return -1;
	}

	for(geo_index = 0; geo_index < SETTINGS_GEOFENCE_CNT; geo_index++)
	{
		geo = getGeofenceData(geo_index);
		//if((geo.Name[0]==0x00) || (geo.Name[0]==0xff)){
		if((geo.Name[0] <= 0) || (geo.Name[0] > 127)){
			continue;
		}else{
			if(strcmp(geo.Name, geo_data->Name) == 0){
				memset(geo.Name, 0, sizeof(geo.Name));
				// MCUFlash_WriteGeo(&geo, geo_index);
				//writegeofendata(&geo,geo_index);
				bit_map_write_data_index((char *)&geo,sizeof(geo),geo_index);
				return 0;
			}else{
				continue;
			}
		}
	}
	return -1;
}

void  geofence_init (void) 
{
	unsigned int geo_index = 0;
	 
	for(geo_index = 0; geo_index < SETTINGS_GEOFENCE_CNT; geo_index++)
	{
		geoStatus[geo_index].status = GEO_STATUS_OUT; //默认在外
		geoStatus[geo_index].inCnt = 0;
		geoStatus[geo_index].outCnt = 0;
	}
}

// return: distance of two points uinit in meter.
static double nmea_dist(const point_t *point1, const point_t *point2)
{
    double a = (point1->lat - point2->lat)*__PI/180.0;
    double b = (point1->lng - point2->lng)*__PI/180.0;
    double s = 2*asin(sqrt(sin(a/2)*sin(a/2) + cos(point1->lat*__PI/180.0)*cos(point2->lat*__PI/180.0)*sin(b/2)*sin(b/2)));
    s = s * EARTH_RADIUS;
    return s;
}


//检测点在栅栏内还是外，返回结果
static unsigned char georoundcheck(geofence_t * pgeo, point_t * pointcur_t) 
{
    double dst = 0.0;
	
	if(pgeo->Type == '0'){
	
    if (isnan(pgeo->Point.lat) || 	\
    	isnan(pgeo->Point.lng) || 	\
    	isnan(pgeo->Radius)|| 		\
    	pgeo->Radius <= 0.020) 
    {
    	return GEO_STATUS_UNKNOW;
    }
    if (pgeo->Point.lat == 0.0 || \
    	pgeo->Point.lng == 0.0 || \
        pgeo->Radius == 0.0   )
    {
        return GEO_STATUS_UNKNOW;
    }
#ifdef UNIT_MILE
    dst =  nmea_dist(&pgeo->Point, pointcur_t)/1000/ 1.6;   
#else
    dst =  nmea_dist(&pgeo->Point, pointcur_t)/1000;		//单位是 KM
#endif
    if (dst < pgeo->Radius){
        return GEO_STATUS_IN;
    } else {
        return GEO_STATUS_OUT;
    }
	}else
	{
			if((pgeo->Point.lat <= pointcur_t->lat <= pgeo->Point.lat1) 
				&& (pgeo->Point.lng <= pointcur_t->lng <= pgeo->Point.lng1 ))
				return GEO_STATUS_IN;
			else
				return GEO_STATUS_OUT;
	}
}


//
void  geocheck_check(point_t * pointcur_t)
{
    unsigned int geo_index = 0;
    unsigned char status = GEO_STATUS_UNKNOW;
		geofence_t p_geo;

	for(geo_index = 0; geo_index < SETTINGS_GEOFENCE_CNT; geo_index++)
	{
		p_geo = getGeofenceData(geo_index);

		//if((p_geo.Name[0]==0x00) || (p_geo.Name[0]==0xff)){
		if((p_geo.Name[0] <= 0) || (p_geo.Name[0] > 127)){
			continue;
		}
		//printf("%s,%c,%c\r\n",p_geo.Name,p_geo.AlarmType,p_geo.Type);
		status = georoundcheck(&p_geo, pointcur_t);
		//printf("%d\r\n",status);
		if(status == GEO_STATUS_UNKNOW){
			geoStatus[geo_index].inCnt  = 0;
			geoStatus[geo_index].outCnt = 0;
			continue;
		}else if(status == GEO_STATUS_IN){		
			geoStatus[geo_index].outCnt = 0;
			//printf("%d %d\r\n",geoStatus[geo_index].inCnt,geoStatus[geo_index].status);
			if(geoStatus[geo_index].inCnt >= 3){
				if(geoStatus[geo_index].status == GEO_STATUS_OUT){
					geoStatus[geo_index].status = GEO_STATUS_IN;
					//DEBUG("Geo In: %s", p_geo.Name);
					/*fixme:need add "geo name" and "geo status" into gsm task cmd queue*/
					if(p_geo.AlarmType == '0' || p_geo.AlarmType == '2'){
					geofence_alarm_info(&p_geo,"0",0);
					setEvent(GEOFENCE_EVENT);
					}
					//addevent(GSM_CMD_GEO);	
				}			
			}else{
				geoStatus[geo_index].inCnt++;
			} 
			
			if((pointcur_t->speed > p_geo.Point.speed) && (p_geo.Point.speed > 1.0) && p_geo.AlarmType == '3'){
					geofence_alarm_info(&p_geo,"1",pointcur_t->speed);
					setEvent(GEOFENCE_EVENT);
				}
			
		}else if(status == GEO_STATUS_OUT){
			geoStatus[geo_index].inCnt = 0;
			//printf("%d %d#\r\n",geoStatus[geo_index].outCnt,geoStatus[geo_index].status);
			if(geoStatus[geo_index].outCnt >= 3){
				if (geoStatus[geo_index].status == GEO_STATUS_IN){
					geoStatus[geo_index].status = GEO_STATUS_OUT;
					//DEBUG("Geo Out: %s", p_geo.Name);
					/*fixme:need add "geo name" and "geo status" into gsm task cmd queue*/
					if(p_geo.AlarmType == '1' || p_geo.AlarmType == '2'){
					geofence_alarm_info(&p_geo,"1",0);
					setEvent(GEOFENCE_EVENT);
					}
					//addevent(GSM_CMD_GEO);
				}
			}else{
				geoStatus[geo_index].outCnt++;
			}
		} 		
	}
}



void geofence_alarm_info(geofence_t* geo,uint8_t* geoStatus,uint8_t speedstatus)
{
	memset(geofence_alarm,0,10);
	
	strcat((char *)geofence_alarm,geo->Name);
	
	strcat((char *)geofence_alarm,"|");
	if(speedstatus == 0)
	strcat((char *)geofence_alarm,(char *)geoStatus);
	else
	strcat((char *)geofence_alarm,"2");
}

uint8_t* geofence_state()
{
	return geofence_alarm;
}

