#include <stdio.h>
#include <string.h>
#include <math.h>
#include <stdlib.h>
#include "work.h"
#include "common.h"
#include "hardware.h"
#include "ff_gen_drv.h"
#include "sd_diskio.h"
#include "task.h"
#include "debug.h"
#include "gsm.h"
#include "gps.h"
#include "main.h"
#include "utility.h"
#include "config_file.h"
#include "Ds18b20.h"

#define WORK_TASK_QUEUE_SIZE	4


#define WORK_QUEUE_WAIT_TICKS                         1000 * 2 / portTICK_RATE_MS 

#define STATIC_STATE 0
#define MOTION_STATE 1
#define WAKEUP_START_STATE 2
#define WAKEUP_SUCCESS_STATE 3
#define WAKEUP_FAIL_STATE 4

#define VOLTAGE_0      150
#define VOLTAGE_11_2   735
#define VOLTAGE_11_5   752
#define VOLTAGE_18_0	 1181
#define VOLTAGE_22_0   1440
#define VOLTAGE_22_5   1472

//#define TUB     "*XD"


uint32_t work_state = 0;
int32_t event = -1;
int32_t alarm_time = 0;
int32_t test_time = 0;

int8_t* alarm_status[] ={"SOS=0","SOS=1","BatteryLow=0","BatteryLow=1",
"CutDetect=0","CutDetect=1","OverSpeedWarn=0","OverSpeedWarn=1","ShockWarn=0","ShockWarn=1","GpsState=0","GpsState=1","JammingWarn=0","JammingWarn=1"};

int8_t* alarm_status_ussd[] ={"0","1","2","3","4","5","6","7"};

int8_t* signal_status[] ={"IN1+=0","IN1+=1","IN2+=0","IN2+=1","IN1-=0","IN1-=1","IN2-=0","IN2-=1",
"ACC=ION","ACC=ON","ACC=IOFF","ACC=OFF"};

int8_t* signal_status_ussd[] ={"1","2","3","4"};

int8_t dst[50];
int8_t acc_state[5];
int8_t inp_state[5];
int8_t inn_state[5];
int8_t ADCVolt[20];
int8_t ADCVolt_v[10];
int8_t BattLowADCVolt[20];
int8_t Rs232Val[15]="0";
double acc_simulate_state[2];
double acc_voltage_average = 0;

bool acc_ion = false;
bool acc_ioff = false;
bool acc_on = false;
bool acc_off = false;
bool acc_ioff_send = false;
bool acc_ion_send = false;
bool in_plus = false;
bool in_minus = false;
bool in_plus_send = false;
bool in_minus_send = false;
bool sos_press = false;


bool sos_alarm =false;
bool batterylow_alarm =false;
bool cutdetect_alarm =false;
bool overspeed_alarm =false;
bool shock_alarm =false;
bool gpslost_alarm =false;
bool gpsfix_alarm =false;
bool jamming_alarm =false;
bool no_simcard =false;
bool firstWork = false;
int8_t time_batt_cut = 0;
int8_t time_batt_low = 0;
int32_t time_batt_sleep = 0;
int8_t time_batt_cut_report = 0;
int8_t time_siren = 0;
int8_t siren_status = 0;
int8_t time_simcard_state = 0;
int32_t time_simulate_acc = 0;
int32_t time_lpUart = 0;
bool tubReport = false;

common_timer_t workQueryTimer ; 
xQueueHandle	xWorkTaskQueue;
WorkTask_t	WorkTask;
bool worktestmode = false;
bool gsensortestmode = false;
bool rtcReport = false;

extern bool gsensorok;
extern bool testmode;
extern bool gpstestmode;
extern bool gsmtestmode;
extern bool gsensortestmode;
extern RTC_HandleTypeDef RTCHandle;
extern ADC_HandleTypeDef    AdcHandle;

extern float Temperature;
extern float Humi;
extern int utc_year;

/* ADC channel configuration structure declaration */
ADC_ChannelConfTypeDef        sConfig;

/* Variable used to get converted value */
__IO uint32_t uwADCxConvertedValue = VOLTAGE_11_5;

__IO uint32_t s_uwADCxConvertedValue = VOLTAGE_11_5;

__IO uint32_t new_s_uwADCxConvertedValue = 0;

#define SET_MODE         GPIO_MODE_INPUT  //GPIO_MODE_ANALOG
#define PULL_STATE      GPIO_NOPULL  //GPIO_PULLDOWN
#define FREQ_G             GPIO_SPEED_FREQ_LOW

bool rtc_init = false;
bool wakeup_report = false;

void low_power_pins(void) {
    GPIO_InitTypeDef  GPIO_InitStruct;
	// put gsensor in low power mode
	 // for nucleous board 
   
	/*  gsmPowerOn() ;
	  HAL_Delay(3000) ;
	  gsmPowerOff(); */  

     // enable power down mode 
     // 20161220 remove the powerDown operation for we remove the gsensor on the board 
     // 
/*	if (powerDownGsensor() == 0) {
		if (1) DEBUG("power down gsensor success"); 
	} else {
	       if (1) DEBUG("power down gsensor failure") ; 
	}*/
 	 // VCC 3V3 enable pin  
	GPIO_InitStruct.Pin      	= GPIO_PIN_10;
  GPIO_InitStruct.Mode      =  SET_MODE;
  GPIO_InitStruct.Pull      = PULL_STATE;
  GPIO_InitStruct.Speed     = FREQ_G;
	HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);
	//HAL_GPIO_WritePin(GPIOC, GPIO_PIN_10, GPIO_PIN_SET);
	
	// ADC_IN2
		GPIO_InitStruct.Pin      	= GPIO_PIN_10;
  GPIO_InitStruct.Mode      = SET_MODE;
  GPIO_InitStruct.Pull      = PULL_STATE;
  GPIO_InitStruct.Speed     = FREQ_G;
	HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);
	
	
	 // RESET_N_GSM
			GPIO_InitStruct.Pin      	= GPIO_PIN_2;
  GPIO_InitStruct.Mode      = SET_MODE;
  GPIO_InitStruct.Pull      = PULL_STATE;
  GPIO_InitStruct.Speed     = FREQ_G;
	HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);
	
	 // SDA 
	 		GPIO_InitStruct.Pin      	= GPIO_PIN_10;
  GPIO_InitStruct.Mode      = SET_MODE;
  GPIO_InitStruct.Pull      = PULL_STATE;
  GPIO_InitStruct.Speed     = FREQ_G;
	HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);
	
	// RTS_MCU 
		GPIO_InitStruct.Pin      	= GPIO_PIN_15;
  GPIO_InitStruct.Mode      = SET_MODE;
  GPIO_InitStruct.Pull      = PULL_STATE;
  GPIO_InitStruct.Speed     = FREQ_G;
	HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);	
	
	// ADC_IN1
			GPIO_InitStruct.Pin      	= GPIO_PIN_4;
  GPIO_InitStruct.Mode      = SET_MODE;
  GPIO_InitStruct.Pull      = PULL_STATE;
  GPIO_InitStruct.Speed     = FREQ_G;
	HAL_GPIO_Init(GPIOC , &GPIO_InitStruct);
	
	 // RI_MCU (PB1)
	 		GPIO_InitStruct.Pin      	= GPIO_PIN_1;
  GPIO_InitStruct.Mode      = SET_MODE;
  GPIO_InitStruct.Pull      = PULL_STATE;
  GPIO_InitStruct.Speed     = FREQ_G;
	HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);
	
	// ADC_IN3 ( PB0)
			GPIO_InitStruct.Pin      	= GPIO_PIN_0;
  GPIO_InitStruct.Mode      = SET_MODE;
  GPIO_InitStruct.Pull      = PULL_STATE;
  GPIO_InitStruct.Speed     = FREQ_G;
	HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);
	
	// CHARG_DETECT(PD2) 
			GPIO_InitStruct.Pin      	= GPIO_PIN_2;
  GPIO_InitStruct.Mode      = SET_MODE;
  GPIO_InitStruct.Pull      = PULL_STATE;
  GPIO_InitStruct.Speed     = FREQ_G;
	HAL_GPIO_Init(GPIOD, &GPIO_InitStruct);
	
	 // MCU_TX_GNSS (PB3) 
	 		GPIO_InitStruct.Pin      	= GPIO_PIN_3;
  GPIO_InitStruct.Mode      = SET_MODE;
  GPIO_InitStruct.Pull      = PULL_STATE;
  GPIO_InitStruct.Speed     = FREQ_G;
	HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);
	
	
	 // JTCK (PA14) 
	 		GPIO_InitStruct.Pin      	= GPIO_PIN_14;
  GPIO_InitStruct.Mode      = SET_MODE;
  GPIO_InitStruct.Pull      = PULL_STATE;
  GPIO_InitStruct.Speed     = FREQ_G;
	HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);
	
	
	// JTMS ( PA13) 
			GPIO_InitStruct.Pin      	= GPIO_PIN_13;
  GPIO_InitStruct.Mode      = SET_MODE;
  GPIO_InitStruct.Pull      = PULL_STATE;
  GPIO_InitStruct.Speed     = FREQ_G;
	HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);
	
	
	 // SCL (PA9) 
	 		GPIO_InitStruct.Pin      	= GPIO_PIN_9;
  GPIO_InitStruct.Mode      = SET_MODE;
  GPIO_InitStruct.Pull      = PULL_STATE;
  GPIO_InitStruct.Speed     = FREQ_G;
	HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);
	
	 // GPS_LED(PA8) 
	 		GPIO_InitStruct.Pin      	= GPIO_PIN_8;
  GPIO_InitStruct.Mode      = SET_MODE;
  GPIO_InitStruct.Pull      = PULL_STATE;
  GPIO_InitStruct.Speed     = FREQ_G;
	HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);
	
	
	// SPI2_MOSI ( PB15) 
			GPIO_InitStruct.Pin      	= GPIO_PIN_15;
  GPIO_InitStruct.Mode      = SET_MODE;
  GPIO_InitStruct.Pull      = PULL_STATE;
  GPIO_InitStruct.Speed     = FREQ_G;
	HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);
	
	// USART1_TX (PB10) 
			GPIO_InitStruct.Pin      	= GPIO_PIN_10;
  GPIO_InitStruct.Mode      = SET_MODE;
  GPIO_InitStruct.Pull      = PULL_STATE;
  GPIO_InitStruct.Speed     = FREQ_G;
	HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);
	
	 // USART1_RX (PB11) 
	 		GPIO_InitStruct.Pin      	= GPIO_PIN_11;
  GPIO_InitStruct.Mode      = SET_MODE;
  GPIO_InitStruct.Pull      = PULL_STATE;
  GPIO_InitStruct.Speed     = FREQ_G;
	HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);
	 
	 
	 // PWRDN_N (PA12) 
	/* 		GPIO_InitStruct.Pin      	= GPIO_PIN_12;
  GPIO_InitStruct.Mode      = SET_MODE;
  GPIO_InitStruct.Pull      = GPIO_PULLDOWN;
  GPIO_InitStruct.Speed     = FREQ_G;
	HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);
	*/
	// PWRKEY (PA11) 
			GPIO_InitStruct.Pin      	= GPIO_PIN_11;
  GPIO_InitStruct.Mode      = SET_MODE;
  GPIO_InitStruct.Pull      = PULL_STATE;
  GPIO_InitStruct.Speed     = FREQ_G;
	HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);
	
	
	 // SPI .. (PB12) 
	 		GPIO_InitStruct.Pin      	= GPIO_PIN_12;
  GPIO_InitStruct.Mode      = SET_MODE;
  GPIO_InitStruct.Pull      = PULL_STATE;
  GPIO_InitStruct.Speed     = FREQ_G;
	HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);
	 // spi .. (PB13) 
	  		GPIO_InitStruct.Pin      	= GPIO_PIN_13;
  GPIO_InitStruct.Mode      = SET_MODE;
  GPIO_InitStruct.Pull      = PULL_STATE;
  GPIO_InitStruct.Speed     = FREQ_G;
	HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);
	 
	 // spi .. (PB14) 
	  		GPIO_InitStruct.Pin      	= GPIO_PIN_14;
  GPIO_InitStruct.Mode      = SET_MODE;
  GPIO_InitStruct.Pull      = PULL_STATE;
  GPIO_InitStruct.Speed     = FREQ_G;
	HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);
	
	
	// OSC32_IN ( PC14) 
	 		GPIO_InitStruct.Pin      	= GPIO_PIN_14;
  GPIO_InitStruct.Mode      = SET_MODE;
  GPIO_InitStruct.Pull      = PULL_STATE;
  GPIO_InitStruct.Speed     = FREQ_G;
	HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);
	
	// OSC32_OUT( PC15) 
	 		GPIO_InitStruct.Pin      	= GPIO_PIN_15;
  GPIO_InitStruct.Mode      = SET_MODE;
  GPIO_InitStruct.Pull      = PULL_STATE;
  GPIO_InitStruct.Speed     = FREQ_G;
	HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);
	
	// OSC12M_I (PH0) 
	 		GPIO_InitStruct.Pin      	= GPIO_PIN_0;
  GPIO_InitStruct.Mode      = SET_MODE;
  GPIO_InitStruct.Pull      = PULL_STATE;
  GPIO_InitStruct.Speed     = FREQ_G;
	HAL_GPIO_Init(GPIOH, &GPIO_InitStruct);
	
	// OSC12M_O (PH1) 
	 		GPIO_InitStruct.Pin      	= GPIO_PIN_1;
  GPIO_InitStruct.Mode      = SET_MODE;
  GPIO_InitStruct.Pull      = PULL_STATE;
  GPIO_InitStruct.Speed     = FREQ_G;
	HAL_GPIO_Init(GPIOH, &GPIO_InitStruct);
	
	//  GSENSOR_INT ( PC13)
	
	
	// left for gsensor int
	
	
	// TX_MCU (PA0) 
	GPIO_InitStruct.Pin      	= GPIO_PIN_0;
  GPIO_InitStruct.Mode      = SET_MODE;
  GPIO_InitStruct.Pull      = PULL_STATE;
  GPIO_InitStruct.Speed     = FREQ_G;
	HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);
	
	// RX_MCU( PA1) 
	GPIO_InitStruct.Pin      	= GPIO_PIN_1;
  GPIO_InitStruct.Mode      = SET_MODE;
  GPIO_InitStruct.Pull      = PULL_STATE;
  GPIO_InitStruct.Speed     = FREQ_G;
	HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);


   // GSM_LED ( PB9) 
	GPIO_InitStruct.Pin      	= GPIO_PIN_9;
  GPIO_InitStruct.Mode      = SET_MODE;
  GPIO_InitStruct.Pull      = PULL_STATE;
  GPIO_InitStruct.Speed     = FREQ_G;
	HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);
	
	  // PWR_LED (PB8) 
	GPIO_InitStruct.Pin      	= GPIO_PIN_8;
  GPIO_InitStruct.Mode      = SET_MODE;
  GPIO_InitStruct.Pull      = PULL_STATE;
  GPIO_InitStruct.Speed     = FREQ_G;
	HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);
	
	  // CTS_MCU (PB7) 
		
	GPIO_InitStruct.Pin      	= GPIO_PIN_7;
  GPIO_InitStruct.Mode      = SET_MODE;
  GPIO_InitStruct.Pull      = PULL_STATE;
  GPIO_InitStruct.Speed     = FREQ_G;
	HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);
	
	// DTR_MCU ( PB6) 
	GPIO_InitStruct.Pin      	= GPIO_PIN_6;
  GPIO_InitStruct.Mode      = SET_MODE;
  GPIO_InitStruct.Pull      = PULL_STATE;
  GPIO_InitStruct.Speed     = FREQ_G;
	HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

   // USART2_TX (PA2) 
	GPIO_InitStruct.Pin      	= GPIO_PIN_2;
  GPIO_InitStruct.Mode      = SET_MODE;
  GPIO_InitStruct.Pull      = PULL_STATE;
  GPIO_InitStruct.Speed     = FREQ_G;
	HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);
	
	// USART2_RX (PA3) 
	GPIO_InitStruct.Pin      	= GPIO_PIN_3;
  GPIO_InitStruct.Mode      = SET_MODE;
  GPIO_InitStruct.Pull      = PULL_STATE;
  GPIO_InitStruct.Speed     = FREQ_G;
	HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);
	
	
	// RST_GPS (PA4) 
	GPIO_InitStruct.Pin      	= GPIO_PIN_4;
  GPIO_InitStruct.Mode      = SET_MODE;
  GPIO_InitStruct.Pull      = PULL_STATE;
  GPIO_InitStruct.Speed     = FREQ_G;
	HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);
	
	// MCU_RX_GNSS ( PB4) 
	GPIO_InitStruct.Pin      	= GPIO_PIN_4;
  GPIO_InitStruct.Mode      = SET_MODE;
  GPIO_InitStruct.Pull      = PULL_STATE;
  GPIO_InitStruct.Speed     = FREQ_G;
	HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);
	
	// BOOT0 (ignore) 
	
	// DCD_MCU ( PB5) 
	GPIO_InitStruct.Pin      	= GPIO_PIN_5;
  GPIO_InitStruct.Mode      = SET_MODE;
  GPIO_InitStruct.Pull      = PULL_STATE;
  GPIO_InitStruct.Speed     = FREQ_G;
	HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);
	
	 // KEY_ON ( PA6) 
	GPIO_InitStruct.Pin      	= GPIO_PIN_6;
  GPIO_InitStruct.Mode      = SET_MODE;
  GPIO_InitStruct.Pull      = PULL_STATE;
  GPIO_InitStruct.Speed     = FREQ_G;
	HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);
	
	// ADC_IN4 (PA7)
	GPIO_InitStruct.Pin      	= GPIO_PIN_7;
  GPIO_InitStruct.Mode      = SET_MODE;
  GPIO_InitStruct.Pull      = PULL_STATE;
  GPIO_InitStruct.Speed     = FREQ_G;
	HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

#if 1
	// VCC_CTRL (PC 10)
	GPIO_InitStruct.Pin      	= GPIO_PIN_10;
	GPIO_InitStruct.Mode      = SET_MODE;
	GPIO_InitStruct.Pull      = PULL_STATE;
	GPIO_InitStruct.Speed     = FREQ_G;
	HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);

	// PWRDWN_N  (PA 12)
	GPIO_InitStruct.Pin      	= GPIO_PIN_12;
	GPIO_InitStruct.Mode      = SET_MODE;
	GPIO_InitStruct.Pull      = PULL_STATE;
	GPIO_InitStruct.Speed     = FREQ_G;
	HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

       // GSM_POW_CTRL (PC9) 
	GPIO_InitStruct.Pin      	= GPIO_PIN_9;
	GPIO_InitStruct.Mode      = SET_MODE;
	GPIO_InitStruct.Pull      = PULL_STATE;
	GPIO_InitStruct.Speed     = FREQ_G;
	HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);

	// GPIO_OUT2 (PC8) 
	/*
	GPIO_InitStruct.Pin      	= GPIO_PIN_8;
	GPIO_InitStruct.Mode      = SET_MODE;
	GPIO_InitStruct.Pull      = PULL_STATE;
	GPIO_InitStruct.Speed     = FREQ_G;
	HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);	
	*/
#endif
  // GSENSOR_INT1  (PC13) 
	/*
  GPIO_InitStruct.Pin      	= GPIO_PIN_13;
	GPIO_InitStruct.Mode      = GPIO_MODE_INPUT;
	GPIO_InitStruct.Pull      = PULL_STATE ; //PULL_STATE;
	GPIO_InitStruct.Speed     = FREQ_G;
	HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);
	*/
	GPIO_InitStruct.Pin = GPIO_PIN_13;
  GPIO_InitStruct.Mode = GPIO_MODE_ANALOG;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
	HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);

#if 1
       // B+_AD1 (PC2) 
  GPIO_InitStruct.Pin      	= GPIO_PIN_2;
	GPIO_InitStruct.Mode      = SET_MODE;
	GPIO_InitStruct.Pull      = PULL_STATE;
	GPIO_InitStruct.Speed     = FREQ_G;
	HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);	


	// SIM_CTRL (PC0) 
	GPIO_InitStruct.Pin      	= GPIO_PIN_0;
	GPIO_InitStruct.Mode      = SET_MODE;
	GPIO_InitStruct.Pull      = PULL_STATE;
	GPIO_InitStruct.Speed     = FREQ_G;
	HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);	


       // GPIO_OUT1  (PA5)
/*			 
  GPIO_InitStruct.Pin      	= GPIO_PIN_5;
	GPIO_InitStruct.Mode      = SET_MODE;
	GPIO_InitStruct.Pull      = PULL_STATE;
	GPIO_InitStruct.Speed     = FREQ_G;
	HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);	
	*/
#endif
	
	GPIO_InitStruct.Pin       = GPIO_PIN_12;
  GPIO_InitStruct.Mode      = SET_MODE;
  GPIO_InitStruct.Pull      = PULL_STATE;
  GPIO_InitStruct.Speed     = FREQ_G;
	
	HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);
	
	GPIO_InitStruct.Pin       = GPIO_PIN_6;
  GPIO_InitStruct.Mode      = SET_MODE;
  GPIO_InitStruct.Pull      = PULL_STATE;
  GPIO_InitStruct.Speed     = FREQ_G;
	
	HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);

}




void LpUartInit(void)
{	
	#if 0
	lpUartHandle.Instance = LPUART_USART;
	lpUartHandle.Init.BaudRate   = 9600;
	lpUartHandle.Init.WordLength = UART_WORDLENGTH_8B;
	lpUartHandle.Init.StopBits   = UART_STOPBITS_1;
	lpUartHandle.Init.Parity     = UART_PARITY_NONE;
	lpUartHandle.Init.HwFlowCtl  = UART_HWCONTROL_NONE;
	lpUartHandle.Init.Mode       = UART_MODE_TX_RX;
	
		
	if (HAL_UART_DeInit(&lpUartHandle) != HAL_OK)
  {
    /* Initialization Error */
		if(DebugEnable) DEBUG("lpUart DeInit error");
  }
	if (HAL_UART_Init(&lpUartHandle) != HAL_OK)
  {
    /* Initialization Error */
		if(DebugEnable) DEBUG("lpUart Init error");
  }
	
	uint8_t fRxData;
	if(HAL_UART_Receive_IT(&lpUartHandle, (uint8_t*)&fRxData, RXBUFFERSIZE) != HAL_OK)
	{
		if(DebugEnable) DEBUG(" lp HAL_UART_Receive_IT error");
	}else
	{
		if(DebugEnable) DEBUG(" lp HAL_UART_Receive_IT ok");
	}
	#endif
	
	
	GPIO_InitTypeDef  GPIO_InitStruct;
	//PC6
	__HAL_RCC_GPIOC_CLK_ENABLE();
	GPIO_InitStruct.Pin       = GPIO_PIN_6;
  GPIO_InitStruct.Mode      = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull      = GPIO_NOPULL;
  GPIO_InitStruct.Speed     = GPIO_SPEED_FREQ_VERY_HIGH;
	
	HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);
	HAL_GPIO_WritePin(GPIOC, GPIO_PIN_6, GPIO_PIN_RESET);
	
	lpUartHandle.Instance = LPUART_USART;
	lpUartHandle.Init.BaudRate   = 9600;
	lpUartHandle.Init.WordLength = UART_WORDLENGTH_8B;
	lpUartHandle.Init.StopBits   = UART_STOPBITS_1;
	lpUartHandle.Init.Parity     = UART_PARITY_NONE;
	lpUartHandle.Init.HwFlowCtl  = UART_HWCONTROL_NONE;
	lpUartHandle.Init.Mode       = UART_MODE_TX_RX;
	
		
	if (HAL_UART_DeInit(&lpUartHandle) != HAL_OK)
  {
    /* Initialization Error */
		if(DebugEnable) DEBUG("lpUart DeInit error");
  }
	
	__HAL_RCC_GPIOB_CLK_ENABLE();
	
	MX_LPUART1_UART_Init();
	
	__HAL_RCC_GPIOC_CLK_ENABLE();
	
	GPIO_InitStruct.Pin       = GPIO_PIN_12;
  GPIO_InitStruct.Mode      = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull      = GPIO_NOPULL;
  GPIO_InitStruct.Speed     = GPIO_SPEED_FREQ_VERY_HIGH;
	
	HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);
	HAL_GPIO_WritePin(GPIOC, GPIO_PIN_12, GPIO_PIN_SET);
}

bool RTC_INIT_SUCCESS(void)
{
return rtc_init;
}

void RTC_Config(void)
{
 /* Configure RTC */
  RTCHandle.Instance = RTC;
  /* Set the RTC time base to 1s */
  /* Configure RTC prescaler and RTC data registers as follow:
    - Hour Format = Format 24
    - Asynch Prediv = Value according to source clock
    - Synch Prediv = Value according to source clock
    - OutPut = Output Disable
    - OutPutPolarity = High Polarity
    - OutPutType = Open Drain */
  RTCHandle.Init.HourFormat = RTC_HOURFORMAT_24;
  RTCHandle.Init.AsynchPrediv = RTC_ASYNCH_PREDIV;
  RTCHandle.Init.SynchPrediv = RTC_SYNCH_PREDIV;
  RTCHandle.Init.OutPut = RTC_OUTPUT_DISABLE;
  RTCHandle.Init.OutPutPolarity = RTC_OUTPUT_POLARITY_HIGH;
  RTCHandle.Init.OutPutType = RTC_OUTPUT_TYPE_OPENDRAIN;
  if(HAL_RTC_Init(&RTCHandle) != HAL_OK)
  {
    /* Initialization Error */
   if(DebugEnable) DEBUG("RTC_Init error");
	 rtc_init = false;
	 return;
  }
	rtc_init = true;
	//start from 11
	if(!strcmp((char *)get_conf_elem(RESERVE3_CONF),"1") && !wakeup_report)
	send_message_number(10);
	
	MGA_INI_TIME_UTC();
}




uint8_t RTC_TIME(nmeaTIME time,bool check)
{
	RTC_DateTypeDef  sdatestructure;
  RTC_TimeTypeDef  stimestructure;

	if(check){
	HAL_RTC_GetDate(&RTCHandle, &sdatestructure, RTC_FORMAT_BIN);
	
	if(sdatestructure.Year == (time.year - 2000) && sdatestructure.Month == (time.mon + 1) && 
		sdatestructure.Date == time.day)
		return 1;
	}
	
  /*##-1- Configure the Date #################################################*/
  /* Set Date: Tuesday February 18th 2014 */
  //sdatestructure.Year = time.year-2000;
	sdatestructure.Year = (( time.year > 2000) ?  time.year - 2000 :  time.year);
	if(DebugEnable) DEBUG("***********************************%d",sdatestructure.Year);
	utc_year = sdatestructure.Year;
  sdatestructure.Month = time.mon+1;
  sdatestructure.Date = time.day;
  //sdatestructure.WeekDay = RTC_WEEKDAY_TUESDAY;
  
  if(HAL_RTC_SetDate(&RTCHandle,&sdatestructure,RTC_FORMAT_BIN) != HAL_OK)
  {
    /* Initialization Error */
    if(DebugEnable) DEBUG("RTC_SetDate error");
		return -1;
  } 
  
  /*##-2- Configure the Time #################################################*/
  /* Set Time: 02:20:00 */
  stimestructure.Hours = time.hour;
  stimestructure.Minutes = time.min;
  stimestructure.Seconds = time.sec;
  stimestructure.TimeFormat = RTC_HOURFORMAT12_AM;
  stimestructure.DayLightSaving = RTC_DAYLIGHTSAVING_NONE ;
  stimestructure.StoreOperation = RTC_STOREOPERATION_RESET;
  
  if(HAL_RTC_SetTime(&RTCHandle,&stimestructure,RTC_FORMAT_BIN) != HAL_OK)
  {
    /* Initialization Error */
   if(DebugEnable) DEBUG("RTC_SetTime error");
	 return -1;
  }
	return 1;
}

/**
  * @brief  Display the current time.
  * @param  showtime : pointer to buffer
  * @retval None
  */
void RTC_TimeShow(uint8_t* showtime)
{
  RTC_DateTypeDef sdatestructureget;
  RTC_TimeTypeDef stimestructureget;
  
  /* Get the RTC current Time */
  HAL_RTC_GetTime(&RTCHandle, &stimestructureget, RTC_FORMAT_BIN);
  /* Get the RTC current Date */
  HAL_RTC_GetDate(&RTCHandle, &sdatestructureget, RTC_FORMAT_BIN);
  /* Display time Format : hh:mm:ss */
  //sprintf((char*)showtime,"%02d:%02d:%02d",stimestructureget.Hours, stimestructureget.Minutes, stimestructureget.Seconds);
	
	//if(DebugEnable) DEBUG("showtime %02d",sdatestructureget.Year);
} 



void RTC_WakeUp(void)
{
	
	/* Disable all used wakeup sources*/
  HAL_RTCEx_DeactivateWakeUpTimer(&RTCHandle);
	
	//20 /0.41 
	HAL_RTCEx_SetWakeUpTimer_IT(&RTCHandle, 20, RTC_WAKEUPCLOCK_CK_SPRE_16BITS);
	
  
}


void ADC_Config(void)
{

	   /*
    ??0   PA0
    ??1   PA1
    ??2   PA2
    ??3   PA3
    ??4   PA4
    ??5   PA5
    ??6   PA6
    ??7   PA7
    ??8   PB0
    ??9   PB1
    ??10   PC0
    ??11   PC1
    ??12   PC2
    ??13   PC3
    ??14   PC4
    ??15   PC5 
    */
	AdcHandle.Instance = ADC1;
  
  AdcHandle.Init.OversamplingMode      = DISABLE;
  
  AdcHandle.Init.ClockPrescaler        = ADC_CLOCK_SYNC_PCLK_DIV1;
  AdcHandle.Init.LowPowerAutoPowerOff  = DISABLE;
  AdcHandle.Init.LowPowerFrequencyMode = ENABLE;
  AdcHandle.Init.LowPowerAutoWait      = DISABLE;
    
  AdcHandle.Init.Resolution            = ADC_RESOLUTION_12B;
  AdcHandle.Init.SamplingTime          = ADC_SAMPLETIME_7CYCLES_5;
  AdcHandle.Init.ScanConvMode          = ADC_SCAN_DIRECTION_FORWARD;
  AdcHandle.Init.DataAlign             = ADC_DATAALIGN_RIGHT;
  AdcHandle.Init.ContinuousConvMode    = ENABLE;
  AdcHandle.Init.DiscontinuousConvMode = DISABLE;
  AdcHandle.Init.ExternalTrigConvEdge  = ADC_EXTERNALTRIGCONVEDGE_NONE;
  AdcHandle.Init.EOCSelection          = ADC_EOC_SINGLE_CONV;
  AdcHandle.Init.DMAContinuousRequests = DISABLE;
  
  /* Initialize ADC peripheral according to the passed parameters */
  if (HAL_ADC_Init(&AdcHandle) != HAL_OK)
  {
    if(DebugEnable) DEBUG("ADC_Init error");
  }
  
  
  /* ### - 2 - Start calibration ############################################ */
  if (HAL_ADCEx_Calibration_Start(&AdcHandle, ADC_SINGLE_ENDED) != HAL_OK)
  {
    if(DebugEnable) DEBUG("Calibration_Start error");
  }

  /* ### - 3 - Channel configuration ######################################## */
  /* Select Channel 0 to be converted */
  sConfig.Channel = ADC_CHANNEL_12;
	sConfig.Rank = ADC_RANK_CHANNEL_NUMBER;
  if (HAL_ADC_ConfigChannel(&AdcHandle, &sConfig) != HAL_OK)
  {
    if(DebugEnable) DEBUG("ADC_ConfigChannel error");
  }
	
	sConfig.Channel = ADC_CHANNEL_8;
	if (HAL_ADC_ConfigChannel(&AdcHandle, &sConfig) != HAL_OK)
  {
    if(DebugEnable) DEBUG("ADC_ConfigChannel error");
  }
#if 0
 /*##- 4- Start the conversion process #######################################*/  
  if (HAL_ADC_Start(&AdcHandle) != HAL_OK)
  {
    /* Start Conversation Error */
    if(DebugEnable) DEBUG("ADC_Start error");
  }
#endif

}

void ADC_Value(int8_t time)
{
	
	if(time % 2 != 0)
			return;
	
	int sleepV =  ((atoi(get_conf_elem(RESERVE1_CONF)))*4096)/(3300*19);
	
	
	sConfig.Channel = ADC_CHANNEL_12;
	sConfig.Rank = ADC_RANK_CHANNEL_NUMBER;
  if (HAL_ADC_ConfigChannel(&AdcHandle, &sConfig) != HAL_OK)
  {
    if(DebugEnable) DEBUG("ADC_ConfigChannel error");
  }
	
	if (HAL_ADC_Start(&AdcHandle) != HAL_OK)
  {
    /* Start Conversation Error */
    if(DebugEnable) DEBUG("ADC_Start error");
  }
			
	HAL_ADC_PollForConversion(&AdcHandle, 10);
  
    /* Check if the continous conversion of regular channel is finished */
   if ((HAL_ADC_GetState(&AdcHandle) & HAL_ADC_STATE_REG_EOC) == HAL_ADC_STATE_REG_EOC)
   {
      /*##-6- Get the converted value of regular channel  ########################*/
		 
		 /* before
		   12v:255
			 11v:	231
		   10.8:224
		   10.5:220 - 224
		   10v: 215    
		   9v: 184     
		   8v: 138
			 0v: 150
		 */
		 /*after
		   24v:1536
		   22v:1400
			 22.5:1423
			 18v:1140	 
		   15v:935
		   14v:864
		   12v:725
		   11.5v:704
			 11.2v:680
			 11v:655
		   10.8:639
		   10.5:640
		   10v:215    
		   9v:184     
		   8v:138
			after 1
		   24v:1574
		   22v:1440
		   22.5:1472
		   18v:1181	 
		   15v:980
		   14v:908
		   12v:770
		   11.5v:752
			 11.2v:735
			 11v:700
		   10.8:690
		   10.5:687
		   10v:637    
		   9v:575     
		   8v:511
			 #define VOLTAGE_11_2   735
			 #define VOLTAGE_11_5   752
       #define VOLTAGE_18_0	 1181
       #define VOLTAGE_22_0   1440
       #define VOLTAGE_22_5   1472
		 */
		 
      s_uwADCxConvertedValue = HAL_ADC_GetValue(&AdcHandle);
			
			//if(getGpsSpeed() > 10.0 && s_uwADCxConvertedValue < 725){
			//return;
			//}
			
			uwADCxConvertedValue = (s_uwADCxConvertedValue + uwADCxConvertedValue)/2;
			
			if(uwADCxConvertedValue < VOLTAGE_18_0)
			{
					if(sleepV < VOLTAGE_11_5 || sleepV > VOLTAGE_18_0)
						sleepV = VOLTAGE_11_2 + 10;
			}else
			{
				if((sleepV < VOLTAGE_22_5 && sleepV > VOLTAGE_18_0) || sleepV < VOLTAGE_18_0)
							sleepV = VOLTAGE_22_0 + 15;
			}
				
		 #if 1
			if(uwADCxConvertedValue < VOLTAGE_0 && alarm_time == 0){
				time_batt_cut++;
				time_batt_low = 0;
				time_batt_sleep = 0;
				if((time_batt_cut == 5 && time_batt_cut_report < 2) || worktestmode){
				GlobalStatus |= VBAT_LOW;
				time_batt_cut = 0;
				if(DebugEnable) DEBUG("***cut alarm***");
				if(worktestmode) DEBUG("CUT ALARM TEST OK");
				getBattLowVolt();
				cutdetectlowEvent(CUT_DETECT_EVENT);
				alarm_time++;
				time_batt_cut_report++;
				osDelay(5000);
				if(!strcmp((char *)get_conf_elem(RESERVE6_CONF),"1") || !strcmp((char *)get_conf_elem(RESERVE6_CONF),"2"))
				setEvent(GSENSOR_SLEEP_EVENT);
				}
			}
			else if((uwADCxConvertedValue > VOLTAGE_0 && uwADCxConvertedValue < sleepV && !isAccOn())
				|| (uwADCxConvertedValue > VOLTAGE_18_0 && uwADCxConvertedValue < sleepV && !isAccOn())){
				time_batt_sleep++;
				time_batt_low = 0;
				time_batt_cut = 0;
				time_batt_cut_report = 0;
				if(time_batt_sleep == 120 ){
				time_batt_sleep = 0;
				osDelay(5000);
				setEvent(GSENSOR_SLEEP_EVENT);
				}
				GlobalStatus &= ~VBAT_LOW;
			}
			else if((!firstWork && uwADCxConvertedValue > VOLTAGE_0 && uwADCxConvertedValue < VOLTAGE_11_5 && alarm_time == 0 &&  !strncmp((char *)get_conf_elem(WARNLOWBATT_CONF),"1",1))
				||((!firstWork && uwADCxConvertedValue > VOLTAGE_18_0 && uwADCxConvertedValue < VOLTAGE_22_5 && alarm_time == 0 &&  !strncmp((char *)get_conf_elem(WARNLOWBATT_CONF),"1",1)))){
				time_batt_low++;
				time_batt_cut = 0;
				time_batt_sleep = 0;
				time_batt_cut_report = 0;
				if(time_batt_low == 60 || worktestmode){
				time_batt_low = 0;
				if(DebugEnable) DEBUG("***BatteryLow alarm***");
				if(worktestmode) DEBUG("BATTERRYLOW ALARM TEST OK");
				getBattLowVolt();
				batterylowEvent(BATTLE_LOW_EVENT);
				alarm_time++;
				}
				GlobalStatus &= ~VBAT_LOW;
			}
			else if((uwADCxConvertedValue > VOLTAGE_11_5 && uwADCxConvertedValue <1000)||
				(uwADCxConvertedValue > VOLTAGE_22_5 ))
			{
				alarm_time = 0;
				time_batt_cut = 0;
				time_batt_low = 0;
				time_batt_sleep = 0;
				time_batt_cut_report = 0;
				GlobalStatus &= ~VBAT_LOW;
			}else if(uwADCxConvertedValue > VOLTAGE_0)
			{
				GlobalStatus &= ~VBAT_LOW;
				time_batt_cut_report = 0;
			}
			#if 0
			if(!isAccOn())
			{
				  test_time ++;
					if(test_time == 240){
					test_time = 0;
					osDelay(5000);
					setEvent(GSENSOR_SLEEP_EVENT);
					}
			}else{
					test_time = 0;
			}
			
			if(test_time > 240)
				 test_time = 0;
			#endif
			
			if(alarm_time > 0)
					alarm_time++;
			if(alarm_time > 40)
				alarm_time = 0;
			if(time_batt_cut > 5)
				time_batt_cut = 0;
			if(time_batt_low > 60)
				time_batt_low = 0;
			if(time_batt_sleep > 120)
				time_batt_sleep = 0;
			#endif
			//uwADCxConvertedValue = uwADCxConvertedValue * 12000 / 256;
			//if(DebugEnable) DEBUG("uwADCxConvertedValue %d",uwADCxConvertedValue);
   }
	 
	 if (HAL_ADC_Stop(&AdcHandle) != HAL_OK)
	 {
    /* Start Conversation Error */
    if(DebugEnable) DEBUG("ADC_Stop error");
   }
	 
	 AdcHandle.Instance->CHSELR = 0;

}


void New_ADC_Value(int8_t time)
{
	sConfig.Channel = ADC_CHANNEL_8;
  if (HAL_ADC_ConfigChannel(&AdcHandle, &sConfig) != HAL_OK)
  {
    if(DebugEnable) DEBUG("ADC_ConfigChannel error");
  }
	
	if (HAL_ADC_Start(&AdcHandle) != HAL_OK)
  {
    /* Start Conversation Error */
    if(DebugEnable) DEBUG("ADC_Start error");
  }
			
	HAL_ADC_PollForConversion(&AdcHandle, 10);
  
    /* Check if the continous conversion of regular channel is finished */
   if ((HAL_ADC_GetState(&AdcHandle) & HAL_ADC_STATE_REG_EOC) == HAL_ADC_STATE_REG_EOC)
   { 
		  new_s_uwADCxConvertedValue = HAL_ADC_GetValue(&AdcHandle);
	 }
	 
	 if (HAL_ADC_Stop(&AdcHandle) != HAL_OK)
	 {
    /* Start Conversation Error */
    if(DebugEnable) DEBUG("ADC_Stop error");
   }
	 
	 AdcHandle.Instance->CHSELR = 0;
	 
	 if(new_s_uwADCxConvertedValue > 2){
	 if(worktestmode) DEBUG("new_s_uwADCxConvertedValue %d",new_s_uwADCxConvertedValue);
	 }
	 else
		 new_s_uwADCxConvertedValue = 0;
	 
	 //if(worktestmode) DEBUG("new_s_uwADCxConvertedValue %d",new_s_uwADCxConvertedValue);
}

/**
  * @brief  System Power Configuration
  *         The system Power is configured as follow : 
  *            + System Running at MSI (~32KHz)
  *            + Flash 0 wait state  
  *            + Voltage Range 2
  *            + Code running from Internal FLASH
  *            + Wakeup using User push-button PC.13
  * @param  None
  * @retval None
  */
void SystemClock_Config_LowPowerRun(void)
{
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  /* Enable Power Control clock */
  __HAL_RCC_PWR_CLK_ENABLE();

  /* The voltage scaling allows optimizing the power consumption when the device is 
     clocked below the maximum system frequency, to update the voltage scaling value 
     regarding system frequency refer to product datasheet.
     Low power sleep mode can only be entered when VCORE is in range 2. */
  __HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE2);
  
  /* Enable MSI Oscillator */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_MSI;
  RCC_OscInitStruct.MSIState = RCC_MSI_ON;
  RCC_OscInitStruct.MSIClockRange = RCC_MSIRANGE_5;
  RCC_OscInitStruct.MSICalibrationValue = 0x00;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_NONE;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct)!= HAL_OK)
  {
    /* Initialization Error */
    while(1); 
  }
  
  /* Select MSI as system clock source and configure the HCLK, PCLK1 and PCLK2 
     clocks dividers */
  RCC_ClkInitStruct.ClockType = (RCC_CLOCKTYPE_SYSCLK | RCC_CLOCKTYPE_HCLK | RCC_CLOCKTYPE_PCLK1 | RCC_CLOCKTYPE_PCLK2);
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_MSI;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV2;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV1;  
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;  
  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_0)!= HAL_OK)
  {
    /* Initialization Error */
    while(1); 
  }
  
  /* Set MSI range to 0 */
  __HAL_RCC_MSI_RANGE_CONFIG(RCC_MSIRANGE_0);
  
  /* Configure the SysTick to have interrupt in 10 ms time basis*/
  HAL_SYSTICK_Config(HAL_RCC_GetHCLKFreq()/100);
}

/**
  * @brief  System Power Configuration
  *         The system Power is configured as follow : 
  *            + System Running at MSI (~32KHz)
  *            + Flash 0 wait state  
  *            + Voltage Range 2
  *            + Code running from Internal FLASH
  *            + Wakeup using User push-button PC.13
  * @param  None
  * @retval None
  */
static void SystemPower_Config_LowPowerRun(void)
{
  GPIO_InitTypeDef GPIO_InitStructure;

  /* Enable Power Control clock */
  __HAL_RCC_PWR_CLK_ENABLE();
	
	RTC_Config();

  /* Enable GPIOs clock */
  __HAL_RCC_GPIOA_CLK_ENABLE();
  __HAL_RCC_GPIOB_CLK_ENABLE();
  __HAL_RCC_GPIOC_CLK_ENABLE();
  __HAL_RCC_GPIOD_CLK_ENABLE();
  __HAL_RCC_GPIOH_CLK_ENABLE();
  __HAL_RCC_GPIOE_CLK_ENABLE();

  /* Configure all GPIO port pins in Analog Input mode (floating input trigger OFF) */
  /* Note: Debug using ST-Link is not possible during the execution of this   */
  /*       example because communication between ST-link and the device       */
  /*       under test is done through UART. All GPIO pins are disabled (set   */
  /*       to analog input mode) including  UART I/O pins.           */
  GPIO_InitStructure.Pin = GPIO_PIN_All;
  GPIO_InitStructure.Mode = GPIO_MODE_ANALOG;
  GPIO_InitStructure.Pull = GPIO_NOPULL;

  HAL_GPIO_Init(GPIOA, &GPIO_InitStructure); 
  HAL_GPIO_Init(GPIOB, &GPIO_InitStructure);
  HAL_GPIO_Init(GPIOC, &GPIO_InitStructure);
  HAL_GPIO_Init(GPIOD, &GPIO_InitStructure);
  HAL_GPIO_Init(GPIOH, &GPIO_InitStructure);
  HAL_GPIO_Init(GPIOE, &GPIO_InitStructure);

  /* Disable GPIOs clock */
  __HAL_RCC_GPIOA_CLK_DISABLE();
  __HAL_RCC_GPIOB_CLK_DISABLE();
  __HAL_RCC_GPIOC_CLK_DISABLE();
  __HAL_RCC_GPIOD_CLK_DISABLE();
  __HAL_RCC_GPIOH_CLK_DISABLE();
  __HAL_RCC_GPIOE_CLK_DISABLE();
}


/**
  * @brief  System Lower Power Configuration
  *         The system Power is configured as follow : 
  *            + System Running at MSI (~32KHz)
  *            + Flash 0 wait state  
  *            + Voltage Range 2
  *            + Code running from Internal FLASH
  *            + Wakeup using User push-button PC.13
  * @param  None
  * @retval None
  */
void SystemPower_Config_StandBy(void)
{
	 /* Enable Power Control clock */
  __HAL_RCC_PWR_CLK_ENABLE();

  /* Enable Ultra low power mode */
  HAL_PWREx_EnableUltraLowPower();
  
  /* Enable the fast wake up from Ultra low power mode */
  HAL_PWREx_EnableFastWakeUp();
	
}



/**
  * @brief  System Power Configuration
  *         The system Power is configured as follow :
  *            + Regulator in LP mode
  *            + VREFINT OFF, with fast wakeup enabled
  *            + HSI as SysClk after Wake Up
  *            + No IWDG
  *            + Automatic Wakeup using RTC clocked by LSI (after ~4s)
  * @param  None
  * @retval None
  */
static void SystemPower_Config_Stop(void)
{
  GPIO_InitTypeDef GPIO_InitStructure = {0};

  /* Enable Power Control clock */
  __HAL_RCC_PWR_CLK_ENABLE();

  /* Enable Ultra low power mode */
  HAL_PWREx_EnableUltraLowPower();

  /* Enable the fast wake up from Ultra low power mode */
  HAL_PWREx_EnableFastWakeUp();

  /* Enable GPIOs clock */
  __HAL_RCC_GPIOA_CLK_ENABLE();
  __HAL_RCC_GPIOB_CLK_ENABLE();
  __HAL_RCC_GPIOC_CLK_ENABLE();
  __HAL_RCC_GPIOD_CLK_ENABLE();
  __HAL_RCC_GPIOH_CLK_ENABLE();
  __HAL_RCC_GPIOE_CLK_ENABLE();

  /* Configure all GPIO port pins in Analog Input mode (floating input trigger OFF) */
  /* Note: Debug using ST-Link is not possible during the execution of this   */
  /*       example because communication between ST-link and the device       */
  /*       under test is done through UART. All GPIO pins are disabled (set   */
  /*       to analog input mode) including  UART I/O pins.           */
  GPIO_InitStructure.Pin = GPIO_PIN_All;
  GPIO_InitStructure.Mode = GPIO_MODE_ANALOG;
  GPIO_InitStructure.Pull = GPIO_NOPULL;

  HAL_GPIO_Init(GPIOA, &GPIO_InitStructure); 
  HAL_GPIO_Init(GPIOB, &GPIO_InitStructure);
  HAL_GPIO_Init(GPIOC, &GPIO_InitStructure);
  HAL_GPIO_Init(GPIOD, &GPIO_InitStructure);
  HAL_GPIO_Init(GPIOH, &GPIO_InitStructure);
  HAL_GPIO_Init(GPIOE, &GPIO_InitStructure);

  /* Disable GPIOs clock */
  __HAL_RCC_GPIOA_CLK_DISABLE();
  __HAL_RCC_GPIOB_CLK_DISABLE();
  __HAL_RCC_GPIOC_CLK_DISABLE();
  __HAL_RCC_GPIOD_CLK_DISABLE();
  __HAL_RCC_GPIOH_CLK_DISABLE();
  __HAL_RCC_GPIOE_CLK_DISABLE();
 
}



void StandBy(void)
{
	gpsPowerOff();
	gsmPowerOff();
	
  /* Configure the system Power */
  SystemPower_Config_StandBy();

	/* Check and handle if the system was resumed from StandBy mode */ 
  if(__HAL_PWR_GET_FLAG(PWR_FLAG_SB) != RESET)
  {
    /* Clear Standby flag */
    __HAL_PWR_CLEAR_FLAG(PWR_FLAG_SB); 
  }
  
  /* Insert 5 seconds delay */
  HAL_Delay(5000);
	
	/* Disable all used wakeup sources: GSENSOR_PIN */
  HAL_PWR_DisableWakeUpPin(PWR_WAKEUP_PIN2);
	
	 /* Clear all related wakeup flags */
  __HAL_PWR_CLEAR_FLAG(PWR_FLAG_WU);
	
	/* Enable WakeUp Pin GSENSOR_PIN connected to PC.13 */
  HAL_PWR_EnableWakeUpPin(PWR_WAKEUP_PIN2);
	
	//if(DebugEnable) DEBUG("enter to standby");
	
	 /* Enter the Standby mode */
  HAL_PWR_EnterSTANDBYMode();
}


void Lower_Power_Run()
{
	
	if(DebugEnable) DEBUG("enter to LPR");
	
	gpsPowerOff();
	
	gsmPowerOff();
	
	SystemClock_Config_LowPowerRun();
	
	__HAL_FLASH_PREFETCH_BUFFER_DISABLE();
	
	SystemPower_Config_LowPowerRun();
	
	HAL_PWREx_EnableLowPowerRunMode();

    /* Wait until the system enters LP RUN and the Regulator is in LP mode */
  while(__HAL_PWR_GET_FLAG(PWR_FLAG_REGLP) == RESET)
  {
  }

}


void Stop()
{
	gpsPowerOff();
	gsmPowerOff();
	
	deInitWWDG();
	
	stop_pwm_out1_tosleep();
	
	stop_pwm_out2_tosleep();
	
	//SystemPower_Config_Stop();
	low_power_pins() ;
	
	/* Enable Power Control clock */
  //__HAL_RCC_PWR_CLK_ENABLE();
	
	if((!strncmp((char *)get_conf_elem(WARNCIRCLE_CONF),"1",1)
		&& uwADCxConvertedValue > VOLTAGE_11_5  && uwADCxConvertedValue < 1000) || (!strncmp((char *)get_conf_elem(WARNCIRCLE_CONF),"1",1)
		&& uwADCxConvertedValue > VOLTAGE_22_5))
	gsensor_INT();
	
	accon_INT();
	
	simcard_INT();
		
	if(!strcmp((char *)get_conf_elem(RESERVE6_CONF),"1") || !strcmp((char *)get_conf_elem(RESERVE6_CONF),"2"))
	sos_INT();
	
	//900 /0.41
	if(!strcmp((char *)get_conf_elem(RESERVE6_CONF),"1")){
	if(GlobalStatus & VBAT_LOW){
	HAL_RTCEx_SetWakeUpTimer_IT(&RTCHandle, 900, RTC_WAKEUPCLOCK_CK_SPRE_16BITS);
	update_conf_elem(WARNCIRCLE_CONF,"0");
	}
	setGpsSendTimer(ACC_EVENT_OFF);
	}else if (!strcmp((char *)get_conf_elem(RESERVE6_CONF),"2")){
	HAL_RTCEx_SetWakeUpTimer_IT(&RTCHandle, atoi((char *)get_conf_elem(ACCOFFREPINTERVAL_CONF)), RTC_WAKEUPCLOCK_CK_SPRE_16BITS);
	setGpsSendTimer(ACC_EVENT_OFF);
	}
	
	__HAL_RCC_PWR_CLK_DISABLE();
	
 /* Enter Stop Mode */
  HAL_PWR_EnterSTOPMode(PWR_LOWPOWERREGULATOR_ON, PWR_STOPENTRY_WFI);
	
	wakeup();
	
}

void InitLed()
{
	GPIO_InitTypeDef  GPIO_InitStruct;
	//pwr led
	__HAL_RCC_GPIOB_CLK_ENABLE();
	GPIO_InitStruct.Pin       = GPIO_PIN_8;
  GPIO_InitStruct.Mode      = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull      = GPIO_NOPULL;
  GPIO_InitStruct.Speed     = GPIO_SPEED_FREQ_VERY_HIGH;
	
	HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);
	HAL_GPIO_WritePin(GPIOB, GPIO_PIN_8, GPIO_PIN_RESET);
	
	//gsm led
	GPIO_InitStruct.Pin       = GPIO_PIN_9;
  GPIO_InitStruct.Mode      = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull      = GPIO_NOPULL;
  GPIO_InitStruct.Speed     = GPIO_SPEED_FREQ_VERY_HIGH;
	
	HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);
	HAL_GPIO_WritePin(GPIOB, GPIO_PIN_9, GPIO_PIN_RESET);
	//gps led
	__HAL_RCC_GPIOA_CLK_ENABLE();
	GPIO_InitStruct.Pin       = GPIO_PIN_8;
  GPIO_InitStruct.Mode      = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull      = GPIO_NOPULL;
  GPIO_InitStruct.Speed     = GPIO_SPEED_FREQ_VERY_HIGH;
	
	HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);
	HAL_GPIO_WritePin(GPIOA, GPIO_PIN_8, GPIO_PIN_RESET);

}

void powerLedOn()
{
	GPIO_InitTypeDef  GPIO_InitStruct;
	//pwr led
	__HAL_RCC_GPIOB_CLK_ENABLE();
	GPIO_InitStruct.Pin       = GPIO_PIN_8;
  GPIO_InitStruct.Mode      = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull      = GPIO_NOPULL;
  GPIO_InitStruct.Speed     = GPIO_SPEED_FREQ_VERY_HIGH;
	
	HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);
	HAL_GPIO_WritePin(GPIOB, GPIO_PIN_8, GPIO_PIN_RESET);
}
void ledoff()
{
	GPIO_InitTypeDef  GPIO_InitStruct;
	//pwr led
	__HAL_RCC_GPIOB_CLK_ENABLE();
	GPIO_InitStruct.Pin       = GPIO_PIN_8;
  GPIO_InitStruct.Mode      = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull      = GPIO_NOPULL;
  GPIO_InitStruct.Speed     = GPIO_SPEED_FREQ_VERY_HIGH;
	
	HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);
	HAL_GPIO_WritePin(GPIOB, GPIO_PIN_8, GPIO_PIN_SET);
	
	//gsm led
	GPIO_InitStruct.Pin       = GPIO_PIN_9;
  GPIO_InitStruct.Mode      = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull      = GPIO_NOPULL;
  GPIO_InitStruct.Speed     = GPIO_SPEED_FREQ_VERY_HIGH;
	
	HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);
	HAL_GPIO_WritePin(GPIOB, GPIO_PIN_9, GPIO_PIN_SET);
	
	//gps led
	__HAL_RCC_GPIOA_CLK_ENABLE();
	GPIO_InitStruct.Pin       = GPIO_PIN_8;
  GPIO_InitStruct.Mode      = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull      = GPIO_NOPULL;
  GPIO_InitStruct.Speed     = GPIO_SPEED_FREQ_VERY_HIGH;
	
	HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);
	HAL_GPIO_WritePin(GPIOA, GPIO_PIN_8, GPIO_PIN_SET);

}


void gpsLedToggle()
{
	GPIO_InitTypeDef  GPIO_InitStruct;
	__HAL_RCC_GPIOA_CLK_ENABLE();
	GPIO_InitStruct.Pin       = GPIO_PIN_8;
  GPIO_InitStruct.Mode      = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull      = GPIO_NOPULL;
  GPIO_InitStruct.Speed     = GPIO_SPEED_FREQ_VERY_HIGH;
	
	HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);
	if(HAL_GPIO_ReadPin(GPIOA,GPIO_PIN_8) == GPIO_PIN_RESET)
	HAL_GPIO_WritePin(GPIOA, GPIO_PIN_8, GPIO_PIN_SET);
	else
	HAL_GPIO_WritePin(GPIOA, GPIO_PIN_8, GPIO_PIN_RESET);		

}

void gpsLedOn()
{
	GPIO_InitTypeDef  GPIO_InitStruct;
	__HAL_RCC_GPIOA_CLK_ENABLE();
	GPIO_InitStruct.Pin       = GPIO_PIN_8;
  GPIO_InitStruct.Mode      = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull      = GPIO_NOPULL;
  GPIO_InitStruct.Speed     = GPIO_SPEED_FREQ_VERY_HIGH;
	
	HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);
	HAL_GPIO_WritePin(GPIOA, GPIO_PIN_8, GPIO_PIN_RESET);
}

void gpsLedOff()
{
	GPIO_InitTypeDef  GPIO_InitStruct;
	__HAL_RCC_GPIOA_CLK_ENABLE();
	GPIO_InitStruct.Pin       = GPIO_PIN_8;
  GPIO_InitStruct.Mode      = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull      = GPIO_NOPULL;
  GPIO_InitStruct.Speed     = GPIO_SPEED_FREQ_VERY_HIGH;
	
	HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);
	HAL_GPIO_WritePin(GPIOA, GPIO_PIN_8, GPIO_PIN_SET);
}

void gsmLedToggle()
{
	GPIO_InitTypeDef  GPIO_InitStruct;
	__HAL_RCC_GPIOB_CLK_ENABLE();
	GPIO_InitStruct.Pin       = GPIO_PIN_9;
  GPIO_InitStruct.Mode      = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull      = GPIO_NOPULL;
  GPIO_InitStruct.Speed     = GPIO_SPEED_FREQ_VERY_HIGH;
	
	HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);
	if(HAL_GPIO_ReadPin(GPIOB,GPIO_PIN_9) == GPIO_PIN_RESET)
	HAL_GPIO_WritePin(GPIOB, GPIO_PIN_9, GPIO_PIN_SET);
	else
	HAL_GPIO_WritePin(GPIOB, GPIO_PIN_9, GPIO_PIN_RESET);	
}

void gsmLedOn()
{
	GPIO_InitTypeDef  GPIO_InitStruct;
	__HAL_RCC_GPIOB_CLK_ENABLE();
	GPIO_InitStruct.Pin       = GPIO_PIN_9;
  GPIO_InitStruct.Mode      = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull      = GPIO_NOPULL;
  GPIO_InitStruct.Speed     = GPIO_SPEED_FREQ_VERY_HIGH;
	
	HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);
	HAL_GPIO_WritePin(GPIOB, GPIO_PIN_9, GPIO_PIN_RESET);
}

void gsmLedOff()
{
	GPIO_InitTypeDef  GPIO_InitStruct;
	__HAL_RCC_GPIOB_CLK_ENABLE();
	GPIO_InitStruct.Pin       = GPIO_PIN_9;
  GPIO_InitStruct.Mode      = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull      = GPIO_NOPULL;
  GPIO_InitStruct.Speed     = GPIO_SPEED_FREQ_VERY_HIGH;
	
	HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);
	HAL_GPIO_WritePin(GPIOB, GPIO_PIN_9, GPIO_PIN_SET);
}

void gpio_wakeup(void)
{
	//pc10
	GPIO_InitTypeDef  GPIO_InitStruct;
	
	__HAL_RCC_GPIOC_CLK_ENABLE();
	GPIO_InitStruct.Pin       = GPIO_PIN_10;
  GPIO_InitStruct.Mode      = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull      = GPIO_NOPULL;
  GPIO_InitStruct.Speed     = GPIO_SPEED_FREQ_VERY_HIGH;
	
	HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);
	HAL_GPIO_WritePin(GPIOC, GPIO_PIN_10, GPIO_PIN_SET);
	
	//sos
	__HAL_RCC_GPIOA_CLK_ENABLE();
	
	GPIO_InitStruct.Pin       = GPIO_PIN_7;
  GPIO_InitStruct.Mode      = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull      = GPIO_NOPULL;
  GPIO_InitStruct.Speed     = GPIO_SPEED_FREQ_VERY_HIGH;
	
	HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);
	HAL_GPIO_WritePin(GPIOA, GPIO_PIN_7, GPIO_PIN_SET);
	
	//in+,in-
	__HAL_RCC_GPIOB_CLK_ENABLE();
	
	GPIO_InitStruct.Pin       = GPIO_PIN_0;
  GPIO_InitStruct.Mode      = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull      = GPIO_NOPULL;
  GPIO_InitStruct.Speed     = GPIO_SPEED_FREQ_VERY_HIGH;
	
	HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);
	HAL_GPIO_WritePin(GPIOB, GPIO_PIN_0, GPIO_PIN_SET);
	
	
	__HAL_RCC_GPIOC_CLK_ENABLE();
	
	GPIO_InitStruct.Pin       = GPIO_PIN_5;
  GPIO_InitStruct.Mode      = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull      = GPIO_NOPULL;
  GPIO_InitStruct.Speed     = GPIO_SPEED_FREQ_VERY_HIGH;
	
	HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);
	HAL_GPIO_WritePin(GPIOC, GPIO_PIN_5, GPIO_PIN_SET);
	
	//simcard
	__HAL_RCC_GPIOC_CLK_ENABLE();
	
	GPIO_InitStruct.Pin       = GPIO_PIN_0;
  GPIO_InitStruct.Mode      = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull      = GPIO_NOPULL;
  GPIO_InitStruct.Speed     = GPIO_SPEED_FREQ_VERY_HIGH;
	
	HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);
	
	//acc
	__HAL_RCC_GPIOC_CLK_ENABLE();
	
	GPIO_InitStruct.Pin       = GPIO_PIN_4;
  GPIO_InitStruct.Mode      = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull      = GPIO_NOPULL;
  GPIO_InitStruct.Speed     = GPIO_SPEED_FREQ_VERY_HIGH;
	
	HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);
	
	
}

void Normal_Power_Run(void)
{
	GlobalStatus &= ~GSM_SLEEP;
	setEvent(-1);
	
	gpio_wakeup();

	initWWDG();
	startWWDG();
	HardwareInit();
	initOutStatus();
	if(DebugEnable) DEBUG("wakeup");
	GlobalStatus |= GSM_WAKEUP;
	osDelay(2000);
	wakeup_report = true;
	if(!strcmp((char *)get_conf_elem(RESERVE6_CONF),"1") ||
		!strcmp((char *)get_conf_elem(RESERVE6_CONF),"2") ){
	HAL_RTCEx_SetWakeUpTimer_IT(&RTCHandle, 20, RTC_WAKEUPCLOCK_CK_SPRE_16BITS);
	}
}


uint8_t WorkAddTask(WorkTask_t * task)
{
	if(xWorkTaskQueue != 0){
		if (xQueueSend(xWorkTaskQueue, task, 0) == pdPASS){
		if(DebugEnable) DEBUG("pdPASS");
		return 1;
		}
		else{
		if(DebugEnable) DEBUG("no pdPASS");
		return 0;
		}
	}else{
	return 0;
	}
}


void setEvent(int et)
{
  event = et;
}


uint32_t getEvent()
{
  return event;
}



void handle_work_timer(void) {
	
}

void led_state(){
	
	powerLedOn();
	
	if(GlobalStatus & GSM_SLEEP)
		ledoff();
	if(!GpsNoData())
		gpsLedToggle();
	else
		gpsLedOn();
	if(getTCPConState())
		gsmLedToggle();
	else
		gsmLedOn();
	
}


void led_state_off(bool on){
	
	powerLedOn();
	
	if(GlobalStatus & GSM_SLEEP)
		ledoff();
	if(!GpsNoData() && on)
		gpsLedOn();
	if(!GpsNoData() && !on)
		gpsLedOff();
	else
		gpsLedOn();
	if(getTCPConState() && on)
		gsmLedOn();
	if(getTCPConState() && !on)
		gsmLedOff();
	else
		gsmLedOn();
	
}

int8_t* signal_state(void)
{
	int8_t* state;
	state = dst;
	int8_t str[10];
	
	register GpsContext_t * gc = &GpsContext;

	if(acc_ioff_send){
		acc_ioff_send = false;
		state = strcpyEx(state, signal_status[10]);
	}else if(acc_ion_send){
		acc_ion_send = false;
		state = strcpyEx(state, signal_status[8]);
	}else if(acc_on)
	state = strcpyEx(state, signal_status[9]);
	else
	state = strcpyEx(state, signal_status[11]);
	
	state = strcpyEx(state, ",");
	
	sprintf(str, "%s=%d", "EH", gc->enginHour) ; 
	state = strcpyEx(state,str);	
	state = strcpyEx(state, ",");
	sprintf(str, "%s=%d", "ML", gc->mileage) ;
	state = strcpyEx(state,str);	
	state = strcpyEx(state, ",");
	
	if(in_plus_send)
		state = strcpyEx(state, "INP=1|");
	else
		state = strcpyEx(state, "INP=0|");
	if(in_minus_send)
		state = strcpyEx(state, "1");
	else
		state = strcpyEx(state, "0");
	
	state = strcpyEx(state, ",");
	
	state = strcpyEx(state, "AD=");
	
	state = itoa(state,read_message_number()+1,1);
	
	state = strcpyEx(state, "|");
	
	state = strcpyEx(state, Rs232Val);
	
	state = strcpyEx(state, "|");
	
	memset(str,0,10);
	sprintf(str, "%.1lf", Temperature) ;
	state = strcpyEx(state,str);
	state = strcpyEx(state, ";");
	memset(str,0,10);
	sprintf(str, "%.1lf", Humi) ;
	state = strcpyEx(state,str);

  state = strcpyEx(state, "|");
  
	memset(str,0,10);
	sprintf(str, "%d", new_s_uwADCxConvertedValue) ;
	state = strcpyEx(state,str);
	
	state = strcpyEx(state, "|");
	
	memset(str,0,10);
	sprintf(str, "%d", getLastGPGSVData()->sat_count) ;
	state = strcpyEx(state,str);
	state = strcpyEx(state, ";");
	
	memset(str,0,10);
	sprintf(str, "%.2lf", getLastGPGSAData()->HDOP) ;
	state = strcpyEx(state,str);
	
	state = strcpyEx(state, ",");
	
	GPIO_InitTypeDef  GPIO_InitStruct;
	//PA5
	/*
	__HAL_RCC_GPIOA_CLK_ENABLE();
	GPIO_InitStruct.Pin       = GPIO_PIN_5;
  GPIO_InitStruct.Mode      = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull      = GPIO_NOPULL;
  GPIO_InitStruct.Speed     = GPIO_SPEED_FREQ_VERY_HIGH;
	
	HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);
	
	if(HAL_GPIO_ReadPin(GPIOA, GPIO_PIN_5) == GPIO_PIN_SET)
	state = strcpyEx(state, "OUP=1|");
	else
	state = strcpyEx(state, "OUP=0|");
	
	__HAL_RCC_GPIOC_CLK_ENABLE();
	GPIO_InitStruct.Pin       = GPIO_PIN_8;
  GPIO_InitStruct.Mode      = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull      = GPIO_NOPULL;
  GPIO_InitStruct.Speed     = GPIO_SPEED_FREQ_VERY_HIGH;
	
	HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);
	
	if(HAL_GPIO_ReadPin(GPIOC, GPIO_PIN_8) == GPIO_PIN_SET)
	state = strcpyEx(state, "1");
	else
	state = strcpyEx(state, "0");
	
	*/
	if(getOut1Statue())
	state = strcpyEx(state, "OUP=1|");
	else
	state = strcpyEx(state, "OUP=0|");
	
	if(getOut2Statue())
	state = strcpyEx(state, "1");
	else
	state = strcpyEx(state, "0");
	
	state = strcpyEx(state, ",");
	
	if(!strncmp((char *)get_conf_elem(WARNEFAN_CONF),"1",1))
		state = strcpyEx(state, "SW=1");
	else
		state = strcpyEx(state, "SW=0");
	
	state = strcpyEx(state, ",");
	
	if(!strncmp((char *)get_conf_elem(WARNCIRCLE_CONF),"1",1))
		state = strcpyEx(state, "ARM=1");
	else
		state = strcpyEx(state, "ARM=0");
	
#if 0	
	#if 0
	//in1+ acc
	GPIO_InitTypeDef  GPIO_InitStruct;
	__HAL_RCC_GPIOC_CLK_ENABLE();
	GPIO_InitStruct.Pin       = GPIO_PIN_4;
  GPIO_InitStruct.Mode      = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull      = GPIO_NOPULL;
  GPIO_InitStruct.Speed     = GPIO_SPEED_FREQ_VERY_HIGH;
	
	HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);
	if(HAL_GPIO_ReadPin(GPIOC, GPIO_PIN_4) == GPIO_PIN_RESET)
		state = strcpyEx(state, signal_status[0]);
	else
		state = strcpyEx(state, signal_status[1]);
	
	state = strcpyEx(state, ",");
	#endif
	
	if(in_plus_send)
	{
		state = strcpyEx(state, signal_status[3]);
	}else{
	//in2+
	GPIO_InitStruct.Pin       = GPIO_PIN_5;
  GPIO_InitStruct.Mode      = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull      = GPIO_NOPULL;
  GPIO_InitStruct.Speed     = GPIO_SPEED_FREQ_VERY_HIGH;
	
	HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);
	if(HAL_GPIO_ReadPin(GPIOC, GPIO_PIN_5) == GPIO_PIN_RESET)
		state = strcpyEx(state, signal_status[3]);
	else
		state = strcpyEx(state, signal_status[2]);
	}
	
	state = strcpyEx(state, ",");
	
	//in1-
	__HAL_RCC_GPIOB_CLK_ENABLE();
	GPIO_InitStruct.Pin       = GPIO_PIN_0;
  GPIO_InitStruct.Mode      = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull      = GPIO_NOPULL;
  GPIO_InitStruct.Speed     = GPIO_SPEED_FREQ_VERY_HIGH;
	
	HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);
	if(HAL_GPIO_ReadPin(GPIOB, GPIO_PIN_0) == GPIO_PIN_RESET)
		state = strcpyEx(state, signal_status[5]);
	else
		state = strcpyEx(state, signal_status[4]);
	
	#if 0
	state = strcpyEx(state, ",");
	
	if(in_minus_send){
		state = strcpyEx(state, signal_status[7]);
	}else{
	//in2- sos
	__HAL_RCC_GPIOA_CLK_ENABLE();
	GPIO_InitStruct.Pin       = GPIO_PIN_7;
  GPIO_InitStruct.Mode      = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull      = GPIO_NOPULL;
  GPIO_InitStruct.Speed     = GPIO_SPEED_FREQ_VERY_HIGH;
	
	HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);
	if(HAL_GPIO_ReadPin(GPIOA, GPIO_PIN_7) == GPIO_PIN_RESET)
		state = strcpyEx(state, signal_status[6]);
	else
		state = strcpyEx(state, signal_status[7]);
	}
	#endif
	//state = strcpyEx(state, ",");
#endif	
	
	
	
	return dst;

}


int8_t* signal_state_ussd(void)
{
	int8_t* state;
	state = dst;
	int8_t str[10];
	
	register GpsContext_t * gc = &GpsContext;

	if(acc_ioff_send){
		acc_ioff_send = false;
		state = strcpyEx(state, signal_status_ussd[2]);
	}else if(acc_ion_send){
		acc_ion_send = false;
		state = strcpyEx(state, signal_status_ussd[0]);
	}else if(acc_on)
	state = strcpyEx(state, signal_status_ussd[1]);
	else
	state = strcpyEx(state, signal_status_ussd[3]);
	
	state = strcpyEx(state, ",");
	
	sprintf(str, "%d", gc->enginHour) ; 
	state = strcpyEx(state,str);	
	state = strcpyEx(state, ",");
	sprintf(str, "%d", gc->mileage) ;
	state = strcpyEx(state,str);	
	state = strcpyEx(state, ",");
	
	if(in_plus_send)
		state = strcpyEx(state, "1");
	else
		state = strcpyEx(state, "0");
	if(in_minus_send)
		state = strcpyEx(state, "1");
	else
		state = strcpyEx(state, "0");
	
	state = strcpyEx(state, ",");
	
	if(getOut1Statue())
	state = strcpyEx(state, "1");
	else
	state = strcpyEx(state, "0");
	
	if(getOut2Statue())
	state = strcpyEx(state, "1");
	else
	state = strcpyEx(state, "0");
	
	state = strcpyEx(state, ",");
	
	if(!strncmp((char *)get_conf_elem(WARNEFAN_CONF),"1",1))
		state = strcpyEx(state, "1");
	else
		state = strcpyEx(state, "0");
	
	state = strcpyEx(state, ",");
	
	if(!strncmp((char *)get_conf_elem(WARNCIRCLE_CONF),"1",1))
		state = strcpyEx(state, "1");
	else
		state = strcpyEx(state, "0");
		
	return dst;

}

void in_signal(int8_t time)
{
	int32_t j;
	bool inn = false;
	bool inp = false;
	
	__HAL_RCC_GPIOB_CLK_ENABLE();
	
	GPIO_InitStruct.Pin       = GPIO_PIN_0;
  GPIO_InitStruct.Mode      = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull      = GPIO_NOPULL;
  GPIO_InitStruct.Speed     = GPIO_SPEED_FREQ_VERY_HIGH;
	
	HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);
	
	
	__HAL_RCC_GPIOC_CLK_ENABLE();
	
	GPIO_InitStruct.Pin       = GPIO_PIN_5;
  GPIO_InitStruct.Mode      = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull      = GPIO_NOPULL;
  GPIO_InitStruct.Speed     = GPIO_SPEED_FREQ_VERY_HIGH;
	
	HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);
	
	if(time < 5){
	inn_state[time] = HAL_GPIO_ReadPin(GPIOB,GPIO_PIN_0);
	inp_state[time] = HAL_GPIO_ReadPin(GPIOC,GPIO_PIN_5);
	}
	if(time == 5){
			//time = 0;
			if(inn_state[0]==0){
						inn = true;
						for(j=0;j<5;j++){
							if( inn_state[j] != 0){
									inn = false;
									break;
							}
						}
						}else
						{
							setInState(IN_N,false);
							if(in_minus){
							signalEvent();
							}
							in_minus=false;
						}
		if(inn)	{
			if(DebugEnable) DEBUG("***IN-***");
				if(worktestmode) DEBUG("IN- TEST OK");

			if(!in_minus){
				inEvent(IN_N_EVENT);
			}
			in_minus=true;
		}
		
		if(inp_state[0]==0){
						inp = true;
						for(j=0;j<5;j++){
							if( inp_state[j] != 0){
									inp = false;
									break;
							}
						}
						}else
						{
							setInState(IN_P,false);
							if(in_plus){
							signalEvent();
							}
							in_plus=false;
						}
		if(inp)	{
			if(DebugEnable) DEBUG("***IN+***");
			if(worktestmode) DEBUG("IN+ TEST OK");
								
			if(!in_plus){
				inEvent(IN_P_EVENT);
			}
			in_plus=true;
		}
			
	}
	
#if 0	
	if(HAL_GPIO_ReadPin(GPIOB,GPIO_PIN_0)== GPIO_PIN_RESET)
		{
			#if 1				
			if(DebugEnable) DEBUG("***IN-***");
				if(worktestmode) DEBUG("IN- TEST OK");

			if(!in_minus){
				inEvent(IN_N_EVENT);
			}
			in_minus=true;
			#endif				
			}else{
			setInState(IN_N,false);
			if(in_minus){
			signalEvent();
			}
			in_minus=false;
			}
							
							
	if(HAL_GPIO_ReadPin(GPIOC,GPIO_PIN_5) == GPIO_PIN_RESET)
	{
			if(DebugEnable) DEBUG("***IN+***");
			if(worktestmode) DEBUG("IN+ TEST OK");
								
			if(!in_plus){
				inEvent(IN_P_EVENT);
			}
			in_plus=true;
								
			}else{
			setInState(IN_P,false);
			if(in_plus){
				signalEvent();
			}
			in_plus=false;
			}
#endif

}

void acc_signal(int8_t time)
{
	int32_t j;
	int8_t* p;
	int8_t* pEnd;
	uint8_t acc[10];
	double high_v = 0;
	double low_v = 0;
	double start_d = 0;
	double end_d = 0;
	
	if(strncmp(get_conf_elem(SIMULATE_ACC_CONF),"1",1))
	{
	
	time_simulate_acc = 0;
		
	__HAL_RCC_GPIOC_CLK_ENABLE();
	
	GPIO_InitStruct.Pin       = GPIO_PIN_4;
  GPIO_InitStruct.Mode      = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull      = GPIO_NOPULL;
  GPIO_InitStruct.Speed     = GPIO_SPEED_FREQ_VERY_HIGH;
	
	HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);
	
	if(time < 5)
	acc_state[time] = HAL_GPIO_ReadPin(GPIOC, GPIO_PIN_4);

	if(time == 5){
			//time = 0;
			if(acc_state[0]==0){
						acc_ion = true;
						for(j=0;j<5;j++){
							if( acc_state[j] != 0){
									acc_ion = false;
									break;
							}
						}
						}else
						{
						acc_ioff = true;
						for(j=0;j<5;j++){
							if( acc_state[j] != 1){
									acc_ioff = false;
									break;
						}
						}
						}
		
			if(acc_ion && !acc_on)
			{
						if(DebugEnable) DEBUG("***ACC ON EVENT***");
						if(worktestmode) DEBUG("ACC ON TEST OK");
						setAccState(ACC_ON,true);
						acc_off = false;
						acc_ion = true;
				
						//if(!GpsNoData())
						//setGpsSendTimer();			
						accEvent(ACC_EVENT_ON);
							
						acc_ion = false;
						acc_on = true;
				#if 0
						if(!strncmp((char *)get_conf_elem(WARNSHOCK_CONF),"1",1)){
						setSirenStatus(1);
						setSirenWork(true);
						time_siren = 0;
						accOnEvent(ACC_EVENT_ON);
						}
				#endif
						
			}else
			{
						acc_ion = false;
			}

			if(acc_ioff && !acc_off)
			{
							if(DebugEnable) DEBUG("***ACC OFF EVENT***");
							if(worktestmode) DEBUG("ACC OFF TEST OK");
							setAccState(ACC_OFF,true);
							acc_on = false;
							acc_ioff = true;
							
							//if(!GpsNoData())
							//setGpsSendTimer();
							accEvent(ACC_EVENT_OFF);
							
							acc_ioff = false;
							acc_off = true;
							
			}else{
							acc_ioff = false;
			}

						memset(acc_state,0,5);
			}
	}
else{

	if(time > 5 || time % 2 != 0)
    return;	
	p = get_conf_elem(SIMULATE_ACC_CONF);
	
	if(p == NULL)
		return;
	
	p = (u8*)strchr(p,'|');
	if(p == NULL)
		return;
	p+=1;
	
	pEnd = (u8*)strchr(p,'|');
	if(pEnd == NULL)
		return;
	
	memset(acc,0,10);
	memcpy(acc,p,pEnd-p);
	
	high_v = atof(acc);
		
	p = (u8*)strchr(p,'|');
	p+=1;
	
	pEnd = (u8*)strchr(p,'|');
	if(pEnd == NULL)
		return;
	
	memset(acc,0,10);
	memcpy(acc,p,pEnd-p);
	
	low_v = atof(acc);
		
	p = (u8*)strchr(p,'|');
	p+=1;
	
	pEnd = (u8*)strchr(p,'|');
	if(pEnd == NULL)
		return;
	
	memset(acc,0,10);
	memcpy(acc,p,pEnd-p);
	
	start_d = atof(acc);
	
	p = (u8*)strchr(p,'|');
	p+=1;
	
	memset(acc,0,10);
	memcpy(acc,p,get_conf_elem(SIMULATE_ACC_CONF)+strlen(get_conf_elem(SIMULATE_ACC_CONF))+1-p);
	
	end_d = atof(acc);
	
	if(time_simulate_acc == 0){
	acc_simulate_state[time_simulate_acc] =(((double)s_uwADCxConvertedValue)*3300*19/4096)/1000;
	acc_voltage_average = acc_simulate_state[time_simulate_acc];
	time_simulate_acc++;
	return;
	}
	else{
	acc_simulate_state[1] =(((double)s_uwADCxConvertedValue)*3300*19/4096)/1000;
	acc_voltage_average = (acc_voltage_average+acc_simulate_state[1])/2;
	}
	
	if(DebugEnable) DEBUG("%d %f %f %f",time_simulate_acc,getGpsSpeed(),acc_voltage_average,acc_simulate_state[1]);
	
	if((!GpsNoData() && getGpsSpeed() > 10.0 && getMoveState()) || (GpsNoData() && getMoveState()))
	{
		time_simulate_acc = start_d;
	}else{
	#if 0
	if((acc_simulate_state[0] <= high_v && acc_simulate_state[0] >= low_v) 
		|| (acc_simulate_state[0] < low_v && acc_voltage_average > high_v)
		|| (acc_simulate_state[0] > high_v && acc_voltage_average < low_v)){
		memset(acc_simulate_state,0,2);
		time_simulate_acc = 0;
		return;
	}
	#endif
	
	if((time_simulate_acc > start_d && acc_voltage_average > high_v)){
		memset(acc_simulate_state,0,2);
		time_simulate_acc = 0;
		return;
	}
	}
	
	if(time_simulate_acc == start_d || time_simulate_acc == end_d){
			//time = 0;
			if((acc_voltage_average > high_v && time_simulate_acc == start_d) || (!GpsNoData() && getGpsSpeed() > 10.0 && getMoveState())|| (GpsNoData() && getMoveState())){
						acc_ion = true;
				/*
						for(j=0;j< start_d;j++){
							if( acc_simulate_state[j] < high_v){
									acc_ion = false;
									memset(acc_simulate_state,0,10);
									time_simulate_acc = -1;
									break;
							}
						}
				*/
						}else if(acc_voltage_average < low_v && time_simulate_acc == end_d)
						{
						if(!GpsNoData() && getGpsSpeed() > 10.0 || getMoveState())
						{
								time_simulate_acc = 0;
								memset(acc_simulate_state,0,2);
								return;
						}
						acc_ioff = true;
						/*
						for(j=0;j<end_d;j++){
							if( acc_simulate_state[j] > low_v){
									acc_ioff = false;
									break;
						}
						}
							*/
						}
		
			if(acc_ion && !acc_on)
			{
						if(DebugEnable) DEBUG("***ACC ON EVENT***");
						if(worktestmode) DEBUG("ACC ON TEST OK");
						setAccState(ACC_ON,true);
						acc_off = false;
						acc_ion = true;
				
						//if(!GpsNoData())
						//setGpsSendTimer();			
						accEvent(ACC_EVENT_ON);
							
						acc_ion = false;
						acc_on = true;
				#if 0
						if(!strncmp((char *)get_conf_elem(WARNSHOCK_CONF),"1",1)){
						setSirenStatus(1);
						setSirenWork(true);
						time_siren = 0;
						accOnEvent(ACC_EVENT_ON);
						}
				#endif
						
			}else
			{
						acc_ion = false;
			}

			if(acc_ioff && !acc_off)
			{
							if(DebugEnable) DEBUG("***ACC OFF EVENT***");
							if(worktestmode) DEBUG("ACC OFF TEST OK");
							setAccState(ACC_OFF,true);
							acc_on = false;
							acc_ioff = true;
							
							//if(!GpsNoData())
							//setGpsSendTimer();
							accEvent(ACC_EVENT_OFF);
							
							acc_ioff = false;
							acc_off = true;
							
			}else{
							acc_ioff = false;
			}
						
			}	
			if(start_d >= end_d)
			{
			if(time_simulate_acc >= start_d){
				time_simulate_acc = -1;
				memset(acc_simulate_state,0,2);
			}
			}else{
			if(time_simulate_acc >= end_d){
				   time_simulate_acc = -1;
					 memset(acc_simulate_state,0,2);
			}
			}
			time_simulate_acc++;
		
		}
}

void nosimcard_sleep(void)
{
	if(DebugEnable) DEBUG("nosimcard_sleep");
	no_simcard = true;
	osDelay(5000);
	setEvent(GSENSOR_SLEEP_EVENT);
}

void simcard_state(){
	#if 1
	__HAL_RCC_GPIOC_CLK_ENABLE();
	
	GPIO_InitStruct.Pin       = GPIO_PIN_0;
  GPIO_InitStruct.Mode      = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull      = GPIO_NOPULL;
  GPIO_InitStruct.Speed     = GPIO_SPEED_FREQ_VERY_HIGH;
	
	HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);
	
	//if(DebugEnable) DEBUG("*****************simcard status %d********************",HAL_GPIO_ReadPin(GPIOC, GPIO_PIN_0));
	if(HAL_GPIO_ReadPin(GPIOC, GPIO_PIN_0) == GPIO_PIN_RESET)
	{
		time_simcard_state++;
		if(time_simcard_state == 20){
		if((GlobalStatus & GSM_WAKEUP))
			GlobalStatus &= ~GSM_WAKEUP;
		time_simcard_state = 0;
		no_simcard = true;
		osDelay(5000);
		setEvent(GSENSOR_SLEEP_EVENT);
		}else if(time_simcard_state > 20)
			time_simcard_state = 0;
	}
	else
	{
		time_simcard_state = 0;
		no_simcard = false;
	}
	#endif
}


void sos_state(){
	
	__HAL_RCC_GPIOA_CLK_ENABLE();
	
	GPIO_InitStruct.Pin       = GPIO_PIN_7;
  GPIO_InitStruct.Mode      = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull      = GPIO_NOPULL;
  GPIO_InitStruct.Speed     = GPIO_SPEED_FREQ_VERY_HIGH;
	
	HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);
	
  if(HAL_GPIO_ReadPin(GPIOA, GPIO_PIN_7) == GPIO_PIN_RESET &&  !strncmp((char *)get_conf_elem(WARNURGENT_CONF),"1",1))
	{
		sos_press = true;
	
	}else if(HAL_GPIO_ReadPin(GPIOA, GPIO_PIN_7) == GPIO_PIN_SET &&  !strncmp((char *)get_conf_elem(WARNURGENT_CONF),"1",1))
	{
		if(sos_press)
		{
		sos_press = false;
		#if 1
		if(DebugEnable) DEBUG("***sos alarm***");
		if(worktestmode) DEBUG("SOS ALARM TEST OK");
		sosEvent(SOS_EVENT);
		#endif
		}
	}
		

}

void setAlarmState(int8_t type,bool state)
{
 switch(type)
 {
	 case ALARM_SOS:
		 sos_alarm = state;
		  break;
	 case ALARM_BATTLE_LOW:
		 batterylow_alarm = state;
			break;
	 case ALARM_CUT_DETECT:
		 cutdetect_alarm = state;
			break;
	 case ALARM_OVERSPEED:
		 overspeed_alarm = state;
			break;
	 case ALARM_SHOCK:
		 shock_alarm = state;
			break;
	 case GPS_LOST:
		 gpslost_alarm = state;
			break;
	 case GPS_FIXED:
		 gpsfix_alarm = state;
			break;
	 case ALARM_JAMMING:
		 jamming_alarm = state;
			break;
 }

}

bool getBattLowStat(void)
{
	//if(DebugEnable) DEBUG("getBattLowStat %d %d",batterylow_alarm == true,cutdetect_alarm == true);
	return (batterylow_alarm == true || cutdetect_alarm == true);
}

void setInState(int8_t type,bool state)
{
 switch(type)
 {
	 case IN_P :
			in_plus_send = state;
		  break;
	 case IN_N:
			in_minus_send = state;
			break;
 }
}

void setAccState(int8_t type,bool state)
{
 switch(type)
 {
	 case ACC_OFF :
			acc_ioff_send = state;
		  break;
	 case ACC_ON:
			acc_ion_send = state;
			break;
 }
}

bool getAccState(void)
{
	if(acc_ioff_send || acc_ion_send)
		return true;
	return false;
}

bool getAccIonState(void)
{
	if(acc_ion_send)
		return true;
	return false;
}


bool getAccIoffState(void)
{
	if(acc_ioff_send)
		return true;
	return false;
}


int8_t* alarm_state()
{
	if(sos_alarm){
		return alarm_status[1];
	}
	else if(batterylow_alarm){
		return alarm_status[3];
	}

	else if(cutdetect_alarm){
		return alarm_status[5];
	}
	else if(overspeed_alarm){
		return alarm_status[7];
	}else if(shock_alarm)
	{
		return alarm_status[9];
	}else if(gpslost_alarm)
	{
		return alarm_status[10];
	}else if(gpsfix_alarm)
	{
		return alarm_status[11];
	}else if(jamming_alarm)
	{
		return alarm_status[13];
	}
	else 
	{
		return alarm_status[0];
	}
}


int8_t* alarm_state_ussd()
{
	if(sos_alarm){
		return alarm_status_ussd[1];
	}
	else if(batterylow_alarm){
		return alarm_status_ussd[2];
	}

	else if(cutdetect_alarm){
		return alarm_status_ussd[3];
	}
	else if(overspeed_alarm){
		return alarm_status_ussd[4];
	}else if(shock_alarm)
	{
		return alarm_status_ussd[5];
	}else if(gpslost_alarm)
	{
		return alarm_status_ussd[6];
	}else if(gpsfix_alarm)
	{
		return alarm_status_ussd[7];
	}
	else 
	{
		return alarm_status_ussd[0];
	}
}

bool isAccOn()
{
	return acc_on;
	//return true;
}

bool getWakeup_Report()
{
	return wakeup_report;
}

void setWakeup_Report(bool Reported)
{
		wakeup_report = Reported;
}

void wakeup(){
  Normal_Power_Run();
}

void sleep(){
	//Lower_Power_Run();
	//StandBy();
	Stop();
}

int8_t getCutStatus(void)
{
	return time_batt_cut_report;
}

uint32_t getWorkMode(){
	return work_state;
}

int8_t getSirenStatus(void)
{
	return siren_status;
}

int8_t setSirenStatus(int8_t status)
{
	siren_status = status;
}

void setSirenWork(bool work)
{
 /*
	__HAL_RCC_GPIOC_CLK_ENABLE();
	GPIO_InitStruct.Pin       = GPIO_PIN_8;
	GPIO_InitStruct.Mode      = GPIO_MODE_OUTPUT_PP;
	GPIO_InitStruct.Pull      = GPIO_NOPULL;
	GPIO_InitStruct.Speed     = GPIO_SPEED_FREQ_VERY_HIGH;
	HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);
	
	if(work)
	{
		if(DebugEnable) DEBUG("siren work");
		HAL_GPIO_WritePin(GPIOC, GPIO_PIN_8, GPIO_PIN_SET);
	}else
	{
		if(DebugEnable) DEBUG("siren stop");
		HAL_GPIO_WritePin(GPIOC, GPIO_PIN_8, GPIO_PIN_RESET);
	}
	*/
}

int8_t* getADCVolt(void)
{
	memset(ADCVolt,0,20);
	//sprintf((char *)ADCVolt,"VOL=%d",uwADCxConvertedValue*12000/(3*(0xFF+1)));
	if(GlobalStatus & VBAT_LOW)
	sprintf((char *)ADCVolt,"VOL=%d",atoi(getVoltage()));
	else
	sprintf((char *)ADCVolt,"VOL=%d",uwADCxConvertedValue*3300*19/4096);
	return ADCVolt;
}

int8_t* getADCVolt_v(void)
{
	memset(ADCVolt_v,0,10);
	//sprintf((char *)ADCVolt_v,"%d",uwADCxConvertedValue*12000/(3*(0xFF+1)));
	sprintf((char *)ADCVolt_v,"VOL=%d",uwADCxConvertedValue*3300*19/4096);
	return ADCVolt_v;
}

int8_t* getBattLowVolt(void)
{
	memset(BattLowADCVolt,0,20);
	//sprintf((char *)BattLowADCVolt,"VOL=%d",uwADCxConvertedValue*12000/(3*(0xFF+1)));
	sprintf((char *)BattLowADCVolt,"VOL=%d",uwADCxConvertedValue*3300*19/4096);
	return BattLowADCVolt;
}

int8_t* getBattLowVoltStr(void)
{
	return BattLowADCVolt;
}

#if 0
#define FUEL_LEVEL_SENSOR_COMMAND_PREFIX_1   '$'  //0x24
#define FUEL_LEVEL_SENSOR_COMMAND_PREFIX_2   '!'  //0x21

void int2hexchars(int src, char *dst) {
    char *cHex = "0123456789ABCDEF" ; 
    char t ; 
    t = (src >> 4) & 0x0f ; 
    dst[0] = cHex[t] ; 
    t = src & 0x0f ; 
    dst[1] = cHex[t] ; 
}

void set_sensor_id(int id, char *out) {

    int checksum = 0 ; 
    int i ; 
    out[0] = FUEL_LEVEL_SENSOR_COMMAND_PREFIX_1 ; 
    out[1] = FUEL_LEVEL_SENSOR_COMMAND_PREFIX_2 ; 
    out[2] = 'I' ; 
    out[3] = 'D' ; 
    int2hexchars(id, &out[4]) ;
    for (i = 0 ; i < 6 ; i ++) {
        checksum +=  out[i] ; 
    }
    checksum &= 0xff ;  
    int2hexchars(checksum, &out[6]);
    
    out[8] = '\r' ; 
    out[9] = '\n' ; 
    out[10] = 0 ; 	
}


void sync_set_sensor_id(int id) 
{
		uint8_t tmp[20]; 
    int ret = -1; 
    set_sensor_id(1, tmp) ; 

   if(HAL_UART_Transmit(&lpUartHandle,(uint8_t *) tmp, strlen(tmp),100)== HAL_OK)
	 {
	 if(DebugEnable) DEBUG("oil consumption setid %s",tmp);
	 }else
	 {
		 if(DebugEnable) DEBUG("oil consumption setid fail");
	 }
	 
}
#endif


uint8_t* tub(uint8_t* message)
{
	int8_t* p;
	int8_t* pEnd;
	
	tubReport = true;
	
	if(strlen(message) < 30)
		return NULL;
	
	p = message;
	p  = (u8*)strchr(p,',');
	if(p  == NULL)
		return NULL;
	p+=1;
	p  = (u8*)strchr(p,',');
	if(p  == NULL)
		return NULL;
	p+=1;
	p  = (u8*)strchr(p,',');
	if(p  == NULL)
		return NULL;
	p+=1;
	pEnd  = (u8*)strchr(p,',');
	if(pEnd  == NULL)
		return NULL;
	memset(Rs232Val,0,15);
	memcpy(Rs232Val,p,pEnd-p);
	if(DebugEnable || worktestmode) DEBUG("tub oil %s %d",Rs232Val,strlen(message));
	p  = (u8*)strchr(p,',');
	if(p  == NULL)
		return NULL;
	p+=1;

	if(!strncmp(p,"00",2) || strncmp(p+2,"00",2))
	{
	if(DebugEnable) DEBUG("clean tub");
	memset(Rs232Val,0,15);
	strcpy(Rs232Val,"0");
	}
	
	return Rs232Val;
}

#if 0
void lpUartReceiveHandle(void)
{
		uint8_t fRxData[128] = {0};
		if(HAL_UART_Receive(&lpUartHandle, fRxData, 128,200)!= HAL_OK)
			LpUartInit();
				
		if(strlen(fRxData) > 0 ){
		if(DebugEnable) DEBUG("%s %d",fRxData,strlen(fRxData));
		time_lpUart = 0;
		if(!strncmp(fRxData,TUB,3)||!strncmp(fRxData,"*",1)){
				tub(fRxData);
				
				return;
		}else{
				if(strlen(fRxData) > 1){
				if(DebugEnable) DEBUG("clear tub");
				memset(Rs232Val,0,15);
			  strcpy(Rs232Val,"0");
				}
		}
		
		for(uint8_t i = 0; i < strlen(fRxData);i++)
		{
				if(fRxData[strlen(fRxData)-1] == '\n' || strlen(fRxData) == 1){
				shellData(fRxData[i]);
				if(strlen(fRxData) == 1 && fRxData[0] == 'e')
					USART_SEND_DATAS("ok\r\n",LPUART_USART);
				}
				else
				//HAL_UART_Transmit(&lpUartHandle, "ok\r\n", 4,1000);
				USART_SEND_DATAS("ok\r\n",LPUART_USART);
		}
		}else{
				time_lpUart++;
			  if(time_lpUart == 60){
				if(DebugEnable) DEBUG("timeout tub");
				time_lpUart = 0;
				memset(Rs232Val,0,15);
			  strcpy(Rs232Val,"0");
				}
		}
}
#endif

void vWorkTask(void const *pvArg)
{
	//init_timer(&workQueryTimer, WORK_QUEUE_WAIT_TICKS, handle_work_timer) ;
	int32_t xevent;
	int32_t cbctime = 0;
	int32_t signaltime = 0;
	int8_t time = 0;
	int32_t eh_time = 0;
	uint8_t aShowTime[50] = {0};
	int8_t time_off = 0;
	int8_t sleep_timeout = 0;
	int8_t onewire_time = 0;
	
	osDelay(500);
	
	while ((GlobalStatus & GPS_COMPLETE) == 0)
	{
		osDelay(1000);
	}
	
	work_restart:
	
	if(DebugEnable) DEBUG("work_restart");
	
	RTC_Config();
	
	ADC_Config();
	
	//RTC_WakeUp();
	
	initOutStatus();
	
	firstWork = true;
	
	timer_init();
	
	for(;;)
	{
		xevent = getEvent();
		
		//if(DebugEnable) DEBUG("****************SirenStatus %d %d %d*********************",getSirenStatus(),
			//HAL_GPIO_ReadPin(GPIOC, GPIO_PIN_8),time_siren);
#if 0		
		if(getSirenStatus() == 0)
		{
		time_siren = 0;
		}
		
		if(getSirenStatus() == 1 || getSirenStatus() == 2)
		{
		  time_siren++;
		}
		if(getSirenStatus() == 1 && time_siren == 90)
		{
			setSirenStatus(0);
			setSirenWork(false);
			time_siren = 0;
		}
		
		if(getSirenStatus() == 2 && time_siren == 120)
		{
			setSirenStatus(1);
			setSirenWork(true);
			time_siren = 0;
		}
#endif		
		if(gsensorok && gsensortestmode)
		{
		gsensortestmode = false;
		DEBUG("GSENSOR TEST OK");
		}
		if(time == 4 && isAccOn())
		led_state();
		else if(time_off == 20 && !isAccOn())
		led_state_off(true);
		else if(time_off == 24 && !isAccOn())
		led_state_off(false);
		//simcard_state();
		sos_state();
		acc_signal(time);
		if(!strncmp(get_conf_elem(RESERVE4_CONF),"1",1) || !NewGpioTest()){
		in_signal(time);
		}else if(strncmp(get_conf_elem(RESERVE4_CONF),"1",1) || NewGpioTest())
		{
			//if(testmode){
			if(time % 4 == 0)
			{
			New_ADC_Value(time);
			} 
			
			if(onewire_time == 0)
			{
			taskENTER_CRITICAL();
			if(DHT11_task() == 0){
				DS18B20_task();
			}
			taskEXIT_CRITICAL();
			}
			//}
		}
		
		time++;
		time_off++;
		onewire_time++;
		if(time > 6)
			time = 0;
		if(time_off > 40)
			time_off = 0;
		if(onewire_time > 120)
			onewire_time = 0;
		if(testmode && onewire_time > 30)
			onewire_time = 0;
		#if 1
		if(cbctime == 10 && (GlobalStatus & VBAT_LOW))
		{
		getBattVolt();
		}else if(cbctime > 600)
		{
			cbctime = 0;
		}
		#endif
		if(signaltime == 600 )
		{
		signaltime = 0;
		getSignal();
		}else if(signaltime == 300)
		{
		checkSignal();
		}
		else if(signaltime > 600)
		{
		signaltime = 0;
		}
		if(checkPWMOut1Setup()){
			start_pwm_out1();
			if(!checkPWMOut1Setup())
			signalEvent();
		}
		if(checkPWMOut2Setup()){
			start_pwm_out2();
			if(!checkPWMOut2Setup())
			signalEvent();
		}
		//signal_state();
		switch(xevent){
		case GSENSOR_ACTIVITY_EVENT:
		if(DebugEnable) DEBUG("GSENSOR_EVENT");
		work_state = MOTION_STATE;
		if(!strncmp((char *)get_conf_elem(WARNSHOCK_CONF),"1",1)){
		shockEvent(SHOCK_EVENT);
		#if 0
		setSirenStatus(1);
		setSirenWork(true);
		time_siren = 0;
		#endif
		}
		event = -1;
		break;
		case TIMER_EVENT:
					switch(getWorkMode()){
							case STATIC_STATE:
										break;
							case MOTION_STATE:
										break;
							case WAKEUP_START_STATE:
										break;
							case WAKEUP_SUCCESS_STATE:
										break;
							case WAKEUP_FAIL_STATE:
										break;
		}		
		break;
		case GSENSOR_SLEEP_EVENT:
			if(!strncmp(get_conf_elem(SIMULATE_ACC_CONF),"1",1) || isAccOn())
			{
			event = -1;
			continue;
			}
			if(DebugEnable) DEBUG("go to sleep,please wait");
			GlobalStatus |= GSM_SLEEP;
			sleep_timeout = 60;
			while(1)
			{
			if(getTCPConState())
			{
			if(getGpsSleep() && getGsmSleep())
				break;
			}else{
			if(getGpsSleep() || (no_simcard && getGpsSleep()))
				break;
		  }
			osDelay(1000);
			sleep_timeout--;
			if(sleep_timeout == 0){
				event = -1;
				break;
			}
			}
			if(DebugEnable) DEBUG("sleep success");
			ledoff();
			GPS_DEINIT();
		  GSM_DEINIT();
			LPUART_DEINIT();
			osDelay(1000);
			DEBUG_DEINIT();
		  sleep();
			goto work_restart;
			break;
		case DISABLE_RTC_EVENT:
			if(HAL_RTC_DeInit(&RTCHandle) != HAL_OK)
			{
			/* Initialization Error */
			if(DebugEnable) DEBUG("RTC_DeInit error");
			}
			setEvent(GSENSOR_ACTIVITY_EVENT);
			break;
		case GSENSOR_SHOCK_EVENT:
				if(DebugEnable) DEBUG("GSENSOR_SHOCK");
			//gsensor_detect();
			event = -1;
			break;
		case GEOFENCE_EVENT:
			if(DebugEnable) DEBUG("GEFENCE_EVENT");
			geofenceEvent(GEOFENCE_EVENT);
			event = -1;
			break;
		case RTC_REPORT_EVENT:
			if(DebugEnable) DEBUG("RTC_REPORT_EVENT");
			if(!rtcReport){
				rtcEvent();
				rtcReport = true;
			}
			event = -1;
			break;
	}
	
	RTC_TimeShow(aShowTime);
	//handle_timer(&workQueryTimer, CONTINUOUS_TIMER) ;
	ADC_Value(time);
	firstWork = false;
	//sync_set_sensor_id(1);
	//HAL_UART_Transmit(&lpUartHandle, "d", 1,100)  ;
	//lpUartReceiveHandle();
	if(isAccOn())
	{
	rtcReport = false;
	HAL_RTCEx_DeactivateWakeUpTimer(&RTCHandle);
	eh_time++;
	if(eh_time == 120){
	eh_time = 0;
	if(DebugEnable) DEBUG("add EH");
	calculate_whole_enginhour();
	}
	}else
	{
	eh_time = 0;
	}
  
  if(time_lpUart > 60 && !tubReport){
	if(DebugEnable) DEBUG("timeout tub");
	time_lpUart = 0;
	memset(Rs232Val,0,15);
	strcpy(Rs232Val,"0");
	}else if(time_lpUart < 60)
	{
		if(tubReport)
			time_lpUart = 0;
		if(time_lpUart == 0)
			tubReport = false;
	}
	
	osDelay(500);
	time_lpUart++;
	cbctime++;
	signaltime++;
	}
	
}


void workDebugEnable(bool enable)
{
	DebugEnable = enable;
}

