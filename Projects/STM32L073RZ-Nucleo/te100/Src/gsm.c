#include <stdio.h>
#include <stdarg.h>
#include <stdbool.h>
#include <string.h>
#include <math.h>
#include <stdlib.h> 

#include "hardware.h"
#include "FreeRTOS.h"
#include "queue.h"
#include "semphr.h"
#include "task.h"

#include "main.h"
#include "eeprom.h"
#include "gsm.h"
#include "debug.h"
#include "utility.h"
#include "gpsOne.h"
#include "ring_buffer.h"
#include "config_file.h"
#include "work.h"
#include "gps.h"
#include "bitmap_flash.h"
#include "eeprom.h"
#include "time.h"

#include <nmea/tok.h>

#define GSM_MODE_IGNORE			0
#define GSM_MODE_AT				1
#define GSM_MODE_STREAM			2
#define GSM_MODE_URC				3
#define GSM_MODE_UPDATE 4

#define GSM_PING_MS				40

#define GSM_GPRS_TIMEOUT		30000
#define GSM_TCP_TIMEOUT			30000
#define GSM_MAX_REPLAY_TIMEOUT	15000
#define GSM_INITIAL_WAIT		1000
#define GSM_COMMAND_TIMEOUT	60000

#define GSM_STATUS_TCP_ON		0x01
#define GSM_STATUS_AUTH_ERR		0x02
#define GSM_STATUS_TCP_DISABLE	0x04
#define GSM_STATUS_REGISTER		0x08


#define FTP_CFG_CM 0
#define FTP_OPEN_CM 1
#define FTP_GET_CM 2
#define FTP_GET_RAM_CM 3
#define FTP_CFG_DOWNLOAD_CM 4
#define FTP_CFG_PATH_CM 5
#define FTP_CFG_USER_CM 6
#define FTP_CFG_PASSWORD_CM 7
#define GPRS_FGCNT_CM 8
#define FTP_GSM_OPEN_CM 9
#define GPRS_CSGP_CM 10
#define FTP_CFG_CWD_CM 11
#define FTP_FILESIZE_CM 12
#define FTP_FILEOPEN_CM 13
#define FTP_FILESEEK_CM 14
#define FTP_FILEREAD_CM 15
#define FTP_FILECLOSE_CM 16



#define BUFFER_LENGTH		1024

//#define ussd_payload_format "#469*01*"
#define ussd_payload_format "*126*118*597*"

extern RTC_HandleTypeDef RTCHandle;

typedef struct {
	uint8_t	(*GprsStop)(void);
	uint8_t	(*TcpClose)(void);
	uint8_t	(*GprsSet)(void);
	uint8_t	(*GprsStart)(void);
	uint8_t	(*TcpOpen)(void);	// 4
	const int8_t	* GetIMEI;
	void	(*TcpSend)(void);
	uint8_t	(*GetFTPFile)(void);
	void	(*AudioCodec)(void);
	void   (*GsmCell)(void);
	void   (*LocalIp)(void);
	void	(*LoTcpSend)(void);
	void	(*TcpAck)(void);
	uint8_t	(*SocketState)(void);
	void  (*GsmLBS)(void);
} GsmCommands_t;

typedef struct GsmContext_s
{
	const int8_t *	RxWaitFor;
	const int8_t *	WaitAlive;

	int				WaitAliveLen;
	uint16_t		HostIndex;
	uint8_t			SkipHostLine;
	uint8_t			RxMode;
	int8_t			Data;
	uint8_t			Status;
	uint8_t			AuthErrorCount;
	uint8_t			Error;

	volatile portTickType 	LastReplyTime;
	volatile portTickType	PingInterval;
	volatile portTickType 	PingTick;

	RingBuffer_t	RB_Rx;
	RingBuffer_t	RB_Tx;

	const GsmCommands_t * Commands;
	xSemaphoreHandle	ReadySemaphore;
	xQueueHandle		TaskQueue;
	int8_t				IMEI[24];

} GsmContext_t;

/*	Private functions */
void	gsmSend		(const int8_t *text);
void  gsmSendHex (const int8_t *text,int32_t len);
void	gsmSends	(const int8_t * cmd, ...);
uint8_t	gsmCmd0		(const int8_t* text);
uint8_t	gsmCmd1		(const int8_t* text, u16 timeout);
uint8_t	gsmCmd2		(const int8_t* text, u16 timeout, const int8_t* waitFor, const int8_t* waitAlive);
uint8_t	gsmCmdX0	(const int8_t* str1, int8_t* str2, const int8_t* str3);
uint8_t	gsmCmdX2	(const int8_t* str1, int8_t* str2, const int8_t* str3, int8_t* str4, const int8_t* str5, u16 timeout, const int8_t* wait);

uint8_t	gsmSaveSMS		(const int8_t *da, const int8_t *msg);
uint8_t	gsmSendSMS		(const int8_t *da, const int8_t *msg);
void	gsmScanSMS		(GsmContext_t * gc);
uint8_t	gsmCheckPIN		(GsmContext_t *);
uint8_t	gsmIsRegister	(GsmContext_t *);
uint8_t	gsmWaitRegister	(GsmContext_t *);
void	gsmWaitNetwork	(GsmContext_t * gc);
int8_t *	gsmToken		(int8_t *);
void	gsmRxFlush		(GsmContext_t *);
uint8_t gsmGetFileFTP_UG35(void);
uint8_t gsmGetFileFTP_M35(void);

uint8_t doSendCommand(uint8_t cmd,GsmContext_t *gc,uint32_t timeout,uint32_t timestep);
uint8_t gsmGetHostLine(GsmContext_t *gc, int ms_delay);

GsmContext_t GsmContext;
GsmTask_t	GsmTask;

typedef struct Cell
{  	                                  
 uint8_t cellid[10];                                
 uint8_t MCC[10];                                
 uint8_t MNC[10];
 uint8_t LAC[10];
 uint8_t RXL[5];
 uint8_t cellid1[10];                                
 uint8_t MCC1[10];                                
 uint8_t MNC1[10];
 uint8_t LAC1[10];
 uint8_t RXL1[5];
 uint8_t cellid2[10];                                
 uint8_t MCC2[10];                                
 uint8_t MNC2[10];
 uint8_t LAC2[10];
 uint8_t RXL2[5];
 uint8_t cellid3[10];                                
 uint8_t MCC3[10];                                
 uint8_t MNC3[10];
 uint8_t LAC3[10];
 uint8_t RXL3[5];
}Cellinfo;

Cellinfo cellinfo;


typedef struct SMS_s {
			int	Index;
		uint8_t	Type;
	uint32_t	DateTime;
		int8_t	DA[16];
} SMS_t;

int8_t gsmSenderDA[20];		// Buffer for data to GSM
//int8_t gsmTxBuffer[64];		// Buffer for data to GSM

int8_t gsmRxBuffer[BUFFER_LENGTH+32];	// Buffer for data from GSM
int8_t gsmBuffer  [BUFFER_LENGTH/2];	// Buffer for host response
int8_t localreadBuffer  [BUFFER_LENGTH/4];	// Buffer for host local
int8_t localwriteBuffer  [BUFFER_LENGTH/4];
int8_t flashBuffer  [BUFFER_LENGTH/4];

int8_t			battVolt[20];
int8_t			signal[4]={0};
int8_t			ip[20];
int8_t      imsi[20];
int8_t      iccid[30] = {0};
int8_t      testSignal=0;

const int8_t GSM_AT[]				= "AT\r\n";
const int8_t GSM_E0[]				= "ATE0\r\n";
const int8_t GSM_CFUN1[]			= "AT+CFUN=1\r\n";
const int8_t GSM_CMEE[]			= "AT+CMEE=1\r\n";
const int8_t GSM_GET_CPIN[]		= "AT+CPIN?\r\n";
const int8_t GSM_CPIN_RDY[]		= "+CPIN: READY";
const int8_t GSM_NO_SIM[]			= "+CME ERROR: 10";

const int8_t GSM_CBC[]				= "AT+CBC\r\n";
const int8_t GSM_CSQ[]				= "AT+CSQ\r\n";
const int8_t GSM_ATA[]				= "ATA\r\n";
const int8_t GSM_CLCC[]       = "AT+CLCC\r\n";

const int8_t GSM_CMGF[]			= "AT+CMGF=1\r\n";
const int8_t GSM_CNMI_1[]			= "AT+CNMI=1,2,0,1,0\r\n";
const int8_t GSM_CNMI[]			= "AT+CNMI=1,0,0,0,0\r\n";
const int8_t GSM_CMGL[]			= "AT+CMGL=\"ALL\"\r\n";
const int8_t GSM_CSDH[]			= "AT+CSDH=0\r\n";

const int8_t GSM_CREG[]			= "AT+CREG?\r\n";
const int8_t GSM_CREG0[]			= "AT+CREG=0\r\n";
const int8_t GSM_COPS2[]			= "AT+COPS=2\r\n";
const int8_t GSM_COPS0[]			= "AT+COPS?\r\n";
const int8_t GSM_CREG01[]			= "+CREG: 0,1";
const int8_t GSM_CREG03[]			= "+CREG: 0,3";
const int8_t GSM_CREG05[]			= "+CREG: 0,5";

const int8_t GSM_CGREG[]			= "AT+CGREG?\r\n";
const int8_t GSM_CGREG00[]			= "+CGREG: 0,0";
const int8_t GSM_CGREG02[]			= "+CGREG: 0,2";
const int8_t GSM_CGREG03[]			= "+CGREG: 0,3";
const int8_t GSM_CGREG04[]			= "+CGREG: 0,4";



const int8_t GSM_CGATT[]			= "AT+CGATT?\r\n";
const int8_t GSM_CGATT1[]			= "+CGATT: 1";
const int8_t GSM_LINE_END[]		= "\"\r\n";
const int8_t GSM_CRLF[]			= "\r\n";

const int8_t GSM_OK[]				= "OK";
const int8_t GSM_ERROR[]			= "ERROR";
const int8_t GSM_CME_ERROR[]		= "+CME ERROR:";
const int8_t GSM_CMS_ERROR[]		= "+CMS ERROR:";
const int8_t GSM_SHUTDOWN[]		= "SHUTDOWN";
const int8_t GSM_CONNECT[]		= "CONNECT";
const int8_t GSM_NO_CARRIER[]		= "NO CARRIER";

const int8_t GSM_K0[]				= "AT+IFC=0,0\r\n";
const int8_t GSM_PSSTKI[]			= "AT*PSSTKI=0\r\n";
const int8_t GSM_GSN[]			= "AT+GSN\r\n";
const int8_t GSM_CGSN[]			= "AT+CGSN\r\n";
const int8_t GSM_ATI[]			= "ATI\r\n";
const int8_t GSM_WIPPEERCLOSE[]	= "+WIPPEERCLOSE:";
const int8_t GSM_WIPCFG0[]		= "AT+WIPCFG=0\r\n";
const int8_t GSM_IP_STOP[]		= "AT+WIPCFG=0\r\n";
const int8_t GSM_IP_START[]		= "AT+WIPCFG=1\r\n";
const int8_t GSM_GPRS_SET[]		= "AT+WIPBR=1,6\r\n";
const int8_t GSM_GPRS_APN[]		= "AT+WIPBR=2,6,11,\"";
const int8_t GSM_GPRS_USER[]		= "AT+WIPBR=2,6,0,\"";
const int8_t GSM_GPRS_PASS[]		= "AT+WIPBR=2,6,1,\"";
const int8_t GSM_GPRS_START[]		= "AT+WIPBR=4,6,0\r\n";
//const int8_t GSM_TCP_OPEN[]		= "AT+WIPCREATE=2,1,\"";
const int8_t GSM_TCP_DATA[]		= "AT+WIPDATA=2,1,1\r\n";
const int8_t GSM_TCP_READY[]		= "+WIPREADY: 2,1";
const int8_t GSM_TCP_CLOSE[]		= "AT+WIPCLOSE=2,1\r\n";

uint8_t GSM_FTP_OPEN[]		= "+QFTPOPEN: 0,0";
uint8_t GSM_FTP_OPEN_[]		= "+QFTPOPEN:0";
uint8_t GSM_FTP_GET[]    = "+QFTPGET: 0";
uint8_t GSM_FTP_GET_RAM[]    = "+QFTPGET:";
uint8_t GSM_FTP_GET_RAM_ERROR[]    = "+QFTPGET:-";
uint8_t GSM_FTP_CFG[]    = "+QFTPCFG:0";
uint8_t GSM_FTP_PATH[]   = "+QFTPPATH:";
uint8_t GSM_TCP_OPEN[]    = "+QIOPEN: 1,0";
uint8_t GSM_FTP_CWD[]    = "+QFTPCWD: 0,0";
uint8_t GSM_FTP_FSIZE[]    = "+QFTPSIZE:";
uint8_t GSM_FILE_OPEN[]  = "+QFOPEN:";


uint8_t M35_GSM_TCP_OPEN[]    = "CONNECT OK";
uint8_t M35_GSM_TCP_CLOSE[]    = "CLOSE OK";
uint8_t M35_GSM_QIDEACT[]    = "DEACT OK";

uint8_t gsm_aRxData;
bool gsm_gotoSleep = false;
bool new_sms_incoming = false;

uint32_t connectNetFail = 0;
int seeksize = 0;
uint8_t _r_index = 0;
uint8_t _s_index = 0;
uint8_t TaskRun = 0;
bool restGSM = false;
bool atDebug = false;
bool gsmtestmode = false;
bool sendGsmCell = false;
uint32_t gAddress = BOOT_ADDRESS;
uint32_t receiveData;
uint8_t nosim_wakeup = 0;


typedef  void (*pFunction)(void);
pFunction JumpToApplication;
uint32_t JumpAddress;

uint8_t file_handle[10];
bool latloncopy = false;
int8_t ackTimeout = 0;
int8_t pingTime = 0;
bool isM35 = false;
bool gsmDisconnect = false;
bool login = false;
uint8_t loginWaitresponse = 0;
extern int utc_year;
extern __IO uint32_t new_s_uwADCxConvertedValue;
int8_t module = 0;
bool ackResponse = false;
uint32_t ano_number = 0;
int8_t check_agps_flag = 0;

extern bool rtcReport;
extern bool sos_alarm;
extern bool batterylow_alarm;
extern bool cutdetect_alarm;
extern bool overspeed_alarm;
extern bool shock_alarm;
extern bool gpslost_alarm;
extern bool gpsfix_alarm;
extern bool in_plus_send;
extern bool in_minus_send;

extern uint8_t sleep_time;
extern float Temperature;
extern float Humi;
extern int8_t Rs232Val[15];
	
void gJumpToApplication(void)
{
      /* Jump to user application */
      JumpAddress = *(__IO uint32_t*) (APPLICATION_ADDRESS + 4);
      JumpToApplication = (pFunction) JumpAddress;
      /* Initialize user application's Stack Pointer */
      __set_MSP(*(__IO uint32_t*) APPLICATION_ADDRESS);
      JumpToApplication();	
}

uint8_t getTaskRunStatus(void)
{
	return TaskRun;
}

bool getCopylatlon(void)
{
	return latloncopy;
}

uint8_t GPRS_STOP_UG35(void)
{
	return gsmCmd1("AT+QIDEACT=1\r\n",40000);
}

uint8_t GPRS_TCPCLOSE_UG35(void)
{
	return gsmCmd1("AT+QICLOSE=1\r\n",GSM_GPRS_TIMEOUT);
}

uint8_t GPRS_SET_UG35(void)
{
	 char tmp[80] ; 
   strcpy(tmp, "AT+QICSGP=1,1") ; 
	 strcat(tmp, ",") ; 
	 strcat(tmp, "\"") ; 
	 strcat(tmp, (char*)SET_GRPS_APN_NAME) ;
	 strcat(tmp, "\"") ; 
	 strcat(tmp, ",");
	 strcat(tmp, "\"");
	 strcat(tmp, (char*)SET_GPRS_USERNAME) ;
	 strcat(tmp, "\"") ;
	 strcat(tmp, ",");
	 strcat(tmp, "\"");
	 strcat(tmp, (char*)SET_GPRS_PASSWORD) ;
	 strcat(tmp, "\"");
	 strcat(tmp, ",");
	 strcat(tmp, "1");
	 strcat(tmp, (char*)GSM_CRLF);
	return gsmCmd1((int8_t*)tmp,GSM_GPRS_TIMEOUT);
}

uint8_t TCP_OPEN_UG35(void)
{
	 char tmp[80] ; 
	 uint32_t wait = 2000;
	
	 register GsmContext_t *gc = &GsmContext;
	
   strcpy(tmp, "AT+QIOPEN=1,1") ; 
	 strcat(tmp, ",") ; 
	 strcat(tmp, "\"") ;
	 if(!strncmp(get_conf_elem(WORKINGMODE_CONF),"2",1))
	 strcat(tmp, "UDP") ;
	 else
	 strcat(tmp, "TCP") ;
	 strcat(tmp, "\"") ; 
	 strcat(tmp, ",");
	 strcat(tmp, "\"");
	 strcat(tmp, (char*)SET_HOST_ADDR) ;
	 strcat(tmp, "\"") ;
	 strcat(tmp, ",");
	 strcat(tmp, (char*)SET_HOST_PORT) ;
	 strcat(tmp, ",");
	 strcat(tmp, "0,1");
	 strcat(tmp, (char*)GSM_CRLF);
	 //gsmCmd2((int8_t*)tmp, GSM_TCP_TIMEOUT,NULL,GSM_CONNECT);

	 gsmCmd1((int8_t*)tmp,GSM_GPRS_TIMEOUT);
	 
	 if(!strncmp(get_conf_elem(WORKINGMODE_CONF),"2",1)){
		 while(strstr(gc->RB_Rx.Start + strlen(gsmRxBuffer)+2 ,(char *)GSM_TCP_OPEN) == NULL){
			 osDelay(2000);
			 wait -= 400;
	     if(wait <= 0)
			  break;
		 }
		 if(wait <= 0)
			return E_ERROR;
		 return E_OK;
	 }
	
	 gsmRxFlush(gc);
	 gc->RxMode = GSM_MODE_URC;
		
	 while(strstr((char *)gsmToken(NULL),(char *)GSM_TCP_OPEN) == NULL){
	 osDelay(2000);
	 wait -= 400;
	 if(wait <= 0)
			break;
	 }
	 
	 if(DebugEnable) DEBUG("%s",gsmToken(NULL));
	 
	 if(wait <= 0){
	 GPRS_TCPCLOSE_UG35();
	 memset(tmp,0,80);
	 wait = 2000;
	 strcpy(tmp, "AT+QIOPEN=1,1") ; 
	 strcat(tmp, ",") ; 
	 strcat(tmp, "\"") ; 
	 strcat(tmp, "TCP") ;
	 strcat(tmp, "\"") ; 
	 strcat(tmp, ",");
	 strcat(tmp, "\"");
	 strcat(tmp, (char*)SET_HOST_ADDR) ;
	 strcat(tmp, "\"") ;
	 strcat(tmp, ",");
	 strcat(tmp, (char*)SET_HOST_PORT) ;
	 strcat(tmp, ",");
	 strcat(tmp, "0,1");
	 strcat(tmp, (char*)GSM_CRLF);
	 //gsmCmd2((int8_t*)tmp, GSM_TCP_TIMEOUT,NULL,GSM_CONNECT);

	 gsmCmd1((int8_t*)tmp,GSM_GPRS_TIMEOUT);
	
	 gsmRxFlush(gc);
	 gc->RxMode = GSM_MODE_URC;
		
	 while(strstr((char *)gsmToken(NULL),(char *)GSM_TCP_OPEN) == NULL){
	 osDelay(4000);
	 wait -= 400;
	 if(wait <= 0)
			break;
	 }
	 if(wait <= 0)
	 return E_ERROR;
	 }
	 return E_OK;
	}


uint8_t GPRS_START_UG35(void)
{
	if(E_OK == gsmCmd1("AT+QIACT=1\r\n",60000)){
	return E_OK;
	}
	else{
	restGSM = true;
	return E_ERROR;
	}
	return E_OK;
}

void TCP_SEND_UG35(void)
{
	register GsmContext_t *gc = &GsmContext;
	gc->RxMode = GSM_MODE_STREAM;
	if(strncmp(get_conf_elem(RESERVE3_CONF),"1",1)){
	 gsmSend("AT+QISEND=1\r\n");
	 osDelay(1000);
	 gsmSend((int8_t*)strcat((char *)gsmBuffer,"\x1A\x1B"));
	}
  else{
	if(gsmBuffer[0] == '$'){
	 gsmSend("AT+QISEND=1\r\n");
	 osDelay(1000);
	 gsmSend((int8_t*)strcat((char *)gsmBuffer,"\x1A\x1B"));
	}else{
	int8_t str[20];
	int32_t length = 0;
	//length = ((uint32_t)(gsmBuffer[0] << 8 ))|((uint32_t)gsmBuffer[1])+4;
	length = gsmBuffer[0];
	if(length <= 0)
		return;
	memset(str,0,20);
	sprintf(str, "AT+QISEND=1,%d\r\n",length);
	gsmSend(str);
	osDelay(1000);
	gsmSendHex(gsmBuffer+1,length);
	gsmSend("\x1A\x1B");
	//if(!strcmp((char *)get_conf_elem(RESERVE5_CONF),"1") && length != 18 && length != 10)
		//ackResponse = true;
	}
	}
	 osDelay(500);
}


void AudioCodec_UG35(void)
{
	#if 1
	gsmCmd1("AT+QDAC=3\r\n",2000);
				
	//gsmCmd0("AT+QCLKOUT=1,2\r\n");
				
	gsmCmd1("AT+QAUDMOD=2\r\n",2000);
				
	gsmCmd1("AT+CLVL=90\r\n",2000);
				
	gsmCmd1("AT+QMIC=2,12\r\n",2000);
				
	gsmCmd1("AT+QAUDGAIN=2,100,60\r\n",2000);
				
	gsmCmd1("AT+QSIDET=-450\r\n",2000);
	
	#else
	//gsmCmd1("at+qiic=0,0x36,0x45,2,0x6000\r\n", 2000);
	//gsmCmd1("at+qiic=0,0x36,0x02,2,0xc8c8\r\n", 2000);
	//gsmCmd1("at+qiic=0,0x36,0x02,2,0x0808\r\n", 2000);
	//gsmCmd1("at+qiic=0,0x36,0x53,2,0x0000\r\n", 2000);
	gsmCmd1("at+qiic=0,0x36,0x03,2,0x0000\r\n", 2000);
	//gsmCmd1("at+qiic=0,0x36,0x4E,2,0x0000\r\n", 2000);
	//gsmCmd1("at+qiic=0,0x36,0x4F,2,0x0000\r\n", 2000);
	//gsmCmd1("at+qiic=0,0x36,0x52,2,0x0000\r\n", 2000);
	//gsmCmd1("at+qiic=0,0x36,0x51,2,0x0000\r\n", 2000);
	//gsmCmd1("at+qiic=1,0x36,0x53,2\r\n", 2000);
	//gsmCmd1("at+qiic=1,0x36,0x45,2\r\n", 2000);
	//gsmCmd1("at+qiic=1,0x36,0x02,2\r\n", 2000);
	#endif
}


void GsmCell_UG35(void)
{
	gsmSend("AT+QENG=\"servingcell\"\r\n");
}


void LocalIp_UG35(void)
{
	if (E_OK == gsmCmd1("AT+QIACT?\r\n",1000))
	{
	int8_t* item = TokenBefore(gsmToken(NULL), "OK");
	if (item != NULL)
	{
	//strcpy(ip,(char*)item);
	item = (int8_t*)strchr((char *)item,'"');
	item+=1;
	int8_t* pEnd = (int8_t*)strchr((char *)item,'"');
	memset(ip,0,20);
	memcpy(ip,item,pEnd-item);
	if(DebugEnable) DEBUG("%s",ip);
	}
	}
}

void LOCAL_TCP_SEND_UG35(void)
{
	register GsmContext_t *gc = &GsmContext;
#if 0
 	if (gsmCmd2("AT+QISEND=1\r\n", 5000, NULL, ">")  == E_OK)
	{
	if (gsmCmd2((int8_t*)strcat((char *)gsmBuffer,"\x1A\x1B"), 5000, NULL, "SEND OK")  != E_OK)
	{
	if(DebugEnable) DEBUG("not reveive SEND OK");
	}
	 //gsmRxFlush(gc);
	rb_Init(&gc->RB_Rx, gsmRxBuffer, sizeof(gsmRxBuffer)-4);
	gc->RxMode = GSM_MODE_STREAM;
  }
	else{
	if(DebugEnable) DEBUG("not reveive >");
	}
#else
		gc->RxMode = GSM_MODE_STREAM;
		if(strncmp(get_conf_elem(RESERVE3_CONF),"1",1)){
		gsmSend("AT+QISEND=1\r\n");
		osDelay(1000);
		gsmSend((int8_t*)strcat((char *)localreadBuffer,"\x1A\x1B"));
		}
		else{
		if(localreadBuffer[0] == '$'){
		gsmSend("AT+QISEND=1\r\n");
		osDelay(1000);
		gsmSend((int8_t*)strcat((char *)localreadBuffer,"\x1A\x1B"));
		}else{
		int8_t str[20];
		int32_t length = 0;
		//length = ((uint32_t)(localreadBuffer[0] << 8 ))|((uint32_t)localreadBuffer[1])+4;
		length = localreadBuffer[0];
		if(length <= 0)
		return;
		memset(str,0,20);
		sprintf(str, "AT+QISEND=1,%d\r\n",length);
		gsmSend(str);
		osDelay(1000);
		gsmSendHex(localreadBuffer+1,length);
		gsmSend("\x1A\x1B");
		}
		}
	
	 osDelay(500);
#endif
}

void TCP_ACK_UG35(void)
{
	gsmSend("AT+QISEND=1,0\r\n");
}

uint8_t SocketState_UG35(void)
{
	return gsmCmd1("AT+QISTATE=1,1\r\n",1000);
}

void GsmLBS_UG35(void)
{
	int8_t *p;
	int8_t *pEnd;
	if (E_OK == gsmCmd1("AT+QENG=\"servingcell\"\r\n",1000))
	{
		p = TokenBefore(gsmToken(NULL), "OK");
		if(p == NULL)
			return;
		p = (int8_t*)strchr((char *)p,',');
		if(p == NULL)
			return;
		p +=1;
		p = (int8_t*)strchr((char *)p,',');
		if(p == NULL)
			return;
		p +=1;
		p = (int8_t*)strchr((char *)p,',');
		if(p == NULL)
			return;
		p +=1;
		int8_t* pEnd = (int8_t*)strchr((char *)p,',');
		if(pEnd == NULL)
			return;
		memcpy(cellinfo.MCC,p,pEnd-p);
		if(DebugEnable) DEBUG("MCC %s",cellinfo.MCC);
		p = (int8_t*)strchr((char *)p,',');
		p +=1;
		pEnd = (int8_t*)strchr((char *)p,',');
		if(pEnd == NULL)
			return;
		memcpy(cellinfo.MNC,p,pEnd-p);
		if(DebugEnable) DEBUG("MNC %s",cellinfo.MNC);
	}
}

const GsmCommands_t Commands_UG35 =
{
	GPRS_STOP_UG35,
	GPRS_TCPCLOSE_UG35,
	GPRS_SET_UG35,
	GPRS_START_UG35,
	TCP_OPEN_UG35,
	"AT+GSN\r\n",
	TCP_SEND_UG35,
	gsmGetFileFTP_UG35,
	AudioCodec_UG35,
	GsmCell_UG35,
	LocalIp_UG35,
	LOCAL_TCP_SEND_UG35,
	TCP_ACK_UG35,
	SocketState_UG35,
	GsmLBS_UG35,
};



uint8_t GPRS_STOP_M35(void)
{
	#if 0
	uint32_t wait = 2000;
	gsmCmd1("AT+QIDEACT\r\n",40000);
	
	register GsmContext_t *gc = &GsmContext;
	
	gsmRxFlush(gc);
	gc->RxMode = GSM_MODE_URC;
		
	 while(strstr((char *)gsmToken(NULL),(char *)M35_GSM_QIDEACT) == NULL){
	 osDelay(2000);
	 wait -= 400;
	 if(wait <= 0)
			break;
	 }
	 if(wait <= 0)
		 return E_ERROR;
	return E_OK;//gsmCmd1("AT+QIDEACT\r\n",40000);
	 #endif
	 if (gsmCmd2("AT+QIDEACT\r\n", 1000, NULL, M35_GSM_QIDEACT)  == E_OK)
	 {
		 if(DebugEnable) DEBUG("DEACT OK");
		 return E_OK;
	 }
	 else
	 {
		 if(DebugEnable) DEBUG("DEACT ERROR");
		 return E_ERROR;
	 }
	 
}

uint8_t GPRS_START_M35(void)
{
	if(E_OK == gsmCmd1("AT+QIACT\r\n",60000)) 
	return E_OK;
	else{
	restGSM = true;
	return E_ERROR;
	}
	return E_OK;
}

uint8_t GPRS_TCPCLOSE_M35(void)
{ 
	#if 0
	uint32_t wait = 2000;
	gsmCmd1("AT+QICLOSE\r\n",1000);
	
	register GsmContext_t *gc = &GsmContext;
	
	gsmRxFlush(gc);
	gc->RxMode = GSM_MODE_URC;
		
	 while(strstr((char *)gsmToken(NULL),(char *)M35_GSM_TCP_CLOSE) == NULL){
	 osDelay(5000);
	 wait -= 400;
	 if(wait <= 0)
			break;
	 }
	 if(wait <= 0)
		 return E_ERROR;
	 return E_OK;
	 #endif
	 if (gsmCmd2("AT+QICLOSE\r\n", 1000, NULL, M35_GSM_TCP_CLOSE)  == E_OK)
	 {
		 if(DebugEnable) DEBUG("CLOSE OK");
		 return E_OK;
	 }
	 else
	 {
		 if(DebugEnable) DEBUG("CLOSE ERROR");
		 return E_ERROR;
	 }
}

uint8_t GPRS_SET_M35(void)
{
	 char tmp[80] ; 
   strcpy(tmp, "AT+QIREGAPP=") ;  
	 strcat(tmp, "\"") ; 
	 strcat(tmp, (char*)SET_GRPS_APN_NAME) ;
	 strcat(tmp, "\"") ; 
	 strcat(tmp, ",");
	 strcat(tmp, "\"");
	 strcat(tmp, (char*)SET_GPRS_USERNAME) ;
	 strcat(tmp, "\"") ;
	 strcat(tmp, ",");
	 strcat(tmp, "\"");
	 strcat(tmp, (char*)SET_GPRS_PASSWORD) ;
	 strcat(tmp, "\"");
	 strcat(tmp, (char*)GSM_CRLF);
	return gsmCmd1((int8_t*)tmp,GSM_GPRS_TIMEOUT);
}


uint8_t TCP_OPEN_M35(void)
{
	 char tmp[80] ; 
	 uint32_t wait = 2000;
	
	 register GsmContext_t *gc = &GsmContext;
	
   strcpy(tmp, "AT+QIOPEN=") ; 
	 strcat(tmp, "\"") ;
	 if(!strncmp(get_conf_elem(WORKINGMODE_CONF),"2",1))
	 strcat(tmp, "UDP") ;
   else		 
	 strcat(tmp, "TCP") ;
	 strcat(tmp, "\"") ; 
	 strcat(tmp, ",");
	 strcat(tmp, "\"");
	 strcat(tmp, (char*)SET_HOST_ADDR) ;
	 strcat(tmp, "\"") ;
	 strcat(tmp, ",");
	 strcat(tmp, (char*)SET_HOST_PORT) ;
	 strcat(tmp, (char*)GSM_CRLF);
	 //gsmCmd2((int8_t*)tmp, GSM_TCP_TIMEOUT,NULL,GSM_CONNECT);

	 gsmCmd1((int8_t*)tmp,GSM_GPRS_TIMEOUT);
	 
	 if(!strncmp(get_conf_elem(WORKINGMODE_CONF),"2",1)){
		 while(strstr(gc->RB_Rx.Start + strlen(gsmRxBuffer)+2 ,(char *)M35_GSM_TCP_OPEN) == NULL){
			 osDelay(2000);
			 wait -= 400;
	     if(wait <= 0)
			  break;
		 }
		 if(wait <= 0)
			return E_ERROR;
		 return E_OK;
	 }
		 	
	 gsmRxFlush(gc);
	 gc->RxMode = GSM_MODE_URC;
		
	 while(strstr((char *)gsmToken(NULL),(char *)M35_GSM_TCP_OPEN) == NULL){
	 osDelay(4000);
	 wait -= 400;
	 if(wait <= 0)
			break;
	 }
	 
	 if(DebugEnable) DEBUG("%s",gsmToken(NULL));
	 
	 if(wait <= 0)
		 return E_ERROR;
	 return E_OK;

 }

 void TCP_SEND_M35(void)
{
	register GsmContext_t *gc = &GsmContext;
#if 0
 	if (gsmCmd2("AT+QISEND\r\n", 5000, NULL, ">")  == E_OK)
	{
	 gsmSend((int8_t*)strcat((char *)gsmBuffer,"\x1A\x1B"));
	 gsmRxFlush(gc);
	 gc->RxMode = GSM_MODE_URC;
	 while(strstr((char *)gsmToken(NULL),"SEND OK") == NULL){
	 }
	 //gsmRxFlush(gc);
	}
#endif
	gc->RxMode = GSM_MODE_STREAM;
	
	if(strncmp(get_conf_elem(RESERVE3_CONF),"1",1)){
	 gsmSend("AT+QISEND\r\n");
	 osDelay(3000);
	 gsmSend((int8_t*)strcat((char *)gsmBuffer,"\x1A\x1B"));
	}
  else{
	if(gsmBuffer[0] == '$'){
	 gsmSend("AT+QISEND\r\n");
	 osDelay(3000);
	 gsmSend((int8_t*)strcat((char *)gsmBuffer,"\x1A\x1B"));
	}else{
	int8_t str[20];
	int32_t length = 0;
	//length = ((uint32_t)(gsmBuffer[0] << 8 ))|((uint32_t)gsmBuffer[1])+4;
	length = gsmBuffer[0];
	if(length <= 0)
		return;
	memset(str,0,20);
	sprintf(str, "AT+QISEND=%d\r\n",length);
	gsmSend(str);
	osDelay(3000);
	gsmSendHex(gsmBuffer+1,length);
	gsmSend("\x1A\x1B");
	//if(!strcmp((char *)get_conf_elem(RESERVE5_CONF),"1") && length != 18 && length != 10)
		//ackResponse = true;
	}
	}
	 osDelay(1000);
}


void AudioCodec_M35(void)
{
	gsmCmd1("AT+QAUDCH=0\r\n",2000);
	
	gsmCmd1("AT+CLVL=90\r\n",2000);
	
	//gsmCmd1("AT+QMIC=2,12\r\n",2000);
	
	gsmCmd1("AT+QMIC=0,12\r\n",2000);
	
	//gsmCmd1("AT+QAUDLOOP=1,2\r\n",2000);

}

void GsmCell_M35(void)
{
	gsmSend("AT+QENG=2,1\r\n");
}
 
void LocalIp_M35(void)
{
 register GsmContext_t *gc = &GsmContext;
 gsmRxFlush(gc);
 gc->RxMode = GSM_MODE_STREAM;
 gsmSend("AT+QILOCIP\r\n");
 while(gsmGetHostLine(gc, 2000) == E_OK){
 if(strlen(gsmBuffer) == 0)
	 continue;
 memset(ip,0,20);
 memcpy(ip,gsmBuffer,strlen(gsmBuffer));
 if(DebugEnable) DEBUG("%s",ip,strlen(gsmBuffer));
 }
}


void LOCAL_TCP_SEND_M35(void)
{
	register GsmContext_t *gc = &GsmContext;
#if 0
 	if (gsmCmd2("AT+QISEND\r\n", 5000, NULL, ">")  == E_OK)
	{
	 gsmSend((int8_t*)strcat((char *)gsmBuffer,"\x1A\x1B"));
	 gsmRxFlush(gc);
	 gc->RxMode = GSM_MODE_URC;
	 while(strstr((char *)gsmToken(NULL),"SEND OK") == NULL){
	 }
	 //gsmRxFlush(gc);
	}
#endif
	gc->RxMode = GSM_MODE_STREAM;
	
	if(strncmp(get_conf_elem(RESERVE3_CONF),"1",1)){
	 gsmSend("AT+QISEND\r\n");
	 osDelay(3000);
	 gsmSend((int8_t*)strcat((char *)localreadBuffer,"\x1A\x1B"));
	}
  else{
	if(localreadBuffer[0] == '$'){
	 gsmSend("AT+QISEND\r\n");
	 osDelay(3000);
	 gsmSend((int8_t*)strcat((char *)localreadBuffer,"\x1A\x1B"));
	}else{
	int8_t str[20];
	int32_t length = 0;
	length = localreadBuffer[0];
	if(length <= 0)
		return;
	memset(str,0,20);
	sprintf(str, "AT+QISEND=%d\r\n",length);
	gsmSend(str);
	osDelay(3000);
	gsmSendHex(localreadBuffer+1,length);
	gsmSend("\x1A\x1B");
	}
	}
	osDelay(1000);
}

void TCP_ACK_M35(void)
{
	gsmSend("AT+QISACK\r\n");
}

uint8_t SocketState_M35(void)
{
	return gsmCmd1("AT+QISTAT\r\n",1000);
}


void GsmLBS_M35(void)
{
	int8_t *p;
	int8_t *pEnd;
	uint32_t wait = 2000;
	
	register GsmContext_t *gc = &GsmContext;
	
	if (E_OK == gsmCmd1("AT+QENG=2,0\r\n",1000))
	{
		
		while(strstr(gc->RB_Rx.Start + strlen(gsmRxBuffer)+2 ,"+QENG:") == NULL){
			 osDelay(1000);
			 wait -= 400;
	     if(wait <= 0)
			  break;
		 }
		 if(wait <= 0){
			gsmCmd1("AT+QENG=0,0\r\n",1000);
			return;
		 }
		
		p = gc->RB_Rx.Start + strlen(gsmRxBuffer)+2;
		if(p == NULL){
			gsmCmd1("AT+QENG=0,0\r\n",1000);
			return;
		}
		p = (int8_t*)strchr((char *)p,',');
		if(p == NULL){
			gsmCmd1("AT+QENG=0,0\r\n",1000);
			return;
		}
		p +=1;
		int8_t* pEnd = (int8_t*)strchr((char *)p,',');
		if(pEnd == NULL){
			gsmCmd1("AT+QENG=0,0\r\n",1000);
			return;
		}
		memcpy(cellinfo.MCC,p,pEnd-p);
		if(DebugEnable) DEBUG("MCC %s",cellinfo.MCC);
		p = (int8_t*)strchr((char *)p,',');
		p +=1;
		pEnd = (int8_t*)strchr((char *)p,',');
		if(pEnd == NULL){
			gsmCmd1("AT+QENG=0,0\r\n",1000);
			return;
		}
		memcpy(cellinfo.MNC,p,pEnd-p);
		if(DebugEnable) DEBUG("MNC %s",cellinfo.MNC);
		gsmCmd1("AT+QENG=0,0\r\n",1000);
	}
}

const GsmCommands_t Commands_M35 =
{
	GPRS_STOP_M35,
	GPRS_TCPCLOSE_M35,
	GPRS_SET_M35,
	GPRS_START_M35,
	TCP_OPEN_M35,
	"AT+GSN\r\n",
	TCP_SEND_M35,
	gsmGetFileFTP_M35,
	AudioCodec_M35,
	GsmCell_M35,
	LocalIp_M35,
	LOCAL_TCP_SEND_M35,
	TCP_ACK_M35,
	SocketState_M35,
	GsmLBS_M35,
};

uint8_t GsmIsRegister(void)
{
	return (GsmContext.Status & GSM_STATUS_REGISTER);
}

/*******************************************************************************
* Function Name :	GsmAddTask
* Description   :	Add task to queue to back
*******************************************************************************/
uint8_t GsmAddTask(GsmTask_t * task)
{
	if (GsmContext.TaskQueue != NULL)
	{
		if (xQueueSend(GsmContext.TaskQueue, task, 5000) == pdPASS)
			return 1;
	}
	return 0;
}

/*******************************************************************************
* Function Name :	GsmAddTaskTop
* Description   :	Add task to queue to top
*******************************************************************************/
uint8_t GsmAddTaskAlways(GsmTask_t * task)
{
	if (GsmContext.TaskQueue != NULL)
	{
		while (xQueueSend(GsmContext.TaskQueue, task, 500) != pdPASS)
			xQueueReceive(GsmContext.TaskQueue, NULL, 500);
		return 1;
	}
	return 0;
}

/*******************************************************************************
* Function Name :	GsmIMEI
* Description   :	Return pointer to IMEI string
*******************************************************************************/
int8_t * GsmIMEIIMSI(void)
{
	int8_t imei_imsi[50] = {0};
	if(!strncmp(get_conf_elem(WORKINGMODE_CONF),"0",1)){
		strcat(imei_imsi,GsmContext.IMEI);
		strcat(imei_imsi,"&");
		strcat(imei_imsi,imsi);
		return imei_imsi;
	}else
	return &GsmContext.IMEI[0];
}

/*******************************************************************************
* Function Name :	GsmIMEI
* Description   :	Return pointer to IMEI string
*******************************************************************************/
int8_t * GsmIMEI(void)
{
	if(GsmContext.IMEI[0] != 0)
	return &GsmContext.IMEI[0];
	else
	return get_conf_elem(SN_CONF);
}


/*******************************************************************************
* Function Name :	GsmIMSI
* Description   :	Return pointer to IMSI string
*******************************************************************************/
int8_t * GsmIMSI(void)
{
	return imsi;
}

/*******************************************************************************
* Function Name :	GsmICCID
* Description   :	Return pointer to IMSI string
*******************************************************************************/
int8_t * GsmICCID(void)
{
	return iccid;
}


void recordSn(){
//record sn to config
	if(!strncmp((char*)get_conf_elem(SN_CONF),"000000000000000",strlen("000000000000000"))){
		update_conf_elem(SN_CONF,(uint8_t*)GsmContext.IMEI);
	}
}

void initGsmUart(void)
{
	#if 0
	if(HAL_UART_DeInit(&gsmUartHandle) != HAL_OK)
  {
    if(DebugEnable) DEBUG("gsm UART_DeInit error");
  } 
	
  if(HAL_UART_Init(&gsmUartHandle) != HAL_OK)
  {
    if(DebugEnable) DEBUG("gsm UART_Init error");
  }  
	
	if(HAL_UART_Receive_IT(&gsmUartHandle, (uint8_t *)&gsm_aRxData, RXBUFFERSIZE) != HAL_OK)
  {
    if(DebugEnable) DEBUG(" gsm UART_Receive_IT error");
  }
	#endif
}



/*******************************************************************************
* Function Name	:	GsmDeInit
* Description	:	Deinitialize GSM
*******************************************************************************/
void GsmDeInit(void)
{
	//HAL_UART_DeInit(&gsmUartHandle);
	GSM_PWR_OFF();
	
}
/*******************************************************************************
* Function Name :	GsmInit
* Description   :	Initialize GSM hardware,
*					check communication,
*					read IMEI code
*******************************************************************************/
uint8_t GsmInit(void)
{
	uint8_t result = E_GSM_NOT_READY;
	uint8_t retry;
	int8_t *item;

	GsmContext.Commands = &Commands_UG35;

	GSM_PWR_ON();
	
	retry = 3;
	
	while (retry-- != 0)
	{
		osDelay(1000);
	}
	
	//rb_Init(&GsmContext.RB_Tx, gsmTxBuffer, sizeof(gsmTxBuffer));

	#if 0
	
	gsmUartHandle.Instance        = GSM_USART; 
  gsmUartHandle.Init.BaudRate   = 115200; 
  gsmUartHandle.Init.WordLength = UART_WORDLENGTH_8B; 
  gsmUartHandle.Init.StopBits   = UART_STOPBITS_1; 
  gsmUartHandle.Init.Parity     = UART_PARITY_NONE; 
  gsmUartHandle.Init.HwFlowCtl  = UART_HWCONTROL_NONE; 
  gsmUartHandle.Init.Mode       = UART_MODE_TX_RX; 
	
	initGsmUart();
	
	#endif
	
	MX_USART4_UART_Init();
	
	GsmContext.RxMode = GSM_MODE_IGNORE;
	
	GSM_DTR_LOW();
	GSM_RTS_LOW();
	
	memset(iccid,0,30);
	
	retry = 8;
	while(retry-- != 0)
	{
		osDelay(500);
		if (E_OK == gsmCmd1(GSM_AT, 2000) &&
			E_OK == gsmCmd0(GSM_E0) &&
			E_OK == gsmCmd0(GSM_K0)
			)
		{
			gsmCmd0("AT+IPR=115200\r\n");
			
			if (E_OK == gsmCmd0(GSM_ATI))
			{
				item = TokenBefore(gsmToken(NULL), "OK");
				if(DebugEnable) DEBUG("item:%s",item);
				if (item != NULL && strncmp((char*)item, "Revision: M35FAR01A08",13) == 0){
					GsmContext.Commands = &Commands_M35;
					isM35 = true;
					module = 0;
				}
				else if(item != NULL && strncmp((char*)item, "Revision: UG35ENAR01A09E1G",13) == 0){
					GsmContext.Commands = &Commands_UG35;
					module = 1;
				}else
				{
				GsmContext.Commands = &Commands_UG35;
					module = 2;
				}
			}
			gsmCmd0("ats0=2\r\n");
			
			if(!strcmp((char *)get_conf_elem(RESERVE8_CONF),"1"))
			gsmCmd0("AT+QJDR=1\r\n");
			
			//gsmCmd0("AT+QJDCFG=\"period\",5\r\n");
			
			(GsmContext.Commands->AudioCodec)();
		
			if (E_OK == gsmCmd0(GSM_CMEE)
			&&	E_OK == gsmCmd0(GSM_CREG0)
			&&	E_OK == gsmCmd0(GSM_CFUN1)
				)
			{
				//gsmCmd1("AT+QCFG=\"nwscanmode\",0,1\r\n",2000);
				gsmCmd0(GSM_COPS0);
			
				//if (GsmContext.Commands->PSSSTK != NULL)
					//gsmCmd1(GsmContext.Commands->PSSSTK, 5000);
				
				if (E_OK == gsmCmd0(GsmContext.Commands->GetIMEI))
				{
					item = TokenBefore(gsmToken(NULL), "OK");
					if (item != NULL && (strlen((char*)item) < (sizeof(GsmContext.IMEI) - 1)))
					{
						strcpy((char*)&GsmContext.IMEI[0], (char*)item);
						//update_conf_elem(SN_CONF,GsmContext.IMEI);
						recordSn();
						result = E_OK;
						break;
					}
				}
				
			}
		}
	}
	return result;
}

/*******************************************************************************
* Function Name	:	gsmCmdX0
* Description	:
*******************************************************************************/
uint8_t gsmCmdX0(const int8_t* str1, int8_t* str2, const int8_t* str3)
{
	gsmSends(str1, str2, NULL);
	return gsmCmd0(str3);
}

/*******************************************************************************
* Function Name	:	gsmCmdX2
* Description	:
*******************************************************************************/
uint8_t gsmCmdX2(const int8_t* str1,
			int8_t* str2,
			const int8_t* str3,
			int8_t* str4,
			const int8_t* str5,
			u16 timeout, const int8_t* wait)
{
	gsmSends(str1, str2, str3, str4, NULL);
	return gsmCmd2(str5, timeout, wait, NULL);
}

/*******************************************************************************
* Function Name	:	gsmWaitNetwork
* Description	:	Wait Network Registration
*******************************************************************************/
void gsmWaitNetwork(GsmContext_t * gc)
{
	int retry;
	int restart = 0;
	gc->Status &= ~GSM_STATUS_TCP_ON;
	while(1)
	{
		retry = 20;
		
		if(DebugEnable) DEBUG("gsmWaitNetwork");
		
		if(restart > 5 ){
			if(isAccOn())
			SystemRestart();
		}

		while (--retry != 0)
		{
			if(restGSM)
			{
			restGSM = false;
			GsmDeInit();
			}
			//GsmDeInit();
			if (GsmInit() != E_OK)
			{
				ShowError(ERROR_GSM_NOT_READY);
				//GsmDeInit();
				osDelay(5000);
				continue;
			}

			if (gsmWaitRegister(gc) == E_OK)
			{
				(GsmContext.Commands->GsmLBS)();
				gsmScanSMS(gc);
				return;
			}
#if 0
			if (gc->Error == E_SIM_NOT_READY)
				ShowError(ERROR_GSM_SIM_NOT_READY);
			else if (gc->Error == E_NOT_REGISTER)
			{
				ShowError(ERROR_GSM_NOT_REGISTER);
			}
			else if (gc->Error == E_REG_DENIED)
			{
				//gsmCmd0("AT#MONI\r\n");
				osDelay(1000);
				ShowError(ERROR_GSM_REG_DENIED);
			}
			else if (gc->Error == E_NO_SIM)
				ShowError(ERROR_GSM_NO_SIM);
			else
				ShowError(ERROR_GSM_OTHER);
#endif
		}
		
		restart++;
		GsmDeInit();
		osDelay(5000);
	}
}

/*******************************************************************************
* Function Name	:	gsmDisconnectTCP
* Description	:	Disconnect TCP
*******************************************************************************/
void gsmDisconnectTCP(GsmContext_t * gc)
{
	gc->Status &= ~GSM_STATUS_TCP_ON;
	osDelay(1000);
	if (gsmCmd2("+++", 10000, GSM_NO_CARRIER, NULL) == E_OK
	||	gsmCmd0(GSM_AT) == E_OK
		)
	{
		(*gc->Commands->TcpClose)();
		//(*gc->Commands->GprsStop)();
		osDelay(500);
		if (gsmIsRegister(gc) != E_OK)
		{
			GsmDeInit();
			gsmWaitNetwork(gc);
		}
	}
	else
	{
		GsmDeInit();
		gsmWaitNetwork(gc);
	}
		if(GG_FwSmsIndex != -2){
		GsmTask.Command = GSM_TASK_CMD_LOGIN ;
	  GsmAddTask(&GsmTask);
		}
}

uint8_t gsmHostUndefine(void)
{
	return (SET_HOST_ADDR[0] == 0 ? 1 : 0);
}

bool checkSocketState(GsmContext_t * gc)
{
	int8_t *item;
	int8_t retry = 5;
	int32_t wait = 2000;
	
	while(E_OK == (*gc->Commands->SocketState)())
	{
		if(!isM35){
		item = TokenBefore(gsmToken(NULL), "OK");
		if(DebugEnable) DEBUG("qistate: %s",item);
		item = (int8_t*)strchr((char *)item,',');
		item +=1;
		item = (int8_t*)strchr((char *)item,',');
		item +=1;
		item = (int8_t*)strchr((char *)item,',');
		item +=1;
		item = (int8_t*)strchr((char *)item,',');
		item +=1;
		item = (int8_t*)strchr((char *)item,',');
		item +=1;
		if(!strncmp(item,"3",1))
			return true;
		
	}else
	{
		
	 return true;
	 #if 0
	 gsmRxFlush(gc);
	 gc->RxMode = GSM_MODE_URC;
		
	 while(strstr((char *)gsmToken(NULL),"STATE: CONNECT OK") == NULL){
	 osDelay(1000);
	 wait -= 200;
	 if(wait <= 0){
			if(DebugEnable) DEBUG("qistate: %s",gsmToken(NULL));
			return false;
	 }
	 }
		if(wait > 0){
		if(DebugEnable) DEBUG("qistate: %s",gsmToken(NULL));
		return true;
		}
	 #endif
	}
		
	retry--;
	if(retry == 0)
	return false;
	osDelay(100);
	}
	return false;
}

/*******************************************************************************
* Function Name	:	gsmConnectUSSDTCPUDP
* Description	:	Connect USSDTCPUDP
*******************************************************************************/
uint8_t gsmConnectUSSDTCPUDP(GsmContext_t * gc)
{
	uint8_t last_error;
	
	if (gc->Status & GSM_STATUS_TCP_ON)
	{
		gc->Error = E_OK;
	}
	else
	{
		
		if(!strncmp(get_conf_elem(WORKINGMODE_CONF),"0",1)){
		gc->Status |= GSM_STATUS_TCP_ON;
		gc->RxMode = GSM_MODE_IGNORE;
		gsmRxFlush(gc);
		gc->RxMode = GSM_MODE_STREAM;
		GsmTask.Command = GSM_TASK_CMD_LOGIN;
		GsmAddTask(&GsmTask);
		GlobalStatus |= GSM_LOGIN;
		gc->Error = E_OK;
		return gc->Error;
		}
		
		if ((*gc->Commands->GprsStop)() == E_TIMEOUT)
			gsmDisconnectTCP(gc);

		gsmScanSMS(gc);

		if (gsmHostUndefine())
			return (gc->Error = E_GSM_TCP_NOT_OPEN);

		if (	E_OK == gsmWaitRegister(gc) 
		    && E_OK == (*gc->Commands->GprsSet)()
			  && E_OK == (*gc->Commands->GprsStart)()
		    && E_OK == (*gc->Commands->TcpOpen)()
				&& E_OK == gsmCmd0(GSM_CNMI_1) 
				&& E_OK == gsmCmd0("ats0=0\r\n")
				&& E_OK == gsmCmd0("at+qisde=0\r\n")
				&& checkSocketState(gc)
			//&&	E_OK == gsmCmd2(gc->Commands->TcpData, GSM_TCP_TIMEOUT,NULL, ">")
			)
		{
			(*gc->Commands->LocalIp)();
			if(strncmp(get_conf_elem(RESERVE3_CONF),"1",1)){
			gc->Status |= GSM_STATUS_TCP_ON;
			}else{
			login = false;
			ackResponse = false;
			}
			gc->RxMode = GSM_MODE_IGNORE;
			gsmRxFlush(gc);
			gc->RxMode = GSM_MODE_STREAM;
			gc->Error = E_OK;
			connectNetFail = 0;
			if(strncmp(get_conf_elem(RESERVE3_CONF),"1",1)){
			if(strlen(localwriteBuffer) > 0)
			{
			if(DebugEnable) DEBUG("***reconnect send");
			 if(!isM35){
			 gsmSend("AT+QISEND=1\r\n");
			 osDelay(1000);
			 gsmSend((int8_t*)strcat((char *)localwriteBuffer,"\x1A\x1B"));
	     osDelay(500);
			 }else
			 { 
				gsmSend("AT+QISEND\r\n");
				osDelay(3000);
				gsmSend((int8_t*)strcat((char *)localwriteBuffer,"\x1A\x1B"));
				osDelay(1000);
			 }
			}
			
			//GsmTask.Command = GSM_TASK_CMD_LOGIN;
			//GsmAddTask(&GsmTask);
			//GlobalStatus |= GSM_LOGIN;
			}else{
			//if((GlobalStatus & GSM_LOGIN) == 0){
			
			}
			//}
		}
		else
		{
			connectNetFail++;
			last_error = GsmContext.Error;
			if(E_OK ==(*gc->Commands->TcpClose)())
			{
			}
			if(E_OK ==(*gc->Commands->GprsStop)())
			{
			gsmRxFlush(gc);
			gc->RxMode = GSM_MODE_STREAM;
			}
			gc->Error = last_error;
			
		}
	}
	
	return gc->Error;
}

/*******************************************************************************
* Function Name	:	gsmDeleteSMS
* Description	:
*******************************************************************************/
uint8_t gsmDeleteSMS(int index)
{
	sprintf((char*)gsmBuffer, "AT+CMGD=%d\r\n", index);
	return gsmCmd0(gsmBuffer);
}

/*******************************************************************************
* Function Name	:	gsmGetHostLine
* Description	:
*******************************************************************************/
uint8_t gsmGetHostLine(GsmContext_t *gc, int ms_delay)
{
	int8_t data;
	int retry;

	gc->HostIndex = 0;
	gc->SkipHostLine = 0;
	gc->RxMode = GSM_MODE_STREAM;
	retry = ms_delay;
	int8_t ACK = 0;

	while (retry != 0)
	{
		while (rb_Get(&gc->RB_Rx, &data) != 0)
		{
			retry = ms_delay;
			if (gc->SkipHostLine == 1)
			{
				if (data == 0)	// Check for End Of Line
				{
					gc->SkipHostLine = 0;
					gc->HostIndex = 0;
				}
				continue;
			}
			if (gc->HostIndex >= (sizeof(gsmBuffer) - 2))
			{	// Line too long, ignore it
				gc->SkipHostLine = 1;
				continue;
			}
			gsmBuffer[gc->HostIndex++] = data;
			if(!strncmp(get_conf_elem(RESERVE3_CONF),"1",1)){
				if (ACK == 0 && data == 0)
							return E_OK;
				if(ACK >0 && ACK < 3)
					ACK++;
				if (data == 0x7B)
					ACK++;
				if(ACK == 3){
					gsmBuffer[gc->HostIndex++] = 0;
					return E_OK;
				}
			}else{
			if (data == 0)
				return E_OK;
			}
		}
		osDelay(5);
		retry -= 5;
	}
	return E_TIMEOUT;
}

/*******************************************************************************
* Function Name	:	gsmSendTaskCommand
* Description	:	Send message to host
*******************************************************************************/
int32_t D2I_Int, D2I_Frac;
const double D2I_CONST[] = {
	0.5,		1.0,
	0.05,		10.0,
	0.005,		100.0,
	0.0005,		1000.0,
	0.00005,	10000.0,
	0.000005,	100000.0,
	0.0000005,	1000000.0
};
void D2I_6D(double val, uint8_t digits)
{
	double  dbl2int;
	digits *= 2;
	D2I_Frac = (uint32_t)((modf(val, &dbl2int) + D2I_CONST[digits]) * D2I_CONST[digits + 1]);
	D2I_Int = (uint32_t)dbl2int;
}

/*******************************************************************************
* Function Name	:	gsmSendTaskCommand
* Description	:	Send message to host
*******************************************************************************/
int8_t * D2IToString(int8_t *dst, double value, uint8_t digits)
{
	D2I_6D(value, digits);
	dst = itoa(dst, D2I_Int, 0);
	*dst++ = '.';
	dst = itoa(dst, D2I_Frac, digits);
	*dst++ = ',';
	return dst;
}

nmeaGPRMC* getGPRMCAData(void)
{
	register GsmTask_t * task = &GsmTask;
	return &(task->GPRMC);
}

int frame(int8_t* dst,int cmd,bool local)
{
	int size;
	nmeaGPRMC * rmc;
	RTC_TimeTypeDef rtc;
	RTC_DateTypeDef date;
	GsmTask_t * task = &GsmTask;
	uint8_t utc = 0;
	
		/* Get the RTC current Time */
  HAL_RTC_GetTime(&RTCHandle, &rtc, RTC_FORMAT_BIN);
	
	HAL_RTC_GetDate(&RTCHandle, &date, RTC_FORMAT_BIN);	

	//adjust_timezone_time(&rtc);
	
	if(cmd == GSM_TASK_CMD_GPS)
	rmc = &(task->GPRMC);//getGPRMCData();//&(task->GPRMC);
	else{
	latloncopy = true;
	rmc = getGPRMCData(cmd);
	}
	
	#if ( USE_AVERAGE == 1 || USE_KALMAN == 1 )
				if(rmc->lat == 0)
					rmc->ns = '0';
				else
				rmc->ns = ((rmc->lat > 0) ? 'N' : 'S');
				
				if(rmc->lon == 0)
					rmc->ew = '0';
				else
				rmc->ew = ((rmc->lon > 0) ? 'E' : 'W');
				
				if(cmd == GSM_TASK_CMD_GPS){
				rmc->lat = nmea_degree2ndeg(fabs(rmc->lat));
				rmc->lon = nmea_degree2ndeg(fabs(rmc->lon));
				}
 #endif
			//if(rmc->lat == 0 && rmc->lon == 0){
			//*dst++ =',';*dst++ = ',';
			//*dst++ =',';*dst++ = ',';
			//}else{
			dst = D2IToString(dst, rmc->lat, 6);	*dst++ = rmc->ns;	*dst++ = ',';
			dst = D2IToString(dst, rmc->lon, 6);	*dst++ = rmc->ew;	*dst++ = ',';
			//}
			//*dst++ = ',';
			
			dst = D2IToString(dst, rmc->declination, 1);
			if(isAccOn())
			dst = D2IToString(dst, rmc->speed, 1);
			else
			dst = strcpyEx(dst, "0.0,");
			dst = D2IToString(dst, rmc->direction, 1);
			latloncopy =false;
			if(cmd == GSM_TASK_CMD_GPS && rmc->utc.year != 0)
			{
			dst = itoa(dst, rmc->utc.day, 2);
			dst = itoa(dst, rmc->utc.mon + 1, 2);
			dst = itoa(dst, ((rmc->utc.year > 2000) ? rmc->utc.year - 2000 : rmc->utc.year), 2);
			*dst++ = ',';

			dst = itoa(dst, rmc->utc.hour, 2);
			dst = itoa(dst, rmc->utc.min, 2);
			dst = itoa(dst, rmc->utc.sec, 2);
			utc = 1;
			}else
			{
			if(rmc->lat != 0 && rmc->lon != 0 && rmc->utc.year != 0)
			{
			dst = itoa(dst, rmc->utc.day, 2);
			dst = itoa(dst, rmc->utc.mon + 1, 2);
			dst = itoa(dst, ((rmc->utc.year > 2000) ? rmc->utc.year - 2000 : rmc->utc.year), 2);
			*dst++ = ',';

			dst = itoa(dst, rmc->utc.hour, 2);
			dst = itoa(dst, rmc->utc.min, 2);
			dst = itoa(dst, rmc->utc.sec, 2);
			utc = 3;
			}else if(rtc_set() && GpsNoData() || !rtc_set() || rmc->utc.year == 0){
			dst = itoa(dst, date.Date, 2);
			dst = itoa(dst, date.Month, 2);
			if(utc_year == date.Year)
			dst = itoa(dst, date.Year, 2);
			else if(utc_year > 0)
			dst = itoa(dst, utc_year, 2);
			else
			dst = itoa(dst, date.Year, 2);
			
			dst = strcpyEx(dst, ",");
			dst = itoa(dst, rtc.Hours, 2);
			dst = itoa(dst, rtc.Minutes, 2);
			dst = itoa(dst, rtc.Seconds, 2);
			utc = 2;
			if(!rtc_set() && !getTCPConState()&&!getAccState()){
				if(local)
					memset(flashBuffer,0,BUFFER_LENGTH/4);
				return 0;
			}
			}
			else 
			{
			dst = itoa(dst, rmc->utc.day, 2);
			dst = itoa(dst, rmc->utc.mon + 1, 2);
			dst = itoa(dst, ((rmc->utc.year > 2000) ? rmc->utc.year - 2000 : rmc->utc.year), 2);
			*dst++ = ',';

			dst = itoa(dst, rmc->utc.hour, 2);
			dst = itoa(dst, rmc->utc.min, 2);
			dst = itoa(dst, rmc->utc.sec, 2);
			utc = 0;
			}
			}
			
			//*dst++ = '.';
			//dst = itoa(dst, rmc->utc.hsec, 3);
			//dst = strcpyEx( itoa( strcpyEx(dst, ",1,Switch1="), task->SenseD1, 1), VERSION);
			//dst = strcpyEx(dst, ",1,SosButton=0,voltage=4356,");
			if(!GpsNoData())
			dst = strcpyEx(dst, ",1,");
			else
			dst = strcpyEx(dst, ",0,");
			if(cmd != GSM_TASK_CMD_GEOFENCE){
			if(!strncmp(get_conf_elem(WORKINGMODE_CONF),"0",1))
			dst = strcpyEx(dst, alarm_state_ussd());
			else
			dst = strcpyEx(dst, alarm_state());
			}
			else{
			if(strncmp(get_conf_elem(WORKINGMODE_CONF),"0",1))
			dst = strcpyEx(dst, "GeofenceWarn=");
			dst = strcpyEx(dst, geofence_state());
			}
			dst = strcpyEx(dst, ",");
			if(getBattLowStat()){
			if(!strncmp(get_conf_elem(WORKINGMODE_CONF),"0",1))
			dst = strcpyEx(dst, getBattLowVoltStr()+4);
			else
			dst = strcpyEx(dst, getBattLowVoltStr());
			}
			else{
			if(!strncmp(get_conf_elem(WORKINGMODE_CONF),"0",1))
			dst = strcpyEx(dst, getADCVolt()+4);
			else
			dst = strcpyEx(dst, getADCVolt());
			}
			dst = strcpyEx(dst, ",");
			if(!strncmp(get_conf_elem(WORKINGMODE_CONF),"0",1))
			dst = strcpyEx(dst, signal_state_ussd());
			else
			dst = strcpyEx(dst, signal_state());
			
#if (USE_CAN_SUPPORT)
			// Format: obd=N,value[,value]
			//	N		number of values
			//	value	CAN parameter value
			dst = strcpyEx(dst, ",obd=");
			// dst = itoa(dst, CAN_VALUES_SIZE, 0);

			for (idx = 0; idx < CAN_VALUES_SIZE; idx ++)
				dst = D2IToString(dst, task->CanData[idx], 3);

			--dst;
#endif
			// Add NMEA checksum
			if(!local)
			size = NmeaAddChecksum(dst, gsmBuffer);
			else
			size = NmeaAddChecksum(dst, flashBuffer);
			
			send_message_number(read_message_number()+1);
			
			return size;
}



int frame_cell(int8_t* dst,int cmd)
{
	int size;
	GsmTask_t * task = &GsmTask;
	RTC_TimeTypeDef rtc;
	RTC_DateTypeDef date;

	/* Get the RTC current Time */
  HAL_RTC_GetTime(&RTCHandle, &rtc, RTC_FORMAT_BIN);
	
	HAL_RTC_GetDate(&RTCHandle, &date, RTC_FORMAT_BIN);
	
	adjust_timezone_time(&rtc);
	
			dst = strcpyEx(dst, cellinfo.cellid);
			dst = strcpyEx(dst, ",");
			dst = strcpyEx(dst, cellinfo.MCC);
			dst = strcpyEx(dst, ",");
			dst = strcpyEx(dst, cellinfo.MNC);
			dst = strcpyEx(dst, ",");
			dst = strcpyEx(dst, cellinfo.LAC);
			dst = strcpyEx(dst, ",");
	
			dst = strcpyEx(dst, cellinfo.cellid1);
			dst = strcpyEx(dst, ",");
			dst = strcpyEx(dst, cellinfo.MCC1);
			dst = strcpyEx(dst, ",");
			dst = strcpyEx(dst, cellinfo.MNC1);
			dst = strcpyEx(dst, ",");
			dst = strcpyEx(dst, cellinfo.LAC1);
			dst = strcpyEx(dst, ",");
			
			dst = strcpyEx(dst, cellinfo.cellid2);
			dst = strcpyEx(dst, ",");
			dst = strcpyEx(dst, cellinfo.MCC2);
			dst = strcpyEx(dst, ",");
			dst = strcpyEx(dst, cellinfo.MNC2);
			dst = strcpyEx(dst, ",");
			dst = strcpyEx(dst, cellinfo.LAC2);
			dst = strcpyEx(dst, ",");
			
			dst = strcpyEx(dst, cellinfo.cellid3);
			dst = strcpyEx(dst, ",");
			dst = strcpyEx(dst, cellinfo.MCC3);
			dst = strcpyEx(dst, ",");
			dst = strcpyEx(dst, cellinfo.MNC3);
			dst = strcpyEx(dst, ",");
			dst = strcpyEx(dst, cellinfo.LAC3);
			dst = strcpyEx(dst, ",");
			
			dst = strcpyEx(dst, ip);
			dst = strcpyEx(dst, ",");
			
			dst = strcpyEx(dst, imsi);
			dst = strcpyEx(dst, ",");
			
			dst = strcpyEx(dst, cellinfo.RXL);
			dst = strcpyEx(dst, ";");
			dst = strcpyEx(dst, cellinfo.RXL1);
			dst = strcpyEx(dst, ";");
			dst = strcpyEx(dst, cellinfo.RXL2);
			dst = strcpyEx(dst, ";");
			dst = strcpyEx(dst, cellinfo.RXL3);
			dst = strcpyEx(dst, ",");
			
			dst = itoa(dst, date.Date, 2);
			dst = itoa(dst, date.Month, 2);
			dst = itoa(dst, date.Year, 2);
			dst = strcpyEx(dst, ",");
			dst = itoa(dst, rtc.Hours, 2);
			dst = itoa(dst, rtc.Minutes, 2);
			dst = itoa(dst, rtc.Seconds, 2);
			dst = strcpyEx(dst, ",");

			if(cmd != GSM_TASK_CMD_GEOFENCE)
			dst = strcpyEx(dst, alarm_state());
			else{
			dst = strcpyEx(dst, "GeofenceWarn=");
			dst = strcpyEx(dst, geofence_state());
			}
			
			dst = strcpyEx(dst, ",");
			if(getBattLowStat())
			dst = strcpyEx(dst, getBattLowVoltStr());
			else
			dst = strcpyEx(dst, getADCVolt());

			dst = strcpyEx(dst, ",");
			dst = strcpyEx(dst, signal_state());
			
#if (USE_CAN_SUPPORT)
			// Format: obd=N,value[,value]
			//	N		number of values
			//	value	CAN parameter value
			dst = strcpyEx(dst, ",obd=");
			// dst = itoa(dst, CAN_VALUES_SIZE, 0);

			for (idx = 0; idx < CAN_VALUES_SIZE; idx ++)
				dst = D2IToString(dst, task->CanData[idx], 3);

			--dst;
#endif
			// Add NMEA checksum
			size = NmeaAddChecksum(dst, gsmBuffer);
			
			return size;
}

uint8_t Device_status(void)
{
	uint8_t status = 0;
	
	if(!strncmp((char *)get_conf_elem(WARNCIRCLE_CONF),"1",1))
		status |= 0x01;
	else
		status &= ~(0x01);
	
	if(HAL_GPIO_ReadPin(GPIOA, GPIO_PIN_7) == GPIO_PIN_SET)
		status |= 0x02;
	else
		status &= ~(0x02);
	/*
	if(in_minus_send)
		status |= 0x01;
	else
	  status &= ~(0x01);
	
	if(in_plus_send)
		status |= 0x02;
	else
	  status &= ~(0x02);
	*/
	if(getOut1Statue())
		status |= 0x04;
	else
	  status &= ~(0x04);
	
	if(getOut2Statue())
		status |= 0x08;
	else
	  status &= ~(0x08);
	
	if(GlobalStatus & VBAT_LOW)
		status |= 0x10;
	else
	  status &= ~(0x10);
	
	if(!GpsNoData())
		status |= 0x20;
	else
	  status &= ~(0x20);
	
	if(getTCPConState())
		status |= 0x40;
	else
	  status &= ~(0x40);
	
	if(isAccOn() || getAccIonState())
		status |= 0x80;
	else
	  status &= ~(0x80);
	
	return status;

}


int Ruptela_frame(int8_t* dst,int cmd,bool local)
{
	int size;
	nmeaGPRMC * rmc;
	RTC_TimeTypeDef rtc;
	RTC_DateTypeDef date;
	GsmTask_t * task = &GsmTask;
	uint8_t utc = 0;
	int lat = 0;
	int lon = 0;
	int8_t str[50];
	time_t timep;
  struct tm p;
	int8_t *pEnd;
	int8_t numExtData = 0;
	
		/* Get the RTC current Time */
  HAL_RTC_GetTime(&RTCHandle, &rtc, RTC_FORMAT_BIN);
	
	HAL_RTC_GetDate(&RTCHandle, &date, RTC_FORMAT_BIN);	
	
	if(DebugEnable) DEBUG("**cmd %d",cmd);
	
	if(cmd == GSM_TASK_CMD_GPS)
	rmc = &(task->GPRMC);//getGPRMCData();//&(task->GPRMC);
	else{
	latloncopy = true;
	rmc = getGPRMCData(cmd);
	rmc->lat = nmea_ndeg2degree(rmc->lat);
	rmc->lon = nmea_ndeg2degree(rmc->lon);
	}
#if 0	
	if(rmc->ns == 'S')
	lat = -(rmc->lat * 10000000);
	else
	lat = rmc->lat * 10000000;
	if(rmc->ns == 'W')
	lon = -(rmc->lon * 10000000);
	else
	lon = rmc->lon * 10000000;
#endif	
	
	memset(&p, 0, sizeof(p));
	
	
	if(cmd == GSM_TASK_CMD_GPS && rmc->utc.year != 0)
	{
		p.tm_year = rmc->utc.year-1900;
		p.tm_mon = rmc->utc.mon;
		p.tm_mday = rmc->utc.day;
		p.tm_hour = rmc->utc.hour;
		p.tm_min = rmc->utc.min;
		p.tm_sec = rmc->utc.sec;		
  }else
	{
		if(rmc->lat != 0 && rmc->lon != 0 && rmc->utc.year != 0)
		{
			p.tm_year = rmc->utc.year-1900;
			p.tm_mon = rmc->utc.mon;
			p.tm_mday = rmc->utc.day;
			p.tm_hour = rmc->utc.hour;
			p.tm_min = rmc->utc.min;
			p.tm_sec = rmc->utc.sec;
		}else if(rtc_set() && GpsNoData() || !rtc_set() || rmc->utc.year == 0){

			if(utc_year == date.Year)
			p.tm_year = date.Year + 2000 - 1900;
			else if(utc_year > 0)
			p.tm_year = utc_year + 2000 -1900;
			else
			p.tm_year = date.Year + 2000 - 1900;
			
			p.tm_mon = date.Month-1;
			p.tm_mday = date.Date;
			p.tm_hour = rtc.Hours;
			p.tm_min = rtc.Minutes;
			p.tm_sec = rtc.Seconds;
			
			if(!rtc_set() && !getTCPConState()&&!getAccState()){
				if(local)
					memset(flashBuffer,0,BUFFER_LENGTH/4);
				return 0;
			}
			}
			else 
			{
			p.tm_year = rmc->utc.year-1900;
			p.tm_mon = rmc->utc.mon-1;
			p.tm_mday = rmc->utc.day;
			p.tm_hour = rmc->utc.hour;
			p.tm_min = rmc->utc.min;
			p.tm_sec = rmc->utc.sec;
			}
	}

	
	timep = mktime(&p);
	
	if(DebugEnable) DEBUG("%d",timep);
	
	#if 0
	dst = strcpyEx(dst, "003e");
	
	memset(str,0,20);
	sprintf(str, "%016llx",strtoll(GsmIMEI(),&pEnd,10));
	dst = strcpyEx(dst, str);
	
	dst = strcpyEx(dst, "010001");
	
	memset(str,0,20);
	sprintf(str, "%x",timep);
	dst = strcpyEx(dst, str);
	
	dst = strcpyEx(dst, "0000");
	
	memset(str,0,20);
	sprintf(str, "%08x",lon);
	dst = strcpyEx(dst, str);
	memset(str,0,20);
	sprintf(str, "%08x",lat);
	dst = strcpyEx(dst, str);
	
	memset(str,0,20);
	sprintf(str, "%04x",(int)rmc->declination);
	dst = strcpyEx(dst, str);
	
	memset(str,0,20);
	sprintf(str, "%04x",(int)rmc->direction);
	dst = strcpyEx(dst, str);
	
	dst = strcpyEx(dst, "0a");
	
	memset(str,0,20);
	sprintf(str, "%04x",(int)rmc->speed);
	dst = strcpyEx(dst, str);
	
	dst = strcpyEx(dst, "0b");
	
	dst = strcpyEx(dst, "07");
	
	
	#if 0
	memset(gsmBuffer,0,512);
	memset(str,0,50);
	strcpy(str,"00280003136e3700c50401000159f985d300003dddfc5900c2a5f2022f08b61000000107010500000000");
	//00280003136e3700c50401000159f985d300003dddfc5900c2a5f2022f08b61000000107010500000000db07
	StrToHex(dst,str,strlen(str)/2);
	dst = dst+ strlen(str)/2 ;
	if(DebugEnable) DEBUG("*****%d",dst-gsmBuffer-2);
	sprintf(str, "%04x",crc_16_rec(gsmBuffer+2,(dst-gsmBuffer-2)));
	if(DebugEnable) DEBUG("%s",str);
	//memset(str,0,50);
	//sprintf(str, "%04x",crc_16_rec(gsmBuffer+2,strlen(str)/2 -2));
	//sprintf(str, "%04x",crc_16_rec(gsmBuffer+2,dst-gsmBuffer-2));
	//if(DebugEnable) DEBUG("%s",str);
	StrToHex(dst,str,strlen(str)/2);
	#endif
	
	#endif
		
	#if 0
		
	*dst++ = 0x00;
	*dst++ = 0x01;
	
	memset(str,0,20);
	sprintf(str, "%016llx",strtoll(GsmIMEI(),&pEnd,10));
	
	if(DebugEnable) DEBUG("%s %d",str,strlen(str));
	
	StrToHex(dst,str,strlen(str)/2);
	
	dst = dst + strlen(str)/2;
	
	*dst++ = 0x01;
	*dst++ = 0x00;
	*dst++ = 0x01;
	
	memset(str,0,20);
	sprintf(str, "%x",timep);
	StrToHex(dst,str,strlen(str)/2);
	
	dst = dst + strlen(str)/2;
	
	*dst++ = 0x00;
	*dst++ = 0x00;
	
	memset(str,0,20);
	sprintf(str, "%08x",lon);
	StrToHex(dst,str,strlen(str)/2);
	dst = dst + strlen(str)/2;
	memset(str,0,20);
	sprintf(str, "%08x",lat);
	StrToHex(dst,str,strlen(str)/2);
	dst = dst + strlen(str)/2;
	
	memset(str,0,20);
	sprintf(str, "%04x",(int)rmc->declination);
	StrToHex(dst,str,strlen(str)/2);
	dst = dst + strlen(str)/2;
	
	memset(str,0,20);
	sprintf(str, "%04x",(int)rmc->direction);
	StrToHex(dst,str,strlen(str)/2);
	dst = dst + strlen(str)/2;
	
	*dst++ = 0x0a;
	
	memset(str,0,20);
	sprintf(str, "%04x",(int)rmc->speed);
	StrToHex(dst,str,strlen(str)/2);
	dst = dst + strlen(str)/2;
	
	*dst++ = 0x01;
	
	*dst++ = 0x07;
	
	*dst++ = 0x01;
	
	*dst++ = 0x05;
	
	*dst++ = 0x00;
	*dst++ = 0x00;
	*dst++ = 0x00;
	*dst++ = 0x00;
	
	memset(str,0,20);
	if(!local)
	size = dst-gsmBuffer-2;
	else
	size = dst-flashBuffer-2;
	sprintf(str, "%04x",size);
	if(!local)
	StrToHex(gsmBuffer,str,strlen(str)/2);
	else
	StrToHex(flashBuffer,str,strlen(str)/2);
	memset(str,0,20);
	if(!local){
	if(DebugEnable) DEBUG("*****%d",dst-gsmBuffer-2);
	}
	else{
	if(DebugEnable) DEBUG("*****%d",dst-flashBuffer-2);
	}		
	if(!local)
	sprintf(str, "%04x",crc_16_rec(gsmBuffer+2,dst-gsmBuffer-2));
	else
	sprintf(str, "%04x",crc_16_rec(flashBuffer+2,dst-flashBuffer-2));
	StrToHex(dst,str,strlen(str)/2);
	
	/*#else*/
	//command
	*dst++ = 0x01;
	//version
	*dst++ = 0x00;
	*dst++ = 0x01;
	//imei
	memset(str,0,20);
	sprintf(str, "%016llx",strtoll(GsmIMEI(),&pEnd,10));
	
	StrToHex(dst,str,strlen(str)/2);
	
	dst = dst + strlen(str)/2;
	//MNC MCC
	*dst++ = 0x00;
	*dst++ = 0x00;
	*dst++ = 0x00;
	*dst++ = 0x00;
	//number
	*dst++ = 0x01;
	
	//payload size
	int8_t *paylodsize;
	 paylodsize = dst;
	*dst++ = 0x00;
	*dst++ = 0x01;
	//event id
	int8_t *payload;
	payload = dst;
	*dst++ = 0x01;
	//timestamp
	
	memset(str,0,20);
	sprintf(str, "%x",timep);
	StrToHex(dst,str,strlen(str)/2);
	
	dst = dst + strlen(str)/2;
	
	//gps data 
	memset(str,0,20);
	sprintf(str, "%08x",lon);
	StrToHex(dst,str,strlen(str)/2);
	dst = dst + strlen(str)/2;
	memset(str,0,20);
	sprintf(str, "%08x",lat);
	StrToHex(dst,str,strlen(str)/2);
	dst = dst + strlen(str)/2;
	
	memset(str,0,20);
	sprintf(str, "%04x",(int)rmc->declination);
	StrToHex(dst,str,strlen(str)/2);
	dst = dst + strlen(str)/2;
	
	memset(str,0,20);
	sprintf(str, "%04x",(int)rmc->direction);
	StrToHex(dst,str,strlen(str)/2);
	dst = dst + strlen(str)/2;
	
	memset(str,0,20);
	sprintf(str, "%04x",(int)rmc->speed);
	StrToHex(dst,str,strlen(str)/2);
	dst = dst + strlen(str)/2;
	
	//satellite
	*dst++ = 0x0a;
	
	//HDOP
	*dst++ = 0x0a;
	
	//VOL
	memset(str,0,20);
	sprintf(str, "%04x",atoi(getADCVolt()+4));
	if(DebugEnable) DEBUG("vol %s",str);
	StrToHex(dst,str,strlen(str)/2);
	dst = dst + strlen(str)/2;
	//EH ML
	register GpsContext_t * gc = &GpsContext;
	memset(str,0,20);
	sprintf(str, "%04x",gc->enginHour);
	StrToHex(dst,str,strlen(str)/2);
	dst = dst + strlen(str)/2;
	
	memset(str,0,20);
	sprintf(str, "%04x",gc->mileage);
	StrToHex(dst,str,strlen(str)/2);
	dst = dst + strlen(str)/2;
	//ACC
	*dst++ = 0x01;
	//Extended Data Mask
	*dst++ = 0x00;
	//Message count
	memset(str,0,20);
	sprintf(str, "%04x",read_message_number()+1);
	StrToHex(dst,str,strlen(str)/2);
	dst = dst + strlen(str)/2;
	//CRC
	memset(str,0,20);
	if(!local)
	size = dst-gsmBuffer;
	else
	size = dst-flashBuffer;
	sprintf(str, "%04x",size);
	
	if(DebugEnable) DEBUG("size %d",size);
	
	if(!local)
	StrToHex(gsmBuffer+(paylodsize-gsmBuffer),str,strlen(str)/2);
	else
	StrToHex(flashBuffer+(paylodsize-flashBuffer),str,strlen(str)/2);
	memset(str,0,20);
	if(!local){
	if(DebugEnable) DEBUG("1*****%d",paylodsize-gsmBuffer);
	if(DebugEnable) DEBUG("2*****%d",dst-payload);
	if(DebugEnable) DEBUG("3*****%d",dst-gsmBuffer);
	}
	else{
	if(DebugEnable) DEBUG("1*****%d",paylodsize-flashBuffer);
	if(DebugEnable) DEBUG("2*****%d",dst-payload);
	if(DebugEnable) DEBUG("3*****%d",dst-flashBuffer);
	}		
	if(!local)
	sprintf(str, "%04x",crc_16_rec(gsmBuffer,dst-gsmBuffer));
	else
	sprintf(str, "%04x",crc_16_rec(flashBuffer,dst-flashBuffer));
	StrToHex(dst,str,strlen(str)/2);
	
	if(!local){
	for(uint8_t i = 0; i < dst-gsmBuffer ;i++){
		
		DEBUG("%02x",gsmBuffer[i]);
	}
	}else
	{
	for(uint8_t i = 0; i < dst-flashBuffer ;i++){
		
		DEBUG("%02x",flashBuffer[i]);
	}
	}
	
	#endif
	
	//command length
	*dst++ = 0x20;
	
	//command type
	*dst = task->Result;
		
	switch(cmd)
	{
		case GSM_TASK_CMD_ACCOFF:
			*dst = 0x10;
			break;
		case GSM_TASK_CMD_ACCON:
			*dst = 0x11;
			break;
		case GSM_TASK_CMD_ALARM:
			if(sos_alarm)
				*dst = 0x20;
			else if(cutdetect_alarm)
				*dst = 0x21;
			else if(batterylow_alarm)
				*dst = 0x22;
			else if(overspeed_alarm)
				*dst = 0x23;
			else if(shock_alarm)
				*dst = 0x26;
			else if(gpslost_alarm)
				*dst = 0x14;
			else if(gpsfix_alarm)
				*dst = 0x15;
			break;
		case GSM_TASK_CMD_IN_N:
			 if(in_minus_send)
					*dst = 0x30;
			 else
				  *dst = 0x31;
			 break;
		case GSM_TASK_CMD_IN_P:
			 if(in_plus_send)
				 *dst = 0x32;
			 else
				 *dst = 0x33;
			break;
		case GSM_TASK_CMD_RTC:
			*dst = 0x05;
			break;
		case GSM_TASK_CMD_SIGNAL:
			*dst = 0x06;
			break;
	}
	if(DebugEnable) DEBUG("dst %02x",*dst);
	task->Result = 0;
	dst++;
	
	////device type|IMEI
	memset(str,0,20);
	//strcat(str,GsmIMEI());
	sprintf(str, "%014llx",strtoll(GsmIMEI(),&pEnd,10));

	StrToHex(dst,str,strlen(str)/2);
	
	dst[0] = dst[0] | (0x01 << 2); 
	
	dst = dst + strlen(str)/2;
	
	//timestamp
	memset(str,0,20);
	sprintf(str, "%x",timep);
	StrToHex(dst,str,strlen(str)/2);
	
	dst = dst + strlen(str)/2;
	
	//lat lon
	
	lat = rmc->lat * 10000000;
  
	lon = rmc->lon * 10000000;
	
	
	if(lat != 0 && lon !=0){
		
	memset(str,0,20);
	if(rmc->ns == 'W')
		lon = -lon;
	sprintf(str, "%08x",lon);
	StrToHex(dst,str,strlen(str)/2);
	if(rmc->ns == 'W')
	dst[0] = dst[0] | 0x80;
	dst = dst + strlen(str)/2;
	
	memset(str,0,20);
	if(rmc->ns == 'S')
		lat = -lat;
	sprintf(str, "%08x",lat);
	StrToHex(dst,str,strlen(str)/2);
	if(rmc->ns == 'S')
	dst[0] = dst[0] | 0x80;
	dst = dst + strlen(str)/2;
 }else
 {
	 for(uint8_t i = 0; i < 8 ;i++){
	 *dst = 0xff;
		dst++;
	 }
 }
	
	//angle
	if(DebugEnable) DEBUG("direction %f",rmc->direction);
	memset(str,0,20);
	sprintf(str, "%04x",(int)(rmc->direction*100));
	StrToHex(dst,str,strlen(str)/2);
	dst = dst + strlen(str)/2;
	
	//speed
	memset(str,0,20);
	if(DebugEnable) DEBUG("speed %f",rmc->speed);
	if(!isAccOn())
		rmc->speed = 0;
	sprintf(str, "%04x",(int)(rmc->speed*100));
	StrToHex(dst,str,strlen(str)/2);
	dst = dst + strlen(str)/2;
	latloncopy =false;
	//Number Satellite
	if(!strncmp((char *)get_conf_elem(RESERVE5_CONF),"1",1))
	*dst = getLastGPGSVData()->sat_count|0x80;
	else
	*dst = getLastGPGSVData()->sat_count&~(0x80);
	
	if(strlen(Rs232Val)!=0 && strcmp(Rs232Val,"0"))
		numExtData++;
	if(Temperature != 0)
		numExtData++;
	if(Humi != 0)
		numExtData++;
	
	if(numExtData != 0)
		*dst = *dst | (0x40);
	else
		*dst = *dst &~(0x40);
	
	dst++;
	
	//HDOP
	*dst++ = (getLastGPGSAData()->HDOP*10);
	//VOL
	memset(str,0,20);
	sprintf(str, "%04x",atoi(getADCVolt()+4));
	if(DebugEnable) DEBUG("vol %s",str);
	StrToHex(dst,str,strlen(str)/2);
	dst = dst + strlen(str)/2;
	//EH ML
	register GpsContext_t * gc = &GpsContext;
	memset(str,0,20);
	sprintf(str, "%04x",gc->enginHour);
	StrToHex(dst,str,strlen(str)/2);
	dst = dst + strlen(str)/2;
	
	memset(str,0,20);
	sprintf(str, "%04x",gc->mileage);
	StrToHex(dst,str,strlen(str)/2);
	dst = dst + strlen(str)/2;
	//STATUS
	*dst++ = Device_status();
	//ADC
	memset(str,0,20);
	sprintf(str, "%04x",new_s_uwADCxConvertedValue);
	if(DebugEnable) DEBUG("vol %s",str);
	StrToHex(dst,str,strlen(str)/2);
	dst = dst + strlen(str)/2;
	
	//SENSOR
	if(DebugEnable) DEBUG("numExtData %d",numExtData);
	if(numExtData != 0)
	*dst++ = numExtData;
	
	if(strlen(Rs232Val) != 0 && strcmp(Rs232Val,"0")){
		if(DebugEnable) DEBUG("Rs232Val %s %d",Rs232Val,strlen(Rs232Val));
		*dst++ = 0x11;
		memset(str,0,20);
		sprintf(str, "%04x",atoi(Rs232Val));
		if(DebugEnable) DEBUG("str %s",str);
		StrToHex(dst,str,strlen(str)/2);
		dst = dst + strlen(str)/2;
	}
	
	if(Temperature > 0)
	{
		if(DebugEnable) DEBUG("Temperature %d",Temperature);
		*dst++ = 0x31;
		memset(str,0,20);
		sprintf(str, "%04x",(int)(Temperature*100));
		StrToHex(dst,str,strlen(str)/2);
		dst = dst + strlen(str)/2;
	}else if(Temperature < 0)
	{
		if(DebugEnable) DEBUG("Temperature %d",Temperature);
		*dst++ = 0x31;
		Temperature = 0 - Temperature;
		memset(str,0,20);
		sprintf(str, "%04x",(int)(Temperature*100));
		StrToHex(dst,str,strlen(str)/2);
		dst[0] = dst[0] | 0x80;
		dst = dst + strlen(str)/2;
	}
	
	if(Humi != 0)
	{
		if(DebugEnable) DEBUG("Humi %d",Humi);
		*dst++ = 0x32;
		memset(str,0,20);
		sprintf(str, "%04x",(int)(Humi*100));
		StrToHex(dst,str,strlen(str)/2);
		dst = dst + strlen(str)/2;
	}
	
	//Message count
	memset(str,0,20);
	uint32_t count = read_message_number()+1;
	sprintf(str, "%04x",count);
	StrToHex(dst,str,strlen(str)/2);
	dst = dst + strlen(str)/2;
	
	//CRC
	if(!local)
	sprintf(str, "%02x",crc_16_rec(gsmBuffer+1,dst-gsmBuffer-1));
	else
	sprintf(str, "%02x",crc_16_rec(flashBuffer+1,dst-flashBuffer-1));
	StrToHex(dst,str,strlen(str)/2);
	dst = dst + strlen(str)/2;
	
	if(!local)
	gsmBuffer[0] = dst-gsmBuffer -1;
	else
	flashBuffer[0] = dst-gsmBuffer -1;
	
	if(!local){
	for(uint8_t i = 0; i < dst-gsmBuffer ;i++){
		
		if(DebugEnable) DEBUG("%02x",gsmBuffer[i]);
	}
	}else
	{
	for(uint8_t i = 0; i < dst-flashBuffer ;i++){
		
		if(DebugEnable) DEBUG("%02x",flashBuffer[i]);
	}
	}
	send_message_number(read_message_number()+1);
	
	if(!local)
	return gsmBuffer[0] ;
	else
	return flashBuffer[0];

}


int Ruptela_frame_login(int8_t* dst)
{
	int8_t str[20];
	int8_t *pEnd;
	
	//command length
	*dst++ = 0x15;
	//command type
	*dst++ = 0x01;
	////device type|IMEI
	memset(str,0,20);
	//strcat(str,GsmIMEI());
	sprintf(str, "%014llx",strtoll(GsmIMEI(),&pEnd,10));

	StrToHex(dst,str,strlen(str)/2);
	
	dst[0] = dst[0] | (0x01 << 2); 
	
	dst = dst + strlen(str)/2;

	//Firmware Version
	*dst++ = 0x01;
	*dst++ = 0x06;
	*dst++ = 0x39;
	//Protocol Version
	*dst++ = 0x01;
	*dst++ = 0x01;
	//MCC
	memset(str,0,20);
	sprintf(str, "%04x",atoi(cellinfo.MCC));
	StrToHex(dst,str,strlen(str)/2);
	dst = dst + strlen(str)/2;
	//MNC
	memset(str,0,20);
	sprintf(str, "%04x",atoi(cellinfo.MNC));
	StrToHex(dst,str,strlen(str)/2);
	dst = dst + strlen(str)/2;
	
	//CRC
	sprintf(str, "%02x",crc_16_rec(gsmBuffer+1,dst-gsmBuffer-1));
	StrToHex(dst,str,strlen(str)/2);
	dst = dst + strlen(str)/2;
	gsmBuffer[0] = dst-gsmBuffer -1;
	
	for(uint8_t i = 0; i < dst-gsmBuffer ;i++){
		
		if(DebugEnable) DEBUG("%02x",gsmBuffer[i]);
	}
	
	
	return gsmBuffer[0];
}


int Ruptela_frame_ping(int8_t* dst)
{
	int8_t str[20];
	int8_t *pEnd;
	
	//command length
	*dst++ = 0x04;
	//command type
	*dst++ = 0x7c;
	////device type|IMEI
	memset(str,0,20);
	//strcat(str,GsmIMEI());
	sprintf(str, "%014llx",strtoll(GsmIMEI(),&pEnd,10));

	StrToHex(dst,str,strlen(str)/2);
	
	dst[0] = dst[0] | (0x01 << 2); 
	
	dst = dst + strlen(str)/2;
	
	//STATUS
	*dst++ = Device_status();
	//CRC
	sprintf(str, "%02x",crc_16_rec(gsmBuffer+1,dst-gsmBuffer-1));
	StrToHex(dst,str,strlen(str)/2);
	dst = dst + strlen(str)/2;
	gsmBuffer[0] = dst-gsmBuffer -1;
	for(uint8_t i = 0; i < dst-gsmBuffer ;i++){
		
		if(DebugEnable) DEBUG("%02x",gsmBuffer[i]);
	}
	
	
	return gsmBuffer[0];
}

/*******************************************************************************
* Function Name	:	gsmSendTaskCommand
* Description	:	Send message to host
*******************************************************************************/
uint8_t gsmSendTaskCommand(GsmContext_t *gc)
{
	const GgCommand_t *cmd;
	int send_size = 0;
	uint8_t result = E_ERROR;
	GsmTask_t * task = &GsmTask;
	int8_t *dst = gsmBuffer;
	int8_t *item;
	int8_t *itemEnd;
	uint8_t Command;
	uint8_t Ussd[256]= {0};

#if ( USE_CAN_SUPPORT == 1 )
	uint8_t idx;
#endif

	if (gc->Status & GSM_STATUS_TCP_DISABLE)
	{
		ShowError(ERROR_GSM_TCP_DISABLE);
		return E_GSM_TCP_DISABLE;
	}

	Command = task->Command;
	
	if(DebugEnable) DEBUG("Command %d %d",Command,pingTime);
	
	if(pingTime > 10 && Command != GSM_TASK_CMD_NOP 
		&& Command != GSM_TASK_CMD_PING
		&& Command != GSM_TASK_CMD_AT	
		&& Command != GSM_TASK_CMD_LOGIN )
	{
	pingTime = 0;
	gsmDisconnectTCP(gc);
	}else if(pingTime <= 10 && Command != GSM_TASK_CMD_NOP 
		&& Command != GSM_TASK_CMD_PING
		&& Command != GSM_TASK_CMD_AT	
		&& Command != GSM_TASK_CMD_LOGIN
		 )
	{
		pingTime = 0;
	}
	
	if((!strncmp(get_conf_elem(WORKINGMODE_CONF),"2",1) && !getTCPConState() && Command == GSM_TASK_CMD_PING)
		|| (!strncmp(get_conf_elem(WORKINGMODE_CONF),"2",1) && !getTCPConState() && Command == GSM_TASK_CMD_NEW_PING))
	{
		return E_NO_RESPONSE;
	}
	
	if(gsmDisconnect && !strncmp(get_conf_elem(WORKINGMODE_CONF),"2",1)){
			gsmDisconnect = false;
			memset(localwriteBuffer,0,BUFFER_LENGTH/4);
			gsmDisconnectTCP(gc);
		}
	
	if(Command == GSM_TASK_CMD_RECONNECT && !strncmp(get_conf_elem(WORKINGMODE_CONF),"2",1))
	{
			//gc->Status |= GSM_STATUS_TCP_ON;
			gsmSend(GSM_CSQ);
			result = E_NO_RESPONSE;
			return result;
	}
		
	if (Command == GSM_TASK_CMD_SMS)
	{
		#if 0
		if (task->SmsIndex != 0)
		{
			gsmDisconnectTCP(gc);
			cmd = task->GG_Command;
			if (cmd == NULL
			||	cmd->Response == NULL
			||	((cmd->Response)(gsmBuffer, sizeof(gsmBuffer), true, cmd) == E_OK)
				)
			{
				gsmDeleteSMS(task->SmsIndex);
			}
		}
		#endif
	}else if(Command == GSM_TASK_CMD_AT)
	{
	#if 0
	if(!strncmp((char *)task->Content,(char*)GSM_CBC,strlen((char*)GSM_CBC)))	
	{
	if (E_OK == gsmCmd1(GSM_CBC, 2000))
				{
					item = TokenBefore(gsmToken(NULL), "OK");
					if (item != NULL)
					{
						if(strchr((char*)item,',')!= NULL)
						{
								item = (int8_t*)strchr((char *)item,',');
								item +=1;
								if(strchr((char *)item,',')!= NULL){
									item = (int8_t*)strchr((char *)item,',');
									item +=1;
									if(strchr((char *)item,',') == NULL){
									sprintf((char *)battVolt,"voltage=%s",item);
									}
								}
						}
					}
				}
	if(DebugEnable) DEBUG("battVolt:%s",battVolt);
	}
	#endif
	if(!strncmp((char *)task->Content,(char*)GSM_CBC,strlen((char*)GSM_CBC))){
	gsmSend(GSM_CBC);
	}
	else if(!strncmp((char *)task->Content,(char*)GSM_CSQ,strlen((char*)GSM_CSQ)))	
	{
	#if 0
	if (E_OK == gsmCmd1(GSM_CSQ, 2000))
				{
					item = TokenBefore(gsmToken(NULL), "OK");
					if (item != NULL)
					{
						if(strchr((char*)item,':')!= NULL)
						{
								item = (int8_t*)strchr((char *)item,':');
							  item +=1;
								itemEnd = (int8_t*)strchr((char *)item,',');
								strncpy((char *)signal,(char*)item,itemEnd-item);
						}
					}
				}
	gc->RxMode = GSM_MODE_STREAM;
	if(DebugEnable) DEBUG("signal:%s",signal);
	#endif
	gsmSend(GSM_CSQ);
	testSignal = 2;
	}

	else if(!strncmp((char *)task->Content,(char *)GSM_ATA,strlen((char *)GSM_ATA))){
			gsmCmd1(GSM_ATA, 2000);
	}
	else if(!strncmp((char *)task->Content,(char *)GSM_CLCC,strlen((char *)GSM_CLCC))){
			if (E_OK == gsmCmd1(GSM_CLCC, 2000))
				{
					item = TokenBefore(gsmToken(NULL), "OK");
					if (item != NULL)
					{
						if(isSosNumber(item))
							answerIncomingCall();
					}
				}
		gc->RxMode = GSM_MODE_STREAM;
	}
	else{
		if(DebugEnable) DEBUG("%s",task->Content);
		if(!strncmp((char *)task->Content,"AT+QAUDLOOP=1,0\r\n",strlen("AT+QAUDLOOP=1,0\r\n")))
		{
		DEBUG("AUDIOLOOP TEST");
		}else if(!strncmp((char *)task->Content,"AT+QAUDLOOP=0,0\r\n",strlen("AT+QAUDLOOP=0,0\r\n")))
		{
		DEBUG("AUDIOLOOP TEST OVER");
		}
		//gsmSends((int8_t*)task->Content,NULL);
		gsmCmd1((int8_t*)task->Content, 10000);
	}
	task->Command = GSM_TASK_CMD_NOP;
	GsmAddTask(task);
	gsmRxFlush(gc);
	gc->RxMode = GSM_MODE_STREAM;
	}
	else if (gsmConnectUSSDTCPUDP(gc) == E_OK)
	{
		if(DebugEnable) DEBUG("task->Command %d",Command);
		if (Command == GSM_TASK_CMD_GPS || Command == GSM_TASK_CMD_SIGNAL ||  Command == GSM_TASK_CMD_RTC)
		{
			
			if(strncmp(get_conf_elem(RESERVE3_CONF),"1",1)){
			
			if(!strncmp(get_conf_elem(WORKINGMODE_CONF),"0",1))
			{
			dst = strcpyEx( strcpyEx( strcpyEx( gsmBuffer, "$U,"), GsmIMEI()), ",1, ,");
			send_size = frame(dst,Command,false);
			}
			else{
			
			dst = strcpyEx( strcpyEx( strcpyEx( gsmBuffer, "$FRCMD,"), GsmIMEI()), ",_SendMessage,,");
			send_size = frame(dst,Command,false);
			//dst = strcpyEx(gsmBuffer,"$FRCMD,865789022365383,_Update, ,47.89.17.248:firmware:123456:/var/www/tracker_srv/file_srv/firmware:te101.bin");
			//NmeaAddChecksum(dst,gsmBuffer);
			//send_size = 10;
			}
		}else
		{
			send_size = Ruptela_frame(dst,Command,false);
		}
			result = E_NO_RESPONSE;
		}
	#if 1	
		else if(Command == GSM_TASK_CMD_GSMCELL)
		{
		//gsmSend("AT+QENG=\"servingcell\"\r\n");
			(*gc->Commands->GsmCell)();
		}
		
		else if(Command == GSM_TASK_CMD_GSMCELL_NEIGHBOUR)
		{
		gsmSend("AT+QENG=\"neighbourcell\"\r\n");	
		}
		else if(Command == GSM_TASK_CMD_GSMCELL_SEND)
		{
		dst = strcpyEx( strcpyEx( strcpyEx( gsmBuffer, "$FRCMD,"), GsmIMEI()), ",_SendGsmcell,,");
		//setAccState(ACC_OFF,true);
		send_size = frame_cell(dst,Command);
		result = E_NO_RESPONSE;
		}
	#endif	
		else if (Command == GSM_TASK_CMD_GPS_FILEDATA)
		{
			send_size = 0;
			#if 0
			if(!strncmp(get_conf_elem(WORKINGMODE_CONF),"0",1)){
			memset(localwriteBuffer,0,BUFFER_LENGTH/4);
			memcpy(localwriteBuffer,localreadBuffer,strlen(localreadBuffer));
			memset(Ussd,0,256);
			sprintf(Ussd, "%s,\"%s%s#\"", "AT+CUSD=1",ussd_payload_format,localreadBuffer);
			if(strlen(Ussd)>134)
				return E_NO_RESPONSE;
		  gsmSend(Ussd);
			osDelay(100);
			gsmSend("\r\n");
			return E_NO_RESPONSE;
			}
			#endif
			setTime_noupdate();
			osDelay(1000);
			memset(localwriteBuffer,0,BUFFER_LENGTH/4);
			if(strncmp(get_conf_elem(RESERVE3_CONF),"1",1)){
			memcpy(localwriteBuffer,localreadBuffer,strlen(localreadBuffer));
			}else
			{
			memcpy(localwriteBuffer,localreadBuffer,BUFFER_LENGTH/4);
			}
			(*gc->Commands->LoTcpSend)();
			result = E_NO_RESPONSE;
		}
		#if 0
		else if (Command == GSM_TASK_CMD_CONF)
		{
			send_size = 0;
			uint8_t conf_name[20] = {0};
	    uint8_t *p;
		  uint8_t *pEnd;
			p =  task->Content;
	    pEnd = (int8_t*) strchr((char *)p,',');
	    if(pEnd == NULL)
			return E_NO_RESPONSE;
			memcpy(conf_name,p,pEnd-p);
			pEnd+=1;
			GlobalStatus |= GPS_STOP;
			while(1)
			{
			if(Gps_Ignore())
			break;
			}
		  printf("\r\n%s ** %s\r\n",conf_name,pEnd);
		  update_conf_elem(conf_name,pEnd);
		  GlobalStatus &= ~GPS_STOP;
		  initGsmUart();
			result = E_NO_RESPONSE;
		}
		#endif
		else if(Command == GSM_TASK_CMD_ACCOFF)
		{
		//setAccState(ACC_OFF,true);
		if(strncmp(get_conf_elem(RESERVE3_CONF),"1",1)){
		if(!strncmp(get_conf_elem(WORKINGMODE_CONF),"0",1))
		{
		dst = strcpyEx( strcpyEx( strcpyEx( gsmBuffer, "$U,"), GsmIMEI()), ",1, ,");
		send_size = frame(dst,Command,false);
		}
		else{
		if(!strcmp((char *)get_conf_elem(GPS_MOTION_THRESHOLD_CONF),"1") && GpsNoData()){
		dst = strcpyEx( strcpyEx( strcpyEx( gsmBuffer, "$FRCMD,"), GsmIMEI()), ",_SendGsmcell,,");
		send_size = frame_cell(dst,Command);
		}
		else{
		dst = strcpyEx( strcpyEx( strcpyEx( gsmBuffer, "$FRCMD,"), GsmIMEI()), ",_SendMessage,,");
		send_size = frame(dst,Command,false);
		}
		}
		}else
		{
			send_size = Ruptela_frame(dst,Command,false);
		}
		setAccState(ACC_OFF,false);
		result = E_NO_RESPONSE;
		
		}else if(Command == GSM_TASK_CMD_ACCON)
		{
		//setAccState(ACC_ON,true);
		if(strncmp(get_conf_elem(RESERVE3_CONF),"1",1)){
		if(!strncmp(get_conf_elem(WORKINGMODE_CONF),"0",1))
		{
		dst = strcpyEx( strcpyEx( strcpyEx( gsmBuffer, "$U,"), GsmIMEI()), ",1, ,");
		send_size = frame(dst,Command,false);
		}
		else{
		if(!strcmp((char *)get_conf_elem(GPS_MOTION_THRESHOLD_CONF),"1") && GpsNoData()){
		dst = strcpyEx( strcpyEx( strcpyEx( gsmBuffer, "$FRCMD,"), GsmIMEI()), ",_SendGsmcell,,");
		send_size = frame_cell(dst,Command);
		}
		else{
		dst = strcpyEx( strcpyEx( strcpyEx( gsmBuffer, "$FRCMD,"), GsmIMEI()), ",_SendMessage,,");
		send_size = frame(dst,Command,false);
		}
		}
		}else
		{
			send_size = Ruptela_frame(dst,Command,false);
		}
		setAccState(ACC_ON,false);
		result = E_NO_RESPONSE;
		
		}else if(Command == GSM_TASK_CMD_IN_N)
		{
		setInState(IN_N,true);
		if(strncmp(get_conf_elem(RESERVE3_CONF),"1",1)){
		if(!strncmp(get_conf_elem(WORKINGMODE_CONF),"0",1))
		{
			dst = strcpyEx( strcpyEx( strcpyEx( gsmBuffer, "$U,"), GsmIMEI()), ",1, ,");
			send_size = frame(dst,Command,false);
		}
		else{
		if(!strcmp((char *)get_conf_elem(GPS_MOTION_THRESHOLD_CONF),"1") && GpsNoData()){
		dst = strcpyEx( strcpyEx( strcpyEx( gsmBuffer, "$FRCMD,"), GsmIMEI()), ",_SendGsmcell,,");
		send_size = frame_cell(dst,Command);
		}
		else{
		dst = strcpyEx( strcpyEx( strcpyEx( gsmBuffer, "$FRCMD,"), GsmIMEI()), ",_SendMessage,,");
		send_size = frame(dst,Command,false);
		}
		}
		}else
		{
			send_size = Ruptela_frame(dst,Command,false);
		}
		//setInState(IN_N,false);
		result = E_NO_RESPONSE;
		
		}else if(Command == GSM_TASK_CMD_IN_P)
		{
		setInState(IN_P,true);
		if(strncmp(get_conf_elem(RESERVE3_CONF),"1",1)){
		if(!strncmp(get_conf_elem(WORKINGMODE_CONF),"0",1))
		{
		dst = strcpyEx( strcpyEx( strcpyEx( gsmBuffer, "$U,"), GsmIMEI()), ",1, ,");
		send_size = frame(dst,Command,false);
		}
		else{
		if(!strcmp((char *)get_conf_elem(GPS_MOTION_THRESHOLD_CONF),"1") && GpsNoData()){
		dst = strcpyEx( strcpyEx( strcpyEx( gsmBuffer, "$FRCMD,"), GsmIMEI()), ",_SendGsmcell,,");
		send_size = frame_cell(dst,Command);
		}
		else{
		dst = strcpyEx( strcpyEx( strcpyEx( gsmBuffer, "$FRCMD,"), GsmIMEI()), ",_SendMessage,,");
		send_size = frame(dst,Command,false);
		}
		}
		}else
		{
			send_size = Ruptela_frame(dst,Command,false);
		}
		//setInState(IN_P,false);
		result = E_NO_RESPONSE;
		
		}else if(Command == GSM_TASK_CMD_EH)
		{
		//initEH(task->Content);
		result = E_NO_RESPONSE;
		
		}else if(Command == GSM_TASK_CMD_ML)
		{
		//initML(task->Content);	
		result = E_NO_RESPONSE;
		}else if(Command == GSM_TASK_CMD_POINT)
		{
		geocheck_check(getPoint());
		result = E_NO_RESPONSE;
		}
		
		else if(Command == GSM_TASK_CMD_ALARM)
		{
		if(strncmp(get_conf_elem(RESERVE3_CONF),"1",1)){
		if(!strncmp(get_conf_elem(WORKINGMODE_CONF),"0",1))	
		dst = strcpyEx( strcpyEx( strcpyEx( gsmBuffer, "$U,"), GsmIMEI()), ",2, ,");
		else
		dst = strcpyEx( strcpyEx( strcpyEx( gsmBuffer, "$FRCMD,"), GsmIMEI()), ",_AlarmInfo,,");
		}
		if(!strncmp((char *)task->Content,"SosButton",strlen((char *)"SosButton"))){
			setAlarmState(ALARM_SOS,true);
			report_alarm_message("sos alarm");
		}
		else if(!strncmp((char *)task->Content,"BatteryLow",strlen((char *)"BatteryLow"))){
			setAlarmState(ALARM_BATTLE_LOW,true);
			report_alarm_message("batterylow alarm");
		}
		else if(!strncmp((char *)task->Content,"CutDetect",strlen((char *)"CutDetect"))){
			setAlarmState(ALARM_CUT_DETECT,true);
			report_alarm_message("cutdetect alarm");
		}
		else if(!strncmp((char *)task->Content,"OverSpeedWarn",strlen((char *)"OverSpeedWarn"))){
			setAlarmState(ALARM_OVERSPEED,true);
			report_alarm_message("overspeed alarm");
		}
		else if(!strncmp((char *)task->Content,"ShockWarn",strlen((char *)"ShockWarn"))){
			setAlarmState(ALARM_SHOCK,true);
			report_alarm_message("shock alarm");
		}else if(!strncmp((char *)task->Content,"AccOn",strlen((char *)"AccOn"))){
			report_alarm_message("AccOn alarm");
		}else if(!strncmp((char *)task->Content,"GpsState=0",strlen((char *)"GpsState=0"))){
			//report_alarm_message("AccOn alarm");
			setAlarmState(GPS_LOST,true);
		}else if(!strncmp((char *)task->Content,"GpsState=1",strlen((char *)"GpsState=1"))){
			//report_alarm_message("AccOn alarm");
			setAlarmState(GPS_FIXED,true);
		}else if(!strncmp((char *)task->Content,"JammingWarn",strlen((char *)"JammingWarn"))){
			//report_alarm_message("AccOn alarm");
			setAlarmState(ALARM_JAMMING,true);
		}
		
		if(strncmp(get_conf_elem(RESERVE3_CONF),"1",1)){
		if(!strcmp((char *)get_conf_elem(GPS_MOTION_THRESHOLD_CONF),"1") && GpsNoData())
		send_size = frame_cell(dst,Command);
		else
		send_size = frame(dst,Command,false);
		}else
		{
			send_size = Ruptela_frame(dst,Command,false);
		}

		setAlarmState(ALARM_SOS,false);
		setAlarmState(ALARM_BATTLE_LOW,false);
		setAlarmState(ALARM_CUT_DETECT,false);
		setAlarmState(ALARM_OVERSPEED,false);
		setAlarmState(ALARM_SHOCK,false);
		setAlarmState(GPS_LOST,false);
		setAlarmState(GPS_FIXED,false);
		setAlarmState(ALARM_JAMMING,false);
		
		result = E_NO_RESPONSE;
		}
		else if(Command == GSM_TASK_CMD_GEOFENCE)
		{
			if(strncmp(get_conf_elem(RESERVE3_CONF),"1",1)){
			if(!strncmp(get_conf_elem(WORKINGMODE_CONF),"0",1))	
			dst = strcpyEx( strcpyEx( strcpyEx( gsmBuffer, "$U,"), GsmIMEI()), ",2, ,");
			else
			dst = strcpyEx( strcpyEx( strcpyEx( gsmBuffer, "$FRCMD,"), GsmIMEI()), ",_AlarmInfo,,");
			if(!strcmp((char *)get_conf_elem(GPS_MOTION_THRESHOLD_CONF),"1") && GpsNoData())
			send_size = frame_cell(dst,Command);
			else
			send_size = frame(dst,Command,false);
			}else
			{
			send_size = Ruptela_frame(dst,Command,false);
			}
		}else if(Command == GSM_TASK_CMD_SYNC)
		{
		if(isJTSimcard()){
		 cmd = setupBysms_toSync();
		 if (cmd != NULL){
			 if(DebugEnable) DEBUG("cmd->Command %s",cmd->Command);
		(cmd->Response)(gsmBuffer, sizeof(gsmBuffer), false, cmd);
		 send_size = 1;
		}
		}
		}
		else if (Command == GSM_TASK_CMD_PING)
		{
			if(pingTime < 11)
			pingTime++;
			
			if(strncmp(get_conf_elem(WORKINGMODE_CONF),"0",1) && !strncmp(get_conf_elem(ACCSTATUSHINT_CONF),"1",1)){
			//if(strncmp(get_conf_elem(RESERVE3_CONF),"1",1))
			//dst = strcpyEx(strcpyEx(strcpyEx(strcpyEx(gsmBuffer, "$FRCMD,"), GsmIMEI()), ",_Ping,"),getADCVolt());
			//else
			if(strncmp(get_conf_elem(RESERVE3_CONF),"1",1)){
			dst = strcpyEx(strcpyEx(strcpyEx(gsmBuffer, "$FRCMD,"), GsmIMEI()), ",_Ping");
			//dst = itoa(dst, task->SenseD1, 1);
			//dst = strcpyEx(dst, VERSION);

#if (USE_CAN_SUPPORT)
			// Format: obd=N,value[,value]
			//	N		number of values
			//	value	CAN parameter value
			dst = strcpyEx(dst, ",obd=");
			// dst = itoa(dst, CAN_VALUES_SIZE, 0);

			for (idx = 0; idx < CAN_VALUES_SIZE; idx ++)
				dst = D2IToString(dst, task->CanData[idx], 3);

			--dst;
#endif
			// Add NMEA checksum
			send_size = NmeaAddChecksum(dst, gsmBuffer);
			pingTime = 0;
			result = E_OK;
			}else
			{
			send_size = Ruptela_frame_ping(dst);
			pingTime = 0;
			result = E_NO_RESPONSE;
			}
			
			}else if(strncmp(get_conf_elem(ACCSTATUSHINT_CONF),"1",1))
			{
				gsmSend(GSM_CMGL);
				memset(localwriteBuffer,0,BUFFER_LENGTH/4);
			}
		}else if(Command == GSM_TASK_CMD_LOGIN){
			if(!isAccOn() && ((GlobalStatus & GSM_LOGIN) != 0)){
				gc->Status |= GSM_STATUS_TCP_ON;
				result = E_NO_RESPONSE;
				return result;
			}
			if(strncmp(get_conf_elem(RESERVE3_CONF),"1",1)){
			if(strncmp(get_conf_elem(IAP_CONF),"-b",2))
			dst = strcpyEx(strcpyEx(strcpyEx(strcpyEx(gsmBuffer, "$FRLIN,IMEI,"), GsmIMEIIMSI()), ","),(int8_t*)get_conf_elem(SW_VERSION_CONF));
			else
			dst = strcpyEx(strcpyEx(strcpyEx(strcpyEx(strcpyEx(gsmBuffer, "$FRLIN,IMEI,"), GsmIMEIIMSI()), ","),(int8_t*)get_conf_elem(SW_VERSION_CONF)),(int8_t*)get_conf_elem(IAP_CONF));
			send_size = NmeaAddChecksum(dst, gsmBuffer);
			//login = true;
			}else
			{
			gc->Status &= ~GSM_STATUS_TCP_ON;
			login = true;
			loginWaitresponse = 0;
			send_size = Ruptela_frame_login(dst);
			}
			result = E_OK;
			GlobalStatus |= GSM_LOGIN;
		}else if(Command == GSM_TASK_CMD_NEW_PING)
		{
			if(strncmp(get_conf_elem(RESERVE3_CONF),"1",1)){
			dst = strcpyEx(strcpyEx(strcpyEx(gsmBuffer, "$FRCMD,"), GsmIMEI()), ",_Ping");
			send_size = NmeaAddChecksum(dst, gsmBuffer);
			}else
			{
			send_size = Ruptela_frame_ping(dst);
			}
			result = E_NO_RESPONSE;
		}else if(Command == GSM_TASK_CMD_REQUEST)
		{
			dst = strcpyEx(strcpyEx(strcpyEx(gsmBuffer, "$FRCMD,"), GsmIMEI()), ",_RequestDate");
			send_size = NmeaAddChecksum(dst, gsmBuffer);
			result = E_NO_RESPONSE;
		}
		else if(Command == GSM_TASK_CMD_RESEND_DATA)
		{
				int8_t str[20];
				int32_t length = 0;
				//length = ((uint32_t)(localwriteBuffer[0] << 8 ))|((uint32_t)localwriteBuffer[1])+4;
				length = localwriteBuffer[0];
				if(length > 0){
				memset(str,0,20);
				if(!isM35)
				sprintf(str, "AT+QISEND=1,%d\r\n",length);
				else
				sprintf(str, "AT+QISEND=%d\r\n",length);
				gsmSend(str);
				osDelay(3000);
				gsmSendHex(localwriteBuffer+1,length);
				gsmSend("\x1A\x1B");
				osDelay(1000);
			  }
			result = E_NO_RESPONSE;
		}
		else
		{
			result = E_NO_RESPONSE;
			if ((cmd = task->GG_Command) != NULL)
			{
				if (Command == GSM_TASK_CMD_RET)
				{
					if (cmd->Response != NULL
					&&	(cmd->Response)(gsmBuffer, sizeof(gsmBuffer), false, cmd) == E_OK
						)
						send_size = 1;
					if (task->SmsIndex != 0)
					{
						if (cmd->Counts == NULL
						||	(cmd->Response != NULL && (cmd->Response)(NULL, task->SmsIndex, true, cmd) == E_OK)
							)
						{	// Add post processing SMS
							task->Command = GSM_TASK_CMD_SMS;
							GsmAddTask(task);
						}
					}
				}
				else if (Command == GSM_TASK_CMD_ERR)
				{
					dst = strcpyEx( strcpyEx( gsmBuffer, DGG_FRERR ), GsmIMEI() );
					*dst++ = ',';
					send_size = NmeaAddChecksum(strcpyEx(dst, cmd->Command), gsmBuffer);

					if (task->SmsIndex != 0)
					{	// Add post processing SMS
						task->Command = GSM_TASK_CMD_SMS;
						GsmAddTask(task);
					}
				}
			}
		}
		
		if (send_size != 0)
		{
			#if 0
			if(!strncmp(get_conf_elem(WORKINGMODE_CONF),"0",1)){
			if(DebugEnable) DEBUG("length:%d",strlen(gsmBuffer));
			memset(localwriteBuffer,0,BUFFER_LENGTH/4);
			memcpy(localwriteBuffer,gsmBuffer,strlen(gsmBuffer));
			memset(Ussd,0,256);
			sprintf(Ussd, "%s,\"%s%s#\"", "AT+CUSD=1",ussd_payload_format,gsmBuffer);
			if(strlen(Ussd)>134)
				return E_NO_RESPONSE;
			//if(DebugEnable) DEBUG("%s",Ussd);
		  //gsmSend("AT+CUSD=1,\"#469*01*$FRCMD,865789024202212,_Ping,VOL=12652*7D#\"\r\n");
			gsmSend(Ussd);
			osDelay(100);
			gsmSend("\r\n");
			return E_NO_RESPONSE;
			}
			#endif
			if(getTCPConState()||(!strncmp(get_conf_elem(RESERVE3_CONF),"1",1) && Command == GSM_TASK_CMD_LOGIN)){
			setTime_noupdate();
			osDelay(1000);
			memset(localwriteBuffer,0,BUFFER_LENGTH/4);
			if(Command != GSM_TASK_CMD_LOGIN && Command != GSM_TASK_CMD_PING && Command != GSM_TASK_CMD_NEW_PING){
			if(strncmp(get_conf_elem(RESERVE3_CONF),"1",1)){
			memcpy(localwriteBuffer,gsmBuffer,strlen(gsmBuffer));
			}else
			{
			memcpy(localwriteBuffer,gsmBuffer,BUFFER_LENGTH/4);
			}
			}
			(*gc->Commands->TcpSend)();
			if((cmd = task->GG_Command) != NULL && Command == GSM_TASK_CMD_RET){
						 if(!strcmp(cmd->Command,"_Recover")||!strcmp(cmd->Command,"_Reset")||!strcmp(cmd->Command,"_UssdTcpUdp")){
						if(DebugEnable) DEBUG("Command %s",cmd->Command);
							osDelay(5000);
						  SystemRestart();
						 }else if(!strcmp(cmd->Command,"_Update"))
						 {
							osDelay(1000);
						 }else if(!strcmp(cmd->Command,"_Setup")){
							 if(getresetIndex() == 2){
								  osDelay(5000);
									SystemRestart();
							 }
						 }
				}
			
			if(!strcmp((char *)get_conf_elem(RESERVE6_CONF),"1")){
				if(Command == GSM_TASK_CMD_RTC){
						rtcReport = false; 
						sleep_time = 0;
						memset(localwriteBuffer,0,BUFFER_LENGTH/4);
						osDelay(2000);
						setEvent(GSENSOR_SLEEP_EVENT);
				}
			}else if(!strcmp((char *)get_conf_elem(RESERVE6_CONF),"2"))
			{
				if(Command == GSM_TASK_CMD_ACCOFF || Command == GSM_TASK_CMD_RTC){
						rtcReport = false;
						sleep_time = 0;
						memset(localwriteBuffer,0,BUFFER_LENGTH/4);
						osDelay(2000);
						setEvent(GSENSOR_SLEEP_EVENT);
				}
			}
				
			//if(!strncmp(get_conf_elem(RESERVE3_CONF),"1",1) && Command == GSM_TASK_CMD_LOGIN)
					//gc->Status &= ~GSM_STATUS_TCP_ON;
			}
			else
			{
				#if 0
				//if(task->Command != GSM_TASK_CMD_PING && task->Command != GSM_TASK_CMD_LOGIN){
				//if(DebugEnable) DEBUG("***************writefile");
					//writefile(LOCATION_DATA,gsmBuffer);
					//bit_map_write_data(gsmBuffer,BITMAP_FLASH_DATA_LEN);
				}
				#endif
			}
		}
		else
			result = E_NO_RESPONSE;
	}
	else
	{
		if(connectNetFail < 10)
		GsmAddTask(task);
		else if(connectNetFail%60 == 0)
		GsmAddTask(task);
		//ShowError(ERROR_GSM_TCP_NOT_OPEN);
		result = E_GSM_TCP_NOT_OPEN;
	}

	return result;
}
uint8_t jammingStatus = 0;
uint8_t jammingTimes = 0;
/*******************************************************************************
* Function Name	:	gsmProcessHostResponse
* Description	:	Process command from host
*******************************************************************************/
void gsmProcessHostResponse(GsmContext_t * gc, bool disconnect)
{
	bool reply = true;
	const GgCommand_t *cmd;
	uint8_t timeout = 30;
	uint32_t messageNum = 0;
	if(DebugEnable) DEBUG("gsmProcessHostResponse");
	while(reply)
	{
		if (gsmGetHostLine(gc, (disconnect ? 6000 : 1000)) != E_OK)
		{
			//if(DebugEnable) DEBUG("gsmBuffe1 %s",gsmBuffer);
			if(!strncmp(get_conf_elem(RESERVE3_CONF),"1",1)){
				if(login || ackResponse){
				loginWaitresponse++;
				if((loginWaitresponse > 10 && disconnect) 
					|| (loginWaitresponse > 60 && !disconnect)){
					loginWaitresponse = 0;
					if(DebugEnable) DEBUG("no response timeout 1");
					gsmDisconnectTCP(gc);
					break;
				}
				osDelay(100);
				continue;
				}else
				{
				if (disconnect)
					gsmDisconnectTCP(gc);
					break;
				}
			}else
			{
			if (disconnect)
				gsmDisconnectTCP(gc);
				break;
			}
		}else {
			
			if(!strncmp(get_conf_elem(RESERVE3_CONF),"1",1)){
				//if(login){
					//if(strncmp((char*)gsmBuffer,"7B0001",strlen("7B0001"))){
				//uint8_t* p  = gsmBuffer + 2;
				//sscanf(p, "%x", &messageNum);
				//if(DebugEnable) DEBUG("%02x,%02x,%02x",gsmBuffer[0],gsmBuffer[1],gsmBuffer[2]);
				messageNum = gsmBuffer[1] << 8 | gsmBuffer[2];
				//if(DebugEnable) DEBUG("%02x,%02x",messageNum,read_message_number());
				//if((login && strncmp((char*)gsmBuffer,"7B0001",strlen("7B0001")))
				if((login && gsmBuffer[0] != 0x7B && gsmBuffer[1] != 0x00 && gsmBuffer[2] != 0x01)
					|| (ackResponse && (messageNum != read_message_number()))){
					//if(DebugEnable) DEBUG("messageNum %02x %02x",messageNum,gsmBuffer[2]);
					
					if(!strncmp((char*)gsmBuffer,"+CMT:",strlen("+CMT:")))
					{
					getSmsNumber(gsmBuffer);
					new_sms_incoming = true;
					continue;
					}
		
					if(new_sms_incoming){
					new_sms_incoming = false;
					if(handleSmsCommand(gsmBuffer,false,false) != 0)
					{
					if(getTCPConState()){
					GsmTask.Command = GSM_TASK_CMD_SYNC;
					GsmAddTask(&GsmTask);
					}
					}
					gsmRxFlush(gc);
					}
					
					loginWaitresponse++;
					if((loginWaitresponse > 10 && disconnect) 
					|| (loginWaitresponse > 60 && !disconnect)){
					loginWaitresponse = 0;
					if(DebugEnable) DEBUG("no response timeout 2");
					gsmDisconnectTCP(gc);
					break;
				}	
						osDelay(500);
						continue;
					}
				//}
			}
		}
			
		//if(DebugEnable) DEBUG("gsmBuffe %s",gsmBuffer);
		
		// Ignore empty response string
		if (strlen((char*)gsmBuffer) == 0){
			if(!strncmp(get_conf_elem(RESERVE3_CONF),"1",1)){
				if(login){
				loginWaitresponse++;
					
				if(!strncmp((char*)gsmBuffer,"+CMT:",strlen("+CMT:")))
				{
					getSmsNumber(gsmBuffer);
					new_sms_incoming = true;
					continue;
				}
		
				if(new_sms_incoming){
					new_sms_incoming = false;
				  if(handleSmsCommand(gsmBuffer,false,false) != 0)
					{
					if(getTCPConState()){
					GsmTask.Command = GSM_TASK_CMD_SYNC;
					GsmAddTask(&GsmTask);
					}
					}
					gsmRxFlush(gc);
				}	
				
				if((loginWaitresponse > 10 && disconnect) 
					|| (loginWaitresponse > 60 && !disconnect)){
					loginWaitresponse = 0;
					if(DebugEnable) DEBUG("no response timeout 0");
					gsmDisconnectTCP(gc);
					break;
				}
				osDelay(500);
				continue;
				}else
				{
				timeout--;
				if(timeout == 0)
					break;
				continue;
				}
			}else{
			timeout--;
			if(timeout == 0)
				break;
			continue;
			}
		}
		
			
		//if(DebugEnable) DEBUG("gsmRxBuffer1 %s",gsmRxBuffer);
		if(DebugEnable) DEBUG("gsmBuffe %s",gsmBuffer);
		//if(DebugEnable) DEBUG("gsmRxBuffer3 %s",gsmRxBuffer);
		
		if(!strncmp((char*)gsmBuffer,GSM_CGREG00,strlen(GSM_CGREG00))
			|| !strncmp((char*)gsmBuffer,GSM_CGREG02,strlen(GSM_CGREG02))
				|| !strncmp((char*)gsmBuffer,GSM_CGREG03,strlen(GSM_CGREG03))
					|| !strncmp((char*)gsmBuffer,GSM_CGREG04,strlen(GSM_CGREG04)))
		{
			if(strncmp(get_conf_elem(RESERVE3_CONF),"1",1)){
			if(strlen(localwriteBuffer) > 0 && !strncmp(localwriteBuffer,DGG_FRCMD,7))
			bit_map_write_data(localwriteBuffer,BITMAP_FLASH_DATA_LEN);
			}else
			{
			bit_map_write_data(localwriteBuffer,BITMAP_FLASH_DATA_LEN);
			}
			memset(localwriteBuffer,0,BUFFER_LENGTH/4);
			gsmDisconnectTCP(gc);
			break;
		}
		
		if(!strncmp((char*)gsmBuffer,"SEND OK",strlen("SEND OK")))
		{
				testSignal = 0;
				ackTimeout++;
				osDelay(1000);
				#if 0
				if(!strncmp(get_conf_elem(WORKINGMODE_CONF),"2",1)){
				if(!isM35)
				gsmSend("AT+QISTATE=1,1\r\n");
				else
				gsmSend("AT+QISTAT\r\n");
				}
				else
				#endif
				(*gc->Commands->TcpAck)();
				break;
		}
		
		if(!strncmp((char*)gsmBuffer,"+QISTATE:",strlen("+QISTATE:"))
			|| !strncmp((char*)gsmBuffer,"STATE:",strlen("STATE:") ))
		{
			testSignal = 0;
			if(!isM35){
			int8_t*	item = (int8_t*)strchr((char *)gsmBuffer,',');
			item +=1;
			item = (int8_t*)strchr((char *)item,',');
			item +=1;
			item = (int8_t*)strchr((char *)item,',');
			item +=1;
			item = (int8_t*)strchr((char *)item,',');
			item +=1;
			item = (int8_t*)strchr((char *)item,',');
			item +=1;
			if(strncmp(item,"3",1)){
			gsmDisconnectTCP(gc);
			break;
			}
			}else
			{
				int8_t*	item = (int8_t*)strchr((char *)gsmBuffer,':');
				item +=2;
				if(strcmp(item,"CONNECT OK")){
				gsmDisconnectTCP(gc);
				break;
				}
			}
		}
		
		if(!strncmp((char*)gsmBuffer,"+QISEND:",strlen("+QISEND:"))
			||!strncmp((char*)gsmBuffer,"+QISACK:",strlen("+QISACK:"))
		)
		{
			
			if(isM35 && !strncmp(get_conf_elem(WORKINGMODE_CONF),"2",1)){
				testSignal = 0;
				break;
			}
			
			int8_t* p = (int8_t*)strchr((char *)gsmBuffer,',');
			if(p == NULL)
				break;
			p+=1;
			p = (int8_t*)strchr((char *)p,',');
			if(p == NULL)
				break;
			p+=1;
			//if(DebugEnable) DEBUG("p %s",p);
			
			if(!strncmp(p,"0",1) ||!strncmp(p," 0",2))
			{
			testSignal = 0;
			ackTimeout = 0;
			}else{
			if(strncmp(get_conf_elem(RESERVE3_CONF),"1",1)){
			//if(strlen(localwriteBuffer) > 0 && !strncmp(localwriteBuffer,DGG_FRCMD,7))
			//bit_map_write_data(localwriteBuffer,BITMAP_FLASH_DATA_LEN);
			}else
			{
			//bit_map_write_data(localwriteBuffer,BITMAP_FLASH_DATA_LEN);
			}
			//memset(localwriteBuffer,0,BUFFER_LENGTH/4);
			testSignal = 0;
			}
			
			if(strncmp(p,"0",1) && ackTimeout > 20){
					gsmDisconnectTCP(gc);
			}
			if( ackTimeout > 20)
					 ackTimeout = 0;
				break;
		}
		
		//if(!strncmp((char*)gsmBuffer,"7B0000",strlen("7B0000")))
		if(gsmBuffer[0] == 0x7B && gsmBuffer[1] == 0x00 && gsmBuffer[2]== 0x00)
		{
			if(DebugEnable) DEBUG("BH LOGIN FAIL");
			memset(gsmRxBuffer,0,BUFFER_LENGTH+32);
			loginWaitresponse = 0;
			gsmDisconnectTCP(gc);
			break;
		}//else if(!strncmp((char*)gsmBuffer,"7B0001",strlen("7B0001")))
		else if(gsmBuffer[0] == 0x7B && gsmBuffer[1] == 0x00 && gsmBuffer[2]== 0x01)
		{
			if(DebugEnable) DEBUG("BH LOGIN SUCCESS");
			gc->Status |=  GSM_STATUS_TCP_ON;
			loginWaitresponse = 0;
			if(localwriteBuffer[0] == 0x13)
			memset(localwriteBuffer,0,BUFFER_LENGTH/4);
			GsmTask.Command = GSM_TASK_CMD_RESEND_DATA;
			GsmAddTask(&GsmTask);
			login = false;
			break;
		}
		
		if(!strncmp((char*)gsmBuffer,"+CMGL:",strlen("+CMGL:")))
		{
				gsmDisconnectTCP(gc);
				break;
		}
			
		if(!strncmp((char*)gsmBuffer,"+QIURC: \"pdpdeact\"",strlen("+QIURC: \"pdpdeact\""))
			|| !strncmp((char*)gsmBuffer,"+CPIN: NOT READY",strlen("+CPIN: NOT READY"))
			|| !strncmp((char*)gsmBuffer,"SEND FAIL",strlen("SEND FAIL"))				
			|| !strncmp((char*)gsmBuffer,"ERROR",strlen("ERROR")) 
			|| !strncmp((char*)gsmBuffer,"+QIURC: \"closed\"",strlen("+QIURC: \"closed\""))){
				if(strncmp(get_conf_elem(RESERVE3_CONF),"1",1)){
				if(strlen(localwriteBuffer) > 0 && !strncmp(localwriteBuffer,DGG_FRCMD,7))
				bit_map_write_data(localwriteBuffer,BITMAP_FLASH_DATA_LEN);
				}else
				{
				bit_map_write_data(localwriteBuffer,BITMAP_FLASH_DATA_LEN);
				}
				memset(localwriteBuffer,0,BUFFER_LENGTH/4);
				gsmDisconnectTCP(gc);
				break;
			}else if(strncmp((char*)gsmBuffer,">",1))
			{
			//if(strlen(localwriteBuffer) > 0)
			//memset(localwriteBuffer,0,strlen(localwriteBuffer));
			}
			
			
	if(!strncmp((char*)gsmBuffer,"+QJDR: JAMMED",strlen("+QJDR: JAMMED")))
	{
		jammingTimes++;
		if(jammingTimes < 3)
			break;
		if(jammingStatus == 0){
		jammingStatus = 1;
		jammingEvent();
		start_pwm_out1();
		start_pwm_out2();
		}
		break;
	}else if(!strncmp((char*)gsmBuffer,"+QJDR: NO JAMMING",strlen("+QJDR: NO JAMMING")))
	{
		jammingTimes = 0;
		if(jammingStatus == 1){
		jammingStatus = 0;
		stop_pwm_out1();
		stop_pwm_out2();
		}
		break;
	}
			
	if(!strncmp((char*)gsmBuffer,"+QIURC:",strlen("+QIURC:")))
				continue;
	
	#if 0
	if(!strncmp((char*)gsmBuffer,"+CCLK:",strlen("+CCLK:")))
	{
		int8_t timebuff[5];
		int8_t settime[25];
		nmeaTIME time;
		int8_t* p = (int8_t*)strchr((char *)gsmBuffer,'"');
		p+=1;
		int8_t* pEnd = (int8_t*)strchr((char *)p,'/');
		memset(timebuff,0,5);
		strncpy((char *)timebuff,p,pEnd-p);
		
		time.year =atoi(timebuff);
		
		
		p = (int8_t*)strchr((char *)p,'/');
		p+=1;
		pEnd = (int8_t*)strchr((char *)p,'/');
		memset(timebuff,0,5);
		strncpy((char *)timebuff,p,pEnd-p);
		
		time.mon =atoi(timebuff);
		
		
		p = (int8_t*)strchr((char *)p,'/');
		p+=1;
		pEnd = (int8_t*)strchr((char *)p,',');
		memset(timebuff,0,5);
		strncpy((char *)timebuff,p,pEnd-p);
		
		time.day = atoi(timebuff);
		
		
		p = (int8_t*)strchr((char *)p,',');
		p+=1;
		pEnd = (int8_t*)strchr((char *)p,':');
		memset(timebuff,0,5);
		strncpy((char *)timebuff,p,pEnd-p);
		
		time.hour = atoi(timebuff);
		
		
		p = (int8_t*)strchr((char *)p,':');
		p+=1;
		pEnd = (int8_t*)strchr((char *)p,':');
		memset(timebuff,0,5);
		strncpy((char *)timebuff,p,pEnd-p);
		
		time.min = atoi(timebuff);
		
		
		p = (int8_t*)strchr((char *)p,':');
		p+=1;
		pEnd = (int8_t*)strchr((char *)p,'+');
		memset(timebuff,0,5);
		strncpy((char *)timebuff,p,pEnd-p);
		
		time.sec = atoi(timebuff);
		
		if(time.year != 4 && time.mon != 1 && time.day != 1){
			time.year +=2000;
			RTC_TIME(time,true);
		}
		
		if(time.year == 4 && time.mon == 1 && time.day == 1 && !GpsNoData())
		{
			nmeaGPRMC * rmc = getLastGPRMCData();
			memset(settime,0,25);
			sprintf((char*)settime,"AT+CCLK=\"%02d/%02d/%02d,%02d:%02d:%02d+00\"\r\n",rmc->utc.year-2000, rmc->utc.mon, rmc->utc.day,rmc->utc.hour,rmc->utc.min,rmc->utc.sec);
			if(DebugEnable) DEBUG("%s",settime);
			gsmSend(settime);
		}
	
		break;

	}
	#endif
	
			
	 if(!strncmp((char*)gsmBuffer,"+CUSD:",strlen("+CUSD:")))
		{
			int8_t* p = (int8_t*)strchr((char *)gsmBuffer,'"');
			if(p == NULL)
				break;
			p+=1;
			int8_t* pEnd = (int8_t*)strchr((char *)p,'"');
			if(pEnd == NULL)
				break;
			strncpy((char *)localwriteBuffer,p,pEnd-p);
			if(DebugEnable) DEBUG("CUSD:%s",localwriteBuffer);
			break;
		}
			
			
		if(!strncmp((char*)gsmBuffer,"+CSQ:",strlen("+CSQ:")))
		{
			int8_t* p = (int8_t*)strchr((char *)gsmBuffer,',');
			memset(signal,0,4);
			strncpy((char *)signal,(char*)gsmBuffer+strlen("+CSQ: "),p-(gsmBuffer+strlen("+CSQ: ")));
			if(DebugEnable) DEBUG("signal2:%s",signal);
			testSignal = 0;
			break;
		}
		
		
		if(!strncmp((char*)gsmBuffer,"+CBC:",strlen("+CBC:")))
		{
			int8_t* p = (int8_t*)strchr((char *)gsmBuffer,',');
			p+=1;
			p = (int8_t*)strchr((char *)p,',');
			p+=1;
			memset(battVolt,0,20);
			strcpy((char *)battVolt,p);
			if(DebugEnable) DEBUG("battVolt:%s",battVolt);
			break;
		}
#if 1		
		if(!strncmp((char*)gsmBuffer,"+QENG:",strlen("+QENG:")))
		{
			//if(DebugEnable) DEBUG("%s",gsmBuffer);
			int8_t* p = (int8_t*)strchr((char *)gsmBuffer,'"');
			if(p == NULL)
			p = (int8_t*)strchr((char *)gsmBuffer,':');	
			p+=1;
			//if(DebugEnable) DEBUG("p %s",p);
			if(!strncmp(p,"servingcell",strlen("servingcell")))
			{
			sendGsmCell = true;
			memset(&cellinfo,0,sizeof(Cellinfo));
			p = (int8_t*)strchr((char *)gsmBuffer,',');
			if(p == NULL)
			break;
			p+=1;
			p = (int8_t*)strchr((char *)p,',');
			if(p == NULL)
			break;
			p+=1;
			p = (int8_t*)strchr((char *)p,',');
			if(p == NULL)
			break;
			p+=1;
			int8_t* pEnd = (int8_t*)strchr((char *)p,',');
			if(pEnd == NULL)
			break;
			memcpy(cellinfo.MCC,p,pEnd-p);
			p = (int8_t*)strchr((char *)p,',');
			if(p == NULL)
			break;
			p+=1;
			pEnd = (int8_t*)strchr((char *)p,',');
			if(pEnd == NULL)
			break;
			memcpy(cellinfo.MNC,p,pEnd-p);
			p = (int8_t*)strchr((char *)p,',');
			if(p == NULL)
			break;
			p+=1;
			pEnd = (int8_t*)strchr((char *)p,',');
			if(pEnd == NULL)
			break;
			memcpy(cellinfo.LAC,p,pEnd-p);
			p = (int8_t*)strchr((char *)p,',');
			if(p == NULL)
			break;
			p+=1;
			pEnd = (int8_t*)strchr((char *)p,',');
			if(pEnd == NULL)
			break;
			memcpy(cellinfo.cellid,p,pEnd-p);
			p = (int8_t*)strchr((char *)p,',');
			if(p == NULL)
			break;
			p+=1;
			p = (int8_t*)strchr((char *)p,',');
			if(p == NULL)
			break;
			p+=1;
			p = (int8_t*)strchr((char *)p,',');
			if(p == NULL)
			break;
			p+=1;
			p = (int8_t*)strchr((char *)p,',');
			if(p == NULL)
			break;
			p+=1;
			pEnd = (int8_t*)strchr((char *)p,',');
			if(pEnd == NULL)
			break;
		  memcpy(cellinfo.RXL,p,pEnd-p);
			GsmTask.Command = GSM_TASK_CMD_GSMCELL_NEIGHBOUR;
			GsmAddTask(&GsmTask);
			}else if(!strncmp(p,"neighbourcell",strlen("neighbourcell")))
			{
			p = (int8_t*)strchr((char *)gsmBuffer,',');
			if(p == NULL)
				break;
			p+=1;
			p = (int8_t*)strchr((char *)p,',');
			if(p == NULL)
				break;
			p+=1;
			int8_t* pEnd = (int8_t*)strchr((char *)p,',');
			if(pEnd == NULL)
			break;
			if(!strlen(cellinfo.MCC1))
			memcpy(cellinfo.MCC1,p,pEnd-p);
			else if(!strlen(cellinfo.MCC2))
			memcpy(cellinfo.MCC2,p,pEnd-p);
			else if(!strlen(cellinfo.MCC3))
			memcpy(cellinfo.MCC3,p,pEnd-p);
			
			p = (int8_t*)strchr((char *)p,',');
			if(p == NULL)
			break;
			p+=1;
			pEnd = (int8_t*)strchr((char *)p,',');
			if(pEnd == NULL)
			break;
			if(!strlen(cellinfo.MNC1))
			memcpy(cellinfo.MNC1,p,pEnd-p);
			else if(!strlen(cellinfo.MNC2))
			memcpy(cellinfo.MNC2,p,pEnd-p);
			else if(!strlen(cellinfo.MNC3))
			memcpy(cellinfo.MNC3,p,pEnd-p);
		
			p = (int8_t*)strchr((char *)p,',');
			if(p == NULL)
			break;
			p+=1;
			pEnd = (int8_t*)strchr((char *)p,',');
			if(pEnd == NULL)
			break;
			if(!strlen(cellinfo.LAC1))
			memcpy(cellinfo.LAC1,p,pEnd-p);
			else if(!strlen(cellinfo.LAC2))
			memcpy(cellinfo.LAC2,p,pEnd-p);
			else if(!strlen(cellinfo.LAC3))
			memcpy(cellinfo.LAC3,p,pEnd-p);
		
			p = (int8_t*)strchr((char *)p,',');
			if(p == NULL)
			break;
			p+=1;
			pEnd = (int8_t*)strchr((char *)p,',');
			if(pEnd == NULL)
			break;
			if(!strlen(cellinfo.cellid1))
			memcpy(cellinfo.cellid1,p,pEnd-p);
			else if(!strlen(cellinfo.cellid2))
			memcpy(cellinfo.cellid2,p,pEnd-p);
			else if(!strlen(cellinfo.cellid3))
			memcpy(cellinfo.cellid3,p,pEnd-p);
		
			p = (int8_t*)strchr((char *)p,',');
			if(p == NULL)
			break;
			p+=1;
			p = (int8_t*)strchr((char *)p,',');
			if(p == NULL)
			break;
			p+=1;
			p = (int8_t*)strchr((char *)p,',');
			if(p == NULL)
			break;
			p+=1;
			pEnd = (int8_t*)strchr((char *)p,',');
			if(pEnd == NULL)
			break;
		  if(!strlen(cellinfo.RXL1))
			memcpy(cellinfo.RXL1,p,pEnd-p);
			else if(!strlen(cellinfo.RXL2))
			memcpy(cellinfo.RXL2,p,pEnd-p);
			else if(!strlen(cellinfo.RXL3))
			memcpy(cellinfo.RXL3,p,pEnd-p);
			
			if(strlen(cellinfo.RXL3) && sendGsmCell){
			sendGsmCell = false;
			GsmTask.Command = GSM_TASK_CMD_GSMCELL_SEND;
			GsmAddTask(&GsmTask);
			}
			}else if(!strncmp(p," 0",2))
			{
			memset(&cellinfo,0,sizeof(Cellinfo));
			p = (int8_t*)strchr((char *)gsmBuffer,',');
			if(p == NULL)
				return;
			p+=1;
			int8_t* pEnd = (int8_t*)strchr((char *)p,',');
			if(pEnd == NULL)
			break;
			memcpy(cellinfo.MCC,p,pEnd-p);
			p = (int8_t*)strchr((char *)p,',');
			if(p == NULL)
			break;
			p+=1;
			pEnd = (int8_t*)strchr((char *)p,',');
			if(pEnd == NULL)
			break;
			memcpy(cellinfo.MNC,p,pEnd-p);
			p = (int8_t*)strchr((char *)p,',');
			if(p == NULL)
			break;
			p+=1;
			pEnd = (int8_t*)strchr((char *)p,',');
			if(pEnd == NULL)
			break;
			memcpy(cellinfo.LAC,p,pEnd-p);
			p = (int8_t*)strchr((char *)p,',');
			if(p == NULL)
			break;
			p+=1;
			pEnd = (int8_t*)strchr((char *)p,',');
			if(pEnd == NULL)
			break;
			memcpy(cellinfo.cellid,p,pEnd-p);
			p = (int8_t*)strchr((char *)p,',');
			if(p == NULL)
			break;
			p+=1;
			p = (int8_t*)strchr((char *)p,',');
			if(p == NULL)
			break;
			p+=1;
			p = (int8_t*)strchr((char *)p,',');
			if(p == NULL)
			break;
			p+=1;
			pEnd = (int8_t*)strchr((char *)p,',');
			if(pEnd == NULL)
			break;
		  memcpy(cellinfo.RXL,p,pEnd-p);
			}else if(!strncmp(p," 1",2))
			{
			p = (int8_t*)strchr((char *)gsmBuffer,',');
			if(p == NULL)
			break;
			p+=1;
			p = (int8_t*)strchr((char *)p,',');
			if(p == NULL)
			break;
			p+=1;
			p = (int8_t*)strchr((char *)p,',');
			if(p == NULL)
			break;
			p+=1;
			int8_t*	pEnd = (int8_t*)strchr((char *)p,',');
			if(pEnd == NULL)
			break;
		  memcpy(cellinfo.RXL1,p,pEnd-p);
			p = (int8_t*)strchr((char *)p,',');
			if(p == NULL)
			break;
			p+=1;
			p = (int8_t*)strchr((char *)p,',');
			if(p == NULL)
			break;
			p+=1;
			p = (int8_t*)strchr((char *)p,',');
			if(p == NULL)
			break;
			p+=1;
			p = (int8_t*)strchr((char *)p,',');
			if(p == NULL)
			break;
			p+=1;
			pEnd = (int8_t*)strchr((char *)p,',');
			if(pEnd == NULL)
			break;
			memcpy(cellinfo.MCC1,p,pEnd-p);
			p = (int8_t*)strchr((char *)p,',');
			if(p == NULL)
			break;
			p+=1;
			pEnd = (int8_t*)strchr((char *)p,',');
			if(pEnd == NULL)
			break;
			memcpy(cellinfo.MNC1,p,pEnd-p);
			p = (int8_t*)strchr((char *)p,',');
			if(p == NULL)
			break;
			p+=1;
			pEnd = (int8_t*)strchr((char *)p,',');
			if(pEnd == NULL)
			break;
			memcpy(cellinfo.LAC1,p,pEnd-p);
			p = (int8_t*)strchr((char *)p,',');
			if(p == NULL)
			break;
			p+=1;
			pEnd = (int8_t*)strchr((char *)p,',');
			if(pEnd == NULL)
			break;
			memcpy(cellinfo.cellid1,p,pEnd-p);
			p = (int8_t*)strchr((char *)p,',');
			if(p == NULL)
			break;
			p+=1;
			p = (int8_t*)strchr((char *)p,',');
			if(p == NULL)
			break;
			p+=1;
			p = (int8_t*)strchr((char *)p,',');
			if(p == NULL)
			break;
			p+=1;
			pEnd = (int8_t*)strchr((char *)p,',');
			if(pEnd == NULL)
			break;
		  memcpy(cellinfo.RXL2,p,pEnd-p);
			p = (int8_t*)strchr((char *)p,',');
			if(p == NULL)
			break;
			p+=1;
			p = (int8_t*)strchr((char *)p,',');
			if(p == NULL)
			break;
			p+=1;
			p = (int8_t*)strchr((char *)p,',');
			if(p == NULL)
			break;
			p+=1;
			p = (int8_t*)strchr((char *)p,',');
			if(p == NULL)
			break;
			p+=1;
			pEnd = (int8_t*)strchr((char *)p,',');
			if(pEnd == NULL)
			break;
			memcpy(cellinfo.MCC2,p,pEnd-p);
			p = (int8_t*)strchr((char *)p,',');
			if(p == NULL)
			break;
			p+=1;
			pEnd = (int8_t*)strchr((char *)p,',');
			if(pEnd == NULL)
			break;
			memcpy(cellinfo.MNC2,p,pEnd-p);
			p = (int8_t*)strchr((char *)p,',');
			if(p == NULL)
			break;
			p+=1;
			pEnd = (int8_t*)strchr((char *)p,',');
			if(pEnd == NULL)
			break;
			memcpy(cellinfo.LAC2,p,pEnd-p);
			p = (int8_t*)strchr((char *)p,',');
			if(p == NULL)
			break;
			p+=1;
			pEnd = (int8_t*)strchr((char *)p,',');
			if(pEnd == NULL)
			break;
			memcpy(cellinfo.cellid2,p,pEnd-p);
			p = (int8_t*)strchr((char *)p,',');
			if(p == NULL)
			break;
			p+=1;
			p = (int8_t*)strchr((char *)p,',');
			if(p == NULL)
			break;
			p+=1;
			p = (int8_t*)strchr((char *)p,',');
			if(p == NULL)
			break;
			p+=1;
			pEnd = (int8_t*)strchr((char *)p,',');
			if(pEnd == NULL)
			break;
		  memcpy(cellinfo.RXL3,p,pEnd-p);
			p = (int8_t*)strchr((char *)p,',');
			if(p == NULL)
			break;
			p+=1;
			p = (int8_t*)strchr((char *)p,',');
			if(p == NULL)
			break;
			p+=1;
			p = (int8_t*)strchr((char *)p,',');
			if(p == NULL)
			break;
			p+=1;
			p = (int8_t*)strchr((char *)p,',');
			if(p == NULL)
			break;
			p+=1;
			pEnd = (int8_t*)strchr((char *)p,',');
			if(pEnd == NULL)
			break;
			memcpy(cellinfo.MCC3,p,pEnd-p);
			p = (int8_t*)strchr((char *)p,',');
			if(p == NULL)
			break;
			p+=1;
			pEnd = (int8_t*)strchr((char *)p,',');
			if(pEnd == NULL)
			break;
			memcpy(cellinfo.MNC3,p,pEnd-p);
			p = (int8_t*)strchr((char *)p,',');
			if(p == NULL)
			break;
			p+=1;
			pEnd = (int8_t*)strchr((char *)p,',');
			if(pEnd == NULL)
			break;
			memcpy(cellinfo.LAC3,p,pEnd-p);
			p = (int8_t*)strchr((char *)p,',');
			if(p == NULL)
			break;
			p+=1;
			pEnd = (int8_t*)strchr((char *)p,',');
			if(pEnd == NULL)
			break;
			memcpy(cellinfo.cellid3,p,pEnd-p);
			gsmSend("AT+QENG=0\r\n");
			GsmTask.Command = GSM_TASK_CMD_GSMCELL_SEND;
			GsmAddTask(&GsmTask);
			}
			break;
		}
		
	#endif	
		if(!strncmp((char*)gsmBuffer,"+CMT:",strlen("+CMT:")))
		{
			getSmsNumber(gsmBuffer);
			new_sms_incoming = true;
			continue;
		}
		
		if(new_sms_incoming){
			new_sms_incoming = false;
			if(handleSmsCommand(gsmBuffer,false,false) != 0)
			{
			if(getTCPConState()){
			GsmTask.Command = GSM_TASK_CMD_SYNC;
			GsmAddTask(&GsmTask);
			}
			}
			gsmRxFlush(gc);
			reply = false;
			break;
		}
		
		if(!strncmp((char*)gsmBuffer,"RING",strlen("RING")))
		{
			memset(gsmRxBuffer,0,BUFFER_LENGTH+32);
			clccIncomingCall();
			//answerIncomingCall();
			reply = false;
		}
		
		if(messageNum == read_message_number())
		{
			if(DebugEnable) DEBUG("BH MESSAGE SEND SUCCESS");
			ackResponse = false;
			break;
		}
		
		//	gsmLastReplyTime = xTaskGetTickCount();
		//	Process host line
		if (strncmp((char*)gsmBuffer, (char*)GG_FRRET, GG_FRRET_S) == 0)
		{
			reply = false;
		}
		else if (strncmp((char*)gsmBuffer, (char*)GG_FRSES, GG_FRSES_S) == 0)
		{
			#if 0
			extern uint8_t ota[100];
			GG_FwSmsIndex = -1;
			GG_FwFileName[0]='U';
			memcpy(ota,"gps.scjtracker.com:firmware:123456:/var/www/tracker_srv/file_srv/firmware:te102.bin",
						strlen("gps.scjtracker.com:firmware:123456:/var/www/tracker_srv/file_srv/firmware:te102.bin"));
			#endif
			#if 0
			cmd = ProcessGpsGateCommand(gsmBuffer, true);
			if (cmd != NULL && cmd->Response != NULL)
			{
				
				if ((cmd->Response)(gsmBuffer, sizeof(gsmBuffer), true, cmd) == E_OK)
				{
					gsmDisconnectTCP(gc);
				}
			}
			#endif
			login = false;
			loginWaitresponse = 0;
			reply = false;
		}
		else if (strncmp((char*)gsmBuffer, (char*)GG_FRCMD, GG_FRCMD_S) == 0)
		{
			cmd = ProcessGpsGateCommand(gsmBuffer, true);
			GsmTask.Command = GSM_TASK_CMD_ERR;
			GsmTask.GG_Command = cmd;
			GsmTask.SmsIndex = 0;

			if (cmd != NULL && cmd->Response != NULL)
			{
				if ((cmd->Response)(gsmBuffer, sizeof(gsmBuffer), true, cmd) == E_OK)
				{
					gsmDisconnectTCP(gc);
					//gsmSaveSMS(SMS_CENTER_1, gsmBuffer);
				}
				GsmTask.Command = GSM_TASK_CMD_RET;
			}
			if(GsmTask.Command != GSM_TASK_CMD_ERR)
			GsmAddTask(&GsmTask);
			disconnect = false;
		}
		else if (strncmp((char*)gsmBuffer, (char*)GG_FRERR_AUTH, GG_FRERR_AUTH_S) == 0)
		{	// Authentication error
			#if 0
			gc->AuthErrorCount++;
			if (gc->AuthErrorCount >= 2)
			{
				gc->Status |= GSM_STATUS_TCP_DISABLE;
				gsmDisconnectTCP(gc);
				break;
			}
			#endif
			reply = false;
		}else if (strncmp((char*)gsmBuffer, (char*)GG_FRSES, GG_FRSES_S) == 0)
		{
			reply = false;
		}
		else if (
				strncmp((char*)gsmBuffer, (char*)GSM_SHUTDOWN,(sizeof(GSM_SHUTDOWN)-1)) == 0
			//||	strncmp((char*)gsmBuffer, (char*)GSM_NO_CARRIER, (sizeof(GSM_NO_CARRIER)-1)) == 0
			//||	strncmp((char*)gsmBuffer, (char*)GSM_WIPPEERCLOSE, (sizeof(GSM_WIPPEERCLOSE)-1)) == 0
				)
		{
			gsmDisconnectTCP(gc);
			break;
		}else 
		{
			reply = false;
		}
	}
}

/*******************************************************************************
* Function Name	:	gsmSendSMS
* Description	:
*******************************************************************************/
uint8_t gsmSendSMS(const int8_t *da, const int8_t *msg)
{
	int8_t sendMessage[30];
	
	gsmCmd1("AT+CMGD=1,3\r\n", 10000);
	
	if(!strcmp((char *)get_conf_elem(RESERVE2_CONF),"1"))
		return 0;
	
	sprintf((char*)sendMessage, "AT+CMGS=\"%s\"\r\n", da);
	if (gsmCmd2(sendMessage, 2000, NULL, "> ")  == E_OK)
	{
		
		if (gsmCmd1((int8_t *)strcat((char *)msg,"\x1A"), 20000) != E_OK)
			gsmSend("\x1B");
	}
	
	osDelay(100);
	
	//if(GsmContext.Error == E_OK)
	gsmCmd1("AT+CMGD=1,3\r\n", 2000);
	
	return GsmContext.Error;
}

/*******************************************************************************
* Function Name	:	gsmSaveSMS
* Description	:
*******************************************************************************/
uint8_t gsmSaveSMS(const int8_t *da, const int8_t *msg)
{
	int8_t sendMessage[25];
	sprintf((char*)sendMessage, "AT+CMGW=%s\r\n", da);
	if (gsmCmd2(sendMessage, 2000, NULL, "> ")  == E_OK)
	{
		
		if (gsmCmd1((int8_t *)strcat((char *)msg,"\x1A"), 20000) != E_OK)
			gsmSend("\x1B");
	}
	
	return GsmContext.Error;
}

/*******************************************************************************
* Function Name	:	gsmGetNextIMEI
* Description	:
*******************************************************************************/
int8_t * gsmGetNextIMEI(int8_t * cmd)
{
	int8_t * token = TokenNextComma(cmd);
	if (token == NULL || strncmp((char*)token,(char*)GsmContext.IMEI, strlen((char*)GsmContext.IMEI)) != 0)
		return NULL;
	return token;
}

int8_t* gsmSenderDAnumber(void)
{
	return gsmSenderDA;
}

/*******************************************************************************
* Function Name	:	gsmGetNextIMEI
* Description	:
*******************************************************************************/
uint8_t gsmScanSMSInt(GsmContext_t * gc, bool not_delete)
{
	bool is_text;
	int smsIndex;
	int nlen;
	int8_t * token;
	uint32_t smsDate;
	int smsDelete;
	uint8_t ntoken;
	const GgCommand_t *cmds;
	uint8_t timeout =0;

sms_rescan:
	smsIndex = 0;
	smsDelete = 0;
	smsDate = 0;
	is_text = false;
	
	gsmRxFlush(gc);
	gc->RxMode = GSM_MODE_STREAM;
	gsmSend(GSM_CMGL);

	//SET_HOST_ADDR[0] = 0;
	GG_ClearAllCounts();
	GlobalStatus &= ~SMS_INIT_OK;

	while (gsmGetHostLine(gc, 2000) == E_OK)
	{
		//if(DebugEnable) DEBUG("gsmGetHostLine gsmBuffer:%s",gsmBuffer);
		nlen = strlen((char*)gsmBuffer);
		if (is_text)
		{
			is_text = false;
			if (smsDelete != 0)
				continue;

			if (nlen == 0)
			{
sms_delete:		if (!not_delete && smsIndex != 0)
					smsDelete = smsIndex;
				continue;
			}

			cmds = ProcessGpsGateCommand(gsmBuffer, not_delete);
			if (cmds == NULL)
			{	// Bad IMEI or unknown command
				if (not_delete)
				{
				}
				else
				{	// Scan pass - delete this sms
					goto sms_delete;
				}
			}
			else if (not_delete)
			{
				if (cmds->Response != NULL)
					(cmds->Response)(NULL, smsIndex, true, cmds);
			}
			else if (cmds->Counts != NULL)
			{	// Scan pass - check for duplicated sms
				if (smsDate != 0)
				{
					if (cmds->Counts->Count == 0)
					{
						cmds->Counts->Count++;
						cmds->Counts->DateTime = smsDate;
						cmds->Counts->Index = smsIndex;
					}
					else if (smsDate > cmds->Counts->DateTime)
					{
						nlen = cmds->Counts->Index;
						cmds->Counts->Count++;
						cmds->Counts->DateTime = smsDate;
						cmds->Counts->Index = smsIndex;
						smsIndex = nlen;
						goto sms_delete;
					}
					else
						goto sms_delete;
				}
			}
			continue;
		}
		
		if (nlen == 0)
			continue;
		if(DebugEnable) DEBUG("gsmBuffer %s",gsmBuffer);
		if(DebugEnable) DEBUG("smsDelete %d",smsDelete);
		if (strcmp((char*)gsmBuffer, "OK") == 0)
		{
			timeout = 0;
			if (smsDelete != 0)
			{
				if (gsmDeleteSMS(smsDelete) != E_OK)
				{	// Delete failed, so delete all
					gsmCmd1("AT+CMGD=1,4\r\n", 2000);
					return E_ERROR;
				}
				goto sms_rescan;
			}
			return E_OK;
		}
		if (strncmp((char*)gsmBuffer, "+CMGL:", 6) == 0)
		{
			is_text = true;
			if (smsDelete != 0){
				timeout++;
				if(timeout > 50){
						gsmCmd1("AT+CMGD=1,4\r\n", 2000);
						return E_ERROR;
				}
				continue;
			}

			//!!! memset(&gsmSMS, 0, sizeof(SMS_t));
			smsDate = 0;
			ntoken = 0;
			token = gsmBuffer;
			do
			{
				nlen = TokenSizeComma(token);
				if (ntoken == 0)
				{
					/*
					int nsize = TokenSizeQuote(token);
					if (nlen > nsize)
					{	// no sms index if AT+CMGR=n
						ntoken++;
						continue;
					}
					*/
					if (nlen < 8)
						break;
					smsIndex = nmea_atoi((char*)&gsmBuffer[6], 5, 10);
					//!!! gsmSMS.Index = smsIndex;
				}
				else if (ntoken == 2)
				{
					if (nlen == 0 || nlen > sizeof(gsmSenderDA)){
						memcpy(gsmSenderDA,"null",4);
						break;	// No or bad Sender DA
					}
					memset(gsmSenderDA,0,20);
					strncpy((char*)gsmSenderDA, (char*)token, nlen);
					gsmSenderDA[nlen] = 0;
					if(DebugEnable) DEBUG("gsmSenderDA %s",gsmSenderDA);
					//!!! strncpy(&gsmSMS.DA[0], token, nlen);
				}
				else if (ntoken == 4)
				{	// Date
					if (nlen > 0)
					{
						int yy,mm,dd;
						if (nmea_scanf((char*)token, nlen, "\"%2d/%2d/%2d", &yy, &mm, &dd) == 3)
						{
							smsDate = ((yy - 10) * 365 + mm * 12 + dd) * 24 * 60 * 60;
							//!!! gsmSMS.DateTime = smsDate;
						}
						else
							break;
					}
				}
				else if (ntoken == 5)
				{	// Time
					if (nlen > 0)
					{
						int hh,mm,ss;
						if (nmea_scanf((char*)token, nlen, "%2d:%2d:%2d", &hh, &mm, &ss) == 3)
						{
							smsDate += (hh * 24 + mm) * 60 + ss;
							//!!! gsmSMS.DateTime = smsDate;
						}
						else
							break;
					}
				}
				ntoken++;
			} while (ntoken < 6 && (token = TokenNextComma(token)) != NULL);

			if (ntoken != 6 && ntoken != 4)
						goto sms_delete;
					if(ntoken == 4)
					{
						if (smsDelete != 0)
						{
						if (gsmDeleteSMS(smsDelete) != E_OK)
						{	// Delete failed, so delete all
							gsmCmd1("AT+CMGD=1,4\r\n", 2000);
						}	
						}
					}
		}else
		{
				timeout++;
				if(timeout > 20){
						gsmCmd1("AT+CMGD=1,4\r\n", 2000);
						return E_ERROR;
				}
		}
	}

	return E_TIMEOUT;
}
/*******************************************************************************
* Function Name	:	gsmScanSMS
* Description	:
*******************************************************************************/
void gsmScanSMS(GsmContext_t * gc)
{
	if	(
		gsmCmd0(GSM_CMGF) == E_OK
	&&	gsmCmd0(GSM_CSDH) == E_OK
	&&	gsmCmd0(GSM_CNMI) == E_OK
	&&	gsmScanSMSInt(gc, false) == E_OK
	&&	gsmScanSMSInt(gc, true) == E_OK
	&&	(GlobalStatus & (SMS_INIT_OK | SMS_INIT_SENT)) == 0
		)
	{
		gsmCmd0("AT+CSCA?\r\n");
		
		gsmCmd0("AT+CSCS=\"GSM\"\r\n");

		GlobalStatus |= SMS_INIT_SENT;
		if(DebugEnable) DEBUG("INIT SMS");

		NmeaAddChecksum(
			strcpyEx(
			strcpyEx(
			strcpyEx( gsmBuffer, "$FRCMD," ),
				GsmIMEI()),
				",_InitSMS,,0"),
			gsmBuffer);
		
/*
		if (gsmSendSMS(SMS_CENTER_1, gsmBuffer) == E_OK)
		{
			gsmSaveSMS(SMS_CENTER_1, gsmBuffer);
			gsmSendSMS(SMS_CENTER_2, gsmBuffer);
		}
		*/
	}
	if(getresetIndex()!= 0){
	if(getTCPConState()){
	GsmTask.Command = GSM_TASK_CMD_SYNC;
	GsmAddTask(&GsmTask);
	}
	}
	
	if(getresetIndex() == 2)
			SystemRestart();
}

/*******************************************************************************
* Function Name	:	gsmWaitRegister
* Description	:	Wait GSM registration
*******************************************************************************/
uint8_t gsmWaitRegister(GsmContext_t * gc)
{
	uint8_t retry = 20;
	int8_t *item;
	int8_t *itemEnd;
	
	// Wait for SIM ready
	while (retry-- != 0)
	{
		gsmCheckPIN(gc);
		if(gc->Error == E_OK)
			nosim_wakeup = 0;
		
		if (gc->Error == E_OK || gc->Error == E_TIMEOUT)
			break;
		if (gc->Error == E_NO_SIM)
			osDelay(1000);
		else
			osDelay(2000);
	}
	
	if (gc->Error == E_OK)
	{	// Wait for registration
		
		if (E_OK == gsmCmd0("AT+CIMI\r\n"))
				{
					item = TokenBefore(gsmToken(NULL), "OK");
					if (item != NULL)
					{
						strcpy(imsi, (char*)item);
						if(DebugEnable) DEBUG("imsi:%s",imsi);
						if( isJTSimcard() && strcmp(get_conf_elem(APNNAME_CONF),"bhm2m"))
						{
						if(strcmp(get_conf_elem(APNNAME_CONF),"data641003")){
							//change to BH config file
							update_conf_elem_map(APNNAME_CONF,"bhm2m");
							update_conf_elem_map(MAINSERVERIPADDR_CONF,"114.129.33.207");
							update_conf_elem_map(MAINSERVERPORTNUMBER_CONF,"59100");
							update_conf_elem_map(GSMSWITCH_CONF,"3");
							update_conf_elem_map(REPINTERVAL_CONF,"30");
							update_conf_elem_map(ACCOFFREPINTERVAL_CONF,"3600");
							update_conf_elem_map(DISTANCE_CONF,"500");
							update_conf_elem_map(GSENSOR_SENSITIVITY_CONF,"2");
							update_conf_elem_map(WARNOVERSPEED_CONF,"0");
							update_conf_elem_map(WARNEFAN_CONF,"1");
							update_conf_elem_map(SPEED_CONF,"80");
							update_conf_elem_map(OVERSPEED_TIME_CONF,"300");
							update_conf_elem_map(WORKINGMODE_CONF,"2");
							update_conf_elem_map(ANGLE_CONF,"25");
							update_conf_elem_map(SIMULATE_ACC_CONF,"0|12.9|13.0|10|180");
							update_conf_elem_map(RESERVE3_CONF,"1");
							update_conf_elem_rom();
							}
							else if(!strcmp(get_conf_elem(APNNAME_CONF),"data641003")
								&& !strcmp(get_conf_elem(MAINSERVERIPADDR_CONF),"114.129.33.207")
								&& !strcmp(get_conf_elem(MAINSERVERPORTNUMBER_CONF),"59100"))
							{
							update_conf_elem_map(MAINSERVERIPADDR_CONF,"47.89.17.248");
							update_conf_elem_map(MAINSERVERPORTNUMBER_CONF,"4700");
							update_conf_elem_map(GSMSWITCH_CONF,"1");
							update_conf_elem_map(REPINTERVAL_CONF,"30");
							update_conf_elem_map(ACCOFFREPINTERVAL_CONF,"3600");
							update_conf_elem_map(DISTANCE_CONF,"500");
							update_conf_elem_map(GSENSOR_SENSITIVITY_CONF,"2");
							update_conf_elem_map(WARNOVERSPEED_CONF,"1");
							update_conf_elem_map(WARNEFAN_CONF,"0");
							update_conf_elem_map(SPEED_CONF,"120");
							update_conf_elem_map(OVERSPEED_TIME_CONF,"300");
							update_conf_elem_map(WORKINGMODE_CONF,"1");
							update_conf_elem_map(ANGLE_CONF,"25");
							update_conf_elem_map(SIMULATE_ACC_CONF,"0|12.9|13.0|10|180");
							update_conf_elem_map(RESERVE3_CONF,"0");
							update_conf_elem_rom();
							}
						}
					}
				}
				
		if (E_OK == gsmCmd1("AT+QCCID\r\n", 2000))
		{
					item = TokenBefore(gsmToken(NULL), "OK");
					if (item != NULL)
					{
						if(strchr((char*)item,':')!= NULL)
						{
								item = (int8_t*)strchr((char *)item,':');
							  item +=2;
						}
						strcpy((char *)iccid,(char*)item);
						if(DebugEnable) DEBUG("iccid:%s",iccid);
					}
		}
				
		retry = 20;
		while (retry-- != 0 && gsmIsRegister(gc) != E_OK)
			osDelay(2000);
		if (gc->Error == E_OK)
		{
		if (E_OK == gsmCmd1(GSM_CSQ, 2000))
				{
					item = TokenBefore(gsmToken(NULL), "OK");
					if (item != NULL)
					{
						if(strchr((char*)item,':')!= NULL)
						{
								item = (int8_t*)strchr((char *)item,':');
							  item +=1;
								itemEnd = (int8_t*)strchr((char *)item,',');
								memset(signal,0,4);
								strncpy((char *)signal,(char*)item,itemEnd-item);
								if(DebugEnable) DEBUG("signal:%s",signal);
						}
					}
				}
			if (E_OK == gsmCmd1(GSM_CBC, 2000))
				{
					item = TokenBefore(gsmToken(NULL), "OK");
					if (item != NULL)
					{
						if(strchr((char*)item,',')!= NULL)
						{
								item = (int8_t*)strchr((char *)item,',');
								item +=1;
								if(strchr((char *)item,',')!= NULL){
									item = (int8_t*)strchr((char *)item,',');
									item +=1;
									if(strchr((char *)item,',') == NULL){
									memset(battVolt,0,20);
									strcpy((char *)battVolt,item);
									if(DebugEnable) DEBUG("battVolt:%s",battVolt);
									}
								}
						}
					}
				}
		
		}
	}
	return gc->Error;
}

/*******************************************************************************
* Function Name	:	gsmCheckPIN
* Description	:	Check SIM ready
*******************************************************************************/
uint8_t gsmCheckPIN(GsmContext_t * gc)
{
	if (gsmCmd0(GSM_GET_CPIN) == E_OK)
	{
		if (FindToken(gsmToken(NULL), GSM_CPIN_RDY) == NULL)
			gc->Error = E_SIM_NOT_READY;
		else if (FindToken(gsmToken(NULL), GSM_NO_SIM) != NULL)
			gc->Error = E_NO_SIM;
	}
	return gc->Error;
}

/*******************************************************************************
* Function Name	:	gsmIsRegister
* Description	:	Check registration (CREG and CGATT)
*******************************************************************************/
uint8_t gsmIsRegister(GsmContext_t * gc)
{
	gc->Status &= ~GSM_STATUS_REGISTER;
	if (gsmCmd0(GSM_CREG) == E_OK)
	{
		if (FindToken(gsmToken(NULL), GSM_CREG01) != NULL
		||	FindToken(gsmToken(NULL), GSM_CREG05) != NULL
			)
		{
			if (gsmCmd0(GSM_CGATT) == E_OK
			&&	FindToken(gsmToken(NULL), GSM_CGATT1) == NULL
				)
				gc->Error = E_NOT_REGISTER;
			else
				gc->Status |= GSM_STATUS_REGISTER;
		}
		else if (FindToken(gsmToken(NULL), GSM_CREG03) != NULL)
		{
			gc->Error = E_REG_DENIED;
		}
		else
			gc->Error = E_NOT_REGISTER;
	}

	return gc->Error;
}

/*******************************************************************************
* Function Name	:	gsmSend
* Description	:	Put string to GSM TX queue
* Input			:	string
* Return		:	result code
*******************************************************************************/
void gsmSend(const int8_t *text)
{
	if(DebugEnable) DEBUG("%s",text);
	#if 0
	GsmContext_t * gc = &GsmContext;
	if(HAL_UART_Transmit_IT(&gsmUartHandle, (uint8_t*)text, strlen((char*)text)) != HAL_OK)
	{
		if(DebugEnable) DEBUG("gsmSend error");
		//gsmDisconnectTCP(gc);
	}
	#endif
	USART_SEND_DATAS((char *)text,USART4);
}

/*******************************************************************************
* Function Name	:	gsmSendHex
* Description	:	Put string to GSM TX queue
* Input			:	string
* Return		:	result code
*******************************************************************************/
void gsmSendHex(const int8_t *text,int32_t len)
{
	if(DebugEnable) DEBUG("hex %02x",text[0]);
	//HAL_UART_Transmit_IT(&gsmUartHandle, (uint8_t*)text, len);
	USART_SEND_HEXDATAS((char *)text,USART4,len);
}

/*******************************************************************************
* Function Name	:	gsmSends
* Description	:
*******************************************************************************/
void gsmSends(const int8_t * cmd, ...)
{
	va_list arg_ptr;
	const int8_t *p = cmd;

	va_start(arg_ptr, cmd);
	while (p != NULL)
	{
		gsmSend(p);
		p = (const int8_t *)va_arg(arg_ptr, const int8_t *);
	}
	va_end(arg_ptr);
}

/*******************************************************************************
* Function Name	:	gsmToken
* Description	:	Get first/next GSM token
* Input			:	NULL for first, previous token for next
* Return		:	next token or NULL
*******************************************************************************/
int8_t * gsmToken(int8_t * src)
{
	if (src == NULL)
		src = gsmRxBuffer;
	else
		while(*src++ != 0);
	return (*src == 0 ? NULL : src);
}

/*******************************************************************************
* Function Name	:	gsmShowResponse
* Description   :	Display response from GSM
*******************************************************************************/
void gsmShowResponse(void)
{
	int8_t * token = gsmToken(NULL);
	if (token != NULL)
	{
		if(DebugEnable || atDebug) 
			DEBUG("%s",token);
		
	}
	/*
	while(token != NULL)
	{
		if(DebugEnable) DEBUG(token);
		token = gsmToken(token);
	}
	*/
}

/*******************************************************************************
* Function Name	:	gsmShowError
* Description   :	Display last GSM Error
*******************************************************************************/
void gsmShowError(void)
{
	if (GsmContext.Error == E_OK){
		if(DebugEnable || atDebug) DEBUG("E_OK");
		//GlobalStatus |= GSM_WAKEUP;
	}
	else if (GsmContext.Error == E_ERROR){
		if(DebugEnable || atDebug) DEBUG("E_ERROR");
		if(gsmToken(NULL) != NULL){
			if(!strcmp(gsmToken(NULL),"+CME ERROR: 10")){
			//if((GlobalStatus & GSM_WAKEUP)){
				nosim_wakeup++;
				if(nosim_wakeup == 20)
				{
					nosim_wakeup = 0;
					GlobalStatus &= ~GSM_WAKEUP;
					restGSM = true;
					//nosimcard_sleep();
				}
			//}
			//else
				//nosimcard_sleep();
			}
			else if(strcmp(gsmToken(NULL),"+CMS ERROR: 69") && strcmp(gsmToken(NULL),"+CME ERROR: 14")){
				if(strcmp((char *)get_conf_elem(RESERVE3_CONF),"1"))
			   restGSM = true;
			}
		}else
			restGSM = true;
	}
	else if (GsmContext.Error == E_TIMEOUT){
		if(DebugEnable || atDebug) DEBUG("E_TIMEOUT");
	}
	else if (GsmContext.Error == E_SEND_ERROR){
		if(DebugEnable || atDebug) DEBUG("E_SEND_ERROR");
	}
	else{
		if(DebugEnable || atDebug) DEBUG("Unknown E_?");
	}
	if(atDebug){
		atDebug = false;
	}
}

/*******************************************************************************
* Function Name	:	gsmRxFlush
* Description   :
*******************************************************************************/
void gsmRxFlush(GsmContext_t * gc)
{
	gc->RxMode	= GSM_MODE_IGNORE;
	rb_Init(&gc->RB_Rx, gsmRxBuffer, sizeof(gsmRxBuffer)-4);
	gc->Error	= E_TIMEOUT;
	
	// Clear semaphore
	while(xSemaphoreTake(gc->ReadySemaphore, 1) == pdTRUE)
	{ }
	gc->RxMode	= GSM_MODE_AT;
}

/*******************************************************************************
* Function Name	:	gsmCmd0
* Description   :	Send string to GSM and wait response within 500ms
* Input         :	string to send
* Return		:	result code
*******************************************************************************/
uint8_t gsmCmd0(const int8_t* cmd)
{
	return gsmCmd2(cmd, 500, NULL, NULL);
}

/*******************************************************************************
* Function Name	:	gsmCmd1
* Description   :	Send string to GSM and wait response with timeout
* Input         :	string to send
*				:	timeout
* Return		:	result code
*******************************************************************************/
uint8_t gsmCmd1(const int8_t* text, uint16_t timeout)
{
	return gsmCmd2(text, timeout, NULL, NULL);
}
uint8_t gsmCmd1E(const int8_t* text, uint16_t timeout)
{
	return gsmCmd2(text, timeout, NULL, NULL);
}

/*******************************************************************************
* Function Name	:	gsmCmd2
* Description   :	Send string to GSM and wait response with timeout or alive,
*				:	always check ERROR, +CME ERROR: and +CMS ERROR:
* Input         :	string to send
*				:	timeout
*				:	wait for token for complete
*				:	wait for alive token for complete
* Return		:	result code
*******************************************************************************/
uint8_t gsmCmd2(const int8_t* text, uint16_t timeout, const int8_t* waitFor, const int8_t* waitAlive)
{
	GsmContext_t * gc = &GsmContext;

	if (text == NULL)
		return (gc->Error = E_OK);

	gc->RxMode	= GSM_MODE_IGNORE ;
	gc->WaitAlive = (int8_t *)waitAlive ;
	gc->WaitAliveLen = (gc->WaitAlive == NULL) ? 0 : strlen((char*)gc->WaitAlive);
	gc->RxWaitFor = (waitFor == NULL) ? GSM_OK : waitFor;
	gsmRxFlush(gc);
	
	gsmSend(text);

	// Wait for response and set ignore on timeout
	if (xSemaphoreTake(gc->ReadySemaphore, timeout) != pdTRUE)
		gc->RxMode = GSM_MODE_IGNORE;
#if ( USE_DEBUG )
	else
		gsmShowResponse();
		gsmShowError();
#endif
	return (gc->Error);
}

void gsmData(uint8_t data)
{
	register GsmContext_t *gc = &GsmContext;
		
		gc->Data = data;
	
		receiveData++;
	
		TaskRun = 4;
		if(gc->RxMode == GSM_MODE_UPDATE){
			rb_Put(&gc->RB_Rx, gc->Data);
			return;
		}
	
		if (gc->Data != '\r')
		{
			if (gc->Data == '\n') // Receive End Of Line
			gc->Data = 0;
				
			if (gc->RxMode == GSM_MODE_AT)
			{
				// Save data to buffer (use as linear)
				if (gc->Data == 0)
				{
					if (gc->RB_Rx.Start != gc->RB_Rx.Tail)// Check for empty token and ignore it
					{
						*gc->RB_Rx.Tail++ = 0;
						if (strcmp((char*)gc->RB_Rx.Start, (char*)gc->RxWaitFor) == 0)
						{
							*gc->RB_Rx.Tail = 0;		// Write Double 0 as End of tokens
							gc->Error = E_OK;
						}
						else if (strcmp((char*)gc->RB_Rx.Start, (char*)GSM_ERROR) == 0
							||	strncmp((char*)gc->RB_Rx.Start, (char*)GSM_CME_ERROR, sizeof(GSM_CME_ERROR) - 1) == 0
							||	strncmp((char*)gc->RB_Rx.Start, (char*)GSM_CMS_ERROR, sizeof(GSM_CMS_ERROR) - 1) == 0
								)
						{
							*gc->RB_Rx.Tail = 0;		// Write Double 0 as End of tokens
							gc->Error = E_ERROR;
						}
						else
							gc->RB_Rx.Start = gc->RB_Rx.Tail;
					}
				}
				else if (gc->RB_Rx.Tail >= gc->RB_Rx.End)
				{
					gc->Error = E_BAD_RESPONSE;
				}
				else
				{
					*gc->RB_Rx.Tail++ = gc->Data;
					if (gc->WaitAlive != NULL 
					&&	(gc->RB_Rx.Tail - gc->RB_Rx.Start) == gc->WaitAliveLen
					   )
					{
						// Check for non-CRLF tokens (such as CONNECT)
						if (strncmp((char*)gc->RB_Rx.Start, (char*)gc->WaitAlive, gc->WaitAliveLen) == 0)
						{
							*gc->RB_Rx.Tail++ = 0;		// Write Double 0 as End of tokens
							*gc->RB_Rx.Tail   = 0;
							gc->Error = E_OK;
						}
					}
				}

				if (gc->Error != E_TIMEOUT )
				{
					portBASE_TYPE RxNeedTaskSwitch;
					//gc->RxMode = GSM_MODE_IGNORE;
					RxNeedTaskSwitch = pdFALSE;
					xSemaphoreGiveFromISR( gc->ReadySemaphore, &RxNeedTaskSwitch );
					portEND_SWITCHING_ISR( RxNeedTaskSwitch );
				}
			}
			else if (gc->RxMode == GSM_MODE_STREAM || (gc->RxMode == GSM_MODE_IGNORE && getTCPConState()))
			{
				if((_s_index > 0 && gc->Data =='$') || (_s_index > 0 && gc->Data =='+'))
				{
				_s_index = 0;
				rb_Put(&gc->RB_Rx, 0);
				}else
				_s_index = 0;
				
				rb_Put(&gc->RB_Rx, gc->Data);
				if(gc->Data == '*')
					_r_index++;
				if(_r_index > 0)
					_r_index++;
				if(_r_index == 4 || _r_index > 4)
				{
					_r_index = 0;
					rb_Put(&gc->RB_Rx, 0);
				}
						
				if(gc->Data == ' ')
				{
						_s_index++;
					  return;
				}		
		
				if(gc->Data == '>'){
					rb_Put(&gc->RB_Rx, 0);
					return;
				}
#if 0				
				if(gc->Data == 'S' || gc->Data == 'E'){
						_s_index++;
						return;
				}
				
				if((_s_index == 1 && gc->Data == 'E') ||( _s_index == 1 && gc->Data == 'R')){
						_s_index++;
						return;
				}
				
				if((_s_index == 2 && gc->Data == 'N') || (_s_index == 2 && gc->Data == 'R')){
						_s_index++;
						return;
				}
				
				if((_s_index == 3 && gc->Data == 'D') || (_s_index == 3 && gc->Data == 'O')){
						_s_index++;
						return;
				}
				
				if(_s_index == 4 && gc->Data == ' '){
						_s_index++;
						return;
				}
				
				if(_s_index == 4 && gc->Data == 'R'){
						_s_index=0;
						rb_Put(&gc->RB_Rx, 0);
						return;
				}
				
				if(_s_index == 5 && gc->Data == 'O' || _s_index == 5 && gc->Data == 'F'){
						_s_index++;
						return;
				}
				
				if(_s_index == 6 && gc->Data == 'K'){
						_s_index=0;
						rb_Put(&gc->RB_Rx, 0);
						return;
				}
				
				if(_s_index == 6 && gc->Data == 'A')
				{
						_s_index++;
						return;
				}
				
				if(_s_index == 7 && gc->Data == 'I'){
						_s_index++;
						return;
				}
				
				if(_s_index == 8 && gc->Data == 'L'){
						_s_index=0;
						rb_Put(&gc->RB_Rx, 0);
						return;
				}	
	#endif			
			}else if(gc->RxMode == GSM_MODE_URC){
				if(gc->Data != 0)
				rb_Put(&gc->RB_Rx, gc->Data);
			}
		}
}
/*******************************************************************************
* Function Name	:	str2hex
* Description	:	Convert hex int8_tacter to byte
* Return		:	true	int8_t is hex int8_t
*				:	false	bad int8_tacter
*******************************************************************************/
bool str2hex(int8_t c, uint8_t *data)
{
	if (c >= '0' && c <= '9')
		*data = (c - '0');
	else
	{
		if (c >= 'a')	// Check and convert to upper
			c -= ('a' - 'A');
		if (c >= 'A' && c <= 'F')
			*data = c - ('A' - 0xA);
		else
			return false;
	}
	return true;
}

/*******************************************************************************
* Function Name	:	handleReadData
* Description	:
*******************************************************************************/
uint8_t handleReadData(GsmContext_t *gc)
{
	int8_t * token, *dst;
	uint32_t nlen;
	uint8_t result = E_ERROR;
	uint32_t wait = 4000;
	uint32_t ramsource;
	int8_t size = 0;

	while(wait != 0){
		if ( (token = FindTokenStartWith(gsmRxBuffer+2, "CONNECT ")) == NULL)
		{
			DEBUG("token is null");
			osDelay(100);
			wait -= 100;
			continue;
		}else
			break;
		}
	  if(token == NULL)
			 return result;
		//DEBUG("token is %s",token);
		dst = token = token + sizeof("CONNECT ")-1;
		while(*dst!='\r' &&  *(dst+1) != '\n'){
		DEBUG("dst:%x",*dst);
		dst++;
		size++;
		}
		*dst = 0;
		DEBUG("size:%d",size);
		if(size > 5)
			return result;
	  DEBUG("token:%s",token);
		nlen = atoi((char *)token);
		
		if(nlen > 1040)		
			return result;
					
		DEBUG("nlen:%d",nlen);
		DEBUG("len:%d",strlen((char *)token));
		dst = token = token+ strlen((char *)token)+2;
							
		DEBUG("start:%d",token-gsmRxBuffer);
		DEBUG("token:%x",*token);
		DEBUG("fw_address:%x",gAddress);
		
		wait = 4000;
		while(wait != 0){
		DEBUG("receiveData:%d",receiveData);
		if (receiveData >= nlen){
			 //osDelay(300);
		   break;
		}
			osDelay(100);
			wait -= 100;
		}
		wait = 4000;
				
		if(token-gsmRxBuffer != 16){
			for(int i = 0;i< nlen;i++){
					gsmRxBuffer[i+8] = gsmRxBuffer[dst-gsmRxBuffer];
					dst++;
			}
		ramsource = (uint32_t) & gsmRxBuffer[8];
		}
		else
		ramsource = (uint32_t) & gsmRxBuffer[token-gsmRxBuffer];
		DEBUG("flash start");
		taskENTER_CRITICAL();
		result = FLASH_If_Write(gAddress, (uint32_t*)ramsource, nlen/4);
		taskEXIT_CRITICAL();

		if (result == FLASHIF_OK)                   
		{
		gAddress += nlen;
		DEBUG("flash success");
		}
		DEBUG("result %d",result);
							
		if(nlen > 0 && nlen < 1024 && wait !=0){
			gc->RxMode = GSM_MODE_IGNORE;
			gAddress = BOOT_ADDRESS;
			return E_FW_OK;
		}
		return result;
}

uint8_t handleReadAGPSData(GsmContext_t *gc)
{
	
	int8_t * token, *dst;
	uint32_t nlen;
	uint8_t result = E_ERROR;
	uint32_t wait = 4000;
	uint32_t ramsource;
	int8_t size = 0;
	
	while(wait != 0){
		if ( (token = FindTokenStartWith(gsmRxBuffer+2, "CONNECT ")) == NULL)
		{
			osDelay(100);
			wait -= 100;
			continue;
		}else
			break;
		}
	  if(token == NULL)
			 return result;
		//DEBUG("token is %s",token);
		dst = token = token + sizeof("CONNECT ")-1;
		while(*dst!='\r' &&  *(dst+1) != '\n'){
		dst++;
		size++;
		}
		*dst = 0;
		
		if(size > 5)
			return result;
	 
		nlen = atoi((char *)token);
		
		if(nlen > 1040)		
			return result;
					
	
		dst = token = token+ strlen((char *)token)+2;
							
	
		wait = 4000;
		while(wait != 0){
		if (receiveData >= nlen){
			 //osDelay(300);
		   break;
		}
			osDelay(100);
			wait -= 100;
		}
		wait = 4000;
		if(DebugEnable) DEBUG("%d %02x,%02x",ano_number,gsmRxBuffer[token-gsmRxBuffer],gsmRxBuffer[token-gsmRxBuffer+4]);
		bit_map_write_agps_data_index(&gsmRxBuffer[token-gsmRxBuffer],84,ano_number);
		//AID_ALP(&gsmRxBuffer[token-gsmRxBuffer],nlen);
		result = FLASHIF_OK;
		osDelay(350);
		bit_map_write_agps_data_index(&gsmRxBuffer[token-gsmRxBuffer],84,ano_number);


		if (result == FLASHIF_OK)                   
		{
		gAddress += nlen;
		}
							
		if(nlen > 0 && nlen < 84 && wait !=0){
			gc->RxMode = GSM_MODE_IGNORE;
			gAddress = BOOT_ADDRESS;
			return E_FW_OK;
		}
		return result;
}

/*******************************************************************************
* Function Name	:	handle_fieldata
* Description	:
*******************************************************************************/
void handle_locationdata(void)
{
	//int8_t * filedata;
	//int8_t filedatasize;
	//uint32_t buffsize = 0;
	register GsmContext_t *gc = &GsmContext;
	uint32_t wait = 2000;
	int8_t* p;
	
	if (bit_map_unread() == true) {
	if(DebugEnable) DEBUG("handle_locationdata");
	memset(localreadBuffer,0,BUFFER_LENGTH/4);
	bit_map_read_data(localreadBuffer, BITMAP_FLASH_DATA_LEN);
	if(DebugEnable) DEBUG("%s %d %x %x",localreadBuffer,strlen(localreadBuffer),localreadBuffer[strlen(localreadBuffer)-2],
		localreadBuffer[strlen(localreadBuffer)-1]);
	if(strncmp(get_conf_elem(RESERVE3_CONF),"1",1)){
	if(strlen(localreadBuffer) > 0 && !strncmp(localreadBuffer,"$FR",3)){
	if(localreadBuffer[strlen(localreadBuffer)-2]!= 0x0d && localreadBuffer[strlen(localreadBuffer)-1]!= 0x0a )
	{
	if(DebugEnable) DEBUG("add 0d0a");
	uint32_t length = strlen(localreadBuffer);
	localreadBuffer[length] = 0x0d;
	localreadBuffer[length+1] = 0x0a;
	}
	GsmTask.Command = GSM_TASK_CMD_GPS_FILEDATA;
	GsmAddTask(&GsmTask);
	}else
	{
	p = strstr(localreadBuffer,"$FRCMD");
	if(p != NULL)
	{
	if(strlen(localreadBuffer) < BITMAP_FLASH_DATA_LEN)
	memcpy(localreadBuffer,p,BITMAP_FLASH_DATA_LEN-(p-localreadBuffer));
	else
	memcpy(localreadBuffer,p,strlen(localreadBuffer)-(p-localreadBuffer));
	if(localreadBuffer[strlen(localreadBuffer)-2]!= 0x0d && localreadBuffer[strlen(localreadBuffer)-1]!= 0x0a )
	{
	uint32_t length = strlen(localreadBuffer);
	if(DebugEnable) DEBUG("add 0d0a");
	localreadBuffer[length] = 0x0d;
	localreadBuffer[length+1] = 0x0a;
	}
	for(uint32_t i = 0;i < strlen(localreadBuffer);i++)
	{
	if(localreadBuffer[i] > 0x7f || localreadBuffer[i] <0){
		localreadBuffer[i] = 0x00;
	}
	}
	GsmTask.Command = GSM_TASK_CMD_GPS_FILEDATA;
	GsmAddTask(&GsmTask);
	}
	}
	}else
	{
	#if 0	
	if(localreadBuffer[0] != 0x20)
	{
	for(uint32_t i = 0;i < 2*BITMAP_FLASH_DATA_LEN;i++)
	{
	 if(localreadBuffer[i] == 0x20)
	 {
	 memcpy(localreadBuffer,&localreadBuffer[i],0x20);
		break;
	 }
	}
	}
	#endif
	GsmTask.Command = GSM_TASK_CMD_GPS_FILEDATA;
	GsmAddTask(&GsmTask);
	}
	}
	
	/*
	if(strlen(gsmBuffer) > 0){
	if(DebugEnable) DEBUG("strlen(gsmBuffer):%d gsmBuffer %s",strlen(gsmBuffer),gsmBuffer);
	(*gc->Commands->TcpSend)();
	}
	*/
	#if 0
		if(!openReadFile(LOCATION_DATA))
			return;
	  while(1)
		{
			if(DebugEnable) DEBUG("handle_filedata");
			
			filedatasize = readfile(LOCATION_DATA);			
			if(filedatasize <= 0){
				closeFile(LOCATION_DATA);
				return;
			}
			int i;
			bool parse_frame_success= false;
			filedata = getReadData();
			
			for(i =0;i<filedatasize;i++){
			gsmBuffer[i+buffsize] = filedata[i];
			if(filedata[i] == '\n')
			{
			parse_frame_success = true;
			seeksize = seeksize+i+1;
				if(DebugEnable) DEBUG("seeksize:%d strlen(gsmBuffer):%d gsmBuffer %s",seeksize,strlen(gsmBuffer),gsmBuffer);
			seekfilesize(seeksize - strlen(gsmBuffer));
			//filedataEvent();
			gsmBuffer[i+buffsize+1] = 0;
			(*gc->Commands->TcpSend)();
			break;
			}
			}
			buffsize = buffsize+filedatasize;
			if(!parse_frame_success){
			seeksize = seeksize+filedatasize;
			if(DebugEnable) DEBUG("seeksize:%d",seeksize);
			if(!seekfilesize(seeksize))
				break;
			}else
			{
				break;
			}
			
		}
		closeFile(LOCATION_DATA);
		if(seeksize == getfilesize(LOCATION_DATA)){
		deletefile(LOCATION_DATA);
		}
		#endif
}

void write_locationdata(uint8_t Command,uint8_t alarm)
{
	memset(flashBuffer,0,BUFFER_LENGTH/4);
	int8_t *dst = flashBuffer;
	int send_size = 0;
	

	if(Command == GSM_TASK_CMD_GPS || Command == GSM_TASK_CMD_SIGNAL){
		Command=GSM_TASK_CMD_SIGNAL;
		if(strncmp(get_conf_elem(RESERVE3_CONF),"1",1)){
		if(!strncmp(get_conf_elem(WORKINGMODE_CONF),"0",1))
		dst = strcpyEx( strcpyEx( strcpyEx( gsmBuffer, "$U,"), GsmIMEI()), ",1, ,");
		else
		dst = strcpyEx( strcpyEx( strcpyEx( flashBuffer, "$FRCMD,"), GsmIMEI()), ",_SendMessage,,");
		send_size = frame(dst,Command,true);
		}else
		{
			send_size = Ruptela_frame(dst,Command,true);
		}
		
	}else if(Command == GSM_TASK_CMD_ACCOFF)
		{
		if(strncmp(get_conf_elem(RESERVE3_CONF),"1",1)){
		if(!strncmp(get_conf_elem(WORKINGMODE_CONF),"0",1))
		dst = strcpyEx( strcpyEx( strcpyEx( gsmBuffer, "$U,"), GsmIMEI()), ",1, ,");
		else
		dst = strcpyEx( strcpyEx( strcpyEx( flashBuffer, "$FRCMD,"), GsmIMEI()), ",_SendMessage,,");
		//setAccState(ACC_OFF,true);
		send_size = frame(dst,Command,true);
		}else
		{
			send_size = Ruptela_frame(dst,Command,true);
		}	
		setAccState(ACC_OFF,false);

		}else if(Command == GSM_TASK_CMD_ACCON)
		{
		if(strncmp(get_conf_elem(RESERVE3_CONF),"1",1)){
		if(!strncmp(get_conf_elem(WORKINGMODE_CONF),"0",1))
		dst = strcpyEx( strcpyEx( strcpyEx( gsmBuffer, "$U,"), GsmIMEI()), ",1, ,");
		else
		dst = strcpyEx( strcpyEx( strcpyEx( flashBuffer, "$FRCMD,"), GsmIMEI()), ",_SendMessage,,");
		//setAccState(ACC_ON,true);
		send_size = frame(dst,Command,true);
		if(!strncmp(get_conf_elem(WORKINGMODE_CONF),"2",1)){ 
		//memcpy(localreadBuffer,flashBuffer,BUFFER_LENGTH/4);
		//GsmTask.Command = GSM_TASK_CMD_GPS_FILEDATA;
		//GsmAddTask(&GsmTask);
		//send_size = 0;
		}
		}else
		{
			send_size = Ruptela_frame(dst,Command,true);
			register GsmContext_t *gc = &GsmContext;
			gc->PingTick =10000;
		}
		setAccState(ACC_ON,false);
		
		}else if(Command == GSM_TASK_CMD_IN_N)
		{
		if(strncmp(get_conf_elem(RESERVE3_CONF),"1",1)){
		if(!strncmp(get_conf_elem(WORKINGMODE_CONF),"0",1))
		dst = strcpyEx( strcpyEx( strcpyEx( gsmBuffer, "$U,"), GsmIMEI()), ",1, ,");
		else
		dst = strcpyEx( strcpyEx( strcpyEx( flashBuffer, "$FRCMD,"), GsmIMEI()), ",_SendMessage,,");
		setInState(IN_N,true);
		send_size = frame(dst,Command,true);
		//setInState(IN_N,false);
		}else
		{
			send_size = Ruptela_frame(dst,Command,true);
		}
		
		}else if(Command == GSM_TASK_CMD_IN_P)
		{
		if(strncmp(get_conf_elem(RESERVE3_CONF),"1",1)){
		if(!strncmp(get_conf_elem(WORKINGMODE_CONF),"0",1))
		dst = strcpyEx( strcpyEx( strcpyEx( gsmBuffer, "$U,"), GsmIMEI()), ",1, ,");
		else
		dst = strcpyEx( strcpyEx( strcpyEx( flashBuffer, "$FRCMD,"), GsmIMEI()), ",_SendMessage,,");
		setInState(IN_P,true);
		send_size = frame(dst,Command,true);
		//setInState(IN_P,false);
		}else
		{
			send_size = Ruptela_frame(dst,Command,true);
		}
		}	else if(Command == GSM_TASK_CMD_ALARM)
		{
		if(strncmp(get_conf_elem(RESERVE3_CONF),"1",1)){
		if(!strncmp(get_conf_elem(WORKINGMODE_CONF),"0",1))
		dst = strcpyEx( strcpyEx( strcpyEx( gsmBuffer, "$U,"), GsmIMEI()), ",2, ,");
		else
		dst = strcpyEx( strcpyEx( strcpyEx( flashBuffer, "$FRCMD,"), GsmIMEI()), ",_AlarmInfo,,");
		}
		if( alarm == ALARM_SOS)
		setAlarmState(ALARM_SOS,true);
		else if( alarm == ALARM_BATTLE_LOW)
		setAlarmState(ALARM_BATTLE_LOW,true);
		else if( alarm == ALARM_CUT_DETECT)
		setAlarmState(ALARM_CUT_DETECT,true);
		else if( alarm == ALARM_OVERSPEED)
		setAlarmState(ALARM_OVERSPEED,true);
		else if( alarm == ALARM_SHOCK)
		setAlarmState(ALARM_SHOCK,true);
		else if( alarm == GPS_LOST)
		setAlarmState(GPS_LOST,true);
		else if( alarm == GPS_FIXED)
		setAlarmState(GPS_FIXED,true);
		else if( alarm == ALARM_JAMMING)
		setAlarmState(ALARM_JAMMING,true);
		
		if(strncmp(get_conf_elem(RESERVE3_CONF),"1",1)){
		send_size = frame(dst,Command,true);
		}else
		{
			send_size = Ruptela_frame(dst,Command,true);
		}
		
		setAlarmState(ALARM_SOS,false);
		setAlarmState(ALARM_BATTLE_LOW,false);
		setAlarmState(ALARM_CUT_DETECT,false);
		setAlarmState(ALARM_OVERSPEED,false);
		setAlarmState(ALARM_SHOCK,false);
		setAlarmState(GPS_LOST,false);
		setAlarmState(GPS_FIXED,false);
		setAlarmState(ALARM_JAMMING,false);
	
		}
		else if(Command == GSM_TASK_CMD_GEOFENCE)
		{
			if(strncmp(get_conf_elem(RESERVE3_CONF),"1",1)){
			if(!strncmp(get_conf_elem(WORKINGMODE_CONF),"0",1))
			dst = strcpyEx( strcpyEx( strcpyEx( gsmBuffer, "$U,"), GsmIMEI()), ",2, ,");
			else
			dst = strcpyEx( strcpyEx( strcpyEx( flashBuffer, "$FRCMD,"), GsmIMEI()), ",_AlarmInfo,,");
			send_size = frame(dst,Command,true);
			}else
			{
			send_size = Ruptela_frame(dst,Command,true);
			}
		}
		if(send_size == 0)
			return;
		if(DebugEnable) DEBUG("flashBuffer %02x",flashBuffer[0]);
		if(send_size > 0)
		bit_map_write_data(flashBuffer,BITMAP_FLASH_DATA_LEN);
		memset(flashBuffer,0,BUFFER_LENGTH/4);
		
	
}

/*******************************************************************************
* Function Name	:	gsmGetFileFTP
* Description	:
*******************************************************************************/
uint8_t gsmGetFileFTP_M35(void)
{
	uint8_t result = E_OK;
	uint8_t retry = 2;
	
	register GsmContext_t *gc = &GsmContext;
	
	retry = 1;
		while (retry-- != 0 && (result = doSendCommand(GPRS_FGCNT_CM,gc,2000,400)) != E_OK)
		osDelay(2000);
		if(result!=E_OK)
			return result;
		retry = 1;
		while (retry-- != 0 && (result = doSendCommand(GPRS_CSGP_CM,gc,2000,400)) != E_OK)
		osDelay(2000);
		if(result!=E_OK)
			return result;
		#if 0
		//gsmCmd1("AT+QIACT\r\n", GSM_COMMAND_TIMEOUT);
		
		//gsmSend("AT+QILOCIP\r\n");
		
		while(retry > 0)
		{
			if(DebugEnable) DEBUG("gsmToken(NULL):%s",gsmToken(NULL));
			retry --;
			osDelay(50);
		}
		#endif
		retry = 1;
		while (retry-- != 0 && (result = doSendCommand(FTP_CFG_USER_CM,gc,2000,400)) != E_OK)
		osDelay(2000);
		if(result!=E_OK)
			return result;
	  retry = 1;
		while (retry-- != 0 && (result = doSendCommand(FTP_CFG_PASSWORD_CM,gc,2000,100)) != E_OK)
		osDelay(2000);
		if(result!=E_OK)
			return result;
		retry = 1;
		while (retry-- != 0 && (result = doSendCommand(FTP_GSM_OPEN_CM,gc,2000,100)) != E_OK)
		osDelay(2000);
		if(result!=E_OK)
			return result;
		retry = 1;
		#if 1
		while (retry-- != 0 && (result = doSendCommand(FTP_CFG_DOWNLOAD_CM,gc,2000,100)) != E_OK)
		osDelay(2000);
		if(result!=E_OK)
			return result;
		 retry = 1;
		#endif
		osDelay(5000);
		#if 1
		while (retry-- != 0 && (result = doSendCommand(FTP_CFG_PATH_CM,gc,2000,400)) != E_OK)
		osDelay(2000);
		if(result!=E_OK)
			return result;
	  retry = 1;
		#endif
		
		while (retry-- != 0 && (result = doSendCommand(FTP_GET_RAM_CM,gc,2000,100)) != E_OK)
		osDelay(2000);
		if(result!=E_OK)
			return result;
		
		retry = 1;
		while (retry-- != 0 && (result = doSendCommand(FTP_FILESIZE_CM,gc,2000,100)) != E_OK)
		osDelay(2000);
		if(result!=E_OK)
			return result;
			
	return result;
}


/*******************************************************************************
* Function Name	:	gsmGetFileFTP
* Description	:
*******************************************************************************/
uint8_t gsmGetFileFTP_UG35(void)
{
	uint8_t result = E_OK;
	uint8_t retry = 1;
	
	register GsmContext_t *gc = &GsmContext;
	
	while (retry-- != 0 && (result = doSendCommand(FTP_CFG_CM,gc,2000,400)) != E_OK)
		osDelay(2000);
		if(result!=E_OK)
			return result;
		retry = 1;
	while (retry-- != 0 && (result = doSendCommand(FTP_OPEN_CM,gc,2000,200)) != E_OK)
		osDelay(2000);
		if(result!=E_OK)
			return result;
		retry = 1;
	
	while (retry-- != 0 && (result = doSendCommand(FTP_CFG_CWD_CM,gc,2000,200)) != E_OK)
		osDelay(2000);
		if(result!=E_OK)
			return result;
		
	retry = 1;
	while (retry-- != 0 && (result = doSendCommand(FTP_GET_CM,gc,2000,100)) != E_OK)
		osDelay(2000);
		if(result!=E_OK)
			return result;
		
	retry = 1;
	while (retry-- != 0 && (result = doSendCommand(FTP_FILESIZE_CM,gc,2000,100)) != E_OK)
		osDelay(2000);
		if(result!=E_OK)
			return result;
		
	return result;
}

/*******************************************************************************
* Function Name	:	openBootFile
* Description	:
*******************************************************************************/
uint8_t flashBootFile(GsmContext_t *gc)
{
	uint8_t result = E_OK;
	uint8_t retry = 1;
	MX_USART4_UART_Init();
	MX_USART5_UART_Init(9600);
	osDelay(2000);
	
	gsmCmd1(GSM_AT, 2000);
	
#if 1
	open_file:
	while (retry-- != 0 && (result = doSendCommand(FTP_FILEOPEN_CM,gc,2000,100)) != E_OK)
			osDelay(2000);
		if(result!=E_OK)
			return result;
		retry = 1;
	while (retry-- != 0 && (result = doSendCommand(FTP_FILESEEK_CM,gc,2000,100)) != E_OK)
		osDelay(2000);
		if(result!=E_OK)
			return result;
	taskENTER_CRITICAL();
	FLASH_If_Erase(BOOT_ADDRESS);
	taskEXIT_CRITICAL();
	DEBUG("FLASH_If_Erase");
	gAddress = BOOT_ADDRESS;
	for(;;)
	{
	gsmRxFlush(gc);
	gc->RxMode = GSM_MODE_UPDATE;
	result = doSendCommand(FTP_FILEREAD_CM,gc,2000,400);
		if(result == E_FW_OK)
			break;
		if(result == E_ERROR)
		{
			initGsmUart();
			doSendCommand(FTP_FILECLOSE_CM,gc,2000,400);
			goto open_file;
		}
	}
	
	
	update_conf_elem(IAP_CONF,"-b");
	
	__disable_irq();
	HAL_NVIC_SystemReset();
	__enable_irq() ;
	#endif
	return result;
}


uint8_t updateAGPS(GsmContext_t *gc)
{

	uint8_t result = E_OK;
	uint8_t retry = 1;
		
	int8_t date_buff[10];
	int8_t *date_point;
	
	MX_USART4_UART_Init();
	MX_USART5_UART_Init(9600);
	osDelay(2000);
	
	ano_number = 0;
	gsmCmd1(GSM_AT, 2000);
	
	open_file:
	while (retry-- != 0 && (result = doSendCommand(FTP_FILEOPEN_CM,gc,2000,100)) != E_OK)
			osDelay(2000);
		if(result!=E_OK)
			return result;
		retry = 1;
	while (retry-- != 0 && (result = doSendCommand(FTP_FILESEEK_CM,gc,2000,100)) != E_OK)
		osDelay(2000);
		if(result!=E_OK)
			return result;
	
	bit_map_erase_agps_start();
		
	bit_map_erase_agps_start();
	RTC_DateTypeDef date;
	HAL_RTC_GetDate(&RTCHandle, &date, RTC_FORMAT_BIN);
	memset(date_buff,0,10);
	date_point = date_buff;
	date_point = itoa(date_buff, date.Date, 2);
	date_point = itoa(date_point, date.Month, 2);
	DEBUG("###################date_point %s",date_buff);
	bit_map_write_agps_flag(date_buff,4);
	
  CFC_HOT_RST();		
		
	for(;;)
	{
	gsmRxFlush(gc);
	gc->RxMode = GSM_MODE_UPDATE;
	if(ano_number == 52){
		return E_FW_OK;
	}
	result = doSendCommand(FTP_FILEREAD_CM,gc,2000,400);
	ano_number++;
		if(result == E_FW_OK)
			break;
		if(result == E_ERROR)
		{
			initGsmUart();
			doSendCommand(FTP_FILECLOSE_CM,gc,2000,400);
			goto open_file;
		}
	}
	
	return result;

}


uint8_t doSendCommand(uint8_t cmd,GsmContext_t *gc,uint32_t timeout,uint32_t timestep)
{
  uint32_t wait = timeout;
	int8_t sendMessage[50];
	uint8_t ota_parameter[50]= {0};
	uint8_t ota_parameter1[20]={0};
	uint8_t filename[20]={0};
	uint8_t * Token;
	uint8_t * Error;
	uint8_t * p;
	uint8_t * pEnd;
	uint8_t result = E_OK;

	p = getOta();
	
	switch(cmd)
	{
		case FTP_CFG_CM:
			p = (int8_t*)strchr((char *)p,':');
			if(p == NULL)
				return E_ERROR;
			p +=1;
			pEnd = (int8_t*)strchr((char *)p,':');
			if(pEnd == NULL)
				return E_ERROR;
			memcpy(ota_parameter,p,pEnd-p);
			p = (int8_t*)strchr((char *)p,':');
			if(p == NULL)
				return E_ERROR;
			p +=1;
			pEnd = (int8_t*)strchr((char *)p,':');
			if(pEnd == NULL)
				return E_ERROR;
			memcpy(ota_parameter1,p,pEnd-p);
			if(strlen(ota_parameter)!=0 && strlen(ota_parameter1)!=0)
			sprintf((char*)sendMessage, "AT+QFTPCFG=\"account\",\"%s\",\"%s\"\r\n", ota_parameter,ota_parameter1);
			else
			sprintf((char*)sendMessage, "AT+QFTPCFG=\"account\",\"%s\",\"%s\"\r\n", "firmware","123456");
		  Token = NULL;
			break;
		case FTP_OPEN_CM:
			pEnd = (int8_t*)strchr((char *)p,':'); 
			if(pEnd == NULL)
				return E_ERROR;
			memcpy(ota_parameter,p,pEnd-p);
			if(strlen(ota_parameter)!=0)
			sprintf((char*)sendMessage, "AT+QFTPOPEN=\"%s\",%s\r\n",ota_parameter,"21");
			else
			sprintf((char*)sendMessage, "AT+QFTPOPEN=\"%s\",%s\r\n",(char *)get_conf_elem(MAINSERVERDOMAINNAME_CONF),"21");
			Token = GSM_FTP_OPEN;
			break;
		case FTP_GSM_OPEN_CM:
			pEnd = (int8_t*)strchr((char *)p,':');
			if(pEnd == NULL)
				return E_ERROR;		
			memcpy(ota_parameter,p,pEnd-p);
			if(strlen(ota_parameter)!=0)
			sprintf((char*)sendMessage, "AT+QFTPOPEN=\"%s\",%s\r\n",ota_parameter,"21");
			else
			sprintf((char*)sendMessage, "AT+QFTPOPEN=\"%s\",%s\r\n",(char *)get_conf_elem(MAINSERVERDOMAINNAME_CONF),"21");
			Token = GSM_FTP_OPEN_;
			break;
		case FTP_GET_CM:
			p = (int8_t*)strchr((char *)p,':');
			if(p == NULL)
				return E_ERROR;
			p +=1;
			p = (int8_t*)strchr((char *)p,':');
			if(p == NULL)
				return E_ERROR;
			p +=1;
			p = (int8_t*)strchr((char *)p,':');
			if(p == NULL)
				return E_ERROR;
			p +=1;
			p = (int8_t*)strchr((char *)p,':');
			if(p == NULL)
				return E_ERROR;
			p +=1;
			memcpy(ota_parameter,p,strlen(p));
			if(strlen(ota_parameter)!=0){
			sprintf((char*)sendMessage, "AT+QFTPGET=\"%s\",\"%s\"\r\n", ota_parameter,strcat(strcat(ota_parameter1,"RAM:"),ota_parameter));
			memset(GG_FwFileName,0,32);
			strncpy((char *)GG_FwFileName, ota_parameter, strlen(ota_parameter));
			}
			else
			sprintf((char*)sendMessage, "AT+QFTPGET=\"%s\",\"%s\"\r\n", "te101.bin","RAM:te101.bin");
			Token = GSM_FTP_GET;
			break;
		case FTP_GET_RAM_CM:
			p = (int8_t*)strchr((char *)p,':');
			if(p == NULL)
				return E_ERROR;
			p +=1;
			p = (int8_t*)strchr((char *)p,':');
			if(p == NULL)
				return E_ERROR;
			p +=1;
			p = (int8_t*)strchr((char *)p,':');
			if(p == NULL)
				return E_ERROR;
			p +=1;
			p = (int8_t*)strchr((char *)p,':');
			if(p == NULL)
				return E_ERROR;
			p +=1;
			memcpy(ota_parameter,p,strlen(p));
			if(strlen(ota_parameter)!=0){
			sprintf((char*)sendMessage, "AT+QFTPGET=\"%s\",%d\r\n", ota_parameter,204800);
			memset(GG_FwFileName,0,32);
			strncpy((char *)GG_FwFileName, ota_parameter, strlen(ota_parameter));
			}
			else
			sprintf((char*)sendMessage, "AT+QFTPGET=\"%s\",%d\r\n", "te101.bin",204800);
			Token = GSM_FTP_GET_RAM;
			Error = GSM_FTP_GET_RAM_ERROR;
			break;
		case FTP_CFG_DOWNLOAD_CM:
			p = (int8_t*)strchr((char *)p,':');
			if(p == NULL)
				return E_ERROR;
			p +=1;
			p = (int8_t*)strchr((char *)p,':');
			if(p == NULL)
				return E_ERROR;
			p +=1;
			p = (int8_t*)strchr((char *)p,':');
			if(p == NULL)
				return E_ERROR;
			p +=1;
			p = (int8_t*)strchr((char *)p,':');
			if(p == NULL)
				return E_ERROR;
			p +=1;
			memcpy(ota_parameter,p,strlen(p));
			if(strlen(ota_parameter)!=0){
			sprintf((char*)sendMessage, "AT+QFTPCFG=4,\"/%s/%s\"\r\n","RAM",ota_parameter);
			memset(GG_FwFileName,0,32);
			strncpy((char *)GG_FwFileName, ota_parameter, strlen(ota_parameter));
			}
			else
			sprintf((char*)sendMessage, "AT+QFTPCFG=4,\"/%s/%s\"\r\n","RAM","te101.bin");
			Token = NULL;
			//Token = GSM_FTP_CFG;
			break;
		case FTP_CFG_PATH_CM:
			p = (int8_t*)strchr((char *)p,':');
			if(p == NULL)
				return E_ERROR;
			p +=1;
			p = (int8_t*)strchr((char *)p,':');
			if(p == NULL)
				return E_ERROR;
			p +=1;
			p = (int8_t*)strchr((char *)p,':');
			if(p == NULL)
				return E_ERROR;
			p +=1;
			pEnd = (int8_t*)strchr((char *)p,':');
			if(pEnd == NULL)
				return E_ERROR;
			memcpy(ota_parameter,p,pEnd-p);
			if(strlen(ota_parameter)!=0)
			sprintf((char*)sendMessage, "AT+QFTPPATH=\"%s\"\r\n", strcat(ota_parameter,"/"));
			else
			sprintf((char*)sendMessage, "AT+QFTPPATH=\"%s\"\r\n", "/var/www/tracker_srv/file_srv/firmware/");
			//Token = GSM_FTP_PATH;
			Token = NULL;
			break;
		case FTP_CFG_CWD_CM:
			p = (int8_t*)strchr((char *)p,':');
			if(p == NULL)
				return E_ERROR;
			p +=1;
			p = (int8_t*)strchr((char *)p,':');
			if(p == NULL)
				return E_ERROR;
			p +=1;
			p = (int8_t*)strchr((char *)p,':');
			if(p == NULL)
				return E_ERROR;
			p +=1;
			pEnd = (int8_t*)strchr((char *)p,':');
			if(pEnd == NULL)
				return E_ERROR;
			memcpy(ota_parameter,p,pEnd-p);
			if(strlen(ota_parameter)!=0)
			sprintf((char*)sendMessage, "AT+QFTPCWD=\"%s\"\r\n", strcat(ota_parameter,"/"));
			else
			sprintf((char*)sendMessage, "AT+QFTPCWD=\"%s\"\r\n", "/var/www/tracker_srv/file_srv/firmware/");
			Token = GSM_FTP_CWD;
			break;
		case FTP_CFG_USER_CM:
			p = (int8_t*)strchr((char *)p,':');
			if(p == NULL)
				return E_ERROR;
			p +=1;
			pEnd = (int8_t*)strchr((char *)p,':');
			if(pEnd == NULL)
				return E_ERROR;
			memcpy(ota_parameter,p,pEnd-p);
			if(strlen(ota_parameter)!=0)
			sprintf((char*)sendMessage, "AT+QFTPUSER=\"%s\"\r\n", ota_parameter);
			else
			sprintf((char*)sendMessage, "AT+QFTPUSER=\"%s\"\r\n", "firmware");
			Token = NULL;
			break;
		case FTP_CFG_PASSWORD_CM:
			p = (int8_t*)strchr((char *)p,':');
			if(p == NULL)
				return E_ERROR;
			p +=1;
			p = (int8_t*)strchr((char *)p,':');
			if(p == NULL)
				return E_ERROR;
			p +=1;
			pEnd = (int8_t*)strchr((char *)p,':');
			if(pEnd == NULL)
				return E_ERROR;
			memcpy(ota_parameter,p,pEnd-p);
			if(strlen(ota_parameter)!=0)
			sprintf((char*)sendMessage, "AT+QFTPPASS=\"%s\"\r\n", ota_parameter);
			else
			sprintf((char*)sendMessage, "AT+QFTPPASS=\"%s\"\r\n", "123456");
			Token = NULL;
			break;
		case GPRS_FGCNT_CM:
			sprintf((char*)sendMessage, "AT+QIFGCNT=0\r\n");
			Token = NULL;
			break;
		case GPRS_CSGP_CM:
			sprintf((char*)sendMessage, "AT+QICSGP=1,\"%s\"\r\n",(char *)get_conf_elem(APNNAME_CONF));
			Token = NULL;
			break;
		case FTP_FILESIZE_CM:
			if(strlen(GG_FwFileName)!=0)
			sprintf((char*)sendMessage, "AT+QFTPSIZE=\"%s\"\r\n",GG_FwFileName);
			else
			sprintf((char*)sendMessage, "AT+QFTPSIZE=\"%s\"\r\n","te101.bin");
			Token = GSM_FTP_FSIZE;
			break;
		case FTP_FILEOPEN_CM:
			if(strlen(GG_FwFileName)!=0)
			sprintf(sendMessage, "AT+QFOPEN=\"%s\",%s\r\n", (char *)strcat(strcat((char *)filename,"RAM:"),GG_FwFileName),"0");
			else
			sprintf(sendMessage, "AT+QFOPEN=\"%s\",%s\r\n", (char *)strcat(strcat((char *)filename,"RAM:"),"iap.bin"),"0");
			Token = GSM_FILE_OPEN;
			break;
		case FTP_FILESEEK_CM:
			sprintf(sendMessage, "AT+QFSEEK=%s,%s,%s\r\n", file_handle,"0","0");
			Token = NULL;
			break;
		case FTP_FILEREAD_CM:
			sprintf(sendMessage, "AT+QFREAD=%s,%s\r\n", file_handle,"1024");
			Token = NULL;
			break;
		case FTP_FILECLOSE_CM:
			sprintf(sendMessage, "AT+QFCLOSE=%s\r\n", file_handle);
			Token = NULL;
			break;
	}
	
	while(wait != 0){
			if(cmd == FTP_FILEREAD_CM){
			initGsmUart();
		  gsmRxFlush(gc);
		  gc->RxMode = GSM_MODE_UPDATE;
		  memset(gsmRxBuffer,0,BUFFER_LENGTH+32);
			if(GG_FwSmsIndex == -2)
			sprintf(sendMessage, "AT+QFREAD=%s,%s\r\n", file_handle,"84");
			else
		  sprintf(sendMessage, "AT+QFREAD=%s,%s\r\n", file_handle,"1024");
		  gsmSend((int8_t*)sendMessage);
		  receiveData = 0;
			if(GG_FwSmsIndex == -2)
			result = handleReadAGPSData(gc);
			else
		  result = handleReadData(gc);
			return result;	
			}
		
			if (E_OK != gsmCmd1((int8_t*)sendMessage, GSM_COMMAND_TIMEOUT))
			{
			osDelay(5000);
			wait -= timestep;
			continue;
		}else{
			gsmRxFlush(gc);
			gc->RxMode = GSM_MODE_URC;
			break;
	}
	}
	
	if(Token != NULL){
		wait = timeout;
		while(strstr((char *)gsmToken(NULL), (char *)Token) == NULL){
		if(strstr((char *)gsmToken(NULL), "+QFTPGET: 609") != NULL)
		{
			return E_TIMEOUT;
		}
		osDelay(10000);
		wait -= timestep;
		if(wait == 0)
			break;
	}
	if(strstr((char *)gsmToken(NULL),"+QFTPSIZE:") != NULL)
	{
		p = gsmToken(NULL);
		p+=strlen("+QFTPSIZE:");
		if(*p == ' ')
			p +=3;
		if(DebugEnable) DEBUG("p:%s",p);
		if(DebugEnable) DEBUG("filesize:%s",battVolt);
		if(strncmp(p,battVolt,5))
			return E_TIMEOUT;
		else
			return E_OK;
	}else if(strstr((char *)gsmToken(NULL),"+QFTPGET:") != NULL)
	{
		p = gsmToken(NULL);
		p+=strlen("+QFTPGET:");
		if(*p == ' ')
			p +=3;
		memset(battVolt,0,strlen(battVolt));
		strncpy(battVolt,p,5);
	}
	
		if(FTP_FILEOPEN_CM == cmd){
		memset(file_handle,0,strlen(file_handle));		
		strcpy((char *)file_handle,(char *)gsmToken(NULL)+9);
		}

	if(DebugEnable) DEBUG("gsmToken:%s",gsmToken(NULL));
	}

  if(wait == 0)
	{
		return E_TIMEOUT;
	}
  return E_OK;
}

void setGsmSleep(bool sleep)
{
 gsm_gotoSleep = sleep;
}

bool getGsmSleep()
{
 return gsm_gotoSleep;
}

bool getTaskWaiting(void)
{
	register GsmContext_t *gc = &GsmContext;
	return uxQueueMessagesWaiting(gc->TaskQueue) == 0;
}

void IF_DOWNLOAD_AGPS(void)
{
	uint8_t agps_buff[5];
	uint8_t date_buff[5];
	uint8_t *date_point;
	
	if(strcmp((char *)get_conf_elem(RESERVE7_CONF),"1"))
		return;
	
	memset(agps_buff,0,5);
	memset(date_buff,0,5);
	bit_map_read_agps_flag(agps_buff,4);
	if(DebugEnable)  DEBUG("agps data date %s",agps_buff);
	
	RTC_DateTypeDef date;
	HAL_RTC_GetDate(&RTCHandle, &date, RTC_FORMAT_BIN);
	memset(date_buff,0,5);
	date_point = date_buff;
	date_point = itoa(date_buff, date.Date, 2);
	date_point = itoa(date_point, date.Month, 2);	
	if(DebugEnable)  DEBUG("rtc data date %s",date_buff);
	
	if(!strcmp(agps_buff,date_buff)){
		check_agps_flag = 0;
		return;
	}
	else if(getTCPConState())
		check_agps_flag++;
	
	if(check_agps_flag < 2)
		return;
	else
		check_agps_flag = 0;
	
	setAGPS_Ota();
	GG_FwSmsIndex = -2;
	GG_FwFileName[0]='U';
}

/*******************************************************************************
* Function Name	:	vGsmTask
* Description	:	GSM Task
*******************************************************************************/
void vGsmTask(void const  *pvArg)
{
	register GsmContext_t *gc = &GsmContext;
	int GsmTaskTimeOut = 500;
	uint8_t ota = 2;
	uint8_t timeRequestDate = 0;

#ifdef DISPLAY_STACK
	if(DebugEnable) DEBUG("*** GSM Stack 0:");
#endif
	vSemaphoreCreateBinary( gc->ReadySemaphore );
	if (gc->ReadySemaphore == NULL)
		ShowFatalError(ERROR_GSM_TASK_FAIL);
	xSemaphoreTake( gc->ReadySemaphore, 1 );

	gc->TaskQueue = xQueueCreate(GSM_TASK_QUEUE_SIZE, sizeof(GsmTask_t));

	if (gc->TaskQueue == NULL)
		ShowFatalError(ERROR_GSM_TASK_FAIL);

#ifdef DISPLAY_STACK
	if(DebugEnable) DEBUG("*** GSM Stack 1:");
#endif

gsm_restart:
	
	if(DebugEnable) DEBUG("gsm_restart");

	GsmDeInit();
	seeksize = 0;
	
	gc->Status &= ~GSM_STATUS_TCP_ON;
	
	gc->PingTick = 60;

	while (gc->PingTick != 0 && (GlobalStatus & GPS_COMPLETE) == 0)
	{
		gc->PingTick--;
		osDelay(1000);
	}
	
	read_eh_ml_data();
	
	geofence_init();
	
	//open_eh_and_ml_file();
	
	//open_gefence_file();
		
	gc->PingInterval = GSM_PING_MS * configTICK_RATE_HZ;
	gc->Status = 0;
	gc->AuthErrorCount = 0;
	gc->IMEI[0] = 0;
	
	// Initial delay w/o transmite any so GPS can fix up quickly
	gc->LastReplyTime = xTaskGetTickCount();
	
	while((xTaskGetTickCount() - gc->LastReplyTime) < GSM_INITIAL_WAIT)
	{
		if (uxQueueMessagesWaiting(gc->TaskQueue))
			break;
		osDelay(1000);
	}
	
	gsmWaitNetwork(gc);
	
	GlobalStatus |= GSM_COMPLETE;

	#if 0
	while (gsmHostUndefine())
	{
		osDelay(5000);
		gsmScanSMS(gc);
		if (gsmIsRegister(gc) != E_OK)
			gsmWaitNetwork(gc);
	}
	#endif

	// Set next time for send Ping
	gc->PingTick = xTaskGetTickCount() + 60000; // first time ping period longer

	if(GsmTask.Command != GSM_TASK_CMD_LOGIN){
	GsmTask.Command = GSM_TASK_CMD_LOGIN ;
	GsmAddTask(&GsmTask);
	}
	

#ifdef DISPLAY_STACK
	if(DebugEnable) DEBUG("*** GSM Stack 2:");
#endif
	
	for(;;)
	{		
		TaskRun = 1;
		
		if(gsmtestmode) 
		{
		gsmtestmode = false;
		osDelay(4000);
		if(module == 0){
		DEBUG("GSM TEST OK,MODULE IS M35");
		}
		else if(module == 1){
		DEBUG("GSM TEST OK,MODULE IS UG35");
		}else
		{
		DEBUG("GSM TEST OK,MODULE IS UG96");
		}
		}
		
		initGprsParameter();
		
		if ((GlobalStatus & GSM_SLEEP)){
			setGsmSleep(true);
			continue;
		}else{
			setGsmSleep(false);
		}

		if((GlobalStatus & GSM_WAKEUP))
		{
			GlobalStatus &= ~GPS_COMPLETE;
			GlobalStatus |= GPS_STOP;
			GSM_PWR_OFF();
			goto gsm_restart;
		}

		GlobalStatus &= ~GPS_STOP;
		
		if (xQueueReceive(gc->TaskQueue, &GsmTask, GsmTaskTimeOut) != pdPASS)
		{
			// No command for GSM, check timeout and make a Ping
			if (xTaskGetTickCount() < gc->PingTick)
			{	// No message to host, check host nessages
				if (gc->Status & GSM_STATUS_TCP_ON){
					if(xTaskGetTickCount() % 10 == 0)
							gsmSend(GSM_CGREG);
							//gsmSend("AT+CCLK?\r\n");
							//gsmSend("AT+QPING=1,\"47.89.17.248\"\r\n");
					}
					if(xTaskGetTickCount() % 15 == 0 && !strcmp((char *)get_conf_elem(RESERVE8_CONF),"1"))
							gsmSend("AT+QJDR?\r\n");
					gsmProcessHostResponse(gc, false);
				//}
				continue;
			}
			else
			{
				// Send ping if no task during GSM_PING_MS ms
				GsmTask.Command = GSM_TASK_CMD_PING;
				GsmTask.SenseD1 = (SENS_D1_READ() ? 1 : 0);
				GsmTask.SenseA1 = 0;

#if (USE_CAN_SUPPORT == 1)
				CanEnable = 0;
				while (!CanDisabled)
					osDelay(1);

				for (idx = 0; idx < CAN_VALUES_SIZE; idx ++)
					GsmTask.CanData[idx] = CanSumGet(idx);

				CanSumInit();
				CanEnable = 1;
#endif
			}
		}

		gc->PingInterval = atoi(get_conf_elem(PING_TIME_CONF))* configTICK_RATE_HZ;
		
		if(gc->PingInterval < 40 * configTICK_RATE_HZ)
			gc->PingInterval = 40 * configTICK_RATE_HZ;
		else if(gc->PingInterval > 600 * configTICK_RATE_HZ)
		  gc->PingInterval = 300 * configTICK_RATE_HZ;
		else
			gc->PingInterval = 60 * configTICK_RATE_HZ;
		
		if(getTCPConState())
		gc->PingTick = xTaskGetTickCount() + gc->PingInterval;
		else
		gc->PingTick = xTaskGetTickCount() + 90 * configTICK_RATE_HZ;
		
		if(getCutStatus() > 0)
			gc->PingTick = xTaskGetTickCount() + 120 * configTICK_RATE_HZ;

		gc->Error = gsmSendTaskCommand(gc);
		
		if (gc->Error == E_OK)
			gsmProcessHostResponse(gc, true);
		else if (gc->Error == E_NO_RESPONSE)
			gsmProcessHostResponse(gc, false);
		else if (gsmHostUndefine())
			goto gsm_restart;
		else
			gsmProcessHostResponse(gc, false);
		
		//if(getTCPConState() && (GlobalStatus & GPS_COMPLETE))
		   //handle_locationdata();
		
		if(DebugEnable) DEBUG("GlobalStatus:%x",GlobalStatus);
		
		TaskRun = 2;
		
		if(restGSM && !getTCPConState())
		{
		restGSM = false;
		goto gsm_restart;
		}else if(restGSM && getTCPConState())
		{
		restGSM = false;
		}
		
		if(GG_FwSmsIndex == 0)
		{
		IF_DOWNLOAD_AGPS();
		}
		
		RTC_DateTypeDef date;
		HAL_RTC_GetDate(&RTCHandle, &date, RTC_FORMAT_BIN);	
		
		timeRequestDate++;		
		if(date.Year < 10 && getTCPConState() && timeRequestDate % 2 == 0){
		timeRequestDate = 0;
		GsmTask.Command = GSM_TASK_CMD_REQUEST;	
		GsmAddTask(&GsmTask);
		}
		
		if (GG_FwSmsIndex != 0
		&&	GG_FwFileName[0] != 0) //&&
			//uxQueueMessagesWaiting(gc->TaskQueue) == 0)
		{
			if(GG_FwSmsIndex == -2)
			{
				osDelay(1000);
			}
			gsmDisconnectTCP(gc);
			ota = 0;
			//switch (gsmGetFileFTP(gc, GG_FwFileName))
			//switch (gsmGetFileFTP_UG35(gc, "te101.bin"))
			ota_restart:
			switch((*gc->Commands->GetFTPFile)())
			{
				case E_ERROR:
					if(ota > 0){
					ota--;
					goto ota_restart;
					}else{
					gsmCmd1("AT+QFTPCLOSE\r\n", GSM_COMMAND_TIMEOUT);
					//SystemRestart();
					GG_FwSmsIndex = 0;
					GG_FwFileName[0] = 0;
					ota_update_sms_notify();
					}
					if(GG_FwSmsIndex != 0)
						GG_FwSmsIndex = 0;
					break;
				case E_FW_ERROR:
					if(GG_FwSmsIndex != 0)
						GG_FwSmsIndex = 0;
					break;
				case E_TIMEOUT:
					if(ota > 0){
					ota--;
					goto ota_restart;
					}else{
					gsmCmd1("AT+QFTPCLOSE\r\n", GSM_COMMAND_TIMEOUT);
					//SystemRestart();
					GG_FwSmsIndex = 0;
					GG_FwFileName[0] = 0;
					ota_update_sms_notify();
					}
					if(GG_FwSmsIndex != 0)
						GG_FwSmsIndex = 0;
					break;
				case E_OK:
					//gsmDeleteSMS(GG_FwSmsIndex);
					gsmCmd1("AT+QFTPCLOSE\r\n", GSM_COMMAND_TIMEOUT);
					if(DebugEnable) DEBUG("GG_FwFileName %s",GG_FwFileName);
				
					if(GG_FwSmsIndex == -2)
					{
						updateAGPS(gc);
						ano_number = 0;
						GG_FwSmsIndex = 0;
						GG_FwFileName[0]= 0;
						goto gsm_restart;
						
					}else{
					if(strncmp(GG_FwFileName,"iap.bin",7)){
					if(DebugEnable) DEBUG("Reboot");
					if(update_conf_elem(IAP_CONF,GG_FwFileName))
					{
					if(DebugEnable)
						DEBUG("success %s",GG_FwFileName);
					}
				  }
					HAL_RTCEx_BKUPWrite(&RTCHandle,RTC_BKP_DR3,1);
					if(HAL_RTC_DeInit(&RTCHandle) != HAL_OK)
					{
					if(DebugEnable) DEBUG("RTC_DeInit error");
					}
					disableIRQ();
					GPS_DEINIT();
					//initGsmUart();
					deInitWWDG();
					__disable_irq();
					if(!strncmp(GG_FwFileName,"iap.bin",7)||!strncmp(GG_FwFileName,"te103boot.bin",13)){
					if(flashBootFile(gc) != E_OK)
						ota_update_sms_notify();
					}
					else
					gJumpToApplication();
					//__enable_irq();
					//SystemRestart();
					//CoreRestart();
					}
					break;
			}
			GlobalStatus &= ~GPS_STOP;
		}
#ifdef DISPLAY_STACK
		if(DebugEnable) DEBUG("*** GSM Stack 3:");
#endif
	}
}

bool isJTSimcard(void)
{
if(!strncmp(imsi,"23450",5))
	return true;
else
	return false;
}

bool getTCPConState(void)
{
	register GsmContext_t *gc = &GsmContext;
	if (gc->Status & GSM_STATUS_TCP_ON)
		return true;
	else 
		return false;

}

void getBattVolt(void)
{
	if (GG_FwSmsIndex == 0){
	atCommand(GSM_CBC);
	}
}

void getSignal(void)
{
	TaskRun = 0;
	if(testSignal != 0){
		//if(isAccOn())
		//SystemRestart();
		//else
		{
		register GsmContext_t *gc = &GsmContext;
		gc->Status &= ~GSM_STATUS_TCP_ON;
		restGSM = true;
		}
	}
	if (GG_FwSmsIndex == 0){
	testSignal = 1;
	atCommand(GSM_CSQ);
	}

}

void checkSignal(void)
{
	if(DebugEnable) DEBUG("checkSignal %d",TaskRun);
	
	if(TaskRun == 0){
		HAL_RTCEx_BKUPWrite(&RTCHandle,RTC_BKP_DR3,0);
		SystemRestart();
	}
	
	if(testSignal != 0){
		//SystemRestart();
		register GsmContext_t *gc = &GsmContext;
		gc->Status &= ~GSM_STATUS_TCP_ON;
		restGSM = true;
	}
}

int8_t* getSigalQuality(void)
{
		return signal;
}

int8_t* getVoltage(void)
{
		return battVolt;
}

void gsmDisconnectNotify()
{
		gsmDisconnect = true;
}

void filedataEvent(void)
{
	GsmTask_t * task = &GsmTask;
	task->Command = GSM_TASK_CMD_GPS_FILEDATA;
	GsmAddTask(task);
}

void accEvent(uint8_t event)
{
	//if(event == ACC_EVENT_ON)
	//CFC_GLONASS_OR_BDS(true);
	//else
	//CFC_GLONASS_OR_BDS(false);
	
	register GsmContext_t *gc = &GsmContext;
	#if 0
	if(!GpsNoData()){
		if(event == ACC_EVENT_ON && !strncmp(get_conf_elem(WORKINGMODE_CONF),"2",1) 
			&& getTCPConState()){
		gc->Status &= ~GSM_STATUS_TCP_ON;
		}
		setGpsSendTimer();
		return;
	}
	#endif
	GsmTask_t * task = &GsmTask;
	if(event == ACC_EVENT_ON && !strncmp(get_conf_elem(WORKINGMODE_CONF),"2",1) 
			&& getTCPConState()){
		gc->Status &= ~GSM_STATUS_TCP_ON;
		gsmDisconnectNotify();
		task->Command = GSM_TASK_CMD_RECONNECT;
		GsmAddTask(task);		
	}
	setGpsSendTimer(event);
	if(event == ACC_EVENT_OFF)
	task->Command = GSM_TASK_CMD_ACCOFF;
	else
	task->Command = GSM_TASK_CMD_ACCON;
	if(getTCPConState()){
	GsmAddTask(task);
	}else
		write_locationdata(task->Command,NO_ALARM);
}

void inEvent(uint8_t event)
{
	GsmTask_t * task = &GsmTask;
	if(event == IN_N_EVENT)
	task->Command = GSM_TASK_CMD_IN_N;
	else
	task->Command = GSM_TASK_CMD_IN_P;
	if(getTCPConState()){
	GsmAddTask(task);
	}else{
		write_locationdata(task->Command,NO_ALARM);
	}
}


void ehEvent(uint8_t event, int8_t* cmd)
{
	GsmTask_t * task = &GsmTask;
	task->Command = GSM_TASK_CMD_EH;
	memset(task->Content,0,strlen((char*)task->Content));
	if(cmd != NULL)
	memcpy(task->Content,cmd,strlen((char*)cmd));
	else
	memcpy(task->Content,"NULL",4);
	GsmAddTask(task);
}

void mlEvent(uint8_t event,int8_t* cmd)
{
	GsmTask_t * task = &GsmTask;
	task->Command = GSM_TASK_CMD_ML;
	memset(task->Content,0,strlen((char*)task->Content));
	memcpy(task->Content,cmd,strlen((char*)cmd));
	GsmAddTask(task);
}

void pointEvent(uint8_t event)
{
	GsmTask_t * task = &GsmTask;
	task->Command = GSM_TASK_CMD_POINT;
	GsmAddTask(task);
}

void sosEvent(uint8_t event)
{
	if(getTCPConState()){
	GsmTask_t * task = &GsmTask;
	task->Command = GSM_TASK_CMD_ALARM;
	memset(task->Content,0,strlen((char*)task->Content));
	memcpy(task->Content,"SosButton",strlen((char*)"SosButton"));
	GsmAddTask(task);
	}else{
		write_locationdata(GSM_TASK_CMD_ALARM,ALARM_SOS);
	}
}


void batterylowEvent(uint8_t event)
{
	if(getTCPConState()){
	GsmTask_t * task = &GsmTask;
	task->Command = GSM_TASK_CMD_ALARM;
	memset(task->Content,0,strlen((char*)task->Content));
	memcpy(task->Content,"BatteryLow",strlen((char*)"BatteryLow"));
	GsmAddTask(task);
	}else{
		write_locationdata(GSM_TASK_CMD_ALARM,ALARM_BATTLE_LOW);
	}
}


void cutdetectlowEvent(uint8_t event)
{
	if(getTCPConState()){
	GsmTask_t * task = &GsmTask;
	task->Command = GSM_TASK_CMD_ALARM;
	memset(task->Content,0,strlen((char*)task->Content));
	memcpy(task->Content,"CutDetect",strlen((char*)"CutDetect"));
	GsmAddTask(task);
	}else{
		write_locationdata(GSM_TASK_CMD_ALARM,ALARM_CUT_DETECT);
	}
}


void overspeedEvent(uint8_t event)
{
	if(getTCPConState()){
	GsmTask_t * task = &GsmTask;
	task->Command = GSM_TASK_CMD_ALARM;
	memset(task->Content,0,strlen((char*)task->Content));
	memcpy(task->Content,"OverSpeedWarn",strlen((char*)"OverSpeedWarn"));
	GsmAddTask(task);
	}else{
		write_locationdata(GSM_TASK_CMD_ALARM,ALARM_OVERSPEED);
	}
}

void shockEvent(uint8_t event)
{
	if(getTCPConState()){
	GsmTask_t * task = &GsmTask;
	task->Command = GSM_TASK_CMD_ALARM;
	memset(task->Content,0,strlen((char*)task->Content));
	memcpy(task->Content,"ShockWarn",strlen((char*)"ShockWarn"));
	GsmAddTask(task);
	}else{
		write_locationdata(GSM_TASK_CMD_ALARM,ALARM_SHOCK);
	}
}

void gpslostEvent(uint8_t event)
{
	if(getTCPConState()){
	GsmTask_t * task = &GsmTask;
	task->Command = GSM_TASK_CMD_ALARM;
	memset(task->Content,0,strlen((char*)task->Content));
	memcpy(task->Content,"GpsState=0",strlen((char*)"GpsState=0"));
	GsmAddTask(task);
	}else{
		write_locationdata(GSM_TASK_CMD_ALARM,GPS_LOST);
	}
}

void gpsfixEvent(uint8_t event)
{
	if(getTCPConState()){
	GsmTask_t * task = &GsmTask;
	task->Command = GSM_TASK_CMD_ALARM;
	memset(task->Content,0,strlen((char*)task->Content));
	memcpy(task->Content,"GpsState=1",strlen((char*)"GpsState=1"));
	GsmAddTask(task);
	}else{
		write_locationdata(GSM_TASK_CMD_ALARM,GPS_FIXED);
	}
}


void accOnEvent(uint8_t event)
{
	if(getTCPConState()){
	GsmTask_t * task = &GsmTask;
	task->Command = GSM_TASK_CMD_ALARM;
	memset(task->Content,0,strlen((char*)task->Content));
	memcpy(task->Content,"AccOn",strlen((char*)"AccOn"));
	GsmAddTask(task);
	}
	else{
		write_locationdata(GSM_TASK_CMD_ALARM,NO_ALARM);
	}
}

void geofenceEvent(uint8_t event)
{
	if(getTCPConState()){
	GsmTask_t * task = &GsmTask;
	task->Command = GSM_TASK_CMD_GEOFENCE;
	GsmAddTask(task);
	}else{
		write_locationdata(GSM_TASK_CMD_GEOFENCE,NO_ALARM);
	}
}

void signalEvent(void)
{
	GsmTask_t * task = &GsmTask;
	task->Command = GSM_TASK_CMD_SIGNAL;
	GsmAddTask(task);
	if(!getTCPConState())
		write_locationdata(GSM_TASK_CMD_SIGNAL,NO_ALARM);
}

void rtcEvent(void)
{
	GsmTask_t * task = &GsmTask;
	task->Command = GSM_TASK_CMD_RTC;
	GsmAddTask(task);
}

void jammingEvent(void)
{
	if(getTCPConState()){
	GsmTask_t * task = &GsmTask;
	task->Command = GSM_TASK_CMD_ALARM;
	memset(task->Content,0,strlen((char*)task->Content));
	memcpy(task->Content,"JammingWarn",strlen((char*)"JammingWarn"));
	GsmAddTask(task);
	}else{
		write_locationdata(GSM_TASK_CMD_ALARM,ALARM_JAMMING);
	}
}

void atCommand(const int8_t* cmd)
{
	if((GlobalStatus & GSM_COMPLETE) == 0)
	{
		//gsmSends(cmd,NULL);
		if(!strncmp(cmd,"AT+QAUDLOOP=1,0\r\n",strlen("AT+QAUDLOOP=1,0\r\n")))
		{
		DEBUG("AUDIOLOOP TEST");
		}else if(!strncmp(cmd,"AT+QAUDLOOP=0,0\r\n",strlen("AT+QAUDLOOP=0,0\r\n")))
		{
		DEBUG("AUDIOLOOP TEST OVER");
		}
		
		if(gsmCmd1(cmd, 10000) == E_OK){
			if(!strncmp(cmd,(char*)GSM_CSQ,strlen((char*)GSM_CSQ)))
				testSignal = 0;
		}
		return;
	}
	
	if(!getTCPConState()){
		if(gsmCmd1(cmd, 10000) == E_OK){
			if(!strncmp(cmd,(char*)GSM_CSQ,strlen((char*)GSM_CSQ))){
				testSignal = 0;
				int8_t *item;
				int8_t *itemEnd;
				item = TokenBefore(gsmToken(NULL), "OK");
					if (item != NULL)
					{
						if(strchr((char*)item,':')!= NULL)
						{
								item = (int8_t*)strchr((char *)item,':');
							  item +=1;
								itemEnd = (int8_t*)strchr((char *)item,',');
								memset(signal,0,4);
								strncpy((char *)signal,(char*)item,itemEnd-item);
								if(DebugEnable) DEBUG("signal1 %s",signal);
						}
					}
			}
		}
		return;
	}
	
	GsmTask_t * task = &GsmTask;
	memset(task->Content,0,strlen((char*)task->Content));
	memcpy(task->Content,cmd,strlen((char*)cmd));
	task->Command = GSM_TASK_CMD_AT;
	GsmAddTask(task);
}


void answerIncomingCall(void)
{
	atCommand(GSM_ATA);
}

void clccIncomingCall(void)
{
	atCommand(GSM_CLCC);
}


bool getNewSmsIncoming(void)
{
	return new_sms_incoming;
}
	
void gsmDTRLow(void)
{
	GPIO_InitTypeDef  GPIO_InitStruct;
	
	__HAL_RCC_GPIOB_CLK_ENABLE();
	
	GPIO_InitStruct.Pin       = GPIO_PIN_6;
  GPIO_InitStruct.Mode      = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull      = GPIO_NOPULL;
  GPIO_InitStruct.Speed     = GPIO_SPEED_FREQ_VERY_HIGH;
	
	HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);
	HAL_GPIO_WritePin(GPIOB, GPIO_PIN_6, GPIO_PIN_RESET);

}

void gsmRtsLow(void)
{
	GPIO_InitTypeDef  GPIO_InitStruct;
	
	__HAL_RCC_GPIOA_CLK_ENABLE();
	
	GPIO_InitStruct.Pin       = GPIO_PIN_15;
  GPIO_InitStruct.Mode      = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull      = GPIO_NOPULL;
  GPIO_InitStruct.Speed     = GPIO_SPEED_FREQ_VERY_HIGH;
	
	HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);
	HAL_GPIO_WritePin(GPIOA, GPIO_PIN_15, GPIO_PIN_RESET);
}



void gsmPowerOff(void)
{

	GPIO_InitTypeDef  GPIO_InitStruct;
	
	__HAL_RCC_GPIOA_CLK_ENABLE();
	
	GPIO_InitStruct.Pin       = GPIO_PIN_12;
  GPIO_InitStruct.Mode      = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull      = GPIO_NOPULL;
  GPIO_InitStruct.Speed     = GPIO_SPEED_FREQ_VERY_HIGH;
	
	HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);
	HAL_GPIO_WritePin(GPIOA, GPIO_PIN_12, GPIO_PIN_RESET);
	
	
	GPIO_InitStruct.Pin       = GPIO_PIN_11;
  GPIO_InitStruct.Mode      = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull      = GPIO_NOPULL;
  GPIO_InitStruct.Speed     = GPIO_SPEED_FREQ_VERY_HIGH;

  HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

	HAL_GPIO_WritePin(GPIOA, GPIO_PIN_11, GPIO_PIN_RESET);
	
#if 0	
	//PC4 TE202
	__HAL_RCC_GPIOC_CLK_ENABLE();
	GPIO_InitStruct.Pin       = GPIO_PIN_4;
  GPIO_InitStruct.Mode      = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull      = GPIO_NOPULL;
  GPIO_InitStruct.Speed     = GPIO_SPEED_FREQ_VERY_HIGH;
	
	HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);
	HAL_GPIO_WritePin(GPIOC, GPIO_PIN_4, GPIO_PIN_RESET);

	
	__HAL_RCC_GPIOC_CLK_ENABLE();
	GPIO_InitStruct.Pin       = GPIO_PIN_9;
  GPIO_InitStruct.Mode      = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull      = GPIO_NOPULL;
  GPIO_InitStruct.Speed     = GPIO_SPEED_FREQ_VERY_HIGH;
	
	HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);
	HAL_GPIO_WritePin(GPIOC, GPIO_PIN_9, GPIO_PIN_RESET);
#endif

	osDelay(500);

}

void gsmPowerOn(void)
{
	GPIO_InitTypeDef  GPIO_InitStruct;
	
#if 0	
	//PC4 TE202
	__HAL_RCC_GPIOC_CLK_ENABLE();
	GPIO_InitStruct.Pin       = GPIO_PIN_4;
  GPIO_InitStruct.Mode      = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull      = GPIO_NOPULL;
  GPIO_InitStruct.Speed     = GPIO_SPEED_FREQ_VERY_HIGH;
	
	HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);
	HAL_GPIO_WritePin(GPIOC, GPIO_PIN_4, GPIO_PIN_SET);
	
#endif
	
	__HAL_RCC_GPIOC_CLK_ENABLE();
	GPIO_InitStruct.Pin       = GPIO_PIN_9;
  GPIO_InitStruct.Mode      = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull      = GPIO_NOPULL;
  GPIO_InitStruct.Speed     = GPIO_SPEED_FREQ_VERY_HIGH;
	
	HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);
	HAL_GPIO_WritePin(GPIOC, GPIO_PIN_9, GPIO_PIN_SET);
	
	__HAL_RCC_GPIOB_CLK_ENABLE();
	
	//PB2
	GPIO_InitStruct.Pin      	= GPIO_PIN_2;
  GPIO_InitStruct.Mode      = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull      = GPIO_NOPULL;
  GPIO_InitStruct.Speed     = GPIO_SPEED_FREQ_VERY_HIGH;
	
	HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);
	HAL_GPIO_WritePin(GPIOB, GPIO_PIN_2, GPIO_PIN_SET);
	
	//PA12
	__HAL_RCC_GPIOA_CLK_ENABLE();
	
	GPIO_InitStruct.Pin       = GPIO_PIN_12;
  GPIO_InitStruct.Mode      = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull      = GPIO_NOPULL;
  GPIO_InitStruct.Speed     = GPIO_SPEED_FREQ_VERY_HIGH;
	
	HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);
	HAL_GPIO_WritePin(GPIOA, GPIO_PIN_12, GPIO_PIN_SET);
	
	//PA11
	GPIO_InitStruct.Pin       = GPIO_PIN_11;
  GPIO_InitStruct.Mode      = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull      = GPIO_NOPULL;
  GPIO_InitStruct.Speed     = GPIO_SPEED_FREQ_VERY_HIGH;

  HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

	HAL_GPIO_WritePin(GPIOA, GPIO_PIN_11, GPIO_PIN_RESET);
	osDelay(1000);
	HAL_GPIO_WritePin(GPIOA, GPIO_PIN_11, GPIO_PIN_SET);
	
}

void gsmDebugEnable(bool enable)
{
	DebugEnable = enable;
}

void gsmATDebugEnable(bool enable)
{
	atDebug = enable;
}

bool gsmATDebug(void)
{
	return atDebug;
}
