#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <math.h>
#include "eeprom.h"
#include "debug.h"

#define FLASH_USER_START_ADDR   DATA_EEPROM_BANK2_BASE   /* Start @ of user Flash area */
#define FLASH_USER_END_ADDR     DATA_EEPROM_BANK2_END   /* End @ of user Flash area */

uint32_t Address = 0;
__IO uint32_t data32 = 0 , MemoryProgramStatus = 0;

#define ABS_RETURN(x,y)               (((x) < (y)) ? (y) : (x))


bool EEPROM_Erase(void)
{
	uint32_t index = 0;
	
	HAL_FLASHEx_DATAEEPROM_Unlock();

	/* Erase the user Flash area
    (area defined by FLASH_USER_START_ADDR and FLASH_USER_END_ADDR) ***********/

	 if (HAL_FLASHEx_DATAEEPROM_Erase(FLASH_USER_START_ADDR) != HAL_OK)
  {
    /*
      Error occurred while page erase.
      User can add here some code to deal with this error.
      PAGEError will contain the faulty page and then to know the code error on this page,
      user can call function 'HAL_FLASH_GetError()'
    */
	 index = HAL_FLASH_GetError();
	 if(DebugEnable) DEBUG("EEPROM ERASE FAIL %x",index);
   return false;
  }
	return true;
}

bool EEPROM_WriteWords(uint8_t * buff,uint32_t length)
{
	uint32_t index = 0;

	HAL_FLASHEx_DATAEEPROM_Unlock();

  /* Program the user Flash area word by word
    (area defined by FLASH_USER_START_ADDR and FLASH_USER_END_ADDR) ***********/

  Address = FLASH_USER_START_ADDR;

  while (Address < FLASH_USER_END_ADDR && index <= length)
  {
		data32 =  buff[index];
		
		index++;
		//if(DebugEnable) DEBUG("write eeprom data32:%x",data32);
		
    if (HAL_FLASHEx_DATAEEPROM_Program(FLASH_TYPEPROGRAMDATA_WORD, Address, data32) == HAL_OK)
    {
      Address = Address + 4;
    }
   else
    {
      /* Error occurred while writing data in Flash memory.
         User can add here some code to deal with this error */
		 index = HAL_FLASH_GetError();
		 if(DebugEnable) DEBUG("EEPROM FLASH FAIL %x",index);
     return false;
    }
  }
	HAL_FLASHEx_DATAEEPROM_Lock();
#if 0	
	/* Check if the programmed data is OK 
      MemoryProgramStatus = 0: data programmed correctly
      MemoryProgramStatus != 0: number of words not programmed correctly ******/
  Address = FLASH_USER_START_ADDR;
  MemoryProgramStatus = 0x0;
	index = 0;
  
  while (Address < FLASH_USER_END_ADDR && index <= length)
  {
    data32 = *(__IO uint32_t*)Address;

    if (data32 != buff[index])
    {
      MemoryProgramStatus++;  
    }
		index++;
    Address = Address + 4;
  } 
	
  /*Check if there is an issue to program data*/
  if (MemoryProgramStatus == 0)
  {
    return true;
  }
  else
  {
		 if(DebugEnable) DEBUG("error flash %d",MemoryProgramStatus);
		return false;
  }
	#endif
	return true;
}

bool flash_eeprom(uint8_t * buff,uint32_t length)
{
	if(getMainActionDone())
	deInitWWDG();
	stopIWDG();
	taskENTER_CRITICAL();
	//__disable_irq();
	EEPROM_Erase();
	bool write_ok = EEPROM_WriteWords(buff,length);
	if(!getMainActionDone())
	__enable_irq();
	taskEXIT_CRITICAL();
	if(getMainActionDone()){
	initWWDG();
	startWWDG();
	}
	return write_ok;
}

uint32_t read_eeprom(uint8_t * buff,uint32_t length)
{
	uint32_t index = 0;
	
	Address = FLASH_USER_START_ADDR;

  while (Address < FLASH_USER_END_ADDR && index <= length)
  {
    data32= *(__IO uint32_t*)Address;
		*buff++  = data32;
    Address = Address + 4;
		index++;	
  }
	return index;
}

/* Public functions ---------------------------------------------------------*/
/**
  * @brief  This function writes a data buffer in flash (data are 32-bit aligned).
  * @note   After writing data buffer, the flash content is checked.
  * @param  destination: start address for target location
  * @param  p_source: pointer on buffer with data to write
  * @param  length: length of data buffer (unit is 32-bit word)
  * @retval uint32_t 0: Data successfully written to Flash memory
  *         1: Error occurred while writing data in Flash memory
  *         2: Written Data in flash memory is different from expected one
  */
uint32_t FLASH_If_Write(uint32_t destination, uint32_t *p_source, uint32_t length)
{
  uint32_t i = 0;

  /* Unlock the Flash to enable the flash control register access *************/
  HAL_FLASH_Unlock();

  for (i = 0; (i < length) && (destination <= (BOOT_FLASH_END_ADDRESS-4)); i++)
  {
    /* Device voltage range supposed to be [2.7V to 3.6V], the operation will
       be done by word */ 
    if (HAL_FLASH_Program(FLASH_TYPEPROGRAM_WORD, destination, *(uint32_t*)(p_source+i)) == HAL_OK)      
    {
     /* Check the written value */
      if (*(uint32_t*)destination != *(uint32_t*)(p_source+i))
      {
        /* Flash content doesn't match SRAM content */
        return(FLASHIF_WRITINGCTRL_ERROR);
      }
      /* Increment FLASH destination address */
      destination += 4;
    }
    else
    {
      /* Error occurred while writing data in Flash memory */
      return (FLASHIF_WRITING_ERROR);
    }
  }

  /* Lock the Flash to disable the flash control register access (recommended
     to protect the FLASH memory against possible unwanted operation) *********/
  HAL_FLASH_Lock();

  return (FLASHIF_OK);
}


/**
  * @brief  This function does an erase of all user flash area
  * @param  start: start of user flash area
  * @retval FLASHIF_OK : user flash area successfully erased
  *         FLASHIF_ERASEKO : error occurred
  */
uint32_t FLASH_If_Erase(uint32_t start)
{
  FLASH_EraseInitTypeDef desc;
  uint32_t result = FLASHIF_OK;

  HAL_FLASH_Unlock();

  desc.PageAddress = start;
  desc.TypeErase = FLASH_TYPEERASE_PAGES;

/* NOTE: Following implementation expects the IAP code address to be < Application address */  
  if (start < BOOT_FLASH_END_ADDRESS )
  {
    desc.NbPages = (BOOT_FLASH_END_ADDRESS - start) / FLASH_PAGE_SIZE;
    if (HAL_FLASHEx_Erase(&desc, &result) != HAL_OK)
    {
      result = FLASHIF_ERASEKO;
    }
  }

  if (result == FLASHIF_OK )
  {
    desc.PageAddress = ABS_RETURN(start, BOOT_FLASH_END_ADDRESS);
    desc.NbPages = (APPLICATION_ADDRESS - ABS_RETURN(start, BOOT_FLASH_END_ADDRESS)) / FLASH_PAGE_SIZE;
    if (HAL_FLASHEx_Erase(&desc, &result) != HAL_OK)
    {
      result = FLASHIF_ERASEKO;
    }
  }
  
  HAL_FLASH_Lock();

  return result;
}	