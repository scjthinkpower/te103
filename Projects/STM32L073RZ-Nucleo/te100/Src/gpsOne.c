#include <stdio.h>
#include <string.h>
#include <math.h>
#include <stdlib.h>

#include "hardware.h"
#include "gpsOne.h"
#include "utility.h"
#include "main.h"
#include "gsm.h"
#include "gps.h"
#include "eeprom.h"
#include "debug.h"
#include <string.h>
#include <nmea/tok.h>
#include "task.h"
#include "config_file.h"
#include "work.h"

extern RTC_HandleTypeDef RTCHandle;

const int8_t GG_FRCMD_PING[]		= "$FRCMD,%s,_Ping,,vers=%s";
const int8_t GG_FRCMD_INIT_MSG[]	= "$FRCMD,%s,_Init";

const int8_t GG_FRERR_AUTH[]		= DGG_FRERR_AUTH;
const int8_t GG_FRERR_NOTSUP[]	= DGG_FRERR_NOTSUP;
const int8_t GG_FRERR_NOTEXEC[]	= DGG_FRERR_NOTEXEC;

const int8_t GG_FRERR[]			= DGG_FRERR;
const int8_t GG_FRRET[]			= DGG_FRRET;
const int8_t GG_FRCMD[]			= DGG_FRCMD;
const int8_t GG_FRSES[]			= DGG_FRSES;

int8_t SET_GRPS_APN_NAME[32]="internet";
int8_t SET_GPRS_USERNAME[32]="internet";
int8_t SET_GPRS_PASSWORD[32]="internet";
//int8_t SET_HOST_ADDR[32]="47.89.17.248";
int8_t SET_HOST_ADDR[32]="58.96.182.170";
int8_t SET_HOST_PORT[8]="4700";
int8_t SET_HOST_DOMAIN[32]="gps.scjtracker.com";
int8_t szNumber[20];

uint8_t GGC_GprsSettings	(int8_t *, const GgCommand_t *, bool);
uint8_t GGC_StartTracking	(int8_t *, const GgCommand_t *, bool);
uint8_t GGC_StopTracking	(int8_t *, const GgCommand_t *, bool);
uint8_t GGC_PollPosition	(int8_t *, const GgCommand_t *, bool);
uint8_t GGC_DeviceReset		(int8_t *, const GgCommand_t *, bool);
uint8_t GGC_FwUpdate		(int8_t *, const GgCommand_t *, bool);
uint8_t GGC_InitSMS			(int8_t *, const GgCommand_t *, bool);
uint8_t GGC_ShutDown    (int8_t *, const GgCommand_t *, bool);
uint8_t GGC_TimeZone    (int8_t *, const GgCommand_t *, bool);
uint8_t GGC_Guard    		(int8_t *, const GgCommand_t *, bool);
uint8_t GGC_SOSSetting  (int8_t *, const GgCommand_t *, bool);
uint8_t GGC_AlarmSetting (int8_t *, const GgCommand_t *, bool);
uint8_t GGC_APNSetting (int8_t *, const GgCommand_t *, bool);
uint8_t GGC_ServerSetting (int8_t *, const GgCommand_t *, bool);
uint8_t GGC_Recover (int8_t *, const GgCommand_t *, bool);
uint8_t GGC_Reset (int8_t *, const GgCommand_t *, bool);
uint8_t GGC_SetInitialEnginer (int8_t *, const GgCommand_t *, bool);
uint8_t GGC_SetInitialMileage (int8_t *, const GgCommand_t *, bool);
uint8_t GGC_FuelCircuitControl (int8_t *, const GgCommand_t *, bool);
uint8_t GGC_WorkMode (int8_t *, const GgCommand_t *, bool);
uint8_t GGC_Sync (int8_t *, const GgCommand_t *, bool);
uint8_t GGC_OutPut(int8_t *, const GgCommand_t *, bool);
uint8_t GGC_SafeZone(int8_t *, const GgCommand_t *, bool);
uint8_t GGC_SimulateAcc(int8_t *, const GgCommand_t *, bool);
uint8_t GGC_Angle(int8_t *, const GgCommand_t *, bool);
uint8_t GGC_UssdTcpUdp(int8_t *, const GgCommand_t *, bool);
uint8_t GGC_KeepAlive(int8_t *, const GgCommand_t *, bool);
uint8_t GGC_GpsStatus(int8_t *, const GgCommand_t *, bool);
uint8_t GGC_Setup(int8_t *, const GgCommand_t *, bool);
uint8_t GGC_AckStatus(int8_t *, const GgCommand_t *, bool);
uint8_t GGC_Protocol(int8_t *, const GgCommand_t *, bool);
uint8_t GGC_SleepMode(int8_t *, const GgCommand_t *, bool);
uint8_t GGC_Agps(int8_t *, const GgCommand_t *, bool);
uint8_t GGC_Jamming(int8_t *, const GgCommand_t *, bool);
uint8_t GGC_SetupDate(int8_t *, const GgCommand_t *, bool);


uint8_t GGR_Command			(int8_t *, int buf_size, const int8_t *cmd);
uint8_t GGR_General			(int8_t *, int buf_size, bool sms, const GgCommand_t * cmd);
uint8_t GGR_GprsSettings	(int8_t *, int buf_size, bool sms, const GgCommand_t * cmd);
uint8_t GGR_FwUpdate		(int8_t *, int buf_size, bool sms, const GgCommand_t * cmd);
uint8_t GGR_PollPosition (int8_t *, int buf_size, bool sms, const GgCommand_t * cmd);
uint8_t GGR_Sync (int8_t *, int buf_size, bool sms, const GgCommand_t * cmd);
uint8_t GGR_Setup (int8_t *, int buf_size, bool sms, const GgCommand_t * cmd);

uint8_t GGR_SendMessage(int8_t *, const GgCommand_t *, bool);

GgCommandCounts GprsSetCounts;
GgCommandCounts StartTrackingCounts;
GgCommandCounts FwUpdateCounts;
GgCommandCounts InitSMSCounts;

const int8_t GG_GPRS_SETTINGS[]	= DGG_GPRS_SETTINGS;
const int8_t GG_START_TRACKING[]	= DGG_START_TRACKING;

const GgCommand_t GgCommandMap[] =
{
	//{ "_InitSMS",			&InitSMSCounts,			GGC_InitSMS,		NULL				},
	//{ GG_GPRS_SETTINGS,		&GprsSetCounts,			GGC_GprsSettings,	GGR_GprsSettings	},
	{ "_Update",			&FwUpdateCounts,		GGC_FwUpdate,		GGR_General		},
	//{ GG_START_TRACKING,	&StartTrackingCounts,	GGC_StartTracking,	GGR_General			},

	//{ "_StopTracking",		NULL,					GGC_StopTracking,	GGR_General			},
	{ "_PollPosition",		NULL,					GGC_PollPosition,	GGR_PollPosition				},
	//{ "_DeviceReset",		NULL,					GGC_DeviceReset	,	GGR_General			},
	{ "_ShutDown",		NULL,					GGC_ShutDown	,	GGR_General			},
	{ "_Timezone",		NULL,					GGC_TimeZone	,	GGR_General			},
	{ "_Guard",		NULL,					GGC_Guard	,	GGR_General			},
	{ "_SOSSetting",		NULL,					GGC_SOSSetting	,	GGR_General			},
	{ "_AlarmSetting",		NULL,					GGC_AlarmSetting	,	GGR_General			},
	{ "_APNSetting",		NULL,					GGC_APNSetting	,	GGR_General			},
	{ "_ServerSetting",		NULL,					GGC_ServerSetting	,	GGR_General			},
	{ "_Recover",		NULL,					GGC_Recover	,	GGR_General			},
	{ "_Reset",		NULL,					GGC_Reset	,	GGR_General			},
	{ "_SetInitialEnginer",		NULL,					GGC_SetInitialEnginer	,	GGR_General			},
	{ "_SetInitialMileage",		NULL,					GGC_SetInitialMileage	,	GGR_General			},
	{ "_FuelCircuitControl",		NULL,					GGC_FuelCircuitControl ,	GGR_General			},
	{ "_Workmode",		NULL,					GGC_WorkMode ,	GGR_General			},
	{ "_Sync",		NULL,					GGC_Sync ,	GGR_Sync			},
	{ "_SetOutput",		NULL,					GGC_OutPut ,	GGR_General			},
	{ "_SafeZone",		NULL,					GGC_SafeZone ,	GGR_General			},
	{ "_SimulateAcc",		NULL,					GGC_SimulateAcc ,	GGR_General			},
	{ "_Angle",		NULL,					GGC_Angle ,	GGR_General			},
	{ "_UssdTcpUdp",		NULL,					GGC_UssdTcpUdp ,	GGR_General			},
	{ "_KeepAlive",		NULL,					GGC_KeepAlive ,	GGR_General			},
	{ "_ReportGpsStatus",		NULL,					GGC_GpsStatus ,	GGR_General			},
	{ "_Setup",		NULL,					GGC_Setup ,	GGR_Setup			},
	{ "_AckStatus",		NULL,					GGC_AckStatus ,	GGR_General			},
	{ "_Protocol",		NULL,					GGC_Protocol ,	GGR_General			},
	{ "_SleepMode",		NULL,					GGC_SleepMode ,	GGR_General			},
	{ "_Agps",		NULL,					GGC_Agps ,	GGR_General			},
	{ "_Jamming",		NULL,					GGC_Jamming ,	GGR_General			},
	{ "_SetupDate",		NULL,					GGC_SetupDate ,	GGR_General			},
 
	{ NULL }
};

const GgCommand_t GgReplayMap[] =
{
	{ "_SendMessage",		NULL,					GGR_SendMessage,	NULL				},
	{ NULL }
};

int8_t GG_FwFileName[32];
int  GG_FwSmsIndex = 0;
int8_t resetIndex;
bool GNC_SET = false;
uint8_t ota[100];
int8_t* pSetupResponse;
int8_t reply[128];
int8_t cmdnumer[20];

int8_t getresetIndex(void)
{
	return resetIndex;
}

uint8_t* getOta(void)
{
	return ota;
}

void setAGPS_Ota(void)
{
	memset(ota,0,100);
	memcpy(ota,"gps.scjtracker.com:firmware:123456:/var/www/tracker_srv/file_srv/firmware:mgaoffline.ubx",
						strlen("gps.scjtracker.com:firmware:123456:/var/www/tracker_srv/file_srv/firmware:mgaoffline.ubx"));
}

void GG_ClearAllCounts(void)
{
	const GgCommand_t * cmd = GgCommandMap;
	while (cmd->Handler != NULL)
	{
		if (cmd->Counts != NULL)
		{
			cmd->Counts->Count = 0;
			cmd->Counts->DateTime = 0;
			cmd->Counts->Index = 0;
		}
		cmd++;
	}
}
uint8_t parse_eh_setting(int8_t* cmd);
uint8_t parse_ml_setting(int8_t* cmd);

/*******************************************************************************
* Function Name	:	GGC_InitSMS
* Description	:
*******************************************************************************/
uint8_t GGC_InitSMS(int8_t * cmd, const GgCommand_t * gg_cmd, bool exec)
{
	if (exec)
		GlobalStatus |= SMS_INIT_OK;
	return E_OK;
}

/*******************************************************************************
* Function Name	:	GGR_SendMessage
* Description	:
*******************************************************************************/
uint8_t GGR_SendMessage(int8_t * cmd, const GgCommand_t * gg_cmd, bool exec)
{
	return E_OK;
}

/*******************************************************************************
* Function Name	:	GGC_FwUpdate
* Description	:
*******************************************************************************/
uint8_t GGC_FwUpdate(int8_t * cmd, const GgCommand_t * gg_cmd, bool exec)
{
	#if 0
	int nsize = TokenSizeComma(cmd);
	if (nsize > 0 && nsize < sizeof(GG_FwFileName) - 1)
	{
		strncpy((char *)GG_FwFileName, (char *)cmd, nsize);
		GG_FwFileName[nsize] = 0;
		return E_OK;
	}
	#endif
	GG_FwSmsIndex = -1;
	GG_FwFileName[0]='U';
	memset(ota,0,100);
	if(strlen(cmd) > 10)
	memcpy(ota,cmd,strlen(cmd));
	else
	memcpy(ota,"gps.scjtracker.com:firmware:123456:/var/www/tracker_srv/file_srv/firmware:te101.bin",
						strlen("gps.scjtracker.com:firmware:123456:/var/www/tracker_srv/file_srv/firmware:te101.bin"));
	return E_OK;
	//return E_ERROR;
}


/*******************************************************************************
* Function Name	:	GGR_FwUpdate
* Description	:
*******************************************************************************/
uint8_t GGR_FwUpdate(int8_t* buffer, int buf_size, bool sms, const GgCommand_t * cmd)
{
	if (sms)
	{
		if (buffer == NULL && buf_size != 0)
			GG_FwSmsIndex = buf_size;
		return E_ERROR;
	}
	return GGR_Command(buffer, buf_size, cmd->Command);
}

/*******************************************************************************
* Function Name	:	GGC_GprsSettings
* Description	:
*******************************************************************************/
uint8_t GGC_GprsSettings(int8_t * cmd, const GgCommand_t * gg_cmd, bool exec)
{
	int size;
	uint8_t state = 0;
	// apn,username,password,hostname,port
	while (cmd != NULL && state < 99)
	{
		size = TokenSizeComma(cmd);
		switch (state)
		{
			case 0:
				if (size > 0 && size < (sizeof(SET_GRPS_APN_NAME) - 1))
				{
					if (exec)
					{
						strncpy((char *)SET_GRPS_APN_NAME, (char *)cmd, size);
						SET_GRPS_APN_NAME[size] = 0;
					}
				}
				else
					state = 99;
				break;
			case 1:
				if (size > 0 && size < (sizeof(SET_GPRS_USERNAME) - 1))
				{
					if (exec)
					{
						strncpy((char *)SET_GPRS_USERNAME, (char *)cmd, size);
						SET_GPRS_USERNAME[size] = 0;
					}
				}
				else
					SET_GPRS_USERNAME[0] = 0;
				break;
			case 2:
				if (size > 0 && size < (sizeof(SET_GPRS_PASSWORD) - 1))
				{
					if (exec)
					{
						strncpy((char *)SET_GPRS_PASSWORD, (char *)cmd, size);
						SET_GPRS_PASSWORD[size] = 0;
					}
				}
				else
					SET_GPRS_PASSWORD[0] = 0;
				break;
			case 3:
				if (size > 0 && size < (sizeof(SET_HOST_ADDR) - 1))
				{
					if (exec)
					{
						strncpy((char *)SET_HOST_ADDR, (char *)cmd, size);
						SET_HOST_ADDR[size] = 0;
					}
				}
				else
					state = 99;
				break;
			case 4:
				if (size > 0 && size < (sizeof(SET_HOST_PORT) - 1))
				{
					if (exec)
					{
						strncpy((char *)SET_HOST_PORT, (char *)cmd, size);
						SET_HOST_PORT[size] = 0;
					}
					return E_OK;
				}
				else
					state = 99;
				break;
		}
		cmd = TokenNextComma(cmd);
		state++;
	}
	return E_ERROR;
}

/*******************************************************************************
* Function Name	:	GGC_GprsSettings
* Description	:	Send message to host
*******************************************************************************/
uint8_t GGR_Command(int8_t* buffer, int buf_size, const int8_t *cmd)
{
	if (buffer != NULL && buf_size != 0)
	{
		NmeaAddChecksum( strcpyEx( strcpyEx( strcpyEx( strcpyEx( buffer,
			DGG_FRRET),
			GsmIMEI()),
			","),
			cmd), buffer
		);
		return E_OK;
	}
	return E_ERROR;
}

/*******************************************************************************
* Function Name	:	GGC_GprsSettings
* Description	:	Send message to host
*******************************************************************************/
uint8_t GGR_General(int8_t* buffer, int buf_size, bool sms, const GgCommand_t * cmd)
{
	if (sms)
		return E_ERROR;
	return GGR_Command(buffer, buf_size, cmd->Command);
}

/*******************************************************************************
* Function Name	:	GGC_GprsSettings
* Description	:	Send message to host
*******************************************************************************/
uint8_t GGR_GprsSettings(int8_t* buffer, int buf_size, bool sms, const GgCommand_t * cmd)
{
	if (!sms && buffer != NULL && buf_size != 0)
	{
		NmeaAddChecksum( strcpyEx( strcpyEx( strcpyEx( strcpyEx( strcpyEx( buffer,
			DGG_FRRET),
			GsmIMEI()),
			","),
			GG_GPRS_SETTINGS),
			",,GpsGate TrackerOne,FWTracker,"),
			buffer
		);
		return E_OK;
	}	
	return E_ERROR;
}

/*******************************************************************************
* Function Name	:	GGC_StartTracking
* Description	:
*******************************************************************************/
uint8_t GGC_StartTracking(int8_t* cmd, const GgCommand_t * ggCommand, bool exec)
{
	int8_t *token = cmd;
	int value;
	while (token != NULL)
	{
		if (TokenSizeComma(token) >= (sizeof("TimeFilter=") - 1 + 2))
		{
			if (strncmp((char *)token, "TimeFilter=", sizeof("TimeFilter=") - 1) == 0)
			{
				value = nmea_atoi((char *)token + sizeof("TimeFilter=") - 1, 5, 10);
				if (value >= 15 && value <= (60 * 60))
				{
					if (exec && GpsContext.SendInterval != value)
						GpsContext.SendInterval = value;
					return E_OK;
				}
				return E_ERROR;
			}
		}
		token = TokenNextComma(token);
	}
	return E_ERROR;
}

/*******************************************************************************
* Function Name	:	GGC_StopTracking
* Description	:
*******************************************************************************/
uint8_t GGC_StopTracking(int8_t* cmd, const GgCommand_t * ggCommand, bool exec)
{
	return E_OK;
}
/*******************************************************************************
* Function Name	:	GGC_PollPosition
* Description	:
*******************************************************************************/
uint8_t GGC_PollPosition(int8_t* cmd, const GgCommand_t * ggCommand, bool exec)
{
	return E_OK;
}

/*******************************************************************************
* Function Name	:	GGR_PollPosition
* Description	:
*******************************************************************************/
uint8_t GGR_PollPosition(int8_t* buffer, int buf_size, bool sms, const GgCommand_t * cmd)
{
	int8_t* ptr;
	if (!sms && buffer != NULL && buf_size != 0)
	{
		ptr =  strcpyEx(strcpyEx(strcpyEx( strcpyEx( strcpyEx( strcpyEx( buffer,
			DGG_FRRET),
			GsmIMEI()),
		  ","),
			cmd->Command),
			", "),
			",");
		
		if(!strcmp((char *)get_conf_elem(GPS_MOTION_THRESHOLD_CONF),"1") && GpsNoData()){
		frame_cell(ptr,GSM_TASK_CMD_POLLPOSITION);
		return E_OK;
		}
		else{
		if(frame(ptr,GSM_TASK_CMD_POLLPOSITION,false) != 0){
			return E_OK;
		}
	}
			
	}
	return E_ERROR;
}

/*******************************************************************************
* Function Name	:	GGR_Setup
* Description	:
*******************************************************************************/
uint8_t GGR_Setup(int8_t* buffer, int buf_size, bool sms, const GgCommand_t * cmd)
{	
	if (sms)
		return E_ERROR;
	
		NmeaAddChecksum(strcpyEx(strcpyEx(strcpyEx(strcpyEx( strcpyEx( strcpyEx( strcpyEx( buffer,
			DGG_FRRET),
			GsmIMEI()),
		  ","),
			cmd->Command),
			","),cmdnumer),pSetupResponse),buffer);
	return E_OK;
}

/*******************************************************************************
* Function Name	:	GGC_DeviceReset
* Description	:
*******************************************************************************/
uint8_t GGC_DeviceReset(int8_t* cmd, const GgCommand_t * ggCommand, bool exec)
{
	return E_OK;
}


/*******************************************************************************
* Function Name	:	GGC_ShutDown
* Description	:
*******************************************************************************/
uint8_t GGC_ShutDown(int8_t* cmd, const GgCommand_t * ggCommand, bool exec)
{
	return E_OK;
}

/*******************************************************************************
* Function Name	:	GGC_TimeZone
* Description	:
*******************************************************************************/
uint8_t GGC_TimeZone(int8_t* cmd, const GgCommand_t * ggCommand, bool exec)
{
	if(strcmp((char *)get_conf_elem(TIMEZONE_CONF),cmd))
	update_conf_elem(TIMEZONE_CONF,(uint8_t*)cmd);
	return E_OK;
}


/*******************************************************************************
* Function Name	:	GGC_Guard
* Description	:
*******************************************************************************/
uint8_t GGC_Guard(int8_t* cmd, const GgCommand_t * ggCommand, bool exec)
{
	if(GNC_SET){
		if(DebugEnable) DEBUG("GNC_SET IS DONE");
		return E_ERROR;
	}
	osDelay(100);
	GNC_SET = true;

	if(strcmp((char *)get_conf_elem(WARNCIRCLE_CONF),cmd))
	update_conf_elem(WARNCIRCLE_CONF,(uint8_t*)cmd);
	GNC_SET = false;
	signalEvent();
	return E_OK;
}

/*******************************************************************************
* Function Name	:	parse_sos_setting
* Description	:
*******************************************************************************/
void parse_sos_setting(int8_t* cmd)
{
	int8_t* p;
	int8_t* pEnd;
	int8_t number[20];
	
	if(DebugEnable) DEBUG("cmd:%s",cmd);
	
	p = (int8_t*)strchr((char*)(char*)cmd,',');
		if(p == NULL)
			return;
	if(p-cmd > 0 && p-cmd < 5){
	if(DebugEnable) DEBUG("sos is null");
	if(strcmp((char *)get_conf_elem(SOSTELENUMBER_CONF),"null"))
	update_conf_elem_map(SOSTELENUMBER_CONF,"null");
	}else if(p-cmd > 0 && p-cmd <20){
	memcpy(number,cmd+1,p-cmd-1);
	number[p-cmd-1] = 0;
	if(DebugEnable) DEBUG("sos:%s",number);
	if(strcmp((char *)get_conf_elem(SOSTELENUMBER_CONF),number))
	update_conf_elem_map(SOSTELENUMBER_CONF,(uint8_t*)number);
	}else
	{
	if(DebugEnable) DEBUG("sos is null");
	if(strcmp((char *)get_conf_elem(SOSTELENUMBER_CONF),"null"))
	update_conf_elem_map(SOSTELENUMBER_CONF,"null");
	}
	
	p = (int8_t*)strchr((char*)(char *)p,',');
	if(p == NULL)
			return;
	p+=1;
	pEnd = (int8_t*)strchr((char*)(char *)p,',');
	if(pEnd == NULL)
			return;
	if(pEnd-p > 0 && pEnd-p < 5 ){
	if(DebugEnable) DEBUG("sos1 is null");
	if(strcmp((char *)get_conf_elem(SOS1TELENUMBER_CONF),"null"))
	update_conf_elem_map(SOS1TELENUMBER_CONF,"null");
	}else if(pEnd-p > 0 && pEnd-p < 20 ){
	memcpy(number,p+1,pEnd-p-1);
	number[pEnd-p-1] = 0;
	if(DebugEnable) DEBUG("sos1:%s",number);
	if(strcmp((char *)get_conf_elem(SOS1TELENUMBER_CONF),number))
	update_conf_elem_map(SOS1TELENUMBER_CONF,(uint8_t*)number);
	}else
	{
	if(DebugEnable) DEBUG("sos1 is null");
	if(strcmp((char *)get_conf_elem(SOS1TELENUMBER_CONF),"null"))
	update_conf_elem_map(SOS1TELENUMBER_CONF,"null");
	}
	
	p = (int8_t*)strchr((char*)p,',');
	if(p == NULL)
			return;
	p+=1;
	pEnd = (int8_t*)strchr((char*)p,',');
	if(pEnd == NULL)
			return;
	if(pEnd-p > 0 && pEnd-p < 5 ){
	if(DebugEnable) DEBUG("sos2:%s",number);
	if(strcmp((char *)get_conf_elem(SOS2TELENUMBER_CONF),"null"))
	update_conf_elem_map(SOS2TELENUMBER_CONF,(uint8_t*)number);
	}else if(pEnd-p > 0 && pEnd-p < 20 ){
	memcpy(number,p+1,pEnd-p-1);
	number[pEnd-p-1] = 0;
	if(DebugEnable) DEBUG("sos2:%s",number);
	if(strcmp((char *)get_conf_elem(SOS2TELENUMBER_CONF),number))
	update_conf_elem_map(SOS2TELENUMBER_CONF,(uint8_t*)number);
	}
	else
	{
	if(DebugEnable) DEBUG("sos2 is null");
	if(strcmp((char *)get_conf_elem(SOS2TELENUMBER_CONF),"null"))
	update_conf_elem_map(SOS2TELENUMBER_CONF,"null");
	}
	
	p = (int8_t*)strchr((char*)p,',');
	if(p == NULL)
			return;
	p+=1;
	if(cmd+strlen((char*)(char*)cmd)+1-p-1 > 0 && cmd+strlen((char*)(char*)cmd)+1-p-1 <5){
	if(DebugEnable) DEBUG("sos3 is null");
	if(strcmp((char *)get_conf_elem(SOS3TELENUMBER_CONF),"null"))
	update_conf_elem_map(SOS3TELENUMBER_CONF,"null");
	}
	else if(cmd+strlen((char*)(char*)cmd)+1-p-1 > 0 && cmd+strlen((char*)(char*)cmd)+1-p-1 <20){
	memcpy(number,p+1,cmd+strlen((char*)(char*)cmd)+1-p-1);
	number[cmd+strlen((char*)(char*)cmd)+1-p-1] = 0;
	if(DebugEnable) DEBUG("sos3:%s",number);
	if(strcmp((char *)get_conf_elem(SOS3TELENUMBER_CONF),number))
	update_conf_elem_map(SOS3TELENUMBER_CONF,(uint8_t*)number);
	}
	else
	{
	if(DebugEnable) DEBUG("sos3 is null");
	if(strcmp((char *)get_conf_elem(SOS3TELENUMBER_CONF),"null"))
	update_conf_elem_map(SOS3TELENUMBER_CONF,"null");
	}
	update_conf_elem_rom();
}

/*******************************************************************************
* Function Name	:	parse_sms_sos_setting
* Description	:
*******************************************************************************/
uint8_t parse_sms_sos_setting(int8_t* cmd)
{
	int8_t* p;
	int8_t* pEnd;
	int8_t number[20];
	
	if(DebugEnable) DEBUG("cmd:%s",cmd);
		
	p = cmd;
	p = (int8_t*)strchr((char*)p,':');
	if(p == NULL){
		return E_ERROR;
	}
   p+=1;
	if(strchr((char*)p,',') != NULL){
	pEnd= (int8_t*)strchr((char*)p,',');
	if(pEnd-p <15){
	memcpy(number,p,(pEnd-p));
	number[pEnd-p] = 0;
	if(DebugEnable)
	DEBUG("sos %s",number);
	if(strcmp((char *)get_conf_elem(SOSTELENUMBER_CONF),number))
	update_conf_elem(SOSTELENUMBER_CONF,(uint8_t*)number);
	}

	if(strchr((char*)p,':') != NULL){
	p = (int8_t*)strchr((char*)p,':');
	p+=1;
	if(strchr((char*)p,',') != NULL){
	pEnd= (int8_t*)strchr((char*)p,',');
	if(pEnd-p <15){
	memcpy(number,p,(pEnd-p));
	number[pEnd-p] = 0;
	if(DebugEnable)
	DEBUG("sos1 %s",number);
	if(strcmp((char *)get_conf_elem(SOS1TELENUMBER_CONF),number))
	update_conf_elem(SOS1TELENUMBER_CONF,(uint8_t*)number);
	}
	}else{
		return E_OK;
	}

	if(strchr((char*)p,':') != NULL){
		p = (int8_t*)strchr((char*)p,':');
		p+=1;
		if(strchr((char*)p,',') != NULL){
			pEnd= (int8_t*)strchr((char*)p,',');
			if(pEnd-p <15){
			memcpy(number,p,(pEnd-p));
			number[pEnd-p] = 0;
			if(DebugEnable)
			DEBUG("sos2 %s",number);
			if(strcmp((char *)get_conf_elem(SOS2TELENUMBER_CONF),number))
			update_conf_elem(SOS2TELENUMBER_CONF,(uint8_t*)number);
			}
		}else{
			if(cmd+strlen((char*)(char*)cmd)+1-p <15){
			memcpy(number,p,cmd+strlen((char*)(char*)cmd)+1-p);
			number[cmd+strlen((char*)(char*)cmd)+1-p] = 0;
			if(DebugEnable)
			DEBUG("sos2 %s",number);
			if(strcmp((char *)get_conf_elem(SOS2TELENUMBER_CONF),number))
			update_conf_elem(SOS2TELENUMBER_CONF,(uint8_t*)number);
			}
			return E_OK;
		}
	}else{
		return E_OK;
	}
	
    
	if(strchr((char*)p,':') != NULL){
		p = (int8_t*)strchr((char*)p,':');
		p+=1;
		if(strchr((char*)p,':') == NULL){
		if(cmd+strlen((char*)(char*)cmd)+1-p <15){
		memcpy(number,p,cmd+strlen((char*)cmd)+1-p);
		number[cmd+strlen((char*)cmd)+1-p] = 0;
		if(DebugEnable)
		DEBUG("sos3 %s",number);
		if(strcmp((char *)get_conf_elem(SOS3TELENUMBER_CONF),number))
		update_conf_elem(SOS3TELENUMBER_CONF,(uint8_t*)number);
		}
		}
	}else{
		return E_OK;
	}
	
	}
	}
	else{
	if(cmd+strlen((char*)cmd)+1-p <15){
	memcpy(number,p,cmd+strlen((char*)cmd)+1-p);
	number[cmd+strlen((char*)cmd)+1-p] = 0;
	if(DebugEnable)
	DEBUG("sos %s",number);
	if(strcmp((char *)get_conf_elem(SOSTELENUMBER_CONF),number))
	update_conf_elem(SOSTELENUMBER_CONF,(uint8_t*)number);
	}
	}
	return E_OK;
}



/*******************************************************************************
* Function Name	:	GGC_SOSSetting
* Description	:
*******************************************************************************/
uint8_t GGC_SOSSetting(int8_t* cmd, const GgCommand_t * ggCommand, bool exec)
{
	parse_sos_setting(cmd);
	return E_OK;
}


/*******************************************************************************
* Function Name	:	GGC_AlarmSetting
* Description	:
*******************************************************************************/
uint8_t GGC_AlarmSetting(int8_t* cmd, const GgCommand_t * ggCommand, bool exec)
{
	int8_t type[2];
	int8_t status[2];
	int8_t value[20];
	int8_t value_1[10];
	int8_t* p;
	
	p = (int8_t*)strchr((char*)cmd,',');
	memcpy(type,cmd,p-cmd);
	type[1] = 0;
	if(DebugEnable)
	DEBUG("type:%s",type);
	p = (int8_t*)strchr((char*)p,',');
	p+=1;
	memcpy(status,p,1);
	status[1] = 0;
	if(DebugEnable)
	DEBUG("status:%s",status);
	
	if((int8_t*)strchr((char*)p,',') != NULL)
	{
	p = (int8_t*)strchr((char*)p,',');
	p+=1;
	if(cmd+strlen((char*)cmd)+1-p > 0 && cmd+strlen((char*)cmd)+1-p < 20)
	memcpy(value,p,cmd+strlen((char*)cmd)+1-p);
	value[cmd+strlen((char*)cmd)+1-p] = 0;
	if(DebugEnable)
	DEBUG("value:%s",value);
	}
	
	if(!strncmp((char*)type,"1",1)){
		if(!strcmp(status,"0")){
			if(strcmp((char *)get_conf_elem(WARNLOWBATT_CONF),status))
			update_conf_elem(WARNSHOCK_CONF,(uint8_t*)status);
		}
		else{
			if(strcmp((char *)get_conf_elem(WARNLOWBATT_CONF),status))
			update_conf_elem(WARNSHOCK_CONF,(uint8_t*)status);
		if(strcmp((char *)get_conf_elem(GSENSOR_SENSITIVITY_CONF),value))
		update_conf_elem(GSENSOR_SENSITIVITY_CONF,(uint8_t*)value);
		}
	}
	else if(!strncmp((char*)type,"2",1)){
		if(strcmp((char *)get_conf_elem(WARNLOWBATT_CONF),status))
		update_conf_elem(WARNLOWBATT_CONF,(uint8_t*)status);
	}
	else if(!strncmp((char*)type,"3",1)){
		if(strcmp((char *)get_conf_elem(WARNURGENT_CONF),status))
		update_conf_elem(WARNURGENT_CONF,(uint8_t*)status);
	}
	else if(!strncmp((char *)type,"5",1)){
		if(strcmp((char *)get_conf_elem(WARNOVERSPEED_CONF),status))
		update_conf_elem(WARNOVERSPEED_CONF,(uint8_t*)status);
		p = (int8_t*)strchr((char*)value,';');
		if(p == NULL)
		{
		if(strcmp((char *)get_conf_elem(SPEED_CONF),value))
		update_conf_elem(SPEED_CONF,(uint8_t*)value);
		}
		else{
		memset(value_1,0,10);
		memcpy(value_1,value,p-value);
		if(DebugEnable) DEBUG("value_1:%s",value_1);
		if(strcmp((char *)get_conf_elem(SPEED_CONF),value_1))
		update_conf_elem(SPEED_CONF,(uint8_t*)value_1);
		p+=1;
		memset(value_1,0,10);
		memcpy(value_1,p,value+strlen((char*)value)+1-p);
		if(DebugEnable) DEBUG("value_1:%s",value_1);
		if(strcmp((char *)get_conf_elem(OVERSPEED_TIME_CONF),value_1))
		update_conf_elem(OVERSPEED_TIME_CONF,(uint8_t*)value_1);
		}
	}
	
	return E_OK;
}



/*******************************************************************************
* Function Name	:	parse_apn_setting
* Description	:
*******************************************************************************/
uint8_t parse_apn_setting(int8_t* cmd)
{
	int8_t apn[30];
	int8_t* p;
	int8_t* pEnd;
	
	p = (int8_t*)strchr((char*)cmd,',');
	if(p == NULL && strlen((char*)cmd) == 0){
		return E_ERROR;
	}
	else if(p == NULL && strlen((char*)cmd) > 0 && strlen((char*)cmd) < 30)
	{
	memcpy(apn,cmd,strlen((char*)cmd));
	apn[strlen((char*)cmd)] = 0;
	if(DebugEnable)
	DEBUG("apn:%s",apn);
	if(strcmp((char *)get_conf_elem(APNNAME_CONF),apn))
	update_conf_elem(APNNAME_CONF,(uint8_t*)apn);
	return E_OK;
	}
	
	if(p-cmd > 0 && p-cmd <30){
	memcpy(apn,cmd,p-cmd);
	apn[p-cmd] = 0;
	if(DebugEnable)
	DEBUG("apn:%s",apn);
	if(strcmp((char *)get_conf_elem(APNNAME_CONF),apn))
	update_conf_elem(APNNAME_CONF,(uint8_t*)apn);
	}
	
	p+=1;
	pEnd = (int8_t*)strchr((char*)p,',');
	if(pEnd == NULL && cmd+strlen((char*)cmd)+1-p-1 ==0){
		return E_OK;
	}
	else if(pEnd == NULL && cmd+strlen((char*)cmd)+1-p-1 > 0 && cmd+strlen((char*)cmd)+1-p-1 < 30)
	{
	memcpy(apn,p,cmd+strlen((char*)cmd)+1-p-1);
	apn[cmd+strlen((char*)cmd)+1-p-1] = 0;
	if(DebugEnable)
	DEBUG("user:%s",apn);
	if(strcmp((char *)get_conf_elem(APNUSER_CONF),apn))
	update_conf_elem(APNUSER_CONF,(uint8_t*)apn);
	return E_OK;
	}
	if(pEnd-p > 0 && pEnd-p < 30){
	memcpy(apn,p,pEnd-p);
	apn[pEnd-p] = 0;
	if(DebugEnable)
	DEBUG("user:%s",apn);
	if(strcmp((char *)get_conf_elem(APNUSER_CONF),apn))
	update_conf_elem(APNUSER_CONF,(uint8_t*)apn);
	}
	
	p = (int8_t*)strchr((char*)p,',');
	if(p == NULL){
		return E_OK;
	}
	p+=1;
	if(cmd+strlen((char*)cmd)+1-p-1 > 0 && cmd+strlen((char*)cmd)+1-p-1 < 30){
	memcpy(apn,p,cmd+strlen((char*)cmd)+1-p);
	apn[cmd+strlen((char*)cmd)+1-p] = 0;
	if(DebugEnable)
	DEBUG("password:%s",apn);
	if(strcmp((char *)get_conf_elem(APNPASS_CONF),apn))
	update_conf_elem(APNPASS_CONF,(uint8_t*)apn);
	}
	
	return E_OK;
}

/*******************************************************************************
* Function Name	:	GGC_APNSetting
* Description	:
*******************************************************************************/
uint8_t GGC_APNSetting(int8_t* cmd, const GgCommand_t * ggCommand, bool exec)
{
	parse_apn_setting(cmd);
	return E_OK;
}


/*******************************************************************************
* Function Name	:	parse_server_setting
* Description	:
*******************************************************************************/
uint8_t parse_server_setting(int8_t* cmd)
{
	int8_t server[20];
	int8_t* p;
	DEBUG("parse_server_setting %s",cmd);
	p = (int8_t*)strchr((char*)cmd,',');
	if(p == NULL && strlen((char*)cmd) == 0){
		return E_ERROR;
	}
	if(p-cmd > 0 && p-cmd <20){
	memcpy(server,cmd,p-cmd);
	server[p-cmd] = 0;
	if(DebugEnable)
	DEBUG("ip:%s",server);
	if(strcmp((char *)get_conf_elem(MAINSERVERIPADDR_CONF),server))
	update_conf_elem(MAINSERVERIPADDR_CONF,(uint8_t*)server);
	}

	p = (int8_t*)strchr((char*)p,',');
	if(p == NULL){
		return E_OK;
	}
	p+=1;
	if(cmd+strlen((char*)cmd)+1-p-1 > 0 && cmd+strlen((char*)cmd)+1-p-1 < 10){
	memcpy(server,p,cmd+strlen((char*)cmd)+1-p);
	server[cmd+strlen((char*)cmd)+1-p] = 0;
	if(DebugEnable)
	DEBUG("port:%s",server);
	if(strcmp((char *)get_conf_elem(MAINSERVERPORTNUMBER_CONF),server))
	update_conf_elem(MAINSERVERPORTNUMBER_CONF,(uint8_t*)server);
	}
	
	return E_OK;
}


/*******************************************************************************
* Function Name	:	parse_server_setting
* Description	:
*******************************************************************************/
uint8_t parse_domain_setting(int8_t* cmd)
{
	int8_t server[32];
	int8_t* p;
	
	p = (int8_t*)strchr((char*)cmd,',');
	if(p == NULL && strlen((char*)cmd) == 0){
		return E_ERROR;
	}
	if(p-cmd > 0 && p-cmd <20){
	memcpy(server,cmd,p-cmd);
	server[p-cmd] = 0;
	if(DebugEnable)
	DEBUG("domain:%s",server);
	if(strcmp((char *)get_conf_elem(MAINSERVERDOMAINNAME_CONF),server))
	update_conf_elem(MAINSERVERDOMAINNAME_CONF,(uint8_t*)server);
	}

	p = (int8_t*)strchr((char*)p,',');
	if(p == NULL){
		return E_OK;
	}
	p+=1;
	if(cmd+strlen((char*)cmd)+1-p-1 > 0 && cmd+strlen((char*)cmd)+1-p-1 < 10){
	memcpy(server,p,cmd+strlen((char*)cmd)+1-p);
	server[cmd+strlen((char*)cmd)+1-p] = 0;
	if(DebugEnable)
	DEBUG("port:%s",server);
	if(strcmp((char *)get_conf_elem(MAINSERVERPORTNUMBER_CONF),server))
	update_conf_elem(MAINSERVERPORTNUMBER_CONF,(uint8_t*)server);
	}
	
	return E_OK;
}


/*******************************************************************************
* Function Name	:	parse_setupdate_setting
* Description	:
*******************************************************************************/
uint8_t parse_setupdate_setting(int8_t* cmd)
{
	int8_t timebuff[5];
	nmeaTIME time;
	int8_t* p;
	
	p = (int8_t*)strchr((char*)cmd,',');
	if(p == NULL && strlen((char*)cmd) == 0){
		return E_ERROR;
	}
	p = cmd;
	
	int8_t* pEnd = (int8_t*)strchr((char *)p,'/');
	memset(timebuff,0,5);
	strncpy((char *)timebuff,p,pEnd-p);

	time.year =atoi(timebuff);
	
		
	p = (int8_t*)strchr((char *)p,'/');
	p+=1;
	pEnd = (int8_t*)strchr((char *)p,'/');
	memset(timebuff,0,5);
	strncpy((char *)timebuff,p,pEnd-p);
		
	time.mon =atoi(timebuff);
	
	p = (int8_t*)strchr((char *)p,'/');
	p+=1;
	pEnd = (int8_t*)strchr((char *)p,',');
	memset(timebuff,0,5);
	strncpy((char *)timebuff,p,pEnd-p);
		
	time.day = atoi(timebuff);
		
	

	p = (int8_t*)strchr((char*)p,',');
	if(p == NULL){
		return E_OK;
	}
	p+=1;
	if(cmd+strlen((char*)cmd)+1-p-1 > 0 && cmd+strlen((char*)cmd)+1-p-1 < 20){
		
		int8_t* pEnd = (int8_t*)strchr((char *)p,':');
		memset(timebuff,0,5);
		strncpy((char *)timebuff,p,pEnd-p);
		
		time.hour = atoi(timebuff);
		
		
		p = (int8_t*)strchr((char *)p,':');
		p+=1;
		pEnd = (int8_t*)strchr((char *)p,':');
		memset(timebuff,0,5);
		strncpy((char *)timebuff,p,pEnd-p);
		
		time.min = atoi(timebuff);
		
		
		p = (int8_t*)strchr((char *)p,':');
		p+=1;
		memset(timebuff,0,5);
		strncpy((char *)timebuff,p,cmd+strlen((char*)cmd)-p);
		
		time.sec = atoi(timebuff);
		
		time.year +=2000;
		time.mon = time.mon -1;
		RTC_TIME(time,false);
	
	}
	
	return E_OK;
}


/*******************************************************************************
* Function Name	:	GGC_ServerSetting
* Description	:
*******************************************************************************/
uint8_t GGC_ServerSetting(int8_t* cmd, const GgCommand_t * ggCommand, bool exec)
{
	parse_server_setting(cmd);
	return E_OK;
}


/*******************************************************************************
* Function Name	:	parse_recover_setting
* Description	:
*******************************************************************************/
void parse_recover_setting(int8_t* cmd)
{
	
	delete_sw_conf_noReset();
	write_eh_data(0);
	write_ml_data(0);
	write_out1_status(0);
	write_out2_status(0);
	bit_map_erase_flag_all_sector();
	bit_map_erase_flag_start();
}


/*******************************************************************************
* Function Name	:	GGC_Recover
* Description	:
*******************************************************************************/
uint8_t GGC_Recover(int8_t* cmd, const GgCommand_t * ggCommand, bool exec)
{
	parse_recover_setting(cmd);
	return E_OK;
}

/*******************************************************************************
* Function Name	:	parse_reset_setting
* Description	:
*******************************************************************************/
uint8_t parse_reset_setting(int8_t* cmd)
{
	return E_OK;
}

/*******************************************************************************
* Function Name	:	GGC_Reset
* Description	:
*******************************************************************************/
uint8_t GGC_Reset(int8_t* cmd, const GgCommand_t * ggCommand, bool exec)
{
	parse_reset_setting(cmd);
	return E_OK;
}


/*******************************************************************************
* Function Name	:	parse_InitialEnginer_setting
* Description	:
*******************************************************************************/
void parse_InitialEnginer_setting(int8_t* cmd)
{	
	setGc_enginHour(cmd);
	//initEH(cmd);
}

/*******************************************************************************
* Function Name	:	GGC_SetInitialEnginer
* Description	:
*******************************************************************************/
uint8_t GGC_SetInitialEnginer(int8_t* cmd, const GgCommand_t * ggCommand, bool exec)
{
	parse_InitialEnginer_setting(cmd);
	return E_OK;
}


/*******************************************************************************
* Function Name	:	parse_InitialMileage_setting
* Description	:
*******************************************************************************/
void parse_InitialMileage_setting(int8_t* cmd)
{
	setGc_mileage(cmd);
	//initML(cmd);
}

/*******************************************************************************
* Function Name	:	GGC_SetInitialMileage
* Description	:
*******************************************************************************/
uint8_t GGC_SetInitialMileage(int8_t* cmd, const GgCommand_t * ggCommand, bool exec)
{
	parse_InitialMileage_setting(cmd);
	return E_OK;
}



/*******************************************************************************
* Function Name	:	parse_FuelCircuitControl_setting
* Description	:
*******************************************************************************/
void parse_FuelCircuitControl_setting(int8_t* cmd)
{
	if(DebugEnable) DEBUG("FuelCircuitControl %s",cmd);
	
	if(strncmp((char *)get_conf_elem(WARNCIRCLE_CONF),"1",1)){
			signalEvent();
			return;
	}
	
	GPIO_InitTypeDef  GPIO_InitStruct;
	//PA5
	/*
	__HAL_RCC_GPIOA_CLK_ENABLE();
	GPIO_InitStruct.Pin       = GPIO_PIN_5;
  GPIO_InitStruct.Mode      = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull      = GPIO_PULLUP;
  GPIO_InitStruct.Speed     = GPIO_SPEED_FREQ_VERY_HIGH;
	
	HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);
	*/
	if(!strncmp((char*)cmd, "1", 1)){
	//HAL_GPIO_WritePin(GPIOA, GPIO_PIN_5, GPIO_PIN_SET);
		start_pwm_out1();
	}
	else{
	//HAL_GPIO_WritePin(GPIOA, GPIO_PIN_5, GPIO_PIN_RESET);
		stop_pwm_out1();
	}
	
	//PC8
	/*
	__HAL_RCC_GPIOC_CLK_ENABLE();
	GPIO_InitStruct.Pin       = GPIO_PIN_8;
  GPIO_InitStruct.Mode      = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull      = GPIO_PULLUP;
  GPIO_InitStruct.Speed     = GPIO_SPEED_FREQ_VERY_HIGH;
	
	HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);
	*/
	if(!strncmp((char*)cmd, "1", 1)){
	//HAL_GPIO_WritePin(GPIOC, GPIO_PIN_8, GPIO_PIN_SET);
		start_pwm_out2();
	}
	else{
	//HAL_GPIO_WritePin(GPIOC, GPIO_PIN_8, GPIO_PIN_RESET);
		stop_pwm_out2();
	}		
	signalEvent();
}

/*******************************************************************************
* Function Name	:	GGC_FuelCircuitControl
* Description	:
*******************************************************************************/
uint8_t GGC_FuelCircuitControl(int8_t* cmd, const GgCommand_t * ggCommand, bool exec)
{
	parse_FuelCircuitControl_setting(cmd);
	return E_OK;
}



/*******************************************************************************
* Function Name	:	parse_WorkMode_setting
* Description	:
*******************************************************************************/
uint8_t parse_WorkMode_setting(int8_t* cmd)
{
	int8_t* p;
	int8_t* pEnd;
	uint8_t mode[10];
	
	if(DebugEnable) DEBUG("parse_WorkMode_setting %s",cmd);
		
	p = (int8_t*)strchr((char*)cmd,',');
	if(p == NULL){
		return E_ERROR;
	}
	p+=1;

	if(!strncmp(cmd,"1",1)){
		if(strcmp((char *)get_conf_elem(GSMSWITCH_CONF),"1"))
		update_conf_elem(GSMSWITCH_CONF,"1");
		
	pEnd = (int8_t*)strchr((char*)cmd,';');
		if(pEnd == NULL){
			return E_ERROR;
		}
		
	 if(pEnd-p > 0 && pEnd-p <10 ){
		 memcpy(mode,p,pEnd-p);
		 mode[pEnd-p] = 0;
		 if(DebugEnable) DEBUG("ontime:%s",mode);
		 if(strcmp((char *)get_conf_elem(REPINTERVAL_CONF),mode))
		 {
		 //if(atoi(mode) >= 10){
		 update_conf_elem(REPINTERVAL_CONF,(uint8_t*)mode);
		 osDelay(500);
		 setGpsSendTimer(ACC_EVENT_ON);
		 //}
		 }
	 }
		
	 p = (int8_t*)strchr(p,';');
	 p+=1;
	 if(cmd+strlen((char*)cmd)+1-p-1 > 0 && cmd+strlen((char*)cmd)+1-p-1 <10){
		  memcpy(mode,p,cmd+strlen((char*)cmd)+1-p);
		 if(DebugEnable) DEBUG("offtime:%s",mode);
		  mode[cmd+strlen((char*)cmd)+1-p] = 0;
		 if(strcmp((char *)get_conf_elem(ACCOFFREPINTERVAL_CONF),mode)){
		 if(atoi(mode) >= 60){
		 update_conf_elem(ACCOFFREPINTERVAL_CONF,(uint8_t*)mode);
		 osDelay(500);
		 setGpsSendTimer(ACC_EVENT_OFF);
		 }
		 }
	 }
	}if(!strncmp(cmd,"2",1)){
		
		if(strcmp((char *)get_conf_elem(GSMSWITCH_CONF),"2"))
		update_conf_elem(GSMSWITCH_CONF,"2");
		
		 if(cmd+strlen((char*)cmd)+1-p-1 > 0 && cmd+strlen((char*)cmd)+1-p-1 <10){
		  memcpy(mode,p,cmd+strlen((char*)cmd)+1-p);
		 if(DebugEnable) DEBUG("distance:%s",mode);
		  mode[cmd+strlen((char*)cmd)+1-p] = 0;
		 if(strcmp((char *)get_conf_elem(DISTANCE_CONF),mode)){
		 update_conf_elem(DISTANCE_CONF,(uint8_t*)mode);
		 }
	 }
		
	}if(!strncmp(cmd,"3",1) || !strncmp(cmd,"4",1)){
		if(!strncmp(cmd,"3",1) && strcmp((char *)get_conf_elem(GSMSWITCH_CONF),"3"))
			update_conf_elem(GSMSWITCH_CONF,"3");
		else if(!strncmp(cmd,"4",1) && strcmp((char *)get_conf_elem(GSMSWITCH_CONF),"4"))
			update_conf_elem(GSMSWITCH_CONF,"4");
		
		pEnd = (int8_t*)strchr((char*)cmd,';');
		if(pEnd == NULL){
			return E_ERROR;
		}
		
	 if(pEnd-p > 0 && pEnd-p <10 ){
		 memcpy(mode,p,pEnd-p);
		 mode[pEnd-p] = 0;
		 if(DebugEnable) DEBUG("ontime:%s",mode);
		 if(strcmp((char *)get_conf_elem(REPINTERVAL_CONF),mode))
		 {
		 //if(atoi(mode) >= 10){
		 update_conf_elem(REPINTERVAL_CONF,(uint8_t*)mode);
		 osDelay(500);
		 setGpsSendTimer(ACC_EVENT_ON);
		 //}
		 }
	 }
		
	 p = (int8_t*)strchr(p,';');
	 p+=1;
	 pEnd = (int8_t*)strchr((char*)p,';');
		if(pEnd == NULL){
			return E_ERROR;
		}
		
		if(pEnd-p > 0 && pEnd-p <10 ){
		 memcpy(mode,p,pEnd-p);
		 mode[pEnd-p] = 0;
		 if(DebugEnable) DEBUG("offtime:%s",mode);
		 if(strcmp((char *)get_conf_elem(ACCOFFREPINTERVAL_CONF),mode))
		 {
		 if(atoi(mode) >= 60){
		 update_conf_elem(ACCOFFREPINTERVAL_CONF,(uint8_t*)mode);
		 osDelay(500);
		 setGpsSendTimer(ACC_EVENT_OFF);
		 }
		 }
	 }
	 
	 p = (int8_t*)strchr(p,';');
	 p+=1;
	 if(cmd+strlen((char*)cmd)+1-p-1 > 0 && cmd+strlen((char*)cmd)+1-p-1 <10){
		  memcpy(mode,p,cmd+strlen((char*)cmd)+1-p);
		 if(DebugEnable) DEBUG("distance:%s",mode);
		  mode[cmd+strlen((char*)cmd)+1-p] = 0;
		 if(strcmp((char *)get_conf_elem(DISTANCE_CONF),mode)){
		 update_conf_elem(DISTANCE_CONF,(uint8_t*)mode);
		 }
	 }
		
	}else
	return E_ERROR;
	
	return E_OK;
}



/*******************************************************************************
* Function Name	:	GGC_WorkMode
* Description	:
*******************************************************************************/
uint8_t GGC_WorkMode(int8_t* cmd, const GgCommand_t * ggCommand, bool exec)
{
	parse_WorkMode_setting(cmd);
	return E_OK;
}


/*******************************************************************************
* Function Name	:	GGC_Sync
* Description	:
*******************************************************************************/
uint8_t GGC_Sync(int8_t* cmd, const GgCommand_t * ggCommand, bool exec)
{
	return E_OK;
}



/*******************************************************************************
* Function Name	:	GGR_PollPosition
* Description	:
*******************************************************************************/
uint8_t GGR_Sync(int8_t* buffer, int buf_size, bool sms, const GgCommand_t * cmd)
{
	char str[10];
	int8_t* ptr;
	
	if (!sms && buffer != NULL && buf_size != 0)
	{
			ptr =  strcpyEx(strcpyEx(strcpyEx( strcpyEx( strcpyEx( strcpyEx( buffer,
			DGG_FRRET),
			GsmIMEI()),
		  ","),
			cmd->Command),
			", "),
			",");
		
		ptr = strcpyEx(ptr,get_conf_elem(MAINSERVERIPADDR_CONF));
		ptr = strcpyEx(ptr,",");
		ptr = strcpyEx(ptr,get_conf_elem(MAINSERVERPORTNUMBER_CONF));
		ptr = strcpyEx(ptr,",");
		ptr = strcpyEx(ptr,get_conf_elem(SOSTELENUMBER_CONF));
		ptr = strcpyEx(ptr,",");
		ptr = strcpyEx(ptr,get_conf_elem(SOS1TELENUMBER_CONF));
		ptr = strcpyEx(ptr,",");
		ptr = strcpyEx(ptr,get_conf_elem(SOS2TELENUMBER_CONF));
		ptr = strcpyEx(ptr,",");
		ptr = strcpyEx(ptr,get_conf_elem(SOS3TELENUMBER_CONF));
		ptr = strcpyEx(ptr,",");
		ptr = strcpyEx(ptr,get_conf_elem(APNNAME_CONF));
		ptr = strcpyEx(ptr,",");
		ptr = strcpyEx(ptr,get_conf_elem(APNUSER_CONF));
		ptr = strcpyEx(ptr,",");
		ptr = strcpyEx(ptr,get_conf_elem(APNPASS_CONF));
		ptr = strcpyEx(ptr,",");
		ptr = strcpyEx(ptr,get_conf_elem(GSMSWITCH_CONF));
		ptr = strcpyEx(ptr,",");
		if(!strncmp(get_conf_elem(GSMSWITCH_CONF),"1",1)){
		ptr = strcpyEx(ptr,get_conf_elem(REPINTERVAL_CONF));
		ptr = strcpyEx(ptr,";");
		ptr = strcpyEx(ptr,get_conf_elem(ACCOFFREPINTERVAL_CONF));
		}else if(!strncmp(get_conf_elem(GSMSWITCH_CONF),"2",1))
		{
		ptr = strcpyEx(ptr,get_conf_elem(DISTANCE_CONF));
		}else if(!strncmp(get_conf_elem(GSMSWITCH_CONF),"3",1)||
		!strncmp(get_conf_elem(GSMSWITCH_CONF),"4",1))
		{
		ptr = strcpyEx(ptr,get_conf_elem(REPINTERVAL_CONF));
		ptr = strcpyEx(ptr,";");
		ptr = strcpyEx(ptr,get_conf_elem(ACCOFFREPINTERVAL_CONF));
		ptr = strcpyEx(ptr,";");
		ptr = strcpyEx(ptr,get_conf_elem(DISTANCE_CONF));
		}
		ptr = strcpyEx(ptr,",");
		ptr = strcpyEx(ptr,get_conf_elem(ACCSLEEP_CONF));
		ptr = strcpyEx(ptr,",");
		ptr = strcpyEx(ptr,get_conf_elem(ACCSTATUSHINT_CONF));
		ptr = strcpyEx(ptr,",");
		ptr = strcpyEx(ptr,get_conf_elem(WARNSHOCK_CONF));
		ptr = strcpyEx(ptr,",");
		ptr = strcpyEx(ptr,get_conf_elem(GSENSOR_SENSITIVITY_CONF));
		ptr = strcpyEx(ptr,",");
		ptr = strcpyEx(ptr,get_conf_elem(WARNEFAN_CONF));
		ptr = strcpyEx(ptr,",");
		ptr = strcpyEx(ptr,get_conf_elem(WARNLOWBATT_CONF));
		ptr = strcpyEx(ptr,",");
		ptr = strcpyEx(ptr,get_conf_elem(WARNOVERSPEED_CONF));
		ptr = strcpyEx(ptr,",");
		ptr = strcpyEx(ptr,get_conf_elem(SPEED_CONF));
		ptr = strcpyEx(ptr,";");
		ptr = strcpyEx(ptr,get_conf_elem(OVERSPEED_TIME_CONF));
		ptr = strcpyEx(ptr,",");
		ptr = strcpyEx(ptr,get_conf_elem(WARNURGENT_CONF));
		ptr = strcpyEx(ptr,",");
		ptr = strcpyEx(ptr,get_conf_elem(WARNCIRCLE_CONF));
		ptr = strcpyEx(ptr,",");
		ptr = strcpyEx(ptr,get_conf_elem(TIMEZONE_CONF));
		ptr = strcpyEx(ptr,",");
		ptr = strcpyEx(ptr,"0");
		ptr = strcpyEx(ptr,",");
		sprintf(str, "%d",getEnginHour()) ;
		ptr = strcpyEx(ptr,str);
		ptr = strcpyEx(ptr,",");
		memset(str,0,10);
		sprintf(str, "%d",getMileage()) ;
		ptr = strcpyEx(ptr,str);
		ptr = strcpyEx(ptr,",");
		ptr = strcpyEx(ptr,get_conf_elem(WORKINGMODE_CONF));
		ptr = strcpyEx(ptr,",");
		ptr = strcpyEx(ptr,get_conf_elem(ANGLE_CONF));
		ptr = strcpyEx(ptr,",");
		ptr = strcpyEx(ptr,get_conf_elem(WARNCIRCLE_CONF));
		ptr = strcpyEx(ptr,",");
		if(!strncmp(get_conf_elem(SIMULATE_ACC_CONF),"1",1))
		ptr = strcpyEx(ptr,"1");
		else
		ptr = strcpyEx(ptr,"0");
		ptr = strcpyEx(ptr,",");
		ptr = strcpyEx(ptr,get_conf_elem(SIMULATE_ACC_CONF)+2);
		ptr = strcpyEx(ptr,",");
		ptr = strcpyEx(ptr,get_conf_elem(ACCSLEEP_CONF));
		ptr = strcpyEx(ptr,",");
		ptr = strcpyEx(ptr,get_conf_elem(ACCSTATUSHINT_CONF));
		ptr = strcpyEx(ptr,",");
		ptr = strcpyEx(ptr,GsmICCID());
		ptr = strcpyEx(ptr,",");
		ptr = strcpyEx(ptr,get_conf_elem(RESERVE5_CONF));	
		ptr = strcpyEx(ptr,",");
		ptr = strcpyEx(ptr,get_conf_elem(RESERVE3_CONF));	
		ptr = strcpyEx(ptr,",");
		ptr = strcpyEx(ptr,get_conf_elem(RESERVE6_CONF));	
		ptr = strcpyEx(ptr,",");
		ptr = strcpyEx(ptr,get_conf_elem(RESERVE7_CONF));
		ptr = strcpyEx(ptr,",");
		ptr = strcpyEx(ptr,get_conf_elem(RESERVE8_CONF));
		NmeaAddChecksum(ptr,buffer);
		return E_OK;
	}
	return E_ERROR;
}

/*******************************************************************************
* Function Name	:	GGC_OutPut
* Description	:
*******************************************************************************/
uint8_t GGC_OutPut(int8_t* cmd, const GgCommand_t * ggCommand, bool exec)
{
	int8_t* p;
	int8_t* pEnd;
	uint8_t out[5];
	
	if(DebugEnable) DEBUG("cmd:%s",cmd);
	
	if(strncmp((char *)get_conf_elem(WARNCIRCLE_CONF),"1",1))
			return E_OK;

/*	
	__HAL_RCC_GPIOA_CLK_ENABLE();
	GPIO_InitStruct.Pin       = GPIO_PIN_5;
  GPIO_InitStruct.Mode      = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull      = GPIO_NOPULL;
  GPIO_InitStruct.Speed     = GPIO_SPEED_FREQ_VERY_HIGH;
	
	HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);
*/
	pEnd = (u8*)strchr(cmd,'|');
	
	if(pEnd == NULL)
			pEnd = (u8*)strchr(cmd,',');
		memset(out,0,5);
		memcpy(out,cmd,pEnd-cmd);
		if(DebugEnable) DEBUG("out1:%s",out);
	if(!strncmp((char *)get_conf_elem(WARNCIRCLE_CONF),"1",1)&& atoi(out) == 1){
		//PA5
		//HAL_GPIO_WritePin(GPIOA, GPIO_PIN_5, GPIO_PIN_SET);
		start_pwm_out1();
	}else if(!strncmp((char *)get_conf_elem(WARNCIRCLE_CONF),"1",1)&& atoi(out) == 0)
	{
		//HAL_GPIO_WritePin(GPIOA, GPIO_PIN_5, GPIO_PIN_RESET);
		stop_pwm_out1();
	}
	
	p = cmd;
	
	pEnd = (u8*)strchr(p,'|');
	
	if(pEnd != NULL){
		//PC8
		/*
		__HAL_RCC_GPIOC_CLK_ENABLE();
		GPIO_InitStruct.Pin       = GPIO_PIN_8;
		GPIO_InitStruct.Mode      = GPIO_MODE_OUTPUT_PP;
		GPIO_InitStruct.Pull      = GPIO_NOPULL;
		GPIO_InitStruct.Speed     = GPIO_SPEED_FREQ_VERY_HIGH;
	
		HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);
		*/
		p = (u8*)strchr(p,'|');
		p+=1;
		pEnd = (u8*)strchr(p,',');
		memset(out,0,5);
		memcpy(out,p,pEnd-p);
		if(DebugEnable) DEBUG("out2:%s",out);
		if(!strncmp((char *)get_conf_elem(WARNCIRCLE_CONF),"1",1)&& atoi(out) == 1){
		//PC8
		//HAL_GPIO_WritePin(GPIOC, GPIO_PIN_8, GPIO_PIN_SET);
			start_pwm_out2();
		}else if(!strncmp((char *)get_conf_elem(WARNCIRCLE_CONF),"1",1)&& atoi(out) == 0)
		{
		//HAL_GPIO_WritePin(GPIOC, GPIO_PIN_8, GPIO_PIN_RESET);
		stop_pwm_out2();
		}	
	}
	
		p = (u8*)strchr(p,',');
		p+=1;
		memset(out,0,5);
		memcpy(out,p,cmd+strlen(cmd)+1-p);
		if(DebugEnable) DEBUG("out3:%s",out);
		if((atoi(out) & 0xffff)==1 && !strncmp((char *)get_conf_elem(WARNEFAN_CONF),"0",1))
		update_conf_elem(WARNEFAN_CONF,"1");
		else if((atoi(out) & 0xffff)==0 && !strncmp((char *)get_conf_elem(WARNEFAN_CONF),"1",1))
		update_conf_elem(WARNEFAN_CONF,"0");
	
	signalEvent();
	return E_OK;
}



/*******************************************************************************
* Function Name	:	GGC_SafeZone
* Description	:
*******************************************************************************/
uint8_t GGC_SafeZone(int8_t* cmd, const GgCommand_t * ggCommand, bool exec)
{
	geofence_t geo;
	int8_t* p;
	int8_t* pEnd;
	uint8_t value[15];
	
	p = cmd;
	pEnd  = (u8*)strchr(p,',');
	if(pEnd  == NULL)
		return E_ERROR;
	
	memset(geo.Name,0,sizeof(geo.Name));
	memcpy(geo.Name,p,pEnd-p);
	if(DebugEnable) DEBUG("geo.Name:%s",geo.Name);
	
	p = (u8*)strchr(p,',');
	p+=1;
	
	pEnd = (u8*)strchr(p,',');
	
	if(pEnd == NULL)
		return E_ERROR;
	
	geo.Type = *p;
	if(DebugEnable) DEBUG("geo.Type:%c",geo.Type);
	
	p = (u8*)strchr(p,',');
	p+=1;
	
	pEnd = (u8*)strchr(p,'|');
	
	if(pEnd == NULL){
		deleteGeofenceData(&geo);
		return E_OK;
	}
	memset(value,0,15);
	memcpy(value,p,pEnd-p);
	
	p = p+(pEnd-p-1);
	
	if(DebugEnable) DEBUG("p:%c",*p);
	
	geo.Point.lat = atof(value);
	
	if( *p == 'S')
		geo.Point.lat = -geo.Point.lat;
	
	if(DebugEnable) DEBUG("lat:%f",geo.Point.lat);
	
	p = (u8*)strchr(p,'|');
	p+=1;
	
	
	pEnd = (u8*)strchr(p,'|');
	
	if(pEnd == NULL)
		return E_ERROR;
	
	memset(value,0,15);
	memcpy(value,p,pEnd-p);
	
	p = p+(pEnd-p-1);
	
	if(DebugEnable) DEBUG("p:%c",*p);
	
	geo.Point.lng = atof(value);
	
	if( *p == 'W')
		geo.Point.lng = -geo.Point.lng;
	
	if(DebugEnable) DEBUG("lng:%f",geo.Point.lng);
	
	p = (u8*)strchr(p,'|');
	p+=1;
	
	if(geo.Type == '0'){
	
	pEnd = (u8*)strchr(p,',');
	
	if(pEnd == NULL)
		return E_ERROR;
	
	memset(value,0,15);
	memcpy(value,p,pEnd-p);
	
	geo.Radius = atof(value)/1000;
	
	if(DebugEnable) DEBUG("Radius:%f",geo.Radius);
	
	geo.Point.lat1 = 0.0;
	geo.Point.lng1 = 0.0;
	
	if(DebugEnable) DEBUG("lat1:%f",geo.Point.lat1);
	if(DebugEnable) DEBUG("lng1:%f",geo.Point.lng1);
	
	}else
	{
	pEnd = (u8*)strchr(p,'|');
		
	if(pEnd == NULL)
		return E_ERROR;
	
	memset(value,0,15);
	memcpy(value,p,pEnd-p);
	
	p = p+(pEnd-p-1);
	
	geo.Point.lat1  = atof(value);
	
	
	
	if( *p == 'S')
		geo.Point.lat1 = -geo.Point.lat1;
	
	if(DebugEnable) DEBUG("lat1:%f",geo.Point.lat1);
	
	p = (u8*)strchr(p,'|');
	p+=1;
	
	pEnd = (u8*)strchr(p,',');
	
	if(pEnd == NULL)
		return E_ERROR;
	
	memset(value,0,15);
	memcpy(value,p,pEnd-p);
	
	p = p+(pEnd-p-1);
	
	geo.Point.lng1 = atof(value);
	
	if( *p == 'W')
		geo.Point.lng1 = -geo.Point.lng1;
	
	if(DebugEnable) DEBUG("lng1:%f",geo.Point.lng1);
	
	}
	
	p = (u8*)strchr(p,',');
	p+=1;
	
	pEnd = (u8*)strchr(p,',');
	
	if(pEnd == NULL)
		return E_ERROR;
	
	geo.AlarmType = *p;
	
	if(DebugEnable) DEBUG("geo.AlarmType:%c",geo.AlarmType);
	
	p = (u8*)strchr(p,',');
	p+=1;
	
	memset(value,0,15);
	memcpy(value,p,cmd+strlen(cmd)+1-p);
	
	geo.Point.speed = atof(value);
	
	if(DebugEnable) DEBUG("speed:%f",geo.Point.speed);
	
	addGeofenceData(&geo);
	
	return E_OK;
}

/*******************************************************************************
* Function Name	:	GGC_SimulateAcc
* Description	:
*******************************************************************************/
uint8_t GGC_SimulateAcc(int8_t* cmd, const GgCommand_t * ggCommand, bool exec)
{
	if(strlen(cmd) < 30)
	update_conf_elem(SIMULATE_ACC_CONF,(uint8_t*)cmd);
	return E_OK;
}


/*******************************************************************************
* Function Name	:	GGC_Direction
* Description	:
*******************************************************************************/
uint8_t GGC_Angle(int8_t* cmd, const GgCommand_t * ggCommand, bool exec)
{
	if(strlen(cmd) < 10)
	update_conf_elem(ANGLE_CONF,(uint8_t*)cmd);
	return E_OK;
}

/*******************************************************************************
* Function Name	:	GGC_UssdTcpUdp
* Description	:
*******************************************************************************/
uint8_t GGC_UssdTcpUdp(int8_t* cmd, const GgCommand_t * ggCommand, bool exec)
{
	if(!strncmp(cmd,"0",1))
		return E_OK;
	
	update_conf_elem(WORKINGMODE_CONF,(uint8_t*)cmd);
	return E_OK;
}


/*******************************************************************************
* Function Name	:	GGC_KeepAlive
* Description	:
*******************************************************************************/
uint8_t GGC_KeepAlive(int8_t* cmd, const GgCommand_t * ggCommand, bool exec)
{
	update_conf_elem(ACCSTATUSHINT_CONF,(uint8_t*)cmd);
	return E_OK;
}

/*******************************************************************************
* Function Name	:	GGC_GpsStatus
* Description	:
*******************************************************************************/
uint8_t GGC_GpsStatus(int8_t* cmd, const GgCommand_t * ggCommand, bool exec)
{
	update_conf_elem(ACCSLEEP_CONF,(uint8_t*)cmd);
	return E_OK;
}


/*******************************************************************************
* Function Name	:	GGC_Setup
* Description	:
*******************************************************************************/
uint8_t GGC_Setup(int8_t* cmd, const GgCommand_t * ggCommand, bool exec)
{
	int8_t* p;
	memset(cmdnumer,0,20);
	 
	p = (int8_t*)strchr((char*)cmd,',');
	p+=1;
	memcpy(cmdnumer,cmd,p-cmd);
	
	resetIndex = handleSmsCommand(p,true,true);
	return E_OK;
}

/*******************************************************************************
* Function Name	:	GGC_AckStatus
* Description	:
*******************************************************************************/
uint8_t GGC_AckStatus(int8_t* cmd, const GgCommand_t * ggCommand, bool exec)
{
	update_conf_elem(RESERVE5_CONF,(uint8_t*)cmd);
	return E_OK;
}


/*******************************************************************************
* Function Name	:	GGC_Protocol
* Description	:
*******************************************************************************/
uint8_t GGC_Protocol(int8_t* cmd, const GgCommand_t * ggCommand, bool exec)
{
	update_conf_elem(RESERVE3_CONF,(uint8_t*)cmd);
	return E_OK;
}


/*******************************************************************************
* Function Name	:	GGC_SleepMode
* Description	:
*******************************************************************************/
uint8_t GGC_SleepMode(int8_t* cmd, const GgCommand_t * ggCommand, bool exec)
{
	update_conf_elem(RESERVE6_CONF,(uint8_t*)cmd);
	return E_OK;
}


/*******************************************************************************
* Function Name	:	GGC_Agps
* Description	:
*******************************************************************************/
uint8_t GGC_Agps(int8_t* cmd, const GgCommand_t * ggCommand, bool exec)
{
	update_conf_elem(RESERVE7_CONF,(uint8_t*)cmd);
	return E_OK;
}

/*******************************************************************************
* Function Name	:	GGC_Jamming
* Description	:
*******************************************************************************/
uint8_t GGC_Jamming(int8_t* cmd, const GgCommand_t * ggCommand, bool exec)
{
	update_conf_elem(RESERVE8_CONF,(uint8_t*)cmd);
	return E_OK;
}

/*******************************************************************************
* Function Name	:	GGC_SetupDate
* Description	:
*******************************************************************************/

uint8_t GGC_SetupDate(int8_t* cmd, const GgCommand_t * ggCommand, bool exec)
{
	parse_setupdate_setting(cmd);
  return E_OK;
}


/*******************************************************************************
* Function Name	:	parse_onrepinterval_setting
* Description	:
*******************************************************************************/
uint8_t parse_onrepinterval_setting(int8_t* cmd)
{
	int8_t reporttime[10];
	int8_t* p;
	
	p = (int8_t*)strchr((char*)cmd,',');
	if(p== NULL){
		return E_ERROR;
	}
	p+=1;
	if(cmd+strlen((char*)cmd)+1-p-1 > 0 && cmd+strlen((char*)cmd)+1-p-1 <10){
	memcpy(reporttime,p,cmd+strlen((char*)cmd)+1-p);
	reporttime[cmd+strlen((char*)cmd)+1-p] = 0;
	if(DebugEnable)
	DEBUG("onrepinterval:%s",reporttime);
	if(strcmp((char *)get_conf_elem(REPINTERVAL_CONF),reporttime)){
	//if(atoi(reporttime) >= 10)
	update_conf_elem(REPINTERVAL_CONF,(uint8_t*)reporttime);
	osDelay(500);
	setGpsSendTimer(ACC_EVENT_ON);
	}
	}
	return E_OK;
}

/*******************************************************************************
* Function Name	:	parse_offrepinterval_setting
* Description	:
*******************************************************************************/
uint8_t parse_offrepinterval_setting(int8_t* cmd)
{
	int8_t reporttime[10];
	int8_t* p;
	
	p = (int8_t*)strchr((char*)cmd,',');
	if(p == NULL){
		return E_ERROR;
	}
	p+=1;
	if(cmd+strlen((char*)cmd)+1-p-1 > 0 && cmd+strlen((char*)cmd)+1-p-1 <10){
	memcpy(reporttime,p,cmd+strlen((char*)cmd)+1-p);
	reporttime[cmd+strlen((char*)cmd)+1-p] = 0;
	if(DebugEnable)
	DEBUG("offrepinterval:%s",reporttime);
	if(strcmp((char *)get_conf_elem(ACCOFFREPINTERVAL_CONF),reporttime)){
	if(atoi(reporttime) >= 60){
	update_conf_elem(ACCOFFREPINTERVAL_CONF,(uint8_t*)reporttime);
	osDelay(500);
	setGpsSendTimer(ACC_EVENT_OFF);
	}
	}
	}

	return E_OK;
}


/*******************************************************************************
* Function Name	:	parse_guard_setting
* Description	:
*******************************************************************************/
uint8_t parse_guard_setting(int8_t* cmd)
{
	int8_t guard[5];
	int8_t* p;
	
	p = (int8_t*)strchr((char*)cmd,',');
	if(p == NULL){
		return E_ERROR;
	}
	p+=1;
	if(cmd+strlen((char*)cmd)+1-p-1 > 0 && cmd+strlen((char*)cmd)+1-p-1 <5){
	memcpy(guard,p,cmd+strlen((char*)cmd)+1-p);
	guard[cmd+strlen((char*)cmd)+1-p] = 0;
	if(DebugEnable)
	DEBUG("guard:%s",guard);

	if(!strncmp(guard,"on",2)){
	update_conf_elem(WARNCIRCLE_CONF,"1");
	return E_OK;
	}
	else if(!strncmp(guard,"off",3))
	{
	update_conf_elem(WARNCIRCLE_CONF,"0");
	return E_OK;
	}
	else
	{
	return E_OK;
	}
	
	}
	
	return E_OK;
}

/*******************************************************************************
* Function Name	:	parse_lowbat_setting
* Description	:
*******************************************************************************/
int8_t parse_lowbat_setting(int8_t* cmd)
{
	int8_t lowbat[5];
	int8_t* p;
	
	p = (int8_t*)strchr((char*)cmd,',');
	if(p == NULL){
		return E_ERROR;
	}
	p+=1;
	if(cmd+strlen((char*)cmd)+1-p-1 > 0 && cmd+strlen((char*)cmd)+1-p-1 <5){
	memcpy(lowbat,p,cmd+strlen((char*)cmd)+1-p);
	lowbat[cmd+strlen((char*)cmd)+1-p] = 0;
	if(DebugEnable)
	DEBUG("lowbat:%s",lowbat);
	
	if(!strncmp(lowbat,"on",2)){
	update_conf_elem(WARNLOWBATT_CONF,"1");
	return E_OK;
	}
	else if(!strncmp(lowbat,"off",3))
	{
	update_conf_elem(WARNLOWBATT_CONF,"0");
	return E_OK;
	}
	else
	{
	return E_OK;
	}
	}
	return E_ERROR;
}


/*******************************************************************************
* Function Name	:	parse_urgent_setting
* Description	:
*******************************************************************************/
uint8_t parse_urgent_setting(int8_t* cmd)
{
	int8_t urgent[5];
	int8_t* p;
	
	p = (int8_t*)strchr((char*)cmd,',');
	if(p == NULL){
		return E_ERROR;
	}
	p+=1;
	if(cmd+strlen((char*)cmd)+1-p-1 > 0 && cmd+strlen((char*)cmd)+1-p-1 <5){
	memcpy(urgent,p,cmd+strlen((char*)cmd)+1-p);
	urgent[cmd+strlen((char*)cmd)+1-p] = 0;
	if(DebugEnable)
	DEBUG("urgent:%s",urgent);
	update_conf_elem(WARNURGENT_CONF,(uint8_t*)urgent);
	
	
	if(!strncmp(urgent,"on",2)){
	update_conf_elem(WARNURGENT_CONF,"1");
	return E_OK;
	}else if(!strncmp(urgent,"off",3))
	{
	update_conf_elem(WARNURGENT_CONF,"0");
	return E_OK;
	}
	else
	{
	return E_ERROR;
	}

	}
	return E_ERROR;
}


/*******************************************************************************
* Function Name	:	parse_shock_setting
* Description	:
*******************************************************************************/
uint8_t parse_shock_setting(int8_t* cmd)
{
	int8_t shock[5];
	int8_t* p;
	
	p = (int8_t*)strchr((char*)cmd,',');
	if(p == NULL){
		return E_ERROR;
	}
	p+=1;
	if(cmd+strlen((char*)cmd)+1-p-1 > 0 && cmd+strlen((char*)cmd)+1-p-1 <5){
	memcpy(shock,p,cmd+strlen((char*)cmd)+1-p);
	shock[cmd+strlen((char*)cmd)+1-p] = 0;
	if(DebugEnable)
	DEBUG("shock:%s",shock);
	update_conf_elem(WARNSHOCK_CONF,(uint8_t*)shock);
	
	
	if(!strncmp(shock,"on",2)){
	update_conf_elem(WARNSHOCK_CONF,"1");
	return E_OK;
	}else if(!strncmp(shock,"off",3))
	{
	update_conf_elem(WARNSHOCK_CONF,"0");
	return E_OK;
	}
	else
	{
	return E_ERROR;
	}

	}
	return E_ERROR;
}

/*******************************************************************************
* Function Name	:	parse_overspeed_setting
* Description	:
*******************************************************************************/
uint8_t parse_overspeed_setting(int8_t* cmd)
{
	int8_t overspeed[5];
	int8_t speed[10];
	int8_t* p;
	
	p = (int8_t*)strchr((char*)cmd,',');
	if(p == NULL && strlen((char*)cmd) == 0){
		return E_ERROR;
	}
	else if(p == NULL && strlen((char*)cmd) > 0 && strlen((char*)cmd) < 5)
	{
	memcpy(overspeed,cmd,strlen((char*)cmd));
	overspeed[strlen((char*)cmd)] = 0;
	if(DebugEnable)
	DEBUG("overspeed:%s",overspeed);
	if(!strncmp(overspeed,"on",2) && strncmp((char *)get_conf_elem(WARNOVERSPEED_CONF),"1",1))
	update_conf_elem(WARNOVERSPEED_CONF,"1");
	else if(!strncmp(overspeed,"off",3) && strncmp((char *)get_conf_elem(WARNOVERSPEED_CONF),"0",1))
	{
	update_conf_elem(WARNOVERSPEED_CONF,"0");
	return E_OK;
	}
	}
	
	if(p-cmd > 0 && p-cmd <5){
	memcpy(overspeed,cmd,p-cmd);
	overspeed[p-cmd] = 0;
	if(DebugEnable)
	DEBUG("overspeed:%s",overspeed);
	if(!strncmp(overspeed,"on",2) && strncmp((char *)get_conf_elem(WARNOVERSPEED_CONF),"1",1))
	update_conf_elem(WARNOVERSPEED_CONF,"1");
	else if(!strncmp(overspeed,"off",3) && strncmp((char *)get_conf_elem(WARNOVERSPEED_CONF),"0",1))
	{
	update_conf_elem(WARNOVERSPEED_CONF,"0");
	return E_OK;
	}
	
	}

	p = (int8_t*)strchr((char*)p,',');
	if(p == NULL){
		return E_OK;
	}
	p+=1;
	if(cmd+strlen((char*)cmd)+1-p-1 > 0 && cmd+strlen((char*)cmd)+1-p-1 < 10){
	memcpy(speed,p,cmd+strlen((char*)cmd)+1-p);
	speed[cmd+strlen((char*)cmd)+1-p] = 0;
	if(DebugEnable)
	DEBUG("speed:%s",speed);
	if(strcmp((char *)get_conf_elem(SPEED_CONF),speed))
	update_conf_elem(SPEED_CONF,(uint8_t*)speed);
	}
	
	return E_OK;

}


/*******************************************************************************
* Function Name	:	parse_arm_setting
* Description	:
*******************************************************************************/
uint8_t parse_arm_setting(int8_t* cmd)
{
	if(strcmp((char *)get_conf_elem(WARNCIRCLE_CONF),"1"))
	update_conf_elem(WARNCIRCLE_CONF,"1");
	signalEvent();
	#if 0
	setSirenWork(true);
	osDelay(400);
	setSirenWork(false);
	#endif
	return E_OK;
}


/*******************************************************************************
* Function Name	:	parse_disarm_setting
* Description	:
*******************************************************************************/
uint8_t parse_disarm_setting(int8_t* cmd)
{
	if(strcmp((char *)get_conf_elem(WARNCIRCLE_CONF),"0"))
	update_conf_elem(WARNCIRCLE_CONF,"0");
	signalEvent();
	#if 0
	setSirenWork(true);
	osDelay(1000);
	setSirenWork(false);
	osDelay(1000);
	setSirenWork(true);
	osDelay(1000);
	setSirenWork(false);
	#endif
	return E_OK;
}


/*******************************************************************************
* Function Name	:	parse_igkillon_setting
* Description	:
*******************************************************************************/
uint8_t parse_igkillon_setting(int8_t* cmd){
	
	if(strncmp((char *)get_conf_elem(WARNCIRCLE_CONF),"1",1))
			return E_ERROR;
	
	GPIO_InitTypeDef  GPIO_InitStruct;
	//PA5
	/*
	__HAL_RCC_GPIOA_CLK_ENABLE();
	GPIO_InitStruct.Pin       = GPIO_PIN_5;
  GPIO_InitStruct.Mode      = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull      = GPIO_NOPULL;
  GPIO_InitStruct.Speed     = GPIO_SPEED_FREQ_VERY_HIGH;
	
	HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

	HAL_GPIO_WritePin(GPIOA, GPIO_PIN_5, GPIO_PIN_SET);
	
	
	//PC8
	__HAL_RCC_GPIOC_CLK_ENABLE();
	GPIO_InitStruct.Pin       = GPIO_PIN_8;
  GPIO_InitStruct.Mode      = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull      = GPIO_NOPULL;
  GPIO_InitStruct.Speed     = GPIO_SPEED_FREQ_VERY_HIGH;
	
	HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);
	
	HAL_GPIO_WritePin(GPIOC, GPIO_PIN_8, GPIO_PIN_SET);
	*/
	start_pwm_out1();
	start_pwm_out2();
	signalEvent();
	
	return E_OK;
		
}

/*******************************************************************************
* Function Name	:	parse_igkilloff_setting
* Description	:
*******************************************************************************/
uint8_t parse_igkilloff_setting(int8_t* cmd){
	
	if(strncmp((char *)get_conf_elem(WARNCIRCLE_CONF),"1",1))
			return E_ERROR;
	
	GPIO_InitTypeDef  GPIO_InitStruct;
	//PA5
	/*
	__HAL_RCC_GPIOA_CLK_ENABLE();
	GPIO_InitStruct.Pin       = GPIO_PIN_5;
  GPIO_InitStruct.Mode      = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull      = GPIO_NOPULL;
  GPIO_InitStruct.Speed     = GPIO_SPEED_FREQ_VERY_HIGH;
	
	HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

	HAL_GPIO_WritePin(GPIOA, GPIO_PIN_5, GPIO_PIN_RESET);
	
	
	//PC8
	__HAL_RCC_GPIOC_CLK_ENABLE();
	GPIO_InitStruct.Pin       = GPIO_PIN_8;
  GPIO_InitStruct.Mode      = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull      = GPIO_NOPULL;
  GPIO_InitStruct.Speed     = GPIO_SPEED_FREQ_VERY_HIGH;
	
	HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);
	
	HAL_GPIO_WritePin(GPIOC, GPIO_PIN_8, GPIO_PIN_RESET);
	*/
	
	stop_pwm_out1();
	stop_pwm_out2();
	signalEvent();
	
	return E_OK;
}

/*******************************************************************************
* Function Name	:	parse_swon_setting
* Description	:
*******************************************************************************/
uint8_t parse_swon_setting(int8_t* cmd){
	
	if(strcmp((char *)get_conf_elem(WARNEFAN_CONF),"1"))
	update_conf_elem(WARNEFAN_CONF,"1");
	signalEvent();

	return E_OK;
	
	
}


/*******************************************************************************
* Function Name	:	parse_swoff_setting
* Description	:
*******************************************************************************/
uint8_t parse_swoff_setting(int8_t* cmd){
	
	if(strcmp((char *)get_conf_elem(WARNEFAN_CONF),"0"))
	update_conf_elem(WARNEFAN_CONF,"0");
	signalEvent();

	return E_OK;
	
}


/*******************************************************************************
* Function Name	:	parse_cellon_setting
* Description	:
*******************************************************************************/
uint8_t parse_cellon_setting(int8_t* cmd){
	
	if(strcmp((char *)get_conf_elem(GPS_MOTION_THRESHOLD_CONF),"1"))
	update_conf_elem(GPS_MOTION_THRESHOLD_CONF,"1");

	return E_OK;
	
	
}


/*******************************************************************************
* Function Name	:	parse_celloff_setting
* Description	:
*******************************************************************************/
uint8_t parse_celloff_setting(int8_t* cmd){
	
	if(strcmp((char *)get_conf_elem(GPS_MOTION_THRESHOLD_CONF),"2"))
	update_conf_elem(GPS_MOTION_THRESHOLD_CONF,"2");

	return E_OK;
	
}



/*******************************************************************************
* Function Name	:	parse_sirenon_setting
* Description	:
*******************************************************************************/
uint8_t parse_sirenon_setting(int8_t* cmd){
	
	if(strncmp((char *)get_conf_elem(WARNCIRCLE_CONF),"1",1)){
			signalEvent();
			return E_ERROR;
	}
	
	setSirenStatus(1);
	/*
	__HAL_RCC_GPIOC_CLK_ENABLE();
	GPIO_InitStruct.Pin       = GPIO_PIN_8;
  GPIO_InitStruct.Mode      = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull      = GPIO_NOPULL;
  GPIO_InitStruct.Speed     = GPIO_SPEED_FREQ_VERY_HIGH;
	
	HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);
	
	HAL_GPIO_WritePin(GPIOC, GPIO_PIN_8, GPIO_PIN_SET);
	*/
	start_pwm_out2();
	signalEvent();
	return E_OK;
	
}

/*******************************************************************************
* Function Name	:	parse_sirenoff_setting
* Description	:
*******************************************************************************/
uint8_t parse_sirenoff_setting(int8_t* cmd){
	
	if(strncmp((char *)get_conf_elem(WARNCIRCLE_CONF),"1",1)){
			signalEvent();
			return E_ERROR;
	}
	
	setSirenStatus(0);
	/*
	__HAL_RCC_GPIOC_CLK_ENABLE();
	GPIO_InitStruct.Pin       = GPIO_PIN_8;
  GPIO_InitStruct.Mode      = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull      = GPIO_NOPULL;
  GPIO_InitStruct.Speed     = GPIO_SPEED_FREQ_VERY_HIGH;
	
	HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);
	
	HAL_GPIO_WritePin(GPIOC, GPIO_PIN_8, GPIO_PIN_RESET);
	*/
	stop_pwm_out2();
	signalEvent();
	return E_OK;
	
}

/*******************************************************************************
* Function Name	:	parse_out1on_setting
* Description	:
*******************************************************************************/
uint8_t parse_out1on_setting(int8_t* cmd){
	
	if(strncmp((char *)get_conf_elem(WARNCIRCLE_CONF),"1",1)){
			signalEvent();
			return E_ERROR;
	}
	
	setSirenStatus(1);
	/*
	__HAL_RCC_GPIOC_CLK_ENABLE();
	GPIO_InitStruct.Pin       = GPIO_PIN_8;
  GPIO_InitStruct.Mode      = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull      = GPIO_NOPULL;
  GPIO_InitStruct.Speed     = GPIO_SPEED_FREQ_VERY_HIGH;
	
	HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);
	
	HAL_GPIO_WritePin(GPIOC, GPIO_PIN_8, GPIO_PIN_SET);
	*/
	start_pwm_out1();
	signalEvent();
	return E_OK;
	
}

/*******************************************************************************
* Function Name	:	parse_out1off_setting
* Description	:
*******************************************************************************/
uint8_t parse_out1off_setting(int8_t* cmd){
	
	if(strncmp((char *)get_conf_elem(WARNCIRCLE_CONF),"1",1)){
			signalEvent();
			return E_ERROR;
	}
	
	setSirenStatus(0);
	/*
	__HAL_RCC_GPIOC_CLK_ENABLE();
	GPIO_InitStruct.Pin       = GPIO_PIN_8;
  GPIO_InitStruct.Mode      = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull      = GPIO_NOPULL;
  GPIO_InitStruct.Speed     = GPIO_SPEED_FREQ_VERY_HIGH;
	
	HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);
	
	HAL_GPIO_WritePin(GPIOC, GPIO_PIN_8, GPIO_PIN_RESET);
	*/
	stop_pwm_out1();
	signalEvent();
	return E_OK;
	
}


/*******************************************************************************
* Function Name	:	parse_gpsfixed_setting
* Description	:
*******************************************************************************/
uint8_t parse_gpsfixed_setting(int8_t* cmd){
	
	if(strcmp((char *)get_conf_elem(ACCSLEEP_CONF),"1"))
	update_conf_elem(ACCSLEEP_CONF,"1");

	return E_OK;
	
}


/*******************************************************************************
* Function Name	:	parse_gpslost_setting
* Description	:
*******************************************************************************/
uint8_t parse_gpslost_setting(int8_t* cmd){
	
	if(strcmp((char *)get_conf_elem(ACCSLEEP_CONF),"0"))
	update_conf_elem(ACCSLEEP_CONF,"0");

	return E_OK;
	
}

/*******************************************************************************
* Function Name	:	parse_eh_setting
* Description	:
*******************************************************************************/
uint8_t parse_eh_setting(int8_t* cmd){
	int8_t* p;
	int8_t eh[10];
		
		p = cmd;
		if((u8*)strchr(p,',') != NULL){
			p= (u8*)strchr(p,',');
			p+=1;
			if((u8*)strchr(p,',') == NULL){
			if(cmd+strlen(cmd)+1-p < 10 && (cmd+strlen(cmd)+1-p) >=0){
			memcpy(eh,p,cmd+strlen(cmd)+1-p);
			if(DebugEnable) DEBUG("eh %s",eh);
			setGc_enginHour(eh);
			//ehEvent(EH_EVENT,eh);
			return E_OK;
			}
			}
		}
		return E_ERROR;
}


/*******************************************************************************
* Function Name	:	parse_ml_setting
* Description	:
*******************************************************************************/
uint8_t parse_ml_setting(int8_t* cmd){
	int8_t* p;
	int8_t ml[10];
		
		p = cmd;
		if((u8*)strchr(p,',') != NULL){
			p= (u8*)strchr(p,',');
			p+=1;
			if((u8*)strchr(p,',') == NULL){
			if(cmd+strlen(cmd)+1-p < 10 && (cmd+strlen(cmd)+1-p) >=0){
			memcpy(ml,p,cmd+strlen(cmd)+1-p);
			if(DebugEnable) DEBUG("ml %s",ml);
			setGc_mileage(ml);
			//mlEvent(ML_EVENT,ml);
			return E_OK;
			}
			}
		}
		return E_ERROR;
}





/*******************************************************************************
* Function Name	:	parse_update_setting
* Description	:
*******************************************************************************/
void parse_update_setting(int8_t* cmd)
{
	update_conf_elem(IAP_CONF,"1");
}


/*******************************************************************************
* Function Name	:	parse_poweroff_setting
* Description	:
*******************************************************************************/
uint8_t parse_poweroff_setting(int8_t* cmd)
{
	SystemRestart();
	return E_OK;
}


/*******************************************************************************
* Function Name	:	parse_timezone_setting
* Description	:
*******************************************************************************/
uint8_t parse_timezone_setting(int8_t* cmd)
{
	int8_t timezone[10];
	int8_t* p;
	
	p = (int8_t*)strchr((char*)cmd,',');
	if(p == NULL){
		return E_ERROR;
	}
	p+=1;
	if(cmd+strlen((char*)cmd)+1-p-1 > 0 && cmd+strlen((char*)cmd)+1-p-1 <10){
	memcpy(timezone,p,cmd+strlen((char*)cmd)+1-p);
	timezone[cmd+strlen((char*)cmd)+1-p] = 0;
	sprintf(timezone, "%.1lf",atof(timezone)) ;
	if(DebugEnable)
	DEBUG("timezone:%s",timezone);
	if(strcmp((char *)get_conf_elem(TIMEZONE_CONF),timezone)){
	update_conf_elem(TIMEZONE_CONF,(uint8_t*)timezone);
	}
	}
	
	return E_OK;
}


/*******************************************************************************
* Function Name	:	adjust_timezone_time
* Description	:
*******************************************************************************/
void adjust_timezone_time(RTC_TimeTypeDef* rtc)
{
	int8_t timezone[5]={0};
	int8_t* p;

	if(strcmp((char *)get_conf_elem(TIMEZONE_CONF),"0")==0){
		if(rtc->Hours+8>=24)
			rtc->Hours = rtc->Hours+8-24;
		else
			rtc->Hours = rtc->Hours+8;
	}else{
 	memset(timezone,0,5);
 	memcpy(timezone,(char *)get_conf_elem(TIMEZONE_CONF),strlen((char *)get_conf_elem(TIMEZONE_CONF)));
 	p = timezone;
 	p=(int8_t*)strchr((char *)p,'.');
 	p+=1; 
 		if(rtc->Hours+atoi((char *)timezone)>=24)
		rtc->Hours = rtc->Hours+atoi((char *)timezone)-24;
 		else if(rtc->Hours+atoi((char *)timezone)<0)
		rtc->Hours = rtc->Hours+atoi((char *)timezone)+24;
		else
		rtc->Hours = rtc->Hours+atoi((char *)timezone);

		if(atoi((char *)timezone) > 0){
			if(rtc->Minutes+atoi((char *)p)*60/10 >=60){
				rtc->Minutes = rtc->Minutes+atoi((char *)p)*60/10-60;
				rtc->Hours=rtc->Hours+1;
		if(rtc->Hours>=24)
				rtc->Hours=rtc->Hours-24;
		}
	else{
		rtc->Minutes = rtc->Minutes+atoi((char *)p)*60/10;
	}
	}else{
		if(rtc->Minutes-atoi((char *)p)*60/10 < 0){
			rtc->Minutes = rtc->Minutes-atoi((char *)p)*60/10+60;
			if(rtc->Hours<1)
			rtc->Hours=rtc->Hours+23;
			else
			rtc->Hours=rtc->Hours-1;
		}else
			rtc->Minutes = rtc->Minutes-atoi((char *)p)*60/10;
	}
}

}

/*******************************************************************************
* Function Name	:	parse_parameter_message
* Description	:
*******************************************************************************/
uint8_t parse_parameter_message(int8_t* reply,bool setup)
{
	if(!setup){
	sprintf((char *)reply, "%s,\r\n%s,\r\n%s,\r\n%s,\r\n%s,\r\n%s,\r\n%s,\r\n%s,\r\n%s,\r\n%s", 
		GsmIMEI(), 
		get_conf_elem(SW_VERSION_CONF),
		get_conf_elem(SOSTELENUMBER_CONF),
		get_conf_elem(TIMEZONE_CONF),
		get_conf_elem(MAINSERVERIPADDR_CONF),
		get_conf_elem(MAINSERVERPORTNUMBER_CONF),
		get_conf_elem(APNNAME_CONF),
		get_conf_elem(APNUSER_CONF),
		get_conf_elem(APNPASS_CONF),
		get_conf_elem(REPINTERVAL_CONF)
	) ; 
	}
	else
	{
	sprintf((char *)reply, "%s,%s,%s,%s,%s,%s,%s,%s,%s,%s", 
		GsmIMEI(), 
		get_conf_elem(SW_VERSION_CONF),
		get_conf_elem(SOSTELENUMBER_CONF),
		get_conf_elem(TIMEZONE_CONF),
		get_conf_elem(MAINSERVERIPADDR_CONF),
		get_conf_elem(MAINSERVERPORTNUMBER_CONF),
		get_conf_elem(APNNAME_CONF),
		get_conf_elem(APNUSER_CONF),
		get_conf_elem(APNPASS_CONF),
		get_conf_elem(REPINTERVAL_CONF)
	) ; 
	}
	return E_OK;
}

/*******************************************************************************
* Function Name	:	parse_status_message
* Description	:
*******************************************************************************/
uint8_t parse_status_message(int8_t* reply,bool setup)
{
	char* chargestatus;
	char* netstatus;
	char* gps;
	
	if(isAccOn())
		chargestatus="yes";
	else
		chargestatus="no";
	if(getTCPConState())
		netstatus = "connected";
	else
		netstatus = "unconnect";
	if(!GpsNoData())
		gps = "success";
	else
		gps = "fail";
	if(!setup){
	sprintf((char *)reply, "battery:%s,\r\nchargstatus:%s,\r\nnetstatus:%s,\r\ngsmsignal:%s,\r\ngps:%s\r\n", 
			getADCVolt_v(), 
			chargestatus,
			netstatus,
			getSigalQuality(),
			gps
	) ; 		
	}else
	{
	sprintf((char *)reply, "battery:%s,chargstatus:%s,netstatus:%s,gsmsignal:%s,gps:%s", 
			getADCVolt_v(), 
			chargestatus,
			netstatus,
			getSigalQuality(),
			gps
	) ; 
	}
		return E_OK;
}

/*******************************************************************************
* Function Name	:	parse_position_message
* Description	:
*******************************************************************************/
uint8_t parse_position_message(int8_t* reply)
{
	double latitude;
  double longitude;
	double degree ; 
  double minute; 
	
  RTC_TimeTypeDef rtc;
	
	RTC_DateTypeDef date;

	nmeaGPRMC * rmc = getLastGPRMCData();
	
	if(rmc->lat == 0)
		rmc->ns = '0';
	else
		rmc->ns = ((rmc->lat > 0) ? 'N' : 'S');
				
	if(rmc->lon == 0)
		rmc->ew = '0';
	else
	rmc->ew = ((rmc->lon > 0) ? 'E' : 'W');
				
	rmc->lat = nmea_degree2ndeg(fabs(rmc->lat));
	rmc->lon = nmea_degree2ndeg(fabs(rmc->lon));
	
	if(DebugEnable) DEBUG("rmc->lat %f",rmc->lat);
	
	#if 1
	degree = floor(rmc->lat / 100);
  minute = rmc->lat - degree * 100;
  latitude= degree + minute / 60;
	
	degree = floor(rmc->lon / 100);
  minute = rmc->lon - degree * 100;
  longitude = degree + minute / 60;
	#endif
	
  /* Get the RTC current Time */
  HAL_RTC_GetTime(&RTCHandle, &rtc, RTC_FORMAT_BIN);
	
	HAL_RTC_GetDate(&RTCHandle, &date, RTC_FORMAT_BIN);
	
	adjust_timezone_time(&rtc);
			   
	if( rmc->ns =='N' && rmc->ew =='E'){
	   sprintf((char *)reply, "%s,c,%s,%02d:%02d:%02d,https://www.google.com/maps?q=%f,%f", GsmIMEI(), "te103", rtc.Hours,rtc.Minutes,rtc.Seconds, latitude, longitude) ; 	            		
	} else if ( rmc->ns =='N' && rmc->ew =='W'){
	   sprintf((char *)reply, "%s,c,%s,%02d:%02d:%02d,https://www.google.com/maps?q=%f,-%f", GsmIMEI(), "te103", rtc.Hours,rtc.Minutes,rtc.Seconds, latitude, longitude) ; 
	} else if (rmc->ns =='S' && rmc->ew =='E'){		
	   sprintf((char *)reply, "%s,c,%s,%02d:%02d:%02d,https://www.google.com/maps?q=-%f,%f", GsmIMEI(), "te103", rtc.Hours,rtc.Minutes,rtc.Seconds, latitude, longitude) ; 
	} else if (rmc->ns =='S' && rmc->ew =='W' ){
	   sprintf((char *)reply, "%s,c,%s,%02d:%02d:%02d,https://www.google.com/maps?q=-%f,-%f", GsmIMEI(), "te103", rtc.Hours,rtc.Minutes,rtc.Seconds, latitude, longitude) ; 
	}else {
		 sprintf((char *)reply, "%s,c,%s,%02d:%02d:%02d,https://www.google.com/maps?q=%f,%f", GsmIMEI(), "te103", rtc.Hours,rtc.Minutes,rtc.Seconds, latitude, longitude) ; 
	}
	return E_OK;
}
/*******************************************************************************
* Function Name	:	report_alarm_message
* Description	:
*******************************************************************************/
void report_alarm_message(int8_t* alarm)
{
	double latitude;
  double longitude;
	double degree ; 
  double minute; 
	int8_t reply[200];
	uint8_t number[20];
	
  RTC_TimeTypeDef rtc;
	
	RTC_DateTypeDef date;

	nmeaGPRMC * rmc = getLastGPRMCData();
	
	if(rmc->lat == 0)
		rmc->ns = '0';
	else
		rmc->ns = ((rmc->lat > 0) ? 'N' : 'S');
				
	if(rmc->lon == 0)
		rmc->ew = '0';
	else
	rmc->ew = ((rmc->lon > 0) ? 'E' : 'W');
				
	rmc->lat = nmea_degree2ndeg(fabs(rmc->lat));
	rmc->lon = nmea_degree2ndeg(fabs(rmc->lon));
	
	#if 1
	degree = floor(rmc->lat / 100);
  minute = rmc->lat - degree * 100;
  latitude= degree + minute / 60;
	
	degree = floor(rmc->lon / 100);
  minute = rmc->lon - degree * 100;
  longitude = degree + minute / 60;
	#endif
	
  /* Get the RTC current Time */
  HAL_RTC_GetTime(&RTCHandle, &rtc, RTC_FORMAT_BIN);
	
	HAL_RTC_GetDate(&RTCHandle, &date, RTC_FORMAT_BIN);
	
	adjust_timezone_time(&rtc);
			   
	if( rmc->ns =='N' && rmc->ew =='E'){
	   sprintf((char *)reply, "%s,%s,%s,%02d:%02d:%02d,https://www.google.com/maps?q=%f,%f", GsmIMEI(), alarm,"te103", rtc.Hours,rtc.Minutes,rtc.Seconds, latitude, longitude) ; 	            		
	} else if ( rmc->ns =='N' && rmc->ew =='W'){
	   sprintf((char *)reply, "%s,%s,%s,%02d:%02d:%02d,https://www.google.com/maps?q=%f,-%f", GsmIMEI(), alarm,"te103", rtc.Hours,rtc.Minutes,rtc.Seconds, latitude, longitude) ; 
	} else if (rmc->ns =='S' && rmc->ew =='E'){		
	   sprintf((char *)reply, "%s,%s,%s,%02d:%02d:%02d,https://www.google.com/maps?q=-%f,%f", GsmIMEI(), alarm,"te103", rtc.Hours,rtc.Minutes,rtc.Seconds, latitude, longitude) ; 
	} else if (rmc->ns =='S' && rmc->ew =='W' ){
	   sprintf((char *)reply, "%s,%s,%s,%02d:%02d:%02d,https://www.google.com/maps?q=-%f,-%f", GsmIMEI(), alarm,"te103", rtc.Hours,rtc.Minutes,rtc.Seconds, latitude, longitude) ; 
	}else {
		 sprintf((char *)reply, "%s,%s,%s,%02d:%02d:%02d,https://www.google.com/maps?q=%f,%f", GsmIMEI(), alarm,"te103", rtc.Hours,rtc.Minutes,rtc.Seconds, latitude, longitude) ; 
	}

	if(strncmp(get_conf_elem(SOSTELENUMBER_CONF),"null",strlen(get_conf_elem(SOSTELENUMBER_CONF))) 
		&& strncmp(get_conf_elem(SOSTELENUMBER_CONF),"008613000000000",strlen("008613000000000")) )
	gsmSendSMS(get_conf_elem(SOSTELENUMBER_CONF),reply);
	if(strncmp(get_conf_elem(SOS1TELENUMBER_CONF),"null",strlen(get_conf_elem(SOS1TELENUMBER_CONF)))
		&& strncmp(get_conf_elem(SOS1TELENUMBER_CONF),"008613000000000",strlen("008613000000000")) )
	gsmSendSMS(get_conf_elem(SOS1TELENUMBER_CONF),reply);
	if(strncmp(get_conf_elem(SOS2TELENUMBER_CONF),"null",strlen(get_conf_elem(SOS2TELENUMBER_CONF)))
		&& strncmp(get_conf_elem(SOS2TELENUMBER_CONF),"008613000000000",strlen("008613000000000")))
	gsmSendSMS(get_conf_elem(SOS2TELENUMBER_CONF),reply);
	if(strncmp(get_conf_elem(SOS3TELENUMBER_CONF),"null",strlen(get_conf_elem(SOS3TELENUMBER_CONF)))
		&& strncmp(get_conf_elem(SOS3TELENUMBER_CONF),"008613000000000",strlen("008613000000000")))
	gsmSendSMS(get_conf_elem(SOS3TELENUMBER_CONF),reply);
	
}


/*******************************************************************************
* Function Name	:	parse_position_message
* Description	:
*******************************************************************************/
uint8_t parse_update_message(int8_t* cmd)
{
	int8_t* p;
	
	GG_FwSmsIndex = -1;
	GG_FwFileName[0]='U';
	
	p = cmd;
	
	memset(ota,0,100);
	
	if((u8*)strchr(p,',') != NULL){
			p= (u8*)strchr(p,',');
			p+=1;
			if((u8*)strchr(p,',') == NULL){
			if(cmd+strlen(cmd)+1-p < 100 && (cmd+strlen(cmd)+1-p) >=0){
			memcpy(ota,p,cmd+strlen(cmd)+1-p);
			for(uint8_t i = 0;i<strlen(ota);i++)
			{
				if(ota[i] == 0x11)
					ota[i] = 0x5F;
			}
			if(DebugEnable) DEBUG("ota %s",ota);
			return E_OK;
			}
			}
	}else{
	memcpy(ota,"gps.scjtracker.com:firmware:123456:/var/www/tracker_srv/file_srv/firmware:te101.bin",
						strlen("gps.scjtracker.com:firmware:123456:/var/www/tracker_srv/file_srv/firmware:te101.bin"));
	}
	return E_OK;
}

/*******************************************************************************
* Function Name	:	parse_simulateacc_message
* Description	:
*******************************************************************************/
uint8_t parse_simulateacc_message(int8_t* cmd)
{
	int8_t* p;
	int8_t acc[30];
	
	p = cmd;
	
	if((u8*)strchr(p,',') != NULL){
			p= (u8*)strchr(p,',');
			p+=1;
			if((u8*)strchr(p,',') == NULL){
			if(cmd+strlen(cmd)+1-p < 30 && (cmd+strlen(cmd)+1-p) >=0){
			memset(acc,0,30);
			memcpy(acc,p,cmd+strlen(cmd)+1-p);
				
			for(uint8_t i = 0;i<strlen(acc);i++)
			{
				if(acc[i] == 0x3b)
					acc[i] = 0x7c;
			}
			if(DebugEnable) DEBUG("acc %s",acc);
			update_conf_elem(SIMULATE_ACC_CONF,acc);
			return E_OK;
			}
			}
	}
	return E_ERROR;
}


/*******************************************************************************
* Function Name	:	parse_direction_message
* Description	:
*******************************************************************************/
uint8_t parse_direction_message(int8_t* cmd)
{
	int8_t* p;
	int8_t angle[10];
	
	p = cmd;
	
	if((u8*)strchr(p,',') != NULL){
			p= (u8*)strchr(p,',');
			p+=1;
			if((u8*)strchr(p,',') == NULL){
			if(cmd+strlen(cmd)+1-p < 10 && (cmd+strlen(cmd)+1-p) >=0){
			memset(angle,0,10);
			memcpy(angle,p,cmd+strlen(cmd)+1-p);
			if(DebugEnable) DEBUG("angle %s",angle);
			update_conf_elem(ANGLE_CONF,angle);
			return E_OK;
			}
			}
	}
	return E_ERROR;
}



/*******************************************************************************
* Function Name	:	parse_sensor_message
* Description	:
*******************************************************************************/
uint8_t parse_sensor_message(int8_t* cmd)
{
	int8_t* p;
	int8_t sensor[5];
	
	p = cmd;
	
	if((u8*)strchr(p,',') != NULL){
			p= (u8*)strchr(p,',');
			p+=1;
			if((u8*)strchr(p,',') == NULL){
			if(cmd+strlen(cmd)+1-p < 5 && (cmd+strlen(cmd)+1-p) >=0){
			memset(sensor,0,5);
			memcpy(sensor,p,cmd+strlen(cmd)+1-p);
			if(DebugEnable) DEBUG("sensor %s",sensor);
			update_conf_elem(GSENSOR_SENSITIVITY_CONF,sensor);
			return E_OK;
			}
			}
	}
	
	return E_ERROR;
}


/*******************************************************************************
* Function Name	:	parse_report_message
* Description	:
*******************************************************************************/
uint8_t parse_report_message(int8_t* cmd)
{
	int8_t* p;
	int8_t report[5];
	
	p = cmd;
	
	if((u8*)strchr(p,',') != NULL){
			p= (u8*)strchr(p,',');
			p+=1;
			if((u8*)strchr(p,',') == NULL){
			if(cmd+strlen(cmd)+1-p < 5 && (cmd+strlen(cmd)+1-p) >=0){
			memset(report,0,5);
			memcpy(report,p,cmd+strlen(cmd)+1-p);
			if(DebugEnable) DEBUG("report %s",report);
			update_conf_elem(GSMSWITCH_CONF,report);
			return E_OK;
			}
			}
	}
	return E_ERROR;
}


/*******************************************************************************
* Function Name	:	parse_distance_message
* Description	:
*******************************************************************************/
uint8_t parse_distance_message(int8_t* cmd)
{
	int8_t* p;
	int8_t distance[10];
	
	p = cmd;
		
	if((u8*)strchr(p,',') != NULL){
			p= (u8*)strchr(p,',');
			p+=1;
			if((u8*)strchr(p,',') == NULL){
			if(cmd+strlen(cmd)+1-p < 10 && (cmd+strlen(cmd)+1-p) >=0){
			memset(distance,0,10);
			memcpy(distance,p,cmd+strlen(cmd)+1-p);
			if(DebugEnable) DEBUG("distance %s",distance);
			update_conf_elem(DISTANCE_CONF,distance);
			return E_OK;
			}
			}
	}
	return E_ERROR;
}


/*******************************************************************************
* Function Name	:	parse_speedtime_message
* Description	:
*******************************************************************************/
uint8_t parse_speedtime_message(int8_t* cmd)
{
	int8_t* p;
	int8_t speedtime[10];
	
	p = cmd;
	
	if((u8*)strchr(p,',') != NULL){
			p= (u8*)strchr(p,',');
			p+=1;
			if((u8*)strchr(p,',') == NULL){
			if(cmd+strlen(cmd)+1-p < 10 && (cmd+strlen(cmd)+1-p) >=0){
			memset(speedtime,0,10);
			memcpy(speedtime,p,cmd+strlen(cmd)+1-p);
			if(DebugEnable) DEBUG("speedtime %s",speedtime);
			update_conf_elem(OVERSPEED_TIME_CONF,speedtime);
			return E_OK;
			}
			}
	}
	return E_ERROR;
}


/*******************************************************************************
* Function Name	:	parse_pingtime_message
* Description	:
*******************************************************************************/
uint8_t parse_pingtime_message(int8_t* cmd)
{
	int8_t* p;
	int8_t pingtime[10];
	
	p = cmd;
		
	if((u8*)strchr(p,',') != NULL){
			p= (u8*)strchr(p,',');
			p+=1;
			if((u8*)strchr(p,',') == NULL){
			if(cmd+strlen(cmd)+1-p < 10 && (cmd+strlen(cmd)+1-p) >=0){
			memset(pingtime,0,10);
			memcpy(pingtime,p,cmd+strlen(cmd)+1-p);
			if(DebugEnable) DEBUG("pingtime %s",pingtime);
			update_conf_elem(PING_TIME_CONF,pingtime);
			return E_OK;
			}
			}
	}
	return E_ERROR;
}


/*******************************************************************************
* Function Name	:	parse_sleepvoltage_message
* Description	:
*******************************************************************************/
uint8_t parse_sleepvoltage_message(int8_t* cmd)
{
	int8_t* p;
	int8_t voltage[10];
	
	p = cmd;
		
	if((u8*)strchr(p,',') != NULL){
			p= (u8*)strchr(p,',');
			p+=1;
			if((u8*)strchr(p,',') == NULL){
			if(cmd+strlen(cmd)+1-p < 10 && (cmd+strlen(cmd)+1-p) >=0){
			memset(voltage,0,10);
			memcpy(voltage,p,cmd+strlen(cmd)+1-p);
			if(DebugEnable) DEBUG("voltage %s",voltage);
			update_conf_elem(RESERVE1_CONF,voltage);
			return E_OK;
			}
			}
	}
	return E_ERROR;
}


/*******************************************************************************
* Function Name	:	parse_heartbeaton_setting
* Description	:
*******************************************************************************/
uint8_t parse_heartbeaton_message(int8_t* cmd){
	
	if(strcmp((char *)get_conf_elem(ACCSTATUSHINT_CONF),"1"))
	update_conf_elem(ACCSTATUSHINT_CONF,"1");

	return E_OK;
	
}

/*******************************************************************************
* Function Name	:	parse_heartbeatoff_setting
* Description	:
*******************************************************************************/
uint8_t parse_heartbeatoff_message(int8_t* cmd){
	
	if(strcmp((char *)get_conf_elem(ACCSTATUSHINT_CONF),"0"))
	update_conf_elem(ACCSTATUSHINT_CONF,"0");

	return E_OK;
	
}


/*******************************************************************************
* Function Name	:	parse_smson_setting
* Description	:
*******************************************************************************/
uint8_t parse_smson_message(int8_t* cmd){
	
	if(strcmp((char *)get_conf_elem(RESERVE2_CONF),"0"))
	update_conf_elem(RESERVE2_CONF,"0");

	return E_OK;
	
}


/*******************************************************************************
* Function Name	:	parse_smsoff_message
* Description	:
*******************************************************************************/
uint8_t parse_smsoff_message(int8_t* cmd){
	
	if(strcmp((char *)get_conf_elem(RESERVE2_CONF),"1"))
	update_conf_elem(RESERVE2_CONF,"1");

	return E_OK;
	
}

/*******************************************************************************
* Function Name	:	parse_ackon_message
* Description	:
*******************************************************************************/
uint8_t parse_ackon_message(int8_t* cmd){
	
	if(strcmp((char *)get_conf_elem(RESERVE5_CONF),"1"))
	update_conf_elem(RESERVE5_CONF,"1");

	return E_OK;
	
}


/*******************************************************************************
* Function Name	:	parse_ackoff_message
* Description	:
*******************************************************************************/
uint8_t parse_ackoff_message(int8_t* cmd){
	
	if(strcmp((char *)get_conf_elem(RESERVE5_CONF),"0"))
	update_conf_elem(RESERVE5_CONF,"0");

	return E_OK;
	
}


/*******************************************************************************
* Function Name	:	parse_sleepmodeon_message
* Description	:
*******************************************************************************/
uint8_t parse_sleepmodeon_message(int8_t* cmd){
	
	if(strcmp((char *)get_conf_elem(RESERVE6_CONF),"1"))
	update_conf_elem(RESERVE6_CONF,"1");

	return E_OK;
	
}


/*******************************************************************************
* Function Name	:	parse_sleepmodeon_message
* Description	:
*******************************************************************************/
uint8_t parse_sleepmode1on_message(int8_t* cmd){
	
	if(strcmp((char *)get_conf_elem(RESERVE6_CONF),"2"))
	update_conf_elem(RESERVE6_CONF,"2");

	return E_OK;
	
}


/*******************************************************************************
* Function Name	:	parse_sleepmodeoff_message
* Description	:
*******************************************************************************/
uint8_t parse_sleepmodeoff_message(int8_t* cmd){
	
	if(strcmp((char *)get_conf_elem(RESERVE6_CONF),"0"))
	update_conf_elem(RESERVE6_CONF,"0");

	return E_OK;
	
}


/*******************************************************************************
* Function Name	:	parse_jammingon_message
* Description	:
*******************************************************************************/
uint8_t parse_jammingon_message(int8_t* cmd){
	
	if(strcmp((char *)get_conf_elem(RESERVE8_CONF),"1"))
	update_conf_elem(RESERVE8_CONF,"1");

	return E_OK;
	
}


/*******************************************************************************
* Function Name	:	parse_jammingoff_message
* Description	:
*******************************************************************************/
uint8_t parse_jammingoff_message(int8_t* cmd){
	
	if(strcmp((char *)get_conf_elem(RESERVE8_CONF),"0"))
	update_conf_elem(RESERVE8_CONF,"0");

	return E_OK;
	
}

/*******************************************************************************
* Function Name	:	parse_agpson_message
* Description	:
*******************************************************************************/
uint8_t parse_agpson_message(int8_t* cmd){
	
	if(strcmp((char *)get_conf_elem(RESERVE7_CONF),"1"))
	update_conf_elem(RESERVE7_CONF,"1");

	return E_OK;
	
}


/*******************************************************************************
* Function Name	:	parse_agpsoff_message
* Description	:
*******************************************************************************/
uint8_t parse_agpsoff_message(int8_t* cmd){
	
	if(strcmp((char *)get_conf_elem(RESERVE7_CONF),"0"))
	update_conf_elem(RESERVE7_CONF,"0");

	return E_OK;
	
}

/*******************************************************************************
* Function Name	:	parse_ussdtcpudp_message
* Description	:
*******************************************************************************/
uint8_t parse_ussdtcpudp_message(int8_t* cmd)
{
	int8_t* p;
	int8_t mode[5];
	
	p = cmd;
	
	//return E_OK;
		
	if((u8*)strchr(p,',') != NULL){
			p= (u8*)strchr(p,',');
			p+=1;
			if((u8*)strchr(p,',') == NULL){
			if(cmd+strlen(cmd)+1-p < 5 && (cmd+strlen(cmd)+1-p) >=0){
			memset(mode,0,5);
			memcpy(mode,p,cmd+strlen(cmd)+1-p);
			if(DebugEnable) DEBUG("mode %s",mode);
			update_conf_elem(WORKINGMODE_CONF,mode);
			return E_OK;
			}
			}
	}
	return E_ERROR;
}


/*******************************************************************************
* Function Name	:	parse_cleandata_message
* Description	:
*******************************************************************************/
uint8_t parse_cleandata_message(int8_t* cmd)
{
	bit_map_erase_flag_all_sector();
	bit_map_erase_flag_start();
	return E_OK;
}

/*******************************************************************************
* Function Name	:	parse_protocol_message
* Description	:
*******************************************************************************/
uint8_t parse_protocol_message(int8_t* cmd)
{
	int8_t* p;
	int8_t protocol[5];
	
	p = cmd;
		
	if((u8*)strchr(p,',') != NULL){
			p= (u8*)strchr(p,',');
			p+=1;
			if((u8*)strchr(p,',') == NULL){
			if(cmd+strlen(cmd)+1-p < 5 && (cmd+strlen(cmd)+1-p) >=0){
			memset(protocol,0,5);
			memcpy(protocol,p,cmd+strlen(cmd)+1-p);
			if(DebugEnable) DEBUG("protocol %s",protocol);
			update_conf_elem(RESERVE3_CONF,protocol);
			return E_OK;
			}
			}
	}
	return E_ERROR;
}


/*******************************************************************************
* Function Name	:	parse_newhardware_message
* Description	:
*******************************************************************************/
uint8_t parse_newhardware_message(int8_t* cmd){
	
	if(strcmp((char *)get_conf_elem(RESERVE4_CONF),"0"))
	update_conf_elem(RESERVE4_CONF,"0");

	return E_OK;
	
}

/*******************************************************************************
* Function Name	:	parse_newhardware_message
* Description	:
*******************************************************************************/
uint8_t parse_oldhardware_message(int8_t* cmd){
	
	if(strcmp((char *)get_conf_elem(RESERVE4_CONF),"1"))
	update_conf_elem(RESERVE4_CONF,"1");

	return E_OK;
	
}


/*******************************************************************************
* Function Name	:	handleSmsCommand
* Description	:
*******************************************************************************/

void getSmsNumber(int8_t* cmd)
{
	int8_t* p;
	int8_t* pEnd;
	
	if(DebugEnable) DEBUG("getSmsNumber %s",cmd);

	p = (int8_t*)strchr((char*)cmd,'\"');
	p+=1;
	pEnd = (int8_t*)strchr((char*)p,'\"');
	//pEnd+=1;
	//p-=1;
	memset(szNumber,0,20);
	
	memcpy(szNumber,p,pEnd-p);
	
	if(DebugEnable) DEBUG("number:%s",szNumber);
	
}

/*******************************************************************************
* Function Name	:	isSosNumber
* Description	:
*******************************************************************************/
bool isSosNumber(int8_t* cmd)
{
	int8_t* p;
	int8_t* pEnd;
	int8_t number[20];
	
	p = (int8_t*)strchr((char*)cmd,'\"');
	if(p == NULL)
		return false;
	p+=1;
	pEnd = (int8_t*)strchr((char*)p,'\"');
	if(pEnd == NULL)
		return false;
	if(pEnd-p >0 && pEnd-p <20){
		memcpy(number,p,pEnd-p);
		number[pEnd-p] = 0;
	}
	if(DebugEnable) DEBUG("isSosNumber %s",number);
	if(strstr((char*)get_conf_elem(SOSTELENUMBER_CONF),(char*)number))
		return true;
	else
		return false;
}

/*******************************************************************************
* Function Name	:	handleSmsCommand
* Description	:
*******************************************************************************/
uint8_t handleSmsCommand(int8_t* cmd,bool smsscan,bool setup)
{
	if(DebugEnable) DEBUG("%s",cmd);
	int8_t* p = reply;
	int8_t* pEnd;
	uint8_t cmdOK=0;
	
	pSetupResponse = reply;
	
	memset(reply,0,128);
	if(strlen(cmd) < 100)
	strcpyEx(strcpyEx(strcpyEx(p,GsmIMEI()),",success,"),cmd);
	else {
	//strcpyEx(strcpyEx(strcpyEx(p,GsmIMEI()),",fail,"),cmd);
	return 0;
	}
		
	if(!strncmp((char*)cmd, "Sos:", 4) || !strncmp((char*)cmd, "sos:", 4))
			cmdOK = parse_sms_sos_setting(cmd);
	else if(!strncmp((char*)cmd, "Apn,", 4) || !strncmp((char*)cmd, "apn,", 4))
	    cmdOK = parse_apn_setting(cmd+4);
	else if(!strncmp((char*)cmd, "Ip,", 3) || !strncmp((char*)cmd, "ip,", 3))
			cmdOK = parse_server_setting(cmd+3);
	else if(!strncmp((char*)cmd, "Setupdate,", 10) || !strncmp((char*)cmd, "setupdate,", 10))
			cmdOK = parse_setupdate_setting(cmd+10);
	else if(!strncmp((char*)cmd, "Domain,", 7) || !strncmp((char*)cmd, "domain,", 7))
			cmdOK = parse_domain_setting(cmd+7);
	else if(!strncmp((char*)cmd,"onrepinterval,",14)||!strncmp((char*)cmd,"Onrepinterval,",14))
			cmdOK = parse_onrepinterval_setting(cmd);
	else if(!strncmp((char*)cmd,"offrepinterval,",15)||!strncmp((char*)cmd,"Offrepinterval,",15))
			cmdOK = parse_offrepinterval_setting(cmd);
	//else if(!strncmp((char*)cmd,"guard,",6)||!strncmp((char*)cmd,"Guard,",6))
			//cmdOK = parse_guard_setting(cmd);
	else if(!strncmp((char*)cmd,"lowbat,",7)||!strncmp((char*)cmd,"Lowbat,",7))
			cmdOK = parse_lowbat_setting(cmd);
	else if(!strncmp((char*)cmd,"urgent,",7)||!strncmp((char*)cmd,"Urgent,",7))
			cmdOK = parse_urgent_setting(cmd);
	else if(!strncmp((char*)cmd,"shock,",6)||!strncmp((char*)cmd,"Shock,",6))
			cmdOK = parse_shock_setting(cmd);
	else if(!strncmp((char*)cmd,"overspeed,",10)||!strncmp((char*)cmd,"Overspeed,",10))
			cmdOK = parse_overspeed_setting(cmd+10);
	
	else if(!strcmp((char*)cmd,"arm")||!strcmp((char*)cmd,"Arm"))
			cmdOK = parse_arm_setting(reply);
	else if(!strcmp((char*)cmd,"disarm")||!strcmp((char*)cmd,"Disarm"))
			cmdOK = parse_disarm_setting(reply);
	
	else if(!strcmp((char*)cmd,"igkillon")||!strcmp((char*)cmd,"Igkillon"))
			cmdOK = parse_igkillon_setting(reply);
	
	else if(!strcmp((char*)cmd,"igkilloff")||!strcmp((char*)cmd,"Igkilloff"))
			cmdOK = parse_igkilloff_setting(reply);
	
	else if(!strcmp((char*)cmd,"swon")||!strcmp((char*)cmd,"Swon"))
			cmdOK = parse_swon_setting(reply);
	
	else if(!strcmp((char*)cmd,"swoff")||!strcmp((char*)cmd,"Swoff"))
			cmdOK = parse_swoff_setting(reply);
	
	else if(!strcmp((char*)cmd,"cellon")||!strcmp((char*)cmd,"Cellon"))
			cmdOK = parse_cellon_setting(reply);
	
	else if(!strcmp((char*)cmd,"celloff")||!strcmp((char*)cmd,"Celloff"))
			cmdOK = parse_celloff_setting(reply);
	
	else if(!strcmp((char*)cmd,"sirenon")||!strcmp((char*)cmd,"Sirenon"))
			cmdOK = parse_sirenon_setting(reply);
	
	else if(!strcmp((char*)cmd,"sirenoff")||!strcmp((char*)cmd,"Sirenoff"))
			cmdOK = parse_sirenoff_setting(reply);
	
	else if(!strcmp((char*)cmd,"out1on")||!strcmp((char*)cmd,"Out1on"))
			cmdOK = parse_out1on_setting(reply);
	
	else if(!strcmp((char*)cmd,"out1off")||!strcmp((char*)cmd,"Out1off"))
			cmdOK = parse_out1off_setting(reply);
	
	else if(!strcmp((char*)cmd,"out2on")||!strcmp((char*)cmd,"Out2on"))
			cmdOK = parse_sirenon_setting(reply);
	
	else if(!strcmp((char*)cmd,"out2off")||!strcmp((char*)cmd,"Out2off"))
			cmdOK = parse_sirenoff_setting(reply);
	
	else if(!strcmp((char*)cmd,"gpsfixed")||!strcmp((char*)cmd,"Gpsfixed"))
			cmdOK = parse_gpsfixed_setting(reply);
	
	else if(!strcmp((char*)cmd,"gpslost")||!strcmp((char*)cmd,"Gpslost"))
			cmdOK = parse_gpslost_setting(reply);
	
	else if(!strncmp((char*)cmd,"timezone,",9)||!strncmp((char*)cmd,"Timezone,",9))
			cmdOK = parse_timezone_setting(cmd);
	
	else if(!strncmp((char*)cmd,"update",6)||!strncmp((char*)cmd,"Update",6))
			cmdOK = parse_update_message(cmd);
	
	else if(!strncmp((char*)cmd,"simulateacc,",12)||!strncmp((char*)cmd,"Simulateacc,",12))
			cmdOK = parse_simulateacc_message(cmd);
	
	else if(!strncmp((char*)cmd,"angle,",6)||!strncmp((char*)cmd,"Angle,",6))
	    cmdOK = parse_direction_message(cmd);
	
	else if(!strncmp((char*)cmd,"sensor,",7)||!strncmp((char*)cmd,"Sensor,",7))
	    cmdOK = parse_sensor_message(cmd);
	
	else if(!strncmp((char*)cmd,"reportmode,",11)||!strncmp((char*)cmd,"Reportmode,",11))
	    cmdOK = parse_report_message(cmd);
	
	else if(!strncmp((char*)cmd,"workmode,",9)||!strncmp((char*)cmd,"Workmode,",9))
				cmdOK = parse_WorkMode_setting(cmd+9);
	
	else if(!strncmp((char*)cmd,"distance,",9)||!strncmp((char*)cmd,"Distance,",9))
	    cmdOK = parse_distance_message(cmd);
	
	else if(!strncmp((char*)cmd,"speedalarmtime,",15)||!strncmp((char*)cmd,"Speedalarmtime,",15))
	    cmdOK = parse_speedtime_message(cmd);
	
	else if(!strncmp((char*)cmd,"pingtime,",9)||!strncmp((char*)cmd,"Pingtime,",9))
	    cmdOK = parse_pingtime_message(cmd);
	
	else if(!strncmp((char*)cmd,"sleepvoltage,",13)||!strncmp((char*)cmd,"Sleepvoltage,",13))
	    cmdOK = parse_sleepvoltage_message(cmd);
	
	else if(!strcmp((char*)cmd,"heartbeaton")||!strcmp((char*)cmd,"Heartbeaton"))
	    cmdOK = parse_heartbeaton_message(cmd);
	
	else if(!strcmp((char*)cmd,"heartbeatoff")||!strcmp((char*)cmd,"Heartbeatoff"))
	    cmdOK = parse_heartbeatoff_message(cmd);
	
	else if(!strcmp((char*)cmd,"smson")||!strcmp((char*)cmd,"Smson"))
	    cmdOK = parse_smson_message(cmd);
	
	else if(!strcmp((char*)cmd,"smsoff")||!strcmp((char*)cmd,"Smsoff"))
	    cmdOK = parse_smsoff_message(cmd);
	
	else if(!strcmp((char*)cmd,"ackon")||!strcmp((char*)cmd,"Ackon"))
	    cmdOK = parse_ackon_message(cmd);
	
	else if(!strcmp((char*)cmd,"ackoff")||!strcmp((char*)cmd,"Ackoff"))
	    cmdOK = parse_ackoff_message(cmd);
	
	else if(!strcmp((char*)cmd,"sleepmodeon")||!strcmp((char*)cmd,"Sleepmodeon"))
	    cmdOK = parse_sleepmodeon_message(cmd);
	
	else if(!strcmp((char*)cmd,"sleepmode1on")||!strcmp((char*)cmd,"Sleepmode1on"))
	    cmdOK = parse_sleepmode1on_message(cmd);
	
	else if(!strcmp((char*)cmd,"sleepmodeoff")||!strcmp((char*)cmd,"Sleepmodeoff"))
	    cmdOK = parse_sleepmodeoff_message(cmd);
	
	else if(!strcmp((char*)cmd,"jammingon")||!strcmp((char*)cmd,"Jammingon"))
	    cmdOK = parse_jammingon_message(cmd);
	
	else if(!strcmp((char*)cmd,"jammingoff")||!strcmp((char*)cmd,"Jammingoff"))
	    cmdOK = parse_jammingoff_message(cmd);
	
	else if(!strcmp((char*)cmd,"agpson")||!strcmp((char*)cmd,"Agpson"))
	    cmdOK = parse_agpson_message(cmd);
	
	else if(!strcmp((char*)cmd,"agpsoff")||!strcmp((char*)cmd,"Agpsoff"))
	    cmdOK = parse_agpsoff_message(cmd);
	
	
	else if(!strncmp((char*)cmd,"ussdtcpudp,",11)||!strncmp((char*)cmd,"Ussdtcpudp,",11))
	    cmdOK = parse_ussdtcpudp_message(cmd);
	
	else if(!strcmp((char*)cmd,"newhardware")||!strcmp((char*)cmd,"Newhardware"))
	    cmdOK = parse_newhardware_message(cmd);
	
	else if(!strcmp((char*)cmd,"oldhardware")||!strcmp((char*)cmd,"Oldhardware"))
	    cmdOK = parse_oldhardware_message(cmd);
	
	else if(!strncmp((char*)cmd,"protocol,",9)||!strncmp((char*)cmd,"Protocol,",9))
	    cmdOK = parse_protocol_message(cmd);
	
	else if(!strcmp((char*)cmd,"cleandata")||!strcmp((char*)cmd,"Cleandata"))
	    cmdOK = parse_cleandata_message(cmd);
	
	//else if(!strncmp((char*)cmd, "poweroff", 8) || !strncmp((char*)cmd, "Poweroff", 8))
			//cmdOK = parse_poweroff_setting(cmd);
	
	else if(!strcmp((char*)cmd,"parameter")||!strcmp((char*)cmd,"Parameter"))
			cmdOK = parse_parameter_message(reply,setup);
	else if(!strcmp((char*)cmd,"status")||!strcmp((char*)cmd,"Status"))
			cmdOK = parse_status_message(reply,setup);
	
	else if(!strncmp((char*)cmd,"enginehours,",12)||!strncmp((char*)cmd,"Enginehours,",12))
			cmdOK = parse_eh_setting(cmd);
	else if(!strncmp((char*)cmd,"mileage,",8)||!strncmp((char*)cmd,"mileage,",8))
			cmdOK = parse_ml_setting(cmd);
	else if(!strcmp((char*)cmd,"C")||!strcmp((char*)cmd,"c"))
			cmdOK = parse_position_message(reply);
	else if(!strcmp((char*)cmd, "Reset") || !strcmp((char*)cmd, "reset")){
			if(!smsscan){
			if(!setup){
			gsmSendSMS(szNumber,reply);
			}
	    SystemRestart();
			return 1;
			}else
			{
			return 2;
			}
	}
	else if(!strcmp((char*)cmd, "Factory") || !strcmp((char*)cmd, "factory")){
		if(!smsscan){
			if(!setup){
			gsmSendSMS(szNumber,reply);
			}
	    parse_recover_setting(cmd);
			SystemRestart();
			return 1;
			}else
			{
			parse_recover_setting(cmd);
			return 2;
			}
	}
	else{
			strcpyEx(strcpyEx(strcpyEx(p,GsmIMEI()),",fail,"),cmd);
		  return 0;
	}
	
	if(setup)
		return 1;
	
	if(!smsscan && cmdOK == E_OK){
	gsmSendSMS(szNumber,reply);
	}
	if(smsscan && cmdOK == E_OK){
		if(DebugEnable) DEBUG("gsmSenderDAnumber() %s",gsmSenderDAnumber());
		if(strncmp(gsmSenderDAnumber(),"null",strlen("null"))){
			getSmsNumber(gsmSenderDAnumber());
			if(DebugEnable) DEBUG("szNumber %s",szNumber);
			gsmSendSMS(szNumber,reply);
			memset(gsmSenderDAnumber(),0,20);
			memcpy(gsmSenderDAnumber(),"null",4);
		}
		else{
	if(strncmp(get_conf_elem(SOSTELENUMBER_CONF),"null",strlen(get_conf_elem(SOSTELENUMBER_CONF))) 
		&& strncmp(get_conf_elem(SOSTELENUMBER_CONF),"008613000000000",strlen("008613000000000")) )
				gsmSendSMS(get_conf_elem(SOSTELENUMBER_CONF),reply);
		}
	}
	
	return 1;
	
}

void ota_update_sms_notify(void)
{
		int8_t notify[30];
		
		if(strlen(szNumber) > 0){
		memset(notify,0,30);
		strcat(notify,GsmIMEI());
		strcat(notify," update fail");
		gsmSendSMS(szNumber,notify);
		memset(szNumber,0,20);
		}
}


/*******************************************************************************
* Function Name	:	ggFindCommand
* Description	:
*******************************************************************************/
const GgCommand_t * ggFindCommand(int8_t *cmd, const GgCommand_t * cmds, int size)
{
	while (cmds->Command != NULL)
	{
		if (strncmp((char *)cmd, (char *)cmds->Command, size) == 0)
			return cmds;
		cmds++;
	}
	return NULL;
}

/*******************************************************************************
* Function Name	:	CheckGGCommand
* Description	:	Compare and remove checksum from line
*******************************************************************************/
uint8_t GG_CheckCommand(int8_t * cmd)
{
	uint8_t cs;
	int8_t * pos;
	int len;

	len = strlen((char*)(char *)cmd);
	if (len > 10)
	{
		pos =(int8_t *) strchr((char*)(char *)cmd, '*');
		if (pos != NULL && ((pos - cmd) == (len - 3)))
		{
			cs = nmea_atoi((char *)pos + 1, 2, 16);
			*pos = 0;
			if (cs == nmea_calc_crc((char *)cmd + 1, len - 4))
				return E_OK;
		}
	}
	return E_BAD_COMMAND;
}

/*******************************************************************************
* Function Name	:	ProcessGpsGateCommand
* Description	:
*******************************************************************************/
const GgCommand_t * ProcessGpsGateCommand(int8_t * text, bool exec)
{
	int size;
	uint8_t state = 0;
	const GgCommand_t * gg_cmd = NULL;

	while (text != NULL)
	{
		size = TokenSizeComma(text);
		if (state == 0)
		{	// Check for checksum and command
			if (size == 6 && GG_CheckCommand(text) == E_OK)
			{
				if (strncmp((char *)text, "$FRRET", size) == 0)
					gg_cmd = &GgReplayMap[0];
				else if (strncmp((char *)text, "$FRCMD", size) == 0)
					gg_cmd = &GgCommandMap[0];
				else if (strncmp((char *)text, "$FRSES", size) == 0)
					gg_cmd = &GgCommandMap[0];
			}
			resetIndex = handleSmsCommand(text,true,false);
			if (gg_cmd == NULL)
				break;
		}
		else if (state == 1)
		{	// Check IMEI code from command and module
			if (size == 0 || strncmp((char *)text, (char *)GsmIMEI(), size) != 0)
				break;
		}
		else if (state == 2)
		{	// Find command and execute handler
			if (size != 0
			&&	(gg_cmd = ggFindCommand(text, gg_cmd, size)) != NULL
				)
			{
				text = TokenNextComma(text);	// skip command name
				text = TokenNextComma(text);	// skip inline
				if ((gg_cmd->Handler)(text, gg_cmd, exec) == E_OK)
					return gg_cmd;
			}
			break;
		}

		text = TokenNextComma(text);
		++state;
	}

	return NULL;
}

const GgCommand_t * setupBysms_toSync(void)
{
		const GgCommand_t * gg_cmd = NULL;
		gg_cmd = &GgCommandMap[0];
		gg_cmd = ggFindCommand("_Sync", gg_cmd, 5);
	  return gg_cmd;
}

void initGprsParameter(void)
{
  int size = 0;
	size = strlen((char*)get_conf_elem(APNNAME_CONF));
	strncpy((char *)SET_GRPS_APN_NAME, (char *)get_conf_elem(APNNAME_CONF), size);
	SET_GRPS_APN_NAME[size] = 0;
	
	size = strlen((char*)get_conf_elem(APNUSER_CONF));
	strncpy((char *)SET_GPRS_USERNAME, (char *)get_conf_elem(APNUSER_CONF), size);
	SET_GPRS_USERNAME[size] = 0;
	
	size = strlen((char*)get_conf_elem(APNPASS_CONF));
	strncpy((char *)SET_GPRS_PASSWORD, (char *)get_conf_elem(APNPASS_CONF), size);
	SET_GPRS_PASSWORD[size] = 0;
	
	size = strlen((char*)get_conf_elem(MAINSERVERIPADDR_CONF));
	strncpy((char *)SET_HOST_ADDR, (char *)get_conf_elem(MAINSERVERIPADDR_CONF), size);
	SET_HOST_ADDR[size] = 0;
	
	size = strlen((char*)get_conf_elem(MAINSERVERPORTNUMBER_CONF));
	strncpy((char *)SET_HOST_PORT, (char *)get_conf_elem(MAINSERVERPORTNUMBER_CONF), size);
	SET_HOST_PORT[size] = 0;
	
	size = strlen((char*)get_conf_elem(MAINSERVERDOMAINNAME_CONF));
	strncpy((char *)SET_HOST_DOMAIN, (char *)get_conf_elem(MAINSERVERDOMAINNAME_CONF), size);
	SET_HOST_DOMAIN[size] = 0;

}

void gpsOneDebugEnable(bool enable)
{
	DebugEnable = enable;
}
