//#include"includes.h"
#include "timer.h"
#include <stdint.h>
#include "main.h"
#include "debug.h"

//温度测量 中断延时不能用

volatile uint16_t  time_delay;

/*假设系统时钟=72Mhz
 *((1+TIM_Prescaler )/72M)*(1+TIM_Period )=((1+35999)/72M)*(1+1999)=1秒
 */

/*假设系统时钟=12Mhz
 *((1+TIM_Prescaler )/12M)*(1+TIM_Period )=((1+6999)/12M)*(1+1999)=1秒
 */
 
TIM_HandleTypeDef htim6;

uint8_t timer_init(void)
{
 #if 0
 TIM_TimeBaseInitTypeDef TIM_TimeBaseInitStruct;

 RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM2, ENABLE);//打开TIM2外时钟
 
 TIM_DeInit(TIM2);             //初始化TIM2为缺省值
 TIM_TimeBaseInitStruct.TIM_Period = 1;      //配置ARR自动重载寄存器           1us
 TIM_TimeBaseInitStruct.TIM_Prescaler = 72;        //配置PSC时钟预分频        1us
 TIM_TimeBaseInitStruct.TIM_ClockDivision = TIM_CKD_DIV1;   //配置时间分割值
 TIM_TimeBaseInitStruct.TIM_CounterMode = TIM_CounterMode_Down;     //配置计数器向下计算
 TIM_TimeBaseInit(TIM2, &TIM_TimeBaseInitStruct);     //初始化
 #endif

	htim6.Instance = TIM6;
  htim6.Init.Prescaler = 0;
  htim6.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim6.Init.Period = 65535;
  htim6.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
	if(HAL_TIM_Base_DeInit(&htim6) != HAL_OK)
		return 0;
  if(HAL_TIM_Base_Init(&htim6) != HAL_OK)
		return 0;
	
	return 1;

}


/************************************ 计数式延时 ****************************************************/
void Delay_Nms( uint16_t nCount)
{
	#if 0
  uint16_t TIMCounter =( nCount*1000);  //TIMCounter(0-65535)      //当 nCount =10~3时，精确延时1ms。 ( 1MHz )

  TIM_Cmd(TIM2, ENABLE); //使能TIM2定时器
  TIM_SetCounter(TIM2, TIMCounter);
  while (TIMCounter>1){
    TIMCounter = TIM_GetCounter(TIM2);
  }
  TIM_Cmd(TIM2, DISABLE);
	
	unsigned int i, j;
	for ( i = 1000 ; i > 0 ; i -- ) 
	    for ( j = (1000 * 1000 /108) ; j > 0 ; j --) ; 

	uint16_t i;
  while(nCount--)
  {
    i=2400;
    while(i--);
  }
	
	uint16_t i;
	while(nCount--)
  {
    i=10;
    while(i--)
		{
		Delay_Nus(100);
		}
  }
	
	#endif
	
	//LL_mDelay(nCount) ;
	uint16_t TIMCounter = 24000 * nCount ;
	__HAL_TIM_SetCounter(&htim6, 0);//htim17 ????????? ???

  __HAL_TIM_ENABLE(&htim6);        
  while(__HAL_TIM_GetCounter(&htim6) < TIMCounter);//48??????? ????? ????? ??? 48????
  /* Disable the Peripheral */
  __HAL_TIM_DISABLE(&htim6);
	
}


void Delay_Nus(uint16_t nCount) //nCount最好大于1
{
	#if 0
  uint16_t TIMCounter = nCount;  //(0-65535)  
  
  TIM_Cmd(TIM2, ENABLE); //使能TIM2定时器
  TIM_SetCounter(TIM2, TIMCounter);
  while (TIMCounter>1){
    TIMCounter = TIM_GetCounter(TIM2);
  }
  TIM_Cmd(TIM2, DISABLE);
	
	uint32_t i, j;
       for ( i = nCount ; i > 0 ; i --) {
            for (j= (1000/128) ; j > 0 ; j --) ;  // this instruction about 1us
       }
	
	uint16_t i;
  while(nCount--)
  {
    i= 2;
    while(i--);
  }
	
	uint8_t i;

  while(nCount)
  {    
    for (i = 0; i < 10; i++)
    {
      
    }
    nCount--;
  }
			 
	#endif
	#if 1
	uint16_t TIMCounter = 24 * nCount;
	__HAL_TIM_SetCounter(&htim6, 0);//htim17 ????????? ???

  __HAL_TIM_ENABLE(&htim6);        
  while(__HAL_TIM_GetCounter(&htim6) < TIMCounter);//48??????? ????? ????? ??? 48????
  /* Disable the Peripheral */
  __HAL_TIM_DISABLE(&htim6);
	#endif
	
}

/********************************************************************************************************/



