#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <math.h>
#include "hardware.h"
#include "main.h"
#include "debug.h"
#include "gps.h"
#include "gsm.h"
#include "ring_buffer.h"
#include "work.h"
#include "config_file.h"
#include "bitmap_flash.h"
#include "Ds18b20.h"

#define SHELL_CMD_BUFFER_LEN	128

#define SHELL_AT_BIGGER_COMMAND       "AT"
#define SHELL_AT_LOWER_COMMAND        "at"
#define SHELL_AT_PREFIX_LENGTH             2

#define SHELL_STAND_BY_COMMAND        "standby"
#define SHELL_STAND_BY_LENGTH             7

#define SHELL_SHOW_CONFIG_COMMAND        "sconfig"
#define SHELL_SHOW_CONFIG_LENGTH             7

#define SHELL_REBOOT_COMMAND        "reboot"
#define SHELL_REBOOT_LENGTH             6

#define SHELL_STOP_COMMAND        "stop"
#define SHELL_STOP_LENGTH             4

#define SHELL_DELETE_CONFIG_COMMAND        "dconfig"
#define SHELL_DELETE_CONFIG_LENGTH             7

#define SHELL_OPEN_GPS_LOG_COMMAND        "ogps"
#define SHELL_OPEN_GPS_LOG_LENGTH   				4

#define SHELL_CLOSE_GPS_LOG_COMMAND        "cgps"
#define SHELL_CLOSE_GPS_LOG_LENGTH   				4

#define SHELL_TEST_MODE_COMMAND        "t"
#define SHELL_TEST_MODE_LENGTH   				1

#define SHELL_RELAY_1_COMMAND        "1"
#define SHELL_RELAY_LENGTH   				1

#define SHELL_RELAY_2_COMMAND        "2"

#define SHELL_3_COMMAND        "3"

#define SHELL_MIC_COMMAND        "m"
#define SHELL_MIC_LENGTH   				1

#define SHELL_NORMAL_COMMAND        "n"
#define SHELL_NORMAL_LENGTH   				1

#define SHELL_MIC_CLOSE_COMMAND        "o"
#define SHELL_MIC_LENGTH   				1

#define SHELL_ENGINEER_COMMAND       "engineer enter"
#define SHELL_ENGINEER_LENGTH              14

#define SHELL_ENGINEEREXIT_COMMAND       "engineer exit"
#define SHELL_ENGINEEREXIT_LENGTH              13

#define SHELL_IMEI_COMMAND        "engineer get sn"
#define SHELL_IMEI_LENGTH             15

#define SHELL_ICCID_COMMAND        "engineer get iccid"
#define SHELL_ICCID_LENGTH             18

#define SHELL_GETAPNNAME_COMMAND        "engineer get apnname"
#define SHELL_GETAPNNAME_LENGTH             20

#define SHELL_SETAPNNAME_COMMAND        "engineer set apnname"
#define SHELL_SETAPNNAME_LENGTH             20

#define SHELL_GETAPNUSER_COMMAND        "engineer get apnuser"
#define SHELL_GETAPNUSER_LENGTH             20

#define SHELL_SETAPNUSER_COMMAND        "engineer set apnuser"
#define SHELL_SETAPNUSER_LENGTH             20

#define SHELL_GETAPNPASS_COMMAND        "engineer get apnpass"
#define SHELL_GETAPNPASS_LENGTH             20

#define SHELL_SETAPNPASS_COMMAND        "engineer set apnpass"
#define SHELL_SETAPNPASS_LENGTH             20

#define SHELL_QUERYMODE_COMMAND        "query mode"
#define SHELL_QUERYMODE_LENGTH             10


#define SHELL_FACTORY_COMMAND        "factory"
#define SHELL_FACTORY_LENGTH             7


#define SHELL_CLEANDATA_COMMAND        "cleandata"
#define SHELL_CLEANDATA_LENGTH             9



#define SHELL_CONF_SET_COMMAND         "conf set "
#define SHELL_CONF_LENGTH            9

#define SHELL_CONF_GET_COMMAND        "conf get "

#define SHELL_NEW_GPIO_COMMAND  "g"
#define SHELL_NEW_GPIO_LENGTH            1

#define SHELL_RS232DEBUG_COMMAND        "debug"
#define SHELL_RS232DEBUG_LENGTH             5

#define SHELL_JAMMINGON_COMMAND        "jammingon"
#define SHELL_JAMMINGON_LENGTH             9

#define SHELL_JAMMINGOFF_COMMAND        "jammingoff"
#define SHELL_JAMMINGOFF_LENGTH             10

#define SHELL_GPSHOTSTART_COMMAND        "gpshot"
#define SHELL_GPSHOTSTART_LENGTH             6

#define SHELL_GPSWARMSTART_COMMAND        "gpswarm"
#define SHELL_GPSWARMSTART_LENGTH             7

#define SHELL_GPSCOLDSTART_COMMAND        "gpscold"
#define SHELL_GPSCOLDSTART_LENGTH             7

#define TUB     "*XD"



#define ARRAY_SIZE(a)	(sizeof(a)/sizeof(a[0]))
	
#define SHELL_PROMPT_STRING	"\r\ntracker# "


extern bool gpstestmode;
extern bool gsmtestmode;
extern bool gsensortestmode;
extern bool worktestmode;
extern RTC_HandleTypeDef RTCHandle;
extern uint8_t ota[100];

bool testmode = false;
bool newGpio = true;
bool rs232Debug = false;

#if   ((USE_USB == 1) && (USE_USB_DEBUG == 1))
	#include "usbd_cdc_vcp.h"
#endif
	
#if	( USE_SERIAL_DEBUG == 1)
	
#ifdef __GNUC__
/* With GCC/RAISONANCE, small printf (option LD Linker->Libraries->Small printf
   set to 'Yes') calls __io_putchar() */
#define PUTCHAR_PROTOTYPE int __io_putchar(int ch)
#else
#define PUTCHAR_PROTOTYPE int fputc(int ch, FILE *f)
#endif /* __GNUC__ */
	
/**
  * @brief  Retargets the C library printf function to the USART.
  * @param  None
  * @retval None
  */
PUTCHAR_PROTOTYPE
{
  /* Place your implementation of fputc here */
  /* e.g. write a character to the USART2 and Loop until the end of transmission */
	
	//while(HAL_UART_Transmit(&debugUartHandle, (uint8_t *)&ch, 1,0xFFFF) != HAL_OK){
	//}
	
	LL_USART_TransmitData8(USART2, ch)  ;
       while (READ_BIT(USART2->ISR, USART_ISR_TXE) == RESET) ; 
	
	if(testmode || rs232Debug){
	//while(HAL_UART_Transmit(&lpUartHandle, (uint8_t *)&ch, 1,0xFFFF) != HAL_OK){
	//}
			LL_USART_TransmitData8(LPUART1, ch)  ;
       while (READ_BIT(LPUART1->ISR, USART_ISR_TXE) == RESET) ; 
	}
  return ch;
}

#endif

struct {
	char buf[SHELL_CMD_BUFFER_LEN];
	unsigned int index;
	unsigned int match;
} shell; 

static struct shell_command commands[] = {
	{ "help",			 shell_help },

} ;


static char is_whitespace(char c)
{
	return (c == ' ' || c == '\t' || c == '\r' || c == '\n');
}

static void shell_help(int argc, char **argv)
{
	
}

bool NewGpioTest(void)
{
		return newGpio;
}


	/*******************************************************************************
	* Function Name	:	DebugInit
	* Description	:
	* Input			:
	* Return		:
	*******************************************************************************/
void DebugInit(void)
{
	#if 0
	uint8_t c;
	
	debugUartHandle.Instance = DEBUG_USART;
	debugUartHandle.Init.BaudRate   = 115200;
	debugUartHandle.Init.WordLength = UART_WORDLENGTH_8B;
	debugUartHandle.Init.StopBits   = UART_STOPBITS_1;
	debugUartHandle.Init.Parity     = UART_PARITY_NONE;
	debugUartHandle.Init.HwFlowCtl  = UART_HWCONTROL_NONE;
	debugUartHandle.Init.Mode       = UART_MODE_TX_RX;
		
	if (HAL_UART_DeInit(&debugUartHandle) != HAL_OK)
  {
    /* Initialization Error */
  }
	if (HAL_UART_Init(&debugUartHandle) != HAL_OK)
  {
    /* Initialization Error */
  }
	
	HAL_UART_Receive_IT(&debugUartHandle,(uint8_t *)&c,RXBUFFERSIZE);
	#endif
	
	MX_USART2_UART_Init();
}

static void prompt(void)
{
	if(!testmode)
	worktestmode = false;
	gsmDebugEnable(false);
	gpsDataLogEnable(false);
	ds18b20DebugEnable(false);
	//HAL_UART_Transmit_IT(&debugUartHandle,SHELL_PROMPT_STRING,11);
	USART_SEND_DATAS(SHELL_PROMPT_STRING,DEBUG_USART);
}


void putch(uint8_t data)
{
	//HAL_UART_Transmit_IT(&debugUartHandle, (uint8_t *)&data, 1);
	USART_SEND_DATA(data,DEBUG_USART);

	LL_USART_TransmitData8(LPUART_USART, data)  ;
}


void uart_puts( char *s)
{
  	while (*s)
    	putch(*s++);
}


static void shell_testgprs(int argc, char **argv)
{
	 gsmATDebugEnable(true);
	 strcat(argv[0],"\r\n");
	 atCommand((int8_t*)argv[0]);
}

static void shell_teststandby(int argc, char **argv)
{
	setEvent(GSENSOR_SLEEP_EVENT);
}


static void shell_teststop(int argc, char **argv)
{
	setEvent(GSENSOR_SLEEP_EVENT);
}

static void shell_testreboot(int argc, char **argv)
{
	if(HAL_RTCEx_BKUPRead(&RTCHandle,RTC_BKP_DR3) == 1)
		HAL_RTCEx_BKUPWrite(&RTCHandle,RTC_BKP_DR3,0);
	SystemRestart();
}


static void shell_testsconfig(int argc, char **argv)
{
	show_sw_conf();
}

static void shell_testdconfig(int argc, char **argv)
{
	write_eh_data(0);
	write_ml_data(0);
	write_out1_status(0);
	write_out2_status(0);
	bit_map_erase_flag_all_sector();
	bit_map_erase_flag_start();
	delete_sw_conf();
}

static void shell_opengpslog(int argc, char **argv)
{
	gpsDataLogEnable(true);
}

static void shell_closegpslog(int argc, char **argv)
{
	gpsDataLogEnable(false);
}

static void shell_cleandata(int argc, char **argv)
{
	DEBUG("clean data");
	bit_map_erase_flag_all_sector();
	bit_map_erase_flag_start();
	bit_map_erase_agps_start();
}

void shell_relay_1(int argc, char **argv)
{
	#if 0
	GPIO_InitTypeDef  GPIO_InitStruct;
	//PA5
	__HAL_RCC_GPIOA_CLK_ENABLE();
	GPIO_InitStruct.Pin       = GPIO_PIN_5;
  GPIO_InitStruct.Mode      = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull      = GPIO_NOPULL;
  GPIO_InitStruct.Speed     = GPIO_SPEED_FREQ_VERY_HIGH;
	
	HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

	if(HAL_GPIO_ReadPin(GPIOA,GPIO_PIN_5) == GPIO_PIN_RESET)
	HAL_GPIO_WritePin(GPIOA, GPIO_PIN_5, GPIO_PIN_SET);
	else
	HAL_GPIO_WritePin(GPIOA, GPIO_PIN_5, GPIO_PIN_RESET);
	#endif
	if(getOut1Statue())
	stop_pwm_out1();
	else
	start_pwm_out1();
	DEBUG("RELAY 1 TEST");
}


void shell_relay_2(int argc, char **argv)
{
	#if 0
	GPIO_InitTypeDef  GPIO_InitStruct;
	//PC8
	__HAL_RCC_GPIOC_CLK_ENABLE();
	GPIO_InitStruct.Pin       = GPIO_PIN_8;
  GPIO_InitStruct.Mode      = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull      = GPIO_NOPULL;
  GPIO_InitStruct.Speed     = GPIO_SPEED_FREQ_VERY_HIGH;
	
	HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);
	
	if(HAL_GPIO_ReadPin(GPIOC,GPIO_PIN_8) == GPIO_PIN_RESET)
	HAL_GPIO_WritePin(GPIOC, GPIO_PIN_8, GPIO_PIN_SET);
	else
	HAL_GPIO_WritePin(GPIOC, GPIO_PIN_8, GPIO_PIN_RESET);
	#endif
	if(getOut2Statue())
	stop_pwm_out2();
	else
	start_pwm_out2();
	DEBUG("RELAY 2 TEST");
	
}

void shell_3(int argc, char **argv)
{
	//update_conf_elem(APNNAME_CONF,"bhm2m");
	//DEBUG("apnname:%s",get_conf_elem(APNNAME_CONF));
}


static void shell_test_mic(int argc, char **argv)
{
		gsmATDebugEnable(true);
		atCommand("AT+QAUDLOOP=1,0\r\n");
}

static void shell_test_mic_close(int argc, char **argv)
{
		gsmATDebugEnable(true);
		atCommand("AT+QAUDLOOP=0,0\r\n");
}

static void shell_new_gpio(int argc, char **argv)
{
	if(!newGpio)
	newGpio = true;
	else
	newGpio = false;	
}

static void shell_rs232_debug(int argc, char **argv)
{
	if(!rs232Debug)
	rs232Debug = true;
  else
  rs232Debug = false;	
	gsmDebugEnable(true);
	gpsDebugEnable(true);
	workDebugEnable(true);
	gpsOneDebugEnable(true);
	mainOneDebugEnable(true);
	gsenssorDebugEnable(true);
	configFileDebugEnable(true);
	bitmapEnable(true);
	ds18b20DebugEnable(true);
	gpsDataLogEnable(false);
}

static void shell_jamming_on(int argc, char **argv)
{
	jammingEvent();
	start_pwm_out1();
	start_pwm_out2();
}

static void shell_jamming_off(int argc, char **argv)
{
	stop_pwm_out1();
	stop_pwm_out2();
}

static void shell_gpshotstart(int argc, char **argv)
{
	CFC_HOT_RST();
}

static void shell_gpswarmstart(int argc, char **argv)
{
	CFC_WARM_RST();
}

static void shell_gpscoldstart(int argc, char **argv)
{
	CFC_COLD_RST();
}


static void shell_conf_set(int argc, char **argv)
{
		uint8_t conf_name[20] = {0};
	  uint8_t *p;
		uint8_t *pEnd;
    p =  argv[0]+SHELL_CONF_LENGTH;
	  pEnd = (int8_t*) strchr((char *)p,' ');
	  if(pEnd == NULL)
			return;
		memcpy(conf_name,p,pEnd-p);
		pEnd+=1;
		
		if(!strncmp(conf_name,"ota",3) && !strncmp(pEnd,"boot",4))
		{
		printf("\r\n%s %s \r\n",conf_name,pEnd);
		GG_FwSmsIndex = -1;
		GG_FwFileName[0]='U';
		memset(ota,0,100);
		memcpy(ota,"gps.scjtracker.com:firmware:123456:/var/www/tracker_srv/file_srv/firmware:iap.bin",
						strlen("gps.scjtracker.com:firmware:123456:/var/www/tracker_srv/file_srv/firmware:iap.bin"));
		return;
		}else if (!strncmp(conf_name,"ota",3) && !strncmp(pEnd,"app",3))
		{
		printf("\r\n%s %s \r\n",conf_name,pEnd);
		GG_FwSmsIndex = -1;
		GG_FwFileName[0]='U';
		memset(ota,0,100);
		memcpy(ota,"gps.scjtracker.com:firmware:123456:/var/www/tracker_srv/file_srv/firmware:te103t.bin",
						strlen("gps.scjtracker.com:firmware:123456:/var/www/tracker_srv/file_srv/firmware:te103t.bin"));
		return;
		}else if (!strncmp(conf_name,"ota",3) && !strncmp(pEnd,"te103boot",9))
		{
		printf("\r\n%s %s \r\n",conf_name,pEnd);
		GG_FwSmsIndex = -1;
		GG_FwFileName[0]='U';
		memset(ota,0,100);
		memcpy(ota,"gps.scjtracker.com:firmware:123456:/var/www/tracker_srv/file_srv/firmware:te103boot.bin",
						strlen("gps.scjtracker.com:firmware:123456:/var/www/tracker_srv/file_srv/firmware:te103boot.bin"));
		return;
		}
		
		printf("\r\n configing \r\n");
		update_conf_elem(conf_name,pEnd);
		printf("\r\n%s %s ok\r\n",conf_name,pEnd);
}

static void shell_conf_get(int argc, char **argv)
{	
		printf("\r\n  %s \r\n",get_conf_elem(argv[0]+SHELL_CONF_LENGTH));
}

static void shell_test_engineer (int argc, char **argv)
{
	gsmDebugEnable(false);
	gpsDebugEnable(false);
	workDebugEnable(false);
	gpsOneDebugEnable(false);
	mainOneDebugEnable(false);
	gsenssorDebugEnable(false);
	configFileDebugEnable(false);
	gpsDataLogEnable(false);
	bitmapEnable(false);
	ds18b20DebugEnable(false);
	printf("ok\r\n");
	//HAL_UART_Transmit(&lpUartHandle, "ok\r\n", 4,1000);
	USART_SEND_DATAS("ok\r\n",LPUART_USART);
}

static void shell_test_engineerexit(int argc, char **argv)
{
	USART_SEND_DATAS("ok\r\n",LPUART_USART);
}


static void shell_test_imei (int argc, char **argv)
{
	printf("sn=%s\r\n",GsmIMEI());
	memset(argv[0],0,strlen(argv[0]));
	sprintf((char*)argv[0], "sn=%s\r\n",GsmIMEI());
	//HAL_UART_Transmit(&lpUartHandle, argv[0], strlen(argv[0]),1000);
	USART_SEND_DATAS(argv[0],LPUART_USART);
}


static void shell_test_iccid (int argc, char **argv)
{
	printf("iccid=%s\r\n",GsmICCID());
	memset(argv[0],0,strlen(argv[0]));
	if(strlen(GsmICCID()) == 0)
	sprintf((char*)argv[0], "iccid=%s\r\n","null");
	else
	sprintf((char*)argv[0], "iccid=%s\r\n",GsmICCID());
	//HAL_UART_Transmit(&lpUartHandle, argv[0], strlen(argv[0]),1000);
	USART_SEND_DATAS(argv[0],LPUART_USART);
}

static void shell_test_getapnname (int argc, char **argv)
{
	printf("\r\napnname=%s\r\n",get_conf_elem(APNNAME_CONF));
	memset(argv[0],0,strlen(argv[0]));
	if(strlen(get_conf_elem(APNNAME_CONF)) == 0)
	sprintf((char*)argv[0], "ok\r\napnname=%s\r\n","null");
	else
	sprintf((char*)argv[0], "ok\r\napnname=%s\r\n",get_conf_elem(APNNAME_CONF));
	//HAL_UART_Transmit(&lpUartHandle, argv[0], strlen(argv[0]),1000);
	USART_SEND_DATAS(argv[0],LPUART_USART);
}

static void shell_test_setapnname (int argc, char **argv)
{
	uint8_t *p;
	p =  argv[0]+SHELL_SETAPNNAME_LENGTH+1;
	update_conf_elem(APNNAME_CONF,p);
	//HAL_UART_Transmit(&lpUartHandle, "ok\r\n", 4,1000);
	USART_SEND_DATAS("ok\r\n",LPUART_USART);
}

static void shell_test_getapnuser (int argc, char **argv)
{
	printf("\r\napnuser=%s\r\n",get_conf_elem(APNUSER_CONF));
	memset(argv[0],0,strlen(argv[0]));
	if(strlen(get_conf_elem(APNUSER_CONF)) == 0)
	sprintf((char*)argv[0], "ok\r\napnuser=%s\r\n","null");
	else
	sprintf((char*)argv[0], "ok\r\napnuser=%s\r\n",get_conf_elem(APNUSER_CONF));
	//HAL_UART_Transmit(&lpUartHandle, argv[0], strlen(argv[0]),1000);
	USART_SEND_DATAS(argv[0],LPUART_USART);
}

static void shell_test_setapnuser (int argc, char **argv)
{
	uint8_t *p;
	p =  argv[0]+SHELL_SETAPNUSER_LENGTH+1;
	update_conf_elem(APNUSER_CONF,p);
	//HAL_UART_Transmit(&lpUartHandle, "ok\r\n", 4,1000);
	USART_SEND_DATAS("ok\r\n",LPUART_USART);
}

static void shell_test_getapnpass (int argc, char **argv)
{
	printf("\r\napnpass=%s\r\n",get_conf_elem(APNPASS_CONF));
	memset(argv[0],0,strlen(argv[0]));
	if(strlen(get_conf_elem(APNPASS_CONF)) == 0)
	sprintf((char*)argv[0], "ok\r\napnpass=%s\r\n","null");
	else
	sprintf((char*)argv[0], "ok\r\napnpass=%s\r\n",get_conf_elem(APNPASS_CONF));
	//HAL_UART_Transmit(&lpUartHandle, argv[0], strlen(argv[0]),1000);
	USART_SEND_DATAS(argv[0],LPUART_USART);
}

static void shell_test_setapnpass (int argc, char **argv)
{
	uint8_t *p;
	p =  argv[0]+SHELL_SETAPNPASS_LENGTH+1;
	update_conf_elem(APNPASS_CONF,p);
	//HAL_UART_Transmit(&lpUartHandle, "ok\r\n", 4,1000);
	USART_SEND_DATAS("ok\r\n",LPUART_USART);
}
static void shell_test_querymode (int argc, char **argv)
{
	printf("**shell_test_querymode**");
	//HAL_UART_Transmit(&lpUartHandle, "mode=engineer\r\n", strlen("mode=engineer\r\n"),1000);
	USART_SEND_DATAS("mode=engineer\r\n",LPUART_USART);
}

static void shell_testmode(int argc, char **argv)
{
	testmode = true;
	gsmDebugEnable(false);
	gpsDebugEnable(false);
	workDebugEnable(false);
	gpsOneDebugEnable(false);
	mainOneDebugEnable(false);
	gsenssorDebugEnable(false);
	configFileDebugEnable(false);
	gpsDataLogEnable(false);
	bitmapEnable(false);
	ds18b20DebugEnable(false);
	DEBUG("TEST MODE");
	gpstestmode = true;
	gsmtestmode = true;
	gsensortestmode = true;
	worktestmode = true;
}


static void do_command(int argc, char **argv)
{
	unsigned int i;
	unsigned int nl = strlen(argv[0]);
	unsigned int cl;
	
	for (i = 0; i < ARRAY_SIZE(commands); i++) {
		cl = strlen(commands[i].name);

		if (cl == nl && commands[i].function != NULL &&
				!strncmp(argv[0], commands[i].name, nl)) {
			commands[i].function(argc, argv);
			putch('\r');
			putch('\n');
      break;
		} else if (!strncmp(argv[0], SHELL_AT_BIGGER_COMMAND, SHELL_AT_PREFIX_LENGTH)) /* for simple the at command biggercase*/{
		  shell_testgprs(argc, argv);
      putch('\r');
      putch('\n');
			break;
		} else if (!strncmp(argv[0], SHELL_AT_LOWER_COMMAND, SHELL_AT_PREFIX_LENGTH)) /* for simple the at command lowercase*/{
		  shell_testgprs(argc, argv);
      putch('\r');
      putch('\n');
			break;
		}else if (!strncmp(argv[0], SHELL_STAND_BY_COMMAND, SHELL_STAND_BY_LENGTH)) /* for simple the at command lowercase*/{
		  shell_teststandby(argc, argv);
      putch('\r');
      putch('\n');
			break;
		}else if (!strncmp(argv[0], SHELL_STOP_COMMAND, SHELL_STOP_LENGTH)) /* for simple the at command lowercase*/{
		  shell_teststop(argc, argv);
      putch('\r');
      putch('\n');
			break;
		}else if (!strncmp(argv[0], SHELL_SHOW_CONFIG_COMMAND, SHELL_SHOW_CONFIG_LENGTH)) /* for simple the at command lowercase*/{
		  testmode = true;
		  shell_testsconfig(argc, argv);
      putch('\r');
      putch('\n');
			testmode = false;
			break;
		}else if (!strncmp(argv[0], SHELL_REBOOT_COMMAND, SHELL_REBOOT_LENGTH)) /* for simple the at command lowercase*/{
		  shell_testreboot(argc, argv);
      putch('\r');
      putch('\n');
			break;
		}else if (!strncmp(argv[0], SHELL_DELETE_CONFIG_COMMAND, SHELL_DELETE_CONFIG_LENGTH)
		|| !strncmp(argv[0], SHELL_FACTORY_COMMAND, SHELL_FACTORY_LENGTH)) /* for simple the at command lowercase*/{
		  shell_testdconfig(argc, argv);
      putch('\r');
      putch('\n');
			break;
		}else if (!strncmp(argv[0], SHELL_CLEANDATA_COMMAND, SHELL_CLEANDATA_LENGTH)) /* for simple the at command lowercase*/{
		  shell_cleandata(argc, argv);
      putch('\r');
      putch('\n');
			break;
		}else if (!strncmp(argv[0], SHELL_OPEN_GPS_LOG_COMMAND, SHELL_OPEN_GPS_LOG_LENGTH)) /* for simple the at command lowercase*/{
		  shell_opengpslog(argc, argv);
      putch('\r');
      putch('\n');
			break;
		}else if (!strncmp(argv[0], SHELL_CLOSE_GPS_LOG_COMMAND, SHELL_CLOSE_GPS_LOG_LENGTH)) /* for simple the at command lowercase*/{
		  shell_closegpslog(argc, argv);
      putch('\r');
      putch('\n');
			break;
		}else if (!strncmp(argv[0], SHELL_TEST_MODE_COMMAND, SHELL_TEST_MODE_LENGTH)) /* for simple the at command lowercase*/{
		  shell_testmode(argc, argv);
      putch('\r');
      putch('\n');
			break;
		}else if (!strncmp(argv[0], SHELL_RELAY_1_COMMAND, SHELL_RELAY_LENGTH)) /* for simple the at command lowercase*/{
		  shell_relay_1(argc, argv);
      putch('\r');
      putch('\n');
			break;
		}else if (!strncmp(argv[0], SHELL_RELAY_2_COMMAND, SHELL_RELAY_LENGTH)) /* for simple the at command lowercase*/{
		  shell_relay_2(argc, argv);
      putch('\r');
      putch('\n');
			break;
		}else if (!strncmp(argv[0], SHELL_3_COMMAND, SHELL_RELAY_LENGTH)) /* for simple the at command lowercase*/{
		  shell_3(argc, argv);
      putch('\r');
      putch('\n');
			break;
		}
		else if (!strncmp(argv[0], SHELL_IMEI_COMMAND, SHELL_IMEI_LENGTH)) /* for simple the at command lowercase*/{
		  shell_test_imei(argc, argv);
      putch('\r');
      putch('\n');
			break;
		}
		else if (!strncmp(argv[0], SHELL_ICCID_COMMAND, SHELL_ICCID_LENGTH)) /* for simple the at command lowercase*/{
		  shell_test_iccid(argc, argv);
      putch('\r');
      putch('\n');
			break;
		}
		else if (!strncmp(argv[0], SHELL_GETAPNNAME_COMMAND, SHELL_GETAPNNAME_LENGTH)) /* for simple the at command lowercase*/{
			shell_test_getapnname(argc, argv);
      putch('\r');
      putch('\n');
			break;
		}
		else if (!strncmp(argv[0], SHELL_SETAPNNAME_COMMAND, SHELL_SETAPNNAME_LENGTH)) /* for simple the at command lowercase*/{
		  shell_test_setapnname(argc, argv);
      putch('\r');
      putch('\n');
			break;
		}
		else if (!strncmp(argv[0], SHELL_GETAPNUSER_COMMAND, SHELL_GETAPNUSER_LENGTH)) /* for simple the at command lowercase*/{
		  shell_test_getapnuser(argc, argv);
      putch('\r');
      putch('\n');
			break;
		}
		else if (!strncmp(argv[0], SHELL_SETAPNUSER_COMMAND, SHELL_SETAPNUSER_LENGTH)) /* for simple the at command lowercase*/{
		  shell_test_setapnuser(argc, argv);
      putch('\r');
      putch('\n');
			break;
		}
		else if (!strncmp(argv[0], SHELL_GETAPNPASS_COMMAND, SHELL_GETAPNPASS_LENGTH)) /* for simple the at command lowercase*/{
		  shell_test_getapnpass(argc, argv);
      putch('\r');
      putch('\n');
			break;
		}
		else if (!strncmp(argv[0], SHELL_SETAPNPASS_COMMAND, SHELL_SETAPNPASS_LENGTH)) /* for simple the at command lowercase*/{
		  shell_test_setapnpass(argc, argv);
      putch('\r');
      putch('\n');
			break;
		}
		else if (!strncmp(argv[0], SHELL_QUERYMODE_COMMAND, SHELL_QUERYMODE_LENGTH)) /* for simple the at command lowercase*/{
		  shell_test_querymode(argc, argv);
      putch('\r');
      putch('\n');
			break;
		}
		else if (!strncmp(argv[0], SHELL_ENGINEER_COMMAND, SHELL_ENGINEER_LENGTH)) /* for simple the at command lowercase*/{
		  shell_test_engineer(argc, argv);
      putch('\r');
      putch('\n');
			break;
		}
		else if (!strncmp(argv[0], SHELL_ENGINEEREXIT_COMMAND, SHELL_ENGINEEREXIT_LENGTH)) /* for simple the at command lowercase*/{
		  shell_test_engineerexit(argc, argv);
      putch('\r');
      putch('\n');
			break;
		}
		else if (!strncmp(argv[0], SHELL_CONF_SET_COMMAND, SHELL_CONF_LENGTH)) /* for simple the at command lowercase*/{
		  testmode = true;
		  shell_conf_set(argc, argv);
      putch('\r');
      putch('\n');
			testmode = false;
			break;
		}else if (!strncmp(argv[0], SHELL_CONF_GET_COMMAND, SHELL_CONF_LENGTH)) /* for simple the at command lowercase*/{
		  testmode = true;
		  shell_conf_get(argc, argv);
      putch('\r');
      putch('\n');
			testmode = false;
			break;
		}else if (!strncmp(argv[0], SHELL_GPSHOTSTART_COMMAND, SHELL_GPSHOTSTART_LENGTH)) /* for simple the at command lowercase*/{
		  shell_gpshotstart(argc, argv);
      putch('\r');
      putch('\n');
			break;
		}
		else if (!strncmp(argv[0], SHELL_GPSWARMSTART_COMMAND, SHELL_GPSWARMSTART_LENGTH)) /* for simple the at command lowercase*/{
		  shell_gpswarmstart(argc, argv);
      putch('\r');
      putch('\n');
			break;
		}else if (!strncmp(argv[0], SHELL_GPSCOLDSTART_COMMAND, SHELL_GPSCOLDSTART_LENGTH)) /* for simple the at command lowercase*/{
			shell_gpscoldstart(argc, argv);
      putch('\r');
      putch('\n');
			break;
		}
		else if (!strncmp(argv[0], SHELL_NORMAL_COMMAND, SHELL_NORMAL_LENGTH)) /* for simple the at command lowercase*/{
			if(testmode){
			testmode = false;
			DEBUG("NORMAL MODE");
			}
		  gsmDebugEnable(true);
			gpsDebugEnable(true);
			workDebugEnable(true);
			gpsOneDebugEnable(true);
			mainOneDebugEnable(true);
			gsenssorDebugEnable(true);
			configFileDebugEnable(true);
			bitmapEnable(true);
			ds18b20DebugEnable(true);
			gpsDataLogEnable(false);
			break;
		}
		#if 1
		else if (!strncmp(argv[0], SHELL_MIC_COMMAND, SHELL_MIC_LENGTH)) /* for simple the at command lowercase*/{
		  shell_test_mic(argc, argv);
      putch('\r');
      putch('\n');
			break;
		}
		
		else if (!strncmp(argv[0], SHELL_MIC_CLOSE_COMMAND, SHELL_MIC_LENGTH)) /* for simple the at command lowercase*/{
		  shell_test_mic_close(argc, argv);
      putch('\r');
      putch('\n');
			break;
		}else if (!strncmp(argv[0], SHELL_NEW_GPIO_COMMAND, SHELL_NEW_GPIO_LENGTH)) /* for simple the at command lowercase*/{
		  shell_new_gpio(argc, argv);
      putch('\r');
      putch('\n');
			break;
		}else if (!strncmp(argv[0], SHELL_RS232DEBUG_COMMAND, SHELL_RS232DEBUG_LENGTH)) /* for simple the at command lowercase*/{
		  shell_rs232_debug(argc, argv);
      putch('\r');
      putch('\n');
			break;
		}else if (!strncmp(argv[0], SHELL_JAMMINGON_COMMAND, SHELL_JAMMINGON_LENGTH)) /* for simple the at command lowercase*/{
		  shell_jamming_on(argc, argv);
      putch('\r');
      putch('\n');
			break;
		}else if (!strncmp(argv[0], SHELL_JAMMINGOFF_COMMAND, SHELL_JAMMINGOFF_LENGTH)) /* for simple the at command lowercase*/{
		  shell_jamming_off(argc, argv);
      putch('\r');
      putch('\n');
			break;
		}
		#endif
		
		else
		{
		if(testmode){
			testmode = false;
			DEBUG("NORMAL MODE");
		}
			gsmDebugEnable(true);
			gpsDebugEnable(true);
			workDebugEnable(true);
			gpsOneDebugEnable(true);
			mainOneDebugEnable(true);
			gsenssorDebugEnable(true);
			configFileDebugEnable(true);
			bitmapEnable(true);
			ds18b20DebugEnable(true);
			gpsDataLogEnable(false);
		}
	}
}



static void parse_command(void)
{
	unsigned char i;
	char *argv[16];
	int argc = 0;
	char *in_arg = NULL;

	for (i = 0; i < shell.index; i++) {
		if (is_whitespace(shell.buf[i]) && argc == 0)
			continue;

		if (is_whitespace(shell.buf[i])) {
			if (in_arg) {
				//shell.buf[i] = '\0';
				//in_arg = NULL;
			}
		} else if (!in_arg) {
			in_arg = &shell.buf[i];
			argv[argc] = in_arg;
			argc++;
		}
		
		if(shell.buf[i] == 0){
			if (argc > 0)
			do_command(argc, argv);
			argc = 0;
			in_arg = NULL;
		}
	}
	//shell.buf[i] = '\0';

	//if (argc > 0)
		//do_command(argc, argv);
	shell.index = 0;
	memset(shell.buf, 0, sizeof(shell.buf));
}

static void handle_tab(void)
{
	int i, j;
	unsigned int match = 0;
	struct shell_command *cmd;
	unsigned int sl = 0;

	shell.match = 0;

	for (i = 0; i < ARRAY_SIZE(commands); i++) {
		for (j = strlen(commands[i].name); j > 0; j--) {
			if (!strncmp(commands[i].name, shell.buf, shell.index)) {
				match++;
				shell.match |= (1<<i);
				break;
			}
		}
	}

	if (match == 1) {
		for (i = 0; i < ARRAY_SIZE(commands); i++) {
			if (shell.match & (1<<i)) {
				cmd = &commands[i];
				memcpy(shell.buf + shell.index, cmd->name + shell.index,
						strlen(cmd->name) - shell.index);
				shell.index += strlen(cmd->name) - shell.index;
			}
		}
	} else if (match > 1) {
		for (i = 0; i < ARRAY_SIZE(commands); i++) {
			if (shell.match & (1<<i)) {
				putch('\r');
				putch('\n');
				uart_puts(commands[i].name);

				if (sl == 0 || strlen(commands[i].name) < sl) {
					sl = strlen(commands[i].name);
				}
			}
		}

		for (i = 0; i < ARRAY_SIZE(commands); i++) {
			if (shell.match & (1<<i) && strlen(commands[i].name) == sl) {
				memcpy(shell.buf + shell.index, commands[i].name + shell.index,
						sl - shell.index);
				shell.index += sl - shell.index;
				break;
			}
		}
	}

	putch('\r');
	putch('\n');
	prompt();
	uart_puts(shell.buf);
}
bool received = false;
static void handle_input(char c)
{
	switch (c) {
		case '\r':
		case '\n':
		case '#' :
			//putch('\r');
			//putch('\n');

			if (shell.index > 0) {
                            
				//parse_command();
				//shell.index = 0;
				//memset(shell.buf, 0, sizeof(shell.buf));
				if(!strncmp(shell.buf,TUB,3)){
					putch(c);
					putch('\r');
					putch('\n');
					shell.buf[shell.index] = c;
				  shell.index++;
					tub(shell.buf);
				}
				received = true;
				shell.buf[shell.index] = 0;
				shell.index++;
				
			}else
			{
				gsmDebugEnable(false);
				gpsDebugEnable(false);
				workDebugEnable(false);
				gpsOneDebugEnable(false);
				mainOneDebugEnable(false);
				gsenssorDebugEnable(false);
				configFileDebugEnable(false);
				gpsDataLogEnable(false);
				bitmapEnable(false);
				ds18b20DebugEnable(false);
			}
			if(shell.index < SHELL_ENGINEER_LENGTH)
			prompt();
			break;

		case '\b':
			if (shell.index) {
				shell.buf[shell.index-1] = '\0';
				shell.index--;
				putch(c);
				putch(' ');
				putch( c);
			}
			break;

		case '\t':
			handle_tab();
			break;

		default:
			if (shell.index < SHELL_CMD_BUFFER_LEN - 1) {
				putch(c);
				shell.buf[shell.index] = c;
				shell.index++;
			}
			
			if (shell.index == SHELL_CMD_BUFFER_LEN - 1)
			{
			putch('\r');
			putch('\n');
			shell.index = 0;
			memset(shell.buf, 0, sizeof(shell.buf));
			}

		}
}


void shellData(uint8_t data)
{	
	handle_input(data);
	#if 0
	if (shell.index < SHELL_CMD_BUFFER_LEN - 1){
	shell.buf[shell.index] = data;
	shell.index++;
	}else
	{
	shell.index = 0;
	memset(shell.buf, 0, sizeof(shell.buf));
	}
	#endif
}


void vDebugTask(void const *pvArg)
{
	uint8_t c = 0;
	uint32_t time = 0;
	shell.index = 0;
	
	memset(shell.buf, 0, sizeof(shell.buf));
	
	for(;;) {
		#if 0
		if(HAL_UART_Receive(&debugUartHandle,(uint8_t *)&c,RXBUFFERSIZE,200) == HAL_TIMEOUT)
		   DebugInit();
		if (c != 0x0 && c < 127) {
			if(testmode){
				if( c == 0x31 || c == 0x32 || c =='\r' || c == '\n')
				{
				shellData(c);
				c = 0;
				continue;
				}
				testmode = false;
				DEBUG("*****************NORMAL MODE********************");
				continue;
			}
			time = 0;
			gsmDebugEnable(false);
			gpsDebugEnable(false);
			workDebugEnable(false);
			gpsOneDebugEnable(false);
			mainOneDebugEnable(false);
			gsenssorDebugEnable(false);
			configFileDebugEnable(false);
			gpsDataLogEnable(false);
			bitmapEnable(false);
			ds18b20DebugEnable(false);
			osDelay(5);
			shellData(c);
		}
		time++;
		if(time > 100 && c == 0){
			time = 0;
			if(testmode)
				continue;
			gsmDebugEnable(true);
			gpsDebugEnable(true);
			workDebugEnable(true);
			gpsOneDebugEnable(true);
			mainOneDebugEnable(true);
			gsenssorDebugEnable(true);
			configFileDebugEnable(true);
			bitmapEnable(true);
			ds18b20DebugEnable(true);
		}
		c = 0;
		osDelay(10);
	}
	#endif
	//lpUartReceiveHandle();
	if (shell.index > 0 && received){
		received = false;
		gsmDebugEnable(false);
		gpsDebugEnable(false);
		workDebugEnable(false);
		gpsOneDebugEnable(false);
		mainOneDebugEnable(false);
		gsenssorDebugEnable(false);
		configFileDebugEnable(false);
		gpsDataLogEnable(false);
		bitmapEnable(false);
		ds18b20DebugEnable(false);
		deInitWWDG();
		parse_command();
		initWWDG();
		startWWDG();
		//shell.index = 0;
		//memset(shell.buf, 0, sizeof(shell.buf));
	}
		osDelay(20);
	}
}
