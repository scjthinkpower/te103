#include <stdio.h>
#include <string.h>
#include <math.h>
#include <stdlib.h>

#include "hardware.h"
#include "FreeRTOS.h"
#include "queue.h"
#include "semphr.h"
#include "task.h"
#include "main.h"
#include "gps.h"
#include "gsm.h"
#include "debug.h"
#include "kalman.h"
#include "gpsOne.h"
#include "nmea/nmea.h"
#include "nmea/tok.h"
#include "can.h"
#include "ring_buffer.h"
#include "debug.h"
#include "work.h"
#include "config_file.h"
#include "geofence.h"
#include "bitmap_flash.h"

#define GPS_SEND_INTERVAL	    atoi((char *)get_conf_elem(REPINTERVAL_CONF))
#define MILEAGE_MAXIMUM        9999999 
#define ENGINHOUR_MAXIMUM   	 9999999
#define OVERSPEED_MAXIMUM        9999999 

extern RTC_HandleTypeDef RTCHandle;

static uint8_t gpsTxBuffer[32];
static uint8_t gpsRxBuffer[128];
GpsContext_t GpsContext;
uint8_t gps_aRxData;
//static int8_t testGps[]="$GPRMC,013257,A,3159.9919,N,12022.105620,E,0.093,,270813,,,A*7A\r\n";

const int8_t HEX_int8_tS[]		= "0123456789ABCDEF";

//const int8_t GPS_SET_MTK0[]		= "PMTK314,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0";
const int8_t GPS_SET_MTK0[]		= "PMTK";


const int8_t GPS_STM_SBASONOFF[]	= "$PSTMSBASONOFF";
const int8_t GPS_STM_GETSWVER[]	= "$PSTMGETSWVER";
const int8_t GPS_STM_REPLAY[]		= "$PSTMVER,";
const int8_t GPS_SET_STM1[]		= "PSTMSETPAR,1201,40";
const int8_t GPS_SET_STM2[]		= "PSTMSAVEPAR";
const int8_t GPS_SET_STM3[]		= "PSTMSETCONSTMASK,3";

uint8_t	GpsGeneral_Init(GpsContext_t * gc);
uint8_t	GpsSiRF_Init(GpsContext_t * gc);
uint8_t	GpsMTK_Init(GpsContext_t * gc);
uint8_t	GpsSTM_Init(GpsContext_t * gc);
uint8_t	GpsML8088_Init(GpsContext_t * gc);

const GpsCommands_t	GpsCmdGeneral	= { GpsGeneral_Init };
const GpsCommands_t	GpsCmdSiRF		= { GpsSiRF_Init };
const GpsCommands_t	GpsCmdMTK		= { GpsMTK_Init };
const GpsCommands_t	GpsCmdSTM		= { GpsSTM_Init };
const GpsCommands_t	GpsCmdML8088	= { GpsML8088_Init };

const int8_t GPS_SET_SIRF0[]		= "PSRF105,0";			// Debug Off
const int8_t GPS_SET_SIRF1[]		= "PSRF103,0,0,0,1";	// Disable GGA
const int8_t GPS_SET_SIRF2[]		= "PSRF103,1,0,0,1";	// Disable GLL
const int8_t GPS_SET_SIRF3[]		= "PSRF103,2,0,0,1";	// Disable GSA
const int8_t GPS_SET_SIRF4[]		= "PSRF103,3,0,0,1";	// Disable GSV
const int8_t GPS_SET_SIRF5[]		= "PSRF103,4,0,1,1";	// Enable  RMC
const int8_t GPS_SET_SIRF6[]		= "PSRF103,5,0,0,1";	// Disable VTG
/*
const int8_t GPS_SET_SIRF7[]		= "PSRF100,0,4800,8,1,0";	// Switch to OSP protocol
															// Enable SBAS
const uint8_t GPS_SET_SIRF8[]		= {	0x85,		// MID: 133
									0x01,		// Uses SBAS Satellite
									0x00, 0x00, 0x00, 0x00,
									0x00
								};
									// Switch to NMEA protocol, Only RMC enable
const uint8_t GPS_SET_SIRF9[]		= {	0x81,		// MID: 129
									0x02,		// Do not change last-set value for NMEA debug messages
									0x00, 0x01,	// GGA
									0x00, 0x01, // GLL
									0x00, 0x01, // GSA
									0x00, 0x01, // GSV
									0x01, 0x01, // RMC
									0x00, 0x01,	// VTG
									0x00, 0x01, // MSS
									0x00, 0x00, // EPE
									0x00, 0x01, // ZDA
									0x00, 0x00,	// Unused
									0x12, 0xC0	// 4800
								};
const uint8_t GPS_SET_SIRF10[]		= {	0x80,	// Factory Reset
									0x00, 0x00, 0x00, 0x00,
									0x00, 0x00, 0x00, 0x00,
									0x00, 0x00, 0x00, 0x00,
									0x00, 0x00, 0x00, 0x00,
									0x00, 0x00, 0x00, 0x00,
									0x00, 0x00,
									0x0C,
									0x88,
									0x01,
									0x14 };
*/

typedef const int8_t * pint8_t;

const pint8_t GPS_SET_STM[] = {
	GPS_SET_STM1,
	GPS_SET_STM1,
	GPS_SET_STM2,
	GPS_SET_STM2,
	GPS_SET_STM3,
	NULL
};

const pint8_t GPS_SET_SIRFS[] = {
	GPS_SET_SIRF0,
	GPS_SET_SIRF1,
	GPS_SET_SIRF2,
	GPS_SET_SIRF3,
	GPS_SET_SIRF4,
	GPS_SET_SIRF5,
	GPS_SET_SIRF6,
	NULL
};

bool gps_gotoSleep = false;

bool rtc_time = false;

double last_lat;
double last_lon;
static const double EARTH_RADIUS_KM = 6378.137;
static const double EARTH_RADIUS_M = EARTH_RADIUS_KM * 1000;

bool gpsDataLog = false;
bool gpsLocationSuccess = false;
bool gpstestmode = false;
bool timetoSend = false;
bool distancetoSend = false;
uint32_t g_meters = 0;
uint32_t g_distance = 0;

nmeaGPRMC lastnmeaGPRMC;
nmeaGPRMC lastnmea_GPRMC;
nmeaGPRMC lastnmeaGPRMC_bakeup;
nmeaGPRMC readGPRMC;

point_t point;
double heading_temp = 0;
uint32_t time_noupdate = 0;
bool gpsRun = true;
uint8_t ioffdata = 0;
bool gpsFixed = true;

double lat_course = 0;
double lon_course = 0;
double heading;
int utc_year = 0;
uint8_t msg_count;
uint32_t gpsdata = 0;

nmeaGPGSV GSV;
nmeaGPGSA GSA;

bool getGpsRun(void)
{
   return gpsRun;
}

void testGpsRun(bool run)
{
		gpsRun = run;
}



/*******************************************************************************
* Function Name	:	gpsTxGet
* Description	:	Get data from GPS TX queue
* Input			:	int8_tacter from queue
* Return		:	result code
*******************************************************************************/
void GpsPut(uint8_t data)
{
	while(rb_Put(&GpsContext.RB_Tx, data) == 0)
	{
		//USART_ITConfig(GPS_USART, USART_IT_TXE, ENABLE);
		__HAL_UART_ENABLE_IT(&gpsUartHandle, UART_IT_TXE);
		HAL_Delay(5);
	}
}

/*******************************************************************************
* Function Name	:	GpsPutLine
* Description	:	Send line to GPS
* Input			:	Add $ at begin and *XX\r\n checksum at end
* Return		:	result code
*******************************************************************************/
void GpsPutLineCS(const int8_t * text)
{
	uint8_t data;
	uint8_t cs = 0;

	GpsPut('$');
	while((data = *text++) != 0)
	{
		cs ^= data;
		GpsPut(data);
	}

	GpsPut('*');
	GpsPut(HEX_int8_tS[cs >> 4]);
	GpsPut(HEX_int8_tS[cs & 0x0F]);
	//GpsPut('\r');
	//GpsPut('\n');
	//USART_ITConfig(GPS_USART, USART_IT_TXE, ENABLE);
	__HAL_UART_ENABLE_IT(&gpsUartHandle, UART_IT_TXE);
}

/*******************************************************************************
* Function Name	:	GpsPutLineLn
* Description	:	Send line to GPS
* Input			:	Add <cr><lf> at end
* Return		:	result code
*******************************************************************************/
void GpsPutLine(const int8_t * text)
{
	uint8_t data;
	while((data = *text++) != 0)
		GpsPut(data);
	//USART_ITConfig(GPS_USART, USART_IT_TXE, ENABLE);
	__HAL_UART_ENABLE_IT(&gpsUartHandle, UART_IT_TXE);
}

void GpsPutLineLn(const int8_t * text)
{
	GpsPutLine(text);
	GpsPut('\r');
	GpsPut('\n');
}

void GpsPutBinary(const int8_t * text, int len)
{
	while(len-- != 0)
		GpsPut(*text++);
	//USART_ITConfig(GPS_USART, USART_IT_TXE, ENABLE);
	__HAL_UART_ENABLE_IT(&gpsUartHandle, UART_IT_TXE);
}

/*******************************************************************************
* Function Name :	GpsSendSiRFHeader
* Description   :	
*******************************************************************************/
void GpsSendSiRF(const uint8_t * cmd, int len)
{
	uint8_t data;
	uint16_t cs = 0;

	GpsPut(0xA0);
	GpsPut(0xA2);
	GpsPut(len >> 8);
	GpsPut(len & 0xFF);
	while (len-- != 0)
	{
		data = *cmd++;
		while(rb_Put(&GpsContext.RB_Tx, data) == 0)
		{
			//USART_ITConfig(GPS_USART, USART_IT_TXE, ENABLE);
			__HAL_UART_ENABLE_IT(&gpsUartHandle, UART_IT_TXE);
			HAL_Delay(5);
		}
		cs += (uint16_t)data;
	}
	cs &= 0x7FFF;
	GpsPut(cs >> 8);
	GpsPut(cs & 0xFF);
	GpsPut(0xB0);
	GpsPut(0xB3);
	//USART_ITConfig(GPS_USART, USART_IT_TXE, ENABLE);
	__HAL_UART_ENABLE_IT(&gpsUartHandle, UART_IT_TXE);
}

void gpsSend(uint8_t *text,uint32_t size)
{
    int32_t i ; 
    for (i = 0 ; i < size ; i++) {
        LL_USART_TransmitData8(GPS_USART, *text++)  ;
        while (READ_BIT(GPS_USART->ISR, USART_ISR_TXE) == RESET) ; 
    }
}


void CFC_RST(void)	
{
	uint8_t i = 0;
	
	memset(gpsTxBuffer,0,32);
	gpsTxBuffer[0] = 0xB5;
	gpsTxBuffer[1] = 0x62;
	gpsTxBuffer[2] = 0x06;
	gpsTxBuffer[3] = 0x04;
	gpsTxBuffer[4] = 0x04;
	gpsTxBuffer[5] = 0x00;
	gpsTxBuffer[6] = 0x01;
	gpsTxBuffer[7] = 0x00;
	gpsTxBuffer[8] = 0x00;
	gpsTxBuffer[9] = 0x00;
  for(i=2;i<10;i++)
	{
	gpsTxBuffer[10] = gpsTxBuffer[10] + gpsTxBuffer[i];
	gpsTxBuffer[11] = gpsTxBuffer[11] + gpsTxBuffer[10];
	}
	
	gpsSend(gpsTxBuffer,12);
	
	}

	void CFC_HOT_RST(void)	
{
	uint8_t i = 0;
	
	//B5 62 06 04 04 00 00 00 02 00 10 68
	
	memset(gpsTxBuffer,0,32);
	gpsTxBuffer[0] = 0xB5;
	gpsTxBuffer[1] = 0x62;
	gpsTxBuffer[2] = 0x06;
	gpsTxBuffer[3] = 0x04;
	gpsTxBuffer[4] = 0x04;
	gpsTxBuffer[5] = 0x00;
	gpsTxBuffer[6] = 0x00;
	gpsTxBuffer[7] = 0x00;
	gpsTxBuffer[8] = 0x02;
	gpsTxBuffer[9] = 0x00;
  for(i=2;i<10;i++)
	{
	gpsTxBuffer[10] = gpsTxBuffer[10] + gpsTxBuffer[i];
	gpsTxBuffer[11] = gpsTxBuffer[11] + gpsTxBuffer[10];
	}
	
	gpsSend(gpsTxBuffer,12);
	
	}

	
	void CFC_WARM_RST(void)	
{
	uint8_t i = 0;
	
	//B5 62 06 04 04 00 01 00 02 00 11 6C
	
	memset(gpsTxBuffer,0,32);
	gpsTxBuffer[0] = 0xB5;
	gpsTxBuffer[1] = 0x62;
	gpsTxBuffer[2] = 0x06;
	gpsTxBuffer[3] = 0x04;
	gpsTxBuffer[4] = 0x04;
	gpsTxBuffer[5] = 0x00;
	gpsTxBuffer[6] = 0x01;
	gpsTxBuffer[7] = 0x00;
	gpsTxBuffer[8] = 0x02;
	gpsTxBuffer[9] = 0x00;
  for(i=2;i<10;i++)
	{
	gpsTxBuffer[10] = gpsTxBuffer[10] + gpsTxBuffer[i];
	gpsTxBuffer[11] = gpsTxBuffer[11] + gpsTxBuffer[10];
	}
	
	gpsSend(gpsTxBuffer,12);
	
	}

	void CFC_COLD_RST(void)	
{
	uint8_t i = 0;
	
	//B5 62 06 04 04 00 FF B9 02 00 C8 8F
	
	memset(gpsTxBuffer,0,32);
	gpsTxBuffer[0] = 0xB5;
	gpsTxBuffer[1] = 0x62;
	gpsTxBuffer[2] = 0x06;
	gpsTxBuffer[3] = 0x04;
	gpsTxBuffer[4] = 0x04;
	gpsTxBuffer[5] = 0x00;
	gpsTxBuffer[6] = 0xFF;
	gpsTxBuffer[7] = 0xB9;
	gpsTxBuffer[8] = 0x02;
	gpsTxBuffer[9] = 0x00;
  for(i=2;i<10;i++)
	{
	gpsTxBuffer[10] = gpsTxBuffer[10] + gpsTxBuffer[i];
	gpsTxBuffer[11] = gpsTxBuffer[11] + gpsTxBuffer[10];
	}
	
	gpsSend(gpsTxBuffer,12);
	
	}

	
void CFC_GLONASS_OR_BDS(bool bds)	
{
	uint8_t BDS[] = {
	0xB5, 0x62, 0x06, 0x3E, 0x3C, 0x00, 0x00, 0x00, 0x20, 0x07, 0x00, 0x08, 0x10, 0x00, 0x00, 0x00, 0x01, 0x01, 0x01, 0x01, 0x03,
	0x00, 0x00, 0x00, 0x01, 0x01, 0x02, 0x04, 0x08, 0x00, 0x00, 0x00, 0x01, 0x01, 0x03, 0x08, 0x10, 0x00, 0x01, 0x00, 0x01, 0x01,
	0x04, 0x00, 0x08, 0x00, 0x00, 0x00, 0x01, 0x01, 0x05, 0x00, 0x03, 0x00, 0x00, 0x00, 0x01, 0x01, 0x06, 0x08, 0x0E, 0x00, 0x00,
	0x00, 0x01, 0x01, 0x2C, 0x35
	};                                                                                            

	uint8_t GLONASS[] = {
	0xB5, 0x62, 0x06, 0x3E, 0x3C, 0x00, 0x00, 0x00, 0x20, 0x07, 0x00, 0x08, 0x10, 0x00, 0x01, 0x00, 0x01, 0x01, 0x01, 0x01, 0x03,
	0x00, 0x00, 0x00, 0x01, 0x01, 0x02, 0x04, 0x08, 0x00, 0x00, 0x00, 0x01, 0x01, 0x03, 0x08, 0x10, 0x00, 0x00, 0x00, 0x01, 0x01,
	0x04, 0x00, 0x08, 0x00, 0x00, 0x00, 0x01, 0x01, 0x05, 0x00, 0x03, 0x00, 0x00, 0x00, 0x01, 0x01, 0x06, 0x08, 0x0E, 0x00, 0x01,
	0x00, 0x01, 0x01, 0x2D, 0x51
	};                                                                                              
	
	
	if(bds)
	gpsSend(BDS,68);
	else
	gpsSend(GLONASS,68);
	
	}

	void AID_ALP(uint8_t* Buffer,uint32_t len)	
{
	gpsSend(Buffer,len);	
}

void CFG_NAVX5(void)
{

	uint8_t NAVX5[] = {

	0xB5 ,0x62 ,0x06 ,0x23 ,0x28 ,0x00 ,0x00 ,0x00 ,0x00 ,0x04 ,0x00 ,0x00 ,0x00 ,0x00 ,0x00 ,0x00, 
  0x00 ,0x00 ,0x00 ,0x00 ,0x00 ,0x00 ,0x00 ,0x01 ,0x00 ,0x00 ,0x00 ,0x00 ,0x00 ,0x00 ,0x00 ,0x00,  
  0x00 ,0x00 ,0x00 ,0x00 ,0x00 ,0x00 ,0x00 ,0x00 ,0x00 ,0x00 ,0x00 ,0x00 ,0x00 ,0x00 ,0x56 ,0x24  
	};
	gpsSend(NAVX5,48);
}


void MGA_INI_TIME_UTC(void)
{
	uint8_t i = 0;
	int8_t str[10];
	
	// B5 62 13 40 18 00 10 00 00 80 E2 07 05 10 05 36  �b.@......?...6
   //0C 00 00 00 00 00 00 00 00 00 00 65 CD 1D 8F E3  ..
	
	
	memset(gpsTxBuffer,0,32);
	
	RTC_DateTypeDef date;
	HAL_RTC_GetDate(&RTCHandle, &date, RTC_FORMAT_BIN);
	
	if(date.Year == 0)
		return;
	
	RTC_TimeTypeDef stimestructureget;
	HAL_RTC_GetTime(&RTCHandle, &stimestructureget, RTC_FORMAT_BIN);
	
	
	
	gpsTxBuffer[0] = 0xB5;
	gpsTxBuffer[1] = 0x62;
	gpsTxBuffer[2] = 0x13;
	gpsTxBuffer[3] = 0x40;
	gpsTxBuffer[4] = 0x18;
	gpsTxBuffer[5] = 0x00;
	gpsTxBuffer[6] = 0x10;
	gpsTxBuffer[7] = 0x00;
	gpsTxBuffer[8] = 0x00;
	gpsTxBuffer[9] = 0x80;
	
	memset(str,0,10);
	sprintf(str, "%04x",date.Year+2000);
	StrToHex(gpsTxBuffer+10,str,strlen(str)/2);
	
	i = gpsTxBuffer[10];
	
	gpsTxBuffer[10] =  gpsTxBuffer[11];
	gpsTxBuffer[11] = i;
	
	gpsTxBuffer[12] =  date.Month;
	gpsTxBuffer[13] =  date.Date;
	
	gpsTxBuffer[14] =  stimestructureget.Hours;
	gpsTxBuffer[15] =  stimestructureget.Minutes;
	gpsTxBuffer[16] =  stimestructureget.Seconds;
	
	gpsTxBuffer[17] =  0x00;
	
	gpsTxBuffer[18] =  0x00;
	gpsTxBuffer[19] =  0x00;
	gpsTxBuffer[20] =  0x00;
	gpsTxBuffer[21] =  0x00;
	
	gpsTxBuffer[22] =  0x00;
	gpsTxBuffer[23] =  0x00;
	
	gpsTxBuffer[24] =  0x00;
	gpsTxBuffer[25] =  0x00;
	
	gpsTxBuffer[26] =  0x00;
	gpsTxBuffer[27] =  0x65;
	gpsTxBuffer[28] =  0xCD;
	gpsTxBuffer[29] =  0x1D;
	
  for(i=2;i<30;i++)
	{
	gpsTxBuffer[30] = gpsTxBuffer[30] + gpsTxBuffer[i];
	gpsTxBuffer[31] = gpsTxBuffer[31] + gpsTxBuffer[30];
	}

	gpsSend(gpsTxBuffer,32);
}


void IF_AGPS(void)
{
	uint8_t agps_buff[90];
	uint8_t ano_number;
	
	if(strcmp((char *)get_conf_elem(RESERVE7_CONF),"1"))
		return;
	
	if(GG_FwSmsIndex == -2)
		return;
	
	if(DebugEnable) DEBUG("IF_AGPS");
	
	memset(agps_buff,0,90);
	
	for(ano_number = 0;ano_number < 52;ano_number++){
	memset(agps_buff,0,90);
	bit_map_read_agps_data_index(agps_buff,84,ano_number);
	if(agps_buff[0] != 0xb5){
	if(DebugEnable) DEBUG("first byte %d %02x %02x,%02x",ano_number,agps_buff[0],agps_buff[4],agps_buff[12]);
	}
	AID_ALP(agps_buff,84);
	osDelay(150);
	}

}

void initGpsUartHandle(uint32_t speed)
{
	if(gpsUartHandle.Instance != GPS_USART)
	{
	gpsUartHandle.Instance        = GPS_USART; 
  gpsUartHandle.Init.BaudRate   = speed; 
  gpsUartHandle.Init.WordLength = UART_WORDLENGTH_8B; 
  gpsUartHandle.Init.StopBits   = UART_STOPBITS_1; 
  gpsUartHandle.Init.Parity     = UART_PARITY_NONE; 
  gpsUartHandle.Init.HwFlowCtl  = UART_HWCONTROL_NONE; 
  gpsUartHandle.Init.Mode       = UART_MODE_TX_RX; 
	}
}

/*******************************************************************************
* Function Name	:	GpsInit
* Description	:	Initialize GPS
*******************************************************************************/
void GpsInit(GpsContext_t * gc, uint32_t speed)
{

	rb_Init(&gc->RB_Tx, gpsTxBuffer, sizeof(gpsTxBuffer));	

	gc->WaitAliveSize = 0;
	//gc->RxMode = GPS_MODE_START;

	GPS_PWR_OFF();
	osDelay(10);
	GPS_PWR_ON();
	//osDelay(500);
	
	
	#if 0
	
	initGpsUartHandle(speed);
	
	 
  //if(HAL_UART_Init(&gpsUartHandle) != HAL_OK)
  //{
		//if(DebugEnable) DEBUG("gps UART_Init error");
  //} 
	

	#endif
	
	
	MX_USART5_UART_Init(speed);
	
	//CFC_RST();
	gpsdata = 0;
	osDelay(500);
	CFC_HOT_RST();
	//CFC_COLD_RST();
	//CFG_NAVX5();
	MGA_INI_TIME_UTC();
	IF_AGPS();
	
}

/*******************************************************************************
* Function Name	:	GpsDeInit
* Description	:	DeInitialize GPS
*******************************************************************************/
void GpsDeInit(GpsContext_t * gc)
{
	if(!gpsRun)
	gpsRun = true;
	gc->RxMode = GPS_MODE_IGNORE;
	initGpsUartHandle(9600);
	GPS_DEINIT(); 
	//GPS_PWR_OFF();
	fatfsUnLink();
	osDelay(5);
}

bool gpsWaitComplete(GpsContext_t * gc, uint16_t ms)
{
	while (gc->RxMode != GPS_MODE_READY && ms != 0)
	{
		if (ms >= 10)
		{
			osDelay(10);
			ms -= 10;
		}
		else
		{
			--ms;
			osDelay(1);
		}
	}
	return (gc->RxMode == GPS_MODE_READY ? true : false);
}

bool gpsWaitReady(GpsContext_t * gc, uint16_t ms)
{
	gc->RxMode = GPS_MODE_START;
	return gpsWaitComplete(gc, ms);
}

/*******************************************************************************
* Function Name	:	gpsCheckType
* Description	:
*******************************************************************************/
bool gpsCheckTypeInit(GpsContext_t * gc, uint32_t speed)
{
	GpsInit(gc, speed);
	//gc->GpsCommand = &GpsCmdMTK;
	return true;
#if 0
	if (gpsWaitComplete(gc, 5000))
	{	// Receive first message from GPS
		if (strcmp(gpsRxBuffer, "$PSRF150,1*3E\r\n") == 0)
		{	// SiRF chipset
			if(DebugEnable) DEBUG(" SiRF");
			gc->GpsCommand = &GpsCmdSiRF;
		}
		else if (strcmp(gpsRxBuffer, "$PMTK011,MTKGPS*08\r\n") == 0)
		{	// MTK chipset
			if(DebugEnable) DEBUG(" MTK");
			gc->GpsCommand = &GpsCmdMTK;
		}
		else if (strncmp(gpsRxBuffer, "$PSTMVER,GNSSLIB_7.2.4.40_ARM", sizeof("$PSTMVER,GNSSLIB_7.2.4.40_ARM") - 1) == 0)
		{	// ML8088s
			if(DebugEnable) DEBUG(" ML8088");
			gc->GpsCommand = &GpsCmdML8088;
		}
		else if (
				strncmp(gpsRxBuffer, "$PSTMVER,", 9) == 0
			||	strncmp(gpsRxBuffer, "$GPTXT,TELIT SW Version: SL869", sizeof("$GPTXT,TELIT SW Version: SL869") - 1) == 0
				)
		{	// STM chipset
			if(DebugEnable) DEBUG(" STM");
			gc->GpsCommand = &GpsCmdSTM;
		}
		return true;
	}
	GpsDeInit(gc);
	return false;
#endif
}

static double rad(double d)
{
   return (d * 3.14159267) / 180.0;
}


double GetDistance(double lat1, double lng1, double lat2, double lng2)
{
      double s ; 
      double radLat1 = rad(lat1);
      double radLat2 = rad(lat2);
      double a = radLat1 - radLat2;
      double b = rad(lng1) - rad(lng2);

      s = 2 * asin(sqrt(pow(sin(a/2),2) +cos(radLat1)*cos(radLat2)*pow(sin(b/2),2)));
			s = s * EARTH_RADIUS_M;
      //s = round(s * 10000) / 10000;
			//if(DebugEnable)
			//DEBUG("##########################################s1 %f",s);
      return s;
}

void calculate_whole_mileage(int distance) {
	#define METERS_OF_1KM 1000
 	//static int ml_meters  ; 
	register GpsContext_t * gc = &GpsContext;
    int kms ; 
	  int report_distance;
      if (isAccOn()) {
			report_distance = atoi(get_conf_elem(DISTANCE_CONF));
			if(report_distance < 50)
					report_distance = 500;
      gc->meters  += distance ;
			g_meters  += distance;
	    kms = gc->meters  / METERS_OF_1KM ;
			g_distance = g_meters /report_distance;
			if(g_distance > 0)
			{
			g_meters = g_meters  - g_distance * report_distance;
			if(!strncmp(get_conf_elem(GSMSWITCH_CONF),"2",1) || !strncmp(get_conf_elem(GSMSWITCH_CONF),"3",1)
				|| !strncmp(get_conf_elem(GSMSWITCH_CONF),"4",1))
				distancetoSend = true;
			}
	    if (kms > 0) {
          gc->mileage += kms ; 
	        gc->meters  = gc->meters  - kms * METERS_OF_1KM ;
			if(gc->mileage > MILEAGE_MAXIMUM)
				gc->mileage = 0;
			write_ml_data(gc->mileage);
	    }
			//if(DebugEnable)
			//DEBUG("distance = %d, ml_meters = %d, mileage = %d", distance ,gc->meters, gc->mileage);
	} 
}

double convert_lat_lng_string_to_double(double data, char flag) {
    double du; 
    double fen ; 
    char dustr[5]; 
    char fenstr[20]; 
    int signc = 1 ; 
		char p[20];
		int8_t* q = NULL;
	  int tmp = 0;
	
	  //3159.9919,N,12022.105620
		/*	
		sprintf(p, "%f", data);
	
		q = (int8_t*)strchr((char *)p,'.');
	
		if(q == NULL)
			return data;
		
    if ( flag == 'n' || flag == 'N' ||flag == 's' || flag == 'S' ) {
     memcpy(dustr, p, (q-p-2)) ;
		 dustr[q-p-2] = 0;
		 memcpy(fenstr, q-2, p+strlen(p)-q+2) ; 
		 fenstr[p+strlen(p)-q+2] = 0;
	   du = atof(dustr); 
	   fen = atof(fenstr) / 60 ;
	   if (flag == 's' || flag == 'S' )    signc = -1 ; 
    }
    if (flag == 'w' || flag == 'e' || flag =='W' || flag == 'E') {
	   memcpy(dustr, p, (q-p-2)) ;
		 dustr[q-p-2] = 0;
	   memcpy(fenstr, q-2, p+strlen(p)-q+2) ;
		 fenstr[p+strlen(p)-q+2] = 0;
	   du = atof(dustr) ; 
	   fen = atof(fenstr) / 60; 
	   if (flag == 'w' || flag == 'W')  signc = -1 ; 
    }
   */ 
	 
		 if ( flag == 'n' || flag == 'N' ||flag == 's' || flag == 'S' ) {
		
		 tmp = data / 100;
     du  = (double)tmp;			 
	   fen = (data - du *100) / 60 ;
	   if (flag == 's' || flag == 'S' )    signc = -1 ;
			
		 }
		 
		 if (flag == 'w' || flag == 'e' || flag =='W' || flag == 'E') {
					
		 tmp = data / 100;
		 du  = (double)tmp;	
	   fen = (data - du *100) / 60; 
	   if (flag == 'w' || flag == 'W')  signc = -1 ; 
		
		 }
	 
     return signc * (du+ fen) ;
}



void calculate_whole_enginhour(){
	
	register GpsContext_t * gc = &GpsContext;
	gc->enginHour+=1;
	if(gc->enginHour > ENGINHOUR_MAXIMUM)
	gc->enginHour = 0;

	write_eh_data(gc->enginHour);
	
}

void open_eh_and_ml_file(void)
{
	register GpsContext_t * gc = &GpsContext;
	
	if(!openfile((uint8_t*)EH_FILE)){
		createfile((uint8_t*)EH_FILE);
		closeFile((uint8_t*) EH_FILE);
	}
	else
	{
	readfile((uint8_t*)EH_FILE);
	gc->enginHour = atoi((char *)getReadData());
	closeFile((uint8_t*) EH_FILE);
	}


	if(!openfile((uint8_t*)ML_FILE)){
		createfile((uint8_t*)ML_FILE);
		closeFile((uint8_t*) ML_FILE);
	}
	else
	{
	readfile((uint8_t*)ML_FILE);
	gc->mileage = atoi((char *)getReadData());
	closeFile((uint8_t*) ML_FILE);
	}

}

void read_eh_ml_data(void)
{
	register GpsContext_t * gc = &GpsContext;
	
	gc->enginHour = HAL_RTCEx_BKUPRead(&RTCHandle, RTC_BKP_DR0);
	
	if(DebugEnable)
			DEBUG("enginHour %d", gc->enginHour);
	
	gc->mileage = HAL_RTCEx_BKUPRead(&RTCHandle, RTC_BKP_DR1);
	
	if(DebugEnable)
			DEBUG("mileage %d", gc->mileage);
}

void write_eh_data(uint32_t value)
{
	HAL_RTCEx_BKUPWrite(&RTCHandle,RTC_BKP_DR0,value);
}

void write_ml_data(uint32_t value)
{
	HAL_RTCEx_BKUPWrite(&RTCHandle,RTC_BKP_DR1,value);
}

uint32_t read_out1_data(void)
{
	return HAL_RTCEx_BKUPRead(&RTCHandle, RTC_BKP_DR2);
}

void write_out1_status(uint32_t value)
{
	HAL_RTCEx_BKUPWrite(&RTCHandle,RTC_BKP_DR2,value);
}

uint32_t read_out2_data(void)
{
	return HAL_RTCEx_BKUPRead(&RTCHandle, RTC_BKP_DR4);
}

void write_out2_status(uint32_t value)
{
	HAL_RTCEx_BKUPWrite(&RTCHandle,RTC_BKP_DR4,value);
}

uint32_t read_message_number(void)
{
	return HAL_RTCEx_BKUPRead(&RTCHandle,RTC_BKP_DR3);
}

void send_message_number(uint32_t value)
{
	HAL_RTCEx_BKUPWrite(&RTCHandle,RTC_BKP_DR3,value);
}

void initOutStatus(void)
{
	if(DebugEnable) DEBUG("%d %d",read_out1_data(),read_out2_data());
	if(read_out1_data() == 1)
		start_pwm_out1();
	else
		stop_pwm_out1();
	if(read_out2_data()==1)
		start_pwm_out2();
	else
		stop_pwm_out2();
}

void open_gefence_file(void)
{
	if(!openfile((uint8_t*)GEOFENCE_DATA)){
		createfile((uint8_t*)GEOFENCE_DATA);
		closeFile((uint8_t*) GEOFENCE_DATA);
	}else
	{
		closeFile((uint8_t*) GEOFENCE_DATA);
	}
	
	geofence_init();
}

void setGc_enginHour(uint8_t* initValue)
{
	register GpsContext_t * gc = &GpsContext;
	gc->enginHour = atoi((char *)initValue);
	write_eh_data(gc->enginHour);
}

void initEH(uint8_t* initValue)
{
	register GpsContext_t * gc = &GpsContext;
	if(gc->enginHour == atoi((char *)initValue)){
	if(DebugEnable)
			DEBUG("gc->enginHour %d", gc->enginHour);
	writefile((uint8_t*) EH_FILE,initValue);
 }
}

void setGc_mileage(uint8_t* initValue)
{
	register GpsContext_t * gc = &GpsContext;
	gc->mileage = atoi((char *)initValue);
	write_ml_data(gc->mileage);
}

void initML(uint8_t* initValue)
{
	register GpsContext_t * gc = &GpsContext;
	if(gc->mileage == atoi((char *)initValue)){
	if(DebugEnable)
			DEBUG("gc->mileage %d", gc->mileage);
	if(DebugEnable)
			DEBUG("mileage %s", initValue);
	writefile((uint8_t*) ML_FILE,initValue);
	}
}

void setTime_noupdate(void)
{
	time_noupdate = 0;
}

/*******************************************************************************
* Function Name	:	vGpsTask
* Description	:	GPS Task
*******************************************************************************/
void vGpsTask(void const *pvArg)
{
	register GpsContext_t * gc = &GpsContext;
	double distance;
	uint32_t overspeed = 0;
	double course;
	bool courseSend = false;
	bool nextCourse = false;
	uint8_t count = 0;
	double lat_accon = 0;
	double lon_accon = 0;
	uint32_t notfixed = 0;
	uint32_t cs = 0;
	uint8_t checkcum[5];
	
#ifdef DISPLAY_STACK
	if(DebugEnable) DEBUG("*** GPS Stack 0:");
#endif

	gc->SendTimer = 5;
	
restart_gps:
		
	if(DebugEnable) DEBUG("restart_gps");
	GpsDeInit(gc);
	
	while ((GlobalStatus & GPS_STOP) ||getWriteFilepStatus()){
		if(GlobalStatus & GSM_WAKEUP)
		{
		GlobalStatus &= ~GSM_WAKEUP;
		GlobalStatus &= ~GPS_STOP;
		}
		osDelay(5000);
	}
	gc->RxMode = GPS_MODE_IGNORE;
	gc->GpsCommand = &GpsCmdGeneral;

	gpsCheckTypeInit(gc, 9600);
	
	#if 0
	while(1)
	{
		if (gpsCheckTypeInit(gc, 115200))
		{
			//if(DebugEnable) DEBUG(" 115200");
			break;
		}

		if (gpsCheckTypeInit(gc, 4800))
		{
			if(DebugEnable) DEBUG(" 4800");
			break;
		}
		if (gpsCheckTypeInit(gc, 9600))
		{
			if(DebugEnable) DEBUG(" 9600");
			break;
		}

		if(DebugEnable) DEBUG(" not found");
		ShowError(ERROR_GPS_INIT_FAIL);
		break;
	}
	#endif

	//if(DebugEnable) DEBUG("gpsCheckTypeInit");
	
	//(*gc->GpsCommand->Init)(gc);

	//fileSystem(false);
	initSpi();
	
	//if(DebugEnable) DEBUG("fileSystem");
	
	GlobalStatus |= GPS_COMPLETE;
	
	if(gc->NoDataTimer > 0)
	initGsmUart();

	gc->NoDataTimer = 0;
	if(isAccOn())
	gc->SendInterval = (uintmax_t) atoi((char *)get_conf_elem(REPINTERVAL_CONF));
	else
	gc->SendInterval  = (uintmax_t) atoi((char *)get_conf_elem(ACCOFFREPINTERVAL_CONF));
	
	gc->RxMode = GPS_MODE_START;

	if(DebugEnable) DEBUG("SendInterval %lld",gc->SendInterval);
	
	
#if ( USE_KALMAN == 1 )
	FilterKalmanInit();
#endif
#if ( USE_AVERAGE == 1 )
	FilterAverageInit(gc);
#endif

	msg_count = 1;
	for(;;)
	{
		if(gpsDataLog) DEBUG("************while gps %d %d ",gpsdata,gc->NoDataTimer);
		osDelay(50);
		if(gpsdata == 0)
		SystemRestart();
		if ((GlobalStatus & GSM_SLEEP)){
			if(!gpsRun)
			gpsRun = true;
			setGpsSleep(true);
			continue;
		}else{
			setGpsSleep(false);
		}
		
		if ((GlobalStatus & GPS_STOP)){
			if(!gpsRun)
			gpsRun = true;
			GlobalStatus &= ~GPS_COMPLETE;
			osDelay(2000); 
			goto restart_gps;
		}
		
		if(isAccOn() && ioffdata == 2)
		{
		if(DebugEnable) DEBUG("erase ioff data");
		ioffdata = 0;
		bit_map_erase_flag_start();
		}
		
		gc->NoDataTimer++;
		if ((gc->NoDataTimer >= 600) || (notfixed >= 400))
		{	// No fix within 5 minutes, restart GPS module
			//HAL_Delay(5000);
			if(!gpsRun)
			gpsRun = true;
			notfixed = 0;
			osDelay(5000);
			goto restart_gps;
		}

		if (!gpsWaitComplete(gc, 5000))		// Wait for packet within 5 seconds
		{	// No data from GPS within 5 seconds
			if(!gpsRun)
			gpsRun = true;
			gc->RxMode = GPS_MODE_START;
			//ShowError(ERROR_GPS_NO_DATA);
			gc->NoDataTimer += 5;
			if(gpsDataLog) DEBUG("gpsWaitComplete %d",gc->NoDataTimer);
			if(gc->NoDataTimer > 300)
				SystemRestart();
			continue;
		}
		if(gpsDataLog) DEBUG("%s",gpsRxBuffer,strlen(gpsRxBuffer));
		
		if(strlen((char *)gpsRxBuffer) == 0)
			goto restart_gps;
		
		if(!strncmp(gpsRxBuffer,"$GPRMC,,V",strlen("$GPRMC,,V"))||
			!strncmp(gpsRxBuffer,"$GNRMC,,V",strlen("$GNRMC,,V"))||
			!strncmp(gpsRxBuffer,"$GBRMC,,V",strlen("$GBRMC,,V")))
			MGA_INI_TIME_UTC();
		
		if (nmea_parse_GPRMC((char*)gpsRxBuffer, gc->RxIndex, &gc->RMC) ||
			  nmea_parse_GNRMC((char*)gpsRxBuffer, gc->RxIndex, &gc->RMC) ||
				nmea_parse_GBRMC((char*)gpsRxBuffer, gc->RxIndex, &gc->RMC) )
		//if (nmea_parse_GPRMC(testGps, strlen(testGps), &gc->RMC))
		{
			//if(DebugEnable) DEBUG("%s",gpsRxBuffer);
			
			cs=gpsRxBuffer[1];
			
			for(uint32_t i = 2;gpsRxBuffer[i]!= '*';i++)
					cs^=gpsRxBuffer[i];
			
			memset(checkcum,0,5);
			memcpy(checkcum,gpsRxBuffer+(gc->RxIndex-4),2);
	
			if(nmea_atoi((char *)checkcum,2,16) != cs){
				if(DebugEnable) DEBUG("gps error");
				if(!gpsRun)
				gpsRun = true;
				goto restart_gps;
			}
			
			if(gpstestmode) 
			{
				gpstestmode = false;
				osDelay(500);
				DEBUG("GPS TEST OK");
			}
			
			if (msg_count++ >= 5 || (!strcmp((char *)get_conf_elem(RESERVE7_CONF),"1") && msg_count > 0 && gc->RMC.status != 'A'))
			{
				msg_count = 0;
				if(getTCPConState() && ((GlobalStatus & VBAT_LOW) == 0))
				handle_locationdata();
			}
				
			gc->RxMode = GPS_MODE_START;

			if (gc->RMC.status != 'A')
			{
				//ShowError(1);	// Position not fixed
				//if(DebugEnable) DEBUG("Position not fixed");
					
				MGA_INI_TIME_UTC();
				if(getTCPConState())
				IF_AGPS();
	
				if(isAccOn() && !gpsFixed)
						notfixed++;
				if(gpsLocationSuccess && isAccOn() && !strncmp(get_conf_elem(ACCSLEEP_CONF),"1",1))
					gpslostEvent(GPS_EVENT);
				gpsLocationSuccess = false;
				if(isAccOn())
				gc->SendInterval =(uintmax_t) atoi((char *)get_conf_elem(REPINTERVAL_CONF));
				else
				gc->SendInterval  = (uintmax_t) atoi((char *)get_conf_elem(ACCOFFREPINTERVAL_CONF));
				if (gc->SendTimer == 0)
				{
						gc->SendTimer = gc->SendInterval;
						if(!strcmp((char *)get_conf_elem(GPS_MOTION_THRESHOLD_CONF),"1")){
						gc->GsmTask.Command = GSM_TASK_CMD_GSMCELL;
						GsmAddTask(&gc->GsmTask);
						}
						
						if(!isAccOn())
						{
						memset(&readGPRMC,0,sizeof(nmeaGPRMC));
						bit_map_read_data_((char *)&readGPRMC,sizeof(nmeaGPRMC));
						if(readGPRMC.lat != 0 && readGPRMC.lon != 0 && readGPRMC.status == 'A'){
						if(DebugEnable) DEBUG("readGPRMC %f",readGPRMC.lat);
						gc->RMC.lat = nmea_ndeg2degree(readGPRMC.lat);
						gc->RMC.lon = nmea_ndeg2degree(readGPRMC.lon);
						gc->RMC.ns =  readGPRMC.ns;
						gc->RMC.ew =  readGPRMC.ew;
						gc->RMC.utc.year =  0;
						memcpy(&gc->GsmTask.GPRMC, &gc->RMC, sizeof(nmeaGPRMC));
						
						if(getTCPConState()){
						gc->GsmTask.Command = GSM_TASK_CMD_GPS;
					
						if (GsmAddTask(&gc->GsmTask) == 0)
						{	// GSM task queue full, show error
							//ShowError(ERROR_GPS_TASK_NOT_ADD);
						}
						}else//else if(GlobalStatus & GSM_COMPLETE)
						{
							//if(!getAccIonState() && !strncmp(get_conf_elem(WORKINGMODE_CONF),"2",1))
							write_locationdata(GSM_TASK_CMD_GPS,NO_ALARM);
							//handle_locationdata();
						}
						}
						//CFC_RST();
						}
						
						if( isAccOn() && !strncmp(get_conf_elem(RESERVE3_CONF),"1",1) && !strncmp(get_conf_elem(GSMSWITCH_CONF),"1",1))
						//if( isAccOn() && !strncmp(get_conf_elem(GSMSWITCH_CONF),"1",1))
						{
						gc->RMC.lat = nmea_ndeg2degree(lastnmeaGPRMC.lat);
						gc->RMC.lon = nmea_ndeg2degree(lastnmeaGPRMC.lon);
						gc->RMC.ns =  lastnmeaGPRMC.ns;
						gc->RMC.ew =  lastnmeaGPRMC.ew;
						gc->RMC.utc.year =  0;
						gc->GsmTask.Result = 2;
						memcpy(&gc->GsmTask.GPRMC, &gc->RMC, sizeof(nmeaGPRMC));
						if(getTCPConState()){
						gc->GsmTask.Command = GSM_TASK_CMD_GPS;
					
						if (GsmAddTask(&gc->GsmTask) == 0)
						{	// GSM task queue full, show error
							//ShowError(ERROR_GPS_TASK_NOT_ADD);
						}
						}else//else if(GlobalStatus & GSM_COMPLETE)
						{
							//if(!getAccIonState() && !strncmp(get_conf_elem(WORKINGMODE_CONF),"2",1))
							write_locationdata(GSM_TASK_CMD_GPS,NO_ALARM);
							//handle_locationdata();
						}
						}
						
				}
				gc->SendTimer--;
				if((gc->SendTimer + 1) == 0)
				gc->SendTimer = 0;
				gc->NoDataTimer = 0;
				time_noupdate++;
				if(time_noupdate > atoi(get_conf_elem(PING_TIME_CONF))*4
					||((!strcmp((char *)get_conf_elem(RESERVE7_CONF),"1") && time_noupdate > atoi(get_conf_elem(PING_TIME_CONF))/10)))
				{
				time_noupdate = 0;
				if(isAccOn() && getTCPConState()){
				gc->GsmTask.Command = GSM_TASK_CMD_NEW_PING;
				GsmAddTask(&gc->GsmTask);
				}
				}
			}
			else
			{
				//if(DebugEnable) DEBUG("%lld",gc->SendTimer);
#if ( USE_AVERAGE == 1 || USE_KALMAN == 1 )
				{
				time_noupdate++;
				if(time_noupdate > atoi(get_conf_elem(PING_TIME_CONF)) * 5)
				{
				time_noupdate = 0;
				if(isAccOn() && getTCPConState()){
				gc->GsmTask.Command = GSM_TASK_CMD_NEW_PING;
				GsmAddTask(&gc->GsmTask);
				}
				}
					
					//gpsFixed = true;
					notfixed = 0;
					
					nmeaGPRMC * rmc = &(gc->RMC);
					
					if(!getCopylatlon()){
					memcpy(&lastnmeaGPRMC, &gc->RMC, sizeof(nmeaGPRMC));
					lastnmeaGPRMC.speed = KMH2KNOT * lastnmeaGPRMC.speed;
					lastnmeaGPRMC.lat = (rmc->ns == 'S') ? -lastnmeaGPRMC.lat : lastnmeaGPRMC.lat;
					lastnmeaGPRMC.lon = (rmc->ew == 'W') ? -lastnmeaGPRMC.lon : lastnmeaGPRMC.lon;
					memcpy(&lastnmeaGPRMC_bakeup, &gc->RMC, sizeof(nmeaGPRMC));
					lastnmeaGPRMC_bakeup.speed = KMH2KNOT * lastnmeaGPRMC_bakeup.speed;
					lastnmeaGPRMC_bakeup.lat = (rmc->ns == 'S') ? -lastnmeaGPRMC_bakeup.lat : lastnmeaGPRMC_bakeup.lat;
					lastnmeaGPRMC_bakeup.lon = (rmc->ew == 'W') ? -lastnmeaGPRMC_bakeup.lon : lastnmeaGPRMC_bakeup.lon;	
					}					
					
					bool use_kalman = true;
					uint32_t time_delta = 0;
					uint32_t time;
					
					//if(!rtc_time && RTC_INIT_SUCCESS()){
					//if(RTC_TIME(rmc->utc) > 0)
						//rtc_time = true;
					//}
					
					if(last_lat != 0 && last_lon != 0)
					{
						if( ((int)last_lat) % ((int)rmc->lat) != 0 || ((int)last_lon) % ((int)rmc->lon) != 0)
						{
						if(!gpsRun)
						gpsRun = true;
						last_lat = rmc->lat;
						last_lon = rmc->lon;
						continue;	
						}
					}
					
					if(!gpsLocationSuccess)
						RTC_TIME(rmc->utc,false);
					else
						RTC_TIME(rmc->utc,true);
					
					double lat= rmc->lat;
					double lon = rmc->lon;
					
					utc_year = rmc->utc.year - 2000;
					
					if(last_lat != 0 && last_lon != 0 && rmc->speed > 2.0){
						
					last_lat = convert_lat_lng_string_to_double(last_lat,rmc->ns);
					last_lon = convert_lat_lng_string_to_double(last_lon,rmc->ew);
					double lat_ = convert_lat_lng_string_to_double(rmc->lat,rmc->ns);
					double lon_ = convert_lat_lng_string_to_double(rmc->lon,rmc->ew);
								
					distance = GetDistance(last_lat, last_lon, lat_, lon_);
					if(distance > 0){
					calculate_whole_mileage(distance);
					}						
					}
					
					last_lat = rmc->lat;
					last_lon = rmc->lon;
					
					
					if(isAccOn() && rmc->speed > 5.0){
					/*
					if((rmc->direction - course) > atof(get_conf_elem(ANGLE_CONF))){
						courseSend = true;
					}
	         else if((rmc->direction - course) < 0.0)
					{
						
						if(course - rmc->direction > atof(get_conf_elem(ANGLE_CONF)))
							courseSend = true;
					}
					*/
						courseSend = heading_compare(&heading,rmc->direction,atof(get_conf_elem(ANGLE_CONF)));
						
						if(lat_course != 0 && lon_course != 0){
							double lat_course_ = convert_lat_lng_string_to_double(lat_course,rmc->ns);
							double lon_course_ = convert_lat_lng_string_to_double(lon_course,rmc->ew);
							double lat_ = convert_lat_lng_string_to_double(rmc->lat,rmc->ns);
							double lon_ = convert_lat_lng_string_to_double(rmc->lon,rmc->ew);
								
							distance = GetDistance(lat_course_, lon_course_, lat_, lon_);
							
							if(nextCourse){
								
								if(distance < 50)
								{
								heading = heading_temp;
								courseSend = false;
								}else{
								nextCourse = false;
								lat_course  = rmc->lat;
								lon_course  = rmc->lon;
								heading  = rmc->direction;
								courseSend = false;
								}
							
							}else{
								if(courseSend){
									lat_course  = rmc->lat;
									lon_course  = rmc->lon;
								}
							}
							
						}else{
							lat_course  = rmc->lat;
							lon_course  = rmc->lon;
						}
						#if 0
						if(courseSend)
						{
							if(lat_course != 0 && lon_course != 0){
							double lat_course_ = convert_lat_lng_string_to_double(lat_course,rmc->ns);
							double lon_course_ = convert_lat_lng_string_to_double(lon_course,rmc->ew);
							double lat_ = convert_lat_lng_string_to_double(rmc->lat,rmc->ns);
							double lon_ = convert_lat_lng_string_to_double(rmc->lon,rmc->ew);
								
							distance = GetDistance(lat_course_, lon_course_, lat_, lon_);
								
							if(distance < 50 && fabs(heading_temp - rmc->direction) < 45){
								//heading = heading_temp;
								lat_course  = rmc->lat;
								lon_course  = rmc->lon;
								courseSend = false;
							}else if(distance < 50 && fabs(heading_temp - rmc->direction) >= 45)
							{
								heading = heading_temp;
								courseSend = false;
							}
							else if(distance > 50 && fabs(heading_temp - rmc->direction) < 40)
							{
								lat_course  = rmc->lat;
								lon_course  = rmc->lon;
								courseSend = false;
							}
							else
							{
							lat_course  = rmc->lat;
							lon_course  = rmc->lon;
							}
							}else{
							lat_course  = rmc->lat;
							lon_course  = rmc->lon;
							}
						}
						#endif
					}
					
					if(!gpsFixed)
						courseSend = true;
					
					gpsFixed = true;
					
					lat_accon = lat = nmea_ndeg2degree((rmc->ns == 'S') ? -lat : lat);
					lon_accon = lon = nmea_ndeg2degree((rmc->ew == 'W') ? -lon : lon);					
					
					time = xTaskGetTickCount() / configTICK_RATE_HZ;
					if (gc->LastFilterTime != 0)
					{
						if (time == gc->LastFilterTime)
						{
							time_delta = 1;
						}
						else if (time > gc->LastFilterTime)
						{
							time_delta = time - gc->LastFilterTime;
						}
						else
						{
							time_delta = gc->LastFilterTime - time;
						}
					}
					gc->LastFilterTime = time;
					
	#if ( USE_AVERAGE == 1 )
					
					FilterAverageUpdate(gc, lat, lon, time_delta);

					if (rmc->speed < AVERAGE_SPEED_5)			// Adaptate average sum with speed
					{
						gc->AverageMaxCount = AVERAGE_SUM_5;
						use_kalman = false;
					}
					else if (rmc->speed < AVERAGE_SPEED_10)
					{
						gc->AverageMaxCount = AVERAGE_SUM_5;
						use_kalman = false;
					}
					else if (rmc->speed < AVERAGE_SPEED_20)
					{
						gc->AverageMaxCount = AVERAGE_SUM_20;
					}
					else
					{
						gc->AverageMaxCount = AVERAGE_SUM_MORE;
					}
					
					FilterAverageGet(gc, &lat, &lon);	// Get avarage position
					gc->RMC.lat = lat;					// and save it
					gc->RMC.lon = lon;
					
					if(isAccOn())
					{
						if(distance  < 1.0)
							rmc->speed = 0;
					}

					course = rmc->direction;
					
					
					rmc->speed = KMH2KNOT * rmc->speed;
					
					if(rmc->speed  > atoi((char *)get_conf_elem(SPEED_CONF)) && !strncmp((char *)get_conf_elem(WARNOVERSPEED_CONF),"1",1)){
						if(overspeed == 0){
							overspeedEvent(OVERSPEED_EVENT);
						}
						  overspeed ++;
						if(overspeed > OVERSPEED_MAXIMUM)
									overspeed = 1;
					}else if(rmc->speed < atoi((char *)get_conf_elem(SPEED_CONF))){
							//overspeed = 0;
					}
					
					if(overspeed > 0)
						overspeed++;
					if(overspeed > atoi((char *)get_conf_elem(OVERSPEED_TIME_CONF)))
						overspeed = 0;
			
					point.lat = rmc->lat;
					point.lng = rmc->lon;
					point.speed = rmc->speed;
					count++;
					if(count == 10){
					count  = 0;
					//pointEvent(POINT_EVENT);
					if(!strncmp((char *)get_conf_elem(WARNEFAN_CONF),"1",1))
						geocheck_check(getPoint());
					}
	#endif
	#if ( USE_KALMAN == 1 )
					if (time_delta == 0)
						time_delta++;
					FilterKalmanUpdate(lat, lon, time_delta);
					if (use_kalman)
					{
						FilterKalmanGet(&(rmc->lat), &(rmc->lon));
					}
	#endif
#endif
				}
				
				if(isAccOn())
				{
				gc->RMC.lat = lat_accon;					
				gc->RMC.lon = lon_accon;
				}
				
				memcpy(&lastnmea_GPRMC, &gc->RMC, sizeof(nmeaGPRMC));
							
				if(!gpsLocationSuccess && isAccOn() && !strncmp(get_conf_elem(ACCSLEEP_CONF),"1",1))
					gpsfixEvent(GPS_EVENT);
				gpsLocationSuccess = true;
				
				if(isAccOn())
				gc->SendInterval =(uintmax_t) atoi((char *)get_conf_elem(REPINTERVAL_CONF));
				else
				gc->SendInterval  = (uintmax_t) atoi((char *)get_conf_elem(ACCOFFREPINTERVAL_CONF));
				
				 if((gc->SendTimer + 1) == 0){
						gc->SendTimer = 0;
				 }
				 	gc->NoDataTimer = 0;
				
				 if(gc->SendTimer == 0){
				 if(!strncmp(get_conf_elem(GSMSWITCH_CONF),"1",1) || !strncmp(get_conf_elem(GSMSWITCH_CONF),"3",1)
					 || !strncmp(get_conf_elem(GSMSWITCH_CONF),"4",1))
					 timetoSend = true;
				 }
				 			 
				 if(!strncmp(get_conf_elem(GSMSWITCH_CONF),"3",1) && isAccOn())
				 {
					 if(!courseSend){
						if(!timetoSend || !distancetoSend)
						{
							if(!gpsRun)
							gpsRun = true;
							gc->SendTimer--;
							if((gc->SendTimer + 1) == 0)
							gc->SendTimer = 0;
							continue;
						}
						
						if(timetoSend && distancetoSend)
						{
							lat_course  = last_lat;
							lon_course  = last_lon;
							heading = gc->RMC.direction;
							nextCourse = true;
						}
					}else
					{
							g_meters = 0;
					}
				 }else if(!strncmp(get_conf_elem(GSMSWITCH_CONF),"3",1) && !isAccOn())
				 {
							if(gc->SendTimer != 0 && timetoSend )
								timetoSend = false;
				 }
				 
				 if(gc->SendTimer == 0)
					 gc->SendTimer = gc->SendInterval;
				 
				//if (GlobalStatus & GSM_LOGIN)
				//{
					if (timetoSend || courseSend || distancetoSend)
					{
						
						//if(gc->SendTimer == 0)
						
						if(!isAccOn())
						{
						memset(&readGPRMC,0,sizeof(nmeaGPRMC));
						bit_map_read_data_((char *)&readGPRMC,sizeof(nmeaGPRMC));
						if(readGPRMC.lat != 0 && readGPRMC.lon != 0 && readGPRMC.status == 'A'){
						if(DebugEnable) DEBUG("readGPRMC %f",readGPRMC.lat);
						gc->RMC.lat = nmea_ndeg2degree(readGPRMC.lat);
						gc->RMC.lon = nmea_ndeg2degree(readGPRMC.lon);
						gc->RMC.ns =  readGPRMC.ns;
						gc->RMC.ew =  readGPRMC.ew;
						}
						}
						
						gc->SendTimer = gc->SendInterval;
						memcpy(&gc->GsmTask.GPRMC, &gc->RMC, sizeof(nmeaGPRMC));
						
						if(courseSend){
							nextCourse = true;
							courseSend = false;
							gc->GsmTask.Result = 4;
						}
						
						if(timetoSend){
							timetoSend = false;
							gc->GsmTask.Result = 2;
						}
						
						if(distancetoSend){
							distancetoSend = false;
							gc->GsmTask.Result = 3;
						}
						
						gc->GsmTask.SenseD1 = (SENS_D1_READ() ? 1 : 0);
						gc->GsmTask.SenseA1 = 0;

#if (USE_CAN_SUPPORT == 1)
						{
							uint8_t idx;
							CanEnable = 0;
							while (!CanDisabled)
								HAL_Delay(1);

							for (idx = 0; idx < CAN_VALUES_SIZE; idx ++)
								gc->GsmTask.CanData[idx] = CanSumGet(idx);

							CanSumInit();
							CanEnable = 1;
						}
#endif
						
						if(getTCPConState()){
						gc->GsmTask.Command = GSM_TASK_CMD_GPS;
					
						if (GsmAddTask(&gc->GsmTask) == 0)
						{	// GSM task queue full, show error
							//ShowError(ERROR_GPS_TASK_NOT_ADD);
						}
						}else//else if(GlobalStatus & GSM_COMPLETE)
						{
							//if(!getAccIonState() && !strncmp(get_conf_elem(WORKINGMODE_CONF),"2",1))
							write_locationdata(GSM_TASK_CMD_GPS,NO_ALARM);
							//handle_locationdata();
						}
#ifdef DISPLAY_STACK
						if(DebugEnable)  DEBUG("*** GPS Stack 1:");
#endif
					}
					
					//if(gc->SendTimer != 0 && gc->SendTimer%9 == 0 && getTCPConState())
								//handle_locationdata();
					
					gc->SendTimer--;
					 if((gc->SendTimer + 1) == 0)
						gc->SendTimer = 0;
				//}
			}
		}
		else{
			gc->RxMode = GPS_MODE_START;
			time_noupdate++;
			if(isAccOn() && !gpsFixed)
				notfixed++;
			if(time_noupdate > atoi(get_conf_elem(PING_TIME_CONF))*4)
			{
				time_noupdate = 0;
				if(isAccOn() && getTCPConState()){
				gc->GsmTask.Command = GSM_TASK_CMD_NEW_PING;
				GsmAddTask(&gc->GsmTask);
			}
			}
			//MGA_INI_TIME_UTC();
		}
		
		if(nmea_parse_GPGSV((char*)gpsRxBuffer, gc->RxIndex, &gc->GSV)
			|| nmea_parse_GLGSV((char*)gpsRxBuffer, gc->RxIndex, &gc->GSV)
			|| nmea_parse_GBGSV((char*)gpsRxBuffer, gc->RxIndex, &gc->GSV))
		{
			GSV.sat_count = gc->GSV.sat_count;
		}
		
		if(nmea_parse_GPGSA((char*)gpsRxBuffer, gc->RxIndex, &gc->GSA)
			|| nmea_parse_GNGSA((char*)gpsRxBuffer, gc->RxIndex, &gc->GSA)
			|| nmea_parse_GBGSA((char*)gpsRxBuffer, gc->RxIndex, &gc->GSA))
		{
			GSA.HDOP = gc->GSA.HDOP;
		}
		
		if(!gpsRun)
		gpsRun = true;
	}
}

/*******************************************************************************
* Function Name	:	GPS_USART_IRQHandler
* Description	:	GPS USART interrupt handler
*******************************************************************************/
void gpsData(uint8_t data)
{	
	register GpsContext_t *gc = &GpsContext;
		
		gpsdata++;
	  if(gpsdata > 48)
			gpsdata = 1;
	
		gc->Data = data;
		
		if (gc->RxMode == GPS_MODE_IGNORE)
		{
			return;
		}
		else if (gc->RxMode == GPS_MODE_END
			||	gc->RxMode == GPS_MODE_END2)
		{
			if (gc->RxIndex > (sizeof(gpsRxBuffer) - 2))
			{	// String too long, ignore it and set wait a $
				gc->RxMode = (gc->RxMode == GPS_MODE_END2) ? GPS_MODE_CHECK : GPS_MODE_START;
			}
			else
			{
				if (gc->Data == '$')
					gc->RxIndex = 0;
					
				gpsRxBuffer[gc->RxIndex++] = gc->Data;
				if (gc->WaitAliveSize != 0)
				{
					if (gc->WaitAliveSize == gc->RxIndex)
					{
						if (strncmp((char*)gpsRxBuffer, (char*)gc->WaitAliveText, gc->WaitAliveSize) == 0)
							gc->WaitAliveSize = 0;
						else
							gc->RxMode = (gc->RxMode == GPS_MODE_END2) ? GPS_MODE_CHECK : GPS_MODE_START;
					}
					else if (gc->Data == '\n')
					{
						gc->RxMode = (gc->RxMode == GPS_MODE_END2) ? GPS_MODE_CHECK : GPS_MODE_START;
					}
				}
				else if (gc->Data == '\n')
				{
					if (GPS_MODE_END2 || (gc->RxIndex > 6 && strncmp((char*)gpsRxBuffer, "$GPRMC", 6) == 0) 
						|| (gc->RxIndex > 6 && strncmp((char*)gpsRxBuffer, "$GNRMC", 6) == 0)
							|| (gc->RxIndex > 6 && strncmp((char*)gpsRxBuffer, "$GBRMC", 6) == 0))
					{
						gpsRxBuffer[gc->RxIndex] = 0;
						gc->RxMode = GPS_MODE_READY;
					}
					else
					{
						gc->RxMode = (gc->RxMode == GPS_MODE_END2) ? GPS_MODE_CHECK : GPS_MODE_START;
					}
				}
			}
		}
		else if (gc->RxMode == GPS_MODE_START || gc->RxMode == GPS_MODE_CHECK)
		{
			if (gc->Data == '$')
			{
				gc->RxIndex = 0;
				gpsRxBuffer[gc->RxIndex++] = gc->Data;
				gc->RxMode = (gc->RxMode == GPS_MODE_CHECK) ? GPS_MODE_CHECK2 : GPS_MODE_END;
			}
		}
		else if (gc->RxMode == GPS_MODE_CHECK2)
		{
			gpsRxBuffer[gc->RxIndex++] = gc->Data;
			if (gc->RxIndex == 3)
			{
				if (gpsRxBuffer[0] == '$'
				&&	gpsRxBuffer[1] == 'G'
				&&	gpsRxBuffer[2] == 'P'
					)
					gc->RxMode = GPS_MODE_END2;
				else if(gpsRxBuffer[0] == '$'
				&&	gpsRxBuffer[1] == 'G'
				&&	gpsRxBuffer[2] == 'N'
				)
				gc->RxMode = GPS_MODE_END2;
				else if(gpsRxBuffer[0] == '$'
				&&	gpsRxBuffer[1] == 'G'
				&&	gpsRxBuffer[2] == 'B'
				)
				gc->RxMode = GPS_MODE_END2;
				else
					gc->RxMode = GPS_MODE_CHECK;
			}
		}
}

bool rtc_set(void)
{
	return rtc_time;
}


bool GpsNoData()
{
 //register GpsContext_t *gc = &GpsContext;
	//if(DebugEnable) DEBUG("***************gc->RMC.status:%c",gc->RMC.status);
 //return gc->RMC.status != 'A';
	return gpsLocationSuccess == false;
}

double getGpsSpeed(void)
{
	return lastnmea_GPRMC.speed;
}

nmeaGPGSV* getLastGPGSVData(void)
{
	//register GpsContext_t *gc = &GpsContext;
	return &GSV;
}


nmeaGPGSA* getLastGPGSAData(void)
{
	//register GpsContext_t *gc = &GpsContext;
	return &GSA;
}

nmeaGPRMC* getLastGPRMCData()
{
	//register GpsContext_t *gc = &GpsContext;
	//return &(gc->RMC);
	return &lastnmea_GPRMC;
}

nmeaGPRMC* getGPRMCData(uint8_t event_type)
{
	//register GpsContext_t *gc = &GpsContext;
	//return &(gc->RMC);
	
	if(DebugEnable) DEBUG("%f",lastnmeaGPRMC.lat);
	
	if(!strncmp(get_conf_elem(RESERVE3_CONF),"1",1) && getAccIonState()){
		write_ml_data(0);
		write_eh_data(0);
	}
	
	if(getAccIonState()){
		gpsFixed = false;
		msg_count = 0;
	}
	
	if(getAccIoffState() && lastnmeaGPRMC.lat != 0 && lastnmeaGPRMC.lon != 0
		&& lastnmeaGPRMC.status == 'A'){
		ioffdata = 1;
		bit_map_write_data_((char *)&(lastnmeaGPRMC),sizeof(nmeaGPRMC));
	}else if(getAccIonState())
	{
		memset(&readGPRMC,0,sizeof(nmeaGPRMC));
		bit_map_read_data_((char *)&readGPRMC,sizeof(nmeaGPRMC));
		if(readGPRMC.lat != 0 && readGPRMC.lon != 0 && readGPRMC.status == 'A'){
		memcpy(&lastnmeaGPRMC, &readGPRMC, sizeof(nmeaGPRMC));
		readGPRMC.utc.year = 0;
		lat_course  = readGPRMC.lat;
		lon_course  = readGPRMC.lon;
		heading = readGPRMC.direction;
		if(DebugEnable) DEBUG("readGPRMC %f",readGPRMC.lat);
		ioffdata = 2;
		
		return &readGPRMC;
	}
	}else if(!isAccOn() && event_type != GSM_TASK_CMD_ALARM)
	{
		memset(&readGPRMC,0,sizeof(nmeaGPRMC));
		bit_map_read_data_((char *)&readGPRMC,sizeof(nmeaGPRMC));
		if(readGPRMC.lat != 0 && readGPRMC.lon != 0 && readGPRMC.status == 'A'){
		memcpy(&lastnmeaGPRMC, &readGPRMC, sizeof(nmeaGPRMC));
		readGPRMC.utc.year = 0;
		lat_course  = readGPRMC.lat;
		lon_course  = readGPRMC.lon;
		heading = readGPRMC.direction;
		return &readGPRMC;
	}
	}
	
	if(getAccIonState() && ioffdata == 1)
			ioffdata = 2;
	
	memcpy(&lastnmeaGPRMC, &lastnmeaGPRMC_bakeup, sizeof(nmeaGPRMC));
	lastnmeaGPRMC.lat = lastnmeaGPRMC_bakeup.lat;
	lastnmeaGPRMC.lon = lastnmeaGPRMC_bakeup.lon;
	
	if(GpsNoData() && !getAccIoffState())
	lastnmeaGPRMC.utc.year = 0;
	
	return &lastnmeaGPRMC;
}


void keepLastnmeaGPRMC(void)
{
	if(getAccIoffState() && lastnmeaGPRMC.lat != 0 && lastnmeaGPRMC.lon != 0
		&& lastnmeaGPRMC.status == 'A'){
		bit_map_write_data_((char *)&(lastnmeaGPRMC),sizeof(lastnmeaGPRMC));
	}
}



point_t* getPoint()
{
	double degree ; 
  double minute; 
	
	point.lat = nmea_degree2ndeg(fabs(point.lat));
	point.lng = nmea_degree2ndeg(fabs(point.lng));
	degree = floor(point.lat / 100);
  minute = point.lat - degree * 100;
  point.lat= degree + minute / 60;
	
	degree = floor(point.lng / 100);
  minute = point.lng - degree * 100;
  point.lng = degree + minute / 60;
	
	if(lastnmeaGPRMC.ns == 'S')
	point.lat = -point.lat;
	if(lastnmeaGPRMC.ew == 'W')
	point.lng = -point.lng;

	point.lat1=0.0;
	point.lng1=0.0;
	return &point;
}


bool heading_compare(double* sourcepoint,double comparepoint,double angle)
{
	if(fabs(*sourcepoint - comparepoint) > angle)
	{
		heading_temp = *sourcepoint;
		*sourcepoint = comparepoint;
		return true;
	}		
	return false;
}



int getEnginHour(void)
{
	register GpsContext_t *gc = &GpsContext;
	return gc->enginHour;
}

int getMileage(void)
{
	register GpsContext_t *gc = &GpsContext;
	return gc->mileage;
}

bool Gps_Ignore()
{
	register GpsContext_t *gc = &GpsContext;
	return gc->RxMode == GPS_MODE_IGNORE;
}

void setGpsSendTimer(uint8_t event)
{
	register GpsContext_t *gc = &GpsContext;
	if(event == ACC_EVENT_ON)
	gc->SendInterval =(uintmax_t) atoi((char *)get_conf_elem(REPINTERVAL_CONF));
	else
	gc->SendInterval  = (uintmax_t) atoi((char *)get_conf_elem(ACCOFFREPINTERVAL_CONF));
	gc->SendTimer = gc->SendInterval - 1;
}

uint8_t GpsGeneral_Init(GpsContext_t * gc)
{
	return E_OK;
}

uint8_t GpsMTK_Init(GpsContext_t * gc)
{
	GpsPutLineCS(GPS_SET_MTK0);
	HAL_Delay(10);
	return E_OK;
}

uint8_t gpsSendBatch(const pint8_t * cmds)
{
	while (*cmds++ != NULL)
	{
		HAL_Delay(500);
		GpsPutLineCS(*cmds);
	}
	HAL_Delay(10);
	return E_OK;
}

uint8_t GpsSiRF_Init(GpsContext_t * gc)
{
	return gpsSendBatch(GPS_SET_SIRFS);
}

uint8_t GpsSTM_Init(GpsContext_t * gc)
{
	return gpsSendBatch(GPS_SET_STM);
}

uint8_t GpsML8088_Init(GpsContext_t * gc)
{
	return gpsSendBatch(GPS_SET_STM);
}

void setGpsSleep(bool sleep)
{
 gps_gotoSleep = sleep;
}

bool getGpsSleep()
{
 return gps_gotoSleep;
}


void gpsPowerOff(void)
{
	GPIO_InitTypeDef  GPIO_InitStruct;
	
	__HAL_RCC_GPIOC_CLK_ENABLE();
	GPIO_InitStruct.Pin       = GPIO_PIN_10;
  GPIO_InitStruct.Mode      = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull      = GPIO_NOPULL;
  GPIO_InitStruct.Speed     = GPIO_SPEED_FREQ_VERY_HIGH;
	
	HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);
	HAL_GPIO_WritePin(GPIOC, GPIO_PIN_10, GPIO_PIN_RESET);
}


void gpsPowerOn(void)
{
	GPIO_InitTypeDef  GPIO_InitStruct;
	
	__HAL_RCC_GPIOC_CLK_ENABLE();
	GPIO_InitStruct.Pin       = GPIO_PIN_10;
  GPIO_InitStruct.Mode      = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull      = GPIO_NOPULL;
  GPIO_InitStruct.Speed     = GPIO_SPEED_FREQ_VERY_HIGH;
	
	HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);
	HAL_GPIO_WritePin(GPIOC, GPIO_PIN_10, GPIO_PIN_SET);
	
}


void gpsDebugEnable(bool enable)
{
	DebugEnable = enable;
}

void gpsDataLogEnable(bool enable)
{
	gpsDataLog = enable;
}

