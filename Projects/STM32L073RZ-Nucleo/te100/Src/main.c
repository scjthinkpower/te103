#include <string.h>
#include <stdint.h>

#include "main.h"
#include "hardware.h"
#include "debug.h"
#include "utility.h"
#include "eeprom.h"
#include "config_file.h"
  
#include "FreeRTOS.h"
#include "task.h"
#include "semphr.h"
#include "gps.h"
#include "gsm.h"
#include "gsensor.h"
#include "work.h"
#include "acc.h"
#include "can.h"
#include "gpsOne.h"
#include "nmea/tok.h"
#include "bitmap_flash.h"

#define SOFTWARE_VERSION   "te101-v1.6.57"

void SystemClock_Config(void);
uint8_t HardwareInit(void);

volatile uint16_t GlobalStatus;

bool mainActionDone = false;

/**
  * @brief  System Clock Configuration
  *         The system Clock is configured as follow : 
  *            System Clock source            = PLL (HSE)
  *            SYSCLK(Hz)                     = 32000000
  *            HCLK(Hz)                       = 32000000
  *            AHB Prescaler                  = 1
  *            APB1 Prescaler                 = 1
  *            APB2 Prescaler                 = 1
  *            Flash Latency(WS)              = 1
  *            Main regulator output voltage  = Scale1 mode
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_ClkInitTypeDef RCC_ClkInitStruct ={0};
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  
  /* Enable Power Control clock */
  __HAL_RCC_PWR_CLK_ENABLE();
  
  /* The voltage scaling allows optimizing the power consumption when the device is 
     clocked below the maximum system frequency, to update the voltage scaling value 
     regarding system frequency refer to product datasheet.  */
  __HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE1);
  
  /* Disable Power Control clock */
  __HAL_RCC_PWR_CLK_DISABLE();
  
  /* Enable HSE Oscillator */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSE;
  RCC_OscInitStruct.HSEState = RCC_HSE_ON;
  RCC_OscInitStruct.PLL.PLLSource   = RCC_PLLSOURCE_HSE;
  RCC_OscInitStruct.PLL.PLLState    = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLMUL      = RCC_PLLMUL_4;
  RCC_OscInitStruct.PLL.PLLDIV      = RCC_PLLDIV_2;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct)!= HAL_OK)
  {
    /* Initialization Error */
    while(1)
    {}
  }
  
  /* Select PLL as system clock source and configure the HCLK, PCLK1 and PCLK2 
     clocks dividers */
  RCC_ClkInitStruct.ClockType = (RCC_CLOCKTYPE_SYSCLK | RCC_CLOCKTYPE_HCLK | RCC_CLOCKTYPE_PCLK1 | RCC_CLOCKTYPE_PCLK2);
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV1;  
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;  
  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_1)!= HAL_OK)
  {
    /* Initialization Error */
    while(1)
    {}
  }
}

bool getMainActionDone(void)
{
return mainActionDone;
}

int action(void)
{
	if(HardwareInit()== ERROR)
		return ERROR;

	if(!init_sw_conf_table()){
		if(DebugEnable) DEBUG("create conf table");
		if(!create_sw_conf_table()){
		if(DebugEnable) DEBUG("create conf table fail");
		}
	}
	
	if(strcmp(get_conf_elem(SW_VERSION_CONF),SOFTWARE_VERSION)){
	if(DebugEnable) DEBUG("update version number");
	update_conf_elem(SW_VERSION_CONF,SOFTWARE_VERSION);
	}
	show_sw_conf();

	//fileSystem(true);
	bit_map_init();

	mainActionDone = true;
	return SUCCESS;
}

void taskRun(void)
{
	#if 1
	initWWDG() ;
	
	/*  WORK_THREAD  definition */
  osThreadDef(WORK_THREAD, vWorkTask, osPriorityNormal, 0, 1.5*configMINIMAL_STACK_SIZE);
	
	 /* Start thread WORK */
	osThreadCreate(osThread(WORK_THREAD), NULL); 
	
	/*  G_SENSOR  definition */
	osThreadDef(GSENSOR_THREAD, vGsensor, osPriorityNormal, 0, configMINIMAL_STACK_SIZE/3);
	
	osThreadCreate(osThread(GSENSOR_THREAD), NULL);

	/* GPS_THREAD definition */
  osThreadDef(GPS_THREAD, vGpsTask, osPriorityNormal, 0, 6*configMINIMAL_STACK_SIZE/4);
	
	/* Start thread GPS */
  osThreadCreate(osThread(GPS_THREAD), NULL);
	
  /*  GSM_THREAD  definition */
  osThreadDef(GSM_THREAD, vGsmTask, osPriorityNormal, 0, 2*configMINIMAL_STACK_SIZE);
	
	/* Start thread GSM */
  osThreadCreate(osThread(GSM_THREAD), NULL); 
	
		/* DEBUG_THREAD definition */
  osThreadDef(DEBUG_THREAD, vDebugTask, osPriorityNormal, 0, 7*configMINIMAL_STACK_SIZE/8);
	
	/* Start thread DEBUG */
  osThreadCreate(osThread(DEBUG_THREAD), NULL);

	startWWDG() ;
	 
  /* Start scheduler */
  osKernelStart();

  //ShowFatalError(ERROR_TASK_FAILED);

  /* We should never get here as control is now taken by the scheduler */
 #endif 
}
/*******************************************************************************
 * Function Name	:	main
 * Description	:	Main program
 *******************************************************************************/
int main(void)
{

	GlobalStatus	= 0;
	action();
	//HAL_Delay(3000);
	//sleep();
	//RTC_Config();
	//RTC_WakeUp();
	taskRun();
	for (;;);
}


void vApplicationTickHook( void ) {
   //refreshIWDG();
		refreshWWDG();
}


	
void mainOneDebugEnable(bool enable)
{
	DebugEnable = enable;
}

/*******************************************************************************
* Function Name	:	SystemRestart
* Description	:	Make MCU restart
*******************************************************************************/
void SystemRestart(void)
{
	//keepLastnmeaGPRMC();
	__disable_irq();
	HAL_NVIC_SystemReset();
	__enable_irq();
}

/*******************************************************************************
* Function Name	:	Exception_Handler
* Description	:	Unknown exception handler
*******************************************************************************/
void Exception_Handler(void)
{
	ShowFatalError(ERROR_UNKNOWN_EXCEPTION);
}

/*******************************************************************************
* Function Name	:	Exception_Handler
* Description	:	Unknownn interrupt handler
*******************************************************************************/
void Default_Handler(void)
{
	ShowFatalError(ERROR_UNKNOWN_INTERRUPT);
}

/*******************************************************************************
* Function Name	:	ShowFatalError
* Description	:	Show fatal errors and make a SystemReset
* Input			:	error code
*******************************************************************************/
void ShowFatalErrorOnly(uint8_t errorCode)
{
	#if 0
	uint8_t count;

	__disable_irq();

	count = (errorCode & 0xF0) >> 4;
	if (count != 0)
	{
		HAL_Delay(1000);
		while(count-- != 0)
		{
			HAL_Delay(30);
			HAL_Delay(500);
		}
		HAL_Delay(500);
	}
	count = errorCode & 0x0F;
	if (count != 0)
	{
		HAL_Delay(500);
		while(count-- != 0)
		{
			HAL_Delay(30);
			HAL_Delay(500);
		}
		HAL_Delay(1000);
	}
	#endif
}

/*******************************************************************************
* Function Name	:	ShowFatalError
* Description	:	Add error to queue
* Input			:	error code
*******************************************************************************/
void ShowFatalError(uint8_t errorCode)
{
	//ShowFatalErrorOnly(errorCode);
	SystemRestart();
}


/*******************************************************************************
* Function Name	:	vLEDTask
* Description	:	LED Task
*******************************************************************************/
void SetLedWithoutKey(uint8_t state)
{

}

/*******************************************************************************
* Function Name	:	ShowError
* Description	:	Add error to queue
* Input			:	error code
*******************************************************************************/
void ShowError(uint8_t errorCode)
{
	#if 0
	uint8_t count;

	count = (errorCode & 0xF0) >> 4;
	if (count != 0)
	{
		HAL_Delay(1000);
		while(count-- != 0)
		{
			SetLedWithoutKey(SET);
			HAL_Delay(50);
			SetLedWithoutKey(RESET);
			HAL_Delay(500);
		}
	}
	count = errorCode & 0x0F;
	if (count == 1)
	{
		SetLedWithoutKey(SET);
		HAL_Delay(50);
		SetLedWithoutKey(RESET);
	}
	else
	{
		if (count > 1)
		{
			HAL_Delay(1000);
			while(count-- != 0)
			{
				SetLedWithoutKey(SET);
				HAL_Delay(50);
				SetLedWithoutKey(RESET);
				HAL_Delay(500);
			}
		}
		HAL_Delay(1000);
	}
	#endif
}



/*******************************************************************************
* Function Name	:	HardwareInit
* Description	:	Initialize hardware
*******************************************************************************/
uint8_t HardwareInit(void)
{

	HAL_Init();

	SystemClock_Config();
	
#if (USE_SERIAL_DEBUG)
	
	DebugInit();
	
	LpUartInit();
	
	initGsensor();
	
	initSpi();
	
	initTimPwm();
	
#endif
	
	return 1;
}

/**
  * @brief  Pre Sleep Processing
  * @param  ulExpectedIdleTime: Expected time in idle state
  * @retval None
  */
void PreSleepProcessing(uint32_t * ulExpectedIdleTime)
{
  /* Called by the kernel before it places the MCU into a sleep mode because
  configPRE_SLEEP_PROCESSING() is #defined to PreSleepProcessing().

  NOTE:  Additional actions can be taken here to get the power consumption
  even lower.  For example, peripherals can be turned off here, and then back
  on again in the post sleep processing function.  For maximum power saving
  ensure all unused pins are in their lowest power state. */

  /* 
    (*ulExpectedIdleTime) is set to 0 to indicate that PreSleepProcessing contains
    its own wait for interrupt or wait for event instruction and so the kernel vPortSuppressTicksAndSleep 
    function does not need to execute the wfi instruction  
  */
  *ulExpectedIdleTime = 0;
  
  /*Enter to sleep Mode using the HAL function HAL_PWR_EnterSLEEPMode with WFI instruction*/
  HAL_PWR_EnterSLEEPMode(PWR_MAINREGULATOR_ON, PWR_SLEEPENTRY_WFI);  
}

/**
  * @brief  Post Sleep Processing
  * @param  ulExpectedIdleTime: Not used
  * @retval None
  */
void PostSleepProcessing(uint32_t * ulExpectedIdleTime)
{
  /* Called by the kernel when the MCU exits a sleep mode because
  configPOST_SLEEP_PROCESSING is #defined to PostSleepProcessing(). */

  /* Avoid compiler warnings about the unused parameter. */
  (void) ulExpectedIdleTime;
}

