#include "config_file.h"
#include "stdio.h"
#include "string.h"
#include "stm32l0xx_hal.h"
#include "eeprom.h"
#include "debug.h"
#include "geofence.h"

#define PRODUCT_SN_NUMBER_LENGTH  20
#define TELE_NUMBER_LENGTH  20  
#define DOMAIN_LENGTH            50 
#define IP_ADDRESS_LENGTH       30
#define PORT_NUMBER_LENGTH           10
#define APN_LENGTH                   10
#define APN_NAME_LENGTH        40
#define APN_USER_LENGTH            40
#define APN_PASSWORD_LENGTH   40
#define LANGUAGE_LENGTH               10
#define REPORT_INTERVAL_TIME       10
#define GPS_MOTION_ThRESHOLD_LENGTHs      10   // in units of meter
#define SWITCH_LENGTH    2
#define VERSION_LENGTH    20
#define SPEED_LENGTH    10
#define TIMEZONE_LENGTH    10
#define OTA_LENGTH      15
#define SIMULATEACC_LENGTH      30
#define ANGLE_LENGTH 10
#define DISTANCE_LENGTH 10
#define OVERSPEEDTIME_LENGTH 10
#define PINGTIME_LENGTH 10
#define RESERVE_LENGTH 10


#define SHELL_LIB_BUF_SIZE 800
#define BUF_SIZE 50

#define CONFIG_CR           '\r'
#define CONFIG_CR_STRING "\r"
#define CONFIG_LN           '\n'
#define CONFIG_LN_STRING "\n"
#define CONFIG_PAD_CHARACTER ' '
#define CONFIG_PAD_STRING " "


#define CONFIG_FILE_SPACE_CHARACTER ' '
#define CONFIG_FILE_EQUAL_STRING "="
#define CONFIG_FILE_EQUAL_CHARACTER '='
#define CONFIG_FILE_YINHAO_STRING "\""
#define CONFIG_PARA_SPLITTER_CHARACTER ','
#define CONFIG_PARA_SPLITTER_STRING ","

uint8_t shellLibBuf[BUF_SIZE + 1] ; 
uint8_t dataBuf[SHELL_LIB_BUF_SIZE + 1] ; 
uint8_t *parray = shellLibBuf ; 
uint8_t *farray  = dataBuf;
uint8_t rtext[10];

typedef enum Work_Mode_E{
    WORK_MODE_NORMAL = 0 , 
    WORK_MODE_INTERVAL = 1 
}work_mode_e ; 

typedef enum {
    GSM_CELL_SWITCH_ON = '1' ,
    GSM_CELL_SWITCH_OFF = '0' 
}gsm_cell_switch_e ; 


typedef enum {
    ACC_SLEEP_ON = '1' ,
    ACC_SLEEP_OFF = '0'
}acc_sleep_enable_e ; 

typedef enum {
    ACC_STATE_HINT_ON = '1' ,
    ACC_STATE_HINT_OFF = '0'
}acc_status_hint_e ; 

typedef enum {
    WARN_VIRBRATE_ON = '1', 
    WARN_VIRBRATE_OFF = '0'
}warn_shock_enable_e ; 

typedef enum {
    WARN_LOW_BATT_ON = '1' ,
    WARN_LOW_BATT_OFF = '0'
}warn_low_batt_enable_e ;

typedef enum {
    WARN_URGENT_ON = '1' , 
    WARN_URGENT_OFF = '0'
}warn_urgent_enable_e ; 

typedef enum {
    WARN_CIRCLE_ON = '1' ,
    WARN_CIRCLE_OFF = '0'
}warn_circle_enable_e ; 

typedef enum {
    WARN_OVER_SPEED_ON = '1',
    WARN_OVER_SPEED_OFF = '0'
}warn_over_speed_enable_e ; 

typedef enum {
    WARN_EFAN_ON = '1' ,
    WARN_EFAN_OFF = '0'
}warn_efan_enable_e ; 


typedef enum {
    SENSORTIVITY_LOW = '0' , 
    SENSORTIVITY_MID  = '1' , 
    SENSORTIVITY_HIGH = '2' , 
    SENSORTIVITY_NONE = 'f' ,
}gsensor_sensortivity_e ;


typedef struct Sw_Conf_S {
		uint8_t Iap[OTA_LENGTH] ;
    uint8_t Sn[PRODUCT_SN_NUMBER_LENGTH] ; // the sn number will be used in large area 
    
    uint8_t SosTeleNum[TELE_NUMBER_LENGTH] ;            // the main sos telephone number
    uint8_t Sos1TeleNum[TELE_NUMBER_LENGTH] ;         //  the second sos telephone number
    uint8_t Sos2TeleNum[TELE_NUMBER_LENGTH] ;         // the third sos telephone number
    uint8_t Sos3TeleNum[TELE_NUMBER_LENGTH]  ;        // the fourth sos telephone number 

    uint8_t MainServIpaddr[IP_ADDRESS_LENGTH] ;       // the main server ip address 
    uint8_t MainServPortNum[PORT_NUMBER_LENGTH] ;  // the main sever port number based on TCP connect
    uint8_t MainServDomainName[DOMAIN_LENGTH] ;     // the main server 's domain name used to replace the ip connect
    
    uint8_t ApnName[APN_NAME_LENGTH] ;                     // the APN NAME means the access point's name
    uint8_t ApnUser[APN_USER_LENGTH] ;                       // the APN user menas the access point's user name
    uint8_t ApnPass[APN_PASSWORD_LENGTH] ;              // the APN password means the access point's password
    uint8_t RepInterval[REPORT_INTERVAL_TIME] ;         //  the report interval of the tracker in the normal working mode
		uint8_t	AccoffRepInterval[REPORT_INTERVAL_TIME] ;	
	
		gsm_cell_switch_e  GsmSwitch[2] ;   
		work_mode_e WorkMode[2] ;  
	
		acc_sleep_enable_e AccSleep[2] ; 
    acc_status_hint_e  AccStatusHint[2] ; 
    warn_shock_enable_e WarnShockEnable[2] ;
    warn_efan_enable_e WarnEfanEnable[2] ;
    warn_low_batt_enable_e WarnLowBattEnable[2];
    warn_over_speed_enable_e WarnOverSpeedEnable[2];
    warn_urgent_enable_e WarnUrgentEnable[2] ;
    warn_circle_enable_e WarnCircleEanble[2] ; 
		
		uint8_t GpsMotionThreshold[GPS_MOTION_ThRESHOLD_LENGTHs] ;  // the gps motions limit value
	
    uint8_t Version[VERSION_LENGTH] ; 
    uint8_t Speed[SPEED_LENGTH] ; 
    uint8_t Gsensor[SWITCH_LENGTH] ; 
    uint8_t Timezone[TIMEZONE_LENGTH] ; 
		uint8_t SimulateAcc[SIMULATEACC_LENGTH] ; 
		uint8_t angle[ANGLE_LENGTH] ;
		uint8_t distance[DISTANCE_LENGTH] ;
		uint8_t overspeedtime[OVERSPEEDTIME_LENGTH] ;
		uint8_t pingtime[PINGTIME_LENGTH] ;
		uint8_t reserve1[RESERVE_LENGTH] ;
		uint8_t reserve2[RESERVE_LENGTH] ;
		uint8_t reserve3[RESERVE_LENGTH] ;
		uint8_t reserve4[RESERVE_LENGTH] ;
		uint8_t reserve5[RESERVE_LENGTH] ;
		uint8_t reserve6[RESERVE_LENGTH] ;
		uint8_t reserve7[RESERVE_LENGTH] ;
		uint8_t reserve8[RESERVE_LENGTH] ;
} sw_conf_t ;


typedef struct Conf_Map_S {
    const uint8_t *name ; 
    void *val ; 
    int len ; 
}conf_map_t ;

sw_conf_t swConf = {0};

conf_map_t swMap[] = {
		{IAP_CONF, &swConf.Iap, sizeof(swConf.Iap)/sizeof(char)},
    {SN_CONF, &swConf.Sn, sizeof(swConf.Sn)/sizeof(char)},
    {SOSTELENUMBER_CONF, &swConf.SosTeleNum, sizeof(swConf.SosTeleNum)/sizeof(char)} ,
    {SOS1TELENUMBER_CONF, &swConf.Sos1TeleNum, sizeof(swConf.Sos1TeleNum)/sizeof(char)},
    {SOS2TELENUMBER_CONF, &swConf.Sos2TeleNum, sizeof(swConf.Sos2TeleNum)/sizeof(char)},
    {SOS3TELENUMBER_CONF, &swConf.Sos3TeleNum, sizeof(swConf.Sos3TeleNum)/sizeof(char)},
    {MAINSERVERIPADDR_CONF, &swConf.MainServIpaddr, sizeof(swConf.MainServIpaddr)/sizeof(char)},
    {MAINSERVERPORTNUMBER_CONF, &swConf.MainServPortNum, sizeof(swConf.MainServPortNum)/sizeof(char)},
    {MAINSERVERDOMAINNAME_CONF, &swConf.MainServDomainName, sizeof(swConf.MainServDomainName)/sizeof(char)},
    {APNNAME_CONF, &swConf.ApnName, sizeof(swConf.ApnName)/sizeof(char)},
    {APNUSER_CONF, &swConf.ApnUser, sizeof(swConf.ApnUser)/sizeof(char)},
    {APNPASS_CONF, &swConf.ApnPass, sizeof(swConf.ApnPass)/sizeof(char)},
    {REPINTERVAL_CONF, &swConf.RepInterval, sizeof(swConf.RepInterval)/sizeof(char)},
		{ACCOFFREPINTERVAL_CONF, &swConf.AccoffRepInterval, sizeof(swConf.AccoffRepInterval)/sizeof(char)},
   
		{GSMSWITCH_CONF, &swConf.GsmSwitch, sizeof(swConf.GsmSwitch)/sizeof(char)},
    {WORKINGMODE_CONF, &swConf.WorkMode, sizeof(swConf.WorkMode)/sizeof(char)},    
    {ACCSLEEP_CONF, &swConf.AccSleep, sizeof(swConf.AccSleep)/sizeof(char)},
    {ACCSTATUSHINT_CONF, &swConf.AccStatusHint, sizeof(swConf.AccStatusHint)/sizeof(char)},
    {WARNSHOCK_CONF, &swConf.WarnShockEnable, sizeof(swConf.WarnShockEnable)/sizeof(char)},
    {WARNEFAN_CONF, &swConf.WarnEfanEnable, sizeof(swConf.WarnEfanEnable)/sizeof(char)},
    {WARNLOWBATT_CONF, &swConf.WarnLowBattEnable, sizeof(swConf.WarnLowBattEnable)/sizeof(char)},
    {WARNOVERSPEED_CONF, &swConf.WarnOverSpeedEnable, sizeof(swConf.WarnOverSpeedEnable)/sizeof(char)},
    {WARNURGENT_CONF, &swConf.WarnUrgentEnable, sizeof(swConf.WarnUrgentEnable)/sizeof(char)},
    {WARNCIRCLE_CONF, &swConf.WarnCircleEanble, sizeof(swConf.WarnCircleEanble)/sizeof(char)},
		{GPS_MOTION_THRESHOLD_CONF, &swConf.GpsMotionThreshold, sizeof(swConf.GpsMotionThreshold)/sizeof(char)},
		
    {SPEED_CONF, &swConf.Speed, sizeof(swConf.Speed) / sizeof(char) },
    {GSENSOR_SENSITIVITY_CONF, &swConf.Gsensor, sizeof(swConf.Gsensor) / sizeof(char) } ,
    {SW_VERSION_CONF, &swConf.Version, sizeof(swConf.Version)/sizeof(char)},
    {TIMEZONE_CONF, &swConf.Timezone, sizeof(swConf.Timezone)/sizeof(char)},
		{SIMULATE_ACC_CONF, &swConf.SimulateAcc, sizeof(swConf.SimulateAcc)/sizeof(char)},
		{ANGLE_CONF, &swConf.angle, sizeof(swConf.angle)/sizeof(char)},
		{DISTANCE_CONF, &swConf.distance, sizeof(swConf.distance)/sizeof(char)},
		{OVERSPEED_TIME_CONF, &swConf.overspeedtime, sizeof(swConf.overspeedtime)/sizeof(char)},
		{PING_TIME_CONF, &swConf.pingtime, sizeof(swConf.pingtime)/sizeof(char)},
		{RESERVE1_CONF, &swConf.reserve1, sizeof(swConf.reserve1)/sizeof(char)},
		{RESERVE2_CONF, &swConf.reserve2, sizeof(swConf.reserve2)/sizeof(char)},
		{RESERVE3_CONF, &swConf.reserve3, sizeof(swConf.reserve3)/sizeof(char)},
		{RESERVE4_CONF, &swConf.reserve4, sizeof(swConf.reserve4)/sizeof(char)},
		{RESERVE5_CONF, &swConf.reserve5, sizeof(swConf.reserve5)/sizeof(char)},
		{RESERVE6_CONF, &swConf.reserve6, sizeof(swConf.reserve6)/sizeof(char)},
		{RESERVE7_CONF, &swConf.reserve7, sizeof(swConf.reserve7)/sizeof(char)},
		{RESERVE8_CONF, &swConf.reserve8, sizeof(swConf.reserve8)/sizeof(char)},
    { NULL, NULL, NULL} 
} ; 

uint8_t default_iap[15] = {"0"};
uint8_t default_sn[20] = {"000000000000000"};
uint8_t default_sostelenumber[20] = {"008613000000000"};
uint8_t default_sos1telenumber[20] = {"008613000000000"};
uint8_t default_sos2telenumber[20] = {"008613000000000"};
uint8_t default_sos3telenumber[20] = {"008613000000000"};
uint8_t default_mainserveripaddr[20] = {"47.89.17.248"};
//uint8_t default_mainserveripaddr[20] = {"58.96.182.170"};
uint8_t default_mainserverportnumber[10] = {"4700"};
uint8_t default_mainserverdomainname[50] = {"gps.scjtracker.com"};
uint8_t default_apnname[40] = {"internet"};
uint8_t default_apnuser[40] = {"internet"};
uint8_t default_apnpass[40] = {"internet"};
uint8_t default_repinterval[10] = {"10"};
uint8_t default_accoffrepinterval[10] = {"900"};
uint8_t default_gsmswitch[2] = {"1"};
uint8_t default_workingmode[2] = {"1"};
uint8_t default_accsleep[2] = {"0"};
uint8_t default_accstatushint[2] = {"1"};
uint8_t default_warnshock[2] = {"1"};
uint8_t default_warnefan[2] = {"1"};
uint8_t default_warnlowbatt[2] = {"1"};
uint8_t default_warnoverspeed[2] = {"1"};
uint8_t default_warnurgent[2] = {"1"};
uint8_t default_warncircle[2] = {"1"};
uint8_t default_gpsthreshold[10] = {"2"};
uint8_t default_speed[10] = {"120"};
uint8_t default_gsensor[2] = {"1"};
uint8_t default_version[20] = {"te100_v1.0.0"};
uint8_t default_timezone[10] = {"8.0"};
uint8_t default_simulateacc[30] = {"0|13.5|13.0|20|120"};
uint8_t default_angle[10] = {"30"};
uint8_t default_distance[10] = {"500"};
uint8_t default_overspeedtime[10] = {"120"};
uint8_t default_pingtime[10] = {"40"};
uint8_t default_reserve1[10] = {"0"};
uint8_t default_reserve2[10] = {"0"};
uint8_t default_reserve3[10] = {"0"};
uint8_t default_reserve4[10] = {"0"};
uint8_t default_reserve5[10] = {"0"};
uint8_t default_reserve6[10] = {"0"};
uint8_t default_reserve7[10] = {"0"};
uint8_t default_reserve8[10] = {"0"};





uint8_t* default_value[42] = {
	default_iap,
	default_sn,
	default_sostelenumber,
	default_sos1telenumber,
	default_sos2telenumber,
	default_sos3telenumber,
	default_mainserveripaddr,
	default_mainserverportnumber,
	default_mainserverdomainname,
	default_apnname,
	default_apnuser,
	default_apnpass,
	default_repinterval,
	default_accoffrepinterval,
	default_gsmswitch,
	default_workingmode,
	default_accsleep,
	default_accstatushint,
	default_warnshock,
	default_warnefan,
	default_warnlowbatt,
	default_warnoverspeed,
	default_warnurgent,
	default_warncircle,
	default_gpsthreshold,
	default_speed,
	default_gsensor,
	default_version,
	default_timezone,
	default_simulateacc,
	default_angle,
	default_distance,
	default_overspeedtime,
	default_pingtime,
	default_reserve1,
	default_reserve2,
	default_reserve3,
	default_reserve4,
	default_reserve5,
	default_reserve6,
	default_reserve7,
	default_reserve8,
};


FATFS FLASHFatFs;  /* File system object for SD card logical drive */
FIL MyFile;     /* File object */
char FLASHPath[4]; /* FLASH logical drive path */
bool writeFileProcess = false;

bool getWriteFilepStatus(void)
{
	return writeFileProcess;
}

void setWriteFilepStatus(bool status)
{
	writeFileProcess = status;
}

void STRCPY(uint8_t *dst, uint8_t *src)  {
    while ( (*dst ++ = *src ++) != 0);
}


/**
* brief : create the sw conf table 
* params :
*         none
* return :
*         < 0 indicate error
**/

bool create_sw_conf_table(void) {
    conf_map_t *p ; 
    uint32_t i=0,count=0 ; 
    
    p = swMap; 
  
    while ((p->name)) {
        memset(parray, 0, sizeof(shellLibBuf)/sizeof(char));         
        sprintf((char *)parray, "%s=%s", p->name,default_value[i]);
				STRCPY(p->val, default_value[i]);
				i++;
        count = strlen((char *)parray);
				parray[count] = CONFIG_PARA_SPLITTER_CHARACTER; 
        parray[count+1] = CONFIG_CR;
        parray[count + 2] = CONFIG_LN;
        memcpy(farray,parray,count+2);
				
				farray = farray + count+2;
        p ++  ;
    }
		farray = dataBuf;
		
		if (!flash_eeprom(dataBuf,SHELL_LIB_BUF_SIZE)) {
       return false; 
    }else{
		memset(dataBuf, 0, sizeof(dataBuf)/sizeof(char));
		}
		
    return true;
}


/**
* brief : initiation the software config table after this done 
*           the 3 volume will mount successfully 
* params:
*           none
* return:
*           < 0 indicate error
**/
bool init_sw_conf_table(void) {
    uint8_t var[BUF_SIZE], val[BUF_SIZE] ; 
    uint8_t *pos; 
    conf_map_t *p ;
		bool exception = true;
	  uint32_t index;
 
	  index = read_eeprom(dataBuf,SHELL_LIB_BUF_SIZE);
	
	  if(strncmp((char*)dataBuf,"iap=",4)|| strlen((char *)dataBuf) < 400)
			return false;
		memset(parray, 0, sizeof(shellLibBuf)/sizeof(char));

		if (dataBuf != NULL) {
		memset(var, 0 , sizeof(var)/sizeof(char));
		memset(val, 0 , sizeof(val)/sizeof(char));

	  pos =(uint8_t *) dataBuf;
			
	  while(pos){
			
		sscanf((char *)pos, "%[^=]", var);
			
		pos =(uint8_t*) strstr((char *)pos, "=");
		
		sscanf((char *)(pos+1), "%[^,]", val);
			
		exception = true;
		
		// put the val into the sw conf table 
		for (p=swMap ; p->name != NULL ; p++) {
			if (strcmp((char *)p->name, (char *)var) == 0) {
				exception = false;
				memset(p->val, 0, p->len) ; 
				STRCPY(p->val, val);
				pos =(uint8_t *) strstr((char *)pos, CONFIG_PARA_SPLITTER_STRING);
				pos = pos+2;
				memset(var, 0, sizeof(var)/sizeof(char));
				if(!strncmp(p->name,RESERVE8_CONF,8))
					return true;
				break;
			}
	}
			if(exception){
				if((pos - dataBuf) < strlen(dataBuf))
				return false;
				else
				return true;
			}
			
	}
	}
    return true;
}


bool write_sw_conf_table(){
	conf_map_t *p ; 
	uint32_t count ; 
	   
	p = swMap; 
	
	memset(dataBuf, 0, sizeof(dataBuf)/sizeof(char));	
	
	farray  = dataBuf; 
	
	while ((p->name)) {
		memset(parray, 0, sizeof(shellLibBuf)/sizeof(char));		
		sprintf((char *)parray, "%s=%s", (char *)p->name, (char *)p->val);
		//if(DebugEnable) DEBUG("write   %s = %s ",(char *)p->name,(char *)p->val);
		count = strlen((char *)parray);
		parray[count] = CONFIG_PARA_SPLITTER_CHARACTER; 
		parray[count + 1] = CONFIG_CR;
		parray[count + 2] = CONFIG_LN;
		memcpy(farray,parray,count+2);
		farray = farray + count+2;
		p ++  ;
		}

		if (!flash_eeprom(dataBuf,SHELL_LIB_BUF_SIZE)) {
       return false; 
    }else{
		memset(dataBuf, 0, sizeof(dataBuf)/sizeof(char));
		}
		return true;
}

bool update_conf_elem(uint8_t *name, uint8_t *val) {
    conf_map_t *p ;
		bool match = false;
    for (p = swMap ; p->name != NULL;  p ++ ) {
      if (strcmp((char *)name,(char *) p->name) == 0) {
				STRCPY(p->val, val) ;
            //break; 
				match = true;
        }
    }
	 if(!match)
		 return false;
   return  write_sw_conf_table();
}


bool update_conf_elem_map(uint8_t *name, uint8_t *val) {
    conf_map_t *p ;  
    for (p = swMap ; p->name != NULL;  p ++ ) {
      if (strcmp((char *)name,(char *) p->name) == 0) {
				STRCPY(p->val, val) ;
            //break; 
        }
    }
}

bool update_conf_elem_rom(void)
{
	return  write_sw_conf_table();
}

uint8_t* get_conf_elem(uint8_t *name){
	conf_map_t *p ;  
	
	for (p = swMap ; p->name != NULL;  p ++ ) {	
		if (strcmp((char *)name, (char *)p->name) == 0) {
			return p->val;
   }
  }
	return NULL;
}

void show_sw_conf(void){
	conf_map_t *p ;

	for (p = swMap ; p->name != NULL;  p ++ ) {
        //if(DebugEnable) 
				DEBUG("%s = %s ",(char *)p->name,(char *)p->val);
    }

}

void delete_sw_conf(void)
{
	uint8_t deleteBuf[10];
	memset(deleteBuf, 0, sizeof(deleteBuf)/sizeof(char));
	flash_eeprom(deleteBuf,10);
	deInitWWDG();
	SystemRestart();
}

void delete_sw_conf_noReset(void)
{
	uint8_t deleteBuf[10];
	memset(deleteBuf, 0, sizeof(deleteBuf)/sizeof(char));
	flash_eeprom(deleteBuf,10);
}


#if 1
bool fileSystem(bool action)
{		
		initSpi();
	
	 if(FATFS_LinkDriver(&FLASH_Driver, FLASHPath) == 0)
	 {
			if(f_mount(&FLASHFatFs, (TCHAR const*)FLASHPath, 0) != FR_OK)
    {
      /* FatFs Initialization Error */
			if(DebugEnable) DEBUG("f_mount fail");
			return false;
    }else{
		 #if 1
		if(!strcmp((char *)get_conf_elem(GPS_MOTION_THRESHOLD_CONF),"2")){
		 if(DebugEnable) DEBUG("f_mkfs start **************************");
		 if(f_mkfs((TCHAR const*)FLASHPath, 0, 512) != FR_OK)
      {
        /* FatFs Format Error */
       if(DebugEnable) DEBUG("f_mkfs fail***************************");
				return false;
      }else
			{
			if(DebugEnable) DEBUG("f_mkfs success***************************");
			update_conf_elem(GPS_MOTION_THRESHOLD_CONF,"3");
			}
		}
		#endif
		}
	 }else
		{
			if(DebugEnable) DEBUG("LinkDriver fail");
			return false;
		}
		if(action)
			GPS_PWR_OFF();
		
		return true;
}
#endif

void fatfsUnLink()
{
	/*##-11- Unlink the RAM disk I/O driver ####################################*/
  FATFS_UnLinkDriver(FLASHPath);
}



bool createfile(uint8_t* filename)
{
	if(f_open(&MyFile, (TCHAR *)filename, FA_CREATE_ALWAYS | FA_WRITE) != FR_OK)
  {
   /* 'location.txt' file Open for write Error */
		if(DebugEnable) DEBUG("create fail");
		return false;
  }
	
	return true;
}

bool openfile(uint8_t* filename)
{
	if(DebugEnable) DEBUG("f_open filename %s",filename);
	if(f_open(&MyFile,(TCHAR *)filename,FA_OPEN_ALWAYS | FA_WRITE | FA_READ ) != FR_OK)
  {
   /* 'location.txt' file Open for write Error */
		if(DebugEnable) DEBUG("f_open fail");
		return false;
  }
	return true;
}


bool writefile(uint8_t* filename,uint8_t* wtext)
{
	FRESULT res;        
	uint32_t byteswritten;
	
	while(1)
	{
		if((GlobalStatus & GPS_COMPLETE))
			break;
		osDelay(100);
	}
	
	setWriteFilepStatus(true);
	
	if(f_open(&MyFile, (TCHAR *)filename,FA_OPEN_ALWAYS | FA_WRITE) != FR_OK)
  {
   /* 'location.txt' file Open for write Error */
		if(DebugEnable) DEBUG("f_open fail");
		setWriteFilepStatus(false);
		return false;
  }
	
	if(!strcmp(filename,LOCATION_DATA))
		seekfile();
	else
		seekfilesize(0);
	
	res = f_write(&MyFile, wtext, strlen((char *)wtext), (void *)&byteswritten);
	
	if(strcmp(filename,LOCATION_DATA))
	res = f_write(&MyFile, "\0",1, (void *)&byteswritten);

	if (f_close(&MyFile) != FR_OK )
  {
    if(DebugEnable) DEBUG("f_close fail");
		setWriteFilepStatus(false);
		return false;
  }

	if((byteswritten == 0) || (res != FR_OK))
  {
    /* 'location.txt' file Write or EOF Error */
    if(DebugEnable) DEBUG("f_write fail");
		setWriteFilepStatus(false);
		return false;
  }
	if(DebugEnable) DEBUG("f_write wtext filename %s %s",filename,wtext);
	setWriteFilepStatus(false);
	return true;
}


bool writegeofendata(geofence_t* geodata,uint32_t index)
{
	FRESULT res;        
	uint32_t byteswritten;
	
	while(1)
	{
		if((GlobalStatus & GPS_COMPLETE))
			break;
		osDelay(100);
	}
	
	setWriteFilepStatus(true);
	
	if(f_open(&MyFile, GEOFENCE_DATA,FA_OPEN_ALWAYS | FA_WRITE) != FR_OK)
  {
   /* 'location.txt' file Open for write Error */
		if(DebugEnable) DEBUG("f_open fail");
		setWriteFilepStatus(false);
		return false;
  }
	
	if (f_lseek(&MyFile,index*sizeof(geofence_t)) != FR_OK )
	{
		if(DebugEnable) DEBUG("seek fail");
		f_close(&MyFile);
		return false;
	}
	
	res = f_write(&MyFile, geodata, sizeof(geofence_t), (void *)&byteswritten);

	if (f_close(&MyFile) != FR_OK )
  {
    if(DebugEnable) DEBUG("f_close fail");
		setWriteFilepStatus(false);
		return false;
  }

	if((byteswritten == 0) || (res != FR_OK))
  {
    /* 'location.txt' file Write or EOF Error */
    if(DebugEnable) DEBUG("f_write fail");
		setWriteFilepStatus(false);
		return false;
  }
	if(DebugEnable) DEBUG("f_write wtext geofence");
	setWriteFilepStatus(false);
	return true;
}


uint32_t readGeofencedata(geofence_t* geodata,uint32_t index)
{
	FRESULT res;    
	uint32_t bytesread;	
#if 1
	if(f_open(&MyFile, (TCHAR *)GEOFENCE_DATA, FA_READ) != FR_OK)
  {
   /* 'location.txt' file Open for read Error */
    if(DebugEnable) DEBUG("f_open fail");
		return -1;
  }
#endif
	
	if (f_lseek(&MyFile,index*sizeof(geofence_t)) != FR_OK )
	{
		if(DebugEnable) DEBUG("seek fail");
		f_close(&MyFile);
		return -1;
	}
	
	/*##-8- Read data from the text file ###########################*/
  res = f_read(&MyFile, geodata, sizeof(geofence_t), (UINT*)&bytesread);
	
	if(res != FR_OK)
  {
		/* 'location.txt' file Read or EOF Error */
    if(DebugEnable) DEBUG("f_read fail");
		f_close(&MyFile);
		return -1;
  }
#if 1
	 f_close(&MyFile);
#endif
	 if(bytesread == 0){
		 //if(DebugEnable) DEBUG("f_read,Geofencedata = 0");
		 return -1;
	 }
	
	if(DebugEnable) DEBUG("geo name %s",geodata->Name);
	return bytesread;
}


bool openReadFile(uint8_t* filename)
{
	if(f_open(&MyFile, (TCHAR *)filename, FA_OPEN_ALWAYS | FA_READ) != FR_OK)
  {
   /* 'location.txt' file Open for read Error */
    if(DebugEnable) DEBUG("f_open fail");
		return false;
  }
	return true;
}

bool closeFile(uint8_t* filename)
{
if (f_close(&MyFile) != FR_OK )
  {
    if(DebugEnable) DEBUG("f_close fail");
		return false;
  }
	return true;
}


uint8_t* getReadData(void)
{
return rtext;
}

uint32_t readfile(uint8_t* filename)
{
	FRESULT res;    
	uint32_t bytesread;	
#if 0
	if(f_open(&MyFile, (TCHAR *)filename, FA_READ) != FR_OK)
  {
   /* 'location.txt' file Open for read Error */
    if(DebugEnable) DEBUG("f_open fail");
		return -1;
  }
#endif
	memset(rtext,0,10);
	
	/*##-8- Read data from the text file ###########################*/
  res = f_read(&MyFile, rtext, sizeof(rtext), (UINT*)&bytesread);
	
	if(res != FR_OK)
  {
		/* 'location.txt' file Read or EOF Error */
    if(DebugEnable) DEBUG("f_read fail");
		f_close(&MyFile);
		return -1;
  }
#if 0
	 f_close(&MyFile);
#endif
	 if(bytesread == 0){
		 if(DebugEnable) DEBUG("f_read,bytesread = 0");
		 return -1;
	 }
	
	if(DebugEnable) DEBUG("read %s",rtext);
	return bytesread;
}

bool seekfile()
{
	if (f_lseek(&MyFile,MyFile.fsize) != FR_OK )
	{
    if(DebugEnable) DEBUG("f_lseek fail");
		f_close(&MyFile);
		return false;
  }
	return true;
}


bool seekfilesize(DWORD size)
{
	if (f_lseek(&MyFile,size) != FR_OK )
	{
    if(DebugEnable) DEBUG("f_lseek size fail");
		f_close(&MyFile);
		return false;
  }
	return true;
}
uint32_t getfilesize(uint8_t* filename)
{
	uint32_t size;
	openReadFile(filename);
	size = MyFile.fsize;
	if(DebugEnable) DEBUG("MyFile.fsize:%d",size);
	closeFile(filename);
	return size;
}

bool deletefile(uint8_t* filename)
{
  if (f_unlink ((TCHAR *)filename)  != FR_OK )
	{
		if(DebugEnable) DEBUG("f_unlink size fail");
		return false;
	}
	return true;
}


void dolocationDatahandle(void)
{
	uint8_t wtext[] = "This is STM32 working with FatFs"; 
	
	if(fileSystem(false))
	{
			if(DebugEnable) DEBUG("openfile");
			writefile(LOCATION_DATA,wtext);
			readfile(LOCATION_DATA);
	}
	
	fatfsUnLink();
}

void configFileDebugEnable(bool enable)
{
	DebugEnable = enable;
}
