#include "main.h"
#include "debug.h"
#include "FreeRTOS.h"
#include "task.h"
#include "Ds18b20.h"
#include "timer.h"

#if 0
static void DS18B20_Init(void);

TEMPERATURE  MyTemper;
float        TemperVlaue_one;     //当前 浮点型 温度值 
//float        TemperVlaue_two;     //当前 浮点型 温度值
char CurTemVlaue_one[12];
//char CurTemVlaue_two[12];
uint16_t DS18B20_Flag_One;
//u16_t DS18B20_Flag_Two;
extern bool worktestmode;

/******************************************
函数名称：GPIO_DQ_Out_Mode
功    能：设置DQ引脚为开漏输出模式
参    数：无
返回值  ：无
*******************************************/
void GPIO_DQ_Out_Mode(void)
{
    GPIO_InitTypeDef GPIO_InitStructure ;
    //RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOB, ENABLE);
		__HAL_RCC_GPIOC_CLK_ENABLE();
	
		#if 0
    GPIO_InitStructure.GPIO_Pin = DQ_GPIO_Pin_One;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz ;
    //GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_OD; //开漏输出
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP; 
		GPIO_Init(DQ_GPIO ,&GPIO_InitStructure) ;
		#endif
	
		GPIO_InitStruct.Pin       = DQ_GPIO_Pin_One;
    GPIO_InitStruct.Mode      = GPIO_MODE_OUTPUT_PP;
    GPIO_InitStruct.Pull      = GPIO_NOPULL;
    GPIO_InitStruct.Speed     = GPIO_SPEED_FREQ_VERY_HIGH;
		
		HAL_GPIO_Init(DQ_GPIO, &GPIO_InitStruct);
}



/******************************************
函数名称：GPIO_DQ_Input_Mode
功    能：设置DQ引脚为浮空输入模式
参    数：无
返回值  ：无
*******************************************/
void GPIO_DQ_Input_Mode(void)
{
    GPIO_InitTypeDef GPIO_InitStructure ;
		
		__HAL_RCC_GPIOC_CLK_ENABLE();
	  
	  #if 0 
    RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOB, ENABLE); 
	
    GPIO_InitStructure.GPIO_Pin = DQ_GPIO_Pin_One;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz ;
    //GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN_FLOATING; //浮空输入
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IPU; 
		GPIO_Init(DQ_GPIO ,&GPIO_InitStructure) ;
	  #endif
	
	  GPIO_InitStruct.Pin       = DQ_GPIO_Pin_One;
    GPIO_InitStruct.Mode      = GPIO_MODE_INPUT;
    GPIO_InitStruct.Pull      = GPIO_NOPULL;
    GPIO_InitStruct.Speed     = GPIO_SPEED_FREQ_VERY_HIGH;
			
		HAL_GPIO_Init(DQ_GPIO, &GPIO_InitStruct);
}




/******************************************
函数名称：Tx_ResetPulse
功    能：发送复位脉冲
参    数：无
返回值  ：无
*******************************************/
void Tx_ResetPulse(void)
{
    GPIO_DQ_Out_Mode() ;
    DQ_Write_0();  //复位脉冲
    Delay_Nus(500); //至少保持480us
    DQ_Write_1();  //加速上升沿速度
    Delay_Nus(60);
}

/******************************************
函数名称：Rx_PresencePulse
功    能：接受应答信号
参    数：无
返回值  ：无
*******************************************/
void Rx_PresencePulse(void)
{
	 uint16_t timeoutCnt = 0;
	 
	 GPIO_DQ_Input_Mode();

	 timeoutCnt = 0;
	 while(DQ_ReadBit_One()){
			timeoutCnt++;
			if(timeoutCnt >= 300){
				timeoutCnt = 0;
				break;
			}
			Delay_Nus(1);
	 };
	 if(!DQ_ReadBit_One()){//如果被拉低，说明有DS18B20反馈
		 	DS18B20_Flag_One = 1;
	 }
	 else{
			DS18B20_Flag_One = 0;
	}

	Delay_Nus(250);
	GPIO_DQ_Out_Mode() ;    //接受完成，主机重新控制总线
	DQ_Write_1();
	Delay_Nus(30);
}



/******************************************
函数名称：Write_OneByte_ToDS18b20
功    能：写一个字节到DS18b20
参    数：无
返回值  ：无
*******************************************/
void Write_OneByte_ToDS18b20(unsigned char data)
{
    unsigned char i ;
    GPIO_DQ_Out_Mode() ;
    for(i=0 ;i<8 ;i++)
    {
        if(data&0x01)    //低位在前
        {
            //写1
            DQ_Write_0(); //写时间空隙总是从总线的低电平开始
            Delay_Nus(10);  //15us内拉高
            DQ_Write_1();
            Delay_Nus(80); //整个写1时隙不低于60us
        }
        else
        {
            //写0
            DQ_Write_0();
            Delay_Nus(80); //保持在60us到120us之间
            DQ_Write_1();
            Delay_Nus(10);
        }
        data >>= 1;
    }
}




/******************************************
函数名称：Read_OneByte_FromDS18b20
功    能：从DS18b20读一个字节
参    数：无
返回值  ：读出的数据
*******************************************/
TemBufValues  Read_OneByte_FromDS18b20(void)
{
  unsigned char i  ;
  TemBufValues data ;

  for(i=0 ;i<8 ;i++)
  {
      GPIO_DQ_Out_Mode() ;
      data.TemBufValues_one >>= 1 ;
      DQ_Write_0() ;
      Delay_Nus(5) ;
      GPIO_DQ_Input_Mode();
      if(DQ_ReadBit_One()) {
          data.TemBufValues_one |= 0x80 ;
      }
      Delay_Nus(80) ;   //等待这一位数据完成传输
  	}
  	GPIO_DQ_Out_Mode() ;
	//DQ_Write_1();
	//Delay_Nus(1);

	return data ;
}





/******************************************
函数名称：Read_Temperature
功    能：读取温度信息
参    数：*sign - 保存符号（零上或零下）
          *integer - 保存整数部分
          *decimal - 保存小数部分
返回值  ：无
*******************************************/
uint8_t serial_num;
uint32_t number=0;	
uint8_t ds18b20_crc; 

void Read_Temperature(unsigned char *sign ,unsigned char * interger ,unsigned int *  decimal)
{
	TemBufValues a;
	TemBufValues b;

	unsigned int tmp_one;
	unsigned int tmp_two;

	
	uint8_t data;
	uint8_t i;
 
	DS18B20_Init();
	Write_OneByte_ToDS18b20(ROM_Read_Cmd);
	a = Read_OneByte_FromDS18b20();
	serial_num = a.TemBufValues_one;
	for(i=0;i<6;i++){  
			a = Read_OneByte_FromDS18b20();
			data = a.TemBufValues_one;
			number |= data;  
			number = number<<8;  
	} 
	a = Read_OneByte_FromDS18b20();
	ds18b20_crc = a.TemBufValues_one;  
	 
	DS18B20_Init();
	Write_OneByte_ToDS18b20(ROM_Skip_Cmd);//跳过读序列号操作
	Write_OneByte_ToDS18b20(Convert_T); //启动温度转换

	Delay_Nms(780);//等待DS18b20转换完成

	DS18B20_Init();
	Write_OneByte_ToDS18b20(ROM_Skip_Cmd);
	Write_OneByte_ToDS18b20(Read_Scratchpad); //读取寄存器内容（可以从寄存器0读到寄存器8）
	a= Read_OneByte_FromDS18b20();		 //温度低8位
	b= Read_OneByte_FromDS18b20();		 //温度高8位

	Tx_ResetPulse();	//中断数据读取

#if 1
	//taskEXIT_CRITICAL();
	if(DS18B20_Flag_One == 1){
		if(DebugEnable)  DEBUG("DS18B20 be checked.");
	}
	else{
		if(DebugEnable) DEBUG("DS18B20 not be checked.");
	}
	if(DebugEnable)  DEBUG("VALUE_L: %x", a .TemBufValues_one);
	if(DebugEnable)  DEBUG("VALUE_H: %x", b.TemBufValues_one);
	//taskENTER_CRITICAL();
#endif

	tmp_one = (b.TemBufValues_one<<8) | a .TemBufValues_one;
	if(b.TemBufValues_one & 0xF0){
		*(sign) = 0 ; 						 //符号部分，负数标志值0
		tmp_one = ~tmp_one+1 ;
	}
	else {
		*(sign) = 1 ; 							 //符号部分，正数标志值1
	}
	*interger = (tmp_one>>4) & 0x00FF;	//整数部分
	*decimal = (tmp_one & 0x000F) * 625 ; //小数部分	
}




/******************************************
函数名称：Write_EEPROM
功    能：写配置参数
参    数：Th - 报警温度上限
          Tl - 报警温度下限
          Register_Con - 控制寄存器的值
返回值  ：读出的数据
*******************************************/
void Write_EEPROM(unsigned char Th,unsigned char Tl,unsigned char Register_Con )
{
  
    DS18B20_Init();
    Write_OneByte_ToDS18b20(ROM_Skip_Cmd);
    Write_OneByte_ToDS18b20(Write_Scratchpad);
    
    Write_OneByte_ToDS18b20(Th);//Th=7F
    Write_OneByte_ToDS18b20(Tl);//Tl=FF 最高位符号位
    Write_OneByte_ToDS18b20(Register_Con);//12位模式
    
    Delay_Nms(700);
    DS18B20_Init();
    Write_OneByte_ToDS18b20(ROM_Skip_Cmd);
    Write_OneByte_ToDS18b20(Copy_Scratchpad);//将寄存器的配置值写入EEPROM
    
    Delay_Nms(300);
 
}


/******************************************
函数名称：DS18B20_Init
功    能：初始化DS18b20
参    数：无
返回值  ：无
*******************************************/
static void DS18B20_Init(void)
{
    Tx_ResetPulse();
    Rx_PresencePulse();
}


void DS18B20_task(void)
{
	static uint8_t delay_s = 0;

	char buf[20] = {0,};

#if 1
	if(++delay_s <= 5){
		return;
	}else{
		delay_s = 0;
	}
#endif
	
	//Disable_WWDG();
	deInitWWDG();
	taskENTER_CRITICAL();

	Read_Temperature(&MyTemper.sign[0],&MyTemper.interger[0] ,&MyTemper.decimal[0]);
	
	if (MyTemper.sign[0] == 1) {
		memcpy(CurTemVlaue_one, "+", 1);
	} else {
	    memcpy(CurTemVlaue_one, "-", 1);
	}
	 /*MyTemper.decimal = MyTemper.decimal /100;//功能；小数点后改为2位*/
	sprintf(CurTemVlaue_one+1, "%d.%-03d", MyTemper.interger[0], MyTemper.decimal[0]);
	TemperVlaue_one = atof(CurTemVlaue_one); //put the temperature into a float vlaue.
	
	taskEXIT_CRITICAL();
	//Enable_WWDG();
	initWWDG();
	startWWDG();

	if(DS18B20_Flag_One == 1){
		if(DebugEnable || worktestmode)  DEBUG("\r\n=====DS18B20 be checked=====");
		if(DebugEnable)  DEBUG("SID(x):%x%x%x",serial_num, number, ds18b20_crc);	
		if(DebugEnable|| worktestmode)  DEBUG("Temprature = %s\r\n", CurTemVlaue_one);
		osDelay(500);
		sprintf(buf, "$WIRE:%s;!", CurTemVlaue_one);
		//main2slave_send(buf);
	}else{
		if(DebugEnable || worktestmode)  DEBUG("\r\n=====DS18B20 not be checked=====\r\n");
		osDelay(500);
		memset(CurTemVlaue_one, 0, sizeof(CurTemVlaue_one));
		strcmp(CurTemVlaue_one, "0.0");
		sprintf(buf, "$WIRE:%s;!", CurTemVlaue_one);
		//main2slave_send(buf);
	}
}

#else

/* ???? ------------------------------------------------------------------*/
/* ??? -------------------------------------------------------------------*/
/***********************   DS18B20 ??????  **************************/
#define DS18B20_Dout_GPIO_CLK_ENABLE()              __HAL_RCC_GPIOC_CLK_ENABLE()
#define DS18B20_Dout_PORT                           DQ_GPIO
#define DS18B20_Dout_PIN                            DQ_GPIO_Pin_One

/***********************   DS18B20 ?????  ****************************/
#define DS18B20_Dout_LOW()                          HAL_GPIO_WritePin(DS18B20_Dout_PORT,DS18B20_Dout_PIN,GPIO_PIN_RESET) 
#define DS18B20_Dout_HIGH()                         HAL_GPIO_WritePin(DS18B20_Dout_PORT,DS18B20_Dout_PIN,GPIO_PIN_SET)
#define DS18B20_Data_IN()                           HAL_GPIO_ReadPin(DS18B20_Dout_PORT,DS18B20_Dout_PIN)

/* ???? ------------------------------------------------------------------*/
/* ???? ------------------------------------------------------------------*/
uint8_t DS18B20_Init(void);
void DS18B20_ReadId( uint8_t * ds18b20_id);
float DS18B20_GetTemp_SkipRom(void);
float DS18B20_GetTemp_MatchRom(uint8_t *ds18b20_id);

float Temperature = 0;
float Humi = 0;
float Temperature_r = 0;
float Humi_r = 0;
extern bool worktestmode;
/**
  ******************************************************************************
  * ????: bsp_DS18B20.c 
  * ?    ?: ?????????
  * ?    ?: V1.0
  * ????: 2015-10-04
  * ?    ?: DS18B20???????????
  ******************************************************************************
  * ??:
  * ???????stm32???YS-F1Pro???
  * 
  * ??:
  * ??:http://www.ing10bbs.com
  * ??????????????,?????
  ******************************************************************************
  */
/* ????? ----------------------------------------------------------------*/


/* ?????? --------------------------------------------------------------*/
/* ????? ----------------------------------------------------------------*/
#define Delay_ms(x)   Delay_Nms(x)
/* ???? ------------------------------------------------------------------*/
/* ???? ------------------------------------------------------------------*/
/* ?????? --------------------------------------------------------------*/
static void DS18B20_Mode_IPU(void);
static void DS18B20_Mode_Out_PP(void);
static void DS18B20_Rst(void);
static uint8_t DS18B20_Presence(void);
static uint8_t DS18B20_ReadBit(void);
static uint8_t DS18B20_ReadByte(void);
static void DS18B20_WriteByte(uint8_t dat);
static void DS18B20_SkipRom(void);
static void DS18B20_MatchRom(void);

/* ??? --------------------------------------------------------------------*/
/**
  * ????: 
  * ????: ?
  * ? ? ?: ?
  * ?    ?:?
  */	
static void DS18B20_Delay(uint16_t time)
{
       Delay_Nus(time);
}

void oneWireInit(void){
	GPIO_InitTypeDef GPIO_InitStruct;
	
	GPIO_InitStruct.Pin = DS18B20_Dout_PIN;
  GPIO_InitStruct.Mode = GPIO_MODE_ANALOG;
  GPIO_InitStruct.Pull = GPIO_NOPULL;

  HAL_GPIO_Init(DS18B20_Dout_PORT, &GPIO_InitStruct); 
}

/**
  * ????: DS18B20 ?????
  * ????: ?
  * ? ? ?: ?
  * ?    ?:?
  */
uint8_t DS18B20_Init(void)
{
  DS18B20_Dout_GPIO_CLK_ENABLE();
  
  DS18B20_Mode_Out_PP();
        
  DS18B20_Dout_HIGH();
        
  DS18B20_Rst();
  
  return DS18B20_Presence ();
}


/**
  * ????: ?DS18B20-DATA??????????
  * ????: ?
  * ? ? ?: ?
  * ?    ?:?
  */
static void DS18B20_Mode_IPU(void)
{
  GPIO_InitTypeDef GPIO_InitStruct;
  
  /* ??????GPIO?? */
  GPIO_InitStruct.Pin   = DS18B20_Dout_PIN;
  GPIO_InitStruct.Mode  = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull  = GPIO_PULLUP;
  HAL_GPIO_Init(DS18B20_Dout_PORT, &GPIO_InitStruct);
        
}

/**
  * ????: ?DS18B20-DATA??????????
  * ????: ?
  * ? ? ?: ?
  * ?    ?:?
  */
static void DS18B20_Mode_Out_PP(void)
{
  GPIO_InitTypeDef GPIO_InitStruct;
  
  /* ??????GPIO?? */
  GPIO_InitStruct.Pin = DS18B20_Dout_PIN;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_HIGH;
  HAL_GPIO_Init(DS18B20_Dout_PORT, &GPIO_InitStruct);          
}

/**
  * ????: ???????????
  * ????: ?
  * ? ? ?: ?
  * ?    ?:?
  */
static void DS18B20_Rst(void)
{
        /* ????????? */
        DS18B20_Mode_Out_PP();
        
        DS18B20_Dout_LOW();
  
        /* ??????480us???????? */
        DS18B20_Delay(750);
        
        /* ??????????,?????? */
        DS18B20_Dout_HIGH();
        
        /*?????????????,??15~60us???????????*/
        DS18B20_Delay(15);
}

/**
  * ????: ??????????????
  * ????: ?
  * ? ? ?: 0:??,1:??
  * ?    ?:?
  */
static uint8_t DS18B20_Presence(void)
{
        uint8_t pulse_time = 0;
        
        /* ????????? */
        DS18B20_Mode_IPU();
        
        /* ?????????,???????60~240us?????? 
         * ???????????????,?????????????,??15~60us???????????
         */
        while( DS18B20_Data_IN() && pulse_time<100 )
        {
                pulse_time++;
                DS18B20_Delay(1);
        }        
        /* ??100us?,??????????*/
        if( pulse_time >=100 )
                return 1;
        else
                pulse_time = 0;
        
        /* ??????,??????????240us */
        while( !DS18B20_Data_IN() && pulse_time<240 )
        {
                pulse_time++;
                DS18B20_Delay(1);
        }        
        if( pulse_time >=240 )
                return 1;
        else
                return 0;
}

/**
  * ????: ?DS18B20????bit
  * ????: ?
  * ? ? ?: ??????
  * ?    ?:?
  */
static uint8_t DS18B20_ReadBit(void)
{
        uint8_t dat;
        
        /* ?0??1????????60us */        
        DS18B20_Mode_Out_PP();
        /* ??????:??????? >1us <15us ?????? */
        DS18B20_Dout_LOW();
        DS18B20_Delay(10);
        
        /* ?????,????,???????????? */
        DS18B20_Mode_IPU();
        DS18B20_Delay(2);
        
        if( DS18B20_Data_IN() == SET )
                dat = 1;
        else
                dat = 0;
        
        /* ???????????? */
        DS18B20_Delay(45);
        
        return dat;
}

/**
  * ????: ?DS18B20?????,????
  * ????: ?
  * ? ? ?: ?????
  * ?    ?:?
  */
static uint8_t DS18B20_ReadByte(void)
{
        uint8_t i, j, dat = 0;        
        
        for(i=0; i<8; i++) 
        {
                j = DS18B20_ReadBit();                
                dat = (dat) | (j<<i);
        }
        
        return dat;
}

/**
  * ????: ??????DS18B20,????
  * ????: dat:?????
  * ? ? ?: ?
  * ?    ?:?
  */
static void DS18B20_WriteByte(uint8_t dat)
{
        uint8_t i, testb;
        DS18B20_Mode_Out_PP();
        
        for( i=0; i<8; i++ )
        {
                testb = dat&0x01;
                dat = dat>>1;                
                /* ?0??1????????60us */
                if (testb)
                {                        
                        DS18B20_Dout_LOW();
                        /* 1us < ???? < 15us */
                        DS18B20_Delay(8);
                        
                        DS18B20_Dout_HIGH();
                        DS18B20_Delay(58);
                }                
                else
                {                        
                        DS18B20_Dout_LOW();
                        /* 60us < Tx 0 < 120us */
                        DS18B20_Delay(70);
                        
                        DS18B20_Dout_HIGH();                
                        /* 1us < Trec(????) < ???*/
                        DS18B20_Delay(2);
                }
        }
}

/**
  * ????: ???? DS18B20 ROM
  * ????: ?
  * ? ? ?: ?
  * ?    ?:?
  */
static void DS18B20_SkipRom ( void )
{
        DS18B20_Rst();                   
        DS18B20_Presence();                 
        DS18B20_WriteByte(0XCC);                /* ?? ROM */        
}

/**
  * ????: ???? DS18B20 ROM
  * ????: ?
  * ? ? ?: ?
  * ?    ?:?
  */
static void DS18B20_MatchRom ( void )
{
        DS18B20_Rst();                   
        DS18B20_Presence();                 
        DS18B20_WriteByte(0X55);                /* ?? ROM */        
}


/*
* ??????16 ???????????????
* ????12?????,??5????,7????,4????
*
*         |---------??----------|-----?? ??? 1/(2^4)=0.0625----|
* ???  | 2^3 | 2^2 | 2^1 | 2^0 | 2^(-1) | 2^(-2) | 2^(-3) | 2^(-4) |
*
*
*         |-----???:0->?  1->?-------|-----------??-----------|
* ???  |  s  |  s  |  s  |  s  |    s   |   2^6  |   2^5  |   2^4  |
*
* 
* ?? = ??? + ?? + ??*0.0625
*/
/**
  * ????: ????? ROM ????? DS18B20 ??? 
  * ????: ?
  * ? ? ?: ???
  * ?    ?:?
  */
float DS18B20_GetTemp_SkipRom ( void )
{
        uint8_t tpmsb, tplsb;
        short s_tem;
        float f_tem;
        
        
        DS18B20_SkipRom ();
        DS18B20_WriteByte(0X44);                                /* ???? */
        
        
        DS18B20_SkipRom ();
				DS18B20_WriteByte(0XBE);                                /* ???? */
        
        tplsb = DS18B20_ReadByte();                 
        tpmsb = DS18B20_ReadByte();      
        
        s_tem = tpmsb<<8;
        s_tem = s_tem | tplsb;
        
        if( s_tem < 0 )                /* ??? */
                f_tem = (~s_tem+1) * 0.0625;        
        else
                f_tem = s_tem * 0.0625;
												
				Temperature_r = f_tem;
			
        return f_tem;         
}

/**
  * ????: ??? ROM ????? DS18B20 ??? 
  * ????: ds18b20_id:???? DS18B20 ??????????
  * ? ? ?: ?
  * ?    ?:?
  */
void DS18B20_ReadId ( uint8_t * ds18b20_id )
{
        uint8_t uc;
                
        DS18B20_WriteByte(0x33);       //?????
        
        for ( uc = 0; uc < 8; uc ++ ){
          ds18b20_id [ uc ] = DS18B20_ReadByte(); 
				}	
						
}

/**
  * ????: ??? ROM ????? DS18B20 ??? 
  * ????: ds18b20_id:?? DS18B20 ??????????
  * ? ? ?: ???
  * ?    ?:?
  */
float DS18B20_GetTemp_MatchRom ( uint8_t * ds18b20_id )
{
        uint8_t tpmsb, tplsb, i;
        short s_tem;
        float f_tem;
        
        
        DS18B20_MatchRom ();            //??ROM
        
				for(i=0;i<8;i++)
        DS18B20_WriteByte ( ds18b20_id [ i ] );        
        
        DS18B20_WriteByte(0X44);                                /* ???? */

        
        DS18B20_MatchRom ();            //??ROM
        
        for(i=0;i<8;i++)
                DS18B20_WriteByte ( ds18b20_id [ i ] );        
        
        DS18B20_WriteByte(0XBE);                                /* ???? */
        
        tplsb = DS18B20_ReadByte();                 
        tpmsb = DS18B20_ReadByte(); 
        
        
        s_tem = tpmsb<<8;
        s_tem = s_tem | tplsb;
        
        if( s_tem < 0 )                /* ??? */
                f_tem = (~s_tem+1) * 0.0625;        
        else
                f_tem = s_tem * 0.0625;
        
        return f_tem;                 
}

#endif




/**
  ******************************************************************************
  * ????: bsp_DHT11.c 
  * ?    ?: ?????????
  * ?    ?: V1.0
  * ????: 2015-10-04
  * ?    ?: DHT11????????????
  ******************************************************************************
  * ??:
  * ???????stm32???YS-F1Pro???
  * 
  * ??:
  * ??:http://www.ing10bbs.com
  * ??????????????,?????
  ******************************************************************************
  */
/* ????? ----------------------------------------------------------------*/

/* ?????? --------------------------------------------------------------*/
/* ????? ----------------------------------------------------------------*/
#define Delay_ms(x)   Delay_Nms(x)

#define DHT11_Dout_LOW()                          HAL_GPIO_WritePin(DS18B20_Dout_PORT,DS18B20_Dout_PIN,GPIO_PIN_RESET)  
#define DHT11_Dout_HIGH()                         HAL_GPIO_WritePin(DS18B20_Dout_PORT,DS18B20_Dout_PIN,GPIO_PIN_SET)
#define DHT11_Data_IN()                           HAL_GPIO_ReadPin(DS18B20_Dout_PORT,DS18B20_Dout_PIN)

/* ???? ------------------------------------------------------------------*/
/* ???? ------------------------------------------------------------------*/
/* ?????? --------------------------------------------------------------*/
static void DHT11_Mode_IPU(void);
static void DHT11_Mode_Out_PP(void);
static uint8_t DHT11_ReadByte(void);

typedef struct
{
        uint8_t  humi_high8bit;                //????:???8?
        uint8_t  humi_low8bit;                 //????:???8?
        uint8_t  temp_high8bit;                 //????:???8?
        uint8_t  temp_low8bit;                 //????:???8?
        uint8_t  check_sum;                     //???
  float    humidity;        //????
  float    temperature;     //????  
} DHT11_Data_TypeDef;

/* ??? --------------------------------------------------------------------*/
/**
  * ????: 
  * ????: ?
  * ? ? ?: ?
  * ?    ?:?
  */
static void DHT11_Delay(uint16_t time)
{
    Delay_Nus(time);
}

/**
  * ????: DHT11 ?????
  * ????: ?
  * ? ? ?: ?
  * ?    ?:?
  */
void DHT11_Init ( void )
{
	DS18B20_Dout_GPIO_CLK_ENABLE();
  
  DS18B20_Mode_Out_PP();
        
  DS18B20_Dout_HIGH();
}

/**
  * ????: ?DHT11-DATA??????????
  * ????: ?
  * ? ? ?: ?
  * ?    ?:?
  */
static void DHT11_Mode_IPU(void)
{
	DS18B20_Mode_IPU();
}

/**
  * ????: ?DHT11-DATA??????????
  * ????: ?
  * ? ? ?: ?
  * ?    ?:?
  */
static void DHT11_Mode_Out_PP(void)
{
  DS18B20_Mode_Out_PP();         
}

/**
  * ????: ?DHT11??????,MSB??
  * ????: ?
  * ? ? ?: ?
  * ?    ?:?
  */
static uint8_t DHT11_ReadByte ( void )
{
        uint8_t i, temp=0;
        uint8_t timeout = 50;

        for(i=0;i<8;i++)    
        {         
                /*?bit?50us???????,???????? ?50us ??? ??*/  
                while(DHT11_Data_IN()==GPIO_PIN_RESET)
								{
								timeout--;
								DHT11_Delay(10);
								if(timeout == 0)
								return ERROR;
								}

                /*DHT11 ?26~28us?????????70us???????
                 *???? x us???????????? ,x ?????? 
                 */
                DHT11_Delay(40); //??x us ??????????0???????                     

                if(DHT11_Data_IN()==GPIO_PIN_SET)/* x us????????????*/
                {
                        /* ????1?????? */
												timeout = 50;
                        while(DHT11_Data_IN()==GPIO_PIN_SET)
												{
												timeout--;
												DHT11_Delay(10);
												if(timeout == 0)
												return ERROR;
												}

                        temp|=(uint8_t)(0x01<<(7-i));  //??7-i??1,MSB?? 
                }
                else         // x us???????????
                {                           
                        temp&=(uint8_t)~(0x01<<(7-i)); //??7-i??0,MSB??
                }
        }
        return temp;
}


/*
 * 
 * 
 */
/**
  * ????: ??????????40bit,????
  * ????: DHT11_Data:DHT11????
  * ? ? ?: ERROR:  ????
  *           SUCCESS:????
  * ?    ?:8bit ???? + 8bit ???? + 8bit ???? + 8bit ???? + 8bit ??? 
  */
uint8_t DHT11_Read_TempAndHumidity(DHT11_Data_TypeDef *DHT11_Data)
{  
  uint8_t temp;
  uint32_t humi_temp;
	uint8_t timeout = 5;
  
        /*????*/
        DHT11_Mode_Out_PP();
        /*????*/
        DHT11_Dout_LOW();
        /*??18ms*/
        Delay_Nms(18);

        /*???? ????30us*/
        DHT11_Dout_HIGH(); 

        DHT11_Delay(30);   //??30us

        /*?????? ????????*/ 
        DHT11_Mode_IPU();

        /*?????????????? ???????,???????*/   
        if(DHT11_Data_IN()==GPIO_PIN_RESET)     
        {
					/*???????? ?80us ??? ??????*/  
					while(DHT11_Data_IN()==GPIO_PIN_RESET)
					{
					timeout--;
					DHT11_Delay(30);
					if(timeout == 0)
						return ERROR;
					}

					timeout = 5;
					/*????????? 80us ??? ??????*/
					while(DHT11_Data_IN()==GPIO_PIN_SET)
					{
					timeout--;
					DHT11_Delay(30);
					if(timeout == 0)
						return ERROR;
					}

					/*??????*/   
					DHT11_Data->humi_high8bit= DHT11_ReadByte();
					DHT11_Data->humi_low8bit = DHT11_ReadByte();
					DHT11_Data->temp_high8bit= DHT11_ReadByte();
					DHT11_Data->temp_low8bit = DHT11_ReadByte();
					DHT11_Data->check_sum    = DHT11_ReadByte();

					/*????,????????*/
					DHT11_Mode_Out_PP();
					/*????*/
					DHT11_Dout_HIGH();
    
					/* ??????? */
					//humi_temp=DHT11_Data->humi_high8bit*100+DHT11_Data->humi_low8bit;
					humi_temp=DHT11_Data->humi_high8bit<<8;
					humi_temp = humi_temp | DHT11_Data->humi_low8bit;
					DHT11_Data->humidity =(float)humi_temp/10;
    
					//humi_temp=DHT11_Data->temp_high8bit*100+DHT11_Data->temp_low8bit;
					humi_temp=DHT11_Data->temp_high8bit<<8;
					humi_temp = humi_temp | DHT11_Data->temp_low8bit;
					
					if( humi_temp > 0x8000){
						 humi_temp = (humi_temp - 0x8000)/10;
						 DHT11_Data->temperature=(float)humi_temp; 
						 DHT11_Data->temperature = - DHT11_Data->temperature;
					}
					else				
          DHT11_Data->temperature=(float)humi_temp/10; 
					
					/*???????????*/
					temp = DHT11_Data->humi_high8bit + DHT11_Data->humi_low8bit + 
          DHT11_Data->temp_high8bit+ DHT11_Data->temp_low8bit;
		
					if(DHT11_Data->check_sum == temp)
					{ 
						return SUCCESS;
					}
					else 
					return ERROR;
					}        
			 else
					return ERROR;
}


bool isDHT11 = false;
bool isDS18B20 = false;
uint8_t DHT11_task(void)
{
	DHT11_Data_TypeDef data;
	uint8_t read;
	
	if(timer_init() == 0)
		return ERROR;
	
	//taskENTER_CRITICAL();
	DHT11_Init();
	read = DHT11_Read_TempAndHumidity(&data);
	//taskEXIT_CRITICAL();
	if(read == SUCCESS){
	isDHT11 = true;
	Temperature = data.temperature;
	Humi = data.humidity;
	if(worktestmode) DEBUG("read temperature success %f %f",data.humidity,data.temperature);
	if(data.temperature == 0 && data.humidity != 0)
		Temperature = 0;
	if(data.humidity == 0 )
		Humi = 0;
	}else
	{
	isDHT11 = false;
	if(!isDS18B20){
	Temperature = 0;
	Humi = 0;
	}
	}
	DHT11_Delay(30);
	oneWireInit();
	DHT11_Delay(30);
	return read;
}




bool ds18s20init = false;
void DS18B20_task(void)
{

	bool readSuceess = false;
	if(isDHT11)
		return;
	//if(timer_init() == 0)
		//return;
	 //if(!ds18s20init)
	{
	//taskENTER_CRITICAL();
	if(DS18B20_Init() == 0)
	{
		//ds18s20init = true;
		DS18B20_GetTemp_SkipRom();
		//DS18B20_ReadId(id);
		readSuceess = true;
		isDS18B20 = true;
	}else
	{
		isDS18B20 = false;
	}
	//taskEXIT_CRITICAL();
	}
	
	if(ds18s20init)
	{
	 //taskENTER_CRITICAL();
	 DS18B20_GetTemp_SkipRom();
	 //taskEXIT_CRITICAL();
	 readSuceess = true;
	}
	
	if(readSuceess){
	if(worktestmode) DEBUG("read temperature success %f",Temperature_r);
	Temperature = Temperature_r;
	if((Temperature_r == 0 && Humi == 0) || Temperature_r == 85.0)
		Temperature = 0;
		Humi = 0;
	}else{
		if(!isDHT11){
		Temperature = 0;
		Humi = 0;
		}
	}
	DS18B20_Delay(30);
	oneWireInit();
}

#if 0
bool ds18s20init = false;
void DS18B20_task(void)
{
	//DEBUG("DS18B20_Init %d",DS18B20_Init());
	uint8_t id[8];
	memset(id,0,8);
	
	bool readSuceess = false;
	
	#if 0
	taskENTER_CRITICAL();
	if(DS18B20_Init() == 0)
	{
	 //DS18B20_ReadId(id);
	 DS18B20_GetTemp_SkipRom();
	 readSuceess = true;
	}else
	{
	//DEBUG("DS18B20_Init fail");
	}
	taskEXIT_CRITICAL();
	
	#endif
	
	if(!ds18s20init)
	{
	taskENTER_CRITICAL();
	if(DS18B20_Init() == 0)
	{
		ds18s20init = true;
	}
	taskEXIT_CRITICAL();
	}
	
	if(ds18s20init)
	{
	 taskENTER_CRITICAL();
	 DS18B20_GetTemp_SkipRom();
	 taskEXIT_CRITICAL();
	 readSuceess = true;
	}
	
	if(readSuceess){
	if(worktestmode) DEBUG("read temperature success %f %f",Temperature_r/100,Humi_r/100);
	//for(uint8_t i = 0; i< 8 ;i++)
		//DEBUG("%x",id[i]);
	Temperature = (Temperature + Temperature_r/100)/2;
	Humi = (Humi + Humi_r/100)/2;
	if(Temperature_r == 0 )
		Temperature = 0;
	if(Humi_r == 0)
		Humi = 0;
	}
}

#endif

/******************* (C) COPYRIGHT 2015-2020 ????????? *****END OF FILE****/




void ds18b20DebugEnable(bool enable)
{
	DebugEnable = enable;
}

