#include "hardware.h"
#include "debug.h"
#include "gps.h"
#include "gsm.h"
#include "work.h"
#include "gsensor.h"
#include "spi_flash.h"
#include "main.h"
#include "timer.h"

uint8_t aRxGsmData = 0;
uint8_t aRxGpsData = 0;
uint8_t aRxShellData = 0;
UART_HandleTypeDef gsmUartHandle;
UART_HandleTypeDef gpsUartHandle;
UART_HandleTypeDef debugUartHandle;
UART_HandleTypeDef lpUartHandle;
SPI_HandleTypeDef SpiHandle;
RTC_HandleTypeDef RTCHandle;
WWDG_HandleTypeDef WwdgHandle;
IWDG_HandleTypeDef IwdgHandle;
ADC_HandleTypeDef    AdcHandle;
TIM_HandleTypeDef    Tim1Handle;
TIM_HandleTypeDef    Tim2Handle;
bool pwmout1 = false;
bool pwmout2 = false;
uint8_t sleep_time = 0;


extern bool rtcReport;

/* Timer Output Compare Configuration Structure declaration */
TIM_OC_InitTypeDef stConfig;

/* Counter Prescaler value */
uint32_t uhPrescalerValue = 0;

uint8_t i2cError= 0;

__IO uint32_t wTransferState = TRANSFER_WAIT;

void Error_Handler(void)
{
   if(DebugEnable) DEBUG("send error");
}


void MX_LPUART1_UART_Init(void)
{
  LL_LPUART_InitTypeDef LPUART_InitStruct;

  LL_GPIO_InitTypeDef GPIO_InitStruct;
  /* Peripheral clock enable */
  LL_APB1_GRP1_EnableClock(LL_APB1_GRP1_PERIPH_LPUART1);
  
  /**LPUART1 GPIO Configuration  
  PB10   ------> LPUART1_RX
  PB11   ------> LPUART1_TX 
  */
 

  GPIO_InitStruct.Pin = LL_GPIO_PIN_10;
  GPIO_InitStruct.Mode = LL_GPIO_MODE_ALTERNATE;
  GPIO_InitStruct.Speed = LL_GPIO_SPEED_FREQ_VERY_HIGH;
  GPIO_InitStruct.OutputType = LL_GPIO_OUTPUT_PUSHPULL;
  GPIO_InitStruct.Pull = LL_GPIO_PULL_UP;
  GPIO_InitStruct.Alternate = GPIO_AF4_LPUART1;
  LL_GPIO_Init(GPIOB, &GPIO_InitStruct);

  GPIO_InitStruct.Pin = LL_GPIO_PIN_11;
  GPIO_InitStruct.Mode = LL_GPIO_MODE_ALTERNATE;
  GPIO_InitStruct.Speed = LL_GPIO_SPEED_FREQ_VERY_HIGH;
  GPIO_InitStruct.OutputType = LL_GPIO_OUTPUT_PUSHPULL;
  GPIO_InitStruct.Pull = LL_GPIO_PULL_UP;
  GPIO_InitStruct.Alternate = GPIO_AF4_LPUART1;
  LL_GPIO_Init(GPIOB, &GPIO_InitStruct);

  /* LPUART1 interrupt Init */
  NVIC_SetPriority(AES_RNG_LPUART1_IRQn,TICK_INT_PRIORITY);
  NVIC_EnableIRQ(AES_RNG_LPUART1_IRQn);

  LPUART_InitStruct.BaudRate = 9600;
  LPUART_InitStruct.DataWidth = LL_LPUART_DATAWIDTH_8B;
  LPUART_InitStruct.StopBits = LL_LPUART_STOPBITS_1;
  LPUART_InitStruct.Parity = LL_LPUART_PARITY_NONE;
  LPUART_InitStruct.TransferDirection = LL_LPUART_DIRECTION_TX_RX;
  LPUART_InitStruct.HardwareFlowControl = LL_LPUART_HWCONTROL_NONE;
  LL_LPUART_Init(LPUART1, &LPUART_InitStruct);


  LL_LPUART_DisableOverrunDetect(LPUART1) ;


  LL_LPUART_Enable(LPUART1);

  
  // enable the interrupt of RXNE 

  LL_LPUART_EnableIT_RXNE(LPUART1);  

}

void MX_LPUART1_UART_Deinit(void) {

  LL_APB1_GRP1_DisableClock(LL_APB1_GRP1_PERIPH_LPUART1);
  // fix me later 
  LL_LPUART_Disable(LPUART1) ;
	
	__HAL_RCC_GPIOC_CLK_ENABLE();
	GPIO_InitStruct.Pin       = GPIO_PIN_6;
  GPIO_InitStruct.Mode      = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull      = GPIO_NOPULL;
  GPIO_InitStruct.Speed     = GPIO_SPEED_FREQ_VERY_HIGH;
	
	HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);
	HAL_GPIO_WritePin(GPIOC, GPIO_PIN_6, GPIO_PIN_SET);
}


void MX_USART2_UART_Init(void)
{
  LL_USART_InitTypeDef USART_InitStruct;

  LL_GPIO_InitTypeDef GPIO_InitStruct;
  /* Peripheral clock enable */
  LL_APB1_GRP1_EnableClock(LL_APB1_GRP1_PERIPH_USART2);

  // add by ww , because of the bootloader just setting the USART2 register
  LL_USART_Disable(USART2)  ;
  /**USART2 GPIO Configuration  
  PA2   ------> USART2_TX
  PA3   ------> USART2_RX 
  */
  GPIO_InitStruct.Pin = LL_GPIO_PIN_2;
  GPIO_InitStruct.Mode = LL_GPIO_MODE_ALTERNATE;
  GPIO_InitStruct.Speed = LL_GPIO_SPEED_FREQ_VERY_HIGH;
  GPIO_InitStruct.OutputType = LL_GPIO_OUTPUT_PUSHPULL;
  GPIO_InitStruct.Pull = LL_GPIO_PULL_UP;
  GPIO_InitStruct.Alternate = LL_GPIO_AF_4;
  LL_GPIO_Init(GPIOA, &GPIO_InitStruct);

  GPIO_InitStruct.Pin = LL_GPIO_PIN_3;
  GPIO_InitStruct.Mode = LL_GPIO_MODE_ALTERNATE;
  GPIO_InitStruct.Speed = LL_GPIO_SPEED_FREQ_VERY_HIGH;
  GPIO_InitStruct.OutputType = LL_GPIO_OUTPUT_PUSHPULL;
  GPIO_InitStruct.Pull = LL_GPIO_PULL_UP;
  GPIO_InitStruct.Alternate = LL_GPIO_AF_4;
  LL_GPIO_Init(GPIOA, &GPIO_InitStruct);

  /* USART2 interrupt Init */
  NVIC_SetPriority(USART2_IRQn, 3);
  NVIC_EnableIRQ(USART2_IRQn);

  USART_InitStruct.BaudRate = 115200;
  USART_InitStruct.DataWidth = LL_USART_DATAWIDTH_8B;
  USART_InitStruct.StopBits = LL_USART_STOPBITS_1;
  USART_InitStruct.Parity = LL_USART_PARITY_NONE;
  USART_InitStruct.TransferDirection = LL_USART_DIRECTION_TX_RX;
  USART_InitStruct.HardwareFlowControl = LL_USART_HWCONTROL_NONE;
  USART_InitStruct.OverSampling = LL_USART_OVERSAMPLING_16;
  LL_USART_Init(USART2, &USART_InitStruct);

  LL_USART_DisableOverrunDetect(USART2);

  LL_USART_ConfigAsyncMode(USART2);

  LL_USART_Enable(USART2);

  
  // enable the interrupt of RXNE 

  LL_USART_EnableIT_RXNE(USART2);   
//  LL_USART_EnableIT_PE(USART2);      

}


void  MX_USART2_UART_Deinit(void) {
  LL_GPIO_InitTypeDef GPIO_InitStruct;
	
  /* Peripheral clock enable */
  LL_APB1_GRP1_DisableClock(LL_APB1_GRP1_PERIPH_USART2);
  // add by ww , because of the bootloader maybe set the USART2 register
  LL_USART_Disable(USART2)  ;

  GPIO_InitStruct.Pin = LL_GPIO_PIN_2;
  GPIO_InitStruct.Mode = LL_GPIO_MODE_ANALOG;
  GPIO_InitStruct.Pull = LL_GPIO_PULL_NO;
  LL_GPIO_Init(GPIOA, &GPIO_InitStruct);

  
  GPIO_InitStruct.Pin = LL_GPIO_PIN_3;
  GPIO_InitStruct.Mode = LL_GPIO_MODE_ANALOG;
  GPIO_InitStruct.Pull = LL_GPIO_PULL_NO;
  LL_GPIO_Init(GPIOA, &GPIO_InitStruct);
  
}


void MX_USART4_UART_Init(void)
{
  LL_USART_InitTypeDef USART_InitStruct;

  LL_GPIO_InitTypeDef GPIO_InitStruct;
  /* Peripheral clock enable */
  LL_APB1_GRP1_EnableClock(LL_APB1_GRP1_PERIPH_USART4);
  // add by ww , because of the bootloader maybe set the USART2 register
  LL_USART_Disable(USART4)  ;
  /**USART4 GPIO Configuration  
  PA0   ------> USART4_TX
  PA1   ------> USART4_RX 
  */
  GPIO_InitStruct.Pin = LL_GPIO_PIN_0;
  GPIO_InitStruct.Mode = LL_GPIO_MODE_ALTERNATE;
  GPIO_InitStruct.Speed = LL_GPIO_SPEED_FREQ_VERY_HIGH;
  GPIO_InitStruct.OutputType = LL_GPIO_OUTPUT_PUSHPULL;
  GPIO_InitStruct.Pull = LL_GPIO_PULL_UP;
  GPIO_InitStruct.Alternate = LL_GPIO_AF_6;
  LL_GPIO_Init(GPIOA, &GPIO_InitStruct);

  GPIO_InitStruct.Pin = LL_GPIO_PIN_1;
  GPIO_InitStruct.Mode = LL_GPIO_MODE_ALTERNATE;
  GPIO_InitStruct.Speed = LL_GPIO_SPEED_FREQ_VERY_HIGH;
  GPIO_InitStruct.OutputType = LL_GPIO_OUTPUT_PUSHPULL;
  GPIO_InitStruct.Pull = LL_GPIO_PULL_UP;
  GPIO_InitStruct.Alternate = LL_GPIO_AF_6;
  LL_GPIO_Init(GPIOA, &GPIO_InitStruct);

  /* USART4 interrupt Init */
  NVIC_SetPriority(USART4_5_IRQn, TICK_INT_PRIORITY+1);
  NVIC_EnableIRQ(USART4_5_IRQn);

  USART_InitStruct.BaudRate = 115200;
  USART_InitStruct.DataWidth = LL_USART_DATAWIDTH_8B;
  USART_InitStruct.StopBits = LL_USART_STOPBITS_1;
  USART_InitStruct.Parity = LL_USART_PARITY_NONE;
  USART_InitStruct.TransferDirection = LL_USART_DIRECTION_TX_RX;
  USART_InitStruct.HardwareFlowControl = LL_USART_HWCONTROL_NONE;
  USART_InitStruct.OverSampling = LL_USART_OVERSAMPLING_16;
  LL_USART_Init(USART4, &USART_InitStruct);

  LL_USART_DisableOverrunDetect(USART4);

  LL_USART_ConfigAsyncMode(USART4);

  LL_USART_Enable(USART4);

  // enable the USART4 RXNE interrupt 

  LL_USART_EnableIT_RXNE(USART4);   


}


void  MX_USART4_UART_Deinit(void) {
  LL_GPIO_InitTypeDef GPIO_InitStruct;
	
  /* Peripheral clock enable */
  LL_APB1_GRP1_DisableClock(LL_APB1_GRP1_PERIPH_USART4);
  // add by ww , because of the bootloader maybe set the USART2 register
  LL_USART_Disable(USART4)  ;

  GPIO_InitStruct.Pin = LL_GPIO_PIN_0;
  GPIO_InitStruct.Mode = LL_GPIO_MODE_ANALOG;
  GPIO_InitStruct.Pull = LL_GPIO_PULL_NO;
  LL_GPIO_Init(GPIOA, &GPIO_InitStruct);

  
  GPIO_InitStruct.Pin = LL_GPIO_PIN_1;
  GPIO_InitStruct.Mode = LL_GPIO_MODE_ANALOG;
  GPIO_InitStruct.Pull = LL_GPIO_PULL_NO;
  LL_GPIO_Init(GPIOA, &GPIO_InitStruct);
  
}


void MX_USART5_UART_Init(uint32_t speed)
{
  LL_USART_InitTypeDef USART_InitStruct;

  LL_GPIO_InitTypeDef GPIO_InitStruct;
	
  /* Peripheral clock enable */
  LL_APB1_GRP1_EnableClock(LL_APB1_GRP1_PERIPH_USART5);
	
	LL_USART_Disable(USART5)  ;
	
	/**USART5 GPIO Configuration  
  PB4   ------> USART5_RX
  PB3   ------> USART5_TX 
  */
  GPIO_InitStruct.Pin = LL_GPIO_PIN_4;
  GPIO_InitStruct.Mode = LL_GPIO_MODE_ALTERNATE;
  GPIO_InitStruct.Speed = LL_GPIO_SPEED_FREQ_VERY_HIGH;
  GPIO_InitStruct.OutputType = LL_GPIO_OUTPUT_PUSHPULL;
  GPIO_InitStruct.Pull = LL_GPIO_PULL_UP;
  GPIO_InitStruct.Alternate = LL_GPIO_AF_6;
  LL_GPIO_Init(GPIOB, &GPIO_InitStruct);

  GPIO_InitStruct.Pin = LL_GPIO_PIN_3;
  GPIO_InitStruct.Mode = LL_GPIO_MODE_ALTERNATE;
  GPIO_InitStruct.Speed = LL_GPIO_SPEED_FREQ_VERY_HIGH;
  GPIO_InitStruct.OutputType = LL_GPIO_OUTPUT_PUSHPULL;
  GPIO_InitStruct.Pull = LL_GPIO_PULL_UP;
  GPIO_InitStruct.Alternate = LL_GPIO_AF_6;
  LL_GPIO_Init(GPIOB, &GPIO_InitStruct);

  /* USART5 interrupt Init */
  NVIC_SetPriority(USART4_5_IRQn, TICK_INT_PRIORITY+1);
  NVIC_EnableIRQ(USART4_5_IRQn);
	
  // add by ww , because of the bootloader maybe set the USART2 register
  LL_USART_Disable(USART5)  ;
  
  
  USART_InitStruct.BaudRate = speed;
  USART_InitStruct.DataWidth = LL_USART_DATAWIDTH_8B;
  USART_InitStruct.StopBits = LL_USART_STOPBITS_1;
  USART_InitStruct.Parity = LL_USART_PARITY_NONE;
  USART_InitStruct.TransferDirection = LL_USART_DIRECTION_TX_RX;
  USART_InitStruct.HardwareFlowControl = LL_USART_HWCONTROL_NONE;
  USART_InitStruct.OverSampling = LL_USART_OVERSAMPLING_16;
  LL_USART_Init(USART5, &USART_InitStruct);

	
  LL_USART_DisableOverrunDetect(USART5);

  LL_USART_ConfigAsyncMode(USART5);

  LL_USART_Enable(USART5);
  // enable the USART5 RXNE interrupt 

  LL_USART_EnableIT_RXNE(USART5);

}


void MX_USART5_UART_Deinit(void) {
  LL_GPIO_InitTypeDef GPIO_InitStruct;

  /* Peripheral clock enable */
  LL_APB1_GRP1_DisableClock(LL_APB1_GRP1_PERIPH_USART5);
  // add by ww , because of the bootloader maybe set the USART2 register
  LL_USART_Disable(USART5)  ;
  GPIO_InitStruct.Pin = LL_GPIO_PIN_4;
  GPIO_InitStruct.Mode = LL_GPIO_MODE_ANALOG;
  GPIO_InitStruct.Pull = LL_GPIO_PULL_NO;
  LL_GPIO_Init(GPIOB, &GPIO_InitStruct);

  
  GPIO_InitStruct.Pin = LL_GPIO_PIN_3;
  GPIO_InitStruct.Mode = LL_GPIO_MODE_ANALOG;
  GPIO_InitStruct.Pull = LL_GPIO_PULL_NO;
  LL_GPIO_Init(GPIOB, &GPIO_InitStruct);
}


void USART_SEND_DATAS(char *data, USART_TypeDef *USARTx) {
    int len = strlen(data) ; 
    int i ; 
    for (i = 0 ; i < len ; i++) {
        LL_USART_TransmitData8(USARTx, *data++)  ;
        while (READ_BIT(USARTx->ISR, USART_ISR_TXE) == RESET) ; 
    }
}

void USART_SEND_HEXDATAS(char *data, USART_TypeDef *USARTx,int len) {
    int i ; 
    for (i = 0 ; i < len ; i++) {
        LL_USART_TransmitData8(USARTx, *data++)  ;
        while (READ_BIT(USARTx->ISR, USART_ISR_TXE) == RESET) ; 
    }
}

void USART_SEND_DATA(char data, USART_TypeDef *USARTx) {

    LL_USART_TransmitData8(USARTx, data)  ;
    while (READ_BIT(USARTx->ISR, USART_ISR_TXE) == RESET) ; 

}


void initTimPwm(void)
{
  uhPrescalerValue = (SystemCoreClock / 100000) - 1;
	//pa5
	Tim1Handle.Instance = TIM2;

  Tim1Handle.Init.Prescaler         = 0;
  Tim1Handle.Init.Period            = uhPrescalerValue;
  Tim1Handle.Init.ClockDivision     = 0;
  Tim1Handle.Init.CounterMode       = TIM_COUNTERMODE_UP;
	
	if (HAL_TIM_PWM_Init(&Tim1Handle) != HAL_OK)
  {
    /* Initialization Error */
    if(DebugEnable) DEBUG("PWM_Init error");
  }
	

  /*##-2- Configure the PWM channels #########################################*/
  /* Common configuration for all channels */
  stConfig.OCMode       = TIM_OCMODE_PWM1;
  stConfig.OCPolarity   = TIM_OCPOLARITY_HIGH;
  stConfig.OCFastMode   = TIM_OCFAST_DISABLE;


  /* Set the pulse value for channel 1 */
  //stConfig.Pulse = PULSE1_VALUE;
	//stConfig.Pulse = 66.7*(uhPrescalerValue - 30)/100;
	stConfig.Pulse = 100*(uhPrescalerValue)/100;
  if (HAL_TIM_PWM_ConfigChannel(&Tim1Handle, &stConfig, TIM_CHANNEL_1) != HAL_OK)
  {
    /* Configuration Error */
    if(DebugEnable) DEBUG("PWM_ConfigChannel error");
  }
	
	//pc8
	Tim2Handle.Instance = TIM3;
	Tim2Handle.Init.Prescaler         = 0;
  Tim2Handle.Init.Period            = uhPrescalerValue;
  Tim2Handle.Init.ClockDivision     = 0;
  Tim2Handle.Init.CounterMode       = TIM_COUNTERMODE_UP;
	
  if (HAL_TIM_PWM_Init(&Tim2Handle) != HAL_OK)
  {
    /* Initialization Error */
    if(DebugEnable) DEBUG("PWM_Init error");
  }
	
  if (HAL_TIM_PWM_ConfigChannel(&Tim2Handle, &stConfig, TIM_CHANNEL_3) != HAL_OK)
  {
    /* Configuration Error */
    if(DebugEnable) DEBUG("PWM_ConfigChannel error");
  }
 
}	


void start_pwm_out1(void)
{
/*##-3- Start PWM signals generation #######################################*/
  /* Start channel 1 */
	pwmout1 = true;
	
	if(!GpsNoData() && getGpsSpeed() > 10.0)
	{
			if(DebugEnable) DEBUG("speed > 10");
			return;
	}
	
	if(!pwmout1)
		return;
	
  if (HAL_TIM_PWM_Start(&Tim1Handle, TIM_CHANNEL_1) != HAL_OK)
  {
    /* PWM Generation Error */
    if(DebugEnable) DEBUG("PWM_Start1 error");
		return;
  }
	write_out1_status(1);
	if(DebugEnable) DEBUG("PWM_Start1 success");
	pwmout1 = false;
}

void start_pwm_out2(void)
{
	pwmout2 = true;
	
	if(!GpsNoData() && getGpsSpeed() > 10.0)
	{
			if(DebugEnable) DEBUG("speed > 10");
			return;
	}
	
	if(!pwmout2)
		return;
	
	/* Start channel 3 */
  if (HAL_TIM_PWM_Start(&Tim2Handle, TIM_CHANNEL_3) != HAL_OK)
  {
    /* PWM generation Error */
    if(DebugEnable) DEBUG("PWM_Start2 error");
		return;
  }
	write_out2_status(1);
	if(DebugEnable) DEBUG("PWM_Start2 success");
	pwmout2 = false;
}


void stop_pwm_out1(void)
{
/*##-3- Start PWM signals generation #######################################*/
  /* Start channel 1 */
  if (HAL_TIM_PWM_Stop(&Tim1Handle, TIM_CHANNEL_1) != HAL_OK)
  {
    /* PWM Generation Error */
    if(DebugEnable) DEBUG("PWM_Start1 error");
		return;
  }
	write_out1_status(0);
	if(DebugEnable) DEBUG("PWM_Stop1 success");
	pwmout1 = false;
}


void stop_pwm_out1_tosleep(void)
{
/*##-3- Start PWM signals generation #######################################*/
  /* Start channel 1 */
  if (HAL_TIM_PWM_Stop(&Tim1Handle, TIM_CHANNEL_1) != HAL_OK)
  {
    /* PWM Generation Error */
    if(DebugEnable) DEBUG("PWM_Start1 error");
		return;
  }
}

void stop_pwm_out2(void)
{
	/* Start channel 3 */
  if (HAL_TIM_PWM_Stop(&Tim2Handle, TIM_CHANNEL_3) != HAL_OK)
  {
    /* PWM generation Error */
    if(DebugEnable) DEBUG("PWM_Start2 error");
		return;
  }
	write_out2_status(0);
		if(DebugEnable) DEBUG("PWM_Stop2 success");
	pwmout2 = false;
}


void stop_pwm_out2_tosleep(void)
{
	/* Start channel 3 */
  if (HAL_TIM_PWM_Stop(&Tim2Handle, TIM_CHANNEL_3) != HAL_OK)
  {
    /* PWM generation Error */
    if(DebugEnable) DEBUG("PWM_Start2 error");
		return;
  }
}

bool getOut1Statue(void)
{
	if(read_out1_data() == 1)
	return true;
	else
	return false;
}

bool getOut2Statue(void)
{
	if(read_out2_data() == 1)
	return true;
	else
	return false;
}

bool checkPWMOut1Setup(void)
{
	return pwmout1;
}

bool checkPWMOut2Setup(void)
{
	return pwmout2;
}
	
void initSpi(void)
{
	GPIO_InitTypeDef  GPIO_InitStruct;

	GPS_PWR_ON();
	
	__HAL_RCC_GPIOB_CLK_ENABLE();
	
	GPIO_InitStruct.Pin      	= GPIO_PIN_12;
  GPIO_InitStruct.Mode      = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull      = GPIO_NOPULL;
  GPIO_InitStruct.Speed     = GPIO_SPEED_FREQ_VERY_HIGH;
	
	HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);
	HAL_GPIO_WritePin(GPIOB, GPIO_PIN_12, GPIO_PIN_SET);
	
	__HAL_RCC_GPIOC_CLK_ENABLE();
	
	GPIO_InitStruct.Pin      	= GPIO_PIN_11;
  GPIO_InitStruct.Mode      = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull      = GPIO_NOPULL;
  GPIO_InitStruct.Speed     = GPIO_SPEED_FREQ_VERY_HIGH;
	
	HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);
	HAL_GPIO_WritePin(GPIOC, GPIO_PIN_11, GPIO_PIN_SET);
	
	SPI_FLASH_Init();
}

void gsensor_INT(void){
	//PC13 GSENSOR
	GPIO_InitTypeDef  GPIO_InitStruct;
	
	__HAL_RCC_GPIOC_CLK_ENABLE();
	__HAL_RCC_SYSCFG_CLK_ENABLE();
	
	GPIO_InitStruct.Pin      	= GSENSOR_PIN;
  GPIO_InitStruct.Mode      = GPIO_MODE_IT_FALLING;
  GPIO_InitStruct.Pull      = GPIO_NOPULL;
  //GPIO_InitStruct.Speed     = GPIO_SPEED_FREQ_VERY_HIGH;
	
	HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);
    
  /* Enable and set Button EXTI Interrupt to the lowest priority */
  NVIC_SetPriority(GSENSOR_EXTI_IRQn, 0x06);
  HAL_NVIC_EnableIRQ(GSENSOR_EXTI_IRQn);
}


void accon_INT(void){
	//PC4 ACC ON
	GPIO_InitTypeDef  GPIO_InitStruct;
	
	__HAL_RCC_GPIOC_CLK_ENABLE();
	__HAL_RCC_SYSCFG_CLK_ENABLE();
	
	GPIO_InitStruct.Pin      	= ACC_PIN;
  GPIO_InitStruct.Mode      = GPIO_MODE_IT_FALLING;
  GPIO_InitStruct.Pull      = GPIO_NOPULL;
  //GPIO_InitStruct.Speed     = GPIO_SPEED_FREQ_VERY_HIGH;
	
	HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);
    
  /* Enable and set Button EXTI Interrupt to the lowest priority */
  NVIC_SetPriority(ACC_EXTI_IRQn, 0x07);
  HAL_NVIC_EnableIRQ(ACC_EXTI_IRQn);
}


void simcard_INT(void){
	//PC0 SIM CARD DETECT
	GPIO_InitTypeDef  GPIO_InitStruct;
	
	__HAL_RCC_GPIOC_CLK_ENABLE();
	__HAL_RCC_SYSCFG_CLK_ENABLE();
	
	GPIO_InitStruct.Pin      	= SIMCARD_PIN;
  GPIO_InitStruct.Mode      = GPIO_MODE_IT_RISING;
  GPIO_InitStruct.Pull      = GPIO_NOPULL;
  //GPIO_InitStruct.Speed     = GPIO_SPEED_FREQ_VERY_HIGH;
	
	HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);
    
  /* Enable and set Button EXTI Interrupt to the lowest priority */
  NVIC_SetPriority(SIMCARD_EXTI_IRQn, 0x08);
  HAL_NVIC_EnableIRQ(SIMCARD_EXTI_IRQn);
}


void RI_INT(void){
	//PB1 RI DETECT
	GPIO_InitTypeDef  GPIO_InitStruct;
	
	__HAL_RCC_GPIOB_CLK_ENABLE();
	__HAL_RCC_SYSCFG_CLK_ENABLE();
	
	GPIO_InitStruct.Pin      	= RI_PIN;
  GPIO_InitStruct.Mode      = GPIO_MODE_IT_RISING;
  GPIO_InitStruct.Pull      = GPIO_NOPULL;
  //GPIO_InitStruct.Speed     = GPIO_SPEED_FREQ_VERY_HIGH;
	
	HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);
    
  /* Enable and set Button EXTI Interrupt to the lowest priority */
  NVIC_SetPriority(RI_EXTI_IRQn, 0x09);
  HAL_NVIC_EnableIRQ(RI_EXTI_IRQn);
}

void sos_INT(void)
{
	//PB1 RI DETECT
	GPIO_InitTypeDef  GPIO_InitStruct;
	
	__HAL_RCC_GPIOA_CLK_ENABLE();
	__HAL_RCC_SYSCFG_CLK_ENABLE();
	
	GPIO_InitStruct.Pin      	= SOS_PIN;
  GPIO_InitStruct.Mode      = GPIO_MODE_IT_RISING;
  GPIO_InitStruct.Pull      = GPIO_NOPULL;
  //GPIO_InitStruct.Speed     = GPIO_SPEED_FREQ_VERY_HIGH;
	
	HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);
    
  /* Enable and set Button EXTI Interrupt to the lowest priority */
  NVIC_SetPriority(SOS_EXTI_IRQn, 0x0A);
  HAL_NVIC_EnableIRQ(SOS_EXTI_IRQn);

}

void initWWDG()
{
	#if 1
	if (__HAL_RCC_GET_FLAG(RCC_FLAG_WWDGRST) != RESET)
  {
    /* Insert 4s delay */
    HAL_Delay(4000);

    /* Clear reset flags */
    __HAL_RCC_CLEAR_RESET_FLAGS();
  }
	
	WwdgHandle.Instance = WWDG;

  WwdgHandle.Init.Prescaler = WWDG_PRESCALER_8;
  WwdgHandle.Init.Window    = 127;
  WwdgHandle.Init.Counter   = 127;

  if (HAL_WWDG_Init(&WwdgHandle) != HAL_OK)
  {
    /* Initialization Error */
    if(DebugEnable) DEBUG("WWDG_Init error");
  }
	#endif
}

void startWWDG()
{
	#if 0
	if (HAL_WWDG_Start(&WwdgHandle) != HAL_OK)
  {
    if(DebugEnable) DEBUG("WWDG_Start error");
  }
	#endif
}


void refreshWWDG()
{
	#if 1
	if (HAL_WWDG_Refresh(&WwdgHandle) != HAL_OK)
  {
      if(DebugEnable) DEBUG("WWDG_Refresh error");
  }
	#endif
}


void deInitWWDG()
{
	#if 0
	if (HAL_WWDG_DeInit(&WwdgHandle) != HAL_OK)
  {
      if(DebugEnable) DEBUG("WWDG_DeInit error");
  }
	#endif
	/* Enable WWDG reset state */
  __HAL_RCC_WWDG_FORCE_RESET();

  /* Release WWDG from reset state */
  __HAL_RCC_WWDG_RELEASE_RESET();
}

void initIWDG(void)
{
	stopIWDG();
	
	IwdgHandle.Instance = IWDG;

  IwdgHandle.Init.Prescaler = IWDG_PRESCALER_32;
  IwdgHandle.Init.Window    = 4095;
  IwdgHandle.Init.Reload   = 4095;

  if (HAL_IWDG_Init(&IwdgHandle) != HAL_OK)
  {
    /* Initialization Error */
    if(DebugEnable) DEBUG("HAL_IWDG_Init error");
  }

}


void MspInitIWDG()
{
	#if 0
	HAL_IWDG_MspInit(&IwdgHandle);
	#endif
}

void startIWDG(void)
{
	#if 0
	if (HAL_IWDG_Start(&IwdgHandle) != HAL_OK)
  {
    if(DebugEnable) DEBUG("HAL_IWDG_Start error");
  }
	#endif
}


void refreshIWDG(void)
{
	//if(DebugEnable) DEBUG("refreshIWDG");
	if (HAL_IWDG_Refresh(&IwdgHandle) != HAL_OK)
  {
     if(DebugEnable) DEBUG("HAL_IWDG_Refresh error");
  }
}

void stopIWDG(void)
{
	if (__HAL_RCC_GET_FLAG(RCC_FLAG_IWDGRST) != RESET)
  {
    /* Insert 4s delay */
    HAL_Delay(4000);

    /* Clear reset flags */
    __HAL_RCC_CLEAR_RESET_FLAGS();
  }
}


void disableIRQ(void)
{
	HAL_NVIC_DisableIRQ(DEBUG_USART_IRQn);
	HAL_NVIC_DisableIRQ(GPS_USART_IRQn);
	HAL_NVIC_DisableIRQ(GSM_USART_IRQn);
	HAL_NVIC_DisableIRQ(LP_USART_IRQn);
	HAL_NVIC_DisableIRQ(I2Cx_IRQn);
	HAL_NVIC_DisableIRQ(SPIx_IRQn);
	HAL_NVIC_DisableIRQ(RTC_IRQn);
	HAL_NVIC_DisableIRQ(GSENSOR_EXTI_IRQn);
	HAL_NVIC_DisableIRQ(ACC_EXTI_IRQn);
	HAL_NVIC_DisableIRQ(SIMCARD_EXTI_IRQn);
	HAL_NVIC_DisableIRQ(RI_EXTI_IRQn);
	
}


/*******************************************************************************
* Function Name	:	LP_USART_IRQHandler
* Description   :	LPUART iterrupt handler
*******************************************************************************/
void LP_USART_IRQHandler(void)
{
	//HAL_UART_IRQHandler(&lpUartHandle);

  /* USER CODE BEGIN AES_RNG_LPUART1_IRQn 0 */
	uint8_t tmp;

	if(LL_LPUART_IsActiveFlag_RXNE(LPUART1)) 
	{
			
	    //LL_GPIO_ResetOutputPin(GPIOA,LL_GPIO_PIN_5);
	    tmp=LL_LPUART_ReceiveData8(LPUART1); 

			shellData(tmp);
	    
	    //LL_USART_TransmitData8(USART5,tmp);  //把数据再从串口发送出去	 

	}
	LL_LPUART_EnableIT_RXNE(LPUART1) ;
  /* USER CODE END AES_RNG_LPUART1_IRQn 0 */
  
  /* USER CODE BEGIN AES_RNG_LPUART1_IRQn 1 */

  /* USER CODE END AES_RNG_LPUART1_IRQn 1 */

}


/*******************************************************************************
* Function Name	:	GSM_USART_IRQHandler
* Description   :	GSM iterrupt handler
*******************************************************************************/
void DEBUG_USART_IRQHandler(void)
{	
	 /* USER CODE BEGIN USART2_IRQn 0 */
	
	if(LL_USART_IsActiveFlag_RXNE(USART2)) //检测是否接收中断
	{
	//LL_GPIO_ResetOutputPin(GPIOA,LL_GPIO_PIN_5);
	aRxShellData = LL_USART_ReceiveData8(USART2);   //读取出来接收到的数据
	shellData(aRxShellData) ;
	//LL_USART_TransmitData8(USART2,tmp);  //把数据再从串口发送出去	 
	}
  /* USER CODE END USART2_IRQn 0 */
  /* USER CODE BEGIN USART2_IRQn 1 */

  /* USER CODE END USART2_IRQn 1 */
}


/*******************************************************************************
* Function Name	:	GSM_USART_IRQHandler
* Description   :	GSM iterrupt handler
*******************************************************************************/
void GPS_GSM_USART_IRQHandler(void)
{	
	//HAL_UART_IRQHandler(&gsmUartHandle);
	//HAL_UART_IRQHandler(&gpsUartHandle);

	if(LL_USART_IsActiveFlag_RXNE(USART4)) 
	{
	    //LL_GPIO_ResetOutputPin(GPIOA,LL_GPIO_PIN_5);
	    aRxGsmData = LL_USART_ReceiveData8(USART4);  
	    gsmData(aRxGsmData) ;
	    
	    //LL_USART_TransmitData8(USART2,aRxGsmData);  //把数据再从串口发送出去	 

	}
	
	if(LL_USART_IsActiveFlag_RXNE(USART5)) 
	{
	    //LL_GPIO_ResetOutputPin(GPIOA,LL_GPIO_PIN_5);
	    aRxGpsData = LL_USART_ReceiveData8(USART5);  
	    gpsData(aRxGpsData) ;
	    //LL_USART_TransmitData8(LPUART1,aRxGpsData);
	    //LL_USART_TransmitData8(USART2,aRxGpsData);  //把数据再从串口发送出去	 

	}
}

/**
  * @brief  Tx Transfer completed callback
  * @param  UartHandle: UART handle. 
  * @note   This example shows a simple way to report end of IT Tx transfer, and 
  *         you can add your own implementation. 
  * @retval None
  */
void HAL_UART_TxCpltCallback(UART_HandleTypeDef *UartHandle)
{
}

/**
  * @brief  UART error callbacks
  * @param  UartHandle: UART handle
  * @note   This example shows a simple way to report transfer error, and you can
  *         add your own implementation.
  * @retval None
*/
void HAL_UART_ErrorCallback(UART_HandleTypeDef *UartHandle)
{
}

/**
  * @brief  Rx Transfer completed callback
  * @param  UartHandle: UART handle
  * @note   This example shows a simple way to report end of DMA Rx transfer, and 
  *         you can add your own implementation.
  * @retval None
  */
void HAL_UART_RxCpltCallback(UART_HandleTypeDef *UartHandle)
{
	 uint8_t ret = HAL_OK ; 
   
	 if(UartHandle->Instance == GSM_USART){
	 do {
        ret = HAL_UART_Receive_IT(UartHandle, (uint8_t*)&aRxGsmData, RXBUFFERSIZE) ; 
    } while (ret != HAL_OK) ;
	 gsmData(aRxGsmData);
	 }
	 else if(UartHandle->Instance == GPS_USART){
	 do {
        ret = HAL_UART_Receive_IT(UartHandle, (uint8_t*)&aRxGpsData, RXBUFFERSIZE) ; 
    } while (ret != HAL_OK) ;
	 gpsData(aRxGpsData);
	 }
	 else if(UartHandle->Instance == DEBUG_USART){
	 do {
        ret = HAL_UART_Receive_IT(UartHandle, (uint8_t*)&aRxShellData, RXBUFFERSIZE) ; 
    } while (ret != HAL_OK) ;
	 shellData(aRxShellData);
	 }
	 //else if(UartHandle->Instance == LPUART_USART)
	 //shellData(aRxData);
}


/**
  * @brief  This function handles External External lines 0 to 1 interrupt request.
  * @param  None
  * @retval None
  */
void EXTI0_1_IRQHandler(void){
	HAL_GPIO_EXTI_IRQHandler(SIMCARD_PIN);
}

/**
  * @brief  This function handles External External lines 4 to 15 interrupt request.
  * @param  None
  * @retval None
  */
void EXTI4_15_IRQHandler(void)
{
  HAL_GPIO_EXTI_IRQHandler(GSENSOR_PIN);
	HAL_GPIO_EXTI_IRQHandler(ACC_PIN);
	HAL_GPIO_EXTI_IRQHandler(SOS_PIN);
	//*(__IO uint32_t *) 0xA0001000 = 0xFF;
}

/**
  * @brief EXTI line detection callbacks
  * @param GPIO_Pin: Specifies the pins connected EXTI line
  * @retval None
  */
void HAL_GPIO_EXTI_Callback(uint16_t GPIO_Pin)
{
	if(GPIO_Pin == GSENSOR_PIN || GPIO_Pin == ACC_PIN || GPIO_Pin == SIMCARD_PIN)
  {
		 __HAL_PWR_CLEAR_FLAG(PWR_FLAG_WU);
		if ((GlobalStatus & GSM_SLEEP)){
			//setEvent(DISABLE_RTC_EVENT);	
		}else{
		setEvent(GSENSOR_SHOCK_EVENT);
		}
		#if 0
		if ((GlobalStatus & GSM_SLEEP)){
		wakeup();
		setEvent(DISABLE_RTC_EVENT);	
		}else
		{
		setEvent(GSENSOR_SHOCK_EVENT);
		notifyShock();
		}
		if(HAL_GPIO_ReadPin(GPIOB,GPIO_PIN_8) == GPIO_PIN_RESET)
		HAL_GPIO_WritePin(GPIOB, GPIO_PIN_8, GPIO_PIN_SET);
		else
		HAL_GPIO_WritePin(GPIOB, GPIO_PIN_8, GPIO_PIN_RESET);
		#endif
	}
	
}

void HAL_RTCEx_WakeUpTimerEventCallback(RTC_HandleTypeDef *hrtc)
{
	#if 0
	if ((GlobalStatus & GSM_SLEEP)){
		//HAL_PWREx_DisableLowPowerRunMode();
    /* Wait until the system exits LP RUN and the Regulator is in main mode */
    //while(__HAL_PWR_GET_FLAG(PWR_FLAG_REGLP) != RESET)
    //{
    //}
		wakeup();
		setEvent(DISABLE_RTC_EVENT);
	}
	#endif

	sleep_time++;
	if(sleep_time > 11 && getWakeup_Report()){
		sleep_time = 0;
		rtcReport = false;
		if(!isAccOn())
		setEvent(GSENSOR_SLEEP_EVENT);
	}else if( sleep_time == 10)
	{
		rtcReport = false;
		setEvent(RTC_REPORT_EVENT);
	}
	else if( sleep_time < 11)
	{ //
		if(!GpsNoData() && getWakeup_Report() && !isAccOn() && getTCPConState())
		{
			setEvent(RTC_REPORT_EVENT);
		}
	}
}



/**
  * @brief  I2C error callbacks.
  * @param  I2cHandle: I2C handle
  * @note   This example shows a simple way to report transfer error, and you can
  *         add your own implementation.
  * @retval None
  */
void HAL_I2C_ErrorCallback(I2C_HandleTypeDef *I2cHandle)
{
   if(DebugEnable) DEBUG("i2c error");
	 i2cError++;
	 if(i2cError == 10)
		 SystemRestart();
}



/**
  * @brief  TxRx Transfer completed callback.
  * @param  hspi: SPI handle
  * @note   This example shows a simple way to report end of Interrupt TxRx transfer, and 
  *         you can add your own implementation. 
  * @retval None
  */
void HAL_SPI_TxRxCpltCallback(SPI_HandleTypeDef *hspi)
{
	 wTransferState = TRANSFER_COMPLETE;
}

/**
  * @brief  SPI error callbacks.
  * @param  hspi: SPI handle
  * @note   This example shows a simple way to report transfer error, and you can
  *         add your own implementation.
  * @retval None
  */
void HAL_SPI_ErrorCallback(SPI_HandleTypeDef *hspi)
{
	wTransferState = TRANSFER_ERROR;
}
