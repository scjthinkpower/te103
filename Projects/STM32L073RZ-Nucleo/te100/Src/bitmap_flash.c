
#include "spi_flash.h"
#include "bitmap_flash_cfg.h"
#include "bitmap_flash.h"
#include "common.h"
#include "FreeRTOS.h"
#include "task.h"
#include "debug.h"

uint16_t PktUnreadCnt;           	// all unread packets in flash
static uint32_t BitFlagAddrIn;         	// the addr to store the write bit flag 
static uint32_t BitFlagAddrOut;           	// the addr to store the read bit flag 

static uint16_t bit_map_get_unread_pkt_num (uint32_t addr_start, uint32_t addr_end);
static uint8_t  bit_map_get_write_idx      (bit_mask_t *pbit_mask);
static uint8_t  bit_map_get_read_idx       (bit_mask_t *pbit_mask);
static uint32_t bit_map_idx_to_addr        (uint16_t idx);
static uint16_t bit_map_bit_to_idx         (bit_mask_t *pbit_mask);
static void  bit_map_erase_flag_sector  (bit_mask_t *pbit_mask);
static void  bit_map_erase_data_sector  (uint32_t addr, uint16_t bytes);

void bit_map_init(void)
{
    	PktUnreadCnt   = bit_map_get_unread_pkt_num(BIT_FLAG_ADDR_START, BIT_FLAG_ADDR_END);
	    if(DebugEnable) DEBUG("###PktUnreadCnt %d",PktUnreadCnt) ;
    	BitFlagAddrIn  = BIT_FLAG_ADDR_START;
    	BitFlagAddrOut = BIT_FLAG_ADDR_START;
}

void bit_map_init_(void)
{
			if(DebugEnable) DEBUG("bit_map_init_ %d",PktUnreadCnt) ;
    	PktUnreadCnt   = 0;
    	BitFlagAddrIn  = BIT_FLAG_ADDR_START;
    	BitFlagAddrOut = BIT_FLAG_ADDR_START;
}

uint8_t bit_map_write_data(const char *pbuf, uint16_t bytes)
{
    uint32_t       		addr_write;                                 // addr to stored log data
    uint16_t       		pkt_idx;
    bit_mask_t  	bit_mask;
    
    if (PktUnreadCnt >= PKT_CNT_MAX) {//16384
        return false;
    }

	//Reset_WWDG();
	//Disable_WWDG();
	//taskENTER_CRITICAL();
	//__disable_irq() ;
	//MSD0_SPI_Configuration(); 

    bit_mask.AddrFlagStart = BitFlagAddrIn;
    if (bit_map_get_write_idx(&bit_mask) == true) {
        BitFlagAddrIn = bit_mask.AddrFlag;                  // update addr in
        pkt_idx       = bit_map_bit_to_idx(&bit_mask);      // 0 to PKT_CNT_MAX - 1
        addr_write    = bit_map_idx_to_addr(pkt_idx);
			
			  if(addr_write < SPIFLASH_RESERVE_END)
							return false;
        bit_map_erase_data_sector(addr_write, bytes);       // check if need to erase sector
        SPI_FLASH_BufferWrite((uint8_t*)pbuf, addr_write, bytes);
        SPI_FLASH_BufferWrite( (uint8_t*)&bit_mask.FlagNew, bit_mask.AddrFlag, 1);
			
				//if(DebugEnable)
        //DEBUG("bit_mask.FlagNew 0x%x",bit_mask.FlagNew) ;
			
				if(DebugEnable)
        DEBUG("bit_map_write_data pkt_idx = 0x%x , addr_write = 0x%x, bit_mask.addrFlag=0x%x,PktUnreadCnt = 0x%x,BitFlagAddrIn=0x%x,bit_mask.Offset=0x%x", 
				pkt_idx , addr_write, bit_mask.AddrFlag, PktUnreadCnt,BitFlagAddrIn,bit_mask.Offset) ;
			
        PktUnreadCnt++;
				
        bit_map_erase_flag_sector(&bit_mask);

        if (pkt_idx >= PKT_CNT_MAX - 1) {
            BitFlagAddrIn = BIT_FLAG_ADDR_START;
        }
	//__enable_irq() ;
	//taskEXIT_CRITICAL();
	//Enable_WWDG();
	
        return true;
    }
	else{

		bit_map_init();
    //__enable_irq() ;
		//taskEXIT_CRITICAL();
		//Enable_WWDG();
		return true;
	}
}

uint8_t backupbuf[SPIFLASH_SECTORSIZE/4];
void bit_map_write_data_index(const char *pbuf, uint16_t bytes,uint16_t index)
{
	 //uint8_t backupbuf[1024];
	 //uint16_t sector_cur;
	
   uint32_t       		addr_write;                                 // addr to stored log data
	 addr_write = SPIFLASH_RESERVE_START;
	 addr_write += index *bytes;
	 if(addr_write > SPIFLASH_RESERVE_START+SPIFLASH_SECTORSIZE/4)
			return;

	 memset(backupbuf,0,SPIFLASH_SECTORSIZE/4);
	 SPI_FLASH_BufferRead((uint8_t*)backupbuf, SPIFLASH_RESERVE_START, SPIFLASH_SECTORSIZE/4);
	 
	 //bit_map_erase_data_sector(addr_write, bytes);
	 SPI_FLASH_SectorErase(SPIFLASH_RESERVE_START);
	 
	 
	 SPI_FLASH_BufferWrite((uint8_t*)backupbuf, SPIFLASH_RESERVE_START, addr_write-SPIFLASH_RESERVE_START);
	 SPI_FLASH_BufferWrite((uint8_t*)pbuf, addr_write, bytes);
	 SPI_FLASH_BufferWrite((uint8_t*)backupbuf+(index+1)*bytes, addr_write+bytes, SPIFLASH_RESERVE_START+SPIFLASH_SECTORSIZE/4-addr_write-bytes);
	 
}


void bit_map_write_data_(const char *pbuf, uint16_t bytes)
{
	 //uint8_t backupbuf[1024];
	 //uint16_t sector_cur;
	
   uint32_t       		addr_write;                                 // addr to stored log data
	 addr_write = SPIFLASH_SYS_SETTINGS_ADDR_START;

	 if(addr_write > SPIFLASH_SYS_SETTINGS_ADDR_START+SPIFLASH_SECTORSIZE/4)
			return;

	 //bit_map_erase_data_sector(addr_write, bytes);
	 SPI_FLASH_SectorErase(SPIFLASH_SYS_SETTINGS_ADDR_START);
	 SPI_FLASH_BufferWrite((uint8_t*)pbuf, addr_write, bytes);
}


void bit_map_write_agps_data_index(const char *pbuf, uint16_t bytes,uint16_t index)
{
	 //uint8_t backupbuf[1024];
	 //uint16_t sector_cur;
	
   uint32_t       		addr_write;                                 // addr to stored log data
	 addr_write = SPIFLASH_BIT_MAP_ADDR_START2+4;
	 addr_write += index *bytes;
	 if(addr_write >SPIFLASH_BIT_MAP_ADDR_END3)
			return;
 
	 SPI_FLASH_BufferWrite((uint8_t*)pbuf, addr_write, bytes);
	 
}

void bit_map_write_agps_flag(const char *pbuf, uint16_t bytes)
{
	 //uint8_t backupbuf[1024];
	 //uint16_t sector_cur;
	
   uint32_t       		addr_write;                                 // addr to stored log data
	 addr_write = SPIFLASH_BIT_MAP_ADDR_START2;
	 if(addr_write >SPIFLASH_BIT_MAP_ADDR_START2+4)
			return;
 
	 SPI_FLASH_BufferWrite((uint8_t*)pbuf, addr_write, bytes);
	 
}

uint8_t bit_map_read_data(char *pbuf, uint16_t bytes)
{
    uint32_t       addr_read;
    uint16_t       pkt_idx;
    bit_mask_t  bit_mask;
    
    if (PktUnreadCnt == 0) {
        return false;
    }

	//Reset_WWDG();
	//Disable_WWDG();
	//taskENTER_CRITICAL();
	//__disable_irq() ;
	//MSD0_SPI_Configuration(); 
    bit_mask.AddrFlagStart = BitFlagAddrOut;
    if (bit_map_get_read_idx(&bit_mask) == true) {
        BitFlagAddrOut = bit_mask.AddrFlag;
        pkt_idx  = bit_map_bit_to_idx(&bit_mask);
        addr_read = bit_map_idx_to_addr(pkt_idx);
			
			if(DebugEnable)
			DEBUG("bit_map_read_data pkt_idx = 0x%x , addr_read = 0x%x, bit_mask.addrFlag=0x%x,PktUnreadCnt = 0x%x,BitFlagAddrOut=0x%x", 
				pkt_idx , addr_read, bit_mask.AddrFlag, PktUnreadCnt,BitFlagAddrOut) ;
			
			
        SPI_FLASH_BufferRead((uint8_t*)pbuf, addr_read, bytes);
				SPI_FLASH_BufferWrite((uint8_t*)&bit_mask.FlagNew, bit_mask.AddrFlag, 1);
        PktUnreadCnt--;
        if (pkt_idx >= PKT_CNT_MAX - 1) {
            BitFlagAddrOut = BIT_FLAG_ADDR_START;
        }

	 //taskEXIT_CRITICAL();
   // __enable_irq() ;
	 //Enable_WWDG();
				//if(DebugEnable)
				//DEBUG("Numbers Gprs_packets in Falsh : %d",PktUnreadCnt);
        return true;
    } else {
       PktUnreadCnt = 0;
			 PktUnreadCnt   = bit_map_get_unread_pkt_num(BIT_FLAG_ADDR_START, BIT_FLAG_ADDR_END);
	     bit_map_init();
    }
     //  __enable_irq() ;
    //taskEXIT_CRITICAL();
	//Enable_WWDG();
    return false;
}


void bit_map_read_data_index(char *pbuf, uint16_t bytes,uint16_t index)
{
		uint32_t       addr_read;
		addr_read = SPIFLASH_RESERVE_START;
		addr_read += index *bytes;
		if(addr_read > SPIFLASH_RESERVE_END)
				return;
		SPI_FLASH_BufferRead((uint8_t*)pbuf, addr_read, bytes);			
}


void bit_map_read_data_(char *pbuf, uint16_t bytes)
{
		uint32_t       addr_read;
		addr_read = SPIFLASH_SYS_SETTINGS_ADDR_START;
		if(addr_read > SPIFLASH_SYS_SETTINGS_ADDR_END)
				return;
		SPI_FLASH_BufferRead((uint8_t*)pbuf, addr_read, bytes);			
}

void bit_map_read_agps_data_index(char *pbuf, uint16_t bytes,uint16_t index)
{
		uint32_t       addr_read;
		addr_read = SPIFLASH_BIT_MAP_ADDR_START2+4;
		addr_read += index *bytes;
		if(addr_read > SPIFLASH_BIT_MAP_ADDR_END3)
				return;
		SPI_FLASH_BufferRead((uint8_t*)pbuf, addr_read, bytes);			
}

void bit_map_read_agps_flag(char *pbuf, uint16_t bytes)
{
		uint32_t       addr_read;
		addr_read = SPIFLASH_BIT_MAP_ADDR_START2;
		if(addr_read > SPIFLASH_BIT_MAP_ADDR_START2+4)
				return;
		SPI_FLASH_BufferRead((uint8_t*)pbuf, addr_read, bytes);			
}


void bit_map_erase_flag_start(void)
{
SPI_FLASH_SectorErase(SPIFLASH_SYS_SETTINGS_ADDR_START);
}

void bit_map_erase_agps_start(void)
{
 //bit_map_erase_data_sector(addr_write, bytes);
	SPI_FLASH_SectorErase(SPIFLASH_BIT_MAP_ADDR_START2);
	SPI_FLASH_SectorErase(SPIFLASH_BIT_MAP_ADDR_START3);
}


uint8_t bit_map_unread(void)
{
    if (PktUnreadCnt > 0) {
        return true;
    } else {
				bit_map_init_();
        return false;
    }
}

static uint32_t bit_map_idx_to_addr(uint16_t idx)
{
    uint32_t addr, tmp;
    tmp  = idx;
    addr = tmp * DATA_LOGGER_PKT_LEN + DATA_ADDR_START;
    return addr;
}

static uint16_t bit_map_bit_to_idx(bit_mask_t *pbit_mask)
{
    uint16_t idx;
		//if(DebugEnable)
		//DEBUG("0x%x 0x%x",pbit_mask->AddrFlag,pbit_mask->Offset);
    if (pbit_mask->AddrFlag < BIT_FLAG_ADDR_START1) {//在sector2 中
        idx = (pbit_mask->AddrFlag - BIT_FLAG_ADDR_START0) * 4 + pbit_mask->Offset;
    } else {//在sector3中
        idx = (pbit_mask->AddrFlag - BIT_FLAG_ADDR_START1) * 4 + pbit_mask->Offset;
    }
    return idx;
}

static uint8_t bit_map_get_read_idx(bit_mask_t *pbit_mask)
{  
    char buf[128];
    char  tmp;
    uint8_t  i, j, end;
    uint16_t addr_offset, len;
    uint32_t addr_sch;             // addr search

    if (pbit_mask->AddrFlagStart > BIT_FLAG_ADDR_END) {
				BitFlagAddrOut = BIT_FLAG_ADDR_START;
        //return false;
    }
    
    end         = false;
    addr_offset = 0;
    len         = 0;
    addr_sch    = pbit_mask->AddrFlagStart;
    
    while (end == false) {
	//Reset_WWDG();
        if ((addr_sch + sizeof(buf)) <= BIT_FLAG_ADDR_END) {
            len = sizeof(buf);
        } else {
            len = BIT_FLAG_ADDR_END - addr_sch + 1;
            end = true;
        }
        
        SPI_FLASH_BufferRead((uint8_t*)buf, addr_sch, len);

        for (i = 0; i < len; i++) {
            tmp = buf[i];
            if (tmp != 0xFF) {
                for (j = 0; j < 4; j++) {
                    if ((tmp & 0x03) == BIT_FLAG_UNREAD) {//??????
                        pbit_mask->Flag     = buf[i];
                        pbit_mask->Offset   = j;
                        tmp                 = 0x01;
                        tmp                 = tmp << (j * 2 + 1);
                        tmp                 = ~tmp;
                        pbit_mask->FlagNew  = tmp & pbit_mask->Flag; 
                        pbit_mask->AddrFlag = pbit_mask->AddrFlagStart + addr_offset;
                        return true;
                    } else {
                        tmp >>= 2;
                    }
                }//end for
            }
            addr_offset++;                // next byte
        }
        addr_sch += len;
    }//end while
    return false;
}

static void bit_map_erase_flag_sector(bit_mask_t *pbit_mask)
{
    if (pbit_mask->AddrFlag == BIT_FLAG_ADDR_END0) {
        if (pbit_mask->Offset == 3) {                   // erase second sector
	      SPI_FLASH_SectorErase(BIT_FLAG_ADDR_START1);
        }
    } else if (pbit_mask->AddrFlag == BIT_FLAG_ADDR_END1) {
        if (pbit_mask->Offset == 3) {                   // erase first sector
            SPI_FLASH_SectorErase(BIT_FLAG_ADDR_START0);
        }
    } 
}


void bit_map_erase_flag_all_sector(void)
{
SPI_FLASH_SectorErase(BIT_FLAG_ADDR_START0);
SPI_FLASH_SectorErase(BIT_FLAG_ADDR_START1);
}

/*
*  get the first available idx
*/
static uint8_t bit_map_get_write_idx(bit_mask_t *pbit_mask)
{
    char  buf[128];
    char  tmp;
    uint8_t  i, j, end;
    uint16_t addr_offset, len;
    uint32_t addr_sch;             // addr search

    if (pbit_mask->AddrFlagStart > BIT_FLAG_ADDR_END) {
		BitFlagAddrIn  = BIT_FLAG_ADDR_START;
       // return false;
    }
    
    end         	= false;
    addr_offset = 0;
    len         = 0;
    addr_sch    = pbit_mask->AddrFlagStart;
    
    while (end == false) {
	 //Reset_WWDG();
	 //Disable_WWDG() ;
        if ((addr_sch + sizeof(buf)) <= BIT_FLAG_ADDR_END) {
            len = sizeof(buf);                   // not overstep the bit map space
        } else {                                    
            len = BIT_FLAG_ADDR_END - addr_sch + 1; // oversteped the bit map space
            end = true;
        }
        // add
        SPI_FLASH_BufferRead((uint8_t*)buf, addr_sch, len);

        for (i = 0; i < len; i++) {
	    	tmp = buf[i];
	    	if (tmp != 0x00) {   //前面 读过的 两个两个bit位可能都是0X00
	        	for (j = 0; j < 4; j++) {
	            	if ((tmp & 0x03) == BIT_FLAG_VALID) {
	                		pbit_mask->Flag     = buf[i];
	                		pbit_mask->Offset   = j;
	                		tmp                 = 0x01;
	                		tmp                 = tmp << (j * 2);
	                		tmp                 = ~tmp;     
	                		pbit_mask->FlagNew  = tmp & pbit_mask->Flag; //把对应标志bit位由0x03有效的变为0x02未读的
	                		pbit_mask->AddrFlag = pbit_mask->AddrFlagStart + addr_offset;
	                		return true;
	            	} else {
	                		tmp >>= 2;
	        		}
	        	}
            }
            addr_offset++;// next byte
        }
        addr_sch += len;
	 //Enable_WWDG() ;
    }
    
    return false;
}

static uint16_t bit_map_get_unread_pkt_num(uint32_t addr_start, uint32_t addr_end)
{
    char buf[128];
    char  tmp;
    uint8_t  i, j;
    uint16_t len;
    uint16_t pkt_num;
    uint8_t  end;  

    if (addr_end <= addr_start) {
        return 0;
    }
    pkt_num = 0;
    end = false;
    
	while (end == false) 
	{	
		//Reset_WWDG();
		if ((addr_start + sizeof(buf)) <= addr_end) {
			len = sizeof(buf);
        } else {
            len = addr_end - addr_start + 1;
            end = true;
        }
		
        SPI_FLASH_BufferRead((uint8_t*)buf, addr_start, len);

        for (i = 0; i < len; i++) {
            tmp = buf[i];
            if (tmp != 0xFF && tmp != 0x00) {
                for (j = 0; j < 4; j++) {      //一个字节存4个bitflag
                    if ((tmp & 0x03) == BIT_FLAG_UNREAD) {
                        pkt_num++;
                    } else {
                        tmp >>= 2;
                    }
                }
            }
        }
        addr_start += len;
	}
    return pkt_num;
}

static void bit_map_erase_data_sector(uint32_t addr, uint16_t bytes)
{
    char buf[64];
    uint8_t  i;
    uint16_t sector_cur, sector_next;
    uint32_t addr_next_sector;

    sector_cur  = addr / SPIFLASH_SECTORSIZE;
    sector_next = (addr + bytes) / SPIFLASH_SECTORSIZE;

    if (sector_cur == sector_next) {    		// same sector
	 SPI_FLASH_BufferRead((uint8_t*)buf, addr, sizeof(buf));
        for (i = 0; i < sizeof(buf); i++) {	// check the first 64 bytes
           	//Reset_WWDG();
		if (buf[i] != 0xFF) {                   
                SPI_FLASH_SectorErase(sector_cur*SPIFLASH_SECTORSIZE);
                return;
            }
        }
    } else { 							// double sector
        addr_next_sector = sector_next * SPIFLASH_SECTORSIZE;
        SPI_FLASH_BufferRead((uint8_t*)buf, addr_next_sector, sizeof(buf));
        for (i = 0; i < sizeof(buf); i++) {	// check the first 64 bytes
           	//Reset_WWDG();
		if (buf[i] != 0xFF) {
		   SPI_FLASH_SectorErase(addr_next_sector);
                return;
            }
        }
    }
}


bool SPI_FLASH_OK = false ;
/**
* brief : spi flash initiation for the bit map flash operation
* params :
*     none
* return :
*     > 0 indicate success 
*     < 0 error 
**/
int SpiFlashInit(void)
{

	int ret = -1 ;
	uint32_t spiFlashID = 0;
	//MSD0_SPI_Configuration() ;
	SPI_FLASH_Init();
	spiFlashID =SPI_FLASH_ReadID();

	DEBUG("\r\nSPIFlash ID:%x", spiFlashID);
#if 0	
	if ((spiFlashID ==  SPI_FLASH_ID) || 
		(spiFlashID == SPI_FLASH_ID_1)) {
#else
       if (spiFlashID != 0) {   // even we have a ID, we regard as a valid spi flash 
#endif
		DEBUG("Flash OK.\n");
		ret = 1 ;
		SPI_FLASH_OK = true ;
	}else{
		DEBUG("Flash ERROR.\n");
		ret = -1 ;
		SPI_FLASH_OK = false ;
	}
	return ret ;
}
		
void bitmapEnable(bool enable)
{
	DebugEnable = enable;
}


