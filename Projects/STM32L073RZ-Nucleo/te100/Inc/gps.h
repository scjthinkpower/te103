#ifndef __GPS_H__
#define __GPS_H__

#include <stdint.h>
#include "hardware.h"
#include "gsm.h"
#include "ring_buffer.h"
#include "FreeRTOS.h"
#include "semphr.h"

#define GPS_MODE_IGNORE 0
#define GPS_MODE_START	1
#define GPS_MODE_END	2
#define GPS_MODE_READY	3
#define GPS_MODE_CHECK	4
#define GPS_MODE_CHECK2	5
#define GPS_MODE_END2	6

#define LOC_INFO_LATITUDE_DU_LENGTH             2
#define LOC_INFO_LATITUDE_FEN_LENGTH           7
#define LOC_INFO_LONGITUDE_DU_LENGTH           3
#define LOC_INFO_LONGITUDE_FEN_LENGTH         7

struct GpsContext_s
{
			const int8_t *WaitAliveText;
			const struct GpsCmds_s *GpsCommand;
			uintmax_t	SendInterval;
			uintmax_t	SendTimer;
			uint32_t	NoDataTimer;
			uint32_t	LastFilterTime;
			uint16_t	RxIndex;
			uint16_t	WaitAliveSize;
			uint32_t	AverageTime;
			uint8_t		AverageIndex;
			uint8_t		AverageCount;
			uint8_t		AverageMaxCount;

			volatile uint8_t	RxMode;
			int8_t		Data;
			RingBuffer_t	RB_Tx;

			GsmTask_t	GsmTask;
			nmeaGPRMC	RMC;
			nmeaGPGSV GSV;
			nmeaGPGSA GSA;
	
			uint32_t mileage ;           // km as unit 
			uint32_t meters ;      // record the meter of driving
			uint32_t enginHour ; 
};

struct GpsCmds_s {
	uint8_t (*Init)(struct GpsContext_s * gc);
};

typedef struct GpsCmds_s GpsCommands_t;
typedef struct GpsContext_s GpsContext_t;

extern GpsContext_t GpsContext;

void vGpsTask(void const *pvArg);
void vGpsReceive(void const *pvArg);
void vGpsHandle(void const *pvArg);
void GpsPut(uint8_t data);
void GpsPutLine(const int8_t * text);
void GpsPutLineCS(const int8_t * text);
int8_t GpsGet(void);
void gpsData(uint8_t data);
void gpsPowerOff(void);
void gpsPowerOn(void);
void setGpsSleep(bool sleep);
bool getGpsSleep(void);
bool Gps_Ignore(void);
void calculate_whole_enginhour(void);
void calculate_whole_mileage(int distance);
bool GpsNoData(void);
nmeaGPRMC* getGPRMCData(uint8_t event_type);
nmeaGPRMC* getLastGPRMCData(void);
nmeaGPGSV* getLastGPGSVData(void);
nmeaGPGSA* getLastGPGSAData(void);
point_t* getPoint(void);
void gpsDebugEnable(bool enable);
void initEH(uint8_t* initValue);
void initML(uint8_t* initValue);
void open_eh_and_ml_file(void);
void open_gefence_file(void);
int getEnginHour(void);
int getMileage(void);
void setGpsSendTimer(uint8_t event);
void gpsDataLogEnable(bool enable);
void setGc_enginHour(uint8_t* initValue);
void setGc_mileage(uint8_t* initValue);
void read_eh_ml_data(void);
void write_eh_data(uint32_t value);
void write_ml_data(uint32_t value);
uint32_t read_out1_data(void);
uint32_t read_out2_data(void);
void write_out1_status(uint32_t value);
void write_out2_status(uint32_t value);
uint32_t read_message_number(void);
void send_message_number(uint32_t value);
void initOutStatus(void);
bool rtc_set(void);
bool heading_compare(double* sourcepoint,double comparepoint,double angle);
double getGpsSpeed(void);
void keepLastnmeaGPRMC(void);
void setTime_noupdate(void);
bool getGpsRun(void);
void testGpsRun(bool run);
void CFC_GLONASS_OR_BDS(bool bds)	;
void CFC_HOT_RST(void);
void CFC_WARM_RST(void);
void CFC_COLD_RST(void);
void AID_ALP(uint8_t* Buffer,uint32_t len);
#endif
