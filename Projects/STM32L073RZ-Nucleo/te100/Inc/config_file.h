#ifndef __CONFIG_FILE_H__
#define __CONFIG_FILE_H__

#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <stdbool.h>
#include <ff.h>
#include "geofence.h"

#define IAP_CONF  "iap"
#define SN_CONF  "sn"
#define SERVERCENTERTELENUMBER_CONF "servercentertelenumber"
#define SMSCENTERTELENUMBER_CONF       "smscentertelenumber"
#define QUERYFEETELENUMBER_CONF         "queryfeetelenumber"
#define SOSTELENUMBER_CONF                   "sos"
#define SOS1TELENUMBER_CONF                 "sos1"
#define SOS2TELENUMBER_CONF                 "sos2"
#define SOS3TELENUMBER_CONF                  "sos3"
#define MAINSERVERIPADDR_CONF              "ipaddr"
#define MAINSERVERPORTNUMBER_CONF     "port"
#define MAINSERVERDOMAINNAME_CONF    "domain"
#define BACKSERVERIPADDR_CONF              "backserveripaddr"
#define BACKSERVERPORTNUM_CONF           "backserverportnumber"
#define BACKSERVERDOMAINNAME_CONF    "backserverdomainname"
#define APN_CONF                                        "apn"
#define APNNAME_CONF                               "apnname"
#define APNUSER_CONF                                "apnuser"
#define APNPASS_CONF                                "apnpass"
#define REPINTERVAL_CONF                         "repinterval"
#define ACCOFFREPINTERVAL_CONF                         "accoffrepinterval"
#define GSMSWITCH_CONF                           "gsmswitch"
#define WORKINGMODE_CONF                      "workingmode"
#define ACCSLEEP_CONF                              "accsleep"
#define ACCSTATUSHINT_CONF                    "accstatushint"
#define WARNSHOCK_CONF                          "warnshock"
#define WARNEFAN_CONF                            "warnefan"
#define WARNLOWBATT_CONF                     "warnlowbatt"
#define WARNOVERSPEED_CONF                  "warnoverspeed"
#define WARNURGENT_CONF                        "warnurgent"
#define WARNCIRCLE_CONF                         "warncircle"
#define LANGUAGE_CONF                             "language"
#define SMSC_CONF                                      "smsc"
#define GPS_MOTION_THRESHOLD_CONF     "gpsthreshold"
#define SPEED_CONF                                    "speed"
#define GSENSOR_SENSITIVITY_CONF         "gsensor"
#define TIMEZONE_CONF                              "timezone"
#define SW_VERSION_CONF                          "version"
#define SIMULATE_ACC_CONF                        "simulateacc"
#define ANGLE_CONF                            "angle"
#define DISTANCE_CONF                            "distance"
#define OVERSPEED_TIME_CONF                           "speedtime"
#define PING_TIME_CONF                           "pingtime"
#define RESERVE1_CONF                           "reserve1"
#define RESERVE2_CONF                           "reserve2"
#define RESERVE3_CONF                           "reserve3"
#define RESERVE4_CONF                           "reserve4"
#define RESERVE5_CONF                           "reserve5"
#define RESERVE6_CONF                           "reserve6"
#define RESERVE7_CONF                           "reserve7"
#define RESERVE8_CONF                           "reserve8"


#define LOCATION_DATA    "location.txt"
#define EH_FILE    "eh.txt"
#define ML_FILE    "ml.txt"
#define GEOFENCE_DATA    "geofence.txt"

bool create_sw_conf_table(void);
bool init_sw_conf_table(void) ;
bool update_conf_elem(uint8_t *name, uint8_t *val);
uint8_t* get_conf_elem(uint8_t *name);
void show_sw_conf(void);
void delete_sw_conf(void);
void delete_sw_conf_noReset(void);
bool fileSystem(bool action);
bool createfile(uint8_t* filename);
bool openfile(uint8_t* filename);
bool writefile(uint8_t* filename,uint8_t* wtext);
uint32_t readfile(uint8_t* filename);
bool openReadFile(uint8_t* filename);
bool closeFile(uint8_t* filename);
bool seekfile(void);
bool deletefile(uint8_t* filename);
void fatfsUnLink(void);
void dolocationDatahandle(void);
void configFileDebugEnable(bool enable);
uint8_t* getReadData(void);
bool seekfilesize(DWORD size);
uint32_t getfilesize(uint8_t* filename);
bool update_conf_elem_map(uint8_t *name, uint8_t *val);
bool update_conf_elem_rom(void);
bool getWriteFilepStatus(void);
bool writegeofendata(geofence_t* geodata,uint32_t index);
uint32_t readGeofencedata(geofence_t* geodata,uint32_t index);
#endif

