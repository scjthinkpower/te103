#ifndef __GSENSOR_H__
#define __GSENSOR_H__

#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#include "hardware.h"
#include "main.h"

#define I2C_ADDRESS_WRITE        0x32
#define I2C_ADDRESS_READ        0x33
#define REQ_X_L    0x28
#define REQ_X_H    0x29
#define REQ_Y_L    0x2a
#define REQ_Y_H    0x2b
#define REQ_Z_L    0x2c
#define REQ_Z_H    0x2d
#define FIFO_MODE_CTRL 0x2e
#define GSENSOR_STACK_SIZE	0x10

/* I2C TIMING Register define when I2C clock source is SYSCLK */
/* I2C TIMING is calculated in case of the I2C Clock source is the SYSCLK = 32 MHz */
//#define I2C_TIMING    0x10A13E56 /* 100 kHz with analog Filter ON, Rise Time 400ns, Fall Time 100ns */ 
#define I2C_TIMING      0x00B1112E /* 400 kHz with analog Filter ON, Rise Time 250ns, Fall Time 100ns */

typedef struct gsensor_s {
	/** Stack */
	u8	stack[GSENSOR_STACK_SIZE];
	int tsk_id;
	int flg_id;
	int gsensor_flg;
  //Attribute
  int gsensor_enable;
  int write_flag;
}gsensor_detect_t;

typedef struct gsensor_info {
	int g_sensor_id;
	int sensor_axis;
	int max_sense;
	int max_bias;
	int adc_resolution;
	int sampling_cycle;

}g_sensor_info_t;

void vGsensor(void const *pvArg);
uint16_t readGsenserXYZ(uint16_t xyz);
uint8_t initGsensor(void);
uint8_t writeReg(u8 reg,u8 value);
uint8_t readReg(uint8_t address);
bool notifyShock(void);
uint8_t gsensor_detect(void);
bool getMoveState(void);
void setMoveState(bool isMove);
#endif

