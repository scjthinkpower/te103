#ifndef _GEOFENCE_H
#define _GEOFENCE_H

#include "debug.h"

#define   EARTH_RADIUS                        6378137    //地球赤道半径
#define   __PI                                3.1415926

#define GEO_STATUS_UNKNOW  0x00
#define GEO_STATUS_IN      0x01
#define GEO_STATUS_OUT     0x02


typedef struct {
    unsigned char inCnt;
    unsigned char outCnt;
    unsigned char status;
} geoStatus_t;   //*pgeoStatus_t;

//point
typedef struct {
    double lat;    //纬度<只有度>
    double lng;   //经度  <只有度>
		double lat1;    //纬度<只有度>
    double lng1;
		double speed;
} point_t;

// geo-fence(40 byte)
typedef struct {
    point_t Point;
    double  Radius;  //单位km
		char    AlarmType;
		char    Zone;
		char    Type;
    char    Name[16];
} geofence_t;

extern int addGeofenceData(geofence_t* geo_data);
extern int deleteGeofenceData(geofence_t* geo_data);

extern void  geofence_init(void);
extern void  geocheck_check(point_t * pointcur_t);
extern uint8_t* geofence_state();
void geofence_alarm_info(geofence_t* geo,uint8_t* geoStatus,uint8_t speedstatus);

#endif
