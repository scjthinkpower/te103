#ifndef __GPSONE_H__
#define __GPSONE_H__

#include <stdint.h>
#include <stdbool.h>

#define DGG_FRCMD			"$FRCMD,"
#define DGG_FRERR			"$FRERR,"
#define DGG_FRRET			"$FRRET,"
#define DGG_FRSES			"$FRSES,"
#define DGG_FRERR_AUTH		"$FRERR,AuthError,"
#define DGG_FRERR_NOTSUP	"$FRERR,NotSupported,"
#define DGG_FRERR_NOTEXEC	"$FRERR,CannotExecute,"
#define DGG_GPRS_SETTINGS	"_GprsSettings"
#define DGG_START_TRACKING	"_StartTracking"

#define GG_FRCMD_S			(sizeof(DGG_FRCMD) - 1)
#define GG_FRERR_S			(sizeof(DGG_FRERR) - 1)
#define GG_FRRET_S			(sizeof(DGG_FRRET) - 1)
#define GG_FRSES_S			(sizeof(DGG_FRSES) - 1)
#define GG_FRERR_AUTH_S		(sizeof(DGG_FRERR_AUTH) - 1)
#define GG_FRERR_NOTSUP_S	(sizeof(DGG_FRERR_NOTSUP) - 1)
#define GG_FRERR_NOTEXEC_S	(sizeof(DGG_FRERR_NOTEXEC) - 1)

typedef struct {
	uint32_t	DateTime;
	uint32_t	DateTimeInt;
	int			Index;
	int			Count;
} GgCommandCounts;

struct GgCommand_s {
	const int8_t		* Command;
	GgCommandCounts * Counts;
	uint8_t	(*Handler)(int8_t *, const struct GgCommand_s *, bool);
	uint8_t	(*Response)(int8_t * buffer, int buf_size, bool sms, const struct GgCommand_s * cmd);
};

typedef struct GgCommand_s GgCommand_t;

extern const int8_t GG_FRCMD_PING[];
extern const int8_t GG_FRCMD_INIT_MSG[];
extern const int8_t GG_FRCMD_UPD_FW[];
extern const int8_t GG_FRCMD_UPD_FDB[];
extern const int8_t GG_FRCMD_UPD_BLOCK[];

extern const int8_t GG_FRERR[];
extern const int8_t GG_FRRET[];
extern const int8_t GG_FRCMD[];
extern const int8_t GG_FRSES[];
extern const int8_t GG_FRERR_AUTH[];
extern const int8_t GG_FRERR_NOTSUP[];
extern const int8_t GG_FRERR_NOTEXEC[];

extern const GgCommand_t GgCommandMap[];
extern const int8_t GG_GPRS_SETTINGS[];
extern const int8_t GG_START_TRACKING[];

extern int8_t GG_FwFileName[32];
extern int  GG_FwSmsIndex;

const GgCommand_t * ProcessGpsGateCommand(int8_t * cmd, bool);
uint8_t GG_CheckCommand(int8_t *);
void GG_ClearAllCounts(void);
void initGprsParameter(void);
uint8_t handleSmsCommand(int8_t* cmd,bool smsscan,bool setup);
void getSmsNumber(int8_t* cmd);
void gpsOneDebugEnable(bool enable);
bool isSosNumber(int8_t* cmd);
void gsenssorDebugEnable(bool enable);
void report_alarm_message(int8_t* alarm);
int8_t getresetIndex(void);
uint8_t* getOta(void);
void ota_update_sms_notify(void);
const GgCommand_t * setupBysms_toSync(void);
void setAGPS_Ota(void);
#endif
