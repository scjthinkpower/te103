#ifndef _TIMER_H
#define _TIMER_H

#include <string.h>
#include <stdint.h>

extern volatile uint16_t  time_delay;

uint8_t timer_init(void);
void Delay_Nus(uint16_t nCount);
void Delay_Nms(uint16_t nCount);


#endif

