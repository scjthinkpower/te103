//*** <<< Use Configuration Wizard in Context Menu >>> ***
#ifndef __HARDWARE_H__
#define __HARDWARE_H__

#include "main.h"
#include "stm32xxx.h"
#include "ff_gen_drv.h"
#include "flash_diskio.h"

void Error_Handler(void);

#define ERROR_LOWEST_POWER			0x02

#define ERROR_GPS_TASK_NOT_ADD		0x12
#define ERROR_GPS_NO_DATA			0x13
#define ERROR_GPS_TASK_FAIL			0x14
#define ERROR_GPS_INIT_FAIL			0x15

#define ERROR_GSM_TCP_DISABLE		0x21
#define ERROR_GSM_TCP_NOT_OPEN		0x22
#define ERROR_GSM_SIM_NOT_READY		0x23
#define ERROR_GSM_NOT_REGISTER		0x24
#define ERROR_GSM_OTHER				0x25
#define ERROR_GSM_NOT_READY			0x26
#define ERROR_GSM_TASK_FAIL			0x27
#define ERROR_GSM_NO_SIM			0x28
#define ERROR_GSM_REG_DENIED		0x29

#define ERROR_CAN_TRANSMITE			0x31
#define ERROR_CAN_TASK_FAIL			0x32

#define ERROR_UNKNOWN_EXCEPTION		0x51
#define ERROR_UNKNOWN_INTERRUPT		0x52
#define ERROR_NO_TASK_MEMORY		0x53
#define ERROR_TASK_FAILED			0x54
#define ERROR_FLASH_ERASE			0x55
#define ERROR_MCU_HARD				0x56
#define ERROR_STACK_LOW				0x57

//	<h>		Communication
//	<s0>	APN Name
//			<i> GPRS APN name
//	<s1>	Username
//			<i> GPRS user name
//	<s2>	Password
//			<i> GPRS password
//	<s3>	Host Address
//			<i> IP address of host
//	<s4>	Port
//			<i> Host port
//	<o>		Protocol
//			<0x00=> TCP
//			<0x01=> HTTP
//			<i> Communication protocol
//	<s5>	FW Update Address
//			<i> IP address of FW update host
//	<s6>	Port
//			<i> FW update host port
//	</h>
#define DEF_GRPS_APN_NAME	"apn name"
#define DEF_GPRS_USERNAME	"username"
#define DEF_GPRS_PASSWORD	"password"
#define DEF_HOST_ADDR		"server_name_or_ip"
#define DEF_HOST_PORT		"server_port"
#define DEF_PROTOCOL		0x00
#define DEF_FOTA_ADDR		"update_server_name_ip"
#define DEF_FOTA_PORT		"update_server_port"

// Service center phones (use ones for init sms)
#define SMS_CENTER_1		"\"+13770820323\""
#define SMS_CENTER_2		"\"+13770820323\""

//<e> Debug via Serial port
//</e>
#define USE_SERIAL_DEBUG	1
//<e> Debug via USB port
//</e>
#define USE_USB_DEBUG		0

#define USE_SERIAL_DEBUG_ENABLE	1

#if ( USE_SERIAL_DEBUG || USE_USB_DEBUG )
	#define USE_DEBUG		1
#else
	#define USE_DEBUG		0
#endif

//<e> GPS Enable
//<e1>	Use Kalman Filter
//</e1>
//<e2>	Use Average Filter
//</e2>
//</e>
#define USE_NMEA			1
#define USE_KALMAN			1
#define USE_AVERAGE			1

//<e> CAN Support Enable
//</e>
#define USE_CAN_SUPPORT		0


enum {
	E_OK				= 0,
	E_ERROR				= 1,
	E_TIMEOUT			= 2,
	E_SEND_ERROR		= 3,
	E_BAD_RESPONSE		= 9,
	E_GSM_NOT_READY		= 10,
	E_SIM_NOT_READY		= 11,
	E_NO_SIM			= 12,
	E_REG_DENIED		= 13,
	E_NOT_REGISTER		= 14,
	E_GSM_TCP_DISABLE	= 15,
	E_GSM_TCP_NOT_OPEN	= 16,
	E_BAD_COMMAND		= 17,
	E_NO_RESPONSE		= 18,
	E_FW_ERROR			= 20,
	E_FW_OK			= 21,

	E_TASK_DONE			= 252,
	E_TASK_ERROR		= 253,
	E_TASK_QUEUED		= 254,
	E_TASK_SENDED	 	= 255
};

enum {
	E_CMD_END		= 0x00,	// End of list
	E_CMD_SMS		= 0x01,	// Only from SMS
	E_CMD_HOST		= 0x02,	// Only from host
	E_CMD_BOTH		= 0x03,	// From SMS and host
	E_CMD_SINGLE	= 0x80	// Save last
};

enum {
	TRANSFER_WAIT,
	TRANSFER_COMPLETE,
	TRANSFER_ERROR
};

extern UART_HandleTypeDef gsmUartHandle;
extern UART_HandleTypeDef gpsUartHandle;
extern UART_HandleTypeDef debugUartHandle;
extern UART_HandleTypeDef lpUartHandle;
static GPIO_InitTypeDef  GPIO_InitStruct;


 /* Exported types ------------------------------------------------------------*/
	/* Exported constants --------------------------------------------------------*/
	/* User can use this section to tailor USARTx/UARTx instance used and associated 
   resources */
	/* Definition for USARTx clock resources */

	#define DEBUG_DEINIT()		\
		do {					\
		MX_USART2_UART_Deinit(); \
		} while(0)

	#define DEBUG_USART                      USART2
	#define DEBUG_USART_CLK_ENABLE()         __HAL_RCC_USART2_CLK_ENABLE()
	#define DEBUG_RX_GPIO_CLK_ENABLE()      __HAL_RCC_GPIOA_CLK_ENABLE()
	#define DEBUG_TX_GPIO_CLK_ENABLE()      __HAL_RCC_GPIOA_CLK_ENABLE()

	#define DEBUG_USART_FORCE_RESET()         __HAL_RCC_USART2_FORCE_RESET()
	#define DEBUG_USART_RELEASE_RESET()       __HAL_RCC_USART2_RELEASE_RESET()

	/* Definition for USARTx Pins */
	#define DEBUG_USART_TX_PIN                    GPIO_PIN_2
	#define DEBUG_USART_TX_GPIO_PORT              GPIOA
	#define DEBUG_USART_TX_AF                     GPIO_AF4_USART2
	#define DEBUG_USART_RX_PIN                    GPIO_PIN_3
	#define DEBUG_USART_RX_GPIO_PORT              GPIOA
	#define DEBUG_USART_RX_AF                     GPIO_AF4_USART2

	/* Definition for USARTx's NVIC */
	#define DEBUG_USART_IRQn                 USART2_IRQn
	#define DEBUG_USART_IRQHandler           USART2_IRQHandler


/* Size of Trasmission buffer */
#define TXBUFFERSIZE                      (COUNTOF(aTxBuffer) - 1)
/* Size of Reception buffer */
#define RXBUFFERSIZE                      1//TXBUFFERSIZE
  
/* Exported macro ------------------------------------------------------------*/
#define COUNTOF(__BUFFER__)   (sizeof(__BUFFER__) / sizeof(*(__BUFFER__)))
/* Exported functions ------------------------------------------------------- */

/*
 *	GPS
 */
	#define GPS_PWR_ON()      gpsPowerOn() 
	#define GPS_PWR_OFF()			gpsPowerOff()

	#define GPS_DEINIT()		\
		do {					\
		MX_USART5_UART_Deinit(); \
		} while(0)
		
	#define GPS_USART                      USART5
	#define GPS_USART_CLK_ENABLE()         __HAL_RCC_USART5_CLK_ENABLE()
	#define GPS_RX_GPIO_CLK_ENABLE()       __HAL_RCC_GPIOB_CLK_ENABLE()
	#define GPS_TX_GPIO_CLK_ENABLE()       __HAL_RCC_GPIOB_CLK_ENABLE()

	#define GPS_USART_FORCE_RESET()         __HAL_RCC_USART5_FORCE_RESET()
	#define GPS_USART_RELEASE_RESET()       __HAL_RCC_USART5_RELEASE_RESET()

	/* Definition for USARTx Pins */
	#define GPS_USART_TX_PIN                    GPIO_PIN_3
	#define GPS_USART_TX_GPIO_PORT              GPIOB
	#define GPS_USART_TX_AF                     GPIO_AF6_USART5
	#define GPS_USART_RX_PIN                    GPIO_PIN_4
	#define GPS_USART_RX_GPIO_PORT              GPIOB
	#define GPS_USART_RX_AF                     GPIO_AF6_USART5

	/* Definition for USARTx's NVIC */
	#define GPS_USART_IRQn                 USART4_5_IRQn
		
/*
 *	GSM
 */
	#define GSM_PWR_ON()			gsmPowerOn()           
	#define GSM_PWR_OFF()			gsmPowerOff()
	
	#define GSM_DEINIT()		\
		do {					\
		MX_USART4_UART_Deinit(); \
		} while(0)
	
	#define GSM_DTR_LOW()           gsmDTRLow()
	#define GSM_RTS_LOW()          	gsmRtsLow()

	#define GSM_USART                      USART4
	#define GSM_USART_CLK_ENABLE()         __HAL_RCC_USART4_CLK_ENABLE()
	#define GSM_RX_GPIO_CLK_ENABLE()       __HAL_RCC_GPIOA_CLK_ENABLE()
	#define GSM_TX_GPIO_CLK_ENABLE()       __HAL_RCC_GPIOA_CLK_ENABLE()

	#define GSM_USART_FORCE_RESET()         __HAL_RCC_USART4_FORCE_RESET()
	#define GSM_USART_RELEASE_RESET()       __HAL_RCC_USART4_RELEASE_RESET()

	/* Definition for USARTx Pins */
	#define GSM_USART_TX_PIN                    GPIO_PIN_0
	#define GSM_USART_TX_GPIO_PORT              GPIOA
	#define GSM_USART_TX_AF                     GPIO_AF6_USART4
	#define GSM_USART_RX_PIN                    GPIO_PIN_1
	#define GSM_USART_RX_GPIO_PORT              GPIOA
	#define GSM_USART_RX_AF                     GPIO_AF6_USART4

	/* Definition for USARTx's NVIC */
	#define GSM_USART_IRQn                 USART4_5_IRQn
	#define GPS_GSM_USART_IRQHandler           USART4_5_IRQHandler
	
	
	
	#define LPUART_DEINIT()		\
		do {					\
		MX_LPUART1_UART_Deinit(); \
		} while(0)
		
	#define LPUART_USART                      LPUART1
	#define LP_USART_CLK_ENABLE()         		__HAL_RCC_LPUART1_CLK_ENABLE()
	#define LP_RX_GPIO_CLK_ENABLE()       		__HAL_RCC_GPIOB_CLK_ENABLE()
	#define LP_TX_GPIO_CLK_ENABLE()       		__HAL_RCC_GPIOB_CLK_ENABLE()

	#define LP_USART_FORCE_RESET()         		__HAL_RCC_LPUART1_FORCE_RESET()
	#define LP_USART_RELEASE_RESET()       		__HAL_RCC_LPUART1_RELEASE_RESET()

	/* Definition for USARTx Pins */
	#define LP_USART_TX_PIN                    GPIO_PIN_10
	#define LP_USART_TX_GPIO_PORT              GPIOB
	#define LP_USART_TX_AF                     GPIO_AF4_LPUART1
	#define LP_USART_RX_PIN                    GPIO_PIN_11
	#define LP_USART_RX_GPIO_PORT              GPIOB
	#define LP_USART_RX_AF                     GPIO_AF4_LPUART1

	/* Definition for USARTx's NVIC */
	#define LP_USART_IRQn                 LPUART1_IRQn
	#define LP_USART_IRQHandler           LPUART1_IRQHandler
		
	
	//Gsensor
	/* Exported types ------------------------------------------------------------*/
	/* Exported constants --------------------------------------------------------*/
	/* User can use this section to tailor I2Cx/I2Cx instance used and associated
   resources */
	/* Definition for I2Cx clock resources */
	#define I2Cx                            I2C1
	#define RCC_PERIPHCLK_I2Cx              RCC_PERIPHCLK_I2C1
	#define RCC_I2CxCLKSOURCE_SYSCLK        RCC_I2C1CLKSOURCE_SYSCLK
	#define I2Cx_CLK_ENABLE()               __HAL_RCC_I2C1_CLK_ENABLE()
	#define I2Cx_SDA_GPIO_CLK_ENABLE()      __HAL_RCC_GPIOA_CLK_ENABLE()
	#define I2Cx_SCL_GPIO_CLK_ENABLE()      __HAL_RCC_GPIOA_CLK_ENABLE() 

	#define I2Cx_FORCE_RESET()              __HAL_RCC_I2C1_FORCE_RESET()
	#define I2Cx_RELEASE_RESET()            __HAL_RCC_I2C1_RELEASE_RESET()

	/* Definition for I2Cx Pins */
	#define I2Cx_SCL_PIN                    GPIO_PIN_9
	#define I2Cx_SCL_GPIO_PORT              GPIOA
	#define I2Cx_SDA_PIN                    GPIO_PIN_10
	#define I2Cx_SDA_GPIO_PORT              GPIOA
	#define I2Cx_SCL_SDA_AF                 GPIO_AF6_I2C1

	/* Definition for I2Cx's NVIC */
	#define I2Cx_IRQn                    I2C1_IRQn
	#define I2Cx_IRQHandler              I2C1_IRQHandler
	
	
	
	/* Definition for SPIx clock resources */
	#define SPIx                             SPI2
	#define SPIx_CLK_ENABLE()                __HAL_RCC_SPI2_CLK_ENABLE()
	#define SPIx_SCK_GPIO_CLK_ENABLE()       __HAL_RCC_GPIOB_CLK_ENABLE()
	#define SPIx_MISO_GPIO_CLK_ENABLE()      __HAL_RCC_GPIOB_CLK_ENABLE()
	#define SPIx_MOSI_GPIO_CLK_ENABLE()      __HAL_RCC_GPIOB_CLK_ENABLE()

	#define SPIx_FORCE_RESET()               __HAL_RCC_SPI2_FORCE_RESET()
	#define SPIx_RELEASE_RESET()             __HAL_RCC_SPI2_RELEASE_RESET()

	/* Definition for SPIx Pins */
	#define SPIx_SCK_PIN                     GPIO_PIN_13
	#define SPIx_SCK_GPIO_PORT               GPIOB
	#define SPIx_SCK_AF                      GPIO_AF0_SPI2
	#define SPIx_MISO_PIN                    GPIO_PIN_14
	#define SPIx_MISO_GPIO_PORT              GPIOB
	#define SPIx_MISO_AF                     GPIO_AF0_SPI2
	#define SPIx_MOSI_PIN                    GPIO_PIN_15
	#define SPIx_MOSI_GPIO_PORT              GPIOB
	#define SPIx_MOSI_AF                     GPIO_AF0_SPI2
	
	/* Definition for SPIx's NVIC */
	#define SPIx_IRQn                        SPI2_IRQn
	#define SPIx_IRQHandler                  SPI2_IRQHandler


/* Definition for TIMx clock resources */
#define TIM2_CLK_ENABLE()              __HAL_RCC_TIM2_CLK_ENABLE()
/* Definition for TIMx clock resources */
#define TIM3_CLK_ENABLE()              __HAL_RCC_TIM3_CLK_ENABLE()

/* Definition for TIMx Channel Pins */
#define TIMx_CHANNEL_GPIO_PORT()       __HAL_RCC_GPIOA_CLK_ENABLE();__HAL_RCC_GPIOC_CLK_ENABLE()
#define TIMx_GPIO_PORT_CHANNEL1        GPIOA
#define TIMx_GPIO_PORT_CHANNEL3        GPIOC
#define TIMx_GPIO_PIN_CHANNEL1         GPIO_PIN_5
#define TIMx_GPIO_PIN_CHANNEL3         GPIO_PIN_8
#define TIMx_GPIO_AF_CHANNEL1          GPIO_AF5_TIM2
#define TIMx_GPIO_AF_CHANNEL3          GPIO_AF2_TIM3

#define TIMx_FORCE_RESET()              __HAL_RCC_TIM2_FORCE_RESET();__HAL_RCC_TIM3_FORCE_RESET()
#define TIMx_RELEASE_RESET()            __HAL_RCC_TIM2_RELEASE_RESET();__HAL_RCC_TIM3_RELEASE_RESET()


/* Private typedef -----------------------------------------------------------*/
#define  PERIOD_VALUE       (uint32_t)(65535/100)  /* Period Value  */
#define  PULSE1_VALUE       (uint32_t)(PERIOD_VALUE*2/3)        /* Capture Compare 1 Value  */
#define  PULSE2_VALUE       (uint32_t)(PERIOD_VALUE*37.5/100) /* Capture Compare 2 Value  */
#define  PULSE3_VALUE       (uint32_t)(PERIOD_VALUE/4)        /* Capture Compare 3 Value  */
#define  PULSE4_VALUE       (uint32_t)(PERIOD_VALUE*12.5/100) /* Capture Compare 4 Value  */



/*

other
	*/	 
	#define SENS_D1_READ()                       1

	#define GSENSOR_PIN                    	 		 GPIO_PIN_13
	#define GSENSOR_EXTI_IRQn                    EXTI4_15_IRQn
	
	#define ACC_PIN                    	 		 		 GPIO_PIN_4
	#define ACC_EXTI_IRQn                    		 EXTI4_15_IRQn
	
	#define SIMCARD_PIN                    	 		 GPIO_PIN_0
	#define SIMCARD_EXTI_IRQn                    EXTI0_1_IRQn
	
	
	#define RI_PIN                    	 		 		 GPIO_PIN_1
	#define RI_EXTI_IRQn                    		 EXTI0_1_IRQn
	
	#define SOS_PIN                    	 		 		 GPIO_PIN_7
	#define SOS_EXTI_IRQn                    		 EXTI4_15_IRQn
	
	
	#define RTC_ASYNCH_PREDIV    0x7F
	//#define RTC_SYNCH_PREDIV     0x0130
	#define RTC_SYNCH_PREDIV   0x00FF

void initSpi(void);
void gsensor_INT(void);
void accon_INT(void);
void simcard_INT(void);
void RI_INT(void);
void initIWDG(void);
void startIWDG(void);
void refreshIWDG(void);
void stopIWDG(void);

void initWWDG(void);
void deInitWWDG(void);
void startWWDG(void);
void refreshWWDG(void);
void initTimPwm(void);
void start_pwm_out1(void);
void start_pwm_out2(void);
void stop_pwm_out1(void);
void stop_pwm_out2(void);
bool getOut1Statue(void);
bool getOut2Statue(void);
void stop_pwm_out1_tosleep(void);
void stop_pwm_out2_tosleep(void);
void disableIRQ(void);
void MX_USART2_UART_Init(void);
void MX_USART4_UART_Init(void);
void MX_USART5_UART_Init(uint32_t speed);
void MX_LPUART1_UART_Deinit(void);
void  MX_USART2_UART_Deinit(void);
void  MX_USART4_UART_Deinit(void);
void  MX_USART5_UART_Deinit(void);
void USART_SEND_DATAS(char *data, USART_TypeDef *USARTx);
void USART_SEND_HEXDATAS(char *data, USART_TypeDef *USARTx,int len);
void USART_SEND_DATA(char data, USART_TypeDef *USARTx);
bool checkPWMOut1Setup(void);
bool checkPWMOut2Setup(void);
#endif
