#ifndef _TEMPERATURE_H
#define _TEMPERATURE_H

#include <stdint.h>
#include "main.h"

typedef enum{
FAILED = 0 ,
PASSED = !FAILED
} TestStatus ;



#define DQ_GPIO  GPIOC
//#define DQ_GPIO_Pin_One 	GPIO_Pin_12
#define DQ_GPIO_Pin_One 	GPIO_PIN_5
//#define DQ_GPIO_Pin_Two 	GPIO_Pin_11

//REGISTER COMMANDS
#define REGISTER_9_BITS  0x1F
#define REGISTER_10_BITS 0x3F
#define REGISTER_11_BITS 0x5F
#define REGISTER_12_BIT2 0x7F

//ROM COMMANDS
#define ROM_Search_Cmd   0xF0
#define ROM_Read_Cmd     0x33
#define ROM_Match_Cmd    0x55
#define ROM_Skip_Cmd     0xCC
#define ROM_AlarmSearch_Cmd 0xEC

//DS18b20 FUNCTION COMMANDS
#define Convert_T          0x44
#define Write_Scratchpad  0x4E
#define Read_Scratchpad   0xBE
#define Copy_Scratchpad   0x48
#define Recall_EEPROM     0x88
#define Read_PowerSupply  0x84



//#define DQ_Write_1()     GPIO_SetBits(DQ_GPIO ,DQ_GPIO_Pin_One)  //写1
//#define DQ_Write_0()     GPIO_ResetBits(DQ_GPIO ,DQ_GPIO_Pin_One)//写0
#define DQ_Write_1()     HAL_GPIO_WritePin(DQ_GPIO, DQ_GPIO_Pin_One, GPIO_PIN_SET); //写1
#define DQ_Write_0()     HAL_GPIO_WritePin(DQ_GPIO, DQ_GPIO_Pin_One, GPIO_PIN_RESET);//写0
//#define DQ_ReadBit_One()     GPIO_ReadInputDataBit(DQ_GPIO ,DQ_GPIO_Pin_One) //读DQ上的值
#define DQ_ReadBit_One()     HAL_GPIO_ReadPin(DQ_GPIO ,DQ_GPIO_Pin_One) //读DQ上的值
//#define DQ_ReadBit_Two()     GPIO_ReadInputDataBit(DQ_GPIO ,DQ_GPIO_Pin_Two) //读DQ上的值



typedef struct temperature {
	unsigned char sign[2];
	unsigned char interger[2];
	unsigned int  decimal[2];
	
} TEMPERATURE;

extern	char CurTemVlaue_one[12];
//extern	char CurTemVlaue_two[12];

typedef struct{
	unsigned char TemBufValues_one;
	//unsigned char TemBufValues_two;
}TemBufValues;



extern TEMPERATURE  MyTemper;
extern  float  TemperVlaue_one;
//extern  float  TemperVlaue_two;
extern  uint16_t DS18B20_Flag_One;
//extern  uint16_t DS18B20_Flag_Two;


void DS18B20_task(void);
uint8_t DHT11_task(void);
void ds18b20DebugEnable(bool enable);

#endif 

