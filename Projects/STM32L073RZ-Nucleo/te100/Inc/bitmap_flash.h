
#ifndef _BITMAP_FLASH_H
#define _BITMAP_FLASH_H

#include "common.h"

#define BIT_FLAG_ADDR_START0            	SPIFLASH_BIT_MAP_ADDR_START0	//0x00002000
#define BIT_FLAG_ADDR_END0              	SPIFLASH_BIT_MAP_ADDR_END0		//0x00002FFF

#define BIT_FLAG_ADDR_START1            	SPIFLASH_BIT_MAP_ADDR_START1	//0x00003000
#define BIT_FLAG_ADDR_END1              	SPIFLASH_BIT_MAP_ADDR_END1		//0x00003FFF


#define BIT_FLAG_ADDR_START             	BIT_FLAG_ADDR_START0			//0x00002000
#define BIT_FLAG_ADDR_END               		BIT_FLAG_ADDR_END1				//0x00003FFF

#define BIT_AGPS_ADDR_START             	SPIFLASH_BIT_MAP_ADDR_START2			//0x00004000
#define BIT_AGPS_ADDR_END               	 SPIFLASH_BIT_MAP_ADDR_END2				//0x00005FFF

#define DATA_ADDR_START                 		SPIFLASH_LOG_ADDR_START		//0x00006000
#define DATA_ADDR_END                   		SPIFLASH_LOG_ADDR_END			//0x003FFFFF


#define DATA_ADDR_START                 		SPIFLASH_LOG_ADDR_START		//0x00006000
#define DATA_ADDR_END                   		SPIFLASH_LOG_ADDR_END			//0x003FFFFF


#define DATA_LOGGER_PKT_LEN             	0x100 //128	//Fixme : now sizeof(GsmTask_t)==80, if change need changge this.


#define PKT_CNT_DATA_SAPCE             (DATA_ADDR_END - DATA_ADDR_START) / DATA_LOGGER_PKT_LEN
#define PKT_CNT_BIT_MAP                 	32768       //  (one sector==>4096 * 4=16384, now used 2 sector, so: 2*16384 = 32768)


#if PKT_CNT_DATA_SAPCE <= PKT_CNT_BIT_MAP
#define PKT_CNT_MAX                     PKT_CNT_DATA_SAPCE
#elif PKT_CNT_DATA_SAPCE > PKT_CNT_BIT_MAP
#define PKT_CNT_MAX                     PKT_CNT_BIT_MAP
#endif

#define BIT_FLAG_VALID           	0x03		
#define BIT_FLAG_UNREAD        	0x02	
#define BIT_FLAG_INVALID		0x00       	


#define	SPI_FLASH_ID	0xef4016

#define   SPI_FLASH_ID_1 0xc22016 


#define	BITMAP_FLASH_DATA_LEN	0x100   // 256 as one data packet


typedef struct {
    uint8_t  Flag;
    uint8_t  FlagNew;
    uint8_t  Offset;                        // 0,1,2,3; every tow bits for one packet
    uint32_t AddrFlag;                   // addr to sotred flag.
    uint32_t AddrFlagStart;           // addr Flag start to search
} bit_mask_t;

extern uint16_t PktUnreadCnt;

void bit_map_init      			 (void);
uint8_t bit_map_unread     	(void);
uint8_t bit_map_read_data  	(char *pbuf, uint16_t bytes);
uint8_t bit_map_write_data 	(const char *pbuf, uint16_t bytes);
void bit_map_write_data_index(const char *pbuf, uint16_t bytes,uint16_t index);
void bit_map_read_data_index(char *pbuf, uint16_t bytes,uint16_t index);
void bit_map_write_data_(const char *pbuf, uint16_t bytes);
void bit_map_read_data_(char *pbuf, uint16_t bytes);
void bit_map_write_agps_data_index(const char *pbuf, uint16_t bytes,uint16_t index);
void bit_map_read_agps_data_index(char *pbuf, uint16_t bytes,uint16_t index);
void bit_map_write_agps_flag(const char *pbuf, uint16_t bytes);
void bit_map_read_agps_flag(char *pbuf, uint16_t bytes);
extern int SpiFlashInit(void) ;
void bitmapEnable(bool enable);
extern bool SPI_FLASH_OK ;
extern void bit_map_erase_flag_all_sector(void);
extern void bit_map_erase_flag_start(void);
extern void bit_map_erase_agps_start(void);
#endif

