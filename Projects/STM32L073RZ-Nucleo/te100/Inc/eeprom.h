#ifndef __EEPROM_H__
#define __EEPROM_H__

#include <stdbool.h>
#include "hardware.h"

#define DATAEEPROM_ERASE_ERROR -1
#define DATAEEPROM_PROGRAM_ERROR -2
#define DATAEEPROM_PROGRAM_VERIFICATION_ERROR -3

#define APPLICATION_ADDRESS     (uint32_t)0x08000000
#define BOOT_ADDRESS     (uint32_t)0x08000000
#define BOOT_FLASH_END_ADDRESS        ((uint32_t)0x08008000)

/* Error code */
enum 
{
  FLASHIF_OK = 0,
  FLASHIF_ERASEKO,
  FLASHIF_WRITINGCTRL_ERROR,
  FLASHIF_WRITING_ERROR,
  FLASHIF_PROTECTION_ERRROR
};

bool flash_eeprom(uint8_t * buff,uint32_t length);
uint32_t read_eeprom(uint8_t * buff,uint32_t length);
uint32_t FLASH_If_Write(uint32_t destination, uint32_t *p_source, uint32_t length);
uint32_t FLASH_If_Erase(uint32_t start);
bool EEPROM_Erase(void);
#endif
