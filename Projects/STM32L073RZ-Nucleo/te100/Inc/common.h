#ifndef __COMMON_H__
#define __COMMON_H__

#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#include "hardware.h"


typedef void (*timer_callback)(void) ; 
typedef struct {
		bool is_running ; 
    int unit    ;   // every timer count 
    int value ;   // the define timer counts  
    int elapse ; 
    timer_callback cb ; 
} common_timer_t ; 

typedef enum {
    ONE_SHORT_TIMER = 0 , 
    CONTINUOUS_TIMER = 1 ,
} common_timer_type_e ; 

void init_timer(common_timer_t *t, int units, timer_callback cb);
void start_timer(common_timer_t *t, int counter);
void stop_timer(common_timer_t *t);
void handle_timer(common_timer_t *t, common_timer_type_e type);



#endif


