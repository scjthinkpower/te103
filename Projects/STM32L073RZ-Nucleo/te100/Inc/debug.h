#ifndef __DEBUG_H__
#define __DEBUG_H__

#include "stdbool.h"
#include "hardware.h"

static bool DebugEnable = true;

#if ( USE_SERIAL_DEBUG_ENABLE )

#define DEBUG(fmt, arg...) \
    do {    \
        printf("DEBUG I: " fmt "\r\n" , ## arg) ; \
	} while (0) ;  \
    	
#else
#define DEBUG(fmt, arg...) \
    do {    \
         \
	} while (0) ;
	
#endif

struct shell_command {
	char *name;
	void (*function )(int argc, char **argv);
};
	

void DebugInit(void);
void vDebugTask(void const *pvArg);
void shellData(uint8_t data);
extern void shell_help(int argc, char **argv);
extern void shell_testgprs(int argc, char **argv);
extern void shell_teststandby(int argc, char **argv);
void shell_relay_1(int argc, char **argv);
void shell_relay_2(int argc, char **argv);
bool NewGpioTest(void);
#endif	// __DEBUG_H__
