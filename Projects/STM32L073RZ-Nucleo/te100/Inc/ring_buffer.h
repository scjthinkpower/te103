#ifndef __RINGBUFFER_H__
#define __RINGBUFFER_H__

#include <stdint.h>

typedef struct RingBuffer_s {
	int8_t * Head;
	int8_t * Tail;
	int8_t * Start;
	int8_t * End;
} RingBuffer_t;

uint8_t rb_Get(RingBuffer_t *rb, int8_t * c);
uint8_t rb_Put(RingBuffer_t *rb, int8_t c);
void    rb_Init(RingBuffer_t *rb, int8_t * buffer, int size);

#endif
