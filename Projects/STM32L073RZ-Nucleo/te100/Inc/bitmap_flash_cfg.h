#ifndef _BITMAP_FLASH_CFG_H
#define _BITMAP_FLASH_CFG_H

// device constants
#define SPIFLASH_SIZE                      		 0x00400000    	// 4M bytes  
#define SPIFLASH_PAGESIZE                   	256         		// 256 bytes/page
#define SPIFLASH_SECTORSIZE                 	4096			//4K byte
#define SPIFLASH_ADDR_END                   	0x003FFFFF		//


// space distribution
#define SPIFLASH_SYS_SETTINGS_ADDR_START    	0x00000000
#define SPIFLASH_SYS_SETTINGS_ADDR_END      	0x00000FFF  	// sector 0	(Ԥ��)

#define SPIFLASH_RESERVE_START              		0x00001000	// sector 1	(Ԥ��)
#define SPIFLASH_RESERVE_END                			0x00001FFF  

#define SPIFLASH_BIT_MAP_ADDR_START0        	0x00002000  	// sector 2
#define SPIFLASH_BIT_MAP_ADDR_END0          		0x00002FFF  

#define SPIFLASH_BIT_MAP_ADDR_START1        	0x00003000  	// sector 3
#define SPIFLASH_BIT_MAP_ADDR_END1          		0x00003FFF

#define SPIFLASH_BIT_MAP_ADDR_START2        	0x00004000  	// sector 3
#define SPIFLASH_BIT_MAP_ADDR_END2          		0x00004FFF

#define SPIFLASH_BIT_MAP_ADDR_START3        	0x00005000  	// sector 4
#define SPIFLASH_BIT_MAP_ADDR_END3          		0x00005FFF


#define SPIFLASH_LOG_ADDR_START             		0x00006000	// sector 4 to flash end
#define SPIFLASH_LOG_ADDR_END               		SPIFLASH_ADDR_END               

#endif

