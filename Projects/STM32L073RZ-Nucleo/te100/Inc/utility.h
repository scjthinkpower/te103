#ifndef __UTILITY_H__
#define __UTILITY_H__

int8_t* NextToken(int8_t *src);
int8_t* FindTokenStartWith(int8_t *src, const int8_t *pattern);
int8_t* FindToken(int8_t *src, const int8_t *pattern);
int8_t* TokenBefore(int8_t *src, const int8_t *pattern);
int8_t* int8_tReplace(int8_t *src, int8_t from, int8_t to);

int TokenSizeComma(int8_t* src);
int TokenSizeQuote(int8_t* src);
int8_t* TokenNextComma(int8_t *src);

// int Base64Decode(u8* dst, int dst_size, int8_t* src);
uint32_t GetCRC32(uint32_t * address, int u32_count, uint32_t * exclude);

int8_t * itoa(int8_t * result, int32_t value, uint8_t width);
int8_t * itoa16(int8_t * result, int32_t value, uint8_t width);
int8_t * strcpyEx(int8_t *dst, const int8_t *src);
int NmeaAddChecksum(int8_t *dst, int8_t *src);
unsigned short crc_16_rec (unsigned char *pucData, unsigned short ucLen);
void StrToHex(BYTE *pbDest, BYTE *pbSrc, int nLen);
#endif
