/**
  ******************************************************************************
  * @file    Templates/Inc/main.h 
  * @author  MCD Application Team
  * @version V1.5.0
  * @date    8-January-2016
  * @brief   Header for main.c module
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; COPYRIGHT(c) 2016 STMicroelectronics</center></h2>
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of STMicroelectronics nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */
  
/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H

/* Includes ------------------------------------------------------------------*/
#include <stdint.h>
#include <stdbool.h>
#include "stm32l0xx_hal.h"
#include "cmsis_os.h"
#include "stm32l0xx_nucleo.h"


#include "stm32l0xx_ll_lpuart.h"
#include "stm32l0xx_ll_rcc.h"

#include "stm32l0xx_ll_bus.h"
#include "stm32l0xx_ll_system.h"
#include "stm32l0xx_ll_cortex.h"
#include "stm32l0xx_ll_utils.h"
#include "stm32l0xx_ll_pwr.h"
#include "stm32l0xx_ll_rtc.h"
#include "stm32l0xx_ll_usart.h"
#include "stm32l0xx_ll_gpio.h"

/* Exported types ------------------------------------------------------------*/
/* Exported constants --------------------------------------------------------*/
/* Exported macro ------------------------------------------------------------*/
/* Exported functions ------------------------------------------------------- */

	#define I2Cx                            I2C1
	#define RCC_PERIPHCLK_I2Cx              RCC_PERIPHCLK_I2C1
	#define RCC_I2CxCLKSOURCE_SYSCLK        RCC_I2C1CLKSOURCE_SYSCLK
	#define I2Cx_CLK_ENABLE()               __HAL_RCC_I2C1_CLK_ENABLE()
	#define I2Cx_SDA_GPIO_CLK_ENABLE()      __HAL_RCC_GPIOA_CLK_ENABLE()
	#define I2Cx_SCL_GPIO_CLK_ENABLE()      __HAL_RCC_GPIOA_CLK_ENABLE() 

	#define I2Cx_FORCE_RESET()              __HAL_RCC_I2C1_FORCE_RESET()
	#define I2Cx_RELEASE_RESET()            __HAL_RCC_I2C1_RELEASE_RESET()

	/* Definition for I2Cx Pins */
	#define I2Cx_SCL_PIN                    GPIO_PIN_9
	#define I2Cx_SCL_GPIO_PORT              GPIOA
	#define I2Cx_SDA_PIN                    GPIO_PIN_10
	#define I2Cx_SDA_GPIO_PORT              GPIOA
	#define I2Cx_SCL_SDA_AF                 GPIO_AF6_I2C1

	/* Definition for I2Cx's NVIC */
	#define I2Cx_IRQn                    I2C1_IRQn
	#define I2Cx_IRQHandler              I2C1_IRQHandler


/* Definition for SPIx clock resources */
	#define SPIx                             SPI2
	#define SPIx_CLK_ENABLE()                __HAL_RCC_SPI2_CLK_ENABLE()
	#define SPIx_SCK_GPIO_CLK_ENABLE()       __HAL_RCC_GPIOB_CLK_ENABLE()
	#define SPIx_MISO_GPIO_CLK_ENABLE()      __HAL_RCC_GPIOB_CLK_ENABLE()
	#define SPIx_MOSI_GPIO_CLK_ENABLE()      __HAL_RCC_GPIOB_CLK_ENABLE()

	#define SPIx_FORCE_RESET()               __HAL_RCC_SPI2_FORCE_RESET()
	#define SPIx_RELEASE_RESET()             __HAL_RCC_SPI2_RELEASE_RESET()

	/* Definition for SPIx Pins */
	#define SPIx_SCK_PIN                     GPIO_PIN_13
	#define SPIx_SCK_GPIO_PORT               GPIOB
	#define SPIx_SCK_AF                      GPIO_AF0_SPI2
	#define SPIx_MISO_PIN                    GPIO_PIN_14
	#define SPIx_MISO_GPIO_PORT              GPIOB
	#define SPIx_MISO_AF                     GPIO_AF0_SPI2
	#define SPIx_MOSI_PIN                    GPIO_PIN_15
	#define SPIx_MOSI_GPIO_PORT              GPIOB
	#define SPIx_MOSI_AF                     GPIO_AF0_SPI2
	
	/* Definition for SPIx's NVIC */
	#define SPIx_IRQn                        SPI2_IRQn
	#define SPIx_IRQHandler                  SPI2_IRQHandler



typedef uint32_t  u32;
typedef uint16_t u16;
typedef uint8_t u8;

typedef enum
{
	GSENSOR_THREAD = 0,
  GPS_THREAD,
  GSM_THREAD,
  WORK_THREAD,
	DEBUG_THREAD,
} Thread_TypeDef;

void ShowError			(uint8_t errorCode);
void ShowFatalError		(uint8_t errorCode);
void ShowFatalErrorOnly	(uint8_t errorCode);
void SystemRestart(void);

extern int8_t SET_GRPS_APN_NAME[];
extern int8_t SET_GPRS_USERNAME[];
extern int8_t SET_GPRS_PASSWORD[];
extern int8_t SET_HOST_ADDR[];
extern int8_t SET_HOST_PORT[];
extern int8_t SET_HOST_DOMAIN[];

#define GSM_COMPLETE	0x001
#define GPS_COMPLETE	0x002
#define GPS_STOP		0x004
#define SMS_INIT_OK		0x008
#define SMS_INIT_SENT	0x010
#define VBAT_LOW		0x100
#define VBAT_STOPPED	0x040
#define VBAT_READY		0x080
#define GSM_LOGIN	0x020
#define GSM_SLEEP	0x040
#define GSM_WAKEUP	0x80

extern volatile uint16_t GlobalStatus;

int action(void);
void taskRun(void);
void mainOneDebugEnable(bool enable);
bool getMainActionDone(void);
#endif /* __MAIN_H */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
