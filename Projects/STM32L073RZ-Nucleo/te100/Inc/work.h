#ifndef __WORK_H__
#define __WORK_H__

#include <stdbool.h>
#include "hardware.h"
#include "FreeRTOS.h"
#include "nmea/nmea.h"
#include "nmea/tok.h"

#define GSENSOR_ACTIVITY_EVENT 0
#define TIMER_EVENT 1
#define GSENSOR_SLEEP_EVENT 2
#define DISABLE_RTC_EVENT 3
#define ACC_EVENT_OFF 4
#define ACC_EVENT_ON  5
#define GSENSOR_SHOCK_EVENT 6
#define IN_N_EVENT 7
#define IN_P_EVENT  8
#define EH_EVENT  9
#define ML_EVENT  10
#define ML_ADD_EVENT 11
#define SOS_EVENT 12
#define BATTLE_LOW_EVENT 13
#define CUT_DETECT_EVENT 14
#define OVERSPEED_EVENT 15
#define SHOCK_EVENT 16
#define GEOFENCE_EVENT 17
#define POINT_EVENT 18
#define GPS_EVENT 19
#define RTC_REPORT_EVENT 20



#define ALARM_SOS 0
#define ALARM_BATTLE_LOW 1
#define ALARM_CUT_DETECT 2
#define ALARM_OVERSPEED  3
#define ALARM_SHOCK 4
#define NO_ALARM 5
#define GPS_LOST 6
#define GPS_FIXED 7
#define ALARM_JAMMING 8

#define ACC_OFF 0
#define ACC_ON 1

#define IN_N 0
#define IN_P 1

struct WorkTask_s {
			int	event;
	    uint8_t result;
};

typedef struct WorkTask_s WorkTask_t;

uint8_t WorkAddTask(WorkTask_t * task);
void setEvent(int32_t et);
uint32_t getEvent(void);
void vWorkTask(void const * pvArg);
void wakeup(void);
void sleep(void);
void InitLed(void);
int8_t* signal_state(void);
int8_t* signal_state_ussd(void);
int8_t* alarm_state(void);
int8_t* alarm_state_ussd(void);
bool isAccOn(void);
void setAlarmState(int8_t type,bool state);
void setInState(int8_t type,bool state);
void setAccState(int8_t type,bool state);
bool getAccState(void);
bool getAccIonState(void);
bool getAccIoffState(void);
uint8_t RTC_TIME(nmeaTIME time,bool check);
void workDebugEnable(bool enable);
void LpUartInit(void);
int8_t* getADCVolt(void);
int8_t* getADCVolt_ussd(void);
int8_t* getADCVolt_v(void);
int8_t* getBattLowVolt(void);
int8_t* getBattLowVoltStr(void);
bool getBattLowStat(void);
void RTC_Config(void);
void ADC_Config(void);
int8_t getCutStatus(void);
int8_t setSirenStatus(int8_t status);
void setSirenWork(bool work);
bool RTC_INIT_SUCCESS(void);
void nosimcard_sleep(void);
void lpUartReceiveHandle(void);
bool getWakeup_Report(void);
void setWakeup_Report(bool Reported);
uint8_t* tub(uint8_t* message);
#endif
