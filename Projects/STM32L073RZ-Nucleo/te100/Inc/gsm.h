#ifndef __GSM_H__
#define __GSM_H__

#include <stdint.h>
#include "nmea/nmea.h"
#include "can.h"
#include "gpsOne.h"
#include "geofence.h"

#define GSM_TASK_QUEUE_SIZE	4

#define GSM_TASK_CMD_NOP	0
#define GSM_TASK_CMD_GPS	1
#define GSM_TASK_CMD_PING	2
#define GSM_TASK_CMD_RET	3
#define GSM_TASK_CMD_ERR	4
#define GSM_TASK_CMD_SMS	5
#define GSM_TASK_CMD_LOGIN	6
#define GSM_TASK_CMD_UPDATE	7
#define GSM_TASK_CMD_ALARM	8
#define GSM_TASK_CMD_AT	9
#define GSM_TASK_CMD_OVERSPEED	10
#define GSM_TASK_CMD_ACCON	11
#define GSM_TASK_CMD_ACCOFF	12
#define GSM_TASK_CMD_IN_N	13
#define GSM_TASK_CMD_IN_P	14
#define GSM_TASK_CMD_EH	15
#define GSM_TASK_CMD_ML 16
#define GSM_TASK_CMD_GPS_FILEDATA 17
#define GSM_TASK_CMD_POLLPOSITION	18
#define GSM_TASK_CMD_GSMCELL	19
#define GSM_TASK_CMD_GSMCELL_NEIGHBOUR	20
#define GSM_TASK_CMD_GSMCELL_SEND 21
#define GSM_TASK_CMD_GEOFENCE	22
#define GSM_TASK_CMD_POINT 23
#define GSM_TASK_CMD_SIGNAL 24
#define GSM_TASK_CMD_SYNC	25
#define GSM_TASK_CMD_NEW_PING	26
#define GSM_TASK_CMD_RESEND_DATA	27
#define GSM_TASK_CMD_RTC 28
#define GSM_TASK_CMD_REQUEST 29
#define GSM_TASK_CMD_RECONNECT	30

struct GsmParams_s {
	const GgCommand_t	* GG_Command;
				int		SmsIndex;
};

typedef struct GsmParams_s GsmParams_t;

struct GsmTask_s {
	
#if (USE_CAN_SUPPORT)
		double CanData[CAN_VALUES_SIZE];
#endif
		nmeaGPRMC	GPRMC;
		const GgCommand_t *	GG_Command;
		int			SmsIndex;
		uint16_t	SenseA1;
		uint8_t		SenseD1;
		uint8_t		Command;
		uint8_t		Result;
		uint8_t		Content[20];
};

typedef struct GsmTask_s GsmTask_t;

uint8_t GsmAddTaskAlways(GsmTask_t * cmd);
uint8_t GsmAddTask(GsmTask_t * cmd);
int8_t * GsmIMEI(void);
void vGsmTask(void const *pvArg);
void gsmRtsLow(void);
void gsmDTRLow(void);
void gsmPowerOn(void);
void gsmPowerOff(void);
void gsmData(uint8_t data);
int frame(int8_t* dst,int cmd,bool local);
void setGsmSleep(bool sleep);
bool getGsmSleep(void);
bool getTCPConState(void);
void getBattVolt(void);
uint8_t gsmCmd1(const int8_t* text, uint16_t timeout);
void gsmSend(const int8_t *text);
void gsmDebugEnable(bool enable);
void answerIncomingCall(void);
bool getNewSmsIncoming(void);
void atCommand(const int8_t* cmd);
void clccIncomingCall(void);
int8_t* getSigalQuality(void);
int8_t* getVoltage(void);
void getSignal(void);
void checkSignal(void);
void accEvent(uint8_t event);
void inEvent(uint8_t event);
void ehEvent(uint8_t event,int8_t* cmd);
void mlEvent(uint8_t event,int8_t* cmd);
void pointEvent(uint8_t event);
void sosEvent(uint8_t event);
void batterylowEvent(uint8_t event);
void overspeedEvent(uint8_t event);
void cutdetectlowEvent(uint8_t event);
void shockEvent(uint8_t event);
void gpslostEvent(uint8_t event);
void gpsfixEvent(uint8_t event);
void accOnEvent(uint8_t event);
void geofenceEvent(uint8_t event);
void signalEvent(void);
void filedataEvent(void);
void rtcEvent(void);
void jammingEvent(void);
void initGsmUart(void);
nmeaGPRMC* getGPRMCAData(void);
bool getTaskWaiting(void);
void gsmATDebugEnable(bool enable);
bool gsmATDebug(void);
int8_t* gsmSenderDAnumber(void);
void write_locationdata(uint8_t Command,uint8_t alarm);
bool getCopylatlon(void);
int8_t * GsmIMSI(void);
int8_t * GsmICCID(void);
void handle_locationdata(void);
bool isJTSimcard(void);
#endif
